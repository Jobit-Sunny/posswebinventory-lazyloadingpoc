export const environment = {
  production: false,
  api_url: 'http://ec2-13-235-209-228.ap-south-1.compute.amazonaws.com/api/v1',
  i18nPrefix: '',
  shortcutConfigFilePath: '/assets/shortcut/config.json'
};
