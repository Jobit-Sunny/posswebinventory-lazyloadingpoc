export const environment = {
  production: true,
  api_url: 'http://ec2-13-234-124-250.ap-south-1.compute.amazonaws.com/api/v1',
  i18nPrefix: '/poss-web',
  shortcutConfigFilePath: '/assets/shortcut/config.json'
};
