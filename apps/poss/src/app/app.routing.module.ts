import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LayoutComponent } from './layout/layout.component';
import { ErrorPageComponent } from './error-page/error-page.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'inventory' },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'inventory',
    loadChildren: () =>
      import('@poss-web/features/inventory').then(m => m.InventoryModule)
  },
  {
    path: 'location',
    loadChildren: () =>
      import('@poss-web/features/inventorymaster').then(
        m => m.InventorymasterModule
      )
  },
  {
    path: '404',
    component: ErrorPageComponent
  },
  {
    path: '500',
    component: ErrorPageComponent
  },
  {
    path: '**',
    redirectTo: '404'
  }
];

@NgModule({
  // useHash supports github.io demo page, remove in your app
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
