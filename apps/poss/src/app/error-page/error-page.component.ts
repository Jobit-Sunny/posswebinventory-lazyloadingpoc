import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'poss-web-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.scss']
})
export class ErrorPageComponent implements OnInit {
  errorCode: string;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.errorPageCondition();
  }

  errorPageCondition() {
    if (this.router.url === '/404') {
      this.errorCode = '404';
    } else if (this.router.url === '/500') {
      this.errorCode = '500';
    } else {
      this.errorCode = '';
    }
  }

  back() {
    this.router.navigate(['..'], { relativeTo: this.activatedRoute });
  }
}
