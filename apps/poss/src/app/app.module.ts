import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { environment } from '../environments/environment';
//import { TranslateService } from '@ngx-translate/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  CoreModule,
  reducers,
  AppsettingEffects,
  debug,
  AppState,
  OverlayNotificationEffects,
  CORE_EFFECTS
} from '@poss-web/core';
import { SharedModule, UicomponentsModule } from '@poss-web/shared';
import { NxModule } from '@nrwl/angular';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {
  StoreRouterConnectingModule,
  RouterStateSerializer
} from '@ngrx/router-store';
import { storeFreeze } from 'ngrx-store-freeze';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { HomeComponent } from './home/home.component';
import { LayoutComponent } from './layout/layout.component';
import { TopMenuComponent } from './layout/navbar/top-menu/top-menu.component';
import { InventoryModule } from '@poss-web/features/inventory';
import { NavBarComponent } from './layout/navbar/navbar.component';
import { MetaReducer } from '@ngrx/store';
import { ErrorPageComponent } from './error-page/error-page.component';

export const metaReducers: MetaReducer<AppState>[] = [debug, storeFreeze];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LayoutComponent,
    TopMenuComponent,
    NavBarComponent,
    ErrorPageComponent
  ],
  imports: [
    BrowserModule,
    CoreModule.forRoot(environment),
    SharedModule,
    UicomponentsModule,
    AppRoutingModule,
    InventoryModule,
    BrowserAnimationsModule,
    NxModule.forRoot(),
    StoreModule.forRoot(reducers, {
      metaReducers: environment.production === false ? metaReducers : [],
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),

    StoreRouterConnectingModule.forRoot(),
    EffectsModule.forRoot(CORE_EFFECTS),
    environment.production === false
      ? StoreDevtoolsModule.instrument({
          maxAge: 25,
          logOnly: environment.production // isDevMode()
        })
      : []
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    // this language will be used as a fallback when a translation isn't found in the current language
    //translate.setDefaultLang('en');
    // // the lang to use, if the lang isn't available, it will use the current loader to get them
    // translate.use('en');
  }
}
