import { Component } from '@angular/core';

@Component({
  selector: 'poss-web-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {}
