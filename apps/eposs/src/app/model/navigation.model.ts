export class Navigation {
  name: string;
  url: string;
  children?: Navigation[];
  icon?: string;
  key: string[];
}
