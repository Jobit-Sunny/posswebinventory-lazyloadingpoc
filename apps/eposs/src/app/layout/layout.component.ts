import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';

import {
  AppsettingFacade,
  AuthFacade,
  PermissionsService
} from '@poss-web/core';
import { Navigation } from '../model/navigation.model';
import { SidenavService } from '../sidenav.service';

@Component({
  selector: 'poss-web-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  loginStatus$: Observable<string>;
  username$: Observable<string>;
  sidenav$: Observable<Navigation[]>;
  ACLMap$: Observable<Map<string, string[]>>;
  isHandset$: Observable<boolean>;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private appSettingFacade: AppsettingFacade,
    private authFacade: AuthFacade,
    private service: SidenavService,
    private permissionService: PermissionsService
  ) {
    this.isHandset$ = this.breakpointObserver
      .observe([Breakpoints.Medium, Breakpoints.Small])
      .pipe(map(result => result.matches));
  }

  ngOnInit() {
    this.ACLMap$ = this.permissionService.getACL();
    this.sidenav$ = this.service.getdata();
    this.loginStatus$ = this.appSettingFacade.getAuthStatus();
    this.username$ = this.authFacade.getUserName();
  }

  logOut = () => this.authFacade.logOut();
}
