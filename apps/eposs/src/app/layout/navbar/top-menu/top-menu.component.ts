import { Component, OnDestroy, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil, filter, take } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Event as NavigationEvent } from '@angular/router';

import {
  AuthFacade,
  AppsettingFacade,
  PermissionsService
} from '@poss-web/core';
import {
  OverlayNotificationService,
  OverlayNotificationType
} from '@poss-web/shared';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'poss-web-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit, OnDestroy {
  isLoading$: Observable<boolean>;
  destroy$: Subject<null> = new Subject<null>();
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));
  storeType$: Observable<boolean>;
  activated = {
    inventory: false,
    approvals: false
  };
  username$: Observable<string>;
  ACLMap: Map<string, string[]>;

  constructor(
    private router: Router,
    private authFacade: AuthFacade,
    private notification: OverlayNotificationService,
    private breakpointObserver: BreakpointObserver,
    private appsettingFacade: AppsettingFacade,
    private permissionService: PermissionsService
  ) {
    this.isLoading$ = authFacade.isLoading();
    this.username$ = authFacade.getUserName();

    this.authFacade
      .getLoginError()
      .pipe(
        takeUntil(this.destroy$),
        filter(error => error !== null)
      )
      .subscribe(error =>
        this.notification.show({
          type: OverlayNotificationType.ERROR,
          error: error,
          hasClose: true
        })
      );

    this.activatedRouterCheck(this.router.url);
    //TODO- To be removed once request approval library is created.
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        takeUntil(this.destroy$)
      )
      .subscribe(
        (event: NavigationEnd): void => {
          this.activatedRouterCheck(event.url);
        }
      );
  }

  activatedRouterCheck(url: string) {
    this.activated = {
      inventory: false,
      approvals: false
    };

    if (url.includes('inventory/approvals')) {
      this.activated = {
        inventory: false,
        approvals: true
      };
    } else if (url.match(`^/inventory`)) {
      this.activated = {
        inventory: true,
        approvals: false
      };
    }
  }

  ngOnInit(): void {
    this.permissionService
      .getACL()
      .pipe(
        filter(aclmap => !!aclmap),
        take(1)
      )
      .subscribe(ACLMap => (this.ACLMap = ACLMap));
    this.storeType$ = this.appsettingFacade
      .getStoreType()
      .pipe(map(storeType => storeType === 'ORG'));
  }

  logOut = () => this.authFacade.logOut();

  hasPermission = (permissions: string[]): boolean =>
    permissions.some(code =>
      code.length === 1
        ? this.ACLMap.has(code)
        : this.ACLMap.has(code.charAt(0))
        ? this.ACLMap.get(code.charAt(0)).includes(code)
        : false
    );

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
