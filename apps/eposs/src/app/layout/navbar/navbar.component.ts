import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Navigation } from '../../model/navigation.model';

@Component({
  selector: 'poss-web-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavBarComponent {
  @Input() sidenav: Navigation[] = [];
  @Input() username = '';
  @Input() ACLMap: Map<string, string[]>;

  @Output() sideNavToggle = new EventEmitter();
  @Output() logout = new EventEmitter();

  constructor() {}

  hasPermission = (permissions: string[]): boolean =>
    permissions.some(code =>
      code.length === 1
        ? this.ACLMap.has(code)
        : this.ACLMap.has(code.charAt(0))
        ? this.ACLMap.get(code.charAt(0)).includes(code)
        : false
    );
}
