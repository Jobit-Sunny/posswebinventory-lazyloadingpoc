import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Navigation } from './model/navigation.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SidenavService {
  constructor(private http: HttpClient) {}

  getdata(): Observable<Navigation[]> {
    return this.http
      .get<Navigation[]>('./assets/sidenav.json')
      .pipe(map(data => data['sidenav']));
  }
}
