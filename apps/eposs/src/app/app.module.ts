import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { environment } from '../environments/environment';
//import { TranslateService } from '@ngx-translate/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  CoreModule,
  reducers,
  debug,
  AppState,
  CORE_EFFECTS
} from '@poss-web/core';
import { SharedModule, UicomponentsModule } from '@poss-web/shared';
import { NxModule } from '@nrwl/angular';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {
  StoreRouterConnectingModule,
  RouterStateSerializer
} from '@ngrx/router-store';
import { storeFreeze } from 'ngrx-store-freeze';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { HomeComponent } from './home/home.component';
import { LayoutComponent } from './layout/layout.component';
import { TopMenuComponent } from './layout/navbar/top-menu/top-menu.component';
import { NavBarComponent } from './layout/navbar/navbar.component';
import { MetaReducer } from '@ngrx/store';
import { ErrorPageComponent } from './error-page/error-page.component';
import {
  TranslateModule,
  TranslateLoader,
  TranslateService
} from '@ngx-translate/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SidenavService } from './sidenav.service';

export const metaReducers: MetaReducer<AppState>[] = [debug, storeFreeze];

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LayoutComponent,
    TopMenuComponent,
    NavBarComponent,
    ErrorPageComponent
  ],
  imports: [
    BrowserModule,
    CoreModule.forRoot(environment),
    HttpClientModule,
    SharedModule,
    UicomponentsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NxModule.forRoot(),
    StoreModule.forRoot(reducers, {
      metaReducers: environment.production === false ? metaReducers : [],
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    StoreRouterConnectingModule.forRoot(),
    EffectsModule.forRoot(CORE_EFFECTS),
    environment.production === false
      ? StoreDevtoolsModule.instrument({
          maxAge: 25,
          logOnly: environment.production // isDevMode()
        })
      : []
  ],
  exports: [TranslateModule],
  providers: [SidenavService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(translate: TranslateService) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('en');
    // // the lang to use, if the lang isn't available, it will use the current loader to get them
    // translate.use('en');
  }
}
