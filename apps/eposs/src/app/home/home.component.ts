import { Component, OnInit } from '@angular/core';
import { ApiService } from '@poss-web/core';
import { TranslateService } from '@ngx-translate/core';
import { AppsettingFacade, AppsettingsState } from '@poss-web/core';
import { Observable } from 'rxjs';

import { AddressModel } from './address.model';
import { TEMPLATE1 } from '@poss-web/shared';
@Component({
  selector: 'poss-web-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  products$;
  setting$: Observable<AppsettingsState>;

  public currentStyle: string[];
  public formFields: any;
  public template = TEMPLATE1;

  private address: AddressModel = new AddressModel(
    123,
    'abccc',
    '',
    [{ id: '1', name: 'india' }, { id: '2', name: 'usa' }],
    [
      { id: '1', name: 'karnataka', country_id: '1' },
      { id: '2', name: 'telangana', country_id: '1' },
      { id: '3', name: 'california', country_id: '2' },
      { id: '4', name: 'texas', country_id: '2' }
    ],
    [
      { id: '1', name: 'bangalore', state_id: '1' },
      { id: '2', name: 'hyderabad', state_id: '2' },
      { id: '3', name: 'los Angeles', state_id: '3' },
      { id: '4', name: 'dallas', state_id: '4' }
    ]
  );

  constructor(private api: ApiService, private setting: AppsettingFacade) {}
  getCssProp() {
    const annot = (HomeComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs() {
    return {
      formConfig: this.setFormConfig(),
      formFields: this.address.buildFormFields()
    };

    // return this.product.buildFormFields();
  }
  public setFormConfig() {
    return {
      formName: 'Address Form',
      formDesc: 'Add Address Details',
      formTemplate: TEMPLATE1
    };
  }

  ngOnInit() {
    //this.products$ = this.api.get('/products');

    this.setting$ = this.setting.getSetting();

    this.formFields = this.getInputs();
    this.currentStyle = this.getCssProp();
  }
}
