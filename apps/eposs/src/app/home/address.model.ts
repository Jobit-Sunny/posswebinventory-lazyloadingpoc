import { Validators } from '@angular/forms';
import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';

export class AddressModel extends DynamicFormFieldsBuilder {
  private id: number;

  @FormField({ fieldType: FormFieldType.TEXT, label: 'Street1' })
  @Validation({ validators: [Validators.required] })
  private street1: string;

  @FormField({ fieldType: FormFieldType.TEXT_AREA, label: 'Street2' })
  @Validation({ validators: [Validators.required] })
  @Class({ className: ['class1', 'class2'] }) // Avi
  private street2: string;

  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: { labelKey: 'name', valueKey: 'id' },
    label: 'Country'
  })
  private country: { id: string; name: string }[];

  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      foreignKey: 'country_id'
    },
    label: 'State',
    dependsOn: '123-country'
  })
  @Class({ className: ['class1', 'class2'] }) // Avi
  @Validation({ validators: [Validators.required] })
  private state: { id: string; name: string; country_id: string }[];

  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      foreignKey: 'state_id'
    },
    label: 'City',
    dependsOn: '123-state'
  })
  private city: { id: string; name: string; state_id: string }[];

  constructor(
    id: number,
    street1: string,
    street2: string,
    country: { id: string; name: string }[],
    state: { id: string; name: string; country_id: string }[],
    city: { id: string; name: string; state_id: string }[]
  ) {
    super();
    this.id = id;
    this.street1 = street1;
    this.street2 = street2;
    this.country = country;
    this.state = state;
    this.city = city;
  }
}
