import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { AuthGuard } from '@poss-web/core';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login' },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'inventory',
    loadChildren: () =>
      import('@poss-web/features/inventory').then(m => m.InventoryModule),
      canActivate: [AuthGuard]
  },
  {
    path: 'master',
    loadChildren: () =>
      import('@poss-web/features/inventorymaster').then(
        m => m.InventorymasterModule
      ),
    canActivate: [AuthGuard]
  },
  {
    path: 'user-management',
    loadChildren: () =>
      import('@poss-web/features/userrolemgmt').then(m => m.UserrolemgmtModule)
  },
  {
    path: '404',
    component: ErrorPageComponent
  },
  {
    path: '500',
    component: ErrorPageComponent
  },
  { path: '**', redirectTo: 'inventory' }
];

@NgModule({
  // useHash supports github.io demo page, remove in your app
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: false
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
