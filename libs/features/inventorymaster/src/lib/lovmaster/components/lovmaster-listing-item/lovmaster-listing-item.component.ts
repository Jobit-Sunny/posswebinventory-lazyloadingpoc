import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LovMaster } from '../../models/lovmaster.model';

@Component({
  selector: 'poss-web-lovmaster-listing-item',
  templateUrl: './lovmaster-listing-item.component.html',
  styleUrls: ['./lovmaster-listing-item.component.scss']
})
export class LovmasterListingItemComponent implements OnInit {
  @Input() lovMasterItem: LovMaster;
  @Output() lovMasterCode = new EventEmitter<LovMaster>();
  @Output() lovMasterSwitch = new EventEmitter<LovMaster>();
  constructor() { }

  ngOnInit() {
  }

  getLovCode() {
    this.lovMasterCode.emit(this.lovMasterItem);
  }

  toggleChange($event: LovMaster) {
    const lovmaster = { ...$event, isActive: !$event.isActive }
    this.lovMasterSwitch.emit(lovmaster);
  }
}
