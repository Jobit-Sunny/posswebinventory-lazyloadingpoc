import { Component, OnInit, Input, OnChanges, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

import { LovMaster, LovMasterType } from '../../models/lovmaster.model';
import { Observable } from 'rxjs';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'poss-web-lovmaster-items',
  templateUrl: './lovmaster-items.component.html',
  styleUrls: ['./lovmaster-items.component.scss']
})
export class LovmasterItemsComponent implements OnInit {

  @Input() lovMasterList: LovMaster[];
  @Input() lovMasterTypes: LovMasterType[];
  @Input() isLoading: boolean;
  @Input() count: number;
  @Input() pageEvent: PageEvent;

  @Output() lovTypeSelect = new EventEmitter<string>();
  @Output() lovMasterItemEdit = new EventEmitter<LovMaster>();
  @Output() lovMasterSwitch = new EventEmitter<LovMaster>();
  @Output() searchKey = new EventEmitter<string>();

  searchBox = '';

  lovSelectedType: string;
  constructor() { }

  ngOnInit() {
  }


  selectedDropdown($event: any) {
    this.lovSelectedType = $event.value;
    this.lovTypeSelect.emit($event.value);
  }

  getLovMasterCode($event: LovMaster) {
    this.lovMasterItemEdit.emit($event)
  }

  lovSearch(key: string) {
    this.searchKey.emit(key);
  }

  clearSearch() {
    this.searchBox = '';
    this.lovSearch(this.searchBox);
  }

  toggleChange($event) {
    this.lovMasterSwitch.emit($event);
  }
}
