import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { LovMaster, LovMasterEnum, LovMasterType } from '../../models/lovmaster.model';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { LovMasterModel } from '../../models/lov-master.model';
import { TEMPLATE8, HelperFunctions } from '@poss-web/shared';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'poss-web-lovmaster-details',
  templateUrl: './lovmaster-details.component.html',
  styleUrls: ['./lovmaster-details.component.scss']
})
export class LovmasterDetailsComponent implements OnInit, OnDestroy {
  dialogData: LovMaster;
  lovMasterType: LovMasterType[];

  lovType: string;
  lovCode: string;
  description: string;
  isActive = true;

  destroy$: Subject<null> = new Subject<null>();

  /// below is dynamic form specific code
  public currentStyle: string[];
  public formFields: any;
  /// above is dynamic form specific code

  constructor(
    private hf: HelperFunctions,
    public dialogRef: MatDialogRef<LovmasterDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog
  ) {
    this.dialogData = data.data;
    this.lovMasterType = data.lovMasterType;
  }

  ngOnInit() {
    const form = this.prepareSet();
    this.formFields = this.getInputs(form);
    this.currentStyle = this.getCssProp();
  }


  /// below is dynamic form specific code

  prepareSet() {
    this.lovMasterType = this.hf.patchValue(this.lovMasterType, 'value', 'selected', this.dialogData.lovType, true);

    const lovMasterModel = new LovMasterModel(
      1,
      this.lovMasterType,
      this.dialogData.lovName,
      this.dialogData.description
    );
    return lovMasterModel;
  }

  getCssProp() {
    const annot = (LovmasterDetailsComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }
  public setFormConfig() {
    return {
      formName: 'Product Category Form',
      formDesc: 'Product Category',
      formTemplate: TEMPLATE8
    };
  }

  onCreate() {
    let mode = '';
    if (this.dialogData.lovName) {
      mode = LovMasterEnum.edit;
    } else {
      mode = LovMasterEnum.new;
    }

    const data: { mode: string, data: LovMaster } = {
      mode,
      data: {
        lovType: this.lovType,
        lovName: this.lovCode,
        description: this.description,
        isActive: this.dialogData.isActive,
      }
    }

    this.dialogRef.close(data);
  }
  onClose() {
    this.dialogRef.close();
  }

  addButton(formGroup: FormGroup) {

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        const formValues = formGroup.getRawValue();
        this.lovType = formValues['1-lovType'];
        this.lovCode = formValues['1-lovCode'];
        this.description = formValues['1-desctiption'];
        this.onCreate();
      }
    });


  }

  deleteButton() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.ngOnInit();
      }
    });
  }

  public formGroupCreated(formGroup: FormGroup) {
    if (this.dialogData.lovName) {
      formGroup.get('1-lovType').disable({ onlySelf: true });
      formGroup.get('1-lovCode').disable({ onlySelf: true });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
