import { Injectable } from '@angular/core';
import { ApiService } from '@poss-web/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { LoadLovListingSuccessPayload } from '../+state/lovmaster.actons';
import { getLOVReasonTypeListingUrl, getProductLOVTypeUrl, getLocationLOVTypeUrl, getInventoryLOVTypeUrl, getProductLOVUrl, getLocationLOVeUrl, getInventoryLOVUrl } from '../../endpoint.constants';
import { LovMasterAdaptor } from '../adaptors/lovmaster.adaptor';
import { LovMasterType, LovMaster } from '../models/lovmaster.model';

@Injectable()
export class LovMasterService {

  constructor(private apiService: ApiService) { }

  getLovMasterType(): Observable<LovMasterType[]> {
    // const url = getLOVReasonTypeListingUrl(); // Switch the endpoint based on the lovType
    // return this.apiService
    //   .get(url)
    //   .pipe(
    //     map(data => {
    //       return LovMasterAdaptor.getLOVReasonTypeListing(data); // Switch the adaptor based on the lovType
    //     })
    //   );
    const masterTypes = [
      // { value: 'LOCATIONTYPE', name: 'Location' },
      { value: 'REASONTYPE', name: 'Reason Type' },
      { value: 'PRODUCTTYPE', name: 'Product Type' },
      { value: 'INDENTTYPE', name: 'Indent Type' },
      { value: 'LOCATIONTYPE', name: 'Location Type' },
      { value: 'OWNERTYPE', name: 'owner Type' },
      { value: 'DEFECTTYPE', name: 'Defect Type' }
      // { value: 'PRICINGTYPE', name: 'Pricing type' },
    ];
    return of(masterTypes);
  }

  getLovMasterList(lovType: string): Observable<LoadLovListingSuccessPayload> {
    let url = '';//getLOVReasonTypeListingUrl(lovType); // Switch the endpoint based on the lovType
    switch (lovType) { // TODO, temp solution
      case 'REASONTYPE':
      case 'PRODUCTTYPE':
      case 'INDENTTYPE':
        url = getProductLOVTypeUrl(lovType);
        break;
      case 'LOCATIONTYPE':
      case 'OWNERTYPE':
        url = getLocationLOVTypeUrl(lovType);
        break;
      case 'DEFECTTYPE':
        url = getInventoryLOVTypeUrl(lovType);
        break;
    }

    return this.apiService
      .get(url)
      .pipe(
        map(data => {
          return LovMasterAdaptor.getLOVTypeListing(data); // Switch the adaptor based on the lovType
        })
      );
  }

  saveLovFormDetails(saveForm: LovMaster): Observable<LoadLovListingSuccessPayload> {
    let url = '';
    switch (saveForm.lovType) { // TODO, temp solution
      case 'REASONTYPE':
      case 'PRODUCTTYPE':
      case 'INDENTTYPE':
        url = getProductLOVTypeUrl(saveForm.lovType);
        break;
      case 'LOCATIONTYPE':
      case 'OWNERTYPE':
        url = getLocationLOVTypeUrl(saveForm.lovType);
        break;
      case 'DEFECTTYPE':
        url = getInventoryLOVTypeUrl(saveForm.lovType);
        break;
    }
    // const url = getProductLOVTypeUrl(saveForm.lovType);
    const formdata = {
      "values": [
        {
          "code": saveForm.lovName,
          "isActive": saveForm.isActive,
          "value": saveForm.description
        }
      ]
    }
    return this.apiService.patch(url, formdata).pipe(
      map(data => {
        return LovMasterAdaptor.getLOVTypeListing(data); // Switch the adaptor based on the lovType
      })
    );
  }

  createLovFormDetails(saveForm: LovMaster): Observable<LovMaster> {
    let url = '';
    switch (saveForm.lovType) { // TODO, temp solution
      case 'REASONTYPE':
      case 'PRODUCTTYPE':
      case 'INDENTTYPE':
        url = getProductLOVUrl();
        break;
      case 'LOCATIONTYPE':
      case 'OWNERTYPE':
        url = getLocationLOVeUrl();
        break;
      case 'DEFECTTYPE':
        url = getInventoryLOVUrl();
        break;
    }

    const formdata = {
      "code": saveForm.lovName,
      "lovType": saveForm.lovType,
      "value": saveForm.description
    };
    return this.apiService.post(url, formdata).pipe(
      map(data => {
        return LovMasterAdaptor.getLOVTypeCreate(data); // Switch the adaptor based on the lovType
      })
    );
  }
}
