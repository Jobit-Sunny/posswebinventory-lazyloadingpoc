
export interface LovMasterType {
  value: string;
  name: string;
}

export interface LovMaster {
  lovType: string;
  lovName: string;
  description: string;
  isActive: boolean;
}

export enum LovMasterEnum {
  new = 'new',
  edit = 'edit'
}

