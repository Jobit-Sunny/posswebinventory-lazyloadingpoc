import { Validators } from '@angular/forms';
import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';

export class LovMasterModel extends DynamicFormFieldsBuilder {
  private id: number;

  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'value',
      selectedKey: 'selected'
    },
    label: 'Select LOV type'
  })
  @Validation({ validators: [Validators.required] })
  @Class({ className: ['col-12'] })
  private lovType: { value: string; name: string }[];

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'LOV Name'
  })
  @Validation({ validators: [Validators.required] })
  @Class({ className: ['col-12'] })
  private lovCode: string;

  @FormField({
    fieldType: FormFieldType.TEXT_AREA,
    label: 'Description'
  })
  @Validation({ validators: [Validators.required] })
  @Class({ className: ['col-12'] })
  private desctiption: string;

  constructor(
    id: number,
    lovType: { value: string; name: string; checked?: boolean }[],
    lovCode: string,
    desctiption: string
  ) {
    super();
    this.id = id;
    this.lovType = lovType;
    this.lovCode = lovCode;
    this.desctiption = desctiption;
  }
}
