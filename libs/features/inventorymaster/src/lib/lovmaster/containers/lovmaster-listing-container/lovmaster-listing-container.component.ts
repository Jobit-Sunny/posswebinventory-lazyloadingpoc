import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';

import {
  LovMaster,
  LovMasterType,
  LovMasterEnum
} from '../../models/lovmaster.model';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { MatDialog, PageEvent } from '@angular/material';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationEventType
} from '@poss-web/shared';
import { LovMasterFacade } from '../../+state/lovmaster.facade';
import { takeUntil, take, map } from 'rxjs/operators';
import { LovmasterDetailsComponent } from '../../components/lovmaster-details/lovmaster-details.component';
import { getInventoryMasterRouteUrl } from '../../../page-route.constants';

@Component({
  selector: 'poss-web-lovmaster-listing-container',
  templateUrl: './lovmaster-listing-container.component.html',
  styleUrls: ['./lovmaster-listing-container.component.scss']
})
export class LovmasterListingContainerComponent implements OnInit {
  LOVType: string;
  destroy$ = new Subject<null>();
  isLoading$: Observable<boolean>;

  lovMasterTypes$: Observable<LovMasterType[]>;
  lovMasterTypes: LovMasterType[];
  lovMasterListing$: Observable<LovMaster[]>;
  lovMasterListingSrch$: Observable<LovMaster[]>;
  lovMasterCount$: Observable<number>;

  lovMasterPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };

  constructor(
    public dialog: MatDialog,
    public router: Router,
    private translate: TranslateService,
    private appsettingFacade: AppsettingFacade,
    private overlayNotification: OverlayNotificationService,
    private lovMasterFacade: LovMasterFacade
  ) { }

  ngOnInit() {
    this.stateInit();
  }

  stateInit() {
    this.lovMasterFacade.resetLovMasterData();

    this.lovMasterFacade.loadLovMasterTypes();
    this.lovMasterTypes$ = this.lovMasterFacade.getLovMasterTypes();

    this.lovMasterListing$ = this.lovMasterListingSrch$ = this.lovMasterFacade.getLovMasterListing();
    this.isLoading$ = this.lovMasterFacade.getIsLoading();
    this.lovMasterCount$ = this.lovMasterFacade.getTotalLovMasterDetails();

    this.lovMasterTypes$
      .pipe(take(1))
      .subscribe((typs: LovMasterType[]) => (this.lovMasterTypes = typs));

    this.lovMasterFacade
      .getLovDetailsSaveResponse()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          if (this.LOVType) {
            this.lovMasterFacade.loadLovMasterListing(this.LOVType);
          }
          this.showNotification('pw.lovmaster.saveSuccessMsg');
        }
      });

    this.lovMasterFacade
      .getLovDetailsEditResponse()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          this.showNotification('pw.lovmaster.editSuccessMsg');
        }
      });

    this.lovMasterFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });
  }

  lovSearch(key: string) {
    this.lovMasterListing$ = this.lovMasterListingSrch$.pipe(
      map((list: LovMaster[]) =>
        list.filter(ls => ls.lovName.toLowerCase().includes(key.toLowerCase()))
      )
    );
  }

  backArrow() {
    this.router.navigate([getInventoryMasterRouteUrl()]);
  }

  getLovTypeSelect($event: string) {
    this.LOVType = $event;
    this.lovMasterFacade.loadLovMasterListing(this.LOVType);
  }

  lovMasterItemEdit($event: LovMaster = null) {
    if (!$event) {
      $event = {
        lovName: '',
        lovType: '',
        description: '',
        isActive: true
      };
    }
    const dialogRef = this.dialog.open(LovmasterDetailsComponent, {
      width: '500px',
      height: 'auto',
      data: { lovMasterType: this.lovMasterTypes, data: $event }
    });

    dialogRef
      .afterClosed()
      .pipe(take(1))
      .subscribe((formData: { mode: string; data: LovMaster }) => {
        if (formData) {
          if (formData.mode === LovMasterEnum.edit) {
            this.lovMasterFacade.editLovFormDetails(formData.data);
          } else if (formData.mode === LovMasterEnum.new) {
            this.lovMasterFacade.saveLovFormDetails(formData.data);
          }
        }
      });
  }

  // showSuccessMessageNotification(msg: any) {
  //   this.overlayNotification
  //     .show({
  //       type: OverlayNotificationType.TIMER,
  //       message: msg,
  //       time: 2000,
  //       hasBackdrop: true
  //     })
  //     .events.pipe(takeUntil(this.destroy$))
  //     .subscribe((event: OverlayNotificationEventRef) => {
  //       if (event.eventType === OverlayNotificationEventType.CLOSE) {
  //       }
  //     });
  // }

  toggleChange($event) {
    this.lovMasterFacade.editLovFormDetails($event);
  }

  showNotification(key: string) {
    this.overlayNotification.close();

    this.translate
      .get(key)
      .pipe(take(1))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMsg,
            time: 2000,
            hasBackdrop: true
          })
          .events.pipe(take(1))
          .subscribe();
      });
  }

  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasBackdrop: true,
        hasClose: true,
        error: error
      })
      .events.pipe(take(1))
      .subscribe();
  }
}
