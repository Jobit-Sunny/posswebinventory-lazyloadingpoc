import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';


import * as lovMasterActions from './lovmaster.actons';
import { State } from '../../inventorymasters.state';
import { LovMasterSelectors } from './lovmaster.selectors';
import { LovMaster } from '../models/lovmaster.model';

@Injectable()
export class LovMasterFacade {
  constructor(private store: Store<State>) { }


  private lovMasterTypes$ = this.store.select(
    LovMasterSelectors.selectLovMasterTypes
  );

  private lovMasterListing$ = this.store.select(
    LovMasterSelectors.selectLovMasterListing
  );

  private lovMasterDetails$ = this.store.select(
    LovMasterSelectors.selectLovMasterDetails
  );

  private isLoading$ = this.store.select(
    LovMasterSelectors.selectIsLoading
  );


  private totalLovMasterDetails$ = this.store.select(
    LovMasterSelectors.selectTotalLovMasterDetails
  );

  private error$ = this.store.select(
    LovMasterSelectors.selectError
  );


  private isLovDetailsSaved$ = this.store.select(
    LovMasterSelectors.selectSaveLovMasterFormResponse
  );

  private isLovDetailsEdited$ = this.store.select(
    LovMasterSelectors.selectEditLovMasterFormResponse
  );

  getIsLoading() {
    return this.isLoading$;
  }

  getTotalLovMasterDetails() {
    return this.totalLovMasterDetails$;
  }


  getLovMasterTypes() {
    return this.lovMasterTypes$;
  }

  getLovMasterListing() {
    return this.lovMasterListing$;
  }


  getLovMasterDetails() {
    return this.lovMasterDetails$;
  }

  getError() {
    return this.error$;
  }

  getLovDetailsSaveResponse() {
    return this.isLovDetailsSaved$;
  }

  getLovDetailsEditResponse() {
    return this.isLovDetailsEdited$;
  }


  loadLovMasterTypes() {
    this.store.dispatch(
      new lovMasterActions.LoadLovTypes()
    );
  }

  loadLovMasterListing(
    lovType: string
  ) {
    this.store.dispatch(
      new lovMasterActions.LoadLovListing(
        lovType
      )
    );
  }

  saveLovFormDetails(saveFormDetails: LovMaster) {
    this.store.dispatch(
      new lovMasterActions.SaveLovFormDetails(saveFormDetails)
    );
  }

  editLovFormDetails(editFormDetails: LovMaster) {
    this.store.dispatch(
      new lovMasterActions.EditLovFormDetails(editFormDetails)
    );
  }

  resetLovMasterData() {
    this.store.dispatch(new lovMasterActions.ResetLovMasterData());
  }

}
