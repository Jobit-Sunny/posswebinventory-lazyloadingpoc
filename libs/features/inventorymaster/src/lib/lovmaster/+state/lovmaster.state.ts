
import { CustomErrors } from '@poss-web/core';
import { LovMaster, LovMasterType } from '../models/lovmaster.model';


export interface LovMasterState {
  error: CustomErrors;
  lovMasterTypes: LovMasterType[];
  lovMasterListing: LovMaster[];
  lovMasterDetails: LovMaster;
  saveLovMasterDetails: LovMaster;
  editLovMasterDetails: LovMaster[];
  totalMasterDetails: number;
  isLoading: boolean;
}
