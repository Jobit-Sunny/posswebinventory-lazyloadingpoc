import { createSelector } from '@ngrx/store';
import { selectInventoryMasters } from '../../inventorymasters.state';


const selectLovMasterTypes = createSelector(
  selectInventoryMasters,
  state => state.lovMasterState.lovMasterTypes
);



const selectLovMasterListing = createSelector(
  selectInventoryMasters,
  state => state.lovMasterState.lovMasterListing
);

const selectLovMasterDetails = createSelector(
  selectInventoryMasters,
  state => state.lovMasterState.totalMasterDetails
);


const selectTotalLovMasterDetails = createSelector(
  selectInventoryMasters,
  state => state.lovMasterState.totalMasterDetails
);

const selectIsLoading = createSelector(
  selectInventoryMasters,
  state => state.lovMasterState.isLoading
);


const selectError = createSelector(
  selectInventoryMasters,
  state => state.lovMasterState.error
);


const selectSaveLovMasterFormResponse = createSelector(
  selectInventoryMasters,
  state => state.lovMasterState.saveLovMasterDetails
);

const selectEditLovMasterFormResponse = createSelector(
  selectInventoryMasters,
  state => state.lovMasterState.editLovMasterDetails
);


export const LovMasterSelectors = {
  selectLovMasterTypes,
  selectLovMasterListing,
  selectLovMasterDetails,
  selectTotalLovMasterDetails,
  selectIsLoading,
  selectError,
  selectSaveLovMasterFormResponse,
  selectEditLovMasterFormResponse
};
