import { LoadLovListingSuccessPayload } from '../+state/lovmaster.actons';
import { LovMaster } from '../models/lovmaster.model';


export class LovMasterAdaptor {

    static getLOVTypeListing(
        data: any
    ): LoadLovListingSuccessPayload {
        const lovMasterListing: LovMaster[] = [];
        for (const listItem of data.values) {
            lovMasterListing.push({
                lovType: data.lovType,
                lovName: listItem.code,
                description: listItem.value,
                isActive: listItem.isActive
            });
        }

        const LovListing: LoadLovListingSuccessPayload = {
            LovListing: lovMasterListing,
            totalElements: 1
        };
        return LovListing;
    }

    static getLOVTypeCreate(
        data: any
    ): LovMaster {
        const lovMaster: LovMaster = {
            lovType: data.lovType,
            lovName: data.value,
            description: data.code,
            isActive: true
        };

        return lovMaster;
    }

}
