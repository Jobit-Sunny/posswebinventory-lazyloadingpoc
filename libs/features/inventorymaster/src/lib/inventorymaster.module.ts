import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {
  SharedModule,
  UicomponentsModule,
  DynamicFormModule
} from '@poss-web/shared';

import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { StoreModule } from '@ngrx/store';
import { FEATURE_NAME, reducers } from './inventorymasters.state';
import { EffectsModule } from '@ngrx/effects';

import {
  CopyDetailsDialogPopup,
  LocationListingItemComponent
} from './locationSetup/components/location-listing-item/location-listing-item.component';
import { LocationListItemsComponent } from './locationSetup/components/location-list-items/location-list-items.component';
import { LocationComponent } from './locationSetup/containers/details/location/location.component';
import { LocationGhsComponent } from './locationSetup/containers/details/location-ghs/location-ghs.component';
import { PrintComponent } from './locationSetup/containers/details/location-print/print.component';
import { LocationGrnIbtInventoryComponent } from './locationSetup/containers/details/location-grn/location-grn-ibt-inventory.component';
import { LocationLoyalityGepGcTepComponent } from './locationSetup/containers/details/location-loyality/location-loyality-gep-gc-tep.component';
import { LocationAdvanceCustomOrderFocComponent } from './locationSetup/containers/details/location-advance-custom-order-foc.component/location-advance-custom-order-foc.component';
import { LocationMasterEffect } from './locationSetup/+state/location-master.effect';

import { PrintSteponeComponent } from './locationSetup/components/details/print-stepone.component';

import { LocationMasterFacade } from './locationSetup/+state/location-master.facade';

import { MainLocationComponent } from './locationSetup/containers/details/mainlocation/mainlocation.component';
import { GHSSteponeComponent } from './locationSetup/components/details/ghs-stepone.component';

import { AdvanceCustomOrdersFocStepthreeComponent } from './locationSetup/components/details/advance-custom-orders-foc-stepthree.component';
import { AdvanceCustomOrdersFocSteptwoComponent } from './locationSetup/components/details/advance-custom-orders-foc-steptwo.component';
import { AdvanceCustomOrdersConfigurationStepOneComponent } from './locationSetup/components/details/advance-custom-order-configuration-stepone.component';
import { LocationListingComponent } from './locationSetup/containers/location-listing/location-listing.component';
import { GRNIBTInventorySteptwoComponent } from './locationSetup/components/details/grn-ibt-inventory.steptwo.component';
import { LoyalitySteponeComponent } from './locationSetup/components/details/loyality.stepone.component';
import { LoyalitySteptwoComponent } from './locationSetup/components/details/loyality.steptwo.component';
import { LoyalityStepthreeComponent } from './locationSetup/components/details/loyality.stepthree.component';
import { LocationSteponeComponent } from './locationSetup/components/details/location-stepone.component';
import { GHSSteptwoComponent } from './locationSetup/components/details/ghs-steptwo.component';
import { LocationSteptwoComponent } from './locationSetup/components/details/location-steptwo.component';
import { LocationStepthreeComponent } from './locationSetup/components/details/location-stepthree.component';
import { GRNIBTInventorySteponeComponent } from './locationSetup/components/details/grn-ibt-inventory.stepone.component';
import { BinGroupListItemsComponent } from './inventoryConfiguration/binGroup/components/bin-group-list-items/bin-group-list-items.component';
import { BinGroupListingComponent } from './inventoryConfiguration/binGroup/containers/bin-group-listing/bin-group-listing.component';
import { BinGroupListingItemComponent } from './inventoryConfiguration/binGroup/components/bin-group-listing-item/bin-group-listing-item.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BinGroupDetailsComponent } from './inventoryConfiguration/binGroup/components/bin-group-details/bin-group-details.component';
import { BinCodeListingComponent } from './inventoryConfiguration/bin/containers/bin-code-listing/bin-code-listing.component';
import { LocationDashboardComponent } from './locationSetup/location-dashboard/location-dashboard.component';
import { CorporateTownEffect } from './corporate-townSetup/+state/corporate-town.effect';
import { CorporateTownFacade } from './corporate-townSetup/+state/corporate-town.facade';
import { InventoryDashboardComponent } from './inventoryConfiguration/inventory-dashboard/inventory-dashboard.component';
import { CourierDetailsListingComponent } from './inventoryConfiguration/courier/containers/courier-details-listing/courier-details-listing.component';
import { CourierDetailsListingItemsComponent } from './inventoryConfiguration/courier/components/courier-details-listing-items/courier-details-listing-items.component';
import { BinGroupFacade } from './inventoryConfiguration/binGroup/+state/bin-group.facade';
import { BinFacade } from './inventoryConfiguration/bin/+state/bin.facade';
import { BinGroupEffect } from './inventoryConfiguration/binGroup/+state/bin-group.effect';
import { BinEffect } from './inventoryConfiguration/bin/+state/bin.effect';
import { CorporateTownListingComponent } from './corporate-townSetup/containers/corporate-town-listing/corporate-town-listing.component';
import { CorporateTownListingItemComponent } from './corporate-townSetup/components/corporate-town-listing-item/corporate-town-listing-item.component';
import { CorporateTownListItemsComponent } from './corporate-townSetup/components/corporate-town-list-items/corporate-town-list-items.component';
import { CorporateTownDetailsComponent } from './corporate-townSetup/components/corporate-town-details/corporate-town-details.component';
import { ProductCategoryService } from './productmaster/productCategory/services/product-category.service';

import { ProductCategoryFacade } from './productmaster/productCategory/+state/product-category.facade';

import { ProductCategoryItemsComponent } from './productmaster/productCategory/components/product-category-items/product-category-items.component';
import { ProductCategoryListingItemComponent } from './productmaster/productCategory/components/product-category-listing-item/product-category-listing-item.component';
import { ProductCategoryDetailsComponent } from './productmaster/productCategory/components/product-category-details/product-category-details.component';
import { ProductMastersDashboardComponent } from './productMaster/productCategory/product-masters-dashboard/product-masters-dashboard.component';
import { CourierDetailsEffects } from './inventoryConfiguration/courier/+state/courier-details.effect';
import { CourierDetailsFacade } from './inventoryConfiguration/courier/+state/courier-details.facade';
import { BinCodeDialogComponent } from './inventoryConfiguration/bin/components/bin-code-dialog/bin-code-dialog.component';
import { WeightToleranceListingItemComponent } from './inventoryConfiguration/weight-tolerance/components/weight-tolerance-listing-item/weight-tolerance-listing-item.component';
import { WeightToleranceListingItemsComponent } from './inventoryConfiguration/weight-tolerance/components/weight-tolerance-listing-items/weight-tolerance-listing-items.component';
import { WeightToleranceListingComponent } from './inventoryConfiguration/weight-tolerance/containers/weight-tolerance-listing/weight-tolerance-listing.component';
import { WeightToleranceEditItemComponent } from './inventoryConfiguration/weight-tolerance/containers/weight-tolerance-edit/weight-tolerance-edit-item/weight-tolerance-edit-item.component';
import { WeightToleranceLineItemComponent } from './inventoryConfiguration/weight-tolerance/containers/weight-tolerance-edit/weight-tolerance-line-item/weight-tolerance-line-item.component';
import { WeightToleranceEditComponent } from './inventoryConfiguration/weight-tolerance/containers/weight-tolerance-edit/weight-tolerance-edit.component';
import { WeightToleranceDetailComponent } from './inventoryConfiguration/weight-tolerance/containers/weight-tolerance-detail/weight-tolerance-detail.component';
import { WeightToleranceEffect } from './inventoryConfiguration/weight-tolerance/+state/weight-tolerance.effect';
import { WeightToleranceFacade } from './inventoryConfiguration/weight-tolerance/+state/weight-tolerance-facade';
import { BrandMasterEffect } from './brand-master/+state/brand-master.effect';
import { ProductCategoryListingComponent } from './productmaster/productCategory/containers/product-category-listing/product-category-listing.component';
import { BrandListingComponent } from './brand-master/containers/brand-listing/brand-listing.component';
import { ProductCategoryEffect } from './productmaster/productCategory/+state/product-category.effect';
import { BrandListngItemComponent } from './brand-master/components/brand-listng-item/brand-listng-item.component';
import { BrandListngItemsComponent } from './brand-master/components/brand-listng-items/brand-listng-items.component';
import { BrandComponent } from './brand-master/containers/details/brand/brand.component';
import { PancardConfigurationComponent } from './brand-master/containers/details/pancard-configuration/pancard-configuration.component';
import { CurrencyComponent } from './brand-master/containers/details/currency/currency.component';
import { CMSMSConfigurationComponent } from './brand-master/containers/details/cm-sms-configuration/cm-sms-configuration.component';
import { ResidualAmountComponent } from './brand-master/containers/details/residual-amount/residual-amount.component';
import { BrandStepOneComponent } from './brand-master/components/details/brand.component';
import { MainbrandMasterComponent } from './brand-master/containers/details/mainbrand-master/mainbrand-master.component';
import { CMSMSConfigurationStepOneComponent } from './brand-master/components/details/cmsmsconfiguration.component';
import { CurrencyStepOneComponent } from './brand-master/components/details/currency.component';
import { PancardConfigurationStepOneComponent } from './brand-master/components/details/pancard-configuration.component';
import { ResidualAmountStepOneComponent } from './brand-master/components/details/residual-amount.component';
import { CourierDetailsListingItemComponent } from './inventoryConfiguration/courier/components/courier-details-listing-item/courier-details-listing-item.component';
import { BrandMasterFacade } from './brand-master/+state/brand-master.facade';
import { LovMasterService } from './lovmaster/services/lov-master.service';
import { LovmasterListingContainerComponent } from './lovmaster/containers/lovmaster-listing-container/lovmaster-listing-container.component';
import { LovMasterFacade } from './lovmaster/+state/lovmaster.facade';
import { LovMasterEffect } from './lovmaster/+state/lovmaster.effect';
import { LovmasterItemsComponent } from './lovmaster/components/lovmaster-items/lovmaster-items.component';
import { LovmasterListingItemComponent } from './lovmaster/components/lovmaster-listing-item/lovmaster-listing-item.component';
import { LovmasterDetailsComponent } from './lovmaster/components/lovmaster-details/lovmaster-details.component';
import { ConfirmDialogComponent } from './master/common/confirm-dialog/confirm-dialog.component';
import { CFAProductCodeComponent } from './productMaster/CFAProductCode/containers/details/cfa-product-code/cfa-product-code.component';
import { CFAProductCodeFacade } from './productMaster/CFAProductCode/+state/cfa-product-code.facade';
import { CFAProductCodeEffects } from './productMaster/CFAProductCode/+state/cfa-product-code.effects';
import { CfaProductCodeListingComponent } from './productMaster/CFAProductCode/containers/cfa-product-code-listing/cfa-product-code-listing.component';
import { CfaProductCodeItemsComponent } from './productMaster/CFAProductCode/components/cfa-product-code-items/cfa-product-code-items.component';
import { CfaProductCodeItemComponent } from './productMaster/CFAProductCode/components/cfa-product-code-item/cfa-product-code-item.component';
import { CourierDetailsSteponeComponent } from './inventoryConfiguration/courier/components/details/courier-details-stepone.component';
import { CourierDetailsComponent } from './inventoryConfiguration/courier/containers/details/courier-details/courier-details.component';
import { SubbrandListComponent } from './subbrand/containers/subbrand-list/subbrand-list.component';
import { SubbrandDetailsComponent } from './subbrand/components/subbrand-details/subbrand-details.component';
import { SubbrandListItemComponent } from './subbrand/components/subbrand-list-item/subbrand-list-item.component';
import { SubbrandListItemsComponent } from './subbrand/components/subbrand-list-items/subbrand-list-items.component';
import { ItemSteponeComponent } from './productmaster/item/components/details/item-stepone.component';
import { ItemContainerComponent } from './productmaster/item/container/item-container/item-container.component';
import { MainItemContainerComponent } from './productmaster/item/container/main-item-container/main-item-container.component';
import { ItemListingComponent } from './productmaster/item/container/item-listing/item-listing.component';
import { ItemEffect } from './productmaster/item/+state/item.effects';
import { ItemListingService } from './productmaster/item/services/item-listing.service';
import { ItemFacade } from './productmaster/item/+state/item.facade';
import { ListItemComponent } from './productmaster/item/components/list-item/list-item.component';
import { ItemsComponent } from './productmaster/item/components/items/items.component';
import { ItemStoneContainerComponent } from './productmaster/item/container/item-stone-container/item-stone-container.component';
import { ItemMakingchargeContainerComponent } from './productmaster/item/container/item-makingcharge-container/item-makingcharge-container.component';
import { CFAProductCodeSteponeComponent } from './productMaster/CFAProductCode/components/details/cfa-product-code-stepone.component';
@NgModule({
  imports: [
    SharedModule,
    UicomponentsModule,
    MatSlideToggleModule,
    ReactiveFormsModule,
    MatIconModule,
    CommonModule,
    MatInputModule,
    MatDialogModule,
    MatFormFieldModule,
    FormsModule,
    MatPaginatorModule,
    DynamicFormModule,
    RouterModule.forChild([
      {
        path: '',
        component: DashboardComponent
      },
      {
        path: 'inventory/courierDetailsListing',
        component: CourierDetailsListingComponent
      },

      {
        path: 'productMasters/productCategory-list',
        component: ProductCategoryListingComponent
      },
      {
        path: 'productMasters/CFAProducts',
        component: CfaProductCodeListingComponent
      },

      {
        path: 'productMasters/brandlist',
        component: BrandListingComponent
      },

      {
        path: 'productMasters/subbrandlist',
        component: SubbrandListComponent
      },

      {
        path: 'location/location-list',
        component: LocationListingComponent
      },
      {
        path: 'location/corporate-towns',
        component: CorporateTownListingComponent
      },
      {
        path: 'inventory',
        component: InventoryDashboardComponent
      },

      {
        path: 'inventory/binGroup',
        component: BinGroupListingComponent
      },

      {
        path: 'inventory/binGroup/:binGroupCode',
        component: BinGroupDetailsComponent
      },
      {
        path: 'inventory/binCode',
        component: BinCodeListingComponent
      },

      // {
      //   path: 'inventory/binCode/edit',
      //   component: BinCodeEditComponent
      // },

      {
        path: 'inventory/binCode/:locationCode',
        component: BinCodeListingComponent
      },

      {
        path: 'location',
        component: LocationDashboardComponent
      },
      {
        path: 'locationMasters',
        redirectTo: 'locationMasters/NEW'
      },
      {
        path: 'productMasters',
        component: ProductMastersDashboardComponent
      },
      {
        path: 'inventory/courierDetailsListing/:courierName',
        component: CourierDetailsComponent
      },

      { path: 'productMaster', redirectTo: 'productMaster/NEW' },
      {
        path: 'productMaster/:brandCode',
        component: MainbrandMasterComponent,
        children: [
          { path: 'brand/:page', component: BrandComponent },
          { path: 'pancard/:page', component: PancardConfigurationComponent },
          { path: 'residualamount/:page', component: ResidualAmountComponent },
          { path: 'SMSConfig/:page', component: CMSMSConfigurationComponent },
          {
            path: 'currency/:page',
            component: CurrencyComponent
          }
        ]
      },
      {
        path: 'productMasters/:productGroupCode/CFAProduct',
        component: CFAProductCodeComponent
      },

      {
        path: 'locationMasters/:locCode',
        component: MainLocationComponent,
        children: [
          { path: 'location', redirectTo: 'location/1' },
          { path: 'location/:page', component: LocationComponent },
          { path: 'ghs', redirectTo: 'ghs/1' },
          { path: 'ghs/:page', component: LocationGhsComponent },
          { path: 'print', redirectTo: 'print/1' },
          { path: 'print/:page', component: PrintComponent },
          { path: 'grn', redirectTo: 'grn/1' },
          { path: 'grn/:page', component: LocationGrnIbtInventoryComponent },
          { path: 'loyality', redirectTo: 'loyality/1' },
          { path: 'location/2', component: LocationSteptwoComponent },
          {
            path: 'loyality/:page',
            component: LocationLoyalityGepGcTepComponent
          },
          { path: 'advance', redirectTo: 'advance/1' },
          {
            path: 'advance/:page',
            component: LocationAdvanceCustomOrderFocComponent
          },
          { path: ':page', pathMatch: 'full', redirectTo: '1' }
        ]
      },

      {
        path: 'product/productCategory',
        component: ProductCategoryListingComponent
      },
      {
        path: 'inventory/weighttolerance',
        component: WeightToleranceListingComponent
      },

      {
        path: 'inventory/weighttolerance/details',
        component: WeightToleranceDetailComponent
      },
      {
        path: 'inventory/weighttolerance/edit/:configId',
        component: WeightToleranceEditComponent
      },
      {
        path: 'lovmaster',
        component: LovmasterListingContainerComponent
      },
      {
        path: 'itemMaster',
        component: ItemListingComponent
      },
      {
        path: 'itemMaster/:itemCode',
        component: MainItemContainerComponent,
        children: [
          // { path: 'itemDetails', redirectTo: 'itemDetails/1' },
          { path: 'itemDetails', component: ItemContainerComponent },
          // { path: 'stoneDetails', redirectTo: 'ghs/1' },
          { path: 'itemStones', component: ItemStoneContainerComponent },
          // { path: 'makingChargesDetails', redirectTo: 'print/1' },
          {
            path: 'itemMakingCharges',
            component: ItemMakingchargeContainerComponent
          }
        ]
      }
    ]),
    StoreModule.forFeature(FEATURE_NAME, reducers),
    EffectsModule.forFeature([
      LocationMasterEffect,
      CorporateTownEffect,
      BinGroupEffect,
      BinEffect,
      ProductCategoryEffect,
      CourierDetailsEffects,
      CorporateTownEffect,
      BinGroupEffect,
      BinEffect,
      WeightToleranceEffect,
      BrandMasterEffect,
      LovMasterEffect,
      CFAProductCodeEffects,
      ItemEffect
    ])
  ],
  declarations: [
    WeightToleranceListingItemComponent,
    WeightToleranceListingItemsComponent,
    WeightToleranceListingComponent,
    WeightToleranceEditItemComponent,
    WeightToleranceLineItemComponent,
    WeightToleranceEditComponent,
    WeightToleranceDetailComponent,
    CopyDetailsDialogPopup,
    CorporateTownDetailsComponent,
    BinGroupDetailsComponent,
    LocationComponent,
    LocationSteptwoComponent,
    LocationStepthreeComponent,
    AdvanceCustomOrdersFocStepthreeComponent,
    AdvanceCustomOrdersFocSteptwoComponent,
    PrintComponent,
    CourierDetailsComponent,

    // PrintSteptwoComponent,
    GRNIBTInventorySteponeComponent,
    GRNIBTInventorySteptwoComponent,
    LoyalitySteponeComponent,
    LoyalitySteptwoComponent,
    LoyalityStepthreeComponent,
    AdvanceCustomOrdersConfigurationStepOneComponent,

    MainLocationComponent,
    PrintSteponeComponent,
    LocationGrnIbtInventoryComponent,
    LocationLoyalityGepGcTepComponent,
    LocationAdvanceCustomOrderFocComponent,
    LocationGhsComponent,
    LocationSteponeComponent,
    LocationListingComponent,
    LocationListingItemComponent,
    LocationListItemsComponent,
    GHSSteponeComponent,
    GHSSteptwoComponent,
    BinGroupListItemsComponent,
    BinGroupListingComponent,
    BinGroupListingItemComponent,
    DashboardComponent,
    InventoryDashboardComponent,
    BinCodeListingComponent,
    LocationDashboardComponent,
    CorporateTownListingComponent,
    CorporateTownListingItemComponent,
    CorporateTownListItemsComponent,
    CorporateTownDetailsComponent,
    BrandListngItemComponent,
    BrandListngItemsComponent,
    BrandListingComponent,
    BrandComponent,
    PancardConfigurationComponent,
    CurrencyComponent,
    CMSMSConfigurationComponent,
    ResidualAmountComponent,
    BrandStepOneComponent,
    MainbrandMasterComponent,
    CMSMSConfigurationStepOneComponent,

    CurrencyStepOneComponent,
    PancardConfigurationStepOneComponent,
    ResidualAmountStepOneComponent,
    ProductCategoryListingComponent,
    ProductCategoryItemsComponent,
    ProductCategoryListingItemComponent,
    ProductCategoryDetailsComponent,
    ProductMastersDashboardComponent,
    CourierDetailsListingComponent,
    CourierDetailsComponent,
    CourierDetailsListingItemsComponent,
    CourierDetailsListingItemComponent,
    BinCodeDialogComponent,
    LovmasterListingContainerComponent,
    LovmasterItemsComponent,
    LovmasterListingItemComponent,
    LovmasterDetailsComponent,
    ConfirmDialogComponent,
    CFAProductCodeComponent,
    CFAProductCodeSteponeComponent,
    CFAProductCodeComponent,
    CfaProductCodeListingComponent,
    CfaProductCodeItemsComponent,
    CfaProductCodeItemComponent,
    CourierDetailsSteponeComponent,
    SubbrandListComponent,
    SubbrandListItemComponent,
    SubbrandListItemsComponent,
    SubbrandDetailsComponent,

    ItemSteponeComponent,
    ItemContainerComponent,
    MainItemContainerComponent,
    ItemListingComponent,
    ListItemComponent,
    ItemsComponent,
    ItemStoneContainerComponent,
    ItemMakingchargeContainerComponent
  ],
  providers: [
    LocationMasterFacade,
    BinGroupFacade,
    BinFacade,
    CorporateTownFacade,
    ProductCategoryService,
    ProductCategoryFacade,
    CourierDetailsFacade,
    WeightToleranceFacade,
    BrandMasterFacade,
    LovMasterService,
    LovMasterFacade,
    CFAProductCodeFacade,
    WeightToleranceFacade,
    ItemListingService,
    ItemFacade
  ],
  entryComponents: [
    CopyDetailsDialogPopup,
    CorporateTownDetailsComponent,
    ProductCategoryDetailsComponent,
    BinCodeDialogComponent,
    LovmasterDetailsComponent,
    ConfirmDialogComponent,
    SubbrandDetailsComponent
  ]
})
export class InventorymasterModule {}
