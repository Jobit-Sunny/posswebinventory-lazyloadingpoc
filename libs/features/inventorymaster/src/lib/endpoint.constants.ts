import { HttpParams } from '@angular/common/http';
import {
  LoadLocationsByBinGroupAndBinCode,
  LocationsByBinGroupAndBinCodePayload
} from './inventoryConfiguration/bin/+state/bin.actions';
import { Observable } from 'rxjs';

export const getLocationBaseUrl = (): string => {
  return `/location/v1`;
};
const getInventoryBaseUrl = (): string => {
  return `/inventory/v2`;
};
export const getProductBaseUrl = (): string => {
  return `/product/v1`;
};

export const getLocationDetailsUrl = (page, pageSize): string => {
  return (
    getLocationBaseUrl() + '/locations' + `?&page=${page}&size=${pageSize}`
  );
};

// export const getLocationDetailsByLocationCodeUrl = (locationCode):  { path: string; params: HttpParams }  => {
//   const params = new HttpParams()
//     .set('locationCode', locationCode)

//   return {
//     path:getLocationBaseUrl() + '/locations',
//     params
//   }
// };

export const getSearchLocationByLocationCode = (locationCode): string => {
  return (
    getLocationBaseUrl() + '/locations/' + encodeURIComponent(locationCode)
  );
};

export const getLocationDetailsByLocationCodeUrl = (locationCode): string => {
  return getLocationBaseUrl() + '/locations' + `/${locationCode}`;
};

export const getLocationTypeUrl = (LOCATIONTYPE): string => {
  return getLocationBaseUrl() + '/lovs' + `/${LOCATIONTYPE}`;
};

export const getLocationFormatUrl = (LOCATIONFORMAT): string => {
  return getLocationBaseUrl() + '/lovs' + `/${LOCATIONFORMAT}`;
};

export const getCountryUrl = (): string => {
  return getLocationBaseUrl() + '/countries';
};

export const getLocationStateUrl = (): string => {
  return getLocationBaseUrl() + '/states';
};

export const getLocationTownUrl = (): string => {
  return getLocationBaseUrl() + '/towns';
};

export const getLocationOwnerTypeUrl = (LOCATIONTYPE): string => {
  return getLocationBaseUrl() + '/lovs' + `/${LOCATIONTYPE}`;
};

export const getLocationRegionUrl = (): string => {
  return getLocationBaseUrl() + '/regions';
};

export const getLocationAllRegionUrl = (): string => {
  return getLocationBaseUrl() + '/regions?regionType=ALL';
};

export const getBrandUrl = (): string => {
  return '/product' + '/v1' + '/brands?size=100'; // ToDO, it should not be page size here
};
export const getSaveLocationDetailsUrl = (): string => {
  return getLocationBaseUrl() + '/locations';
};
export const getSaveFormDetailsUrl = (locationCode: string): string => {
  return getLocationBaseUrl() + '/locations' + `/${locationCode}`;
};
//courierDetails Listing
export const getCourierDetailsListingUrl = (
  page: number,
  pageSize: number
): string => {
  return (
    getInventoryBaseUrl() + '/couriers' + `?&page=${page}&size=${pageSize}`
  );
};
export const getCourierDetailsBasedOnCourierNameUrl = (
  courierName: string
): string => {
  const courier = encodeURIComponent(courierName);
  return getInventoryBaseUrl() + '/couriers' + `/${courier}`;
};
export const getSaveCourierDetailsUrl = (): string => {
  return getInventoryBaseUrl() + '/couriers';
};
export const getUpdateCourierDetailsUrl = (courierName: string): string => {
  return getInventoryBaseUrl() + '/couriers' + `/${courierName}`;
};
export const getSelectedLocationsUrl = (courierName: string): string => {
  return getInventoryBaseUrl() + '/couriers' + `/${courierName}` + '/locations';
};
export const getLocationMappingUrl = (courierName: string): string => {
  return getInventoryBaseUrl() + '/couriers' + `/${courierName}` + '/locations';
};

export const getCopyLocationUrl = (
  sourceLocationCode: string,
  destinationLocationCode: string
): string => {
  return (
    getLocationBaseUrl() +
    '/locations' +
    '/src' +
    `/${sourceLocationCode}` +
    '/dst' +
    `/${destinationLocationCode}`
  );
};

export const getBaseCurrencyUrl = (): string => {
  return getLocationBaseUrl() + '/countries';
};

export const getBinGroupDetailsListingUrl = (
  page: number,
  pageSize: number
): string => {
  if (pageSize > 0) {
    return (
      getInventoryBaseUrl() + '/bingroups' + `?&page=${page}&size=${pageSize}`
    );
  } else {
    return getInventoryBaseUrl() + '/bingroups' + `?isPageable=false`;
  }
};

export const getBinGroupByBinGroupCode = (binGroupCode: string) => {
  const encodedBinGroup = encodeURIComponent(binGroupCode);
  return getInventoryBaseUrl() + '/bingroups' + `/${encodedBinGroup}`;
};

export const getBinGroupEditedFormDetailsUrl = (
  binGroupCode: string
): string => {
  return getInventoryBaseUrl() + '/bingroups' + `/${binGroupCode}`;
};

export const getBinGroupSaveFormDetailsUrl = (): string => {
  return getInventoryBaseUrl() + '/bingroups';
};

export const getBinCodeSaveNewFormDetailsUrl = (): string => {
  return getInventoryBaseUrl() + '/bins';
};

// export const getBinCodeDetailsListingUrl = (
//   page: number,
//   pageSize: number
// ): string => {
//   return (
//     getLocationBaseUrl() + '/locations' + `?&page=${page}&size=${pageSize}`
//   );
// };

export const getBinCodesByBinGroupCode = (binGroupCode: string): string => {
  return getInventoryBaseUrl() + '/bingroups' + `/${binGroupCode}` + '/bins';
};

export const getBinCodeEditedFormDetailsUrl = (binCode: string): string => {
  return getInventoryBaseUrl() + '/bins' + `/${binCode}`;
};

export const getCorporateTownListingUrl = (
  page: number,
  pageSize: number
): string => {
  return getLocationBaseUrl() + '/towns' + `?page=${page}&size=${pageSize}`;
};

export const getStateListingUrl = (page: number, pageSize: number): string => {
  return getLocationBaseUrl() + '/states' + `?&page=${page}&size=${pageSize}`;
};

export const getTownDetailsByTownCodeUrl = (townCode: number): string => {
  return getLocationBaseUrl() + '/towns' + `/${townCode}`;
};

export const getSaveTownFormDetailsUrl = (): string => {
  return getLocationBaseUrl() + '/towns';
};

export const getTownEditedFormDetailsUrl = (townCode: number): string => {
  return getLocationBaseUrl() + '/towns' + `/${townCode}`;
};

export const getRegionListingUrl = (page: number, pageSize: number): string => {
  return (
    getLocationBaseUrl() +
    '/regions' +
    `?page=${page}&size=${pageSize}&regionType=ALL`
  );
};

export const getProductCategoryDetailsListingUrl = (
  page: number,
  pageSize: number
): string => {
  return (
    getProductBaseUrl() +
    '/product-categories' +
    `?&page=${page}&size=${pageSize}`
  );
};

export const getProductCategoryByProductCategoryCodeUrl = (
  productCategoryCode: string
): string => {
  return (
    getProductBaseUrl() +
    '/product-categories' +
    `/${encodeURIComponent(productCategoryCode)}`
  );
};

export const getProductCategorySaveFormDetailsUrl = (): string => {
  return getProductBaseUrl() + '/product-categories';
};
//weight-tolerance
export const getConfigDetailsUrl = (
  configType: string,
  page: any,
  pageSize: any
): string => {
  return (
    getInventoryBaseUrl() +
    '/configs' +
    '?configType=' +
    `${configType}` +
    `&page=${page}&size=${pageSize}`
  );
};

export const getselectedConfigDetailsUrl = (configId: string): string => {
  return getInventoryBaseUrl() + '/configs/' + `${configId}`;
};

export const getSaveConfigDetailsUrl = (): string => {
  return getInventoryBaseUrl() + '/configs';
};

export const getSaveWeightToleranceUrl = (configId: any): string => {
  return (
    getInventoryBaseUrl() +
    '/configs/' +
    `${configId}` +
    '/values' +
    '?configType=' +
    'WEIGHT_TOLERANCE'
  );
};

export const getSearchConfigDetailsByConfigIdUrl = (configId: any): string => {
  return getInventoryBaseUrl() + '/configs/' + encodeURIComponent(configId);
};
export const getUpdateWeightToleranceUrl = (
  configId: any,
  configProductId: any
): string => {
  return (
    getInventoryBaseUrl() +
    '/configs/' +
    `${configId}` +
    '/values/' +
    `${configProductId}` +
    '?configType=' +
    'WEIGHT_TOLERANCE'
  );
};

export const getLoadProductCodeUrl = (): string => {
  return getProductBaseUrl() + '/product-groups?' + 'isPageable=false';
};
export const getRangeWeightUrl = (): string => {
  return getInventoryBaseUrl() + '/ranges?' + 'isPageable=false';
};

export const getProductCategoryCodeUrl = (): string => {
  return getProductBaseUrl() + '/product-categories?' + 'isPageable=false';
};

export const getOrgCodeUrl = (): string => {
  //TODO return url
  return;
};
export const getBrandListUrl = (pageIndex, pageSize): string => {
  return (
    '/product' +
    '/v1' +
    '/brands?' +
    'page=' +
    `${pageIndex}` +
    '&size=' +
    `${pageSize}`
  );
};

export const getParentBrandListUrl = (): string => {
  return '/product' + '/v1' + '/brands?' + 'isPageable=false';
};

export const getSaveBrandUrl = (): string => {
  return getProductBaseUrl() + '/brands';
};

export const getUpdateBrandUrl = (brandCode): string => {
  return getProductBaseUrl() + '/brands' + `/${brandCode}`;
};
export const getBrandDetailsByBrandCode = (brandCode): string => {
  return getProductBaseUrl() + '/brands/' + `${brandCode}`;
};

export const getLOVReasonTypeListingUrl = (lovType: string): string => {
  return getProductBaseUrl() + '/lovs/' + lovType;
};
export const getBinDetailsByBinNameUrl = (binName): string => {
  return getInventoryBaseUrl() + '/bins/' + `${binName}`;
};
export const getCFAProductsUrl = (page: number, pageSize: number): string => {
  return (
    getProductBaseUrl() + '/product-groups' + `?&page=${page}&size=${pageSize}`
  );
};
export const getCFAProductsBasedOnProductGroupCodeUrl = (
  productGroupCode
): string => {
  const CFAProductCode = encodeURIComponent(productGroupCode);
  return getProductBaseUrl() + '/product-groups' + `/${CFAProductCode}`;
};
export const getSaveCFAProductsUrl = (): string => {
  return getProductBaseUrl() + '/product-groups';
};

export const getUpdateCFAProductsUrl = (productGroupCode: string): string => {
  return getProductBaseUrl() + '/product-groups' + `/${productGroupCode}`;
};

export const getProductTypesUrl = (): string => {
  return getProductBaseUrl() + '/lovs' + '/PRODUCTTYPE';
};
export const getMaterialTypesUrl = (): string => {
  return getProductBaseUrl() + '/materials';
};

export const getProductLOVUrl = (): string => {
  return getProductBaseUrl() + '/lovs';
};

export const getLocationLOVeUrl = (): string => {
  return getLocationBaseUrl() + '/lovs';
};

export const getInventoryLOVUrl = (): string => {
  return getInventoryBaseUrl() + '/lovs';
};

export const getProductLOVTypeUrl = (lovType: string): string => {
  return getProductLOVUrl() + '/' + lovType;
};

export const getLocationLOVTypeUrl = (lovType: string): string => {
  return getLocationLOVeUrl() + '/' + lovType;
};

export const getInventoryLOVTypeUrl = (lovType: string): string => {
  return getInventoryLOVUrl() + '/' + lovType;
};

export const getLoadMappedLocationUrl = (configId): string => {
  return getInventoryBaseUrl() + '/configs/' + `${configId}` + '/locations';
};

export const getSaveLocationMappingUrl = (configId): string => {
  return getInventoryBaseUrl() + '/configs/' + `${configId}` + '/locations';
};
export const getUpdateIsActiveUrl = (configId): string => {
  return getInventoryBaseUrl() + '/configs/' + `${configId}`;
};
export const getSearchBrandByBrandCode = (brandCode): string => {
  // const params=new HttpParams()
  // params.set('brandCode',brandCode)
  // return {path:getProductBaseUrl()+ '/brands/',params:params}
  return getProductBaseUrl() + '/brands/' + encodeURIComponent(brandCode); //TODO
};
export const getBrandDetialsByBrandCode = (brandCode): string => {
  return getProductBaseUrl() + '/brands/' + `${brandCode}`;
};

export const generateBinCodeUrl = (binCodes: string[]): string => {
  if (binCodes.length > 0) {
    let binCodeUrl = '';
    binCodes.forEach(element => {
      binCodeUrl += `binCodes=${element}&`;
    });
    return binCodeUrl.slice(0, -1);
  } else {
    return '';
  }
};

export const getLocationsByBinGroupAndBinCode = (
  location: LocationsByBinGroupAndBinCodePayload
): string => {
  return (
    getInventoryBaseUrl() +
    '/bingroups' +
    `/${location.binGroup}` +
    '/locations?' +
    (location.binCodes ? generateBinCodeUrl(location.binCodes) : ``)
  );
};

export const getLocationMappingUpdateUrl = (binGroupCode: string): string => {
  return (
    getInventoryBaseUrl() + '/bingroups/' + `${binGroupCode}` + '/locations'
  );
};

export const getItemDetailsListingUrl = (
  page: number,
  pageSize: number
): string => {
  return getProductBaseUrl() + '/items' + `?&page=${page}&size=${pageSize}`;
};

export const getItemByItemCodeUrl = (itemCode: string): string => {
  return getProductBaseUrl() + '/items' + `/${encodeURIComponent(itemCode)}`;
};

export const getTownRegionListingUrl = (
  page: number,
  pageSize: number
): string => {
  return (
    getLocationBaseUrl() +
    '/regions' +
    `?page=${page}&size=${pageSize}&regionType=REGION`
  );
};
