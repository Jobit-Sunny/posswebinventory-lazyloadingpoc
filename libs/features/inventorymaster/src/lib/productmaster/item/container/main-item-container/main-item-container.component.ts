import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ItemFacade } from '../../+state/item.facade';
import { ItemDetails } from '../../models/item.model';
import { Observable } from 'rxjs';
import { getItemMasterDashboardRouteUrl, getItemDetailsRouteUrl } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-main-item-container',
  templateUrl: './main-item-container.component.html',
  styleUrls: ['./main-item-container.component.scss']
})
export class MainItemContainerComponent implements OnInit {

  itemDetailsUrl: string;
  itemDetailsByCode$: ItemDetails;
  isLoading$: Observable<boolean>;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private itemFacade: ItemFacade
  ) { }

  ngOnInit() {
    const itemCode = decodeURIComponent(this.activatedRoute.snapshot.params['itemCode']);
    this.itemFacade.loadItemDetailsByitemCode(itemCode);
    this.isLoading$ = this.itemFacade.getisLoading();

    this.itemDetailsUrl = getItemDetailsRouteUrl(itemCode)
  }

  back() {
    this.router.navigate([getItemMasterDashboardRouteUrl()]);
  }
}
