import {
  Component,
  OnInit,
  Input,
  OnDestroy
} from '@angular/core';
import { ItemDetails } from '../../models/item.model';
import { ItemFacade } from '../../+state/item.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'poss-web-item-container',
  templateUrl: './item-container.component.html',
  styleUrls: ['./item-container.component.scss']
})
export class ItemContainerComponent implements OnInit, OnDestroy {
  @Input() itemDetailsByCode$: ItemDetails;
  itemByItemCode$: Observable<ItemDetails>;
  constructor(private itemFacade: ItemFacade) {}

  ngOnInit() {
    this.itemByItemCode$ = this.itemFacade.getitemDetailsByitemCode();
  }

  ngOnDestroy() {
    this.itemFacade.resetItemDetailsByitemCode();
  }
}
