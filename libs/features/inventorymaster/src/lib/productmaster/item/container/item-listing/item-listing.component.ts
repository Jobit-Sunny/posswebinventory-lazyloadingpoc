import {
  Component,
  OnInit,
  Input,
  ViewChild,
  AfterViewInit,
  ElementRef
} from '@angular/core';
import { Observable, Subject, fromEvent } from 'rxjs';
import { ItemDetails } from '../../models/item.model';
import { ItemFacade } from '../../+state/item.facade';
import { PageEvent } from '@angular/material';
import { takeUntil, debounceTime, take } from 'rxjs/operators';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import {
  OverlayNotificationService,
  OverlayNotificationEventType,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  ErrorEnums
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { getProductMasterDashboardRouteUrl, getItemDetailsRouteUrl } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-item-listing',
  templateUrl: './item-listing.component.html',
  styleUrls: ['./item-listing.component.scss']
})
export class ItemListingComponent implements OnInit, AfterViewInit {
  destroy$ = new Subject<null>();
  itemPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };
  itemError$: Observable<CustomErrors>;
  itemListing$: Observable<ItemDetails[]>;
  isLoading$: Observable<boolean>;
  itemCount$: Observable<number>;
  itemByItemCode$: Observable<any>;
  searchErrorCode: string;

  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;
  searchForm = new FormGroup({
    searchValue: new FormControl()
  });

  constructor(
    private appsettingFacade: AppsettingFacade,
    private itemFacade: ItemFacade,
    public router: Router,
    private translate: TranslateService,
    private overlayNotification: OverlayNotificationService
  ) { }

  ngOnInit() {
    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        const pageSize = JSON.parse(data);
        this.itemPageEvent.pageSize = pageSize;
        this.loadItemDetails();
      });

    this.isLoading$ = this.itemFacade.getisLoading();
    this.itemListing$ = this.itemFacade.getitemDetailsListing();
    this.itemCount$ = this.itemFacade.getTotalitemDetails();
    this.itemError$ = this.itemFacade.getError();
    this.searchErrorCode = ErrorEnums.ERR_PRO_002;
    this.itemFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });
  }
  ngAfterViewInit(): void {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe(event => {
        const searchValue = this.searchForm.value.searchValue;
        if (searchValue) {
          this.search(searchValue);
        } else {
          this.clearSearch();
        }
      });
  }

  loadItemDetails() {
    this.itemFacade.resetItemDetailsByitemCode();
    this.itemFacade.loadItemDetailsListing(this.itemPageEvent);
  }
  paginate(pageEvent: PageEvent) {
    this.itemPageEvent = pageEvent;
    this.loadItemDetails();
  }
  backArrow() {
    this.router.navigate([getProductMasterDashboardRouteUrl()]);
  }

  getItemCode(itemCode: string) {
    this.router.navigate([getItemDetailsRouteUrl(itemCode)]);
  }

  search(searchValue: string) {
    if (!searchValue.search('^[_A-z0-9]*((-|s)*[_A-z0-9])*$')) {
      this.itemFacade.searchItem(searchValue);
    }
  }
  clearSearch() {
    this.searchForm.reset();
    this.loadItemDetails();
  }

  errorHandler(error: CustomErrors) {
    if (error.code === this.searchErrorCode) {
      return;
    }
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasBackdrop: true,
        hasClose: true,
        error: error
      })
      .events.pipe(take(1))
      .subscribe();
  }
  omit_special_char($event: KeyboardEvent) {
    const pattern = /^[-_A-Za-z0-9]$/;
    return pattern.test($event.key);
  }
}
