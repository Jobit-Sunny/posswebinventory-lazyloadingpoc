import { Injectable } from '@angular/core';
import { ApiService } from '@poss-web/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  LoadItemListingPayload,
  LoadItemListingSuccessPayload
} from '../+state/item.actions';
import {
  getItemDetailsListingUrl,
  getItemByItemCodeUrl
} from '../../../endpoint.constants';
import { ItemAdaptor } from '../adaptors/item.adaptor';
import { ItemDetails } from '../models/item.model';

@Injectable()
export class ItemListingService {
  constructor(private apiService: ApiService) {}

  getItemDetails(
    loadItemListingPayload: LoadItemListingPayload
  ): Observable<LoadItemListingSuccessPayload> {
    const url = getItemDetailsListingUrl(
      loadItemListingPayload.pageIndex,
      loadItemListingPayload.pageSize
    );
    return this.apiService
      .get(url)
      .pipe(map(data => ItemAdaptor.getItemDetailsListing(data)));
  }

  getItemByItemCode(itemCode: string): Observable<ItemDetails> {
    const url = getItemByItemCodeUrl(itemCode);
    // console.log('Log: ItemListingService -> constructor -> url', url);
    return this.apiService
      .get(url)
      .pipe(map(data => ItemAdaptor.getItemDetailsByItemCode(data)));
  }

  getItemSearchResult(searchValue): Observable<ItemDetails[]>{
    const url = getItemByItemCodeUrl(searchValue);
    // console.log('Log: ItemListingService -> constructor -> url', url);
    return this.apiService
      .get(url)
      .pipe(map(data => ItemAdaptor.getSearchItem(data)));
  }
}
