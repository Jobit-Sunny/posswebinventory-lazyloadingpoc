import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { ItemDetails } from '../../models/item.model';
import { Router } from '@angular/router';

@Component({
  selector: 'poss-web-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {
  @Input() itemDetailsList: ItemDetails;
  @Output() itemCode = new EventEmitter<any>();

  constructor(public router: Router) {}

  ngOnInit() {}

  getItemCode(itemCode: string) {
    this.itemCode.emit(itemCode);
  }
}
