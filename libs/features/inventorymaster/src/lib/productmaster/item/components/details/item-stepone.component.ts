import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { TEMPLATE5 } from '@poss-web/shared';
import { ItemModelPartTwo } from '../../models/item-one-sub-two-details.model';
import { ItemModelPartOne } from '../../models/item-one-sub-one-details.model';
import { ItemMainModel } from '../../models/item-one-main.model';
import { Subject, Observable } from 'rxjs';
import { ItemDetails } from '../../models/item.model';
import { FormGroup } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'poss-web-item-stepone',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [disableSubmit]="true"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="['','']"
      (addForm)="addButton($event)"
      (deleteForm)="deleteButton($event)"
    >
    </poss-web-dynamic-form>
  `
})
export class ItemSteponeComponent implements OnInit, OnDestroy {
  destroy$: Subject<null> = new Subject<null>();
  @Input() itemByItemCode$: Observable<ItemDetails>;
  @Output() tabOne: EventEmitter<{
    firstForm: {
      itemCode: string;
      description: string;
      stoneWeight: string;
      indentType: string;
      isConsignable: string;
      maxWeightDeviation: string;
      inventoryType: string;
      stdWeight: string;
      productCode: string;
      brandCode: string;
      productType: string;
      materialCode: string;
      supplyChainCode: string;
      itemNature: string;
      stdPrice: string;
      stoneCharges: string;
      leadTime: string;
      hsnSacCode: string;
      purity: string;
    };
    secondForm: {
      CFAproductCode: string;
      complexityCode: string;
      pricingType: string;
      taxClass: string;
      findingCode: string;
      size: string;
      finishing: string;
      pricingGroupType: string;
      karatage: string;
      diamondKaratage: string;
      diamondClarity: string;
      diamondColour: string;
      perGram: string;
      active: string;
      saleable: string;
      returnable: string;
      indentable: string;
      interBrandAcceptable: string;
    };
  }> = new EventEmitter();

  public currentStyle: string[];
  public formFields: any;

  constructor() { }

  ngOnInit() {
    this.itemByItemCode$.pipe(takeUntil(this.destroy$)).subscribe(itemByItemCode => {
      if (itemByItemCode) {
        const form = this.prepareSet(itemByItemCode);
        this.formFields = this.getInputs(form);
        this.currentStyle = this.getCssProp();
      }
    })
  }

  prepareSet(itemDetails: ItemDetails) {
    const itemPartOne = new ItemModelPartOne(
      1,
      itemDetails.itemCode,
      itemDetails.description,
      itemDetails.stoneWeight,
      itemDetails.indentType,
      itemDetails.isConsignable,
      itemDetails.maxWeightDeviation,
      itemDetails.inventoryType,
      itemDetails.stdWeight,
      itemDetails.productCode,
      itemDetails.brandCode,
      itemDetails.productType,
      itemDetails.materialCode,
      itemDetails.supplyChainCode,
      itemDetails.itemNature,
      itemDetails.stdPrice,
      itemDetails.stoneCharges,
      itemDetails.leadTime,
      itemDetails.hsnSacCode,
      itemDetails.purity
    );

    const itemPartTwo = new ItemModelPartTwo(
      1,
      itemDetails.CFAproductCode,
      itemDetails.complexityCode,
      itemDetails.pricingType,
      itemDetails.taxClass,
      itemDetails.findingCode,
      itemDetails.size,
      itemDetails.finishing,
      itemDetails.pricingGroupType,
      itemDetails.karatage,
      itemDetails.diamondKaratage,
      itemDetails.diamondClarity,
      itemDetails.diamondColour,
      itemDetails.perGram,
      itemDetails.isActive,
      itemDetails.saleable,
      itemDetails.returnable,
      itemDetails.indentable,
      itemDetails.interBrandAcceptable
    );

    const detailsmain = new ItemMainModel(1, itemPartOne, itemPartTwo);

    return detailsmain;
  }

  getCssProp() {
    const annot = (ItemSteponeComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form: any) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }

  public setFormConfig() {
    return {
      formName: 'Item Master Form',
      formDesc: 'Item Master',
      formTemplate: TEMPLATE5
    };
  }

  public addButton() { }

  public deleteButton() { }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
