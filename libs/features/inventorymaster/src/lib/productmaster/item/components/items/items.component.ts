import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { PageEvent } from '@angular/material';
import { Subject } from 'rxjs';
import { AppsettingFacade } from '@poss-web/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'poss-web-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit, OnDestroy {
  @Input() itemByItemCode;
  @Input() itemDetailsList;
  @Input() count: number;
  @Input() pageEvent: PageEvent;
  @Output() itemCode = new EventEmitter<any>();
  @Output() paginator = new EventEmitter<PageEvent>();
  destroy$ = new Subject<null>();
  pageSizeOptions: number[] = [];
  minPageSize = 0;

  constructor(private appSettingFacade: AppsettingFacade) {}

  ngOnInit() {
    this.appSettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.pageSizeOptions = data;
        this.minPageSize = this.pageSizeOptions.reduce((a: number, b: number) =>
          a < b ? a : b
        );
      });
  }

  emitItemCode(itemCode) {
    this.itemCode.emit(itemCode);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
