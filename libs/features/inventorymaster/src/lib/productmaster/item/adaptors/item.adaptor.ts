import {
  LoadItemListingSuccessPayload,
  LoadItemByItemCodeSuccess
} from '../+state/item.actions';
import { ItemDetails } from '../models/item.model';
import { description } from '../../../locationSetup/models/locationsetup.constants';

export class ItemAdaptor {
  static itelDetailsListing: LoadItemListingSuccessPayload;

  static getItemDetailsListing(data: any): LoadItemListingSuccessPayload {
    const itemDetailsListing: ItemDetails[] = [];
    for (const listItem of data.results) {
      itemDetailsListing.push({
        itemCode: listItem.itemCode,
        description: listItem.description,
        isActive: listItem.isActive,
        stoneWeight: listItem.stoneWeight,
        indentType: listItem.indentTypeCode,
        isConsignable: listItem.isConsignable,
        maxWeightDeviation: listItem.maxWeightDeviation,
        inventoryType: listItem.inventoryType,
        stdWeight: listItem.stdWeight,
        productCode: listItem.productCategoryCode,
        brandCode: listItem.brandCode,
        productType: listItem.productGroupCode,
        materialCode: listItem.materialCode,
        supplyChainCode: listItem.supplyChainCode,
        itemNature: listItem.itemNature,
        stdPrice: listItem.stdPrice,
        stoneCharges: listItem.stoneCharges,
        leadTime: listItem.leadTime,
        hsnSacCode: listItem.hsnSacCode,
        purity: listItem.purity,
        CFAproductCode: listItem.CFAproductCode,
        complexityCode: listItem.complexityCode,
        pricingType: listItem.pricingType,
        taxClass: listItem.taxClass,
        findingCode: listItem.findingCode,
        size: listItem.size,
        finishing: listItem.finishing,
        pricingGroupType: listItem.pricingGroupType,
        karatage: listItem.karatage,
        diamondKaratage: listItem.diamondKaratage,
        diamondClarity: listItem.diamondClarity,
        diamondColour: listItem.diamondColour,
        perGram: listItem.perGram,
        saleable: listItem.saleable,
        returnable: listItem.returnable,
        indentable: listItem.indentable,
        interBrandAcceptable: listItem.interBrandAcceptable
      });
    }

    this.itelDetailsListing = {
      itemListing: itemDetailsListing,
      totalElements: data.totalElements
    };
    return this.itelDetailsListing;
  }

  static getItemDetailsByItemCode(data: any): ItemDetails {
    const itemByItemCode: ItemDetails = {
      itemCode: data.itemCode ? data.itemCode : '',
      description: data.description ? data.description : '',
      isActive: +data.isActive ? 'Yes' : 'No',
      stoneWeight: data.itemDetails.stoneWeight
        ? data.itemDetails.stoneWeight
        : '',
      indentType: data.indentTypeCode ? data.indentTypeCode : '',
      isConsignable: +data.configDetails.consignmentFlag ? 'Yes' : 'No',
      maxWeightDeviation: data.itemDetails.maxWeightDeviation
        ? data.itemDetails.maxWeightDeviation
        : '',
      inventoryType: data.itemDetails.inventoryType
        ? data.itemDetails.inventoryType
        : '',
      stdWeight: data.stdWeight ? data.stdWeight : '',
      productCode: data.productCategoryCode ? data.productCategoryCode : '',
      brandCode: data.brandCode ? data.brandCode : '',
      productType: data.itemDetails.productType
        ? data.itemDetails.productType
        : '',
      materialCode: data.materialCode ? data.materialCode : '',
      supplyChainCode: data.itemDetails.supplyChainCode
        ? data.itemDetails.supplyChainCode
        : '',
      itemNature: data.itemDetails.itemNature
        ? data.itemDetails.itemNature
        : '',
      stdPrice: data.stdValue ? data.stdValue : '',
      stoneCharges: data.itemDetails.stoneCharges
        ? data.itemDetails.stoneCharges
        : '',
      leadTime: data.leadTime ? data.leadTime : '',
      hsnSacCode: data.itemDetails.hsnSacCode
        ? data.itemDetails.hsnSacCode
        : '',
      purity: data.itemDetails.purity ? data.itemDetails.purity : '',
      CFAproductCode: data.productGroupCode ? data.productGroupCode : '',
      complexityCode: data.complexityCode ? data.complexityCode : '',
      pricingType: data.itemDetails.pricingType
        ? data.itemDetails.pricingType
        : '',
      taxClass: data.itemDetails.taxClass ? data.itemDetails.taxClass : '',
      findingCode: data.itemDetails.findingCode
        ? data.itemDetails.findingCode
        : '',
      size: data.itemDetails.size ? data.itemDetails.size : '',
      finishing: data.itemDetails.finishing ? data.itemDetails.finishing : '',
      pricingGroupType: data.pricingGroupType ? data.pricingGroupType : '',
      karatage: data.itemDetails.karatage ? data.itemDetails.karatage : '',
      diamondKaratage: data.itemDetails.diamondCaratage
        ? data.itemDetails.diamondCaratage
        : '',
      diamondClarity: data.itemDetails.diamondClarity
        ? data.itemDetails.diamondClarity
        : '',
      diamondColour: data.itemDetails.diamondColor
        ? data.itemDetails.diamondColor
        : '',
      perGram: +data.configDetails.isPerGram ? 'Yes'
        : 'No',
      saleable: +data.configDetails.isSaleable
        ? 'Yes'
        : 'No',
      returnable: +data.configDetails.isReturnable
        ? 'Yes'
        : 'No',
      indentable: +data.configDetails.isForIndent
        ? 'Yes'
        : 'No',
      interBrandAcceptable: +data.configDetails.isInterBrandAcceptance ? 'Yes' : 'No'
    };
    return itemByItemCode;
  }

  static getSearchItem(data: any): ItemDetails[] {
    const Item: ItemDetails[] = [];
    Item.push({
      itemCode: data.itemCode,
      description: data.description,
      isActive: data.isActive,
      stoneWeight: data.stoneWeight,
      indentType: data.indentTypeCode,
      isConsignable: data.isConsignable,
      maxWeightDeviation: data.maxWeightDeviation,
      inventoryType: data.inventoryType,
      stdWeight: data.stdWeight,
      productCode: data.productCategoryCode,
      brandCode: data.brandCode,
      productType: data.productGroupCode,
      materialCode: data.materialCode,
      supplyChainCode: data.supplyChainCode,
      itemNature: data.itemNature,
      stdPrice: data.stdPrice,
      stoneCharges: data.stoneCharges,
      leadTime: data.leadTime,
      hsnSacCode: data.hsnSacCode,
      purity: data.purity,
      CFAproductCode: data.CFAproductCode,
      complexityCode: data.complexityCode,
      pricingType: data.pricingType,
      taxClass: data.taxClass,
      findingCode: data.findingCode,
      size: data.size,
      finishing: data.finishing,
      pricingGroupType: data.pricingGroupType,
      karatage: data.karatage,
      diamondKaratage: data.diamondKaratage,
      diamondClarity: data.diamondClarity,
      diamondColour: data.diamondColour,
      perGram: data.perGram,
      saleable: data.saleable,
      returnable: data.returnable,
      indentable: data.indentable,
      interBrandAcceptable: data.interBrandAcceptable
    });
    return Item;
  }
}
