import { Validators } from '@angular/forms';
import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';

export class ItemModelPartOne extends DynamicFormFieldsBuilder {
  private id: number;


  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterCode'
  })

  private itemCode: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterDescription'
  })

  private description: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterStoneWeight'
  })

  private stoneWeight: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterIndentType'
  })

  private indentType: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterIsConsignment'
  })

  private isConsignable: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterWeightDeviation'
  })

  private maxWeightDeviation: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterInventoryType'
  })

  private inventoryType: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterStdWeight'
  })

  private stdWeight: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterProductCode'
  })

  private productCode: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterBrandCode'
  })

  private brandCode: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterProductType'
  })

  private productType: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterMaterialCode'
  })

  private materialCode: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterSupplyChainCode'
  })

  private supplyChainCode: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterItemNature'
  })

  private itemNature: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterStdPrice'
  })

  private stdPrice: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterStoneCharges'
  })

  private stoneCharges: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterLeadTime'
  })

  private leadTime: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterHSNSACCode'
  })

  private hsnSacCode: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterPurity'
  })
  private purity: string;
  constructor(
    id: number,
    itemCode: string,
    description: string,
    stoneWeight: string,
    indentType: string,
    isConsignable: string,
    maxWeightDeviation: string,
    inventoryType: string,
    stdWeight: string,
    productCode: string,
    brandCode: string,
    productType: string,
    materialCode: string,
    supplyChainCode: string,
    itemNature: string,
    stdPrice: string,
    stoneCharges: string,
    leadTime: string,
    hsnSacCode: string,
    purity: string
  ) {
    super();
    this.id = id;
    this.itemCode = itemCode;
    this.description = description;
    this.stoneWeight = stoneWeight;
    this.indentType = indentType;
    this.isConsignable = isConsignable;
    this.maxWeightDeviation = maxWeightDeviation;
    this.inventoryType = inventoryType;
    this.stdWeight = stdWeight;
    this.productCode = productCode;
    this.brandCode = brandCode;
    this.productType = productType;
    this.materialCode = materialCode;
    this.supplyChainCode = supplyChainCode;
    this.itemNature = itemNature;
    this.stdPrice = stdPrice;
    this.stoneCharges = stoneCharges;
    this.leadTime = leadTime;
    this.hsnSacCode = hsnSacCode;
    this.purity = purity;
  }
}
