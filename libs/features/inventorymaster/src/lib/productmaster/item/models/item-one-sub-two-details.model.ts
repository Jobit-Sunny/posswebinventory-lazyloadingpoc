import { Validators } from '@angular/forms';
import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';

export class ItemModelPartTwo extends DynamicFormFieldsBuilder {
  private id: number;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterCFAproductCode'
  })

  private CFAproductCode: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterComplexityCode'
  })

  private complexityCode: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterPricingType'
  })

  private pricingType: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterTaxClass'
  })

  private taxClass: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterFindingCode'
  })

  private findingCode: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterSize'
  })

  private size: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterFinishing'
  })

  private finishing: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterPricingGroupType'
  })

  private pricingGroupType: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterKaratage'
  })

  private karatage: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterDiamondKaratage'
  })

  private diamondKaratage: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterDiamondClarity'
  })

  private diamondClarity: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterDiamondColour'
  })

  private diamondColour: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterPerGram'
  })

  private perGram: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterActive'
  })

  private active: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterSaleable'
  })

  private saleable: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterReturnable'
  })

  private returnable: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterIndentable'
  })

  private indentable: string;

  @FormField({
    fieldType: FormFieldType.TEXT_LABEL,
    label: 'pw.itemMaster.itemMasterInterBrandAcceptable'
  })

  private interBrandAcceptable: string;

  constructor(
    id: number,
    CFAproductCode: string,
    complexityCode: string,
    pricingType: string,
    taxClass: string,
    findingCode: string,
    size: string,
    finishing: string,
    pricingGroupType: string,
    karatage: string,
    diamondKaratage: string,
    diamondClarity: string,
    diamondColour: string,
    perGram: string,
    active: string,
    saleable: string,
    returnable: string,
    indentable: string,
    interBrandAcceptable: string
  ) {
    super();
    this.id = id;
    this.CFAproductCode = CFAproductCode;
    this.complexityCode = complexityCode;
    this.pricingType = pricingType;
    this.taxClass = taxClass;
    this.findingCode = findingCode;
    this.size = size;
    this.finishing = finishing;
    this.pricingGroupType = pricingGroupType;
    this.karatage = karatage;
    this.diamondKaratage = diamondKaratage;
    this.diamondClarity = diamondClarity;
    this.diamondColour = diamondColour;
    this.perGram = perGram;
    this.active = active;
    this.saleable = saleable;
    this.returnable = returnable;
    this.indentable = indentable;
    this.interBrandAcceptable = interBrandAcceptable;
  }
}
