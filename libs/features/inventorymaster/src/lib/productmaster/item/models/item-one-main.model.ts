import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType
} from '@poss-web/shared';

import { ItemModelPartOne } from './item-one-sub-one-details.model';
import { ItemModelPartTwo } from './item-one-sub-two-details.model';

export class ItemMainModel extends DynamicFormFieldsBuilder {
  private id: number;

  @FormField({
    fieldType: FormFieldType.SUB_FORM,
    label: 'pw.itemMaster.itemMasterSubFormLabel',
    hide: false
  })
  private itemPartOne: ItemModelPartOne;

  @FormField({
    fieldType: FormFieldType.SUB_FORM,
    label: 'pw.itemMaster.itemMasterSubFormLabel',
    hide: false
  })
  private itemPartTwo: ItemModelPartTwo;

  constructor(
    id: number,
    itemPartOne: ItemModelPartOne,
    itemPartTwo: ItemModelPartTwo
  ) {
    super();
    this.id = id;
    this.itemPartOne = itemPartOne;
    this.itemPartTwo = itemPartTwo;
  }
}
