export interface ItemDetails {
  itemCode: string;
  description: string;
  isActive: string;
  stoneWeight: string;
  indentType: string;
  isConsignable: string;
  maxWeightDeviation: string;
  inventoryType: string;
  stdWeight: string;
  productCode: string;
  brandCode: string;
  productType: string;
  materialCode: string;
  supplyChainCode: string;
  itemNature: string;
  stdPrice: string;
  stoneCharges: string;
  leadTime: string;
  hsnSacCode: string;
  purity: string;
  CFAproductCode: string;
  complexityCode: string;
  pricingType: string;
  taxClass: string;
  findingCode: string;
  size: string;
  finishing: string;
  pricingGroupType: string;
  karatage: string;
  diamondKaratage: string;
  diamondClarity: string;
  diamondColour: string;
  perGram: string;
  saleable: string;
  returnable: string;
  indentable: string;
  interBrandAcceptable: string;
}
// export class ItemdataModel {
//   issueData: ItemdataModel[] = [];
//   totalElements: number;
// }
