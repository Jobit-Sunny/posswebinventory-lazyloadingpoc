import { CustomErrors } from '@poss-web/core';
import { ItemDetails } from '../models/item.model';

export interface ItemListingState {
  error: CustomErrors;
  itemListing: ItemDetails[];
  itemDetails: ItemDetails;
  totalItemDetails: number;
  isLoading: boolean;
}
