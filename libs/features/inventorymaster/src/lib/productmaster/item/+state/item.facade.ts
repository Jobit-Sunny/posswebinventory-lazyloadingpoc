import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import * as itemActions from './item.actions';
import { State } from '../../../inventorymasters.state';
import { ItemSelectors } from './item.selectors';
import { Observable } from 'rxjs';
import { ItemDetails } from '../models/item.model';

@Injectable()
export class ItemFacade {
  constructor(private store: Store<State>) {}

  private itemDetailsListing$ = this.store.select(
    ItemSelectors.selectItemDetailsListing
  );

  private itemDetailsByitemCode$ = this.store.select(
    ItemSelectors.selectItemDetailsByItemCode
  );

  private isLoading$ = this.store.select(ItemSelectors.selectIsLoading);

  private totalItemDetails$ = this.store.select(
    ItemSelectors.selectTotalItemDetailsCount
  );

  private itemError$ = this.store.select(ItemSelectors.selectError);

  getisLoading() {
    return this.isLoading$;
  }

  getTotalitemDetails() {
    return this.totalItemDetails$;
  }

  getitemDetailsListing() {
    return this.itemDetailsListing$;
  }

  getitemDetailsByitemCode() {
    return this.itemDetailsByitemCode$;
  }

  getError() {
    return this.itemError$;
  }

  loadItemDetailsByitemCode(item: string) {
    this.store.dispatch(new itemActions.LoadItemByItemCode(item));
  }

  loadItemDetailsListing(
    loaditemDetailsListingPayload: itemActions.LoadItemListingPayload
  ) {
    this.store.dispatch(
      new itemActions.LoadItemDetails(loaditemDetailsListingPayload)
    );
  }

  resetItemDetailsByitemCode() {
    this.store.dispatch(new itemActions.ResetItemByItemCodeFailure());
  }

  searchItem(itemCode: string) {
    this.store.dispatch(
      new itemActions.SearchItem(itemCode)
    );
  }
}
