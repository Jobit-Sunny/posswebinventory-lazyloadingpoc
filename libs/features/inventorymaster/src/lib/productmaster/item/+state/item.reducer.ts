import { ItemListingState } from './item.state';
import { ItemActions, ItemActionTypes } from './item.actions';

const initialState: ItemListingState = {
  itemListing: null,
  itemDetails: null,
  totalItemDetails: 0,
  isLoading: false,
  error: null
};
export function ItemReducer(
  state: ItemListingState = initialState,
  action: ItemActions
): ItemListingState {
  switch (action.type) {
    case ItemActionTypes.LOAD_ITEM_DETAILS:
    case ItemActionTypes.LOAD_ITEM_DETAILS_BY_ITEM_CODE:
      return {
        ...state,
        isLoading: true,
        error: null
      };

    case ItemActionTypes.LOAD_ITEM_DETAILS_SUCCESS:
      return {
        ...state,
        itemListing: action.payload.itemListing,
        totalItemDetails: action.payload.totalElements,
        isLoading: false
      };

    case ItemActionTypes.LOAD_ITEM_DETAILS_FAILURE:
    case ItemActionTypes.LOAD_ITEM_DETAILS_BY_ITEM_CODE_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoading: false
      };

    // case ItemActionTypes.LOAD_ITEM_DETAILS_BY_ITEM_CODE:
    //   return {
    //     ...state,
    //     isLoading: true,
    //     error: null
    //   };

    case ItemActionTypes.LOAD_ITEM_DETAILS_BY_ITEM_CODE_SUCCESS:
      return {
        ...state,
        itemDetails: action.payload,
        isLoading: false
      };

    // case ItemActionTypes.LOAD_ITEM_DETAILS_BY_ITEM_CODE_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload,
    //     isLoading: false
    //   };

    case ItemActionTypes.RESET_ITEM_DETAILS_BY_ITEM_CODE:
      return {
        ...state,
        itemDetails: null
      };

    case ItemActionTypes.SEARCH_ITEM:
      return {
        ...state,
        isLoading: true
      };
    case ItemActionTypes.SEARCH_ITEM_SUCCESS:
      return {
        ...state,
        itemListing: action.payload,
        totalItemDetails: 1,
        isLoading: false
      };
    case ItemActionTypes.SEARCH_ITEM_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
        itemListing: null,
        totalItemDetails: null
      };

    default:
      return state;
  }
}
