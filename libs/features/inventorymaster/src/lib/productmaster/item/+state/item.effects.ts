import { DataPersistence } from '@nrwl/angular';
import {
  NotificationService,
  CustomErrors,
  CustomErrorAdaptor
} from '@poss-web/core';
import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';

import * as ItemActions from './item.actions';
import { ItemListingService } from '../services/item-listing.service';
import { ItemDetails } from '../models/item.model';

@Injectable()
export class ItemEffect {
  constructor(
    private dataPersistence: DataPersistence<any>,
    private notificationService: NotificationService,
    private itemListingService: ItemListingService
  ) {}

  @Effect()
  loadItemDetails$ = this.dataPersistence.fetch(
    ItemActions.ItemActionTypes.LOAD_ITEM_DETAILS,
    {
      run: (action: ItemActions.LoadItemDetails) => {

        return this.itemListingService
          .getItemDetails(action.payload)
          .pipe(
            map(
              (itemDetails: ItemActions.LoadItemListingSuccessPayload) =>
                new ItemActions.LoadItemDetailsSuccess(itemDetails)
            )
          );
      },
      onError: (
        action: ItemActions.LoadItemDetails,
        error: HttpErrorResponse
      ) => {
        return new ItemActions.LoadItemDetailsFailure(this.errorHandler(error));
      }
    }
  );

  @Effect()
  loadItemDetailsByItemCode$ = this.dataPersistence.fetch(
    ItemActions.ItemActionTypes.LOAD_ITEM_DETAILS_BY_ITEM_CODE,
    {
      run: (action: ItemActions.LoadItemByItemCode) => {
        return this.itemListingService
          .getItemByItemCode(action.payload)
          .pipe(
            map(
              (ItemDetailsByItemCode: ItemDetails) =>
                new ItemActions.LoadItemByItemCodeSuccess(ItemDetailsByItemCode)
            )
          );
      },
      onError: (
        action: ItemActions.LoadItemByItemCode,
        error: HttpErrorResponse
      ) => {
        return new ItemActions.LoadItemByItemCodeFailure(
          this.errorHandler(error)
        );
      }
    }

  );
  @Effect()
  searchItem$ = this.dataPersistence.fetch(
    ItemActions.ItemActionTypes.SEARCH_ITEM,
    {
      run: (action: ItemActions.SearchItem) => {
        return this.itemListingService.getItemSearchResult(
          action.payload
        ).pipe(
          map(
            (SearchResult: ItemDetails[]) =>
              new ItemActions.SearchItemSuccess(SearchResult)
          )
        );
      },
      onError: (
        action: ItemActions.SearchItem,
        error: HttpErrorResponse
      ) => {
        return new ItemActions.SearchItemFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }


}
