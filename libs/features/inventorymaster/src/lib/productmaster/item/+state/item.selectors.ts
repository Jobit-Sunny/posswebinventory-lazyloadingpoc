import { createSelector } from '@ngrx/store';
import { selectInventoryMasters } from '../../../inventorymasters.state';





const selectItemDetailsListing = createSelector(
    selectInventoryMasters,
    state => state.itemListingState.itemListing
);

const selectTotalItemDetailsCount = createSelector(
    selectInventoryMasters,
    state => state.itemListingState.totalItemDetails
);


const selectItemDetailsByItemCode = createSelector(
    selectInventoryMasters,
    state => state.itemListingState.itemDetails
);

const selectIsLoading = createSelector(
    selectInventoryMasters,
    state => state.itemListingState.isLoading
);


const selectError = createSelector(
    selectInventoryMasters,
    state => state.itemListingState.error
);




export const ItemSelectors = {
    selectItemDetailsListing,
    selectItemDetailsByItemCode,
    selectIsLoading,
    selectError,
    selectTotalItemDetailsCount
};
