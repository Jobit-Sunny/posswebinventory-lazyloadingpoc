import { ItemDetails } from '../models/item.model';
import { CustomErrors } from '@poss-web/core';
import { Action } from '@ngrx/store';

export interface LoadItemListingPayload {
  pageIndex: number;
  pageSize: number;
}

export interface LoadItemListingSuccessPayload {
  itemListing: ItemDetails[];
  totalElements: number;
}

export enum ItemActionTypes {
  LOAD_ITEM_DETAILS = '[Load-Item-Details] Load Item Details',
  LOAD_ITEM_DETAILS_SUCCESS = '[Load-Item-Details] Load Item Details Success',
  LOAD_ITEM_DETAILS_FAILURE = '[Load-Item-Details] Load Item Details Failure',

  LOAD_ITEM_DETAILS_BY_ITEM_CODE = '[Load-Item-Details] Load Item Details By Item Code',
  LOAD_ITEM_DETAILS_BY_ITEM_CODE_SUCCESS = '[Load-Item-Details] Load Item Details By Item Code Success',
  LOAD_ITEM_DETAILS_BY_ITEM_CODE_FAILURE = '[Load-Item-Details] Load Item Details By Item Code Failure',

  RESET_ITEM_DETAILS_BY_ITEM_CODE = '[Load-Item-Details] Reset Item Details By Item Code',

  SEARCH_ITEM = '[Item] Search Item',
  SEARCH_ITEM_SUCCESS = '[Item] Search Item Success',
  SEARCH_ITEM_FAILURE = '[Item] Search Item Failure',
}

export class LoadItemDetails implements Action {
  readonly type = ItemActionTypes.LOAD_ITEM_DETAILS;
  constructor(public payload: LoadItemListingPayload) {}
}
export class LoadItemDetailsSuccess implements Action {
  readonly type = ItemActionTypes.LOAD_ITEM_DETAILS_SUCCESS;
  constructor(public payload: LoadItemListingSuccessPayload) {}
}
export class LoadItemDetailsFailure implements Action {
  readonly type = ItemActionTypes.LOAD_ITEM_DETAILS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadItemByItemCode implements Action {
  readonly type = ItemActionTypes.LOAD_ITEM_DETAILS_BY_ITEM_CODE;
  constructor(public payload: string) {}
}
export class LoadItemByItemCodeSuccess implements Action {
  readonly type = ItemActionTypes.LOAD_ITEM_DETAILS_BY_ITEM_CODE_SUCCESS;
  constructor(public payload: ItemDetails) {}
}
export class LoadItemByItemCodeFailure implements Action {
  readonly type = ItemActionTypes.LOAD_ITEM_DETAILS_BY_ITEM_CODE_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class ResetItemByItemCodeFailure implements Action {
  readonly type = ItemActionTypes.RESET_ITEM_DETAILS_BY_ITEM_CODE;
  constructor() {}
}

export class SearchItem implements Action {
  readonly type = ItemActionTypes.SEARCH_ITEM;
  constructor(public payload: string) {}
}
export class SearchItemSuccess implements Action {
  readonly type = ItemActionTypes.SEARCH_ITEM_SUCCESS;
  constructor(public payload: ItemDetails[]) {}
}
export class SearchItemFailure implements Action {
  readonly type = ItemActionTypes.SEARCH_ITEM_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export type ItemActions =
  | LoadItemDetails
  | LoadItemDetailsSuccess
  | LoadItemDetailsFailure
  | LoadItemByItemCode
  | LoadItemByItemCodeSuccess
  | LoadItemByItemCodeFailure
  | ResetItemByItemCodeFailure
  | SearchItem
  | SearchItemSuccess
  | SearchItemFailure;
