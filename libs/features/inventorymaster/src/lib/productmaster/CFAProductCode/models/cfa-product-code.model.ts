export interface CFAProducts {
  productGroupCode: string;
  productType: string;
  description: string;
  materialCode: string;
  orgCode: string;
  configDetails: any;
}
export interface ProductType {
  id: string;
  name: string;
  isActive: boolean;
}
export interface MaterialType {
  id: string;
  name: string;
}
