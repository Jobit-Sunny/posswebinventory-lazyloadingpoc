import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { Subject, Observable, combineLatest } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { CFAProductCodeDetails } from '../../../../master/models/cfa-product-code-details.model';
import { CFAProductCodeCheckItOuts } from '../../../../master/models/cfa-product-code-checkitout.model';
import { TEMPLATE7, HelperFunctions } from '@poss-web/shared';
import { CFAProductCodeMainModel } from '../../../../master/models/cfa-product-code-main.model';
import {
  CFAProducts,
  ProductType,
  MaterialType
} from '../../models/cfa-product-code.model';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../../master/common/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'poss-web-cfa-productcode-stepone',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="[
        'pw.CFAProduct.cancelButtonText',
        'pw.CFAProduct.saveButtonText'
      ]"
      (addForm)="addButton($event)"
      (deleteForm)="deleteButton($event)"
      (formGroupCreated)="formGroupCreated($event)"
    >
    </poss-web-dynamic-form>
  `
})
export class CFAProductCodeSteponeComponent implements OnInit, OnDestroy {
  @Input() CFAProduct$: Observable<CFAProducts>;
  @Input() productTypes$: Observable<ProductType[]>;
  @Input() materialTypes$: Observable<MaterialType[]>;
  CFAProdcut: CFAProducts;
  CFAProductCode: string;

  @Output() CFAProductDetails: EventEmitter<{
    description: string;
    materialCode: string;
    productType: string;
    orgCode: string;
    productGroupCode: string;
    configDetails: {
      productCategoryFlag: string;
      isActive: boolean;
      isMia: boolean;
      goldPriceMandatory: boolean;
      platinumPriceMandatory: boolean;
      makingChargesMandatory: boolean;
      stoneChargesMandatory: boolean;
      isEligibleForLoyalityPoints: boolean;
      printGuarnteeCard: boolean;
      eligibleForBillLevelDiscount: boolean;
      isPlainGold: boolean;
      isGRFStuddedCategory: boolean;
      isLoyalityPointsRedeemable: boolean;
      isPlainSilver: boolean;
      isPlainPlatinum: boolean;
      isFOCApplicable: boolean;
      isBiMetal: boolean;
      isMiaUCP: boolean;
      isPlainStudPricing: boolean;
      isJewelleryItem: boolean;
      quickSilverReedemptionAllowed: boolean;
      whetherItIsStuddedOrNot: boolean;
      TEPIssueOutCategory: boolean;
      DynamicF1CalculationYesOrNo: boolean;
    };
  }> = new EventEmitter();
  destroy$: Subject<null> = new Subject<null>();
  constructor(private hf: HelperFunctions, public dialog: MatDialog) {}
  public currentStyle: string[];
  public formFields: any;
  ngOnInit() {
    combineLatest(
      this.CFAProduct$,
      this.productTypes$,
      this.materialTypes$
    ).subscribe(results => {
      const form = this.prepareSet(results[0], results[1], results[2]);
      this.formFields = this.getInputs(form);
      this.currentStyle = this.getCssProp();
    });
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
  getCssProp() {
    const annot = (CFAProductCodeSteponeComponent as any).__annotations__;
    return annot[0].styles;
  }
  prepareSet(
    CFAProdcut: CFAProducts,
    productTypes: ProductType[],
    materialTypes: MaterialType[]
  ) {
    this.CFAProductCode = CFAProdcut
      ? CFAProdcut.productGroupCode
        ? CFAProdcut.productGroupCode
        : ''
      : '';
    if (CFAProdcut) {
      productTypes = this.hf.patchValue(
        productTypes,
        'id',
        'selected',
        CFAProdcut.productType,
        true
      );
    }
    if (CFAProdcut) {
      materialTypes = this.hf.patchValue(
        materialTypes,
        'id',
        'selected',
        CFAProdcut.materialCode,
        true
      );
    }

    const CFAProductCode = new CFAProductCodeDetails(
      1,
      CFAProdcut
        ? CFAProdcut.productGroupCode
          ? CFAProdcut.productGroupCode
          : ''
        : '',
      [
        {
          id: '1',
          name: 'pw.CFAProduct.isActive',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.isActive
              ? CFAProdcut.configDetails.isActive
              : false
            : false
        }
      ],
      CFAProdcut ? (CFAProdcut.description ? CFAProdcut.description : '') : '',
      [
        {
          id: '2',
          name: 'pw.CFAProduct.isMia',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.isMia
              ? CFAProdcut.configDetails.isMia
              : false
            : false
        }
      ],
      productTypes,
      materialTypes,
      [
        {
          id: '3',
          name: 'pw.CFAProduct.goldPriceMandatory',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.goldPriceMandatory
              ? CFAProdcut.configDetails.goldPriceMandatory
              : false
            : false
        }
      ],
      [
        {
          id: '4',
          name: 'pw.CFAProduct.platinumPriceMandatory',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.platinumPriceMandatory
              ? CFAProdcut.configDetails.platinumPriceMandatory
              : false
            : false
        }
      ],
      [
        {
          id: '5',
          name: 'pw.CFAProduct.makingChargesMandatory',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.makingChargesMandatory
              ? CFAProdcut.configDetails.makingChargesMandatory
              : false
            : false
        }
      ]
    );
    const CFAProductCodeCheckItOut = new CFAProductCodeCheckItOuts(
      1,
      [
        {
          id: '6',
          name: 'pw.CFAProduct.stoneChargesMandatory',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.stoneChargesMandatory
              ? CFAProdcut.configDetails.stoneChargesMandatory
              : false
            : false
        },
        {
          id: '7',
          name: 'pw.CFAProduct.isEligibleForLoyalityPoints',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.isEligibleForLoyalityPoints
              ? CFAProdcut.configDetails.isEligibleForLoyalityPoints
              : false
            : false
        },
        {
          id: '8',
          name: 'pw.CFAProduct.printGuarnteeCard',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.printGuarnteeCard
              ? CFAProdcut.configDetails.printGuarnteeCard
              : false
            : false
        },
        {
          id: '9',
          name: 'pw.CFAProduct.eligibleForBillLevelDiscount',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.eligibleForBillLevelDiscount
              ? CFAProdcut.configDetails.eligibleForBillLevelDiscount
              : false
            : false
        },
        {
          id: '10',
          name: 'pw.CFAProduct.isPlainGold',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.isPlainGold
              ? CFAProdcut.configDetails.isPlainGold
              : false
            : false
        },
        {
          id: '11',
          name: 'pw.CFAProduct.isGRFStuddedCategory',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.isGRFStuddedCategory
              ? CFAProdcut.configDetails.isGRFStuddedCategory
              : false
            : false
        },
        {
          id: '12',
          name: 'pw.CFAProduct.isLoyalityPointsRedeemable',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.isLoyalityPointsRedeemable
              ? CFAProdcut.configDetails.isLoyalityPointsRedeemable
              : false
            : false
        },
        {
          id: '13',
          name: 'pw.CFAProduct.isPlainSilver',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.isPlainSilver
              ? CFAProdcut.configDetails.isPlainSilver
              : false
            : false
        },
        {
          id: '14',
          name: 'pw.CFAProduct.isPlainPlatinum',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.isPlainPlatinum
              ? CFAProdcut.configDetails.isPlainPlatinum
              : false
            : false
        },
        {
          id: '15',
          name: 'pw.CFAProduct.isFOCApplicable',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.isFOCApplicable
              ? CFAProdcut.configDetails.isFOCApplicable
              : false
            : false
        },
        {
          id: '16',
          name: 'pw.CFAProduct.isBiMetal',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.isBiMetal
              ? CFAProdcut.configDetails.isBiMetal
              : false
            : false
        },
        {
          id: '17',
          name: 'pw.CFAProduct.isMiaUCP',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.isMiaUCP
              ? CFAProdcut.configDetails.isMiaUCP
              : false
            : false
        },
        {
          id: '18',
          name: 'pw.CFAProduct.isPlainStudPricing',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.isPlainStudPricing
              ? CFAProdcut.configDetails.isPlainStudPricing
              : false
            : false
        },
        {
          id: '19',
          name: 'pw.CFAProduct.isJewelleryItem',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.isJewelleryItem
              ? CFAProdcut.configDetails.isJewelleryItem
              : false
            : false
        },
        {
          id: '20',
          name: 'pw.CFAProduct.quickSilverRedemptionAllowed',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.quickSilverReedemptionAllowed
              ? CFAProdcut.configDetails.quickSilverReedemptionAllowed
              : false
            : false
        },
        {
          id: '21',
          name: 'pw.CFAProduct.whetherItIsStuddedOrNot',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.whetherItIsStuddedOrNot
              ? CFAProdcut.configDetails.whetherItIsStuddedOrNot
              : false
            : false
        },
        {
          id: '22',
          name: 'pw.CFAProduct.TEPIssueOutCategory',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.TEPIssueOutCategory
              ? CFAProdcut.configDetails.TEPIssueOutCategory
              : false
            : false
        }
      ],
      [
        {
          id: 'yes',
          name: 'Yes',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.DynamicF1CalculationYesOrNo === 'yes'
              ? true
              : false
            : false
        },
        {
          id: 'no',
          name: 'NO',
          checked: CFAProdcut
            ? CFAProdcut.configDetails.DynamicF1CalculationYesOrNo === 'no'
              ? true
              : false
            : false
        }
      ]
    );
    const CFAProductCodeMain = new CFAProductCodeMainModel(
      1,
      CFAProductCode,
      CFAProductCodeCheckItOut
    );
    return CFAProductCodeMain;
  }
  public getInputs(form) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }
  public setFormConfig() {
    return {
      formName: 'Location Master Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE7
    };
  }
  addButton(formGroup: FormGroup) {
    this.CFAProductDetails.emit({
      configDetails: {
        productCategoryFlag:
          formGroup.value['1-CFAProductCodeDetails']['1-productCategoryFlag'],
        isActive: formGroup.value['1-CFAProductCodeDetails']['1-isActive'][0],
        isMia: formGroup.value['1-CFAProductCodeDetails']['1-isMia'][0],
        goldPriceMandatory:
          formGroup.value['1-CFAProductCodeDetails']['1-goldPriceMandatory'][0],
        platinumPriceMandatory:
          formGroup.value['1-CFAProductCodeDetails'][
            '1-platinumPriceMandatory'
          ][0],
        makingChargesMandatory:
          formGroup.value['1-CFAProductCodeDetails'][
            '1-makingChargesMandatory'
          ][0],
        stoneChargesMandatory:
          formGroup.value['1-CFAProductCodeCheckItOuts']['1-checkBoxes'][0],
        isEligibleForLoyalityPoints:
          formGroup.value['1-CFAProductCodeCheckItOuts']['1-checkBoxes'][1],
        printGuarnteeCard:
          formGroup.value['1-CFAProductCodeCheckItOuts']['1-checkBoxes'][2],
        eligibleForBillLevelDiscount:
          formGroup.value['1-CFAProductCodeCheckItOuts']['1-checkBoxes'][3],
        isPlainGold:
          formGroup.value['1-CFAProductCodeCheckItOuts']['1-checkBoxes'][4],
        isGRFStuddedCategory:
          formGroup.value['1-CFAProductCodeCheckItOuts']['1-checkBoxes'][5],
        isLoyalityPointsRedeemable:
          formGroup.value['1-CFAProductCodeCheckItOuts']['1-checkBoxes'][6],
        isPlainSilver:
          formGroup.value['1-CFAProductCodeCheckItOuts']['1-checkBoxes'][7],
        isPlainPlatinum:
          formGroup.value['1-CFAProductCodeCheckItOuts']['1-checkBoxes'][8],
        isFOCApplicable:
          formGroup.value['1-CFAProductCodeCheckItOuts']['1-checkBoxes'][9],
        isBiMetal:
          formGroup.value['1-CFAProductCodeCheckItOuts']['1-checkBoxes'][10],
        isMiaUCP:
          formGroup.value['1-CFAProductCodeCheckItOuts']['1-checkBoxes'][11],
        isPlainStudPricing:
          formGroup.value['1-CFAProductCodeCheckItOuts']['1-checkBoxes'][12],
        isJewelleryItem:
          formGroup.value['1-CFAProductCodeCheckItOuts']['1-checkBoxes'][13],
        quickSilverReedemptionAllowed:
          formGroup.value['1-CFAProductCodeCheckItOuts']['1-checkBoxes'][14],
        whetherItIsStuddedOrNot:
          formGroup.value['1-CFAProductCodeCheckItOuts']['1-checkBoxes'][15],
        TEPIssueOutCategory:
          formGroup.value['1-CFAProductCodeCheckItOuts']['1-checkBoxes'][16],
        DynamicF1CalculationYesOrNo:
          formGroup.value['1-CFAProductCodeCheckItOuts'][
            '1-dynamicF1CalculationYesOrNo'
          ]
      },
      productGroupCode:
        formGroup.value['1-CFAProductCodeDetails']['1-CFAProductCode'],
      description: formGroup.value['1-CFAProductCodeDetails']['1-description'],
      materialCode: formGroup.value['1-CFAProductCodeDetails']['1-metalType'],
      orgCode: '',
      productType:
        formGroup.value['1-CFAProductCodeDetails']['1-productCategory']
    });
  }
  deleteButton(formGroup: FormGroup) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });
  }
  public formGroupCreated(formGroup: FormGroup) {
    if (this.CFAProductCode) {
      // console.log('aaa', this.CFAProductCode);
      formGroup
        .get('1-CFAProductCodeDetails')
        .get('1-CFAProductCode')
        .disable({
          onlySelf: true
        });
    }
  }
}
