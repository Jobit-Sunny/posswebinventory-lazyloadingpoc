import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CfaProductCodeListingComponent } from '../../containers/cfa-product-code-listing/cfa-product-code-listing.component';

@Component({
  selector: 'poss-web-cfa-product-code-item',
  templateUrl: './cfa-product-code-item.component.html',
  styleUrls: ['./cfa-product-code-item.component.scss']
})
export class CfaProductCodeItemComponent implements OnInit {
  @Input() CFAproductCodeItem;
  @Output() productGroupCode = new EventEmitter<any>();
  constructor() {}

  ngOnInit() {}
  edit(productGroupCode) {
    this.productGroupCode.emit(productGroupCode);
  }
}
