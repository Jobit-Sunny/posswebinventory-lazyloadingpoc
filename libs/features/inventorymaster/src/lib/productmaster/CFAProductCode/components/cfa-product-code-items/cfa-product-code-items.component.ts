import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output,
  OnDestroy
} from '@angular/core';
import { PageEvent } from '@angular/material';
import { Subject } from 'rxjs';
import { AppsettingFacade } from '@poss-web/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'poss-web-cfa-product-code-items',
  templateUrl: './cfa-product-code-items.component.html',
  styleUrls: ['./cfa-product-code-items.component.scss']
})
export class CfaProductCodeItemsComponent implements OnInit, OnDestroy {
  @Input() CFAProductCodeListing;
  @Input() count;
  @Input() pageEvent: PageEvent;
  @Output() productGroupCode = new EventEmitter<any>();
  destroy$ = new Subject<null>();
  @Input() pageSizeOptions;
  @Output() paginator = new EventEmitter<PageEvent>();
  @Input() minPageSize;
  constructor(private appSettingFacade: AppsettingFacade) {}
  ngOnInit() {}
  emitProductGroupCode(productGroupCode) {
    this.productGroupCode.emit(productGroupCode);
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
