import { CustomErrors } from '@poss-web/core';
import {
  CFAProducts,
  ProductType,
  MaterialType
} from '../models/cfa-product-code.model';

export interface CFAProductCodeState {
  error: CustomErrors;
  CFAProductCodeListing: CFAProducts[];
  totalElements: number;
  CFAProduct: CFAProducts;
  isLoading: boolean;
  isLoadingCFAProduct: boolean;
  isSaving: boolean;
  isUpdating: boolean;
  productType: ProductType[];
  materialType: MaterialType[];
}
