import { Injectable } from '@angular/core';
import { DataPersistence } from '@nrwl/angular';
import {
  NotificationService,
  CustomErrors,
  CustomErrorAdaptor
} from '@poss-web/core';
import { CfaProductCodeService } from '../services/cfa-product-code.service';
import { Effect } from '@ngrx/effects';
import {
  CFAProductCodeActionTypes,
  LoadCFAProductCodeListingSuccessPayload
} from './cfa-product-code.actions';
import * as CFAProductCodeActions from './cfa-product-code.actions';
import { map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import {
  CFAProducts,
  ProductType,
  MaterialType
} from '../models/cfa-product-code.model';

@Injectable()
export class CFAProductCodeEffects {
  constructor(
    private dataPersistence: DataPersistence<any>,
    private notificationService: NotificationService,
    private CFAProductCodeService: CfaProductCodeService
  ) {}
  @Effect()
  loadCFAProduct$ = this.dataPersistence.fetch(
    CFAProductCodeActionTypes.LOAD_CFA_PRODUCTS,
    {
      run: (action: CFAProductCodeActions.LoadCFAProducts) => {
        return this.CFAProductCodeService.getCFAProducts(action.payload).pipe(
          map(
            (
              loadCFAProductCodeListing: LoadCFAProductCodeListingSuccessPayload
            ) =>
              new CFAProductCodeActions.LoadCFAProductsSuccess(
                loadCFAProductCodeListing
              )
          )
        );
      },
      onError: (
        action: CFAProductCodeActions.LoadCFAProducts,
        error: HttpErrorResponse
      ) => {
        return new CFAProductCodeActions.LoadCFAProductsFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect()
  loadCFAProductsBasedOnProductGroupCode$ = this.dataPersistence.fetch(
    CFAProductCodeActionTypes.LOAD_CFA_PRODUCTS_BASED_ON_PRODUCTGROUPCODE,
    {
      run: (
        action: CFAProductCodeActions.LoadCFAProductsBasedOnProductGroupCode
      ) => {
        return this.CFAProductCodeService.getCFAProductsBasedProductGroupCode(
          action.payload
        ).pipe(
          map(
            (loadCFaProductsBasedOnProductGroupCode: CFAProducts) =>
              new CFAProductCodeActions.LoadCFAProductsBasedOnProductGroupCodeSuccess(
                loadCFaProductsBasedOnProductGroupCode
              )
          )
        );
      },
      onError: (
        action: CFAProductCodeActions.LoadCFAProductsBasedOnProductGroupCode,
        error: HttpErrorResponse
      ) => {
        return new CFAProductCodeActions.LoadCFAProductsBasedOnProductGroupCodeFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect()
  searchCFAProduct$ = this.dataPersistence.fetch(
    CFAProductCodeActionTypes.SEARCH_CFA_PRODUCT,
    {
      run: (action: CFAProductCodeActions.SearchCFAproduct) => {
        return this.CFAProductCodeService.getCFASearchrResult(
          action.payload
        ).pipe(
          map(
            (SearchResult: CFAProducts[]) =>
              new CFAProductCodeActions.SearchCFAProductSuccess(SearchResult)
          )
        );
      },
      onError: (
        action: CFAProductCodeActions.SearchCFAproduct,
        error: HttpErrorResponse
      ) => {
        return new CFAProductCodeActions.SearchCFAProductFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect()
  saveCFAProducts$ = this.dataPersistence.pessimisticUpdate(
    CFAProductCodeActionTypes.SAVE_CFA_PRODUCTS,
    {
      run: (action: CFAProductCodeActions.SaveCFAProducts) => {
        return this.CFAProductCodeService.saveCFAProducts(action.payload).pipe(
          map(() => new CFAProductCodeActions.SaveCFAProductsSuccess())
        );
      },
      onError: (
        action: CFAProductCodeActions.SaveCFAProducts,
        error: HttpErrorResponse
      ) => {
        return new CFAProductCodeActions.SaveCFAProductsFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect()
  updateCFAProducts$ = this.dataPersistence.pessimisticUpdate(
    CFAProductCodeActionTypes.UPDATE_CFA_PRODUCTS,
    {
      run: (action: CFAProductCodeActions.UPdateCFAProducts) => {
        return this.CFAProductCodeService.updateCFAProducts(
          action.payload
        ).pipe(map(() => new CFAProductCodeActions.UPdateCFAProductsSuccess()));
      },
      onError: (
        action: CFAProductCodeActions.UPdateCFAProducts,
        error: HttpErrorResponse
      ) => {
        return new CFAProductCodeActions.UPdateCFAProductsFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect()
  loadProductTypes$ = this.dataPersistence.fetch(
    CFAProductCodeActionTypes.LOAD_PRODUCT_TYPES,
    {
      run: (action: CFAProductCodeActions.LoadProductTypes) => {
        return this.CFAProductCodeService.loadProductTypes().pipe(
          map(
            (productType: ProductType[]) =>
              new CFAProductCodeActions.LoadProductTypesSuccess(productType)
          )
        );
      },
      onError: (
        action: CFAProductCodeActions.LoadProductTypes,
        error: HttpErrorResponse
      ) => {
        return new CFAProductCodeActions.LoadProductTypesFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect()
  loadMaterialTypes$ = this.dataPersistence.fetch(
    CFAProductCodeActionTypes.LOAD_MATERIAL_TYPES,
    {
      run: (action: CFAProductCodeActions.LoadMaterialTypes) => {
        return this.CFAProductCodeService.loadMaterialTypes().pipe(
          map(
            (materialType: MaterialType[]) =>
              new CFAProductCodeActions.LoadMaterialTypesSucceess(materialType)
          )
        );
      },
      onError: (
        action: CFAProductCodeActions.LoadMaterialTypes,
        error: HttpErrorResponse
      ) => {
        return new CFAProductCodeActions.LoadMaterialTypesFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
}
