import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../../inventorymasters.state';
import * as CFAProductCodeActions from './cfa-product-code.actions';
import { CFAProductCodeSelectors } from './cfa-product-code.selectors';
import {
  LoadCFAProductCodeListingPayload,
  UpdateCFAProductsPayload
} from './cfa-product-code.actions';

@Injectable()
export class CFAProductCodeFacade {
  constructor(private store: Store<State>) {}
  private CFAProductCodeListing$ = this.store.select(
    CFAProductCodeSelectors.selectCFAProductCodeListing
  );
  private totalElements$ = this.store.select(
    CFAProductCodeSelectors.selectTotalElements
  );
  private CFAProductCode$ = this.store.select(
    CFAProductCodeSelectors.selectCFAProductCode
  );
  private isLoading$ = this.store.select(
    CFAProductCodeSelectors.selectIsLoading
  );
  private isLoadingCFAProduct$ = this.store.select(
    CFAProductCodeSelectors.selectIsLoadingCFAProduct
  );
  private isSaving$ = this.store.select(CFAProductCodeSelectors.selectIsSaving);
  private isUpdating$ = this.store.select(
    CFAProductCodeSelectors.selectIsUpdating
  );
  private hasError$ = this.store.select(CFAProductCodeSelectors.selectHasError);
  private productTypes$ = this.store.select(
    CFAProductCodeSelectors.selectProductTypes
  );
  private materialTypes$ = this.store.select(
    CFAProductCodeSelectors.selectMaterialTypes
  );
  getCFAProductCodeListing() {
    return this.CFAProductCodeListing$;
  }
  getTotalElements() {
    return this.totalElements$;
  }
  getCFAProductCodeBasedOnProductGroup() {
    return this.CFAProductCode$;
  }
  getIsLoading() {
    return this.isLoading$;
  }
  getIsLoadingCFAProduct() {
    return this.isLoadingCFAProduct$;
  }
  getIsSaving() {
    return this.isSaving$;
  }
  getIsUpdating() {
    return this.isUpdating$;
  }
  getHasError() {
    return this.hasError$;
  }
  getProductTypes() {
    return this.productTypes$;
  }
  getMaterialTypes() {
    return this.materialTypes$;
  }
  loadCFAProductCodeListing(
    loadCFAProductCodePayload: LoadCFAProductCodeListingPayload
  ) {
    this.store.dispatch(
      new CFAProductCodeActions.LoadCFAProducts(loadCFAProductCodePayload)
    );
  }
  loadCFAProductCode(productGroupCode: string) {
    this.store.dispatch(
      new CFAProductCodeActions.LoadCFAProductsBasedOnProductGroupCode(
        productGroupCode
      )
    );
  }
  loadCFAPodcutSearch(searchValue) {
    this.store.dispatch(
      new CFAProductCodeActions.SearchCFAproduct(searchValue)
    );
  }
  saveCFAProducts(data: any) {
    this.store.dispatch(new CFAProductCodeActions.SaveCFAProducts(data));
  }
  resetCFAProducts() {
    this.store.dispatch(new CFAProductCodeActions.ResetCFAProducts());
  }
  updateCFAProducts(updateCFAProductsPayload: UpdateCFAProductsPayload) {
    this.store.dispatch(
      new CFAProductCodeActions.UPdateCFAProducts(updateCFAProductsPayload)
    );
  }
  searchCFAProdcut(prodcutGroupCode: string) {
    this.store.dispatch(
      new CFAProductCodeActions.SearchCFAproduct(prodcutGroupCode)
    );
  }
  loadProdcutTypes() {
    this.store.dispatch(new CFAProductCodeActions.LoadProductTypes());
  }
  loadMaterialTypes() {
    this.store.dispatch(new CFAProductCodeActions.LoadMaterialTypes());
  }
}
