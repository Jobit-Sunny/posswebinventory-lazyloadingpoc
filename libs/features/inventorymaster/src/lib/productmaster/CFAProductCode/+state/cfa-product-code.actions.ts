import { Action } from '@ngrx/store';
import {
  CFAProducts,
  ProductType,
  MaterialType
} from '../models/cfa-product-code.model';
import { CustomErrors } from '@poss-web/core';
import { ProductCategory } from '../../productCategory/models/product-category.model';
export interface LoadCFAProductCodeListingSuccessPayload {
  CFAProductCodeListing: CFAProducts[];
  totalElements: number;
}
export interface LoadCFAProductCodeListingPayload {
  pageIndex: number;
  pageSize: number;
}
export interface UpdateCFAProductsPayload {
  productGroupCode: string;
  data: any;
}
export enum CFAProductCodeActionTypes {
  LOAD_CFA_PRODUCTS = '[CFA Products] Load CFA Products',
  LOAD_CFA_PRODUCTS_SUCCESS = '[CFA Products] Load CFA Products Success',
  LOAD_CFA_PRODUCTS_FAILURE = '[CFA Products] Load CFA Products Failure',
  LOAD_CFA_PRODUCTS_BASED_ON_PRODUCTGROUPCODE = '[CFA Products] Load CFA Product Code Based On ProductGroupCode',
  LOAD_CFA_PRODUCTS_BASED_ON_PRODUCTGROUPCODE_SUCCESS = '[CFA Products] Load CFA Products Based On ProductGroupCode Success',
  LOAD_CFA_PRODUCTS_BASED_ON_PRODUCTGROUPCODE_FAILURE = '[CFA Products] Load CFA Products Based On ProductGroupCode Failure',
  SEARCH_CFA_PRODUCT = '[CFAProducts] Search CFA Product',
  SEARCH_CFA_PRODUCT_SUCCESS = '[CFA Products] Search CFA Product Success',
  SEARCH_CFA_PRODUCT_FAILURE = '[CFA Products] Search CFA Product Failure',
  SAVE_CFA_PRODUCTS = '[CFA Products] Save CFA Products',
  SAVE_CFA_PRODUCTS_SUCCESS = '[CFA Products] Save CFA Products Success',
  SAVE_CFA_PRODUCTS_FAILURE = '[CFA Products] Save CFA Products Failure',
  RESET_CFA_PRODUCTS = '[CFA Products] Reset CFA Products',
  UPDATE_CFA_PRODUCTS = '[CFA Products]Update CFA Products',
  UPDATE_CFA_PRODUCTS_SUCCESS = '[CFA Products]Update CFA Products Success',
  UPDATE_CFA_PRODUCTS_FAILURE = '[CFA Products]Update CFA Products Failure',
  LOAD_PRODUCT_TYPES = '[CFA Products]Load Product Types',
  LOAD_PRODUCT_TYPES_SUCCESS = '[CFA Products]Load Product Types Success',
  LOAD_PRODUCT_TYPES_FAILURE = '[CFA Products]Load Product Types Failure',
  LOAD_MATERIAL_TYPES = '[CFA Products]Load Material Types',
  LOAD_MATERIAL_TYPES_SUCCESS = '[CFA Products]Load Material Types Success',
  LOAD_MATERIAL_TYPES_FAILURE = '[CFA Products]Load Material Types Failure'
}
export class LoadCFAProducts implements Action {
  readonly type = CFAProductCodeActionTypes.LOAD_CFA_PRODUCTS;
  constructor(public payload: LoadCFAProductCodeListingPayload) {}
}
export class LoadCFAProductsSuccess implements Action {
  readonly type = CFAProductCodeActionTypes.LOAD_CFA_PRODUCTS_SUCCESS;
  constructor(public payload: LoadCFAProductCodeListingSuccessPayload) {}
}
export class LoadCFAProductsFailure implements Action {
  readonly type = CFAProductCodeActionTypes.LOAD_CFA_PRODUCTS_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export class LoadCFAProductsBasedOnProductGroupCode implements Action {
  readonly type =
    CFAProductCodeActionTypes.LOAD_CFA_PRODUCTS_BASED_ON_PRODUCTGROUPCODE;
  constructor(public payload: string) {}
}
export class LoadCFAProductsBasedOnProductGroupCodeSuccess implements Action {
  readonly type =
    CFAProductCodeActionTypes.LOAD_CFA_PRODUCTS_BASED_ON_PRODUCTGROUPCODE_SUCCESS;
  constructor(public payload: CFAProducts) {}
}
export class LoadCFAProductsBasedOnProductGroupCodeFailure implements Action {
  readonly type =
    CFAProductCodeActionTypes.LOAD_CFA_PRODUCTS_BASED_ON_PRODUCTGROUPCODE_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export class SearchCFAproduct implements Action {
  readonly type = CFAProductCodeActionTypes.SEARCH_CFA_PRODUCT;
  constructor(public payload: string) {}
}
export class SearchCFAProductSuccess implements Action {
  readonly type = CFAProductCodeActionTypes.SEARCH_CFA_PRODUCT_SUCCESS;
  constructor(public payload: CFAProducts[]) {}
}
export class SearchCFAProductFailure implements Action {
  readonly type = CFAProductCodeActionTypes.SEARCH_CFA_PRODUCT_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export class SaveCFAProducts implements Action {
  readonly type = CFAProductCodeActionTypes.SAVE_CFA_PRODUCTS;
  constructor(public payload: any) {}
}
export class SaveCFAProductsSuccess implements Action {
  readonly type = CFAProductCodeActionTypes.SAVE_CFA_PRODUCTS_SUCCESS;
  //constructor(public payload: any) {}
}
export class SaveCFAProductsFailure implements Action {
  readonly type = CFAProductCodeActionTypes.SAVE_CFA_PRODUCTS_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export class ResetCFAProducts implements Action {
  readonly type = CFAProductCodeActionTypes.RESET_CFA_PRODUCTS;
}
export class UPdateCFAProducts implements Action {
  readonly type = CFAProductCodeActionTypes.UPDATE_CFA_PRODUCTS;
  constructor(public payload: UpdateCFAProductsPayload) {}
}
export class UPdateCFAProductsSuccess implements Action {
  readonly type = CFAProductCodeActionTypes.UPDATE_CFA_PRODUCTS_SUCCESS;
}
export class UPdateCFAProductsFailure implements Action {
  readonly type = CFAProductCodeActionTypes.UPDATE_CFA_PRODUCTS_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export class LoadProductTypes implements Action {
  readonly type = CFAProductCodeActionTypes.LOAD_PRODUCT_TYPES;
}
export class LoadProductTypesSuccess implements Action {
  readonly type = CFAProductCodeActionTypes.LOAD_PRODUCT_TYPES_SUCCESS;
  constructor(public payload: ProductType[]) {}
}
export class LoadProductTypesFailure implements Action {
  readonly type = CFAProductCodeActionTypes.LOAD_PRODUCT_TYPES_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export class LoadMaterialTypes implements Action {
  readonly type = CFAProductCodeActionTypes.LOAD_MATERIAL_TYPES;
}
export class LoadMaterialTypesSucceess implements Action {
  readonly type = CFAProductCodeActionTypes.LOAD_MATERIAL_TYPES_SUCCESS;
  constructor(public payload: MaterialType[]) {}
}
export class LoadMaterialTypesFailure implements Action {
  readonly type = CFAProductCodeActionTypes.LOAD_MATERIAL_TYPES_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export type CFAProductCodeActions =
  | LoadCFAProducts
  | LoadCFAProductsSuccess
  | LoadCFAProductsFailure
  | LoadCFAProductsBasedOnProductGroupCode
  | LoadCFAProductsBasedOnProductGroupCodeSuccess
  | LoadCFAProductsBasedOnProductGroupCodeFailure
  | SearchCFAproduct
  | SearchCFAProductSuccess
  | SearchCFAProductFailure
  | SaveCFAProducts
  | SaveCFAProductsSuccess
  | SaveCFAProductsFailure
  | ResetCFAProducts
  | UPdateCFAProducts
  | UPdateCFAProductsSuccess
  | UPdateCFAProductsFailure
  | LoadProductTypes
  | LoadProductTypesSuccess
  | LoadProductTypesFailure
  | LoadMaterialTypes
  | LoadMaterialTypesSucceess
  | LoadMaterialTypesFailure;
