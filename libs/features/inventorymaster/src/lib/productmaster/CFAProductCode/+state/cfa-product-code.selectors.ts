import { createSelector } from '@ngrx/store';
import { selectInventoryMasters } from '../../../inventorymasters.state';

const selectCFAProductCodeListing = createSelector(
  selectInventoryMasters,
  state => state.CFAProductCodeState.CFAProductCodeListing
);
const selectTotalElements = createSelector(
  selectInventoryMasters,
  state => state.CFAProductCodeState.totalElements
);
const selectCFAProductCode = createSelector(
  selectInventoryMasters,
  state => state.CFAProductCodeState.CFAProduct
);
const selectIsLoading = createSelector(
  selectInventoryMasters,
  state => state.CFAProductCodeState.isLoading
);
const selectIsLoadingCFAProduct = createSelector(
  selectInventoryMasters,
  state => state.CFAProductCodeState.isLoadingCFAProduct
);
const selectIsSaving = createSelector(
  selectInventoryMasters,
  state => state.CFAProductCodeState.isSaving
);
const selectIsUpdating = createSelector(
  selectInventoryMasters,
  state => state.CFAProductCodeState.isUpdating
);
const selectHasError = createSelector(
  selectInventoryMasters,
  state => state.CFAProductCodeState.error
);
const selectProductTypes = createSelector(
  selectInventoryMasters,
  state => state.CFAProductCodeState.productType
);
const selectMaterialTypes = createSelector(
  selectInventoryMasters,
  state => state.CFAProductCodeState.materialType
);
export const CFAProductCodeSelectors = {
  selectCFAProductCodeListing,
  selectTotalElements,
  selectCFAProductCode,
  selectIsLoading,
  selectIsLoadingCFAProduct,
  selectIsSaving,
  selectHasError,
  selectIsUpdating,
  selectProductTypes,
  selectMaterialTypes
};
