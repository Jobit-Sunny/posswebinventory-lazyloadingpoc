import { CFAProductCodeState } from './cfa-product-code.state';
import {
  CFAProductCodeActions,
  CFAProductCodeActionTypes
} from './cfa-product-code.actions';

const initialState: CFAProductCodeState = {
  error: null,
  CFAProductCodeListing: null,
  totalElements: null,
  CFAProduct: null,
  isLoading: null,
  isLoadingCFAProduct: null,
  isSaving: null,
  isUpdating: null,
  productType: null,
  materialType: null
};
export function CFAProductCodeReducer(
  state: CFAProductCodeState,
  action: CFAProductCodeActions
): CFAProductCodeState {
  switch (action.type) {
    case CFAProductCodeActionTypes.LOAD_CFA_PRODUCTS:
      return {
        ...state,
        isLoading: false,
        error: null
      };
    case CFAProductCodeActionTypes.LOAD_CFA_PRODUCTS_SUCCESS:
      return {
        ...state,
        isLoading: true,
        CFAProductCodeListing: action.payload.CFAProductCodeListing,
        totalElements: action.payload.totalElements
      };
    case CFAProductCodeActionTypes.LOAD_CFA_PRODUCTS_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };
    case CFAProductCodeActionTypes.LOAD_CFA_PRODUCTS_BASED_ON_PRODUCTGROUPCODE:
      return {
        ...state,
        isLoadingCFAProduct: false
      };
    case CFAProductCodeActionTypes.LOAD_CFA_PRODUCTS_BASED_ON_PRODUCTGROUPCODE_SUCCESS:
      return {
        ...state,
        CFAProduct: action.payload,
        isLoadingCFAProduct: true
      };
    case CFAProductCodeActionTypes.LOAD_CFA_PRODUCTS_BASED_ON_PRODUCTGROUPCODE_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingCFAProduct: false
      };
    case CFAProductCodeActionTypes.SEARCH_CFA_PRODUCT:
      return {
        ...state
      };
    case CFAProductCodeActionTypes.SEARCH_CFA_PRODUCT_SUCCESS:
      return {
        ...state,
        CFAProductCodeListing: action.payload
      };
    case CFAProductCodeActionTypes.SEARCH_CFA_PRODUCT_FAILURE:
      return {
        ...state,
        error: action.payload,
        CFAProductCodeListing: null,
        totalElements: null
      };
    case CFAProductCodeActionTypes.SAVE_CFA_PRODUCTS:
      return {
        ...state,
        isSaving: false
      };
    case CFAProductCodeActionTypes.SAVE_CFA_PRODUCTS_SUCCESS:
      return {
        ...state,
        isSaving: true
      };
    case CFAProductCodeActionTypes.SAVE_CFA_PRODUCTS_FAILURE:
      return {
        ...state,
        isSaving: false,
        error: action.payload
      };
    case CFAProductCodeActionTypes.RESET_CFA_PRODUCTS:
      return {
        ...state,
        CFAProduct: null,
        error: null,
        isSaving: null,
        isUpdating: null
      };
    case CFAProductCodeActionTypes.UPDATE_CFA_PRODUCTS:
      return {
        ...state,
        isUpdating: false
      };
    case CFAProductCodeActionTypes.UPDATE_CFA_PRODUCTS_SUCCESS:
      return {
        ...state,
        isUpdating: true
      };
    case CFAProductCodeActionTypes.UPDATE_CFA_PRODUCTS_FAILURE:
      return {
        ...state,
        isUpdating: false,
        error: action.payload
      };
    case CFAProductCodeActionTypes.LOAD_PRODUCT_TYPES:
      return {
        ...state
      };
    case CFAProductCodeActionTypes.LOAD_PRODUCT_TYPES_SUCCESS:
      return {
        ...state,
        productType: action.payload
      };
    case CFAProductCodeActionTypes.LOAD_PRODUCT_TYPES_FAILURE:
      return {
        ...state,
        error: action.payload
      };
    case CFAProductCodeActionTypes.LOAD_MATERIAL_TYPES:
      return {
        ...state
      };
    case CFAProductCodeActionTypes.LOAD_MATERIAL_TYPES_SUCCESS:
      return {
        ...state,
        materialType: action.payload
      };
    case CFAProductCodeActionTypes.LOAD_MATERIAL_TYPES_FAILURE:
      return {
        ...state,
        error: action.payload
      };
    default:
      return state;
  }
}
