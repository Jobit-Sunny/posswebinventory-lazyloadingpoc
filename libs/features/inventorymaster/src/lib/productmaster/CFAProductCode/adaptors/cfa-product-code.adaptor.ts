import { LoadCFAProductCodeListingSuccessPayload } from '../+state/cfa-product-code.actions';
import {
  CFAProducts,
  ProductType,
  MaterialType
} from '../models/cfa-product-code.model';

export class CFAProductCodeAdaptor {
  static CFAProductCodeListingData: LoadCFAProductCodeListingSuccessPayload;
  static getCFAProductCodeListing(
    data: any
  ): LoadCFAProductCodeListingSuccessPayload {
    const CFAProductCodeListing: CFAProducts[] = [];
    for (const listItem of data.results) {
      CFAProductCodeListing.push({
        productGroupCode: listItem.productGroupCode,
        productType: listItem.productType,
        description: listItem.description,
        materialCode: listItem.materialCode,
        orgCode: listItem.orgCode,
        configDetails: listItem.configDetails
      });
    }
    this.CFAProductCodeListingData = {
      CFAProductCodeListing: CFAProductCodeListing,
      totalElements: data.totalElements
    };
    return this.CFAProductCodeListingData;
  }
  static getCFAProductsBasdedOnProductGroupCode(): CFAProducts {
    const CFAProduct: CFAProducts = {
      productGroupCode: '',
      productType: '',
      description: '',
      materialCode: '',
      orgCode: '',
      configDetails: {}
    };
    return CFAProduct;
  }
  static getCFASearchProduct(data: any): CFAProducts[] {
    const CFAProduct: CFAProducts[] = [];
    CFAProduct.push({
      productGroupCode: data.productGroupCode,
      productType: data.productType,
      description: data.description,
      materialCode: data.materialCode,
      orgCode: data.orgCode,
      configDetails: data.configDetails
    });
    return CFAProduct;
  }
  static getProductTypes(data: any): any {
    const productTypes: ProductType[] = [];
    for (const productType of data.values) {
      productTypes.push({
        id: productType.code,
        name: productType.value,
        isActive: productType.isActive
      });
    }
    return productTypes;
  }
  static getMaterialTypes(data: any): any {
    const materialTypes: MaterialType[] = [];
    for (const materialType of data.results) {
      materialTypes.push({
        id: materialType.materialCode,
        name: materialType.description
      });
    }
    return materialTypes;
  }
}
