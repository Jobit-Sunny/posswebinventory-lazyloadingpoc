import { Injectable } from '@angular/core';
import {
  getCFAProductsUrl,
  getCFAProductsBasedOnProductGroupCodeUrl,
  getSaveCFAProductsUrl,
  getUpdateCFAProductsUrl,
  getProductTypesUrl,
  getMaterialTypesUrl
} from '../../../endpoint.constants';
import { ApiService } from '@poss-web/core';
import { Observable, of } from 'rxjs';
import {
  LoadCFAProductCodeListingSuccessPayload,
  LoadCFAProductCodeListingPayload,
  UpdateCFAProductsPayload
} from '../+state/cfa-product-code.actions';
import { map } from 'rxjs/operators';
import { CFAProductCodeAdaptor } from '../adaptors/cfa-product-code.adaptor';
import {
  CFAProducts,
  ProductType,
  MaterialType
} from '../models/cfa-product-code.model';

@Injectable({
  providedIn: 'root'
})
export class CfaProductCodeService {
  constructor(private apiService: ApiService) {}
  getCFAProducts(
    loadCFAProductCodeListingPaylaod: LoadCFAProductCodeListingPayload
  ): Observable<LoadCFAProductCodeListingSuccessPayload> {
    const url = getCFAProductsUrl(
      loadCFAProductCodeListingPaylaod.pageIndex,
      loadCFAProductCodeListingPaylaod.pageSize
    );
    return this.apiService
      .get(url)
      .pipe(map(data => CFAProductCodeAdaptor.getCFAProductCodeListing(data)));
  }
  getCFAProductsBasedProductGroupCode(
    productGroupCode
  ): Observable<CFAProducts> {
    if (productGroupCode === 'new') {
      return of(CFAProductCodeAdaptor.getCFAProductsBasdedOnProductGroupCode());
    } else {
      const url = getCFAProductsBasedOnProductGroupCodeUrl(productGroupCode);
      return this.apiService.get(url);
    }
  }
  getCFASearchrResult(searchValue): Observable<CFAProducts[]> {
    const url = getCFAProductsBasedOnProductGroupCodeUrl(searchValue);
    return this.apiService
      .get(url)
      .pipe(map(data => CFAProductCodeAdaptor.getCFASearchProduct(data)));
  }
  saveCFAProducts(saveCFAProducts: any) {
    // console.log('service', saveCFAProducts);
    const url = getSaveCFAProductsUrl();
    return this.apiService.post(url, saveCFAProducts);
  }
  updateCFAProducts(updateCFAProductsPayload: UpdateCFAProductsPayload) {
    const url = getUpdateCFAProductsUrl(
      updateCFAProductsPayload.productGroupCode
    );
    return this.apiService.put(url, updateCFAProductsPayload.data);
  }
  loadProductTypes(): Observable<ProductType[]> {
    const url = getProductTypesUrl();
    return this.apiService
      .get(url)
      .pipe(map(data => CFAProductCodeAdaptor.getProductTypes(data)));
  }
  loadMaterialTypes(): Observable<MaterialType[]> {
    const url = getMaterialTypesUrl();
    return this.apiService
      .get(url)
      .pipe(map(data => CFAProductCodeAdaptor.getMaterialTypes(data)));
  }
}
