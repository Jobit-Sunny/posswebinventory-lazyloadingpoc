import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CFAProductCodeFacade } from '../../../+state/cfa-product-code.facade';
import { Observable, Subject, of } from 'rxjs';
import {
  CFAProducts,
  ProductType,
  MaterialType
} from '../../../models/cfa-product-code.model';
import { takeUntil, take, skipUntil } from 'rxjs/operators';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventType,
  OverlayNotificationEventRef
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { CustomErrors } from '@poss-web/core';
import { getCFAProductsRouteUrl } from 'libs/features/inventorymaster/src/lib/page-route.constants';
@Component({
  selector: 'poss-web-cfa-product-code',
  templateUrl: './cfa-product-code.component.html'
})
export class CFAProductCodeComponent implements OnInit, OnDestroy {
  productGroupCode: string;
  CFAProduct$: Observable<CFAProducts>;
  destroy$: Subject<null> = new Subject<null>();
  isSaving$: boolean;
  isLoadingCFAProduct$: Observable<boolean>;
  hasError$: Observable<CustomErrors>;
  productTypes$: Observable<ProductType[]>;
  materialTypes$: Observable<MaterialType[]>;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private CFAProductFacade: CFAProductCodeFacade,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    const fromPath = this.route.pathFromRoot[2];
    this.productGroupCode = fromPath.snapshot.params['productGroupCode'];
    this.CFAProductFacade.loadCFAProductCode(this.productGroupCode);
    this.CFAProductFacade.loadProdcutTypes();
    this.CFAProductFacade.loadMaterialTypes();
    this.productTypes$ = this.CFAProductFacade.getProductTypes();
    this.materialTypes$ = this.CFAProductFacade.getMaterialTypes();
    this.CFAProduct$ = this.CFAProductFacade.getCFAProductCodeBasedOnProductGroup();
    this.isLoadingCFAProduct$ = this.CFAProductFacade.getIsLoadingCFAProduct();
    this.CFAProductFacade.getHasError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });
    this.CFAProductFacade.getIsUpdating()
      .pipe(
        skipUntil(of(true)),
        takeUntil(this.destroy$)
      )
      .subscribe(isUpdating => {
        if (isUpdating === false) {
          this.showNotifications('pw.CFAProduct.UpdateSuccessMessage');
        }
      });
    this.CFAProductFacade.getIsSaving()
      .pipe(
        skipUntil(of(true)),
        takeUntil(this.destroy$)
      )
      .subscribe(isSaving => {
        if (isSaving === false) {
          this.showNotifications('pw.CFAProduct.saveSuccessMessage');
        }
      });
  }
  back() {
    this.router.navigate([getCFAProductsRouteUrl()]);
    this.CFAProductFacade.resetCFAProducts();
  }
  addButton(saveFormDetails) {
    if (this.productGroupCode === 'new') {
      this.CFAProductFacade.saveCFAProducts({
        configDetails: saveFormDetails.configDetails,
        description: saveFormDetails.description,
        materialCode: saveFormDetails.materialCode,
        orgCode: saveFormDetails.orgCode,
        productGroupCode: saveFormDetails.productGroupCode,
        productType: saveFormDetails.productType
        //saveFormDetails
      });
    } else {
      this.CFAProductFacade.updateCFAProducts({
        productGroupCode: this.productGroupCode,
        data: {
          configDetails: saveFormDetails.configDetails,
          description: saveFormDetails.description,
          materialCode: saveFormDetails.materialCode,
          orgCode: saveFormDetails.orgCode,
          productGroupCode: this.productGroupCode,
          productType: saveFormDetails.productType
        }
      });
    }
  }
  showNotifications(key) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMessage,
            hasBackdrop: true
          })
          .events.subscribe((eventType: OverlayNotificationEventType) => {
            this.CFAProductFacade.resetCFAProducts();
            this.router.navigate([getCFAProductsRouteUrl()]);
          });
      });
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasBackdrop: true,
        hasClose: true,
        error: error
      })
      .events.pipe(take(1))
      .subscribe((event: OverlayNotificationEventRef) => {
        if (event.eventType === OverlayNotificationEventType.CLOSE) {
        }
      });
  }
}
