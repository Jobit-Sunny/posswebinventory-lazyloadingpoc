import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  AfterViewInit
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CFAProductCodeFacade } from '../../+state/cfa-product-code.facade';
import { Observable, Subject, fromEvent } from 'rxjs';
import { CFAProducts } from '../../models/cfa-product-code.model';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import { PageEvent } from '@angular/material';
import { takeUntil, debounceTime, take } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';
import {
  OverlayNotificationType,
  OverlayNotificationEventType,
  OverlayNotificationService,
  OverlayNotificationEventRef,
  ErrorEnums
} from '@poss-web/shared';

import { TranslateService } from '@ngx-translate/core';
import { getProductMasterDashboardRouteUrl } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-cfa-product-code-listing',
  templateUrl: './cfa-product-code-listing.component.html',
  styleUrls: ['./cfa-product-code-listing.component.scss']
})
export class CfaProductCodeListingComponent
  implements OnInit, OnDestroy, AfterViewInit {
  CFAProductCodeListing$: Observable<CFAProducts[]>;
  searchErrorCode: string;
  totalElements$: Observable<number>;
  destroy$ = new Subject<null>();
  isLoading$: Observable<boolean>;
  hasError$: Observable<CustomErrors>;
  CFAProductCodePageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };
  pageSizeOptions: number[] = [];
  minPageSize: number;

  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;
  searchForm = new FormGroup({
    searchValue: new FormControl()
  });
  constructor(
    private router: Router,
    private CFAProductFacade: CFAProductCodeFacade,
    private appsettingFacade: AppsettingFacade,
    private route: ActivatedRoute,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.searchErrorCode = ErrorEnums.ERR_PRO_004;
    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        const pageSize = JSON.parse(data);
        this.CFAProductCodePageEvent.pageSize = pageSize;
        this.loadCFAProductCodeListing();
      });
    this.appsettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.pageSizeOptions = data;
        this.minPageSize = this.pageSizeOptions.reduce((a: number, b: number) =>
          a < b ? a : b
        );
      });

    this.CFAProductCodeListing$ = this.CFAProductFacade.getCFAProductCodeListing();
    this.totalElements$ = this.CFAProductFacade.getTotalElements();
    this.isLoading$ = this.CFAProductFacade.getIsLoading();
    this.hasError$ = this.CFAProductFacade.getHasError();
    this.CFAProductFacade.getHasError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });
  }
  ngAfterViewInit(): void {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe(event => {
        const searchValue = this.searchForm.value.searchValue;
        if (searchValue) {
          this.search(searchValue);
        } else {
          this.clearSearch();
        }
      });
  }
  showSaveNotification(errorMessage: string) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.TIMER,
        message: errorMessage,
        hasBackdrop: true
      })
      .events.subscribe((eventType: OverlayNotificationEventType) => {
        this.CFAProductFacade.resetCFAProducts();
      });
  }

  search(searchValue: string) {
    if (!searchValue.search('^[_A-z0-9]*((-|s)*[_A-z0-9])*$')) {
      this.CFAProductFacade.searchCFAProdcut(searchValue);
    }
  }
  clearSearch() {
    this.searchForm.reset();
    this.loadCFAProductCodeListing();
  }
  loadCFAProductCodeListing() {
    this.CFAProductFacade.loadCFAProductCodeListing(
      this.CFAProductCodePageEvent
    );
  }
  back() {
    this.router.navigate([getProductMasterDashboardRouteUrl()]);
    this.CFAProductFacade.resetCFAProducts();
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
  paginate(pageEvent: PageEvent) {
    this.CFAProductCodePageEvent = pageEvent;
    this.loadCFAProductCodeListing();
  }
  emitProductGroupCode(productGroupCode) {
    this.router.navigate(
      ['/', 'master', 'productMasters', productGroupCode, 'CFAProduct'],
      {
        relativeTo: this.route
      }
    );
  }
  addnew() {
    this.router.navigate(
      ['/', 'master', 'productMasters', 'new', 'CFAProduct'],
      {
        relativeTo: this.route
      }
    );
  }
  errorHandler(error: CustomErrors) {
    if (error.code === this.searchErrorCode) {
      return;
    }

    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasBackdrop: true,
        hasClose: true,
        error: error
      })
      .events.pipe(take(1))
      .subscribe((event: OverlayNotificationEventRef) => {
        if (event.eventType === OverlayNotificationEventType.CLOSE) {
        }
      });
  }

  omit_special_char($event: KeyboardEvent) {
    const pattern = /^[-_A-Za-z0-9]$/;
    return pattern.test($event.key);
  }
}
