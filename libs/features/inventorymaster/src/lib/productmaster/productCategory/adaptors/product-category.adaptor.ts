import { LoadProductCategoryListingSuccessPayload } from '../+state/product-category.actions';
import { ProductCategory, ProductCategoryDetails } from '../models/product-category.model';


export class ProductCategoryAdaptor {
    static binGroupDetailsListing: LoadProductCategoryListingSuccessPayload;

    static getProductCategoryDetailsListing(
        data: any
    ): LoadProductCategoryListingSuccessPayload {
        const productCategoryDetailsListing: ProductCategory[] = [];
        for (const listItem of data.results) {
            productCategoryDetailsListing.push({
                productCategoryCode: listItem.productCategoryCode,
                description: listItem.description
            });
        }

        this.binGroupDetailsListing = {
            productCategoryListing: productCategoryDetailsListing,
            totalElements: data.totalElements
        };
        return this.binGroupDetailsListing;
    }
    static getProductCategoryDetailsSearch(
        data: any
    ): ProductCategoryDetails[] {
        const productCategoryDetails: ProductCategoryDetails[] = [];
        productCategoryDetails.push({
            configDetails: data.configDetails,
            description: data.description,
            orgCode: data.orgCode,
            productCategoryCode: data.productCategoryCode
        });


        return productCategoryDetails;
    }

}
