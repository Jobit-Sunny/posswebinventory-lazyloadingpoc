import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  EventEmitter,
  Output,
  OnDestroy,
  AfterViewInit
} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialog, PageEvent } from '@angular/material';
import { Router } from '@angular/router';
import { Subject, Observable, fromEvent } from 'rxjs';
import { takeUntil, debounceTime, take } from 'rxjs/operators';

import { TranslateService } from '@ngx-translate/core';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationEventType,
  ErrorEnums
} from '@poss-web/shared';

import { ProductCategoryFacade } from '../../+state/product-category.facade';
import { LoadProductCategoryDetails } from '../../+state/product-category.actions';
import {
  ProductCategory,
  ProductCategoryEnum,
  ProductCategoryDetails
} from '../../models/product-category.model';
import { ProductCategoryDetailsComponent } from '../../components/product-category-details/product-category-details.component';
import { getProductMasterDashboardRouteUrl } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-product-category-listing',
  templateUrl: './product-category-listing.component.html',
  styleUrls: ['./product-category-listing.component.scss']
})
export class ProductCategoryListingComponent
  implements OnInit, OnDestroy, AfterViewInit {
  @Output() formEmit = new EventEmitter<any>();

  destroy$ = new Subject<null>();

  productCategoryPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };

  isLoading$: Observable<boolean>;
  productCategoryListing$: Observable<ProductCategory[]>;
  productCategoryCount$: Observable<number>;

  hasError$: Observable<CustomErrors>;

  searchErrorCode: string;

  productCategoryDetailsByCode$: ProductCategoryDetails;

  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;
  searchForm = new FormGroup({
    searchValue: new FormControl()
  });

  constructor(
    public dialog: MatDialog,
    public router: Router,
    private translate: TranslateService,
    private appsettingFacade: AppsettingFacade,
    private overlayNotification: OverlayNotificationService,
    private productCategoryFacade: ProductCategoryFacade
  ) { }

  ngOnInit() {
    this.productCategoryFacade.resetproductCategoryDialogData();
    this.hasError$ = this.productCategoryFacade.getError();
    this.searchErrorCode = ErrorEnums.ERR_PRO_005;
    this.productCategoryFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        const pageSize = JSON.parse(data);
        this.productCategoryPageEvent.pageSize = pageSize;
        this.loadProductCategoryDetails();
      });

    this.isLoading$ = this.productCategoryFacade.getisLoading();
    this.productCategoryListing$ = this.productCategoryFacade.getproductCategoryDetailsListing();
    this.productCategoryCount$ = this.productCategoryFacade.getTotalproductCategoryDetails();

    this.productCategoryFacade
      .getproductCategorySaveResponse()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          this.loadProductCategoryDetails();
          this.showNotification('pw.productCategory.successMsg');
        }
      });

    this.productCategoryFacade
      .getproductCategoryEditResponse()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          this.loadProductCategoryDetails();
          this.showNotification('pw.productCategory.editSuccessMsg');
        }
      });

    this.productCategoryFacade
      .getproductCategoryDetailsByproductCategoryCode()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          this.productCategoryDetailsByCode$ = data;
          const dialogRef = this.dialog.open(ProductCategoryDetailsComponent, {
            width: '500px',
            height: 'auto',
            data: this.productCategoryDetailsByCode$
          });

          dialogRef.afterClosed().subscribe(formData => {
            if (formData) {
              this.createProductCategoryFormDetails(formData);
            }
          });
        }
      });
  }

  loadProductCategoryDetails() {
    this.productCategoryFacade.loadProductCategoryDetailsListing(
      this.productCategoryPageEvent
    );
  }

  getProductCategoryCode(productCategoryCode: string) {
    if (productCategoryCode !== ProductCategoryEnum.NEW) {
      this.productCategoryFacade.loadProductCategoryDetailsByproductCategoryCode(
        productCategoryCode
      );
    } else {
      const newFormData: ProductCategoryDetails = {
        productCategoryCode: ProductCategoryEnum.NEW,
        description: '',
        configDetails: {
          isActive: false,
          isAlddFrDmyStdASSM: false
        },
        orgCode: ''
      };
      const dialogRef = this.dialog.open(ProductCategoryDetailsComponent, {
        width: '500px',
        height: 'auto',
        data: newFormData
      });
      dialogRef.afterClosed().subscribe(data => {
        if (data) {
          this.createProductCategoryFormDetails(data);
        }
      });
    }
  }

  createProductCategoryFormDetails(data: any) {
    this.searchForm.reset();
    if (data.mode === ProductCategoryEnum.new) {
      this.productCategoryFacade.saveproductCategoryFormDetails({
        productCategoryCode: data.productCode,
        description: data.description,
        orgCode: data.orgCode,
        configDetails: {
          isActive: data.configDetails.isActive,
          isAlddFrDmyStdASSM: data.configDetails.isAlddFrDmyStdASSM
        }
      });
    } else if (data.mode === ProductCategoryEnum.edit) {
      this.productCategoryFacade.editproductCategoryFormDetails({
        productCategoryCode: data.productCode,
        description: data.description,
        orgCode: data.orgCode,
        configDetails: {
          isActive: data.configDetails.isActive,
          isAlddFrDmyStdASSM: data.configDetails.isAlddFrDmyStdASSM
        }
      });
    }
  }

  ngAfterViewInit(): void {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe(event => {
        const searchValue = this.searchForm.value.searchValue;
        if (searchValue) {
          this.search(searchValue);
        } else {
          this.clearSearch();
        }
      });
  }

  search(searchValue: string) {
    if (!searchValue.search('^[_A-z0-9]*((-|s)*[_A-z0-9])*$')) {
      this.productCategoryFacade.searchProductCategor(searchValue);
    }
  }
  clearSearch() {
    this.searchForm.reset();
    this.loadProductCategoryDetails();
  }

  paginate(pageEvent: PageEvent) {
    this.productCategoryPageEvent = pageEvent;
    this.loadProductCategoryDetails();
  }

  showNotification(key: string) {
    this.overlayNotification.close();

    this.translate
      .get(key)
      .pipe(take(1))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMsg,
            time: 2000,
            hasBackdrop: true
          })
          .events.pipe(take(1))
          .subscribe();
      });
  }

  errorHandler(error: CustomErrors) {
    if (error.code === this.searchErrorCode) {
      return;
    }
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasBackdrop: true,
        hasClose: true,
        error: error
      })
      .events.pipe(take(1))
      .subscribe();
    // (event: OverlayNotificationEventRef) => {
    //   if (event.eventType === OverlayNotificationEventType.CLOSE) {
    //   }
    // }
  }

  backArrow() {
    this.router.navigate([getProductMasterDashboardRouteUrl()]);
    this.productCategoryFacade.resetproductCategoryDialogData();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
  omit_special_char($event: KeyboardEvent) {
    const pattern = /^[-_A-Za-z0-9]$/;
    return pattern.test($event.key);
  }
}
