import { Injectable } from '@angular/core';
import { ApiService } from '@poss-web/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { LoadProductCategoryListingPayload, LoadProductCategoryListingSuccessPayload, SaveProductCategoryFormDetailsPayload } from '../+state/product-category.actions';
import { getProductCategoryDetailsListingUrl, getProductCategoryByProductCategoryCodeUrl, getProductCategorySaveFormDetailsUrl } from '../../../endpoint.constants';
import { ProductCategoryAdaptor } from '../adaptors/product-category.adaptor';
import { ProductCategoryDetails } from '../models/product-category.model';

@Injectable()
export class ProductCategoryService {

  constructor(private apiService: ApiService) { }

  getProductCategoryDetails(loadProductCategoryListingPayload: LoadProductCategoryListingPayload): Observable<LoadProductCategoryListingSuccessPayload> {
    const url = getProductCategoryDetailsListingUrl(loadProductCategoryListingPayload.pageIndex, loadProductCategoryListingPayload.pageSize);
    return this.apiService
      .get(url)
      .pipe(
        map(data => ProductCategoryAdaptor.getProductCategoryDetailsListing(data))
      );
  }

  getProductCategoryByProductCategoryCode(productCategoryCode: string): Observable<ProductCategoryDetails> {
    const url = getProductCategoryByProductCategoryCodeUrl(productCategoryCode);
    return this.apiService.get(url);
  }

  getProductCategorySearchResult(productCategoryCode: string): Observable<ProductCategoryDetails[]> {
    const url = getProductCategoryByProductCategoryCodeUrl(productCategoryCode);
    return this.apiService.get(url).pipe(map(data => ProductCategoryAdaptor.getProductCategoryDetailsSearch(data)));
  }

  saveProductCategoryFormDetails(saveForm: SaveProductCategoryFormDetailsPayload) {
    const url = getProductCategorySaveFormDetailsUrl();
    return this.apiService.post(url, saveForm);
  }

  editProductCategoryFormDetails(editedForm: SaveProductCategoryFormDetailsPayload) {
    const url = getProductCategoryByProductCategoryCodeUrl(editedForm.productCategoryCode);
    return this.apiService.put(url, editedForm);
  }
}
