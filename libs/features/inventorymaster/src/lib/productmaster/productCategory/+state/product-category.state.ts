
import { CustomErrors } from '@poss-web/core';
import { ProductCategoryDetails, ProductCategory } from '../models/product-category.model';


export interface ProductCategoryState {
    error: CustomErrors;
    productCategoryListing: ProductCategory[];
    productCategoryDetails: ProductCategoryDetails;
    totalProductCategoryDetails: number;
    isLoading: boolean;
    saveProductCategoryResponses: ProductCategory;
    editProductCategoryResponses: ProductCategory;

}
