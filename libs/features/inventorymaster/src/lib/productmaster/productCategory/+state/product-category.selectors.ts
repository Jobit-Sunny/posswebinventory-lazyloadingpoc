import { createSelector } from '@ngrx/store';
import { selectInventoryMasters } from '../../../inventorymasters.state';





const selectProductCategoryDetailsListing = createSelector(
    selectInventoryMasters,
    state => state.productCategoryState.productCategoryListing
);

const selectTotalProductCategoryDetailsCount = createSelector(
    selectInventoryMasters,
    state => state.productCategoryState.totalProductCategoryDetails
);


const selectProductCategoryDetailsByProductCategoryCode = createSelector(
    selectInventoryMasters,
    state => state.productCategoryState.productCategoryDetails
);

const selectIsLoading = createSelector(
    selectInventoryMasters,
    state => state.productCategoryState.isLoading
);


const selectError = createSelector(
    selectInventoryMasters,
    state => state.productCategoryState.error
);


const selectSaveproductCategoryFormResponse = createSelector(
  selectInventoryMasters,
  state => state.productCategoryState.saveProductCategoryResponses
);

const selectEditproductCategoryFormResponse = createSelector(
  selectInventoryMasters,
  state => state.productCategoryState.editProductCategoryResponses
);



export const ProductCategorySelectors = {
    selectProductCategoryDetailsListing,
    selectProductCategoryDetailsByProductCategoryCode,
    selectIsLoading,
    selectError,
    selectTotalProductCategoryDetailsCount,
    selectSaveproductCategoryFormResponse,
    selectEditproductCategoryFormResponse
};
