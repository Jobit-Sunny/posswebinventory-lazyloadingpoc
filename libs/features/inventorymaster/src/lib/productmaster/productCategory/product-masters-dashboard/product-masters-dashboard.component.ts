import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { getInventoryMasterRouteUrl, getProductMasterBrandListRouteUrl, getProductMasterSubBrandListRouteUrl, getProductMasterProductCategoryListRouteUrl, getProductMasterCFAProductListRouteUrl, getProductMasterItemListRouteUrl } from '../../../page-route.constants';

@Component({
  selector: 'poss-web-product-masters-dashboard',
  templateUrl: './product-masters-dashboard.component.html',
  styleUrls: ['./product-masters-dashboard.component.scss']
})
export class ProductMastersDashboardComponent implements OnInit {


  brandListUrl = getProductMasterBrandListRouteUrl();
  subBrandListUrl = getProductMasterSubBrandListRouteUrl();
  productCategoryListUrl = getProductMasterProductCategoryListRouteUrl();
  cfaProductListUrl = getProductMasterCFAProductListRouteUrl();
  itemMasterListUrl = getProductMasterItemListRouteUrl();
  constructor(private router: Router) { }

  ngOnInit() { }
  back() {
    this.router.navigate([getInventoryMasterRouteUrl()]);
  }
}
