import { Component, OnInit, Input, EventEmitter, Output, OnDestroy } from '@angular/core';
import { ProductCategoryDetails } from '../../models/product-category.model';
import { Observable, Subject } from 'rxjs';
import { PageEvent } from '@angular/material';
import { takeUntil } from 'rxjs/operators';
import { AppsettingFacade } from '@poss-web/core';

@Component({
  selector: 'poss-web-product-category-items',
  templateUrl: './product-category-items.component.html',
  styleUrls: ['./product-category-items.component.scss']
})
export class ProductCategoryItemsComponent implements OnInit, OnDestroy {
  @Input() productCategoryDetailsList;//: Observable<ProductCategoryDetails>;
  @Input() count: number;
  @Input() pageEvent: PageEvent;
  @Output() productCategoryCode = new EventEmitter<any>();
  @Output() paginator = new EventEmitter<PageEvent>();
  destroy$ = new Subject<null>();
  pageSizeOptions: number[] = [];
  minPageSize = 0;

  constructor(private appSettingFacade: AppsettingFacade) { }

  ngOnInit() {
    this.appSettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.pageSizeOptions = data;
        this.minPageSize = this.pageSizeOptions.reduce((a: number, b: number) =>
          a < b ? a : b
        );
      });
  }

  emitProductCategoryCode(productCategoryCode) {
    this.productCategoryCode.emit(productCategoryCode);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
