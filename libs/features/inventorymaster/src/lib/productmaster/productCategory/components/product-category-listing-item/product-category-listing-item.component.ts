import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProductCategory } from '../../models/product-category.model';

@Component({
  selector: 'poss-web-product-category-listing-item',
  templateUrl: './product-category-listing-item.component.html',
  styleUrls: ['./product-category-listing-item.component.scss']
})
export class ProductCategoryListingItemComponent implements OnInit {

  @Input() productCategoryDetailsList: ProductCategory;
  @Output() productCategoryCode = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  getProductCategoryCode(productCategoryCode: string) {
    this.productCategoryCode.emit(productCategoryCode);
  }

}
