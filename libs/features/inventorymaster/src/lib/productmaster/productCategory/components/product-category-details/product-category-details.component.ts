import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Observable, Subject } from 'rxjs';

import {
  ProductCategoryDetails,
  ProductCategoryEnum
} from '../../models/product-category.model';
import { ProductCategoryMaster } from '../../models/product-category-master.model';
import { TEMPLATE8 } from '@poss-web/shared';
import { ConfirmDialogComponent } from '../../../../master/common/confirm-dialog/confirm-dialog.component';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'poss-web-product-category-details',
  templateUrl: './product-category-details.component.html',
  styleUrls: ['./product-category-details.component.scss']
})
export class ProductCategoryDetailsComponent implements OnInit, OnDestroy {
  dialogData: ProductCategoryDetails;
  destroy$: Subject<null> = new Subject<null>();
  readOnly: boolean;
  productCategoryEnum: ProductCategoryEnum;

  productCategoryCode: string;
  description: string;
  isActive: boolean;
  isAlddFrDmyStdASSM: boolean;

  /// below is dynamic form specific code
  public currentStyle: string[];
  public formFields: any;
  /// above is dynamic form specific code

  constructor(
    public dialogRef: MatDialogRef<ProductCategoryDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    fb: FormBuilder,
    public dialog: MatDialog
  ) {
    this.dialogData = data;
  }

  ngOnInit() {
    if (this.dialogData.productCategoryCode !== ProductCategoryEnum.NEW) {
      this.readOnly = true;
    }

    const form = this.prepareSet();
    this.formFields = this.getInputs(form);
    this.currentStyle = this.getCssProp();
  }

  onCreate() {
    let mode = '';
    if (this.dialogData.productCategoryCode !== ProductCategoryEnum.NEW) {
      mode = ProductCategoryEnum.edit;
    } else {
      mode = ProductCategoryEnum.new;
    }

    this.dialogRef.close({
      productCode: this.productCategoryCode,
      description: this.description,
      configDetails: {
        isActive: this.isActive,
        isAlddFrDmyStdASSM: this.isAlddFrDmyStdASSM
      },
      orgCode: this.dialogData.orgCode,
      mode: mode
    });
  }

  onClose() {
    this.dialogRef.close();
  }

  /// below is dynamic form specific code

  prepareSet() {
    const productCategory = new ProductCategoryMaster(
      1,
      this.dialogData.productCategoryCode === 'NEW'
        ? ''
        : this.dialogData.productCategoryCode,
      this.dialogData.description,
      [
        {
          id: '1',
          name: 'pw.productCategory.isActiveLabel',
          checked: this.dialogData.productCategoryCode === 'NEW' ? true : this.dialogData.configDetails.isActive
        }
      ],
      [
        {
          id: '2',
          name: 'pw.productCategory.isAssmLabel',
          checked: this.dialogData.productCategoryCode === 'NEW' ? true : this.dialogData.configDetails.isAlddFrDmyStdASSM
        }
      ]
    );
    return productCategory;
  }

  getCssProp() {
    const annot = (ProductCategoryDetailsComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }
  public setFormConfig() {
    return {
      formName: 'Product Category Form',
      formDesc: 'Product Category',
      formTemplate: TEMPLATE8
    };
  }
  addButton(formGroup: FormGroup) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        const formValues = formGroup.getRawValue();
        this.productCategoryCode = formValues['1-prodCatCode'];
        this.description = formValues['1-desctiption'];
        this.isActive = formValues['1-IsActive'][0];
        this.isAlddFrDmyStdASSM = formValues['1-IsAllowed'][0];
        this.onCreate();
      }
    });


    // this.tabOne.emit(formData);
  }
  deleteButton() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.ngOnInit();
      }
    });
  }

  public formGroupCreated(formGroup: FormGroup) {
    if (this.dialogData.productCategoryCode !== 'NEW') {
      formGroup.get('1-prodCatCode').disable({ onlySelf: true });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
