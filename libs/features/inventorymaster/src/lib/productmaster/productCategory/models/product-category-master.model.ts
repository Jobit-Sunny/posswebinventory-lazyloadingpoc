import { Validators } from '@angular/forms';
import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';

export class ProductCategoryMaster extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.productCategory.productCategoryCodeField'
  })
  @Validation({ validators: [Validators.required] })
  @Class({ className: ['col'] })
  private prodCatCode: string;

  @FormField({
    fieldType: FormFieldType.TEXT_AREA,
    label: 'pw.productCategory.descriptionField'
  })
  @Validation({ validators: [Validators.required] })
  @Class({ className: ['col-12'] })
  private desctiption: string;

  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Class({ className: ['col-4', 'pl-0'] })
  private IsActive: { id: string; name: string; checked?: boolean }[];

  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Class({ className: ['col-8'] })
  private IsAllowed: { id: string; name: string; checked?: boolean }[];

  constructor(
    id: number,
    prodCatCode: string,
    desctiption: string,
    IsActive: { id: string; name: string; checked?: boolean }[],
    IsAllowed: { id: string; name: string; checked?: boolean }[]
  ) {
    super();
    this.id = id;
    this.prodCatCode = prodCatCode;
    this.desctiption = desctiption;
    this.IsActive = IsActive;
    this.IsAllowed = IsAllowed;
  }
}
