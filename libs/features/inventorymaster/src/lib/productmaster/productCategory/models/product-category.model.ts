

export interface ProductCategoryDetails {
  productCategoryCode: string;
  description: string;
  configDetails: { isActive: boolean,
                   isAlddFrDmyStdASSM: boolean };
  orgCode: string;
}

export interface ProductCategory {
  productCategoryCode: string;
  description: string;
}

export enum ProductCategoryEnum {
  NEW = 'NEW',
  new = 'new',
  edit = 'edit'
}

