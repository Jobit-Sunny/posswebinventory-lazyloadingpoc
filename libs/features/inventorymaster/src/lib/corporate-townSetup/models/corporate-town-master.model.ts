import { Validators } from '@angular/forms';
import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';

export class CorporateTownModel extends DynamicFormFieldsBuilder {
  private id: number;

  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'description',
      valueKey: 'stateCode',
      selectedKey: 'selected'
    },
    label: 'Select State'
  })
  @Validation({ validators: [Validators.required] })
  @Class({ className: ['col-12'] })
  private state: {
    stateCode: number;
    description: string;
  }[];

  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'description',
      valueKey: 'regionCode',
      selectedKey: 'selected'
    },
    label: 'Select Region'
  })
  @Validation({ validators: [Validators.required] })
  @Class({ className: ['col-12'] })
  private region: { regionCode: string; description: string }[];

  @FormField({
    fieldType: FormFieldType.TEXT_AREA,
    label: 'Town name',
    validationErrorMessages: [{ errorType: 'pattern',
                               errorMessage: 'pw.corporateTown.townNamelengthError' }]
                              })
  @Validation({ validators: [Validators.required,
                             Validators.pattern('^[a-zA-Z_ ]{3,100}$') ] })
  @Class({ className: ['col-12'] })
  private townName: string;

  constructor(
    id: number,
    state: { stateCode: number; description: string; selected?: boolean }[],
    region: { regionCode: string; description: string; selected?: boolean }[],
    townName: string
  ) {
    super();
    this.id = id;
    this.state = state;
    this.region = region;
    this.townName = townName;
  }
}
