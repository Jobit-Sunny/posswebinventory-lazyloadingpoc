export interface CorporateTown {
  townCode: number;
  stateCode: number;
  regionCode: string;
  description: string;
  markupFactor: number;
  markupAmount: number;
  isActive: boolean;
}

export interface StateDetails {
  countryCode: number;
  description: string;
  isActive: boolean;
  stateCode: number;
}

export interface RegionDetails {
  description: string;
  isActive: boolean;
  parentRegionCode: string;
  regionCode: string;
}

export enum CorporateTownEnum {
  NEW = 'NEW',
  new = 'new',
  edit = 'edit'
}
