import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import {
  CorporateTown,
  StateDetails,
  RegionDetails,
  CorporateTownEnum
} from '../../models/corporate-town.model';

import { CorporateTownModel } from '../../models/corporate-town-master.model';
import { TEMPLATE8, HelperFunctions } from '@poss-web/shared';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'poss-web-corporate-town-details',
  templateUrl: './corporate-town-details.component.html',
  styleUrls: ['./corporate-town-details.component.scss']
})
export class CorporateTownDetailsComponent implements OnInit {
  dialogData: CorporateTown;
  corporateTownForm: FormGroup;
  stateDropdown: StateDetails[];
  readOnly: boolean;
  regionDropdown: RegionDetails[];

  public currentStyle: string[];
  public formFields: any;
  state: any;
  description: any;
  townName: any;
  region: any;
  townCode: any;
  destroy$: Subject<null> = new Subject<null>();

  constructor(
    public dialogRef: MatDialogRef<CorporateTownDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    fb: FormBuilder,
    private hf: HelperFunctions,
    public dialog: MatDialog
  ) {
    this.dialogData = data.data;
    this.corporateTownForm = fb.group({
      townCode: [null],
      regionCode: ['', Validators.required],
      stateCode: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  ngOnInit() {
    if (this.data.mode === 'edit') {
      this.readOnly = true;
      this.corporateTownForm.patchValue({
        regionCode: this.dialogData.regionCode,
        townCode: this.dialogData.townCode,
        stateCode: this.dialogData.stateCode,
        description: this.dialogData.description
      });
    }

    const form = this.prepareSet(
      this.dialogData,
      this.data.state,
      this.data.region
    );
    this.formFields = this.getInputs(form);
    this.currentStyle = this.getCssProp();
  }

  onSave() {
    let mode = '';
    if (this.data.mode === 'edit') {
      mode = CorporateTownEnum.edit;
    } else {
      mode = CorporateTownEnum.new;
    }
    if (mode !== 'edit') {
      this.dialogRef.close({
        description: this.townName,
        isActive: this.dialogData.isActive,
        markupAmount: this.dialogData.markupAmount,
        markupFactor: this.dialogData.markupFactor,
        regionCode: this.region,
        stateCode: this.state,
        townCode: this.dialogData.townCode,
        mode: mode
      });
    } else if (
      this.dialogData.regionCode !== this.region &&
      this.dialogData.stateCode !== this.state &&
      this.dialogData.description !== this.townName
    ) {
      this.dialogRef.close({
        townCode: this.dialogData.townCode,
        regionCode: this.region,
        stateCode: this.state,
        description: this.townName
      });
    } else if (
      this.dialogData.regionCode !== this.region &&
      this.dialogData.stateCode !== this.state
    ) {
      this.dialogRef.close({
        townCode: this.dialogData.townCode,
        stateCode: this.state,
        regionCode: this.region
      });
    } else if (
      this.dialogData.regionCode !== this.region &&
      this.dialogData.description !== this.townName
    ) {
      this.dialogRef.close({
        townCode: this.dialogData.townCode,
        description: this.townName,
        regionCode: this.region
      });
    } else if (
      this.dialogData.stateCode !== this.state &&
      this.dialogData.description !== this.townName
    ) {
      this.dialogRef.close({
        townCode: this.dialogData.townCode,
        description: this.townName,
        stateCode: this.state
      });
    } else if (this.dialogData.regionCode !== this.region) {
      this.dialogRef.close({
        townCode: this.dialogData.townCode,
        regionCode: this.region
      });
    } else if (this.dialogData.stateCode !== this.state) {
      this.dialogRef.close({
        townCode: this.dialogData.townCode,
        stateCode: this.state
      });
    } else if (this.dialogData.description !== this.townName) {
      this.dialogRef.close({
        townCode: this.dialogData.townCode,
        description: this.townName
      });
    }
  }

  onClose() {
    this.dialogRef.close();
  }
  onClear() {}

  prepareSet(data, state, region) {
    if (data.stateCode) {
      state = this.hf.patchValue(
        state,
        'stateCode',
        'selected',
        data.stateCode,
        true
      );
    }

    if (data.regionCode) {
      region = this.hf.patchValue(
        region,
        'regionCode',
        'selected',
        data.regionCode,
        true
      );
    }

    const townDetails = new CorporateTownModel(
      1,
      state,
      region,
      this.dialogData.description
    );

    return townDetails;
  }
  getCssProp() {
    const annot = (CorporateTownDetailsComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }
  public setFormConfig() {
    return {
      formName: 'Corporate Town Details Form',
      formDesc: 'Corporate Town',
      formTemplate: TEMPLATE8
    };
  }
  addButton(formGroup: FormGroup) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(result => {
        if (result) {
          const formValues = formGroup.getRawValue();
          this.state = formValues['1-state'];
          this.region = formValues['1-region'];
          this.townName = formValues['1-townName'];
          this.onSave();
        }
      });
  }
  deleteButton() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ngOnInit();
      }
    });
  }
}
