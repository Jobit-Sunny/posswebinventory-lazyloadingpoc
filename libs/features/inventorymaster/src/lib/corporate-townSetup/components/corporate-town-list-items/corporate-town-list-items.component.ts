import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { PageEvent } from '@angular/material';
import { Subject } from 'rxjs';
import { AppsettingFacade } from '@poss-web/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'poss-web-corporate-town-list-items',
  templateUrl: './corporate-town-list-items.component.html',
  styleUrls: ['./corporate-town-list-items.component.scss']
})
export class CorporateTownListItemsComponent implements OnInit, OnDestroy {


  @Input() corporateTownDetailsList;
  @Input() count;
  @Input() pageEvent: PageEvent;
  @Output() corporateTownCode = new EventEmitter<any>();
  destroy$ = new Subject<null>();
  pageSizeOptions: number[] = [];
  @Output() paginator = new EventEmitter<PageEvent>();
  minPageSize = 0;
  //@Input() searchElement;

  constructor(private appSettingFacade: AppsettingFacade) { }


  ngOnInit() {
    this.appSettingFacade
    .getPageSizeOptions()
    .pipe(takeUntil(this.destroy$))
    .subscribe(data => {
      this.pageSizeOptions = data;
      this.minPageSize = this.pageSizeOptions.reduce((a: number, b: number) =>
        a < b ? a : b
      );
    });
  }

  emitCorporateTownCode(Code) {
    this.corporateTownCode.emit(Code);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
