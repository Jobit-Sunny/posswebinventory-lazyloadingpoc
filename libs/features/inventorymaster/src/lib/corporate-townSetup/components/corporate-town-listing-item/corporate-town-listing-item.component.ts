import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'poss-web-corporate-town-listing-item',
  templateUrl: './corporate-town-listing-item.component.html',
  styleUrls: ['./corporate-town-listing-item.component.scss']
})
export class CorporateTownListingItemComponent implements OnInit {

  @Input() corporateTownItem;
  @Output() corporateCode = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {}

  onEdit(townCode: string){
    this.corporateCode.emit(townCode);
  }
}
