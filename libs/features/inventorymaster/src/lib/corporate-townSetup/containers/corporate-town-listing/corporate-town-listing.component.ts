import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy
} from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject, fromEvent } from 'rxjs';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import { takeUntil, debounceTime } from 'rxjs/operators';
import { PageEvent, MatDialog } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import {
  CorporateTown,
  StateDetails,
  RegionDetails
} from '../../models/corporate-town.model';
import { CorporateTownFacade } from '../../+state/corporate-town.facade';
import { CorporateTownDetailsComponent } from '../../components/corporate-town-details/corporate-town-details.component';
import { TranslateService } from '@ngx-translate/core';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationEventType
} from '@poss-web/shared';
import { getLocationMasterDashboardRouteUrl } from '../../../page-route.constants';

@Component({
  selector: 'poss-web-corporate-town-listing',
  templateUrl: './corporate-town-listing.component.html',
  styleUrls: ['./corporate-town-listing.component.scss']
})
export class CorporateTownListingComponent
  implements OnInit, AfterViewInit, OnDestroy {
  corporateTownDetailsList$: Observable<CorporateTown[]>;
  destroy$ = new Subject<null>();
  corporateTownCount$: Observable<number>;
  isLoading$: Observable<boolean>;
  townDetailsByCode$: CorporateTown;
  townSaveResponse$: Observable<CorporateTown>;
  townEditResponse$: Observable<CorporateTown>;
  isSearching$: Observable<boolean>;
  stateDropdown: StateDetails[];
  regionDropdown: RegionDetails[];
  searchElement: CorporateTown[] = [];
  noSearchFound: boolean;
  totalElements: number;
  filterTownDetailsList$: Observable<CorporateTown[]>;
  corporateTownError: CustomErrors = null;

  constructor(
    public router: Router,
    public dialog: MatDialog,
    private appsettingFacade: AppsettingFacade,
    private corporateTownFacade: CorporateTownFacade,
    private translate: TranslateService,
    private overlayNotification: OverlayNotificationService
  ) { }

  corporateTownPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };

  statePageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };

  filterPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };

  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;
  searchForm = new FormGroup({
    searchValue: new FormControl()
  });

  ngOnInit() {
    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        const pageSize = JSON.parse(data);
        this.corporateTownPageEvent.pageSize = pageSize;
        this.loadCorporateTownDetails();
      });

    this.corporateTownFacade.resetTownDialogData();
    this.isSearching$ = this.corporateTownFacade.getIsSerchElements();
    this.isLoading$ = this.corporateTownFacade.getisCorporateTownListingLoading();
    this.corporateTownFacade.loadStateListing(this.statePageEvent);
    this.corporateTownFacade.loadRegionListing(this.statePageEvent);
    this.corporateTownDetailsList$ = this.corporateTownFacade.getCorporateTownDetailsListing();
    this.corporateTownCount$ = this.corporateTownFacade.getTotalCorporateTownDetails();

    this.corporateTownCount$.subscribe(data => {
      this.filterPageEvent.pageSize = data;
    });

    this.corporateTownFacade
      .getTownDetailsSaveResponse()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          const key = 'pw.corporateTown.successMsg';
          this.translate
            .get(key)
            .pipe(takeUntil(this.destroy$))
            .subscribe((translatedMessage: string) => {
              this.showTimerError(translatedMessage);
              this.loadCorporateTownDetails();
            });
        }
      });

    this.corporateTownFacade
      .getTownDetailsEditResponse()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          const key = 'pw.corporateTown.editSuccessMsg';
          this.translate
            .get(key)
            .pipe(takeUntil(this.destroy$))
            .subscribe((translatedMessage: string) => {
              this.showTimerError(translatedMessage);
              if (this.searchForm.value.searchValue) {
                this.search(this.searchForm.value.searchValue);
              } else {
                this.loadCorporateTownDetails();
              }
            });
        }
      });

    this.corporateTownFacade
      .getTownDetailsByTownCode()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          this.townDetailsByCode$ = data;
          if (this.corporateTownError === null) {
            const dialogRef = this.dialog.open(CorporateTownDetailsComponent, {
              width: '500px',
              height: 'auto',
              data: {
                data,
                mode: 'edit',
                state: this.stateDropdown,
                region: this.regionDropdown
              }
            });
            dialogRef.afterClosed().subscribe(formData => {
              if (formData) {
                this.corporateTownFormDetails(formData);
              }
            });
          } else {
            this.corporateTownFacade
              .getError()
              .pipe(takeUntil(this.destroy$))
              .subscribe((error: CustomErrors) => {
                this.error(error);
              });
          }
        }
      });

    this.corporateTownFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.corporateTownError = error;
          this.errorHandler(error);
        }
      });

    this.corporateTownFacade.getStateDetailsListing().subscribe(result => {
      this.stateDropdown = result;
    });

    this.corporateTownFacade.getRegionDetailsListing().subscribe(result => {
      this.regionDropdown = result;
    });
  }

  error(error: CustomErrors) {
    if (error) {
      this.corporateTownError = null;
      this.errorHandler(error);
    }
  }


  ngAfterViewInit(): void {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe(event => {
        const searchValue = this.searchForm.value.searchValue;
        if (searchValue) {
          this.search(searchValue);
        } else {
          this.clearSearch();
        }
      });
  }

  search(searchValue: string) {
    if (searchValue !== null) {
      this.corporateTownFacade.loadCorporateTownListing(this.filterPageEvent);
      this.filterTownDetailsList$ = this.corporateTownFacade.getCorporateTownDetailsListing();
      this.filterTownDetailsList$.subscribe(data => {
        if (data.length > 5) {
          const Townlist = data;
          if (Townlist && searchValue !== null) {
            this.searchElement = Townlist.filter(
              bin => bin.description.toLowerCase() === searchValue.toLowerCase()
            );
            if (searchValue !== null && this.searchElement.length === 0) {
              searchValue = null;
              this.searchElement = [];
              this.noSearchFound = true;
            } else if (this.searchElement.length > 0) {
              this.noSearchFound = false;
              searchValue = null;
            }
          }
        }
      });
    }

    // this.corporateTownFacade.searchCorporateTown(searchValue);
  }

  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error,
        hasBackdrop: true
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => { });
  }

  backArrow() {
    this.corporateTownFacade.resetTownDialogData();
    this.router.navigate([getLocationMasterDashboardRouteUrl()]);
  }

  getCorporateTownCode(townCode: string) {
    if (townCode !== 'NEW') {
      const code = Number(townCode);
      this.corporateTownFacade.loadTownDetailsByTownCode(code);
    } else {
      const data: CorporateTown = {
        description: '',
        isActive: true,
        markupAmount: 0,
        markupFactor: 0,
        regionCode: null,
        stateCode: 0,
        townCode: null
      };


      if (this.corporateTownError === null) {
        const dialogRef = this.dialog.open(CorporateTownDetailsComponent, {
          width: '500px',
          height: 'auto',
          data: {
            data,
            mode: 'new',
            state: this.stateDropdown,
            region: this.regionDropdown
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.corporateTownFormDetails(result);
          }
        });
      } else {
        this.corporateTownFacade
          .getError()
          .pipe(takeUntil(this.destroy$))
          .subscribe((error: CustomErrors) => {
            this.error(error);
          });
      }

    }
  }

  paginate(pageEvent: PageEvent) {
    this.corporateTownPageEvent = pageEvent;
    this.loadCorporateTownDetails();
  }

  loadCorporateTownDetails() {
    this.corporateTownFacade.loadCorporateTownListing(
      this.corporateTownPageEvent
    );
  }

  corporateTownFormDetails(data: any) {
    if (data.mode === 'new') {
      const formData = {
        description: data.description,
        isActive: data.isActive,
        markupAmount: data.markupAmount,
        markupFactor: data.markupAmount,
        regionCode: data.regionCode,
        stateCode: data.stateCode,
        townCode: null
      };
      this.corporateTownFacade.saveTownFormDetails(formData);
    } else {
      this.corporateTownFacade.editTownFormDetails(data);
    }
  }

  showTimerError(translatedMessage: string) {
    this.overlayNotification.show({
      type: OverlayNotificationType.TIMER,
      message: translatedMessage,
      hasClose: true,
      hasBackdrop: true
    });
  }
  clearSearch() {
    this.noSearchFound = false;
    this.searchElement = [];
    this.searchForm.reset();
    this.corporateTownFacade.resetTownDialogData();
    this.loadCorporateTownDetails();
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  omit_special_char($event: KeyboardEvent) {
    const pattern = /^[-_A-Za-z0-9]$/;
    return pattern.test($event.key);
  }
}
