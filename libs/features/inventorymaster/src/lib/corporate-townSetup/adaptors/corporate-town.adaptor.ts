import { LoadCorporateTownListingSuccessPayload, LoadStateListingSuccessPayload, LoadRegionListingSuccessPayload } from '../+state/corporate-town.actions';
import { CorporateTown, StateDetails, RegionDetails } from '../models/corporate-town.model';


export class CorporateTownAdaptor {
  static corporateTownDetailsListing: LoadCorporateTownListingSuccessPayload;
  static stateDetailsListing: LoadStateListingSuccessPayload;
  static regionDetailsListing: LoadRegionListingSuccessPayload;


static getCorporateTownDetailsListing(
  data: any
): LoadCorporateTownListingSuccessPayload {
  const corporateTownDetailsListing: CorporateTown[] = [];
  for (const listItem of data.results) {
    corporateTownDetailsListing.push({
      townCode: listItem.townCode,
      stateCode: listItem.stateCode,
      regionCode: listItem.regionCode,
      description: listItem.description,
      markupFactor: listItem.markupFactor,
      markupAmount: listItem.markupAmount,
      isActive: listItem.isActive
    });
  }
  this.corporateTownDetailsListing = {
    corporateTownDetailsListing: corporateTownDetailsListing,
    totalElements: data.totalElements
  };
  return this.corporateTownDetailsListing;
  }


  static getStateDetailsListing(
    data: any
  ): LoadStateListingSuccessPayload {
    const stateDetailsListing: StateDetails[] = [];
    for (const listItem of data.results) {
      stateDetailsListing.push({
        countryCode: listItem.countryCode,
        description: listItem.description,
        isActive: listItem.isActive,
        stateCode: listItem.stateCode
      });
    }
    this.stateDetailsListing = {
      stateDetailsListing: stateDetailsListing,
      totalElements: data.totalElements
    };
    return this.stateDetailsListing;
    }


    static getRegionDetailsListing(
      data: any
    ): LoadRegionListingSuccessPayload {
      const regionDetailsListing: RegionDetails[] = [];
      for (const listItem of data.results) {
        regionDetailsListing.push({
          description: listItem.description,
          isActive: listItem.isActive,
          parentRegionCode: listItem.parentRegionCode,
          regionCode: listItem.regionCode
        });
      }
      this.regionDetailsListing = {
        regionDetailsListing: regionDetailsListing,
        totalElements: data.totalElements
      };
      return this.regionDetailsListing;
      }

      static getSearchDetailsListing(
        data: any
      ): LoadCorporateTownListingSuccessPayload {
        let searchList: LoadCorporateTownListingSuccessPayload;
        const corporateTownListing: CorporateTown[] = [];
          corporateTownListing.push({
            townCode: data.townCode,
            stateCode: data.stateCode,
            regionCode: data.regionCode,
            description: data.description,
            markupFactor: data.markupFactor,
            markupAmount: data.markupAmount,
            isActive: data.isActive
          });

        let totalElements;
        if (data) {
          totalElements = 1;
        } else {
          totalElements = 0;
        }
        searchList = {
          corporateTownDetailsListing: corporateTownListing,
          totalElements: totalElements
        };
        return searchList;
      }


}
