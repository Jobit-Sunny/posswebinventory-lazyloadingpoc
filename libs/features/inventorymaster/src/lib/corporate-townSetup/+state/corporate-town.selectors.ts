
import { createSelector } from '@ngrx/store';
import { selectInventoryMasters } from '../../inventorymasters.state';
import { corporateTownSelector } from './corporate-town.entity';



const selectCorporateTownDetailsListing = createSelector(
  selectInventoryMasters,
  state => state.corporateTownState.corporateTownDetailsListing
);


const selectLoadedCorporateListing = createSelector(
  selectCorporateTownDetailsListing,
   corporateTownSelector.selectAll
);

const selectTotalCorporateTownDetailsCount = createSelector(
  selectInventoryMasters,
  state => state.corporateTownState.totalCorporateTownDetails
);

const selectStateDetails = createSelector(
  selectInventoryMasters,
  state => state.corporateTownState.stateDetails
);

const selectRegionDetails = createSelector(
  selectInventoryMasters,
  state => state.corporateTownState.regionDetails
);

const selectIsCorporateTownListingLoading = createSelector(
  selectInventoryMasters,
  state => state.corporateTownState.isCorporateTownLoading
);

const selectTownDetailsByTownCode = createSelector(
  selectInventoryMasters,
  state => state.corporateTownState.townDetailsByTownCode
);

const selectSaveTownDetailsFormResponse = createSelector(
  selectInventoryMasters,
  state => state.corporateTownState.saveTownDetailsResponses
);

const selectEditTownDetailsFormResponse = createSelector(
  selectInventoryMasters,
  state => state.corporateTownState.editTownDetailsResponses
);

const selectError = createSelector(
  selectInventoryMasters,
  state => state.corporateTownState.error
  );

  const selectIssearchElements=createSelector(
    selectInventoryMasters,
    state=>state.corporateTownState.isSearchElements
  )

export const CorporateTownSelectors = {
  selectLoadedCorporateListing,
  selectTotalCorporateTownDetailsCount,
  selectStateDetails,
  selectIsCorporateTownListingLoading,
  selectTownDetailsByTownCode,
  selectSaveTownDetailsFormResponse,
  selectEditTownDetailsFormResponse,
  selectError,
  selectRegionDetails,
  selectIssearchElements
}
