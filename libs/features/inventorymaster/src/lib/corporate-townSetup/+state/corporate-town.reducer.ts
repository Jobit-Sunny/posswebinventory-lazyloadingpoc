import { CorporateTownState } from './corporate-town.state';
import { CorporateTownActions, CorporateTownActionTypes } from './corporate-town.actions';
import { corporateTownAdapter } from './corporate-town.entity';

const initialState: CorporateTownState = {
  corporateTownDetailsListing: corporateTownAdapter.getInitialState(),
  totalCorporateTownDetails: 0,
  error: null,
  stateDetails: null,
  isCorporateTownLoading: false,
  townDetailsByTownCode: null,
  saveTownDetailsResponses: null,
  editTownDetailsResponses: null,
  regionDetails: null,
  isSearchElements: true
};

export function CorporateTownReducer(
  state: CorporateTownState = initialState,
  action: CorporateTownActions
): CorporateTownState {
  switch (action.type) {
    case CorporateTownActionTypes.LOAD_CORPORATE_TOWN:
    case CorporateTownActionTypes.LOAD_STATE_DETAILS:
    case CorporateTownActionTypes.LOAD_CORPORATE_TOWN_DETAILS_BY_TOWNCODE:
    case CorporateTownActionTypes.SAVE_CORPORATE_TOWN:
    case CorporateTownActionTypes.EDIT_CORPORATE_TOWN:
    case CorporateTownActionTypes.LOAD_REGION_DETAILS:

      return {
        ...state,
        isCorporateTownLoading: true
      };

    case CorporateTownActionTypes.LOAD_CORPORATE_TOWN_SUCCESS:
    return {
        ...state,
       // corporateTownDetailsListing: action.payload.corporateTownDetailsListing,
        totalCorporateTownDetails: action.payload.totalElements,
        isCorporateTownLoading: false,
        corporateTownDetailsListing: corporateTownAdapter.addAll(
          action.payload.corporateTownDetailsListing,
          state.corporateTownDetailsListing
        ),
        isSearchElements: true
      };

    case CorporateTownActionTypes.LOAD_CORPORATE_TOWN_FAILURE:
    case CorporateTownActionTypes.LOAD_STATE_DETAILS_FAILURE:
    case CorporateTownActionTypes.LOAD_CORPORATE_TOWN_DETAILS_BY_TOWNCODE_FAILURE:
    case CorporateTownActionTypes.SAVE_CORPORATE_TOWN_FAILURE:
    case CorporateTownActionTypes.EDIT_CORPORATE_TOWN_FAILURE:
    case CorporateTownActionTypes.LOAD_REGION_DETAILS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isCorporateTownLoading: false
      };

    // case CorporateTownActionTypes.LOAD_STATE_DETAILS:
    //   return {
    //     ...state,
    //     isCorporateTownLoading: true
    //   };
    case CorporateTownActionTypes.LOAD_STATE_DETAILS_SUCCESS:
      return {
        ...state,
        stateDetails: action.payload.stateDetailsListing,
        isCorporateTownLoading: false
      };
    // case CorporateTownActionTypes.LOAD_STATE_DETAILS_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload,
    //     isCorporateTownLoading: false
    //   };

    // case CorporateTownActionTypes.LOAD_CORPORATE_TOWN_DETAILS_BY_TOWNCODE:
    //   return {
    //     ...state,
    //     isCorporateTownLoading: true
    //   };

    case CorporateTownActionTypes.LOAD_CORPORATE_TOWN_DETAILS_BY_TOWNCODE_SUCCESS:
      return {
        ...state,
        townDetailsByTownCode: action.payload,
        isCorporateTownLoading: false
      };

    // case CorporateTownActionTypes.LOAD_CORPORATE_TOWN_DETAILS_BY_TOWNCODE_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload,
    //     isCorporateTownLoading: false
    //   };

    // case CorporateTownActionTypes.SAVE_CORPORATE_TOWN:
    //   return {
    //     ...state,
    //     isCorporateTownLoading: true
    //   };

    case CorporateTownActionTypes.SAVE_CORPORATE_TOWN_SUCCESS:
      return {
        ...state,
        isCorporateTownLoading: false,
        saveTownDetailsResponses: action.payload
      };

    // case CorporateTownActionTypes.SAVE_CORPORATE_TOWN_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload,
    //     isCorporateTownLoading: false
    //   };

    case CorporateTownActionTypes.RESET_CORPORATE_TOWN_DIALOG_DATA:
      return {
        ...state,
        isCorporateTownLoading: false,
        //corporateTownDetailsListing: null,
        townDetailsByTownCode: null,
        saveTownDetailsResponses: null,
        editTownDetailsResponses: null,
        error: null
      };



      // case CorporateTownActionTypes.EDIT_CORPORATE_TOWN:
      // return {
      //   ...state,
      //   isCorporateTownLoading: true
      // };

    case CorporateTownActionTypes.EDIT_CORPORATE_TOWN_SUCCESS:
      return {
        ...state,
        isCorporateTownLoading: false,
        editTownDetailsResponses: action.payload
      };
    // case CorporateTownActionTypes.EDIT_CORPORATE_TOWN_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload,
    //     isCorporateTownLoading: false
    //   };



      // case CorporateTownActionTypes.LOAD_REGION_DETAILS:
      //   return {
      //     ...state,
      //     isCorporateTownLoading: true
      //   };
      case CorporateTownActionTypes.LOAD_REGION_DETAILS_SUCCESS:
        return {
          ...state,
          regionDetails: action.payload.regionDetailsListing,
          isCorporateTownLoading: false
        };
      // case CorporateTownActionTypes.LOAD_REGION_DETAILS_FAILURE:
      //   return {
      //     ...state,
      //     error: action.payload,
      //     isCorporateTownLoading: false
      //   };


        case CorporateTownActionTypes.SEARCH_CORPORATETOWN_BY_CODE:
          return {
            ...state,
            error: null,
            // hasSearched: false,
            // isSearchElements: true
          };
        case CorporateTownActionTypes.SEARCH_CORPORATETOWN_BY_CODE_SUCCESS:
        return {
            ...state,
            //searchBinGroup: action.payload.searchBinGroupListing,
            error: null,
           // hasSearched: true,
           totalCorporateTownDetails: action.payload.totalElements,
            isSearchElements: true,
            corporateTownDetailsListing: corporateTownAdapter.addAll(
              action.payload.corporateTownDetailsListing,
              state.corporateTownDetailsListing
            )
          };

        case CorporateTownActionTypes.SEARCH_CORPORATETOWN_BY_CODE_FAILURE:
          return {
            ...state,
            error: action.payload,
            isSearchElements: false,
            corporateTownDetailsListing: corporateTownAdapter.removeAll(
                                        state.corporateTownDetailsListing)
          };

    default:
      return state;
  }
}
