import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { CorporateTownSelectors } from './corporate-town.selectors';
import { LoadCorporateTownListingPayload } from './corporate-town.actions';
import * as CorporateTownActions from './corporate-town.actions';
import { State } from '../../inventorymasters.state';
import { CorporateTown } from '../models/corporate-town.model';

@Injectable()
export class CorporateTownFacade {
  constructor(private store: Store<State>) {}

  private corporateTownListing$ = this.store.select(
    CorporateTownSelectors.selectLoadedCorporateListing
  );
  private totalCorporateTown$ = this.store.select(
    CorporateTownSelectors.selectTotalCorporateTownDetailsCount
  );

  private stateListing$ = this.store.select(
    CorporateTownSelectors.selectStateDetails
  );

  private regionListing$ = this.store.select(
    CorporateTownSelectors.selectRegionDetails
  );

  private isCorporateTownLoading$ = this.store.select(
    CorporateTownSelectors.selectIsCorporateTownListingLoading
  );

  private townDetailsByTownCode$ = this.store.select(
    CorporateTownSelectors.selectTownDetailsByTownCode
  );

  private isTownDetailsSaved$ = this.store.select(
    CorporateTownSelectors.selectSaveTownDetailsFormResponse
  );

  private isTownDetailsEdited$ = this.store.select(
    CorporateTownSelectors.selectEditTownDetailsFormResponse
  );

  private corporateError$ = this.store.select(
    CorporateTownSelectors.selectError
  );


  private isSearchElements$ = this.store.select(
    CorporateTownSelectors.selectIssearchElements
  );



  getCorporateTownDetailsListing() {
    return this.corporateTownListing$;
  }

  getTotalCorporateTownDetails() {
    return this.totalCorporateTown$;
  }

  getStateDetailsListing() {
    return this.stateListing$;
  }

  getRegionDetailsListing() {
    return this.regionListing$;
  }

  getisCorporateTownListingLoading() {
    return this.isCorporateTownLoading$;
  }


  getTownDetailsByTownCode() {
    return this.townDetailsByTownCode$;
  }

  getTownDetailsSaveResponse() {
    return this.isTownDetailsSaved$;
  }

  getTownDetailsEditResponse() {
    return this.isTownDetailsEdited$;
  }

  getError() {
    return this.corporateError$;
  }


  getIsSerchElements() {
    return this.isSearchElements$;
  }

  resetTownDialogData() {
    this.store.dispatch(
      new CorporateTownActions.ResetTownDialog()
      );
    }


  loadCorporateTownListing(
    loadCorporateTownListingPayload: LoadCorporateTownListingPayload
  ) {
    this.store.dispatch(
      new CorporateTownActions.LoadCorporateTownDetails(
        loadCorporateTownListingPayload
      )
    );
  }

  loadStateListing(
    loadStateListingPayload: LoadCorporateTownListingPayload
  ) {
    this.store.dispatch(
      new CorporateTownActions.LoadStateDetails(
        loadStateListingPayload
      )
    );
  }

  loadRegionListing(
    loadRegionListingPayload: LoadCorporateTownListingPayload
  ) {
    this.store.dispatch(
      new CorporateTownActions.LoadRegionDetails(
        loadRegionListingPayload
      )
    );
  }



  loadTownDetailsByTownCode(townCode: number) {
    this.store.dispatch(
      new CorporateTownActions.LoadTownDetailsByTownCode(
        townCode
      )
    );
  }


  saveTownFormDetails(saveFormDetails: CorporateTown){
    this.store.dispatch(
      new CorporateTownActions.SaveTownFormDetails(saveFormDetails)
    );
   }

  editTownFormDetails(saveFormDetails: CorporateTown){
    this.store.dispatch(
      new CorporateTownActions.EditTownFormDetails(saveFormDetails)
    );
  }


  searchCorporateTown(corporateTownCode: string){
    this.store.dispatch(new CorporateTownActions.SearchCorporateTownCode(corporateTownCode));
  }


}
