import { CorporateTown, StateDetails, RegionDetails } from '../models/corporate-town.model';
import { CustomErrors } from '@poss-web/core';
import { CorporateTownEntity } from './corporate-town.entity';

export interface CorporateTownState {
  corporateTownDetailsListing: CorporateTownEntity;
  totalCorporateTownDetails: number;
  error: CustomErrors;
  stateDetails: StateDetails[];
  regionDetails: RegionDetails[];
  isCorporateTownLoading: boolean;
  townDetailsByTownCode: CorporateTown;
  saveTownDetailsResponses: CorporateTown;
  editTownDetailsResponses: CorporateTown;
  isSearchElements: boolean
}
