import { Injectable } from '@angular/core';
import { DataPersistence } from '@nrwl/angular';
import { NotificationService, CustomErrors, CustomErrorAdaptor } from '@poss-web/core';
import { CorporateTownService } from '../services/corporate-town.service.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Effect } from '@ngrx/effects';
import {
  CorporateTownActionTypes,
  LoadCorporateTownListingSuccessPayload,
  LoadStateListingSuccessPayload,
  LoadRegionListingSuccessPayload
} from './corporate-town.actions';

import * as CorporateTownActions from './corporate-town.actions';
import { map } from 'rxjs/operators';
import { CorporateTown } from '../models/corporate-town.model';

@Injectable()
export class CorporateTownEffect {
  constructor(
    private dataPersistence: DataPersistence<any>,
    private notificationService: NotificationService,
    private corporateTownService: CorporateTownService
  ) {}

  @Effect()
  loadCorporateTownDetails$ = this.dataPersistence.fetch(CorporateTownActionTypes.LOAD_CORPORATE_TOWN, {
    run: (action: CorporateTownActions.LoadCorporateTownDetails) => {
      return this.corporateTownService
        .getCorporateTownDetails(action.payload)
        .pipe(
          map(
            (corporateTown: LoadCorporateTownListingSuccessPayload) =>
              new CorporateTownActions.LoadCorporateTownDetailsSuccess(corporateTown)
          )
        );
    },
    onError: (action: CorporateTownActions.LoadCorporateTownDetails, error: HttpErrorResponse) => {
      return new CorporateTownActions.LoadCorporateTownDetailsFailure(this.errorHandler(error));
    }
  });

  @Effect()
  loadStatesDetails$ = this.dataPersistence.fetch(CorporateTownActionTypes.LOAD_STATE_DETAILS, {
    run: (action: CorporateTownActions.LoadStateDetails) => {
      return this.corporateTownService
        .getStateDetails(action.payload)
        .pipe(
          map(
            (statesList: LoadStateListingSuccessPayload) => new CorporateTownActions.LoadStateDetailsSuccess(statesList)
          )
        );
    },
    onError: (action: CorporateTownActions.LoadStateDetails, error: HttpErrorResponse) => {
      return new CorporateTownActions.LoadStateDetailsFailure(this.errorHandler(error));
    }
  });

  @Effect()
  loadTownDetailsByTownCode$ = this.dataPersistence.fetch(
    CorporateTownActionTypes.LOAD_CORPORATE_TOWN_DETAILS_BY_TOWNCODE,
    {
      run: (action: CorporateTownActions.LoadTownDetailsByTownCode) => {
        return this.corporateTownService
          .getTownDetailsByTownCode(action.payload)
          .pipe(
            map(
              (townDetailsByTownCode: CorporateTown) =>
                new CorporateTownActions.LoadTownDetailsByTownCodeSuccess(townDetailsByTownCode)
            )
          );
      },
      onError: (
        action: CorporateTownActions.LoadTownDetailsByTownCode,
        error: HttpErrorResponse
        ) => {
        return new CorporateTownActions.LoadTownDetailsByTownCodeFailure(this.errorHandler(error));
      }
    }
  );

  @Effect()
  saveTownFormDetails$ = this.dataPersistence.pessimisticUpdate(
    CorporateTownActionTypes.SAVE_CORPORATE_TOWN,
    {

      run: (action: CorporateTownActions.SaveTownFormDetails) => {
      return this.corporateTownService.
         saveTownFormDetails(action.payload)
         .pipe(
            map((saveData: CorporateTown) => {
             return new CorporateTownActions.SaveTownFormDetailsSuccess(
               saveData
               );
        })
      );
    },
    onError: (
      action: CorporateTownActions.SaveTownFormDetails,
       error: HttpErrorResponse
      ) => {
      return new CorporateTownActions.SaveTownFormDetailsFailure(
        this.errorHandler(error)
        );
    }
  });


  @Effect()
  editTownFormDetails$ = this.dataPersistence.pessimisticUpdate(
    CorporateTownActionTypes.EDIT_CORPORATE_TOWN,
    {
      run:
      (
        action: CorporateTownActions.EditTownFormDetails
        ) => {
              return this.corporateTownService
                .editTownFormDetails(action.payload)
                .pipe(
                  map((saveData: CorporateTown) => {
                    return new CorporateTownActions.EditTownFormDetailsSuccess(
                      saveData
            );
          })
        );
      },
      onError: (
        action: CorporateTownActions.EditTownFormDetails,
        error: HttpErrorResponse
      ) => {
        return new CorporateTownActions.EditTownFormDetailsFailure(
          this.errorHandler(error)
        );
      }
    }
  );




  @Effect()
  loadRegionDetails$ = this.dataPersistence.fetch(
    CorporateTownActionTypes.LOAD_REGION_DETAILS, {
    run: (action: CorporateTownActions.LoadRegionDetails) => {
      return this.corporateTownService
        .getRegionDetails(action.payload)
        .pipe(
          map(
            (
              regionList: LoadRegionListingSuccessPayload
              ) => new CorporateTownActions.LoadRegionDetailsSuccess(regionList)
          )
        );
    },
    onError: (
      action: CorporateTownActions.LoadRegionDetails,
       error: HttpErrorResponse) => {
      return new CorporateTownActions.LoadRegionDetailsFailure(this.errorHandler(error));
    }
  });


  @Effect()
  searchCorporateTownByTownCode$ = this.dataPersistence.fetch(
    CorporateTownActionTypes.SEARCH_CORPORATETOWN_BY_CODE,
    {
      run: (action: CorporateTownActions.SearchCorporateTownCode) => {
        return this.corporateTownService
          .searchCorporateTownByTownCode(action.payload)
          .pipe(
            map(
              (binGroupList: LoadCorporateTownListingSuccessPayload) =>
                new CorporateTownActions.SearchCorporateTownCodeSuccess(
                  binGroupList
                )
            )
          );
      },
      onError: (
        action: CorporateTownActions.SearchCorporateTownCode,
        error: HttpErrorResponse
      ) => {
        return new CorporateTownActions.SearchCorporateTownCodeFailure(
          this.errorHandler(error)
        );
      }
    }
  );


  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
}
