import { ApiService } from '@poss-web/core';
import { Injectable } from '@angular/core';
import { LoadCorporateTownListingPayload, LoadCorporateTownListingSuccessPayload, LoadStateListingSuccessPayload, SaveTownFormDetailsPayload, LoadRegionListingSuccessPayload } from '../+state/corporate-town.actions';
import { Observable } from 'rxjs';
import { getCorporateTownListingUrl, getStateListingUrl, getTownDetailsByTownCodeUrl, getSaveTownFormDetailsUrl, getTownEditedFormDetailsUrl, getTownRegionListingUrl } from '../../endpoint.constants';
import { map } from 'rxjs/operators';
import { CorporateTownAdaptor } from '../adaptors/corporate-town.adaptor';
import { CorporateTown } from '../models/corporate-town.model';

@Injectable({
  providedIn: 'root'
})
export class CorporateTownService {
  constructor(private apiService: ApiService) {}


  getCorporateTownDetails(
    loadCorporateTownDetailsPayload: LoadCorporateTownListingPayload
  ): Observable<LoadCorporateTownListingSuccessPayload> {
    const url = getCorporateTownListingUrl(
      loadCorporateTownDetailsPayload.pageIndex,
      loadCorporateTownDetailsPayload.pageSize
    );
    return this.apiService
      .get(url)
      .pipe(
        map(data =>
          CorporateTownAdaptor.getCorporateTownDetailsListing(data)
        )
      );
  }

  getStateDetails(
    loadCorporateTownDetailsPayload: LoadCorporateTownListingPayload
  ): Observable<LoadStateListingSuccessPayload> {
    const url = getStateListingUrl(
      loadCorporateTownDetailsPayload.pageIndex,
      loadCorporateTownDetailsPayload.pageSize
    );
    return this.apiService
      .get(url)
      .pipe(
        map(data =>
          CorporateTownAdaptor.getStateDetailsListing(data)
        )
      );
  }

  getTownDetailsByTownCode(
    townCode: number
  ): Observable<CorporateTown> {
    const url = getTownDetailsByTownCodeUrl(townCode);
    return this.apiService.get(url);
  }


  saveTownFormDetails(saveForm: SaveTownFormDetailsPayload) {
    const url = getSaveTownFormDetailsUrl();
    return this.apiService.post(url, saveForm);
  }

  editTownFormDetails(editedForm: SaveTownFormDetailsPayload){
    const url = getTownEditedFormDetailsUrl(editedForm.townCode);
    return this.apiService.patch(url, editedForm);
  }


  getRegionDetails(
    loadCorporateTownDetailsPayload: LoadCorporateTownListingPayload
  ): Observable<LoadRegionListingSuccessPayload> {
    const url = getTownRegionListingUrl(
      loadCorporateTownDetailsPayload.pageIndex,
      loadCorporateTownDetailsPayload.pageSize
    );
    return this.apiService
      .get(url)
      .pipe(
        map(data =>
          CorporateTownAdaptor.getRegionDetailsListing(data)
        )
      );
  }


  searchCorporateTownByTownCode(townCode){
    const url = getTownDetailsByTownCodeUrl(townCode);
    return this.apiService
    .get(url)
    .pipe(map(data =>
      CorporateTownAdaptor.getSearchDetailsListing(data)
      )
    );
  }
}
