import { BrandListing } from '../+state/brand-master.actons';
import {
  BrandMaster,
  Currency,
  PancardConfiguration,
  ResidualAmount,
  CMSMSConfiguration
} from '../models/brand-model';

export class BrandMasterAdaptors {
  static getBrandMasterList(data: any): BrandListing {

    const brandList: BrandMaster[] = [];
    let brandListData: BrandListing;
    for (const brandlistdata of data.results) {
      brandList.push({
        brandCode: brandlistdata.brandCode ? brandlistdata.brandCode : '',
        description: brandlistdata.description ? brandlistdata.description : '',
        parentBrandCode: brandlistdata.parentBrandCode
          ? brandlistdata.parentBrandCode
          : '',
        orgCode: brandlistdata.orgCode ? brandlistdata.orgCode : '',
        isActive: brandlistdata.isActive,
        configDetails: brandlistdata.configDetails
          ? brandlistdata.configDetails
          : ''
      });
    }
    brandListData = {
      results: brandList,
      totalElements: data.totalElements
    };

    return brandListData;
  }

  static getParentBrandMasterList(data: any): any {

    const parentBrandList: any = [];

    for (const brandlistdata of data.results) {
      parentBrandList.push({
        brandCode: brandlistdata.brandCode ? brandlistdata.brandCode : '',
        parentBrandCode: brandlistdata.parentBrandCode
          ? brandlistdata.parentBrandCode
          : ''
      });
    }

    return parentBrandList;
  }

  static getSearchResult(data: any): BrandListing {
  const brandMasterData: BrandMaster[] = [];
    let brandList: BrandListing;
    brandMasterData.push({
      brandCode: data.brandCode ? data.brandCode : '',
      description: data.description ? data.description : '',
      parentBrandCode: data.parentBrandCode ? data.parentBrandCode : '',
      configDetails: data.configDetails ? data.configDetails : '',
      orgCode: data.orgCode ? data.orgCode : '',
      isActive: data.isActive ? data.isActive : ''
    });
    let totalElements;
    if (data) {
      totalElements = 1;
    } else {
      totalElements = 0;
    }
    brandList = {
      results: brandMasterData,
      totalElements: totalElements
    };
    return brandList;
  }

  static getAllBrandDetailsByBrandCode(data): BrandMaster {
    let brandMasterData: BrandMaster;
    if (data) {
      brandMasterData = {
        brandCode: data.brandCode ? data.brandCode : '',
        description: data.description ? data.description : '',
        parentBrandCode: data.parentBrandCode ? data.parentBrandCode : '',
        configDetails: data.configDetails ? data.configDetails : '',
        orgCode: data.orgCode ? data.orgCode : '',
        isActive: data.isActive ? data.isActive : ''
      };
    } else {
      brandMasterData = {
        brandCode: '',
        description: '',
        parentBrandCode: '',
        configDetails: null,
        orgCode: '',
        isActive: data.isActive ? data.isActive : false
      };
    }
    return brandMasterData;
  }

  static getOnlyBrandDetails(data: any): BrandMaster {
    if (data !== null) {
      let brandDetails: BrandMaster;
      brandDetails = {
        brandCode: data.brandCode ? data.brandCode : '',
        parentBrandCode: data.parentBrandCode ? data.parentBrandCode : '',
        orgCode: data.orgCode ? data.orgCode : '',
        description: data.description ? data.description : '',
        isActive: data.isActive,
        configDetails: {
          subBrandConfig: {
            isActive: data.configDetails
              ? data.configDetails.subBrandDetails
                ? data.configDetails.subBrandDetails
                : ''
              : ''
          },
          brandConfigDetails: {
            brandName: data.configDetails
              ? data.configDetails.brandConfigDetails
                ? data.configDetails.brandConfigDetails.brandName
                : ''
              : '',
            brandShortName: data.configDetails
              ? data.configDetails.brandConfigDetails
                ? data.configDetails.brandConfigDetails.brandShortName
                : ''
              : '',
            cashRefundLimit: data.configDetails
              ? data.configDetails.brandConfigDetails
                ? data.configDetails.brandConfigDetails.cashRefundLimit
                : ''
              : '',
            ULPServiceURL: data.configDetails
              ? data.configDetails.brandConfigDetails
                ? data.configDetails.brandConfigDetails.ULPServiceURL
                : ''
              : '',
            dummyMobNo: data.configDetails
              ? data.configDetails.brandConfigDetails
                ? data.configDetails.brandConfigDetails.dummyMobNo
                : ''
              : '',
            brandDetailsCheckBox: data.configDetails
              ? data.configDetails.brandConfigDetails
                ? data.configDetails.brandConfigDetails.brandDetailsCheckBox
                : ''
              : '',

            minUtilizationPercentageforGRN: data.configDetails
              ? data.configDetails.brandConfigDetails
                ? data.configDetails.brandConfigDetails
                    .minUtilizationPercentageforGRN
                  ? data.configDetails.brandConfigDetails
                      .minUtilizationPercentageforGRN
                  : ''
                : ''
              : ''
          }
        }
      };
      return brandDetails;
    }
  }

  static getOnlyCMSMSConfigurationDetails(data: any): CMSMSConfiguration {
    let cMSMSConfiguration: CMSMSConfiguration;
    if (data) {
      cMSMSConfiguration = {
        SMSUserName: data.configDetails
          ? data.configDetails.CMMSConfiguration
            ? data.configDetails.CMMSConfiguration.SMSUserName
            : ''
          : '',
        SMSPassword: data.configDetails
          ? data.configDetails.CMMSConfiguration
            ? data.configDetails.CMMSConfiguration.SMSPassword
            : ''
          : '',
        centralS3BucketName: data.configDetails
          ? data.configDetails.CMMSConfiguration
            ? data.configDetails.CMMSConfiguration.centralS3BucketName
            : ''
          : '',
        centralS3AccessKey: data.configDetails
          ? data.configDetails.CMMSConfiguration
            ? data.configDetails.CMMSConfiguration.centralS3AccessKey
            : ''
          : '',
        centralS3SecretKey: data.configDetails
          ? data.configDetails.CMMSConfiguration
            ? data.configDetails.CMMSConfiguration.centralS3SecretKey
            : ''
          : '',
        centralS3RegionEndPoint: data.configDetails
          ? data.configDetails.CMMSConfiguration
            ? data.configDetails.CMMSConfiguration.centralS3RegionEndPoint
            : ''
          : '',
        centralWebAPIURL: data.configDetails
          ? data.configDetails.CMMSConfiguration
            ? data.configDetails.CMMSConfiguration.centralWebAPIURL
            : ''
          : '',
        EPOSSServiceURL: data.configDetails
          ? data.configDetails.CMMSConfiguration
            ? data.configDetails.CMMSConfiguration.EPOSSServiceURL
            : ''
          : ''
      };

      return cMSMSConfiguration;
    }
  }

  static getOnlyCurrencyDetails(data: any): Currency {
    let currency: Currency;
    if (data) {
      currency = {
        currencyCheckbox: data.configDetails
          ? data.configDetails.currency
            ? data.configDetails.currency.currencyCheckbox
            : ''
          : '',
        inventoryCurrency: data.configDetails
          ? data.configDetails.currency
            ? data.configDetails.currency.inventoryCurrency
            : ''
          : '',
        STNCurrency: data.configDetails
          ? data.configDetails.currency
            ? data.configDetails.currency.STNCurrency
            : ''
          : '',
        masterCurrency: data.configDetails
          ? data.configDetails.currency
            ? data.configDetails.currency.masterCurrency
            : ''
          : ''
      };
      return currency;
    }
  }

  static getOnlyPanCardDetails(data: any): PancardConfiguration {
    let panCardConfiguration: PancardConfiguration;
    if (data) {
      panCardConfiguration = {
        configurationAmountForCashMemo: data.configDetails
          ? data.configDetails.pancardConfiguration
            ? data.configDetails.pancardConfiguration
                .configurationAmountForCashMemo
            : ''
          : '',
        configurationAmountForAdvance: data.configDetails
          ? data.configDetails.pancardConfiguration
            ? data.configDetails.pancardConfiguration
                .configurationAmountForAdvance
            : ''
          : '',
        pancardCheckkbox: data.configDetails
          ? data.configDetails.pancardConfiguration
            ? data.configDetails.pancardConfiguration.pancardCheckkbox
            : ''
          : ''
      };
      return panCardConfiguration;
    }
  }

  static getOnlyResidualAmount(data: any): ResidualAmount {
    if (data) {
      let residualAmount: ResidualAmount;
      residualAmount = {
        residualAmountForeGHSTransfer: data.configDetails
          ? data.configDetails.residualAmount
            ? data.configDetails.residualAmount.residualAmountForeGHSTransfer
            : ''
          : ''
      };
      return residualAmount;
    }
  }
}
