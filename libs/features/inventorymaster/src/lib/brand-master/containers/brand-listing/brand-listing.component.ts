import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef
} from '@angular/core';
import { BrandMasterFacade } from '../../+state/brand-master.facade';
import { Observable, Subject, fromEvent } from 'rxjs';
import { PageEvent } from '@angular/material';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import { takeUntil, debounceTime, take } from 'rxjs/operators';

import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { BrandMaster } from '../../models/brand-model';

import {
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationEventType,
  OverlayNotificationService,
  ErrorEnums
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { getBrandDetailsRouteUrl, getProductMasterDashboardRouteUrl } from '../../../page-route.constants';

@Component({
  selector: 'poss-web-brand-listing',
  templateUrl: './brand-listing.component.html',
  styleUrls: ['./brand-listing.component.scss']
})
export class BrandListingComponent implements OnInit, AfterViewInit {
  brandList$: Observable<BrandMaster[]>;
  isLoading$: Observable<boolean>;
  isActiveToggle$: Observable<boolean>;
  destroy$ = new Subject<null>();
  searchErrorCode: string;
  pageSize: number[];
  totalElements$: Observable<number>;
  hasError$: Observable<CustomErrors>;
  brandMasterListingPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };
  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;
  searchForm = new FormGroup({
    searchValue: new FormControl()
  });
  constructor(
    private route: ActivatedRoute,
    private brandMasterFacade: BrandMasterFacade,
    private router: Router,
    private appsettingFacade: AppsettingFacade,
    private translate: TranslateService,
    private overlayNotification: OverlayNotificationService
  ) { }

  ngAfterViewInit(): void {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe(event => {
        const searchValue = this.searchForm.value.searchValue;
        if (searchValue) {
          this.search(searchValue);
        } else {
          this.clearSearch();
        }
      });
  }

  search(searchValue: string) {
    if (!searchValue.search('^[_A-z0-9]*((-|s)*[_A-z0-9])*$')) {
      this.brandMasterFacade.searchBrand(searchValue);
    }
  }
  clearSearch() {
    this.searchForm.reset();
    this.loadBrandMasterList();
  }
  updateIsActive(event) {
    this.brandMasterFacade.upDateIsactive({
      brandCode: event.brandCode,
      isActive: event.isActive
    });
  }
  loadDetailsPage(brandCode: string) {
    this.brandMasterFacade.loadReset();
    if (brandCode) {
      // this.locationService.getLocationDetailsByLocationCode('AAA');

      this.router.navigate([getBrandDetailsRouteUrl(brandCode)]);
    }
  }
  ngOnInit() {
    this.hasError$ = this.brandMasterFacade.getError();
    this.searchErrorCode = ErrorEnums.ERR_PRO_001;
    this.isLoading$ = this.brandMasterFacade.getIsloading();

    this.brandMasterFacade
      .getIsActiveToggle()
      .pipe(takeUntil(this.destroy$))
      .subscribe(d => {
        if (d) {
          this.showNotification('pw.brandMaster.updateSuccessMessage');
          this.brandMasterFacade.resetIsActiveToggle();
        }
      });

    this.appsettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.pageSize = data;
      });
    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.brandMasterListingPageEvent.pageSize = data;
        this.loadBrandMasterList();
        this.brandList$ = this.brandMasterFacade.getBrandMasterList();
        this.totalElements$ = this.brandMasterFacade.getTotalElements();
      });

    this.brandMasterFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });
  }

  loadBrandMasterList() {
    this.brandMasterFacade.loadBrandMasterList(
      this.brandMasterListingPageEvent
    );
  }
  paginate(pageEvent: PageEvent) {
    this.brandMasterListingPageEvent = pageEvent;
    this.loadBrandMasterList();
  }
  addnew() {
    this.brandMasterFacade.loadReset();
    this.router.navigate([getBrandDetailsRouteUrl('NEW')]
    );
  }

  back() {
    this.brandMasterFacade.loadReset();
    this.router.navigate([getProductMasterDashboardRouteUrl()]);
  }

  errorHandler(error: CustomErrors) {
    // let key = '';
    // //check if the error is the custom error
    // if (customErrorTranslateKeyMapMasters.has(error.code)) {
    //   //Obtain the transation key which will be use to obtain the translated error message
    //   //based on the language selected. Default is the english language(refer en.json from asset folder).
    //   key = customErrorTranslateKeyMapMasters.get(error.code);
    // } else {
    //   key = 'pw.global.genericErrorMessage';
    // }

    if (error.code === this.searchErrorCode) {
      // We are not showing error for location not found from search.
      return;
    }
    this.overlayNotification.show({
      type: OverlayNotificationType.ERROR,
      hasClose: true,
      error: error
    });
  }
  omit_special_char($event: KeyboardEvent) {
    const pattern = /^[-_A-Za-z0-9]$/;
    return pattern.test($event.key);
  }

  showNotification(key: string) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMessage,
            time: 2000,
            hasBackdrop: true
          })
          .events.subscribe((eventType: OverlayNotificationEventType) => {
            this.overlayNotification.close();
          });
      });
  }
}
