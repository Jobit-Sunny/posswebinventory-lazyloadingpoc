import { Component, OnInit } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';

import { ActivatedRoute, Router } from '@angular/router';
import { BrandMasterFacade } from '../../../+state/brand-master.facade';
import { takeUntil, take, skipUntil } from 'rxjs/operators';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventType,
  OverlayNotificationEventRef
} from '@poss-web/shared';
import { CustomErrors } from '@poss-web/core';
import { BrandMaster } from '../../../models/brand-model';
import { TranslateService } from '@ngx-translate/core';
import { getBrandDetailsRouteUrl } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-brand',
  templateUrl: './brand.component.html',
  styles: []
})
export class BrandComponent implements OnInit {
  brandDetails$: Observable<BrandMaster>;
  brandCode: string;
  page: string;
  destroy$ = new Subject();
  hasError$: Observable<CustomErrors>;
  isLoading$: Observable<boolean>;
  hasUpdated$: Observable<boolean>;
  hasSaved$: Observable<boolean>;
  errorCode: string;

  constructor(
    private route: ActivatedRoute,
    private brandMasterFacade: BrandMasterFacade,
    private router: Router,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.isLoading$ = this.brandMasterFacade.getIsloading();
    this.hasUpdated$ = this.brandMasterFacade.getHasUpdated();
    this.hasSaved$ = this.brandMasterFacade.getHasSaved();
    this.hasError$ = this.brandMasterFacade.getError();
    const fromPath = this.route.pathFromRoot[2];
    this.brandCode = fromPath.snapshot.params['brandCode'];
    this.hasUpdated$.pipe(takeUntil(this.destroy$)).subscribe(hasUpdated => {
      if (hasUpdated === true) {
        this.showNotification('pw.brandMaster.updateSuccessMessage');
      } else this.overlayNotification.close();
    });

    this.hasSaved$.pipe(takeUntil(this.destroy$)).subscribe(hasSaved => {
      if (hasSaved === true) {
        this.showNotification('pw.brandMaster.saveSuccessMessage');
      } else this.overlayNotification.close();
    });

    this.brandMasterFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    // this.isLoading$
    // .pipe(
    //   takeUntil(this.destroy$)
    // )
    // .subscribe(loading => {
    //   if (loading === false) {
    //     this.showNotification();
    //   }
    // });
    // this.isLoading$
    // .pipe(
    //   skipUntil(of(true)),
    //   takeUntil(this.destroy$)
    // )
    // .subscribe(loading => {
    //   if (loading === false) {
    //     this.showNotification();
    //   }
    // });
    this.brandDetails$ = this.brandMasterFacade.getOnlyBrandDetails();
    this.brandDetails$.pipe(takeUntil(this.destroy$)).subscribe(data => {
      if (
        data &&
        this.brandCode === 'NEW' &&
        data.brandCode !== 'NEW' &&
        data.brandCode !== ''
      ) {
        this.destroy$.next();
        this.destroy$.complete();

        setTimeout(() => {
          this.router.navigate([getBrandDetailsRouteUrl(data.brandCode)]);
        }, 2000);
      }
    });

    this.hasError$ = this.brandMasterFacade.getError();

    this.routerPage(this.page);
  }

  addButton(brandDetails: BrandMaster) {
    const fromPath = this.route.pathFromRoot[2];
    this.brandCode = fromPath.snapshot.params['brandCode'];
    if (this.brandCode === 'NEW') {
      this.brandMasterFacade.saveBrandMasterDetails({
        brandCode: brandDetails.brandCode,
        configDetails: {
          brandConfigDetails: {
            brandShortName:
              brandDetails.configDetails.brandConfigDetails.brandShortName,
            brandName: brandDetails.configDetails.brandConfigDetails.brandName,
            cashRefundLimit:
              brandDetails.configDetails.brandConfigDetails.cashRefundLimit,

            ULPServiceURL:
              brandDetails.configDetails.brandConfigDetails.ULPServiceURL,
            dummyMobNo:
              brandDetails.configDetails.brandConfigDetails.dummyMobNo,
            brandDetailsCheckBox:
              brandDetails.configDetails.brandConfigDetails
                .brandDetailsCheckBox,
            minUtilizationPercentageforGRN:
              brandDetails.configDetails.brandConfigDetails
                .minUtilizationPercentageforGRN
          }
        },
        description: brandDetails.description,
        isActive: true,
        orgCode: 'TJ',
        parentBrandCode: ''
      });
    } else {
      this.brandMasterFacade.updateBrandMasterDetails({
        brandCode: this.brandCode,
        data: {
          isActive: true,
          description: brandDetails.description,
          parentBrandCode: '',
          orgCode: 'TJ',
          configDetails: {
            brandConfigDetails: {
              brandShortName:
                brandDetails.configDetails.brandConfigDetails.brandShortName,
              brandName:
                brandDetails.configDetails.brandConfigDetails.brandName,
              cashRefundLimit:
                brandDetails.configDetails.brandConfigDetails.cashRefundLimit,

              ULPServiceURL:
                brandDetails.configDetails.brandConfigDetails.ULPServiceURL,
              dummyMobNo:
                brandDetails.configDetails.brandConfigDetails.dummyMobNo,
              brandDetailsCheckBox:
                brandDetails.configDetails.brandConfigDetails
                  .brandDetailsCheckBox,
              minUtilizationPercentageforGRN:
                brandDetails.configDetails.brandConfigDetails
                  .minUtilizationPercentageforGRN
            }
          }
        }
      });
    }
  }

  showNotification(key: string) {
    // let keymessage;
    // this.hasError$.pipe(take(1)).subscribe(data => {

    //   if (data) {
    //     keymessage = 'Something Went Wrong';
    //   } else {
    //     keymessage = 'Saved Successfully';
    //   }

    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMessage,
            time: 2000,
            hasBackdrop: true
          })
          .events.subscribe((eventType: OverlayNotificationEventType) => {
            this.overlayNotification.close();
          });
      });
  }

  errorHandler(error) {
    this.overlayNotification.show({
      type: OverlayNotificationType.ERROR,
      hasClose: true,
      error: error
    });
  }
  routerPage(page: string) {
    this.route.paramMap.pipe(takeUntil(this.destroy$)).subscribe(params => {
      this.page = params.get('page');
    });
  }
}
