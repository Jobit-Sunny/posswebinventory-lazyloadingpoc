import { Component, OnInit } from '@angular/core';

import { BrandMasterFacade } from '../../../+state/brand-master.facade';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject, of } from 'rxjs';
import { takeUntil, skipUntil, take } from 'rxjs/operators';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventType,
  OverlayNotificationEventRef,
  ErrorEnums
} from '@poss-web/shared';
import { CustomErrors } from '@poss-web/core';
import { Currency } from '../../../models/brand-model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'poss-web-currency',
  templateUrl: './currency.component.html',
  styles: []
})
export class CurrencyComponent implements OnInit {
  brandCode: string;
  currencyDetails$: Observable<Currency>;
  supportedCurrency$: Observable<any>;
  page: string;
  destroy$ = new Subject();
  hasError$: Observable<CustomErrors>;
  isLoading$: Observable<boolean>;
  hasUpdated$: Observable<boolean>;
  errorCode: string;

  constructor(
    private brandMasterFacade: BrandMasterFacade,
    private route: ActivatedRoute,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.supportedCurrency$ = this.brandMasterFacade.getSupportedCurrencyCode();
    this.hasUpdated$ = this.brandMasterFacade.getHasUpdated();
    this.errorCode = ErrorEnums.ERR_PRO_001;
    this.hasError$ = this.brandMasterFacade.getError();
    this.hasUpdated$.pipe(takeUntil(this.destroy$)).subscribe(hasUpdated => {
      if (hasUpdated === true) {
        this.showNotification('pw.brandMaster.updateSuccessMessage');
      } else this.overlayNotification.close();
    });

    this.brandMasterFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.isLoading$ = this.brandMasterFacade.getIsloading();
    this.routerPage(this.page);
    this.currencyDetails$ = this.brandMasterFacade.getOnlyCurrencyDetails();
    this.hasError$ = this.brandMasterFacade.getError();
  }

  addButton(currencyDetails: Currency) {
    const fromPath = this.route.pathFromRoot[2];
    this.brandCode = fromPath.snapshot.params['brandCode'];
    this.brandMasterFacade.updateBrandMasterDetails({
      brandCode: this.brandCode,
      data: {
        configDetails: {
          currency: {
            currencyCheckbox: currencyDetails.currencyCheckbox,
            inventoryCurrency: currencyDetails.inventoryCurrency,
            STNCurrency: currencyDetails.STNCurrency,
            masterCurrency: currencyDetails.masterCurrency
          }
        }
      }
    });
  }

  showNotification(key: string) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMessage,
            time: 2000,
            hasBackdrop: true
          })
          .events.subscribe((eventType: OverlayNotificationEventType) => {
            this.overlayNotification.close();
          });
      });
  }

  errorHandler(error) {
    this.overlayNotification.show({
      type: OverlayNotificationType.ERROR,
      hasClose: true,
      error: error
    });
  }
  routerPage(page: string) {
    this.route.paramMap.pipe(takeUntil(this.destroy$)).subscribe(params => {
      this.page = params.get('page');
    });
  }
}
