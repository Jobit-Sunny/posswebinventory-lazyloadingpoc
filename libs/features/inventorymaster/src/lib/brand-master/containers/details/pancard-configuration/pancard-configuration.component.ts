import { Component, OnInit } from '@angular/core';

import { BrandMasterFacade } from '../../../+state/brand-master.facade';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject, of } from 'rxjs';
import { takeUntil, skipUntil, take } from 'rxjs/operators';
import {
  OverlayNotificationEventType,
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  ErrorEnums
} from '@poss-web/shared';
import { CustomErrors } from '@poss-web/core';
import { PancardConfiguration } from '../../../models/brand-model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'poss-web-pancard-configuration',
  templateUrl: './pancard-configuration.component.html',
  styles: []
})
export class PancardConfigurationComponent implements OnInit {
  brandCode: string;
  panCardDetails$: Observable<PancardConfiguration>;
  page: string;
  destroy$ = new Subject();
  hasError$: Observable<CustomErrors>;
  isLoading$: Observable<boolean>;
  hasUpdated$: Observable<boolean>;
  errorCode: string;

  constructor(
    private brandMasterFacade: BrandMasterFacade,
    private route: ActivatedRoute,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.hasUpdated$ = this.brandMasterFacade.getHasUpdated();
    this.errorCode = ErrorEnums.ERR_PRO_001;
    this.hasError$ = this.brandMasterFacade.getError();
    this.panCardDetails$ = this.brandMasterFacade.getOnlyPanCardDetails();
    this.isLoading$ = this.brandMasterFacade.getIsloading();
    this.hasError$ = this.brandMasterFacade.getError();

    this.hasUpdated$.pipe(takeUntil(this.destroy$)).subscribe(hasUpdated => {
      if (hasUpdated === true) {
        this.showNotification('pw.brandMaster.updateSuccessMessage');
      } else this.overlayNotification.close();
    });

    this.brandMasterFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.routerPage(this.page);
  }

  addButton(panCardDetails: PancardConfiguration) {
    const fromPath = this.route.pathFromRoot[2];
    this.brandCode = fromPath.snapshot.params['brandCode'];
    let configurationAmountForCashMemo;
    let configurationAmountForAdvance;
    const num1 = panCardDetails.configurationAmountForCashMemo.replace(
      /,/g,
      ''
    );
    const arr1 = num1.split('.');
    if (arr1.length !== 2) {
      configurationAmountForCashMemo = Number(num1).toFixed(3);
    } else {
      configurationAmountForCashMemo = num1;
    }

    const num2 = panCardDetails.configurationAmountForAdvance.replace(/,/g, '');
    const arr2 = num2.split('.');
    if (arr2.length !== 2) {
      configurationAmountForAdvance = Number(num2).toFixed(3);
    } else {
      configurationAmountForAdvance = num2;
    }
    this.brandMasterFacade.updateBrandMasterDetails({
      brandCode: this.brandCode,
      data: {
        configDetails: {
          pancardConfiguration: {
            configurationAmountForCashMemo: configurationAmountForCashMemo,
            configurationAmountForAdvance: configurationAmountForAdvance,
            pancardCheckkbox: panCardDetails.pancardCheckkbox
          }
        }
      }
    });
  }

  showNotification(key: string) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMessage,
            time: 2000,
            hasBackdrop: true
          })
          .events.subscribe((eventType: OverlayNotificationEventType) => {
            this.overlayNotification.close();
          });
      });
  }

  errorHandler(error) {
    this.overlayNotification.show({
      type: OverlayNotificationType.ERROR,
      hasClose: true,
      error: error
    });
  }
  routerPage(page: string) {
    this.route.paramMap.pipe(takeUntil(this.destroy$)).subscribe(params => {
      this.page = params.get('page');
    });
  }
}
