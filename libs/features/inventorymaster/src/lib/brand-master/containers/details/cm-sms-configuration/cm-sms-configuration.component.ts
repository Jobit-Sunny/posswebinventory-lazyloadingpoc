import { Component, OnInit } from '@angular/core';
import { BrandMasterFacade } from '../../../+state/brand-master.facade';

import { Observable, Subject, of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { takeUntil, skipUntil, take } from 'rxjs/operators';
import {
  OverlayNotificationType,
  OverlayNotificationEventType,
  OverlayNotificationService,
  OverlayNotificationEventRef
} from '@poss-web/shared';
import { CustomErrors } from '@poss-web/core';
import { CMSMSConfiguration } from '../../../models/brand-model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'poss-web-cm-sms-configuration',
  templateUrl: './cm-sms-configuration.component.html',
  styles: []
})
export class CMSMSConfigurationComponent implements OnInit {
  CMSMSConfigurationDetails$: Observable<CMSMSConfiguration>;
  brandCode: string;
  page: string;
  destroy$ = new Subject();

  hasError$: Observable<CustomErrors>;
  isLoading$: Observable<boolean>;
  hasUpdated$: Observable<boolean>;
  hasSaved$: Observable<boolean>;
  errorCode: string;
  constructor(
    private brandMasterFacade: BrandMasterFacade,
    private route: ActivatedRoute,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.hasUpdated$ = this.brandMasterFacade.getHasUpdated();
    this.hasSaved$ = this.brandMasterFacade.getHasSaved();
    this.hasError$ = this.brandMasterFacade.getError();
    this.hasUpdated$.pipe(takeUntil(this.destroy$)).subscribe(hasUpdated => {
      if (hasUpdated === true) {
        this.showNotification('pw.brandMaster.updateSuccessMessage');
      } else this.overlayNotification.close();
    });

    this.brandMasterFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.CMSMSConfigurationDetails$ = this.brandMasterFacade.getOnlyCMSMSConfigurationDetails();
    this.isLoading$ = this.brandMasterFacade.getIsloading();
    this.hasError$ = this.brandMasterFacade.getError();

    this.routerPage(this.page);
  }

  addButton(CMMSConfiguration: CMSMSConfiguration) {
    const fromPath = this.route.pathFromRoot[2];
    this.brandCode = fromPath.snapshot.params['brandCode'];
    this.brandMasterFacade.updateBrandMasterDetails({
      brandCode: this.brandCode,
      data: {
        configDetails: {
          CMMSConfiguration: {
            SMSUserName: CMMSConfiguration.SMSUserName,
            SMSPassword: CMMSConfiguration.SMSPassword,
            centralS3BucketName: CMMSConfiguration.centralS3BucketName,
            centralS3AccessKey: CMMSConfiguration.centralS3AccessKey,
            centralS3SecretKey: CMMSConfiguration.centralS3SecretKey,
            centralS3RegionEndPoint: CMMSConfiguration.centralS3RegionEndPoint,
            centralWebAPIURL: CMMSConfiguration.centralWebAPIURL,
            EPOSSServiceURL: CMMSConfiguration.EPOSSServiceURL
          }
        }
      }
    });
  }

  showNotification(key: string) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMessage,
            time: 2000,
            hasBackdrop: true
          })
          .events.subscribe((eventType: OverlayNotificationEventType) => {
            this.overlayNotification.close();
          });
      });
  }

  errorHandler(error) {
    this.overlayNotification.show({
      type: OverlayNotificationType.ERROR,
      hasClose: true,
      error: error
    });
  }
  routerPage(page: string) {
    this.route.paramMap.pipe(takeUntil(this.destroy$)).subscribe(params => {
      this.page = params.get('page');
    });
  }
}
