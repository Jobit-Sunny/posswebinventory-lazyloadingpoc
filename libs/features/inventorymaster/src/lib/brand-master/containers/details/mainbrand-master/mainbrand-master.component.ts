import { Component, OnInit } from '@angular/core';
import { BrandMasterFacade } from '../../../+state/brand-master.facade';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { getBrandListRouteUrl, getBrandMasterBrandRouterLink, getBrandMasterPancardRouterLink, getBrandMasterResidualamountdRouterLink, getBrandMasterSMSConfigRouterLink, getBrandMasterCurrencyRouterLink } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-mainbrand-master',
  templateUrl: './mainbrand-master.component.html',
  styleUrls: ['./mainbrand-master.component.scss']
})
export class MainbrandMasterComponent implements OnInit {

  brandMasterBrandRouterLink = getBrandMasterBrandRouterLink('1');
  brandMasterPancardRouterLink = getBrandMasterPancardRouterLink('1');
  brandMasterResidualamountdRouterLink = getBrandMasterResidualamountdRouterLink('1');
  brandMasterSMSConfigRouterLink = getBrandMasterSMSConfigRouterLink('1');
  brandMasterCurrencyRouterLink = getBrandMasterCurrencyRouterLink('1');

  isNew: boolean;
  isLoading$: Observable<boolean>;
  brandCode: string;
  destroy$ = new Subject();
  constructor(
    private brandMasterFacade: BrandMasterFacade,
    private activatedRoute: ActivatedRoute,
    private route: Router
  ) { }

  back() {
    this.brandMasterFacade.loadReset();
    this.route.navigate([getBrandListRouteUrl()]);
  }

  ngOnInit() {
    this.isLoading$ = this.brandMasterFacade.getIsloading();

    this.brandCode = this.activatedRoute.snapshot.params['brandCode'];
    this.brandMasterFacade.loadSupportedCurrencyCode()
    if (this.brandCode === 'NEW') {
      this.brandMasterFacade.loadNewBrandDetails();
    } else {
      this.brandMasterFacade.loadBrandDetailsByBrandCode(this.brandCode);
    }
    this.activatedRoute.paramMap
      .pipe(takeUntil(this.destroy$))
      .subscribe(params => {
        if (params['params']['brandCode'] === 'NEW') {
          this.isNew = true;
        } else {
          this.isNew = false;
        }
      });
  }
  change() {
    this.brandMasterFacade.loadReset();
  }
}
