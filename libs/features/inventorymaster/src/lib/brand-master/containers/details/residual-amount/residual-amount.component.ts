import { Component, OnInit } from '@angular/core';

import { BrandMasterFacade } from '../../../+state/brand-master.facade';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject, of } from 'rxjs';
import { takeUntil, skipUntil, take } from 'rxjs/operators';
import {
  OverlayNotificationType,
  OverlayNotificationEventType,
  OverlayNotificationService,
  OverlayNotificationEventRef,
  ErrorEnums
} from '@poss-web/shared';
import { CustomErrors } from '@poss-web/core';
import { ResidualAmount } from '../../../models/brand-model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'poss-web-residual-amount',
  templateUrl: './residual-amount.component.html',
  styles: []
})
export class ResidualAmountComponent implements OnInit {
  brandCode: string;
  residualAmount$: Observable<ResidualAmount>;
  page: string;
  destroy$ = new Subject();
  hasError$: Observable<CustomErrors>;
  isLoading$: Observable<boolean>;
  hasUpdated$: Observable<boolean>;
  hasSaved$: Observable<boolean>;
  errorCode: string;
  constructor(
    private brandMasterFacade: BrandMasterFacade,
    private route: ActivatedRoute,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.errorCode = ErrorEnums.ERR_PRO_001;
    this.hasUpdated$ = this.brandMasterFacade.getHasUpdated();
    this.hasSaved$ = this.brandMasterFacade.getHasSaved();
    this.hasError$ = this.brandMasterFacade.getError();
    this.hasUpdated$.pipe(takeUntil(this.destroy$)).subscribe(hasUpdated => {
      if (hasUpdated === true) {
        this.showNotification('pw.brandMaster.updateSuccessMessage');
      } else this.overlayNotification.close();
    });

    this.brandMasterFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.isLoading$ = this.brandMasterFacade.getIsloading();
    this.hasError$ = this.brandMasterFacade.getError();
    this.residualAmount$ = this.brandMasterFacade.getOnlyResidualAmount();

    this.routerPage(this.page);
  }
  addButton(residualAmount: ResidualAmount) {
    const fromPath = this.route.pathFromRoot[2];
    this.brandCode = fromPath.snapshot.params['brandCode'];
    let resamount;
    const num = residualAmount.residualAmountForeGHSTransfer.replace(/,/g, '');
    const arr = num.split('.');
    if (arr.length !== 2) {
      resamount = Number(num).toFixed(3);
    } else {
      resamount = num;
    }

    this.brandMasterFacade.updateBrandMasterDetails({
      brandCode: this.brandCode,
      data: {
        configDetails: {
          residualAmount: {
            residualAmountForeGHSTransfer: resamount
          }
        }
      }
    });
  }

  showNotification(key: string) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMessage,
            time: 2000,
            hasBackdrop: true
          })
          .events.subscribe((eventType: OverlayNotificationEventType) => {
            this.overlayNotification.close();
          });
      });
  }

  errorHandler(error) {
    this.overlayNotification.show({
      type: OverlayNotificationType.ERROR,
      hasClose: true,
      error: error
    });
  }
  routerPage(page: string) {
    this.route.paramMap.pipe(takeUntil(this.destroy$)).subscribe(params => {
      this.page = params.get('page');
    });
  }
}
