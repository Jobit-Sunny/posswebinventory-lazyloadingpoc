import { createSelector } from "@ngrx/store";
import { selectInventoryMasters } from '../../inventorymasters.state';
import { brandSelector } from './brand-master.entity';

const BrandList = createSelector(
  selectInventoryMasters,
  state => state.brandMasterState.brandlist
)
const selectBrandList = createSelector(
  BrandList,
  brandSelector.selectAll
)
const selectsubBrandDetails = createSelector(
  selectInventoryMasters,
  state => state.brandMasterState.subBrandDetails
)

const selectTotalElements = createSelector(
  selectInventoryMasters,
  state => state.brandMasterState.totalElements
)
const selectBrandDetails = createSelector(
  selectInventoryMasters,
  state => state.brandMasterState.brandDetails
)


const selectSubBrandDetails = createSelector(
  selectInventoryMasters,
  state => state.brandMasterState.subBrandDetails
)
const selectIsLoading = createSelector(
  selectInventoryMasters,
  state => state.brandMasterState.isLoading
)
const selectIsActiveToggle = createSelector(
  selectInventoryMasters,
  state => state.brandMasterState.isActiveUpdated
)

const selecthasError = createSelector(
  selectInventoryMasters,
  state => state.brandMasterState.error
)




const selectHasUpdated = createSelector(
  selectInventoryMasters,
  state => state.brandMasterState.hasUpdated
)

const selectHasSaved = createSelector(
  selectInventoryMasters,
  state => state.brandMasterState.hasSaved
)
const selectCurrency = createSelector(
  selectInventoryMasters,
  state => state.brandMasterState.currency
)
const selectParentBrandList = createSelector(
  selectInventoryMasters,
  state => state.brandMasterState.parentBrands
)
export const BrandMasterSelectors = {
  selectBrandList,
  selectTotalElements,
  selectBrandDetails,
  selecthasError,
  selectIsLoading,
  selectsubBrandDetails,
  selectHasSaved,
  selectHasUpdated,
  selectSubBrandDetails,
  selectCurrency,
  selectParentBrandList,
  selectIsActiveToggle
}

