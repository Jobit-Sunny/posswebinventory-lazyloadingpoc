import { BrandMasterState } from './brand-master.state';
import {
  BrandMasterActions,
  BrandMasterActionTypes
} from './brand-master.actons';
import { brandAdaptor } from './brand-master.entity';

const initialState: BrandMasterState = {
  brandlist: brandAdaptor.getInitialState(),
  totalElements: null,
  error: null,
  brandDetails: null,
  isLoading: null,
  subBrandDetails: null,
  hasUpdated: null,
  hasSaved: null,
  currency: null,
  parentBrands: null,
  isActiveUpdated: null
};

export function BrandMasterReducer(
  state: any = initialState,
  action: BrandMasterActions
) {
  switch (action.type) {
    case BrandMasterActionTypes.LOAD_NEW_BRAND_DETAILS:
      return {
        ...state,
        error: null,
        isLoading: true
      };
    case BrandMasterActionTypes.LOAD_NEW_BRAND_DETAILS_SUCCESS:
      return {
        ...state,
        brandDetails: action.payload,
        isLoading: false
      };
    case BrandMasterActionTypes.LOAD_BRAND_MASTER_LISTING:
      return {
        ...state,
        error: null,
        isLoading: true,
        isActiveUpdated: false
      };

    case BrandMasterActionTypes.LOAD_BRAND_MASTER_LISTING_SUCCESS:
      return {
        ...state,
        brandlist: brandAdaptor.addAll(action.payload.results, state.brandlist),
        totalElements: action.payload.totalElements,
        error: null,
        isBrandListLoading: false,
        isSearching: true,
        isActiveUpdated: false,
        isLoading: false
      };

    case BrandMasterActionTypes.LOAD_BRAND_MASTER_LISTING_FAILURE:
      return {
        ...state,
        isSearching: true,
        error: action.payload,

        isLoading: false
      };
    case BrandMasterActionTypes.SAVE_BRAND_MASTER_DETAILS:
      return {
        ...state,
        error: null,
        hasSaved: false,
        isLoading: true
      };
    case BrandMasterActionTypes.SAVE_BRAND_MASTER_DETAILS_FAILURE:
      return {
        ...state,
        error: action.payload,
        hasSaved: false,
        isLoading: false
      };
    case BrandMasterActionTypes.SAVE_BRAND_MASTER_DETAILS_SUCCESS:
      return {
        ...state,
        error: null,
        brandDetails: action.payload,
        hasSaved: true,
        isLoading: false
      };

    case BrandMasterActionTypes.UPDATE_BRAND_MASTER_DETAILS:
      return {
        ...state,
        error: null,
        hasUpdated: false,
        hasSaved: null,
        isLoading: true
      };

    case BrandMasterActionTypes.UPDATE_BRAND_MASTER_DETAILS_SUCCESS:
      return {
        ...state,
        error: null,
        hasSaved: null,
        brandDetails: action.payload,
        hasUpdated: true,
        isLoading: false
      };
    case BrandMasterActionTypes.UPDATE_BRAND_MASTER_DETAILS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
        hasUpdated: false
      };

    case BrandMasterActionTypes.SAVE_SUB_BRAND_MASTER_DETAILS:
      return {
        ...state,
        error: null,
        hasSaved: false,
        subBrandDetails: null,
        isLoading: true
      };
    case BrandMasterActionTypes.SAVE_SUB_BRAND_MASTER_DETAILS_FAILURE:
      return {
        ...state,
        error: action.payload,
        hasSaved: false,
        subBrandDetails: null,
        isLoading: false
      };
    case BrandMasterActionTypes.SAVE_SUB_BRAND_MASTER_DETAILS_SUCCESS:
      return {
        ...state,
        error: null,
        brandDetails: null,
        subBrandDetails: null,
        hasSaved: true,
        isLoading: false
      };
    case BrandMasterActionTypes.UPDATE_SUB_BRAND_MASTER_DETAILS:
      return {
        ...state,
        error: null,
        brandDetails: null,
        hasUpdated: false,
        hasSaved: null,
        isLoading: true
      };

    case BrandMasterActionTypes.UPDATE_SUB_BRAND_MASTER_DETAILS_SUCCESS:
      return {
        ...state,
        brandDetails: null,
        subBrandDetails: null,
        error: null,
        hasSaved: null,
        hasUpdated: true,
        isLoading: false
      };
    case BrandMasterActionTypes.UPDATE_SUB_BRAND_MASTER_DETAILS_FAILURE:
      return {
        ...state,
        error: action.payload,
        subBrandDetails: null,
        isLoading: false,
        hasUpdated: false,
      };


    case BrandMasterActionTypes.LOAD_SUB_BRAND_DETAILS_BY_BRAND_CODE:
        case BrandMasterActionTypes.LOAD_BRAND_DETAILS_BY_BRAND_CODE:
      return {
        ...state,
        brandDetails: null,
        error: null,
        isLoading: true,
        isActiveUpdated: false
      };
    case BrandMasterActionTypes.LOAD_SUB_BRAND_DETAILS_BY_BRAND_CODE_SUCCESS:
      return {
        ...state,
        error: null,

        subBrandDetails: action.payload,
        isLoading: false
      };
      case BrandMasterActionTypes.LOAD_SUB_BRAND_DETAILS_BY_BRAND_CODE_FAILURE:
          case BrandMasterActionTypes.LOAD_BRAND_DETAILS_BY_BRAND_CODE_FAILURE:

      return {
        ...state,
        error: action.payload,
        isLoading: false
      };

    case BrandMasterActionTypes.UPDATE_IS_ACTIVE:
      return {
        ...state,
        isLoading: true,
        error: null,
        isActiveUpdated: false
      };
    case BrandMasterActionTypes.UPDATE_IS_ACTIVE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isActiveUpdated: true
        //TODO
      };
    case BrandMasterActionTypes.UPDATE_IS_ACTIVE_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
        isActiveUpdated: false
      };

    // case BrandMasterActionTypes.LOAD_BRAND_DETAILS_BY_BRAND_CODE:
    //   return {
    //     ...state,
    //     brandDetails: null,
    //     error: null,
    //     isLoading: true,
    //     isActiveUpdated: false
    //   };
    case BrandMasterActionTypes.LOAD_BRAND_DETAILS_BY_BRAND_CODE_SUCCESS:
      return {
        ...state,
        error: null,
        brandDetails: action.payload,

        isLoading: false
      };
    // case BrandMasterActionTypes.LOAD_BRAND_DETAILS_BY_BRAND_CODE_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload,
    //     isLoading: false
    //   };

    case BrandMasterActionTypes.LOAD_SUPPOERTED_CURRENCY_CODE:
      case BrandMasterActionTypes.LOAD_PARENT_BRANDS:
          case BrandMasterActionTypes.SEARCH_BRAND_BY_BRAND_CODE:

    return {
        ...state,

        error: null
      };
    case BrandMasterActionTypes.LOAD_SUPPOERTED_CURRENCY_CODE_SUCCESS:
      return {
        ...state,
        error: null,
        currency: action.payload
      };
    case BrandMasterActionTypes.LOAD_SUPPOERTED_CURRENCY_CODE_FAILURE:
      case BrandMasterActionTypes.LOAD_PARENT_BRANDS_FAILURE:
        return {
        ...state,
        error: action.payload
      };

    // case BrandMasterActionTypes.LOAD_PARENT_BRANDS:
    //   return {
    //     ...state,

    //     error: null
    //   };
    case BrandMasterActionTypes.LOAD_PARENT_BRANDS_SUCCESS:
      return {
        ...state,
        error: null,
        parentBrands: action.payload
      };
    // case BrandMasterActionTypes.LOAD_PARENT_BRANDS_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload
    //   };

    case BrandMasterActionTypes.LOAD_RESET_BRAND_DETAILS:
      return {
        ...state,
        error: null,
        isLoading: null,
        subBrandDetails: null,
        isBrandListLoading: null,
        hasSaved: null,
        hasUpdated: null
      };
    // case BrandMasterActionTypes.SEARCH_BRAND_BY_BRAND_CODE:
    //   return {
    //     ...state,
    //     error: null
    //   };
    case BrandMasterActionTypes.SEARCH_BRAND_BY_BRAND_CODE_SUCCESS:
      return {
        ...state,
        brandlist: brandAdaptor.addAll(action.payload.results, state.brandlist),
        totalElements: action.payload.totalElements,
        error: null
      };
    case BrandMasterActionTypes.SEARCH_BRAND_BY_BRAND_CODE_FAILURE:
      return {
        ...state,
        error: action.payload,
        brandlist: brandAdaptor.removeAll(state.brandlist)
      };

    case BrandMasterActionTypes.RESET_IS_ACTIVE:
      return {
        ...state,
        isActiveUpdated: false
      };
    default:
      return state;
  }
}
