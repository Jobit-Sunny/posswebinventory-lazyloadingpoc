import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { BrandMaster } from '../models/brand-model';

export interface BrandEntity extends EntityState<BrandMaster>{

}


export const brandAdaptor=createEntityAdapter<BrandMaster>({
  selectId:brandList=>brandList.brandCode
})

export const brandSelector=brandAdaptor.getSelectors()
