import { Action } from '@ngrx/store';
import { CustomErrors } from '@poss-web/core';
import { BrandMaster } from '../models/brand-model';

export interface BrandListingPayload {
  pageIndex: number;
  pageSize: number;
}

export interface BrandListing {
  results: BrandMaster[];
  totalElements: number;
}

export interface SaveBrandMasterDetailsPayload {
  brandCode: string;
  description: string;
  parentBrandCode: string;
  configDetails: {};
  orgCode: string;
  isActive: boolean;
}

export interface UpadateIsActivePayload {
  brandCode: string;
  isActive: boolean;
}
export interface UpdateBrandMasterDetailsPayload {
  brandCode: string;
  data: any;
}
export enum BrandMasterActionTypes {
  LOAD_BRAND_MASTER_LISTING = '[Brand-listing] Load Brand Master Listing',
  LOAD_BRAND_MASTER_LISTING_FAILURE = '[Brand-listing] Load Brand Master Listing Failure',
  LOAD_BRAND_MASTER_LISTING_SUCCESS = '[Brand-listing] Load Brand Master Listing Success',

  SAVE_BRAND_MASTER_DETAILS = '[Brand] Save Brand Master Details',
  SAVE_BRAND_MASTER_DETAILS_SUCCESS = '[Brand] Save Brand Master Details Success',
  SAVE_BRAND_MASTER_DETAILS_FAILURE = '[Brand] Save Brand Master Details Failure',

  UPDATE_BRAND_MASTER_DETAILS = '[Brand] Update Brand  Master Details',
  UPDATE_BRAND_MASTER_DETAILS_SUCCESS = '[Brand] Update Brand Master Details Success',
  UPDATE_BRAND_MASTER_DETAILS_FAILURE = '[Brand] Update Brand Master Details Failure',

  SAVE_SUB_BRAND_MASTER_DETAILS = '[Brand] Save Sub Brand Master Details',
  SAVE_SUB_BRAND_MASTER_DETAILS_SUCCESS = '[Brand] Save Sub Brand Master Details Success',
  SAVE_SUB_BRAND_MASTER_DETAILS_FAILURE = '[Brand] Save Sub Brand Master Details Failure',

  UPDATE_SUB_BRAND_MASTER_DETAILS = '[Brand] Update Sub Brand  Master Details',
  UPDATE_SUB_BRAND_MASTER_DETAILS_SUCCESS = '[Brand] Update Sub Brand Master Details Success',
  UPDATE_SUB_BRAND_MASTER_DETAILS_FAILURE = '[Brand] Update Sub Brand Master Details Failure',

  LOAD_BRAND_DETAILS_BY_BRAND_CODE = '[Brand-listing] Load Brand Details By Brand Code',
  LOAD_BRAND_DETAILS_BY_BRAND_CODE_SUCCESS = '[Brand-listing] Load Brand Details by Brand Code Success',
  LOAD_BRAND_DETAILS_BY_BRAND_CODE_FAILURE = '[Brand-listing] Load Brand Details by Brand Code Failure',

  UPDATE_IS_ACTIVE = '[Brand-listing] Update Is Active',
  UPDATE_IS_ACTIVE_SUCCESS = '[Brand-listing] Update Is Active Success',
  UPDATE_IS_ACTIVE_FAILURE = '[Brand-listing]Update Is Active Failure',

  LOAD_SUB_BRAND_DETAILS_BY_BRAND_CODE = '[Brand-listing] Load Sub Brand Details By Brand Code',
  LOAD_SUB_BRAND_DETAILS_BY_BRAND_CODE_SUCCESS = '[Brand-listing] Load Sub Brand Details by Brand Code Success',
  LOAD_SUB_BRAND_DETAILS_BY_BRAND_CODE_FAILURE = '[Brand-listing] Load Sub Brand Details by Brand Code Failure',

  LOAD_NEW_BRAND_DETAILS = '[Brand-listing] Load New Brand Details',
  LOAD_NEW_BRAND_DETAILS_SUCCESS = '[Brand-listing] Load New Brand Details Success',
  LOAD_NEW_BRAND_DETAILS_FAILURE = '[Brand-listing] Load New Brand Deatils Failure',

  SEARCH_BRAND_BY_BRAND_CODE = '[Brand-listing] Search Brand By Brand Code',
  SEARCH_BRAND_BY_BRAND_CODE_SUCCESS = '[Brand-listing] Search Brand By Brand Code Success',
  SEARCH_BRAND_BY_BRAND_CODE_FAILURE = '[Brand-lsiting] Search Brand By Brand Code Failure',

  LOAD_SUPPOERTED_CURRENCY_CODE = '[Brand-Listing] Load Supported Currency Code',
  LOAD_SUPPOERTED_CURRENCY_CODE_SUCCESS = '[Brand-Listing] Load Supported Currency Code Success',
  LOAD_SUPPOERTED_CURRENCY_CODE_FAILURE = '[Brand-Listing] Load Supported Currency Code Failure',

  LOAD_PARENT_BRANDS = '[Brand-listing] Load Parent Brands',
  LOAD_PARENT_BRANDS_SUCCESS = '[Brand-listing] Load Parent Brands Success',
  LOAD_PARENT_BRANDS_FAILURE = '[Brand-listing] Load Parent Brands Failure',



  LOAD_RESET_BRAND_DETAILS = '[Brand-listing] Load Reset Brand Details',
  RESET_IS_ACTIVE = '[Brand-listing] Reset Is Active'
}











export class LoadParenBrands implements Action {
  readonly type = BrandMasterActionTypes.LOAD_PARENT_BRANDS;
  constructor() { }
}

export class LoadParenBrandsSuccess implements Action {
  readonly type = BrandMasterActionTypes.LOAD_PARENT_BRANDS_SUCCESS;
  constructor(public payload: any) { }
}

export class LoadParenBrandsFailure implements Action {
  readonly type = BrandMasterActionTypes.LOAD_PARENT_BRANDS_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadSupportedCurrencyCode implements Action {
  readonly type = BrandMasterActionTypes.LOAD_SUPPOERTED_CURRENCY_CODE;
  constructor() { }
}

export class LoadSupportedCurrencyCodeSuccess implements Action {
  readonly type = BrandMasterActionTypes.LOAD_SUPPOERTED_CURRENCY_CODE_SUCCESS;
  constructor(public payload: any) { }
}

export class LoadSupportedCurrencyCodeFailure implements Action {
  readonly type = BrandMasterActionTypes.LOAD_SUPPOERTED_CURRENCY_CODE_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class UpdateIsActive implements Action {
  readonly type = BrandMasterActionTypes.UPDATE_IS_ACTIVE;
  constructor(public payload: UpadateIsActivePayload) { }
}

export class UpdateIsActiveSuccess implements Action {
  readonly type = BrandMasterActionTypes.UPDATE_IS_ACTIVE_SUCCESS;
  constructor(public payload: any) { }
}

export class UpdateIsActiveFailure implements Action {
  readonly type = BrandMasterActionTypes.UPDATE_IS_ACTIVE_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class ResetIsActiveToggle implements Action {
  readonly type = BrandMasterActionTypes.RESET_IS_ACTIVE;
}
export class LoadReset implements Action {
  readonly type = BrandMasterActionTypes.LOAD_RESET_BRAND_DETAILS;
}
export class LoadNewBrandDetails implements Action {
  readonly type = BrandMasterActionTypes.LOAD_NEW_BRAND_DETAILS;
}
export class LoadNewBrandDetailsSuccess implements Action {
  readonly type = BrandMasterActionTypes.LOAD_NEW_BRAND_DETAILS_SUCCESS;
  constructor(public payload: BrandMaster) { }
}
export class LoadNewBrandDetailsFailure implements Action {
  readonly type = BrandMasterActionTypes.LOAD_NEW_BRAND_DETAILS_FAILURE;
  constructor(public payload: CustomErrors) { }
}
export class SaveBrandMasterDetails implements Action {
  readonly type = BrandMasterActionTypes.SAVE_BRAND_MASTER_DETAILS;
  constructor(public payload: SaveBrandMasterDetailsPayload) { }
}
export class SaveBrandMasterDetailsFailure implements Action {
  readonly type = BrandMasterActionTypes.SAVE_BRAND_MASTER_DETAILS_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class SaveBrandMasterDetailsSuccess implements Action {
  readonly type = BrandMasterActionTypes.SAVE_BRAND_MASTER_DETAILS_SUCCESS;
  constructor(public payload: BrandMaster) { }
}
export class UpdateBrandMasterDetails implements Action {
  readonly type = BrandMasterActionTypes.UPDATE_BRAND_MASTER_DETAILS;
  constructor(public payload: UpdateBrandMasterDetailsPayload) { }
}
export class UpdateBrandMasterDetailsFailure implements Action {
  readonly type = BrandMasterActionTypes.UPDATE_BRAND_MASTER_DETAILS_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class UpdateBrandMasterDetailsSuccess implements Action {
  readonly type = BrandMasterActionTypes.UPDATE_BRAND_MASTER_DETAILS_SUCCESS;
  constructor(public payload: BrandMaster) { }
}

export class SaveSubBrandMasterDetails implements Action {
  readonly type = BrandMasterActionTypes.SAVE_SUB_BRAND_MASTER_DETAILS;
  constructor(public payload: BrandMaster) { }
}
export class SaveSubBrandMasterDetailsFailure implements Action {
  readonly type = BrandMasterActionTypes.SAVE_SUB_BRAND_MASTER_DETAILS_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class SaveSubBrandMasterDetailsSuccess implements Action {
  readonly type = BrandMasterActionTypes.SAVE_SUB_BRAND_MASTER_DETAILS_SUCCESS;
}
export class UpdateSubBrandMasterDetails implements Action {
  readonly type = BrandMasterActionTypes.UPDATE_SUB_BRAND_MASTER_DETAILS;
  constructor(public payload: UpdateBrandMasterDetailsPayload) { }
}
export class UpdateSubBrandMasterDetailsFailure implements Action {
  readonly type =
    BrandMasterActionTypes.UPDATE_SUB_BRAND_MASTER_DETAILS_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class UpdateSubBrandMasterDetailsSuccess implements Action {
  readonly type =
    BrandMasterActionTypes.UPDATE_SUB_BRAND_MASTER_DETAILS_SUCCESS;
}

export class LoadBrandListing implements Action {
  readonly type = BrandMasterActionTypes.LOAD_BRAND_MASTER_LISTING;
  constructor(public payload: BrandListingPayload) { }
}
export class LoadBrandListingSuccess implements Action {
  readonly type = BrandMasterActionTypes.LOAD_BRAND_MASTER_LISTING_SUCCESS;
  constructor(public payload: BrandListing) { }
}

export class LoadBrandListingFailure implements Action {
  readonly type = BrandMasterActionTypes.LOAD_BRAND_MASTER_LISTING_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadBrandDetailsByBrandCode implements Action {
  readonly type = BrandMasterActionTypes.LOAD_BRAND_DETAILS_BY_BRAND_CODE;
  constructor(public payload: string) { }
}
export class LoadBrandDetailsByBrandCodeSuccess implements Action {
  readonly type =
    BrandMasterActionTypes.LOAD_BRAND_DETAILS_BY_BRAND_CODE_SUCCESS;
  constructor(public payload: BrandMaster) { }
}

export class LoadBrandDetailsByBrandCodeFailure implements Action {
  readonly type =
    BrandMasterActionTypes.LOAD_BRAND_DETAILS_BY_BRAND_CODE_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadSubrandDetailsByBrandCode implements Action {
  readonly type = BrandMasterActionTypes.LOAD_SUB_BRAND_DETAILS_BY_BRAND_CODE;
  constructor(public payload: string) { }
}
export class LoadSubrandDetailsByBrandCodeSuccess implements Action {
  readonly type =
    BrandMasterActionTypes.LOAD_SUB_BRAND_DETAILS_BY_BRAND_CODE_SUCCESS;
  constructor(public payload: BrandMaster) { }
}

export class LoadSubrandDetailsByBrandCodeFailure implements Action {
  readonly type =
    BrandMasterActionTypes.LOAD_SUB_BRAND_DETAILS_BY_BRAND_CODE_FAILURE;
  constructor(public payload: CustomErrors) { }
}
export class SearchBrandByBrandCode implements Action {
  readonly type = BrandMasterActionTypes.SEARCH_BRAND_BY_BRAND_CODE;
  constructor(public payload: string) { }
}
export class SearchBrandByBrandCodeSuccess implements Action {
  readonly type = BrandMasterActionTypes.SEARCH_BRAND_BY_BRAND_CODE_SUCCESS;
  constructor(public payload: BrandListing) { }
}

export class SearchBrandByBrandCodeFailure implements Action {
  readonly type = BrandMasterActionTypes.SEARCH_BRAND_BY_BRAND_CODE_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export type BrandMasterActions =
  | LoadBrandListing
  | LoadBrandListingSuccess
  | LoadBrandListingFailure
  | SaveBrandMasterDetails
  | SaveBrandMasterDetailsSuccess
  | SaveBrandMasterDetailsFailure
  | UpdateBrandMasterDetails
  | UpdateBrandMasterDetailsFailure
  | UpdateBrandMasterDetailsSuccess
  | LoadBrandDetailsByBrandCode
  | LoadBrandDetailsByBrandCodeSuccess
  | LoadBrandDetailsByBrandCodeFailure
  | LoadNewBrandDetails
  | LoadNewBrandDetailsSuccess
  | LoadReset
  | SearchBrandByBrandCode
  | SearchBrandByBrandCodeSuccess
  | SearchBrandByBrandCodeFailure
  | UpdateSubBrandMasterDetails
  | UpdateSubBrandMasterDetailsFailure
  | UpdateSubBrandMasterDetailsSuccess
  | SaveSubBrandMasterDetails
  | SaveSubBrandMasterDetailsSuccess
  | SaveSubBrandMasterDetailsFailure
  | LoadSubrandDetailsByBrandCode
  | LoadSubrandDetailsByBrandCodeSuccess
  | LoadSubrandDetailsByBrandCodeFailure
  | UpdateIsActiveSuccess
  | UpdateIsActiveFailure
  | UpdateIsActive
  | LoadSupportedCurrencyCode
  | LoadSupportedCurrencyCodeSuccess
  | LoadSupportedCurrencyCodeFailure
  | LoadParenBrands
  | LoadParenBrandsSuccess
  | LoadParenBrandsFailure
  | ResetIsActiveToggle;
