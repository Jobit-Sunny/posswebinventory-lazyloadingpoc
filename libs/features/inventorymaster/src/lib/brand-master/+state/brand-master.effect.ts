import { DataPersistence } from '@nrwl/angular';
import {
  NotificationService,
  CustomErrors,
  CustomErrorAdaptor
} from '@poss-web/core';
import { BrandMasterService } from '../services/brand-master.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Effect } from '@ngrx/effects';
import { Injectable, ɵConsole } from '@angular/core';
import { BrandMasterActionTypes, BrandListing } from './brand-master.actons';
import * as BrandMasterActions from './brand-master.actons';
import { map } from 'rxjs/operators';
import { BrandMaster } from '../models/brand-model';

@Injectable()
export class BrandMasterEffect {
  constructor(
    private dataPersistence: DataPersistence<any>,
    private notificationService: NotificationService,
    private brandMasterService: BrandMasterService
  ) {}

  @Effect() loadBrandMasterListing$ = this.dataPersistence.fetch(
    BrandMasterActionTypes.LOAD_BRAND_MASTER_LISTING,
    {
      run: (action: BrandMasterActions.LoadBrandListing) => {
        return this.brandMasterService
          .getBrandMasterList(action.payload.pageIndex, action.payload.pageSize)
          .pipe(
            map(
              (brandLists: BrandListing) =>
                new BrandMasterActions.LoadBrandListingSuccess(brandLists)
            )
          );
      },
      onError: (
        action: BrandMasterActions.LoadBrandListing,
        error: HttpErrorResponse
      ) => {
        return new BrandMasterActions.LoadBrandListingFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  saveBrandMasterDetails$ = this.dataPersistence.fetch(
    BrandMasterActionTypes.SAVE_BRAND_MASTER_DETAILS,
    {
      run: (action: BrandMasterActions.SaveBrandMasterDetails) => {
        return this.brandMasterService
          .saveBrandMasterDetails(action.payload)
          .pipe(
            map((brandDetailsByBrandCode: BrandMaster) => {
              return new BrandMasterActions.SaveBrandMasterDetailsSuccess(
                brandDetailsByBrandCode
              );
            })
          );
      },
      onError: (
        action: BrandMasterActions.SaveBrandMasterDetails,
        error: HttpErrorResponse
      ) => {
        return new BrandMasterActions.SaveBrandMasterDetailsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  updateBrandMasterDetails$ = this.dataPersistence.pessimisticUpdate(
    BrandMasterActionTypes.UPDATE_BRAND_MASTER_DETAILS,
    {
      run: (action: BrandMasterActions.UpdateBrandMasterDetails) => {
        return this.brandMasterService
          .updateBrandMasterDetails(
            action.payload.data,
            action.payload.brandCode
          )
          .pipe(
            map((brandDetailsByBrandCode: BrandMaster) => {
              return new BrandMasterActions.UpdateBrandMasterDetailsSuccess(
                brandDetailsByBrandCode
              );
            })
          );
      },
      onError: (
        action: BrandMasterActions.UpdateBrandMasterDetails,
        error: HttpErrorResponse
      ) => {
        return new BrandMasterActions.UpdateBrandMasterDetailsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadNewBrandForm$ = this.dataPersistence.fetch(
    BrandMasterActionTypes.LOAD_NEW_BRAND_DETAILS,
    {
      run: (action: BrandMasterActions.LoadNewBrandDetails) => {
        return new BrandMasterActions.LoadNewBrandDetailsSuccess(
          this.brandMasterService.getNewBrandDetails()
        );
      },
      onError: (
        action: BrandMasterActions.LoadNewBrandDetails,
        error: HttpErrorResponse
      ) => {
        return new BrandMasterActions.LoadNewBrandDetailsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadBrandDetailsByBrandCode$ = this.dataPersistence.fetch(
    BrandMasterActionTypes.LOAD_BRAND_DETAILS_BY_BRAND_CODE,
    {
      run: (action: BrandMasterActions.LoadBrandDetailsByBrandCode) => {
        return this.brandMasterService
          .getBrandDetailsByBrandCode(action.payload)
          .pipe(
            map(
              (brandDetails: BrandMaster) =>
                new BrandMasterActions.LoadBrandDetailsByBrandCodeSuccess(
                  brandDetails
                )
            )
          );
      },
      onError: (
        action: BrandMasterActions.LoadBrandDetailsByBrandCode,
        error: HttpErrorResponse
      ) => {
        return new BrandMasterActions.LoadBrandDetailsByBrandCodeFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() searchBrandByBrandCode$ = this.dataPersistence.fetch(
    BrandMasterActionTypes.SEARCH_BRAND_BY_BRAND_CODE,
    {
      run: (action: BrandMasterActions.SearchBrandByBrandCode) => {
        return this.brandMasterService
          .searchBrandByBrandCode(action.payload)
          .pipe(
            map(
              (brandList: BrandListing) =>
                new BrandMasterActions.SearchBrandByBrandCodeSuccess(brandList)
            )
          );
      },
      onError: (
        action: BrandMasterActions.SearchBrandByBrandCode,
        error: HttpErrorResponse
      ) => {
        return new BrandMasterActions.SearchBrandByBrandCodeFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  saveSubBrandMasterDetails$ = this.dataPersistence.fetch(
    BrandMasterActionTypes.SAVE_SUB_BRAND_MASTER_DETAILS,
    {
      run: (action: BrandMasterActions.SaveSubBrandMasterDetails) => {
        return this.brandMasterService
          .saveBrandMasterDetails(action.payload)
          .pipe(
            map(() => {
              return new BrandMasterActions.SaveSubBrandMasterDetailsSuccess();
            })
          );
      },
      onError: (
        action: BrandMasterActions.SaveSubBrandMasterDetails,
        error: HttpErrorResponse
      ) => {
        return new BrandMasterActions.SaveSubBrandMasterDetailsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  updatesubBrandMasterDetails$ = this.dataPersistence.pessimisticUpdate(
    BrandMasterActionTypes.UPDATE_SUB_BRAND_MASTER_DETAILS,
    {
      run: (action: BrandMasterActions.UpdateSubBrandMasterDetails) => {
        return this.brandMasterService
          .updateBrandMasterDetails(
            action.payload.data,
            action.payload.brandCode
          )
          .pipe(
            map(() => {
              return new BrandMasterActions.UpdateSubBrandMasterDetailsSuccess();
            })
          );
      },
      onError: (
        action: BrandMasterActions.UpdateSubBrandMasterDetails,
        error: HttpErrorResponse
      ) => {
        return new BrandMasterActions.UpdateSubBrandMasterDetailsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  updateIsActive$ = this.dataPersistence.fetch(
    BrandMasterActionTypes.UPDATE_IS_ACTIVE,
    {
      run: (action: BrandMasterActions.UpdateIsActive) => {
        return this.brandMasterService
          .updateIsActive(action.payload.brandCode, action.payload.isActive)
          .pipe(
            map(
              (locationCodes: { id: string; description: string }[]) =>
                new BrandMasterActions.UpdateIsActiveSuccess('')
            )
          );
      },
      onError: (
        action: BrandMasterActions.UpdateIsActive,
        error: HttpErrorResponse
      ) => {
        return new BrandMasterActions.UpdateIsActiveFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadSubBrandDetailsByBrandCode$ = this.dataPersistence.fetch(
    BrandMasterActionTypes.LOAD_SUB_BRAND_DETAILS_BY_BRAND_CODE,
    {
      run: (action: BrandMasterActions.LoadSubrandDetailsByBrandCode) => {
        return this.brandMasterService
          .getBrandDetailsByBrandCode(action.payload)
          .pipe(
            map(
              (brandDetails: BrandMaster) =>
                new BrandMasterActions.LoadSubrandDetailsByBrandCodeSuccess(
                  brandDetails
                )
            )
          );
      },
      onError: (
        action: BrandMasterActions.LoadSubrandDetailsByBrandCode,
        error: HttpErrorResponse
      ) => {
        return new BrandMasterActions.LoadSubrandDetailsByBrandCodeFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadSupportedCurrencyCode$ = this.dataPersistence.fetch(
    BrandMasterActionTypes.LOAD_SUPPOERTED_CURRENCY_CODE,
    {
      run: (action: BrandMasterActions.LoadSupportedCurrencyCode) => {
        return this.brandMasterService
          .getSupportedCurrencyCode()
          .pipe(
            map(
              (currency: any) =>
                new BrandMasterActions.LoadSupportedCurrencyCodeSuccess(
                  currency
                )
            )
          );
      },
      onError: (
        action: BrandMasterActions.LoadSupportedCurrencyCode,
        error: HttpErrorResponse
      ) => {
        return new BrandMasterActions.LoadSupportedCurrencyCodeFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadParentBrand$ = this.dataPersistence.fetch(
    BrandMasterActionTypes.LOAD_PARENT_BRANDS,
    {
      run: (action: BrandMasterActions.LoadParenBrands) => {
        return this.brandMasterService
          .getParentBrandMasterList()
          .pipe(
            map(
              (parentBrands: any) =>
                new BrandMasterActions.LoadParenBrandsSuccess(parentBrands)
            )
          );
      },
      onError: (
        action: BrandMasterActions.LoadParenBrands,
        error: HttpErrorResponse
      ) => {
        return new BrandMasterActions.LoadParenBrandsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
}
