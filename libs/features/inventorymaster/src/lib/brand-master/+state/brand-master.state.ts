import { BrandEntity } from './brand-master.entity';
import { CustomErrors } from '@poss-web/core';
import { BrandMaster } from '../models/brand-model';


export interface BrandMasterState {
  brandlist: BrandEntity;
  totalElements: number;
  error: CustomErrors
  brandDetails: BrandMaster;
  subBrandDetails: BrandMaster;
  isLoading: boolean
  hasUpdated: boolean;
  hasSaved: boolean;
  currency: any,
  parentBrands: any,
  isActiveUpdated: boolean
}
