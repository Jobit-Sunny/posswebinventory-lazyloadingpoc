import { Injectable } from '@angular/core';
import {
  BrandListingPayload,
  SaveBrandMasterDetailsPayload,
  UpdateBrandMasterDetailsPayload,
  UpadateIsActivePayload
} from './brand-master.actons';
import { State } from '../../inventorymasters.state';
import { Store } from '@ngrx/store';
import * as BrandMasterActions from './brand-master.actons';
import { BrandMasterSelectors } from './brand-master.selectors';
import { BrandMasterAdaptors } from '../adaptors/brand-master-adaptors';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class BrandMasterFacade {
  constructor(private store: Store<State>) { }

  private brandList$ = this.store.select(BrandMasterSelectors.selectBrandList);
  private brandMasterDetails$ = this.store.select(
    BrandMasterSelectors.selectBrandDetails
  );
  private subBrandMasterDetails$ = this.store.select(
    BrandMasterSelectors.selectSubBrandDetails
  );
  private totalElements$ = this.store.select(
    BrandMasterSelectors.selectTotalElements
  );

  private hasSaved$ = this.store.select(BrandMasterSelectors.selectHasSaved);
  private hasUpdated$ = this.store.select(
    BrandMasterSelectors.selectHasUpdated
  );
  private hasError$ = this.store.select(BrandMasterSelectors.selecthasError);
  private isLoading$ = this.store.select(BrandMasterSelectors.selectIsLoading);
  private isActiveToggle$ = this.store.select(BrandMasterSelectors.selectIsActiveToggle);

  private currency$ = this.store.select(BrandMasterSelectors.selectCurrency)

  private parentBrandList$ = this.store.select(BrandMasterSelectors.selectParentBrandList)

  getParentBrandList() {
    return this.parentBrandList$
  }
  getSupportedCurrencyCode() {
    return this.currency$
  }

  getHasSaved() {
    return this.hasSaved$;
  }

  getHasUpdated() {
    return this.hasUpdated$;
  }

  getIsloading() {
    return this.isLoading$;
  }
  getIsActiveToggle() {
    return this.isActiveToggle$;
  }
  getError() {
    return this.hasError$;
  }

  getBrandMasterList() {
    return this.brandList$;
  }

  getTotalElements() {
    return this.totalElements$;
  }

  getSubBrandDetails() {
    return this.subBrandMasterDetails$.pipe(
      map(data => BrandMasterAdaptors.getOnlyBrandDetails(data))
    );
  }
  getOnlyBrandDetails() {
    return this.brandMasterDetails$.pipe(
      map(data => BrandMasterAdaptors.getOnlyBrandDetails(data))
    );
  }
  getOnlyCMSMSConfigurationDetails() {
    return this.brandMasterDetails$.pipe(
      map(data => BrandMasterAdaptors.getOnlyCMSMSConfigurationDetails(data))
    );
  }
  getOnlyCurrencyDetails() {
    return this.brandMasterDetails$.pipe(
      map(data => BrandMasterAdaptors.getOnlyCurrencyDetails(data))
    );
  }

  getOnlyPanCardDetails() {
    return this.brandMasterDetails$.pipe(
      map(data => BrandMasterAdaptors.getOnlyPanCardDetails(data))
    );
  }
  getOnlyResidualAmount() {
    return this.brandMasterDetails$.pipe(
      map(data => BrandMasterAdaptors.getOnlyResidualAmount(data))
    );
  }

  loadSupportedCurrencyCode() {
    this.store.dispatch(new BrandMasterActions.LoadSupportedCurrencyCode())
  }
  loadBrandMasterList(brandListingPayload: BrandListingPayload) {
    this.store.dispatch(
      new BrandMasterActions.LoadBrandListing(brandListingPayload)
    );
  }


  loadParentMasterList() {
    this.store.dispatch(
      new BrandMasterActions.LoadParenBrands()
    );
  }


  loadBrandDetailsByBrandCode(brandCode: string) {
    this.store.dispatch(
      new BrandMasterActions.LoadBrandDetailsByBrandCode(brandCode)
    );
  }

  loadSubBrandDetailsByBrandCode(brandCode: string) {
    this.store.dispatch(
      new BrandMasterActions.LoadSubrandDetailsByBrandCode(brandCode)
    );
  }
  loadNewBrandDetails() {
    this.store.dispatch(new BrandMasterActions.LoadNewBrandDetails());
  }
  saveBrandMasterDetails(
    saveBrandMasterDetailsPayload: SaveBrandMasterDetailsPayload
  ) {
    this.store.dispatch(
      new BrandMasterActions.SaveBrandMasterDetails(
        saveBrandMasterDetailsPayload
      )
    );
  }
  upDateIsactive(upadateIsActivePayload: UpadateIsActivePayload) {
    this.store.dispatch(
      new BrandMasterActions.UpdateIsActive(upadateIsActivePayload)
    );
  }
  updateBrandMasterDetails(
    updateBrandMasterDetailsPayload: UpdateBrandMasterDetailsPayload
  ) {
    this.store.dispatch(
      new BrandMasterActions.UpdateBrandMasterDetails(
        updateBrandMasterDetailsPayload
      )
    );
  }

  saveSubBrandMasterDetails(
    saveBrandMasterDetailsPayload: SaveBrandMasterDetailsPayload
  ) {
    this.store.dispatch(
      new BrandMasterActions.SaveSubBrandMasterDetails(
        saveBrandMasterDetailsPayload
      )
    );
  }
  updateSubBrandMasterDetails(
    updateBrandMasterDetailsPayload: UpdateBrandMasterDetailsPayload
  ) {
    this.store.dispatch(
      new BrandMasterActions.UpdateSubBrandMasterDetails(
        updateBrandMasterDetailsPayload
      )
    );
  }

  searchBrand(brandCode: string) {
    this.store.dispatch(
      new BrandMasterActions.SearchBrandByBrandCode(brandCode)
    );
  }
  loadReset() {
    this.store.dispatch(new BrandMasterActions.LoadReset());
  }
  resetIsActiveToggle() {
    this.store.dispatch(new BrandMasterActions.ResetIsActiveToggle());
  }
}
