import { Injectable } from '@angular/core';
import { ApiService } from '@poss-web/core';
import { BrandMasterAdaptors } from '../adaptors/brand-master-adaptors';
import { map } from 'rxjs/operators';
import {
  getBrandListUrl,
  getBrandDetialsByBrandCode,
  getSearchBrandByBrandCode,
  getSaveBrandUrl,
  getUpdateBrandUrl,
  getParentBrandListUrl
} from '../../endpoint.constants';
import { BrandMaster } from '../models/brand-model';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BrandMasterService {
  constructor(private apiservice: ApiService) {}

  getBrandMasterList(pageIndex: number, pageSize: number) {
    const url = getBrandListUrl(pageIndex, pageSize);

    return this.apiservice
      .get(url)
      .pipe(map(data => BrandMasterAdaptors.getBrandMasterList(data)));
  }

  saveBrandMasterDetails(brandDetails: BrandMaster) {
    const url = getSaveBrandUrl();
    return this.apiservice.post(url, brandDetails);
  }

  updateBrandMasterDetails(brandDetails: BrandMaster, brandCode: string) {

    const url = getUpdateBrandUrl(brandCode);
    return this.apiservice.patch(url, brandDetails);
  }

  updateIsActive(brandCode, isActive) {
    const url = getUpdateBrandUrl(brandCode);

    return this.apiservice.patch(url, { isActive: isActive });
  }
  getNewBrandDetails() {
    const data = BrandMasterAdaptors.getAllBrandDetailsByBrandCode(false);
    return data;
  }
  getBrandDetailsByBrandCode(brandCode: string) {
    const url = getBrandDetialsByBrandCode(brandCode);
    return this.apiservice
      .get(url)
      .pipe(
        map(data => BrandMasterAdaptors.getAllBrandDetailsByBrandCode(data))
      );
  }
  searchBrandByBrandCode(brandCode: string) {
    const url = getSearchBrandByBrandCode(brandCode);
    return this.apiservice
      .get(url)
      .pipe(map(data => BrandMasterAdaptors.getSearchResult(data)));
  }

  getSupportedCurrencyCode(){
    return of([{id:'1',name:'INR',},{id:'2',name:'AED'}])
  }

  getParentBrandMasterList() {
    const url = getParentBrandListUrl();

    return this.apiservice
      .get(url)
      .pipe(map(data => BrandMasterAdaptors.getParentBrandMasterList(data)));
  }
}
