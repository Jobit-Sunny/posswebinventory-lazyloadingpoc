import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnDestroy
} from '@angular/core';
import { Subject, combineLatest, Observable } from 'rxjs';
import { TEMPLATE5, HelperFunctions } from '@poss-web/shared';
import { FormGroup } from '@angular/forms';

import { MatDialog } from '@angular/material';
import { CurrencyModel } from '../../models/currency-one.model';
import { CurrencyMainModel } from '../../models/currency-main.model';
import { Currency } from '../../models/brand-model';
import { takeUntil } from 'rxjs/operators';
import {
  currencyFirstForm,
  currencyCheckboxFeild,
  inventoryCurrency,
  STNCurrency,
  masterCurrency
} from '../../models/brand-master-constants';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'poss-web-currency-step-one ',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="['pw.locationMaster.cancel', 'pw.locationMaster.save']"
      (deleteForm)="deleteButton($event)"
      (addForm)="addButton($event)"
    >
    </poss-web-dynamic-form>
  `,
  styles: []
})
export class CurrencyStepOneComponent implements OnInit, OnDestroy {
  destroy$: Subject<null> = new Subject<null>();
  @Input() currencyDetails$: Observable<Currency>;
  @Input() supportedCurrency$: Observable<any>;
  @Output() tabOne = new EventEmitter<Currency>();
  public currentStyle: string[];
  public formFields: any;
  constructor(private dialog: MatDialog, private hf: HelperFunctions) {}

  ngOnInit() {
    combineLatest(this.currencyDetails$, this.supportedCurrency$)
      .pipe(takeUntil(this.destroy$))
      .subscribe(results => {
        const form = this.prepareSet(results[0], results[1]);

        this.formFields = this.getInputs(form);
        this.currentStyle = this.getCssProp();
      });
  }

  prepareSet(currencyDetails: Currency, supportedCurrency: any) {
    let stnCurrency;
    if (supportedCurrency) {
      stnCurrency = this.hf.patchValue(
        supportedCurrency,
        'id',
        'selected',
        currencyDetails.STNCurrency,
        true
      );
    }

    let inventorycurrency
    if (supportedCurrency) {
      inventorycurrency = this.hf.patchValue(
        supportedCurrency,
        'id',
        'selected',
        currencyDetails.inventoryCurrency,
        true
      );
    }

    let mastercurrency;
    if (supportedCurrency) {
      mastercurrency = this.hf.patchValue(
        supportedCurrency,
        'id',
        'selected',
        currencyDetails.masterCurrency,
        true
      );
    }
    const currencyCheckbox = [
      {
        id: '1',
        name: 'pw.brandMaster.isInternational',
        checked: currencyDetails
          ? currencyDetails.currencyCheckbox
            ? currencyDetails.currencyCheckbox.isInternational
            : false
          : false
      }
    ];
    const currency = new CurrencyModel(
      1,
      currencyCheckbox,
      stnCurrency ? stnCurrency : true,
      inventorycurrency ? inventorycurrency : true,
      mastercurrency ? mastercurrency : true
    );
    const currencyMain = new CurrencyMainModel(1, currency);

    return currencyMain;
  }
  getCssProp() {
    const annot = (CurrencyStepOneComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form: any) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }

  public setFormConfig() {
    return {
      formName: 'Location Master Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE5
    };
  }
  addButton(formGroup: FormGroup) {
    const currencyCheckbox = {
      isInternational:
        formGroup.value[currencyFirstForm][currencyCheckboxFeild][0]
    };
    const formData = {
      currencyCheckbox: currencyCheckbox,
      inventoryCurrency: formGroup.value[currencyFirstForm][inventoryCurrency],
      STNCurrency: formGroup.value[currencyFirstForm][STNCurrency],
      masterCurrency: formGroup.value[currencyFirstForm][masterCurrency]
    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(result => {
        if (result) {
          this.tabOne.emit(formData);
        }
      });
  }

  deleteButton() {
    // this.ngOnDestroy();
    // this.destroy$ = new Subject<null>();
    // this.ngOnInit();

    // const control = formGroup.get(locationStepOneFirstSubForm).get(locationStepOneFirstSubFormregistrationNo)
    // if (!this.req) {
    //   this.req = true;
    //   control.setValidators([Validators.required]);
    // } else {
    //   this.req = false;
    //   control.setValidators([]);
    // }
    // control.updateValueAndValidity();
    // formGroup.reset();

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
