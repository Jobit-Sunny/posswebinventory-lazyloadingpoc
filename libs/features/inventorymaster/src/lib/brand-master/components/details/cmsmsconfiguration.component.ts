import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { Subject, Observable, combineLatest } from 'rxjs';

import { takeUntil } from 'rxjs/operators';
import { TEMPLATE5 } from '@poss-web/shared';

import { MatDialog } from '@angular/material';
import { CMSMSConfigurationModel } from '../../models/CM-SMS-configuration-one.model';
import { CMSMSConfigurationMainModel } from '../../models/CM-SMS-configuration.main.model';
import { CMSMSConfiguration } from '../../models/brand-model';
import { FormGroup } from '@angular/forms';
import { CMSMSConfigFirstForm, SMSUserName, SMSPassword, centralS3BucketName, centralS3AccessKey, centralS3SecretKey, centralS3RegionEndPoint, centralWebAPIURL, EPOSSServiceURL } from '../../models/brand-master-constants';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'poss-web-cmsmsconfiguration',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="[
        'pw.locationMaster.cancel',
        'pw.locationMaster.save'
      ]"
      (deleteForm)="deleteButton($event)"
      (addForm)="addButton($event)"
    >
    </poss-web-dynamic-form>
  `,
  styles: []
})
export class CMSMSConfigurationStepOneComponent implements OnInit, OnDestroy {
  @Output() tabOne = new EventEmitter<CMSMSConfiguration>();
  @Input() CMSMSConfigurationDetails$: Observable<CMSMSConfiguration>;
  destroy$: Subject<null> = new Subject<null>();
  public currentStyle: string[];
  public formFields: any;
  constructor(private dialog: MatDialog) { }

  ngOnInit() {
    combineLatest(this.CMSMSConfigurationDetails$)
      .pipe(takeUntil(this.destroy$))
      .subscribe(results => {
        const form = this.prepareSet(results[0]);

        this.formFields = this.getInputs(form);
        this.currentStyle = this.getCssProp();
      });
  }

  prepareSet(CMSMSConfigurationDetails: CMSMSConfiguration) {

    const cmsmsconfiguration = new CMSMSConfigurationModel(
      1,
      CMSMSConfigurationDetails?CMSMSConfigurationDetails.SMSUserName
        ? CMSMSConfigurationDetails.SMSUserName
        : '':'',
        CMSMSConfigurationDetails?CMSMSConfigurationDetails.SMSPassword
        ? CMSMSConfigurationDetails.SMSPassword
        : '':'',
        CMSMSConfigurationDetails?CMSMSConfigurationDetails.centralS3BucketName
        ? CMSMSConfigurationDetails.centralS3BucketName
        : '':'',
        CMSMSConfigurationDetails?CMSMSConfigurationDetails.centralS3AccessKey
        ? CMSMSConfigurationDetails.centralS3AccessKey
        : '':'',
        CMSMSConfigurationDetails? CMSMSConfigurationDetails.centralS3SecretKey
        ? CMSMSConfigurationDetails.centralS3SecretKey
        : '':'',
        CMSMSConfigurationDetails?CMSMSConfigurationDetails.centralS3RegionEndPoint
        ? CMSMSConfigurationDetails.centralS3RegionEndPoint
        : '':'',
        CMSMSConfigurationDetails?CMSMSConfigurationDetails.centralWebAPIURL
        ? CMSMSConfigurationDetails.centralWebAPIURL
        : '':'',
        CMSMSConfigurationDetails?CMSMSConfigurationDetails.EPOSSServiceURL
        ? CMSMSConfigurationDetails.EPOSSServiceURL
        : '':''
    );

    const cmsmsconfigurationMain = new CMSMSConfigurationMainModel(
      1,
      cmsmsconfiguration
    );

    return cmsmsconfigurationMain;
  }
  getCssProp() {
    const annot = (CMSMSConfigurationStepOneComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form: any) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }

  public setFormConfig() {
    return {
      formName: 'Location Master Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE5
    };
  }
  addButton(formGroup: FormGroup) {
    const formData = {

      SMSUserName: formGroup.value[CMSMSConfigFirstForm][SMSUserName],
      SMSPassword: formGroup.value[CMSMSConfigFirstForm][SMSPassword],
      centralS3BucketName:
        formGroup.value[CMSMSConfigFirstForm][centralS3BucketName],
      centralS3AccessKey:
        formGroup.value[CMSMSConfigFirstForm][centralS3AccessKey],
      centralS3SecretKey:
        formGroup.value[CMSMSConfigFirstForm][centralS3SecretKey],
      centralS3RegionEndPoint:
        formGroup.value[CMSMSConfigFirstForm][centralS3RegionEndPoint],
      centralWebAPIURL:
        formGroup.value[CMSMSConfigFirstForm][centralWebAPIURL],
      EPOSSServiceURL:
        formGroup.value[CMSMSConfigFirstForm][EPOSSServiceURL]

    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.tabOne.emit(formData);
      }
    });
  }
  deleteButton() {

    // this.ngOnDestroy();
    // this.destroy$ = new Subject<null>();
    // this.ngOnInit();

    // const control = formGroup.get(locationStepOneFirstSubForm).get(locationStepOneFirstSubFormregistrationNo)
    // if (!this.req) {
    //   this.req = true;
    //   control.setValidators([Validators.required]);
    // } else {
    //   this.req = false;
    //   control.setValidators([]);
    // }
    // control.updateValueAndValidity();
    // formGroup.reset();

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().subscribe(result => {

      if (result) {

        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });


  }
  ngOnDestroy(): void {


    this.destroy$.next();
    this.destroy$.complete();
  }
}
