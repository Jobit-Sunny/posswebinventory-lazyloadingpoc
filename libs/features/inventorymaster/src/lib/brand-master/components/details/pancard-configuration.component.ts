import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { Subject, combineLatest, Observable } from 'rxjs';
import { TEMPLATE5 } from '@poss-web/shared';
import { FormGroup } from '@angular/forms';

import { takeUntil } from 'rxjs/operators';

import { BrandStepOneComponent } from './brand.component';
import { MatDialog } from '@angular/material';
import { PanCardConfiguration } from '../../models/pancard-configuration-one.model';
import { PanCardConfigurationMainModel } from '../../models/pancard-configuration-main.model';
import { PancardConfiguration } from '../../models/brand-model';
import { panCardConfigForm, panCardConfigCheckBox, configurationAmountForCashMemo, configurationAmountForAdvance } from '../../models/brand-master-constants';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'poss-web-pancard-stepone-configuration',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="[
        'pw.locationMaster.cancel',
        'pw.locationMaster.save'
      ]"
      (deleteForm)="deleteButton($event)"
      (addForm)="addButton($event)"
    >
    </poss-web-dynamic-form>
  `,
  styles: []
})
export class PancardConfigurationStepOneComponent implements OnInit, OnDestroy {
  destroy$: Subject<null> = new Subject<null>();
  @Input() panCardDetails$: Observable<PancardConfiguration>;
  @Output() tabOne = new EventEmitter<PancardConfiguration>();
  public currentStyle: string[];
  public formFields: any;
  constructor(private dialog: MatDialog) { }

  ngOnInit() {

    combineLatest(this.panCardDetails$)
      .pipe(takeUntil(this.destroy$))
      .subscribe(results => {
        const form = this.prepareSet(results[0]);

        this.formFields = this.getInputs(form);
        this.currentStyle = this.getCssProp();
      });
  }

  prepareSet(panCardDetails: PancardConfiguration) {

    const pancardCheckkbox = [
      {
        id: '1', name: 'pw.brandMaster.isPancardmandatoryforadvance', checked: panCardDetails ? panCardDetails.pancardCheckkbox ? panCardDetails.pancardCheckkbox.isPanCardMandatoryforAdvance : false : false
      },
      { id: '2', name: 'pw.brandMaster.isPancardmandatoryforcashmemo', checked: panCardDetails ? panCardDetails.pancardCheckkbox ? panCardDetails.pancardCheckkbox.isPanCardMandatoryforCashMemo : false : false },
      { id: '3', name: 'pw.brandMaster.isPancardmandatoryforGHS', checked: panCardDetails ? panCardDetails.pancardCheckkbox ? panCardDetails.pancardCheckkbox.isPanCardMandatoryforGHS : false : false },
      { id: '4', name: 'pw.brandMaster.isPancardonsingleInvoice', checked: panCardDetails ? panCardDetails.pancardCheckkbox ? panCardDetails.pancardCheckkbox.isPanCardOnSingleInvoice : false : false }
    ];

    const pancardConfig = new PanCardConfiguration(
      1,
      panCardDetails ? panCardDetails.configurationAmountForCashMemo : '',
      panCardDetails ? panCardDetails.configurationAmountForAdvance : '',
      pancardCheckkbox
    );
    const panCardConfigMain = new PanCardConfigurationMainModel(
      1,
      pancardConfig
    );

    return panCardConfigMain;
  }

  getCssProp() {
    const annot = (BrandStepOneComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form: any) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }

  public setFormConfig() {
    return {
      formName: 'Location Master Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE5
    };
  }
  addButton(formGroup: FormGroup) {

    const pancardCheckkbox = {
      isPanCardMandatoryforAdvance:
        formGroup.value[panCardConfigForm][panCardConfigCheckBox][0],
      isPanCardMandatoryforCashMemo:
        formGroup.value[panCardConfigForm][panCardConfigCheckBox][1],
      isPanCardMandatoryforGHS:
        formGroup.value[panCardConfigForm][panCardConfigCheckBox][2],
      isPanCardOnSingleInvoice:
        formGroup.value[panCardConfigForm][panCardConfigCheckBox][3]
    };

    const formData = {
      pancardCheckkbox: pancardCheckkbox,
      configurationAmountForCashMemo:
        formGroup.value[panCardConfigForm][configurationAmountForCashMemo],
      configurationAmountForAdvance:
        formGroup.value[panCardConfigForm][configurationAmountForAdvance]
    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.tabOne.emit(formData);
      }
    });
  }

  deleteButton() {

    // this.ngOnDestroy();
    // this.destroy$ = new Subject<null>();
    // this.ngOnInit();

    // const control = formGroup.get(locationStepOneFirstSubForm).get(locationStepOneFirstSubFormregistrationNo)
    // if (!this.req) {
    //   this.req = true;
    //   control.setValidators([Validators.required]);
    // } else {
    //   this.req = false;
    //   control.setValidators([]);
    // }
    // control.updateValueAndValidity();
    // formGroup.reset();

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().subscribe(result => {

      if (result) {

        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });


  }
  ngOnDestroy(): void {


    this.destroy$.next();
    this.destroy$.complete();
  }
}
