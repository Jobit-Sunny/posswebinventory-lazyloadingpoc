import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { Subject, combineLatest, Observable } from 'rxjs';
import { TEMPLATE5 } from '@poss-web/shared';
import { FormGroup } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';

import { MatDialog } from '@angular/material';
import { ResidualAmountModel } from '../../models/residual-one.models';
import { ResidualAmountMainModel } from '../../models/residual-main.model';
import { ResidualAmount } from '../../models/brand-model';
import { residualAmount, residualAmountForeGHSTransfer } from '../../models/brand-master-constants';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';


@Component({
  selector: 'poss-web-residual-amount-step-one',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="[
        'pw.locationMaster.cancel',
        'pw.locationMaster.save'
      ]"
      (deleteForm)="deleteButton($event)"
      (addForm)="addButton($event)"
    >
    </poss-web-dynamic-form>
  `,
  styles: []
})
export class ResidualAmountStepOneComponent implements OnInit, OnDestroy {
  destroy$: Subject<null> = new Subject<null>();
  @Output() tabOne = new EventEmitter<ResidualAmount>();
  @Input() residualAmountS$: Observable<ResidualAmount>;
  public currentStyle: string[];
  public formFields: any;
  constructor(private dialog: MatDialog) { }

  ngOnInit() {

    combineLatest(this.residualAmountS$)
      .pipe(takeUntil(this.destroy$))
      .subscribe(results => {
        const form = this.prepareSet(results[0]);

        this.formFields = this.getInputs(form);
        this.currentStyle = this.getCssProp();
      });
  }

  prepareSet(residualamount: ResidualAmount) {
    const residualAmountModel = new ResidualAmountModel(
      1,
      residualamount ? residualamount.residualAmountForeGHSTransfer : ''
    );
    const residualMain = new ResidualAmountMainModel(1, residualAmountModel);
    return residualMain;
  }
  getCssProp() {
    const annot = (ResidualAmountStepOneComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form: any) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }

  public setFormConfig() {
    return {
      formName: 'Location Master Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE5
    };
  }
  addButton(formGroup: FormGroup) {
    const formData = {
      residualAmountForeGHSTransfer:
        formGroup.value[residualAmount][residualAmountForeGHSTransfer]
    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.tabOne.emit(formData);
      }
    });
  }

  deleteButton() {

    // this.ngOnDestroy();
    // this.destroy$ = new Subject<null>();
    // this.ngOnInit();

    // const control = formGroup.get(locationStepOneFirstSubForm).get(locationStepOneFirstSubFormregistrationNo)
    // if (!this.req) {
    //   this.req = true;
    //   control.setValidators([Validators.required]);
    // } else {
    //   this.req = false;
    //   control.setValidators([]);
    // }
    // control.updateValueAndValidity();
    // formGroup.reset();

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().subscribe(result => {

      if (result) {

        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });


  }
  ngOnDestroy(): void {


    this.destroy$.next();
    this.destroy$.complete();
  }
}
