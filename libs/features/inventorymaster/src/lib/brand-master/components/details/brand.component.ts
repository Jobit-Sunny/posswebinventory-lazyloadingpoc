import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnDestroy
} from '@angular/core';
import { Subject, combineLatest, Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { TEMPLATE5 } from '@poss-web/shared';

import { takeUntil } from 'rxjs/operators';

import { MatDialog } from '@angular/material';
import { BrandOne } from '../../models/brand-one.model';
import { BrandTwo } from '../../models/brand-two.model';
import { BrandMainModel } from '../../models/brand-main.model';
import { BrandMaster } from '../../models/brand-model';
import {
  brandMasterFirstSubForm,
  brandMasterOneCheckbox,
  brandShortName,
  minUtilizationPercentageforGRN,
  cashRefundLimit,
  brandMasterSecondSubForm,
  ULPServiceURL,
  dummyMobNo,
  brandDescription
} from '../../models/brand-master-constants';
import {
  brandName,

  brand


} from '../../../locationSetup/models/locationsetup.constants';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'poss-web-brand-step-one',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="['pw.locationMaster.cancel', 'pw.locationMaster.save']"
      (deleteForm)="deleteButton($event)"
      (addForm)="addButton($event)"
      (formGroupCreated)="formGroupCreated($event)"
    >
    </poss-web-dynamic-form>
  `,
  styles: []
})
export class BrandStepOneComponent implements OnInit, OnDestroy {
  @Input() brandDetails$: Observable<BrandMaster>;
  @Output() tabOne = new EventEmitter<any>(); //TODO TYPE BRAND-MASTER
  destroy$: Subject<null> = new Subject<null>();
  brandCode: string;
  public currentStyle: string[];
  public formFields: any;
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    combineLatest(this.brandDetails$)
      .pipe(takeUntil(this.destroy$))
      .subscribe(results => {
        const form = this.prepareSet(results[0]);

        this.formFields = this.getInputs(form);
        this.currentStyle = this.getCssProp();
      });
  }

  prepareSet(brandDetails: BrandMaster) {
    this.brandCode = brandDetails.brandCode;
    const brandDetailsCheckBox = [
      {
        id: '1',
        name: 'pw.brandMaster.isInterbrandTEPallowed',
        checked: brandDetails
          ? brandDetails.configDetails
            ? brandDetails.configDetails.brandConfigDetails
              ? brandDetails.configDetails.brandConfigDetails
                .brandDetailsCheckBox
                ? brandDetails.configDetails
                  ? brandDetails.configDetails.brandConfigDetails
                    ? brandDetails.configDetails.brandConfigDetails
                      .brandDetailsCheckBox.isInterbrandTEPAllowed
                    : false
                  : false
                : false
              : false
            : false
          : false
      },
      {
        id: '2',
        name: 'pw.brandMaster.referCashpaymentconfig',
        checked: brandDetails
          ? brandDetails.configDetails
            ? brandDetails.configDetails.brandConfigDetails
              ? brandDetails.configDetails.brandConfigDetails
                .brandDetailsCheckBox
                ? brandDetails.configDetails
                  ? brandDetails.configDetails.brandConfigDetails
                    ? brandDetails.configDetails.brandConfigDetails
                      .brandDetailsCheckBox.referCashPaymentConfig
                    : false
                  : false
                : false
              : false
            : false
          : false
      }
    ];

    const brandOne = new BrandOne(
      1,
      brandDetails ? brandDetails.brandCode : '',
      brandDetails
        ? brandDetails.configDetails
          ? brandDetails.configDetails.brandConfigDetails
            ? brandDetails.configDetails.brandConfigDetails.brandName
            : ''
          : ''
        : '',
      brandDetails
        ? brandDetails.configDetails
          ? brandDetails.configDetails.brandConfigDetails
            ? brandDetails.configDetails.brandConfigDetails.brandShortName
            : ''
          : ''
        : '',
      brandDetails
        ? brandDetails.configDetails
          ? brandDetails.configDetails.brandConfigDetails
            ? brandDetails.configDetails.brandConfigDetails.cashRefundLimit
            : ''
          : ''
        : '',

      brandDetailsCheckBox
    );
    const brandTwo = new BrandTwo(
      1,
      brandDetails
        ? brandDetails.configDetails
          ? brandDetails.configDetails.brandConfigDetails
            ? brandDetails.configDetails.brandConfigDetails.ULPServiceURL
            : ''
          : ''
        : '',
      brandDetails
        ? brandDetails.configDetails
          ? brandDetails.configDetails.brandConfigDetails
            ? brandDetails.configDetails.brandConfigDetails.dummyMobNo
            : ''
          : ''
        : '',
      brandDetails
        ? brandDetails.description
          ? brandDetails.description
          : ''
        : '',
      brandDetails
        ? brandDetails.configDetails
          ? brandDetails.configDetails.brandConfigDetails
            ? brandDetails.configDetails.brandConfigDetails
              .minUtilizationPercentageforGRN
            : ''
          : ''
        : ''
    );
    const brandMain = new BrandMainModel(1, brandOne, brandTwo);

    return brandMain;
  }
  getCssProp() {
    const annot = (BrandStepOneComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form: any) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }

  public setFormConfig() {
    return {
      formName: 'Location Master Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE5
    };
  }
  addButton(formGroup: FormGroup) {
    const brandDetailsCheckBox = {
      isInterbrandTEPAllowed:
        formGroup.value[brandMasterFirstSubForm][brandMasterOneCheckbox][0],
      referCashPaymentConfig:
        formGroup.value[brandMasterFirstSubForm][brandMasterOneCheckbox][1]
    };
    const configDetails = {
      brandConfigDetails: {
        brandName: formGroup.value[brandMasterFirstSubForm][brandName],
        brandShortName:
          formGroup.value[brandMasterFirstSubForm][brandShortName],
        cashRefundLimit:
          formGroup.value[brandMasterFirstSubForm][cashRefundLimit],
        minUtilizationPercentageforGRN:
          formGroup.value[brandMasterSecondSubForm][
          minUtilizationPercentageforGRN
          ],

        ULPServiceURL: formGroup.value[brandMasterSecondSubForm][ULPServiceURL],
        dummyMobNo: formGroup.value[brandMasterSecondSubForm][dummyMobNo],

        brandDetailsCheckBox: brandDetailsCheckBox
      }
    };
    const formData = {
      brandCode: formGroup.value[brandMasterFirstSubForm][brand],
      description: formGroup.value[brandMasterSecondSubForm][brandDescription],
      configDetails: configDetails
    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.tabOne.emit(formData);
      }
    });
  }

  deleteButton() {
    // this.ngOnDestroy();
    // this.destroy$ = new Subject<null>();
    // this.ngOnInit();

    // const control = formGroup.get(locationStepOneFirstSubForm).get(locationStepOneFirstSubFormregistrationNo)
    // if (!this.req) {
    //   this.req = true;
    //   control.setValidators([Validators.required]);
    // } else {
    //   this.req = false;
    //   control.setValidators([]);
    // }
    // control.updateValueAndValidity();
    // formGroup.reset();

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });
  }

  public formGroupCreated(formGroup: FormGroup) {
    if (this.brandCode) {
      formGroup
        .get('1-brandOne')
        .get('1-brandCode')
        .disable({ onlySelf: true });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
