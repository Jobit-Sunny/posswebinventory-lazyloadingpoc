import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'poss-web-brand-listng-item',
  templateUrl: './brand-listng-item.component.html',
  styleUrls: ['./brand-listng-item.component.scss']
})
export class BrandListngItemComponent implements OnInit {
@Input()listItem
@Output() emitBrandCode=new EventEmitter<any>()
@Output() emitToggle=new EventEmitter<any>()
checked:boolean

  constructor() { }

  ngOnInit() {
  }

  emitBrandcode(){

    this.emitBrandCode.emit(this.listItem.brandCode)
  }
  change(event){
    const obj={
      isActive:event.checked,
      brandCode:this.listItem.brandCode
    }
    this.emitToggle.emit(obj)
  }
}
