export interface BrandMaster {
  brandCode: string;
  description: string;
  parentBrandCode: string;
  configDetails: Configdetails;
  orgCode: string;
  isActive: boolean;
}

export interface Currency {
  inventoryCurrency: string;
  masterCurrency: string;
  STNCurrency: string;
  currencyCheckbox:any;
}
export interface CMSMSConfiguration {
  SMSUserName: string;
  SMSPassword: string;
  centralS3BucketName: string;
  centralS3AccessKey: string;
  centralS3SecretKey: string;
  centralS3RegionEndPoint: string;
  centralWebAPIURL: string;
  EPOSSServiceURL: string;
}

export interface PancardConfiguration {
  configurationAmountForCashMemo: string;
  configurationAmountForAdvance: string;
  pancardCheckkbox: any
}
export interface ResidualAmount {
  residualAmountForeGHSTransfer: string;
}

export interface Configdetails {

  brandConfigDetails?:any
  currency?: Currency;
  pancardConfiguration?: PancardConfiguration;
  CMSMSConfiguration?: CMSMSConfiguration;
  residualAmount?: ResidualAmount;
  subBrandConfig?:any
}
