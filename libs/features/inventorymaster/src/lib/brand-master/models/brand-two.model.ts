import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class BrandTwo extends DynamicFormFieldsBuilder {
  private id: number;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.ULPServiceURL',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:  'pw.inventoryMasterValidation.ULPServiceURL_pattern'}]
  })
  @Validation({
    validators: [Validators.required ,Validators.pattern('^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$')]
  })
  private ULPServiceURL: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.dummyMobNo',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:  'pw.inventoryMasterValidation.dummyMobNo_pattern'}]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9]{10}$')]
  })
  private dummyMobNo: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.description',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:  'pw.inventoryMasterValidation.maxLength_pattern'}]
  })
  @Validation({
    validators: [
      Validators.required,
      Validators.pattern('^.{1,250}$')
    ]})

  private brandDescription: string;


  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.minUtilizationPercentageforGRN',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:  'pw.inventoryMasterValidation.minUtilizationPercentageforGRN_pattern'}]
  })
  @Validation({
    validators: [
      Validators.required,
      Validators.pattern('^([0-9]|([1-9][0-9])|100)$')
    ]})

  private minUtilizationPercentageforGRN: string;

  constructor(id: number, ULPServiceURL: string, dummyMobNo: string,brandDescription:string, minUtilizationPercentageforGRN:string) {
    super();
    this.id = id;
    (this.ULPServiceURL = ULPServiceURL);
     (this.dummyMobNo = dummyMobNo);
    this.brandDescription=brandDescription
    this.minUtilizationPercentageforGRN=minUtilizationPercentageforGRN

  }
}
