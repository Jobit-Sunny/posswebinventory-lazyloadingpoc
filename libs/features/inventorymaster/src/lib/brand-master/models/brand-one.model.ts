import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class BrandOne extends DynamicFormFieldsBuilder {
  private id: number;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.brandCode',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.brandCode_pattern' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[A-Za-z0-9]{5,20}$')]
  })
  private brandCode: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.brandName'
  })
  @Validation({
    validators: [Validators.required]
  })
  private brandName: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.brandShortName',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:  'pw.inventoryMasterValidation.brandShortName_pattern'}]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[A-Za-z0-9]{1,3}$')]
  })
  private brandShortName: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.cashRefundLimit',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:  'pw.inventoryMasterValidation.maxWeightforFOC_pattern'}]
  })
  @Validation({
    validators: [
      Validators.required,
      Validators.pattern('^([0-9]*[.])?[0-9]+$')
    ]})
  private cashRefundLimit: string;

  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Class({ className: ['row'] })
  private brandCheckBox: {
    id: string;
    name: string;
    checked?: boolean;
  }[];
  constructor(
    id: number,
    brandCode: string,
    brandName: string,
    brandShortName: string,
    cashRefundLimit: string,
    brandCheckbox: { id: string; name: string; checked?: boolean }[]
  ) {
    super();
    this.id = id;
    this.brandCode = brandCode;
      this.brandName = brandName;
      this.brandShortName = brandShortName;
      this.cashRefundLimit = cashRefundLimit;
      this.brandCheckBox = brandCheckbox;
  }
}
