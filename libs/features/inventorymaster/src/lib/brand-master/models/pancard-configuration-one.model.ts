import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class PanCardConfiguration extends DynamicFormFieldsBuilder {
  private id: number;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.configurationAmountForCashMemo',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.maxWeightforFOC_pattern'}]
  })
  @Validation({
    validators: [Validators.required,  Validators.pattern('^([0-9,]{0,9})(.{1})?[0-9]{1,3}$')]
  })
  private configurationAmountForCashMemo: string;


  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.configurationAmountForAdvance',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.maxWeightforFOC_pattern'}]
  })
  @Validation({
    validators: [Validators.required,   Validators.pattern('^([0-9,]{0,9})(.{1})?[0-9]{1,3}$')]
  })
  private configurationAmountForAdvance: string;


  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Class({ className: ['row'] })
  private pancardCheckkbox: {
    id: string;
    name: string;
    checked?: boolean;
  }[];
  constructor(
    id: number,
    configurationAmountForCashMemo:string,
    configurationAmountForAdvance:string,
    pancardCheckkbox: { id: string; name: string; checked?: boolean }[],

  ) {
    super();
    this.id = id;
    this.configurationAmountForCashMemo=configurationAmountForCashMemo;
    this.configurationAmountForAdvance=configurationAmountForAdvance;
    this.pancardCheckkbox=pancardCheckkbox;
  }
}
