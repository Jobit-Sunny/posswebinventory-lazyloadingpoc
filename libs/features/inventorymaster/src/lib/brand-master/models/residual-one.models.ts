import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class ResidualAmountModel extends DynamicFormFieldsBuilder {
  private id: number;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.residualAmountForeGHSTransfer',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.maxWeightforFOC_pattern'}]
  })
  @Validation({
    validators: [
      Validators.required,
      Validators.pattern('^([0-9,]{0,9})(.{1})?[0-9]{1,3}$')
    ]
  })
  private residualAmountForeGHSTransfer: string;
  constructor(id: number, residualAmountForeGHSTransfer: string) {
    super();
    this.id = id;
    this.residualAmountForeGHSTransfer = residualAmountForeGHSTransfer;
  }
}
