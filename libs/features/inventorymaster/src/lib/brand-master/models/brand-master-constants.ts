//brand
export const brandMasterFirstSubForm='1-brandOne'
export const brandMasterSecondSubForm='1-brandTwo'
export const brandMasterOneCheckbox='1-brandCheckBox'
export const brandCode='1-brandCode'
export const brandName='1-brandName'
export const cashRefundLimit='1-cashRefundLimit'
export const brandShortName='1-brandShortName'
export const ULPServiceURL='1-ULPServiceURL'
export const dummyMobNo='1-dummyMobNo'
export const brandDescription='1-brandDescription'
export const minUtilizationPercentageforGRN = '1-minUtilizationPercentageforGRN'

//CMSMS Configuration
export const CMSMSConfigFirstForm='1-CMSMSConfigurationModel'
export const SMSUserName='1-SMSUserName'
export const SMSPassword='1-SMSPassword'
export const centralS3BucketName='1-centralS3BucketName'
export const centralS3AccessKey= '1-centralS3AccessKey'
export const centralS3SecretKey='1-centralS3SecretKey'
export const centralS3RegionEndPoint='1-centralS3RegionEndPoint'
export const centralWebAPIURL='1-centralWebAPIURL'
export const EPOSSServiceURL='1-EPOSSServiceURL'

//CurrencyConfiguration
export const currencyFirstForm='1-currencyModel'
export const currencyCheckboxFeild='1-currencyCheckbox'
export const inventoryCurrency='1-inventoryCurrency'
export const  STNCurrency='1-STNCurrency'
export const masterCurrency='1-masterCurrency'

//PanCardConfiguration
export const panCardConfigForm='1-panCardConfiguration'
export const panCardConfigCheckBox='1-pancardCheckkbox'
export const configurationAmountForCashMemo='1-configurationAmountForCashMemo'
export const configurationAmountForAdvance='1-configurationAmountForAdvance'

//Residual Amount

export const residualAmount='1-residualAmountModel'
export const residualAmountForeGHSTransfer='1-residualAmountForeGHSTransfer'
