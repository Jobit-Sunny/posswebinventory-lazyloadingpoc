import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class CMSMSConfigurationModel extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.SMSUserName'
  })
  @Validation({
    validators: [Validators.required]
  })
  private SMSUserName: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.SMSPassword'
  })
  @Validation({
    validators: [Validators.required]
  })
  private SMSPassword: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.centralS3BucketName'
  })
  @Validation({
    validators: [Validators.required]
  })
  private centralS3BucketName: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.centralS3AccessKey'
  })
  @Validation({
    validators: [Validators.required]
  })
  private centralS3AccessKey: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.centralS3SecretKey'
  })
  @Validation({
    validators: [Validators.required]
  })
  private centralS3SecretKey: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.centralS3RegionEndPoint'
  })
  @Validation({
    validators: [Validators.required]
  })
  private centralS3RegionEndPoint: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.centralWebAPIURL',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:  'pw.inventoryMasterValidation.ULPServiceURL_pattern'}]
  })
  @Validation({
    validators: [
      Validators.required,
      Validators.pattern(
        '^(http://www.|https://www.|http://|https://)[a-z0-9]+([-.]{1}[a-z0-9]+)*.[a-z]{2,5}(:[0-9]{1,5})?(/.*)?$'
      )
    ]
  })
  private centralWebAPIURL: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.EPOSSServiceURL',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:'pw.inventoryMasterValidation.ULPServiceURL_pattern'}]
  })
  @Validation({
    validators: [
      Validators.required,
      Validators.pattern(
        '^(http://www.|https://www.|http://|https://)[a-z0-9]+([-.]{1}[a-z0-9]+)*.[a-z]{2,5}(:[0-9]{1,5})?(/.*)?$'
      )
    ]
  })
  private EPOSSServiceURL: string;

  constructor(
    id: number,
    SMSUserName: string,
    SMSPassword: string,
    centralS3BucketName: string,
    centralS3AccessKey: string,
    centralS3SecretKey: string,
    centralS3RegionEndPoint: string,
    centralWebAPIURL: string,
    EPOSSServiceURL: string
  ) {
    super();
    this.id = id;
    this.SMSUserName = SMSUserName;
      this.SMSPassword = SMSPassword;
    this.centralS3BucketName = centralS3BucketName;
      this.centralS3AccessKey = centralS3AccessKey;
    this.centralS3SecretKey = centralS3SecretKey;
      this.centralS3RegionEndPoint = centralS3RegionEndPoint;
      this.centralWebAPIURL = centralWebAPIURL;
      this.EPOSSServiceURL = EPOSSServiceURL;
  }
}
