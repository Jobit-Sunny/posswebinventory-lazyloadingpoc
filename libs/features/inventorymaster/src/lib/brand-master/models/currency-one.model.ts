import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class CurrencyModel extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'selected'
    },
    label:'pw.brandMaster.inventoryCurrency'
  })
  private inventoryCurrency: string;

  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'selected'
    },
    label:'pw.brandMaster.STNCurrency'
  })
  private STNCurrency: string;

  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'selected'
    },
    label:'pw.brandMaster.masterCurrency'
  })
  private masterCurrency: string;



  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Class({ className: ['row'] })
  private currencyCheckbox: {
    id: string;
    name: string;
    checked?: boolean;
  }[];
  constructor(
    id: number,
    currencyCheckbox: { id: string; name: string; checked?: boolean }[],
    inventoryCurrency:string,
    STNCurrency:string,
    masterCurrency:string

  ) {
    super();
    this.id = id;
    this.currencyCheckbox=currencyCheckbox;
    this.inventoryCurrency=inventoryCurrency;
    this.STNCurrency=STNCurrency;
    this.masterCurrency=masterCurrency;

  }
}
