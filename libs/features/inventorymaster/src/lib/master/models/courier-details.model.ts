import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class CourierDetailsModel extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.courierDetails.courierName'
  })
  @Class({ className: ['col-11'] })
  @Validation({
    validators: [Validators.required]
  })
  private courierName: string;

  @FormField({
    fieldType: FormFieldType.TOGGLE,
    selectOptionKeys: { labelKey: 'name', valueKey: 'id', selectedKey: 'checked' },
    label: ''
  })
  @Class({ className: ['col-1'] })
  private isActive: { id: string, name: string, checked?: boolean }[];

  @FormField({
    fieldType: FormFieldType.TEXT_AREA,
    label: 'pw.courierDetails.addressLabel'
  })
  @Class({ className: ['col-6 wdth'] })
  @Validation({
    validators: [Validators.required]
  })
  private address: string;
  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'selected',
      foreignKey: 'countryCode'
    },
    label: 'pw.courierDetails.stateLabel'
  })
  @Class({ className: ['col-6 wdth'] })
  @Validation({ validators: [Validators.required] })
  private state: { id: string; name: string }[];
  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      foreignKey: 'state_id',
      selectedKey: 'selected'
    },
    label: 'pw.courierDetails.cityLabel',
    dependsOn: '1-state'
  })
  @Class({ className: ['col-6 wdth'] })
  @Validation({ validators: [Validators.required] })
  private city: { id: string; name: string; state_id: string }[];
  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.courierDetails.emailIdLabel',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.invalidEmailId' }]
  })
  @Class({ className: ['col-6 wdth'] })
  @Validation({
    validators: [
      Validators.required,
      Validators.pattern(
        '^[a-zA-Z0-9.!#$%&*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$'
      )
    ]
  })
  private emailId: string;
  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.courierDetails.phoneNumberLabel',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.invalidMobileNumber' }]
  })
  @Class({ className: ['col-6 wdth'] })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9]{10}$')]
  })
  private phoneNumber: string;
  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.courierDetails.mobileNumberLabel',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.invalidMobileNumber' }]
  })
  @Class({ className: ['col-6 wdth'] })
  @Validation({
    validators: [Validators.pattern('^[0-9]{10}$')]
  })
  private mobileNumber: string;
  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.courierDetails.contactPersonLabel'
  })
  @Class({ className: ['col-6 wdth'] })
  @Validation({
    validators: []
  })
  private contactPerson: string;
  constructor(
    id: number,
    courierName: string,
    isActive: { id: string, name: string, checked?: boolean }[],
    address: string,
    state: { id: string; name: string; selected?: boolean }[],
    city: { id: string; name: string; selected?: boolean; state_id: string }[],
    emialId: string,
    phoneNumber: string,
    mobileNumber: string,
    contactPerson: string
  ) {
    super();
    this.id = id;
    this.courierName = courierName;
    this.isActive = isActive
    this.address = address;
    this.state = state;
    this.city = city;
    this.emailId = emialId;
    this.phoneNumber = phoneNumber;
    this.mobileNumber = mobileNumber;
    this.contactPerson = contactPerson;
  }
}
