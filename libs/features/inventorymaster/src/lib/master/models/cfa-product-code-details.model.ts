import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class CFAProductCodeDetails extends DynamicFormFieldsBuilder {
  //one.....................
  private id: number;
  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.CFAProduct.CFAProductCode',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.CFAProduct.CFAProductCode_pattern' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9A-Za-z]*$')]
  })
  private CFAProductCode: string;
  //two.....................
  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Validation({ validators: [] })
  @Class({ className: ['row'] })
  private isActive: { id: string; name: string; checked?: boolean }[];
  //three.....................
  @FormField({
    fieldType: FormFieldType.TEXT_AREA,
    label: 'pw.CFAProduct.description',
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^.{1,250}$')]
  })
  private description: string;
  //four............................
  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Validation({ validators: [] })
  @Class({ className: ['row'] })
  private isMia: { id: string; name: string; checked?: boolean }[];
  //five..............................
  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'selected'
    },
    label: 'pw.CFAProduct.productCategory'
  })
  @Validation({ validators: [] })
  private productCategory: { id: string; name: string; selected?: boolean }[];
  //six..........................................
  // @FormField({
  //   fieldType: FormFieldType.SELECT,
  //   selectOptionKeys: {
  //     labelKey: 'name',
  //     valueKey: 'id',
  //     selectedKey: 'selected'
  //   },
  //   label: 'pw.CFAProduct.productCategoryFlag'
  // })
  // @Validation({ validators: [] })
  // private productCategoryFlag: {
  //   id: string;
  //   name: string;
  //   selected?: boolean;
  // }[];
  //seven..............................................
  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'selected'
    },
    label: 'pw.CFAProduct.metalType'
  })
  @Validation({ validators: [] })
  private metalType: { id: string; name: string; selected?: boolean }[];
  //eight.............................................
  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Validation({ validators: [] })
  @Class({ className: ['row'] })
  private goldPriceMandatory: { id: string; name: string; checked?: boolean }[];
  //nine............................................
  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Validation({ validators: [] })
  @Class({ className: ['row'] })
  private platinumPriceMandatory: {
    id: string;
    name: string;
    checked?: boolean;
  }[];
  //ten...........................................
  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Validation({ validators: [] })
  @Class({ className: ['row'] })
  private makingChargesMandatory: {
    id: string;
    name: string;
    checked?: boolean;
  }[];
  constructor(
    id: number,
    CFAProductCode: string,
    isActive: { id: string; name: string; checked?: boolean }[],
    description: string,
    isMia: { id: string; name: string; checked?: boolean }[],
    productCategory: { id: string; name: string; selected?: boolean }[],
    // productCategoryFlag: { id: string; name: string; selected?: boolean }[],
    metalType: { id: string; name: string; selected?: boolean }[],
    goldPriceMandatory: { id: string; name: string; checked?: boolean }[],
    platinumPriceMandatory: { id: string; name: string; checked?: boolean }[],
    makingChargesMandatory: { id: string; name: string; checked?: boolean }[]
  ) {
    super();
    this.id = id;
    this.CFAProductCode = CFAProductCode;
    this.isActive = isActive;
    this.description = description;
    this.isMia = isMia;
    this.productCategory = productCategory;
    //this.productCategoryFlag = productCategoryFlag;
    this.metalType = metalType;
    this.goldPriceMandatory = goldPriceMandatory;
    this.platinumPriceMandatory = platinumPriceMandatory;
    this.makingChargesMandatory = makingChargesMandatory;
  }
}
