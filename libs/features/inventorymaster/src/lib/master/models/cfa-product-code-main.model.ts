import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType
} from '@poss-web/shared';
import { CFAProductCodeDetails } from './cfa-product-code-details.model';
import { CFAProductCodeCheckItOuts } from './cfa-product-code-checkitout.model';

export class CFAProductCodeMainModel extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.SUB_FORM,
    label: 'pw.CFAProduct.cfaProductDetails',
    hide: false
  })
  private CFAProductCodeDetails: CFAProductCodeDetails;

  @FormField({
    fieldType: FormFieldType.SUB_FORM,
    label: 'pw.CFAProduct.cfaProductDetails',
    hide: false
  })
  private CFAProductCodeCheckItOuts: CFAProductCodeCheckItOuts;
  constructor(
    id: number,
    CFAProductCodeDetails: CFAProductCodeDetails,
    CFAProductCodeCheckItOuts: CFAProductCodeCheckItOuts
  ) {
    super();
    this.id = id;
    this.CFAProductCodeDetails = CFAProductCodeDetails;
    this.CFAProductCodeCheckItOuts = CFAProductCodeCheckItOuts;
  }
}
