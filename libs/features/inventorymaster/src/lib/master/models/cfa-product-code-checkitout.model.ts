import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';

export class CFAProductCodeCheckItOuts extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Class({ className: ['row'] })
  private checkBoxes: { id: string; name: string; checked?: boolean }[];

  @FormField({
    fieldType: FormFieldType.RADIO,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: 'pw.CFAProduct.dynamicF1Calculation'
  })
  @Class({ className: ['row'] })
  private dynamicF1CalculationYesOrNo: {
    id: string;
    name: string;
    checked?: boolean;
  }[];

  constructor(
    id: number,
    checkBoxes: { id: string; name: string; checked?: boolean }[],
    dynamicF1CalculationYesOrNo: {
      id: string;
      name: string;
      checked?: boolean;
    }[]
  ) {
    super();
    this.id = id;
    this.checkBoxes = checkBoxes;
    this.dynamicF1CalculationYesOrNo = dynamicF1CalculationYesOrNo;
  }
}
