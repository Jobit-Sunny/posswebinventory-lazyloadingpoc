export const getNotFoundRouteUrl = (): string => {
    return `404`;
};

/**
 * Inventory Routing Urls
 */

export const getInventoryMasterRouteUrl = (): string => {
    return `/master`;
};

export const getProductMasterDashboardRouteUrl = (): string => {
    return getInventoryMasterRouteUrl() + `/productMasters`;
};

export const getLocationMasterDashboardRouteUrl = (): string => {
    return getInventoryMasterRouteUrl() + `/location`;
};

export const getInventoryMasterDashboardRouteUrl = (): string => {
    return getInventoryMasterRouteUrl() + `/inventory`;
};

export const getItemMasterDashboardRouteUrl = (): string => {
    return getInventoryMasterRouteUrl() + `/itemMaster`;
};


export const getProductMasterBrandListRouteUrl = (): string => {
    return getInventoryMasterRouteUrl() + `/productMasters/brandlist`;
};

export const getProductMasterSubBrandListRouteUrl = (): string => {
    return getInventoryMasterRouteUrl() + `/productMasters/subbrandlist`;
};

export const getProductMasterProductCategoryListRouteUrl = (): string => {
    return getInventoryMasterRouteUrl() + `/productMasters/productCategory-list`;
};

export const getProductMasterCFAProductListRouteUrl = (): string => {
    return getInventoryMasterRouteUrl() + `/productMasters/CFAProducts`;
};

export const getProductMasterItemListRouteUrl = (): string => {
    return getInventoryMasterRouteUrl() + `/itemMaster`;
};



export const getLocationMasterListRouteUrl = (): string => {
    return getInventoryMasterRouteUrl() + `/location/location-list`;
};

export const getCorporateTownListRouteUrl = (): string => {
    return getInventoryMasterRouteUrl() + `/location/corporate-towns`;
};



export const getBrandDetailsRouteUrl = (brandCode: string): string => {
    return getInventoryMasterRouteUrl() + `/productMaster/${brandCode}/brand/1`;
};

export const getBrandListRouteUrl = (): string => {
    return getInventoryMasterRouteUrl() + `/productMasters/brandlist`;
};

export const getCourierDetailsListRouteUrl = (courierName?: string): string => {
    return getInventoryMasterRouteUrl() + `/inventory/courierDetailsListing/${courierName}`;
};


export const getBinGroupRouteUrl = (): string => {
    return getInventoryMasterRouteUrl() + `/inventory/binGroup`;
};

export const getBinCodeRouteUrl = (): string => {
    return getInventoryMasterRouteUrl() + `/inventory/binCode`;
};

export const getWeighttoleranceRouteUrl = (): string => {
    return getInventoryMasterRouteUrl() + `/inventory/weighttolerance`;
};

export const getWeighttoleranceEditRouteUrl = (configId: string): string => {
    return getInventoryMasterRouteUrl() + `/inventory/weighttolerance/edit/${configId}`;
};

export const getWeighttoleranceDetailsRouteUrl = (): string => {
    return getInventoryMasterRouteUrl() + `/inventory/weighttolerance/details`;
};

export const getLocationListRouteUrl = (): string => {
    return getInventoryMasterRouteUrl() + `/location/location-list`;
};

export const getCFAProductsRouteUrl = (): string => {
    return getInventoryMasterRouteUrl() + `/productMasters/CFAProducts`;
};


export const getItemDetailsRouteUrl = (itemCode: string): string => {
    return getInventoryMasterRouteUrl() + `/itemMaster/${encodeURIComponent(itemCode)}/itemDetails`;
};


export const getLocationDetailsLocationRouteUrl = (locationCode: string, page: string): string => {
    return getInventoryMasterRouteUrl() + `/locationMasters/${locationCode}/location/${page}`;
};

export const getLocationDetailsGHSRouteUrl = (locationCode: string, page: string): string => {
    return getInventoryMasterRouteUrl() + `/locationMasters/${locationCode}/ghs/${page}`;
};

export const getLocationDetailsPrintRouteUrl = (locationCode: string): string => {
    return getInventoryMasterRouteUrl() + `/locationMasters/${locationCode}/print/`;
};

export const getLocationDetailsGRNRouteUrl = (locationCode: string, page: string): string => {
    return getInventoryMasterRouteUrl() + `/locationMasters/${locationCode}/grn/${page}`;
};

export const getLocationDetailsLoyalityRouteUrl = (locationCode: string, page: string): string => {
    return getInventoryMasterRouteUrl() + `/locationMasters/${locationCode}/loyality/${page}`;
};

export const getLocationDetailsAdvanceRouteUrl = (locationCode: string, page: string): string => {
    return getInventoryMasterRouteUrl() + `/locationMasters/${locationCode}/advance/${page}`;
};



// RouterLinks
export const getBrandMasterBrandRouterLink = (page: string): string => {
    return `brand/${page}`;
};

export const getBrandMasterPancardRouterLink = (page: string): string => {
    return `pancard/${page}`;
};

export const getBrandMasterResidualamountdRouterLink = (page: string): string => {
    return `residualamount/${page}`;
};

export const getBrandMasterSMSConfigRouterLink = (page: string): string => {
    return `SMSConfig/${page}`;
};

export const getBrandMasterCurrencyRouterLink = (page: string): string => {
    return `currency/${page}`;
};

export const getLovMasterRouterLink = (): string => {
    return `lovmaster`;
};

export const getLocationMasterLocationRouterLink = (): string => {
    return `location`;
};

export const getLocationMasterGhsRouterLink = (): string => {
    return `ghs`;
};

export const getLocationMasterPrintRouterLink = (): string => {
    return `print`;
};

export const getLocationMasterGrnRouterLink = (): string => {
    return `grn`;
};

export const getLocationMasterLoyalityRouterLink = (): string => {
    return `loyality`;
};

export const getLocationMasterAdvanceRouterLink = (): string => {
    return `advance`;
};