import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocationMasterFacade } from '../../../+state/location-master.facade';
import { Observable, Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationEventType
} from '@poss-web/shared';
import { CustomErrors } from '@poss-web/core';
import { getLocationListRouteUrl, getLocationDetailsLocationRouteUrl, getLocationDetailsPrintRouteUrl, getLocationDetailsGRNRouteUrl, getLocationDetailsLoyalityRouteUrl, getLocationDetailsAdvanceRouteUrl, getLocationDetailsGHSRouteUrl, getLocationMasterLocationRouterLink, getLocationMasterGhsRouterLink, getLocationMasterPrintRouterLink, getLocationMasterGrnRouterLink, getLocationMasterAdvanceRouterLink, getLocationMasterLoyalityRouterLink } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-location-setup',
  templateUrl: './mainlocation.component.html',
  styleUrls: ['./mainlocation.component.scss']
})
export class MainLocationComponent implements OnInit, OnDestroy {
  isLoading$: Observable<boolean>;
  isNew: boolean;
  destroy$: Subject<null> = new Subject<null>();

  locationUrl: string;
  ghsUrl: string;
  printUrl: string;
  grnUrl: string;
  loyalityUrl: string;
  advanceUrl: string;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public loactionMasterFacade: LocationMasterFacade,
    private translate: TranslateService,
    private overlayNotification: OverlayNotificationService,
    private locationMasterFacade: LocationMasterFacade
  ) { }

  ngOnInit() {
    this.loactionMasterFacade.loadCountry();
    this.loactionMasterFacade.loadStates();
    this.loactionMasterFacade.loadTowns();
    this.loactionMasterFacade.loadlocationTypes();
    this.loactionMasterFacade.loadRegion();
    this.loactionMasterFacade.loadMarketCode();
    this.loactionMasterFacade.loadBrands();
    this.loactionMasterFacade.loadOwnerInfo();
    this.loactionMasterFacade.loadBaseCurrency();
    this.loactionMasterFacade.loadLocationFormat();
    this.loactionMasterFacade.loadPaymentCode();
    this.loactionMasterFacade.loadPriceGroup();

    this.isLoading$ = this.loactionMasterFacade.getLocationLoading();
    // this.loactionMasterFacade.loadOwnerInfo();

    const locationCode = this.activatedRoute.snapshot.params['locCode'];
    if (locationCode === 'NEW') {
      this.loactionMasterFacade.loadNewLocationAction();
    } else {
      this.loactionMasterFacade.loadLocationByLocationCode(locationCode);
    }

    this.activatedRoute.paramMap
      .pipe(takeUntil(this.destroy$))
      .subscribe(params => {
        if (params['params']['locCode'] === 'NEW') {
          this.isNew = true;
        } else {
          this.isNew = false;
        }
      });

    this.locationMasterFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });


    this.locationUrl = getLocationMasterLocationRouterLink();
    this.ghsUrl = getLocationMasterGhsRouterLink();
    this.printUrl = getLocationMasterPrintRouterLink();
    this.grnUrl = getLocationMasterGrnRouterLink();
    this.loyalityUrl = getLocationMasterLoyalityRouterLink();
    this.advanceUrl = getLocationMasterAdvanceRouterLink();
  }

  back() {
    this.router.navigate([getLocationListRouteUrl()]);
    this.loactionMasterFacade.loadReset();
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasBackdrop: true,
        hasClose: true,
        error: error
      })
      .events.pipe(take(1))
      .subscribe();
  }
}
