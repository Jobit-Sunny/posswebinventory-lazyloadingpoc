import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LocationMasterFacade } from '../../../+state/location-master.facade';
import { Observable, Subject, of } from 'rxjs';
import {
  OverlayNotificationEventType,
  OverlayNotificationType,
  OverlayNotificationService,
  OverlayNotificationEventRef
} from '@poss-web/shared';
import { CustomErrors } from '@poss-web/core';
import { takeUntil, skipUntil, take } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { getLocationDetailsGHSRouteUrl } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-location-ghs',
  templateUrl: './location-ghs.component.html'
})
export class LocationGhsComponent implements OnInit, OnDestroy {
  locationCode: any;

  constructor(
    private route: ActivatedRoute,
    private overlayNotification: OverlayNotificationService,
    private locationMasterFacade: LocationMasterFacade,
    private translate: TranslateService
  ) { }
  page: string;
  disabled = true;
  ghsData$: Observable<any>;
  marketCode$: Observable<any>;
  baseCurrency$: Observable<any>;
  destroy$: Subject<null> = new Subject<null>();
  isLoading$: Observable<boolean>;
  locationDetails$: Observable<any>;

  ngOnInit() {
    this.isLoading$ = this.locationMasterFacade.getLocationLoading();

    this.routerPage();
    this.getDetails();

    this.locationMasterFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.locationMasterFacade
      .getIsSaved()
      .pipe(
        skipUntil(of(true)),
        takeUntil(this.destroy$)
      )
      .subscribe(saved => {
        if (saved === true) {
          this.locationMasterFacade.resetIsSaved();
          this.showNotification('pw.locationMaster.savedSuccessfully');
        }
      });
  }

  getDetails() {
    // this.marketCode$ = this.locationMasterFacade.getMarketCode();
    this.ghsData$ = this.locationMasterFacade.getGhsByLocationCode();
    this.baseCurrency$ = this.locationMasterFacade.getBaseCurrencyTypes();

    this.locationDetails$ = this.locationMasterFacade.getLocationOnlyByLocationCode();
  }

  addButtonOne(location: { baseCurrency: { toString: () => void; }; suspendingCNs: string | number; transferredCNs: string | number; activatedCNs: string | number; ddValidityDays: string | number; consolidateAttempts: string | number; realisationDays: string | number; validityDays: string | number; }) {
    const fromPath = this.route.pathFromRoot[2];
    this.locationCode = fromPath.snapshot.params['locCode'];

    const configDetails = {
      ghsStepOne: {
        dayDetails: {
          baseCurrency: location.baseCurrency.toString(),
          suspendingCNs: +location.suspendingCNs,
          transferredCNs: +location.transferredCNs,
          activatedCNs: +location.activatedCNs,
          ddValidityDays: +location.ddValidityDays,
          consolidateAttempts: +location.consolidateAttempts,
          realisationDays: +location.realisationDays,
          validityDays: +location.validityDays
        }
      }
    };
    this.locationMasterFacade.saveFormDetails({
      locationCode: this.locationCode,
      data: {
        configDetails
      }
    });
  }

  addButtonTwo(formChecks: { checkBoxes: any; }) {
    const fromPath = this.route.pathFromRoot[2];
    this.locationCode = fromPath.snapshot.params['locCode'];
    const configDetails = {
      ghsStepTwo: {
        ghsIbtCheckBox: formChecks.checkBoxes
      }
    };
    this.locationMasterFacade.saveFormDetails({
      locationCode: this.locationCode,
      data: {
        configDetails
      }
    });
  }

  routerPage() {
    this.route.paramMap.pipe(takeUntil(this.destroy$)).subscribe(params => {
      this.page = params.get('page');
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  showNotification(key: string) {
    this.overlayNotification.close();

    this.translate
      .get(key)
      .pipe(take(1))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification.show({
          type: OverlayNotificationType.TIMER,
          message: translatedMsg,
          hasClose: true,
          hasBackdrop: true
        });
      });
  }

  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error,
        hasBackdrop: true
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => { });
  }

  getGhsDetailsRouteUrl(page: string) {
    const fromPath = this.route.pathFromRoot[2];
    this.locationCode = fromPath.snapshot.params['locCode'];
    return getLocationDetailsGHSRouteUrl(this.locationCode, page);
  }
}
