import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LocationMasterFacade } from '../../../+state/location-master.facade';
import { Observable, Subject, of } from 'rxjs';
import {
  OverlayNotificationService,
  OverlayNotificationType
} from '@poss-web/shared';
import { CustomErrors } from '@poss-web/core';
import { takeUntil, take, skipUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { getLocationDetailsLoyalityRouteUrl } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-location-loyality-gep-gc-tep',
  templateUrl: './location-loyality-gep-gc-tep.component.html'
})
export class LocationLoyalityGepGcTepComponent implements OnInit, OnDestroy {
  loyalityDetails$: Observable<any>;
  destroy$: Subject<null> = new Subject<null>();
  hasError: Observable<CustomErrors>;
  isLoading$: Observable<boolean>;
  constructor(
    private route: ActivatedRoute,
    private locationMasterFacade: LocationMasterFacade,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService
  ) { }
  page: string;
  locationCode: string;
  locationDetails$: Observable<any>;

  ngOnInit() {
    this.locationMasterFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.locationMasterFacade
      .getIsSaved()
      .pipe(
        skipUntil(of(true)),
        takeUntil(this.destroy$)
      )
      .subscribe(saved => {
        if (saved === true) {
          this.locationMasterFacade.resetIsSaved();
          this.showNotification('pw.locationMaster.savedSuccessfully');
        }
      });

    this.routerPage();
    this.hasError = this.locationMasterFacade.getError();
    const fromPath = this.route.pathFromRoot[2];
    this.locationCode = fromPath.snapshot.params['locCode'];
    this.loyalityDetails$ = this.locationMasterFacade.getLoyalityByLocationCode();
    this.isLoading$ = this.locationMasterFacade.getLocationLoading();
    this.locationDetails$ = this.locationMasterFacade.getLocationOnlyByLocationCode();
  }

  routerPage() {
    this.route.paramMap.subscribe(params => {
      this.page = params.get('page');
    });
  }
  updateLoyalityOne(loyalityStepOne) {
    const configDetails = {
      loyalityStepOne: {
        loyality: loyalityStepOne.loyality,
        gvPayment: loyalityStepOne.gvPayment,
        personalDetails: {
          gepPureGoldPurity: loyalityStepOne.gepPureGoldPurity,
          gepPureSilverPurity: loyalityStepOne.gepPureSilverPurity,
          gepPurePlatinumPurity: loyalityStepOne.gepPurePlatinumPurity,
          gepStandardDeductionGold: loyalityStepOne.gepStandardDeductionGold,
          gepStandardDeductionSilver:
            loyalityStepOne.gepStandardDeductionSilver,
          gepStandardDeductionPlatinum:
            loyalityStepOne.gepStandardDeductionPlatinum,
          enableGEPSale: loyalityStepOne.enableGEPSale
        }
      }
    };

    this.locationMasterFacade.saveFormDetails({
      locationCode: this.locationCode,
      data: {
        configDetails
      }
    });
    // this.isLoading$.pipe(skipUntil(of(true)), takeUntil(this.destroy$)).subscribe(loading => {
    //   if (loading === false) {
    //     this.showNotification();
    //   }
    // });
  }
  updateLoyalityTwo(loyalityStepTwo) {
    const configDetails = {
      loyalityStepTwo: {
        ccPayment: loyalityStepTwo.ccPayment,
        employeeDiscount: loyalityStepTwo.employeeDiscount,
        giftCardConfiguration: {
          maximumAmount: +loyalityStepTwo.maximumAmount,
          minimumAmount: +loyalityStepTwo.minimumAmount,
          multiplesValue: +loyalityStepTwo.multiplesValue,
          giftCardConfigurationCheckBoxes: loyalityStepTwo.giftCardConfiguration
        }
      }
    };
    this.locationMasterFacade.saveFormDetails({
      locationCode: this.locationCode,
      data: {
        configDetails
      }
    });
    // this.isLoading$.pipe(skipUntil(of(true)), takeUntil(this.destroy$)).subscribe(loading => {
    //   if (loading === false) {
    //     this.showNotification();
    //   }
    // });
  }
  updateLoyalityThree(loyallityStepThree) {
    this.locationMasterFacade.saveFormDetails({
      locationCode: this.locationCode,
      data: {
        configDetails: {
          loyalityStepThree: {
            tep: {
              noOfDaysForFVTPassword: +loyallityStepThree.noOfDaysForFVTPassword,
              enableRTGSrefund: loyallityStepThree.enableRTGSRefund
            }
          }
        }
      }
    });
    // this.isLoading$.pipe(skipUntil(of(true)), takeUntil(this.destroy$)).subscribe(loading => {
    //   if (loading === false) {
    //     this.showNotification();
    //   }
    // });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  showNotification(key: string) {
    this.overlayNotification.close();

    this.translate
      .get(key)
      .pipe(take(1))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMsg,
            time: 2000,
            hasBackdrop: true
          })
          .events.pipe(take(1))
          .subscribe();
      });
  }

  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasBackdrop: true,
        hasClose: true,
        error: error
      })
      .events.pipe(take(1))
      .subscribe();
  }


  getLoyalityDetailsRouteUrl(page: string) {
    const fromPath = this.route.pathFromRoot[2];
    this.locationCode = fromPath.snapshot.params['locCode'];
    return getLocationDetailsLoyalityRouteUrl(this.locationCode, page);
  }
}
