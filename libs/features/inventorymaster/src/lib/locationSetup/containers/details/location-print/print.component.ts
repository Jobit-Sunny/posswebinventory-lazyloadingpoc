import {
  Component,
  OnInit,
  SimpleChanges,
  OnChanges,
  OnDestroy
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject, of } from 'rxjs';
import { LocationMasterFacade } from '../../../+state/location-master.facade';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventType,
  OverlayNotificationEventRef
} from '@poss-web/shared';
import { CustomErrors } from '@poss-web/core';
import { takeUntil, take, skipUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { getLocationDetailsPrintRouteUrl } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-print',
  templateUrl: './print.component.html'
})
export class PrintComponent implements OnInit, OnDestroy {
  locationCode: any;
  ghsData: any;
  destroy$: Subject<null> = new Subject<null>();
  isLoading$: Observable<boolean>;

  constructor(
    private route: ActivatedRoute,
    private overlayNotification: OverlayNotificationService,
    private locationMasterFacade: LocationMasterFacade,
    private translate: TranslateService
  ) { }
  page: string;
  printDetails$: Observable<any>;

  ngOnInit() {
    this.locationMasterFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.locationMasterFacade
      .getIsSaved()
      .pipe(
        skipUntil(of(true)),
        takeUntil(this.destroy$)
      )
      .subscribe(saved => {
        if (saved === true) {
          this.locationMasterFacade.resetIsSaved();
          this.showNotification('pw.locationMaster.savedSuccessfully');
        }
      });

    this.isLoading$ = this.locationMasterFacade.getLocationLoading();
    this.routerPage();
    this.getDetails();
  }

  getDetails() {
    this.printDetails$ = this.locationMasterFacade.getPrintByLocationCode();
  }

  addButtonOne(saveDetails) {
    const fromPath = this.route.pathFromRoot[2];
    this.locationCode = fromPath.snapshot.params['locCode'];

    const configDetails = {
      printStepOne: {
        checkItOut1: {
          printMakingCharges: saveDetails.firstForm.printMakingCharges,
          printPrice: saveDetails.firstForm.printPrice,
          printStoneValue: saveDetails.firstForm.printStoneValue,
          printWastageCharge: saveDetails.firstForm.printWastageCharge,
          printWastageComponent: saveDetails.firstForm.printWastageComponent,
          printWastagePercent: saveDetails.firstForm.printWastagePercent
        },
        makingWastageCharge: +saveDetails.firstForm.makingWastageCharge,

        checkItOut2: {
          printCashMemo: saveDetails.secondForm.printCashMemo,
          printCustomerNumberinReport: saveDetails.secondForm.printCustomerNumberinReport,
          printGoldValue: saveDetails.secondForm.printGoldValue,
          printGuaranteeCard: saveDetails.secondForm.printGuaranteeCard,
          printImage: saveDetails.secondForm.printImage,
          printOtherStoneWeightinAnnexure: saveDetails.secondForm.printOtherStoneWeightinAnnexure,
          printOtherStoneWtinGuaranteeCard: saveDetails.secondForm.printOtherStoneWtinGuaranteeCard,
          freeTextForGrams: saveDetails.secondForm.freeTextForGrams,
          noOfInvoicecopiesforRegularOrQuickCM: +saveDetails.secondForm.noOfInvoicecopiesforRegularOrQuickCM
        }
      }
    };
    this.locationMasterFacade.saveFormDetails({
      locationCode: this.locationCode,
      data: {
        configDetails
      }
    });
  }

  routerPage() {
    this.route.paramMap.pipe(takeUntil(this.destroy$)).subscribe(params => {
      this.page = params.get('page');
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  showNotification(key: string) {
    this.overlayNotification.close();

    this.translate
      .get(key)
      .pipe(take(1))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification.show({
          type: OverlayNotificationType.TIMER,
          message: translatedMsg,
          hasClose: true,
          hasBackdrop: true
        });
      });
  }

  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error,
        hasBackdrop: true
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => { });
  }

  // getPrintDetailsRouteUrl(page: string) {
  //   const fromPath = this.route.pathFromRoot[2];
  //   this.locationCode = fromPath.snapshot.params['locCode'];
  //   return getLocationDetailsPrintRouteUrl(this.locationCode, page);
  // }
}
