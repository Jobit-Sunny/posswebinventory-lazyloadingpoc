import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LocationMasterFacade } from '../../../+state/location-master.facade';
import { Observable, Subject, of } from 'rxjs';
// import { OverlayNotificationService } from 'libs/shared/src/lib/uicomponents/overlay-notification/overlay-notification.service';
// import { CustomErrors } from 'libs/core/src/lib/model/error.model';
// import { OverlayNotificationType, OverlayNotificationEventType } from 'libs/shared/src/lib/uicomponents/overlay-notification/overlay-notification.enum';
import { takeUntil, take, skipUntil } from 'rxjs/operators';
import {
  OverlayNotificationService,
  OverlayNotificationType
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { CustomErrors } from '@poss-web/core';
import { getLocationDetailsAdvanceRouteUrl } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-location-advance-custom-order-foc',
  templateUrl: './location-advance-custom-order-foc.component.html'
})
export class LocationAdvanceCustomOrderFocComponent
  implements OnInit, OnDestroy {
  destroy$: Subject<null> = new Subject<null>();
  advanceCustomOrderDetails$: Observable<any>;
  isLoading$: Observable<boolean>;
  locationCode: any;
  page: string;
  newLocCode$: Subject<null> = new Subject<null>();
  priceGroupCode$: any;
  locationDetails$: Observable<any>;

  constructor(
    private route: ActivatedRoute,
    private locationMasterFacade: LocationMasterFacade,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.locationMasterFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.locationMasterFacade
      .getIsSaved()
      .pipe(
        skipUntil(of(true)),
        takeUntil(this.destroy$)
      )
      .subscribe(saved => {
        if (saved === true) {
          this.locationMasterFacade.resetIsSaved();
          this.showNotification('pw.locationMaster.savedSuccessfully');
        }
      });

    this.routerPage();
    this.isLoading$ = this.locationMasterFacade.getLocationLoading();
    this.locationDetails$ = this.locationMasterFacade.getLocationOnlyByLocationCode();

    const fromPath = this.route.pathFromRoot[2];
    this.locationCode = fromPath.snapshot.params['locCode'];
    this.getAdvanceCustomOrderDetails();
  }
  getAdvanceCustomOrderDetails() {
    this.advanceCustomOrderDetails$ = this.locationMasterFacade.getAdvanceCustomOrder();
    this.priceGroupCode$ = this.locationMasterFacade.getPriceGroupCode();
  }
  routerPage() {
    this.route.paramMap.subscribe(params => {
      this.page = params.get('page');
    });
  }

  updateAdvanceCustomOrderTabTwo(advanceCustomOrder) {
    const configDetails = {
      advanceStepTwo: {
        foc: {
          advanceCustomOrderTabTwoFoccheckBoxes:
            advanceCustomOrder.advanceCustomOrderTabTwoFoccheckBoxes,
          maxWeightforFOC: advanceCustomOrder.maxWeightforFOC === ''
            ? null
            : parseFloat(parseFloat(
              advanceCustomOrder.maxWeightforFOC
            ).toFixed(3)),
          maxValueforFOC: advanceCustomOrder.maxValueforFOC === ''
            ? null
            : parseFloat(parseFloat(
              advanceCustomOrder.maxValueforFOC
            ).toFixed(3)),
        },
        rtgs: {
          advanceCustomOrderTabTwoRtgscheckBoxes:
            advanceCustomOrder.advanceCustomOrderTabTwoRtgscheckBoxes,
          maxNoofCN: Number(advanceCustomOrder.maxNoofCN),
          minOTPCNValue: advanceCustomOrder.minOTPCNValue,
          otpHelpdeskEmailId: advanceCustomOrder.OTPHelpdeskEmailId,
          maxNoofdaysforPOLikelyDate:
            Number(advanceCustomOrder.maxnoofdaysforPOlikelydate),
          serviceTaxGSTRegistrationno:
            advanceCustomOrder.serviceTaxGSTRegistrationno
        }
      }
    }
    this.locationMasterFacade.saveFormDetails({
      locationCode: this.locationCode,
      data: {
        configDetails
      }
    });
    // this.isLoading$.pipe(skipUntil(of(true)), takeUntil(this.destroy$)).subscribe(loading => {
    //   if (loading === false) {
    //     this.showNotification();
    //   }
    // });
  }

  updateAdvanceCustomOrderTabThree(advanceCustomOrder) {
    this.locationMasterFacade.saveFormDetails({
      locationCode: this.locationCode,
      data: {
        configDetails: {
          advanceStepThree: {
            otpConfigurations: {
              advanceCustomOrderTabThreecheckBoxes:
                advanceCustomOrder.advanceCustomOrderTabThreecheckBoxes
            },
            configurePaymentMode: {
              configurePaymentModeCheckBoxes: {
                configurePaymentModeCheckBoxes:
                  advanceCustomOrder.configurePaymentModeCheckBoxes
              },
              paymentCode: advanceCustomOrder.paymentCode
            },
            locationPriceMapping: {
              priceGroupTypeCode: advanceCustomOrder.priceGroupTypeCode,
              priceGroupCode: advanceCustomOrder.priceGroupCode
            }
          }
        }
      }
    });
    // this.isLoading$.pipe(skipUntil(of(true)), takeUntil(this.destroy$)).subscribe(loading => {
    //   if (loading === false) {
    //     this.showNotification();
    //   }
    // });
  }

  updateAdvanceCustomOrderTabOne(advanceCustomOrder) {

    const configDetails = {
      advanceStepOne: {
        advanceCustomOrderConfiguration: {
          advanceCustomOrderStepOneCheckBox:
            advanceCustomOrder.advanceCustomOrderTabOnecheckBoxes,
          validityDaysforAutoClosureInAdvanceBooking: Number(
            advanceCustomOrder.validityDaysforAutoClosureInAdvanceBooking
          ),
          validityDaysforActivateInAdvanceBooking: Number(
            advanceCustomOrder.validityDaysforActivateInAdvanceBooking
          ),
          validityDaysforReleaseInvInAdvancebooking: Number(
            advanceCustomOrder.validityDaysforReleaseInvInAdvancebooking
          ),
          validityDaysforAutoClosureInCustomerOrder: Number(
            advanceCustomOrder.validityDaysforAutoClosureInCustomerOrder
          ),
          validityDaysforActivateInCustomerOrder: Number(
            advanceCustomOrder.validityDaysforActivateInCustomerOrder
          ),
          validitydaysforReleaseInvInCustomerOrder: Number(
            advanceCustomOrder.validitydaysforReleaseInvInCustomerOrder
          ),
          goldRateAttempts:
            advanceCustomOrder.goldRateAttempts,
          manualBillWeightDeviation: advanceCustomOrder.manualBillWeightDeviation === '' ? null : parseFloat(parseFloat(advanceCustomOrder.manualBillWeightDeviation).toFixed(3)),
          sparewtToleranceforStockItem: advanceCustomOrder.sparewtToleranceforStockItem === '' ? null : parseFloat(parseFloat(advanceCustomOrder.sparewtToleranceforStockItem).toFixed(3)),
          servicewtToleranceforStockItem: advanceCustomOrder.servicewtToleranceforStockItem === '' ? null : parseFloat(parseFloat(advanceCustomOrder.servicewtToleranceforStockItem).toFixed(3))
        }
      }
    }

    this.locationMasterFacade.saveFormDetails({
      locationCode: this.locationCode,
      data: {
        configDetails
      }
    });

    // this.isLoading$.pipe(skipUntil(of(true)), takeUntil(this.destroy$)).subscribe(loading => {
    //   if (loading === false) {
    //     this.showNotification();
    //   }
    // });
  }

  ngOnDestroy(): void {
    this.newLocCode$.next();
    this.newLocCode$.complete();
  }

  showNotification(key: string) {
    this.overlayNotification.close();

    this.translate
      .get(key)
      .pipe(take(1))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMsg,
            time: 2000,
            hasBackdrop: true
          })
          .events.pipe(take(1))
          .subscribe();
      });
  }

  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasBackdrop: true,
        hasClose: true,
        error: error
      })
      .events.pipe(take(1))
      .subscribe();
  }

  getAdvanceDetailsRouteUrl(page: string) {
    const fromPath = this.route.pathFromRoot[2];
    this.locationCode = fromPath.snapshot.params['locCode'];
    return getLocationDetailsAdvanceRouteUrl(this.locationCode, page);
  }
}
