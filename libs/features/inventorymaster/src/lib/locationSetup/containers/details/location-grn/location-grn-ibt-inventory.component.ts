import { Component, OnInit, ɵConsole, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LocationMasterFacade } from '../../../+state/location-master.facade';
import { Observable, Subject, of } from 'rxjs';
import { CustomErrors } from '@poss-web/core';
import { takeUntil, skipUntil, take } from 'rxjs/operators';
import {
  OverlayNotificationService,
  OverlayNotificationType
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { getLocationDetailsGRNRouteUrl } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-location-grn-ibt-inventory',
  templateUrl: './location-grn-ibt-inventory.component.html'
})
export class LocationGrnIbtInventoryComponent implements OnInit, OnDestroy {
  GRNIBTDetails$: Observable<any>;
  error: string;
  errorMessage: string;
  destroy$: Subject<null> = new Subject<null>();
  isLoading$: Observable<boolean>;
  constructor(
    private route: ActivatedRoute,
    private locationMasterFacade: LocationMasterFacade,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService
  ) { }
  page: string;
  locationCode: string;
  locationDetails$: Observable<any>;

  ngOnInit() {
    this.routerPage();

    this.locationMasterFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.locationMasterFacade
      .getIsSaved()
      .pipe(
        skipUntil(of(true)),
        takeUntil(this.destroy$)
      )
      .subscribe(saved => {
        if (saved === true) {
          this.locationMasterFacade.resetIsSaved();
          this.showNotification('pw.locationMaster.savedSuccessfully');
        }
      });

    this.GRNIBTDetails$ = this.locationMasterFacade.getGRNIBTByLocationCode();
    this.isLoading$ = this.locationMasterFacade.getLocationLoading();
    this.locationDetails$ = this.locationMasterFacade.getLocationOnlyByLocationCode();
  }

  routerPage() {
    this.route.paramMap.subscribe(params => {
      this.page = params.get('page');
    });
  }
  UpdateGRNOne(GRNStepOne) {
    const fromPath = this.route.pathFromRoot[2];
    this.locationCode = fromPath.snapshot.params['locCode'];

    const configDetails = {
      grnIBTInventoryStepOne: {
        grnGrfConfiguration: {
          noOfDaysGRNAllowed: +GRNStepOne.noOfDaysGRNAllowed,
          maximumNoOfDaysForApprovedGRN: +GRNStepOne.maximumNoOfDaysForApprovedGRN,
          noOfDaysToProtectGoldRateForGRN: +GRNStepOne.noOfDaysToProtectGoldRateForGRN,
          minimumUtilization: GRNStepOne.minimumUtilization,
          grnGrfCheckBoxes: GRNStepOne.grnGrfCheckBoxes
        },
        inventory: {
          maximumNoOfDaysForPhysicalReceiptDate: +GRNStepOne.maximumNoOfDaysForPhysicalReceiptDate,
          configurationAmountForStuddedSplit:
            GRNStepOne.configurationAmountForStuddedSplit,
          maximumNoOfDaysForSTNCancellation: +GRNStepOne.maximumNoOfDaysForSTNCancellation,
          inventoryCheckBoxes: GRNStepOne.inventoryCheckBoxes
        },
        walkins: {
          isWaliknsDetailsMandatory: GRNStepOne.isWaliknsDetailsMandatory,
          noOfDays: +GRNStepOne.noOfDays,
          numberOfDaysToDisplay: +GRNStepOne.numberOfDaysToDisplay
        }
      }
    };

    this.locationMasterFacade.saveFormDetails({
      locationCode: this.locationCode,
      data: {
        configDetails
      }
    });
  }

  UpdateGRNTwo(GRNStepTwo) {
    const fromPath = this.route.pathFromRoot[2];
    this.locationCode = fromPath.snapshot.params['locCode'];

    const configDetails = {
      grnIBTInventoryStepTwo: {
        kycConfiguration: GRNStepTwo.kycConfiguration,
        ulpConfiguration: GRNStepTwo.ulpConfiguration,
        ibtConfiguration: {
          noOfTimesrequestedInCurrentMonth: +GRNStepTwo.noOfTimesrequestedInCurrentMonth,
          totalValueRequestedInCurrentMonth: +GRNStepTwo.totalValueRequestedInCurrentMonth,
          noOfItemsRequestedInCurrentMonth: +GRNStepTwo.noOfItemsRequestedInCurrentMonth
        }
      }
    };
    this.locationMasterFacade.saveFormDetails({
      locationCode: this.locationCode,
      data: {
        configDetails
      }
    });
    // this.isLoading$.pipe(skipUntil(of(true)), takeUntil(this.destroy$)).subscribe(loading => {
    //   if (loading === false) {
    //     this.showNotification();
    //   }
    // });
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  showNotification(key: string) {
    this.overlayNotification.close();

    this.translate
      .get(key)
      .pipe(take(1))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMsg,
            time: 2000,
            hasBackdrop: true
          })
          .events.pipe(take(1))
          .subscribe();
      });
  }

  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasBackdrop: true,
        hasClose: true,
        error: error
      })
      .events.pipe(take(1))
      .subscribe();
  }


  getGrnDetailsRouteUrl(page: string) {
    const fromPath = this.route.pathFromRoot[2];
    this.locationCode = fromPath.snapshot.params['locCode'];
    return getLocationDetailsGRNRouteUrl(this.locationCode, page);
  }
}
