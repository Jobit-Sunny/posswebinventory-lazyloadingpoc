import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocationMasterFacade } from '../../../+state/location-master.facade';
import { Observable, of, Subject } from 'rxjs';
import { takeUntil, skipUntil, take } from 'rxjs/operators';
import {
  OverlayNotificationService,
  OverlayNotificationEventType,
  OverlayNotificationType,
  OverlayNotificationEventRef
} from '@poss-web/shared';
import { CustomErrors } from '@poss-web/core';
import { TranslateService } from '@ngx-translate/core';
import { getLocationDetailsLocationRouteUrl } from '../../../../page-route.constants';


@Component({
  selector: 'poss-web-location',
  templateUrl: './location.component.html'
})
export class LocationComponent implements OnInit, OnDestroy {
  destroy$: Subject<null> = new Subject<null>();
  locationTypes$: Observable<any>;
  ownerInfo$: Observable<any>;
  locationStates$: Observable<any>;
  locationTowns$: Observable<any>;
  locationDetails$: Observable<any>;
  baseCurrency$: Observable<any>;
  regions$: Observable<any>;
  brandName$: Observable<any>;
  subBrandName$: Observable<any>;
  locationSize$: Observable<any>;
  paymentModeForRefund$: Observable<any>;
  locationFormat$: Observable<any>;
  marketCode$: Observable<any>;
  locationCode: string;
  country$: Observable<any>;
  newLocCode$: Subject<null> = new Subject<null>();
  isLoading$: Observable<boolean>;
  page: string;
  disabled = true;
  hasError: Observable<CustomErrors>;
  isNew: boolean;


  constructor(
    private route: ActivatedRoute,
    private locationMasterFacade: LocationMasterFacade,
    private router: Router,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService
  ) { }

  ngOnInit() {

    this.routerPage();
    this.getDetails();
    const fromPath = this.route.pathFromRoot[2];
    this.locationCode = fromPath.snapshot.params['locCode']; // TODO. Recheck
    if (this.locationCode === 'NEW') {
      this.isNew = true;
    } else {
      this.isNew = false;
    }


    this.locationMasterFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.locationMasterFacade.getIsSaved()
      .pipe(
        skipUntil(of(true)),
        takeUntil(this.destroy$)
      )
      .subscribe(saved => {
        if (saved === true) {
          this.locationMasterFacade.resetIsSaved();
          this.showNotification('pw.locationMaster.savedSuccessfully');
        }
      });
  }

  getDetails() {
    this.locationTypes$ = this.locationMasterFacade.getLocationTypes();
    this.ownerInfo$ = this.locationMasterFacade.getOwnerInfo();
    this.locationStates$ = this.locationMasterFacade.getStates();
    this.country$ = this.locationMasterFacade.getCountry();
    this.marketCode$ = this.locationMasterFacade.getMarketCode();
    // .pipe(filter(data => data !== null)); //this.locationService.getPersonalStatesData();
    this.regions$ = this.locationMasterFacade.getRegions();
    this.locationTowns$ = this.locationMasterFacade.getTowns();
    this.locationFormat$ = this.locationMasterFacade.getLocationFormat();
    this.brandName$ = this.locationMasterFacade.getBrandName();
    this.subBrandName$ = this.locationMasterFacade.getSubBrandName();
    this.locationSize$ = this.locationMasterFacade.getLocationSize();
    this.paymentModeForRefund$ = this.locationMasterFacade.getPaymentModeRefund();
    this.locationDetails$ = this.locationMasterFacade.getLocationOnlyByLocationCode();
    this.isLoading$ = this.locationMasterFacade.getLocationLoading();

    this.locationDetails$.pipe(takeUntil(this.newLocCode$)).subscribe(data => {
      if (data) {
        if (this.locationCode === 'NEW' && data.locationCode !== 'NEW') {
          this.newLocCode$.next();
          this.newLocCode$.complete();
          this.isNew = false;
          setTimeout(() => {
            this.router.navigate([getLocationDetailsLocationRouteUrl(data.locationCode, '1')]);
          }, 2000);
        }
      }
    });
  }

  addButton(location) {
    const fromPath = this.route.pathFromRoot[2];
    this.locationCode = fromPath.snapshot.params['locCode']; // TODO. Recheck
    const pinCode = Number(location.pinCode);

    const configDetails = {
      locationStepOne: {
        locationDetails: {
          locationShortName: location.configDetails.locationShortName,
          subBrandName: location.configDetails.subBrandName
        },
        personalDetails: {
          phoneNumberTwo: location.configDetails.phoneNumberTwo,
          adressTwo: location.configDetails.adressTwo,
          name: location.configDetails.name,
          subRegion: location.configDetails.subRegionCode
        }
      }
    }

    const locStepOne = {
      address: location.addressOne,
      brandCode: location.brandCode,
      countryCode: location.countryCode,
      configDetails,
      contactNo: location.phoneNumber,
      description: location.description,
      factoryCodeValue: location.factoryCodeValue,
      isActive: true,
      cfaCodeValue: location.locationCode,
      locationCode: location.locationCode,
      locationEmail: location.email,
      locationFormat: location.locationFormat,
      locationTypeCode: location.locationType,
      ownerTypeCode: location.ownerTypeCode,
      phoneNo: location.mobileNumber,
      pincode: pinCode,
      regionCode: location.regionCode,
      registrationNo: location.registrationNo,
      stateCode: location.state,
      townCode: location.city
    };

    if (this.locationCode === 'NEW') {
      this.locationMasterFacade.saveLocationDetails(locStepOne);
    } else {
      this.isNew = false;
      this.locationMasterFacade.saveFormDetails({
        locationCode: location.locationCode,
        data: locStepOne
      });
    }
  }
  updateLocationTwo(locationConfigDetails) {
    const fromPath = this.route.pathFromRoot[2];
    this.locationCode = fromPath.snapshot.params['locCode'];

    const configDetails = {
      locationStepTwo: {
        otherDetails: {
          regdOffice: locationConfigDetails.regdOffice,
          cinNumber: locationConfigDetails.CINNumber,
          corporateAddress: locationConfigDetails.corporateAddress,
          marketCode: locationConfigDetails.marketCode
        },
        configurationDetails: {
          checkBoxes: locationConfigDetails.checkBoxes
        }
      }
    };


    this.locationMasterFacade.saveFormDetails({
      locationCode: this.locationCode,
      data: {
        configDetails
      }
    });


  }

  updateLocationThree(locationConfigDetails) {
    const fromPath = this.route.pathFromRoot[2];
    this.locationCode = fromPath.snapshot.params['locCode'];

    const configDetails = {
      locationStepThree: {
        remarks: {
          remarks: locationConfigDetails.remarks,
          sapCode: locationConfigDetails.SAPcode,
          paymentModeForRefund: locationConfigDetails.paymentModeForRefund
        },
        locationCheckbox: {
          locationCheckbox: locationConfigDetails.locationCheckbox
        }
      }
    };


    this.locationMasterFacade.saveFormDetails({
      locationCode: this.locationCode,
      data: {
        configDetails
      }
    });

    // this.isLoading$
    //   .pipe(
    //     skipUntil(of(true)),
    //     takeUntil(this.destroy$)
    //   )
    //   .subscribe(loading => {
    //     if (loading === false) {
    //       this.showNotification();
    //     }
    //   });
  }
  // this.locationDetails.pipe(take(1)).subscribe(data => { // We shal not navigate automatically, let user click on tab numbers to navigate
  //   this.router.navigate([
  //     'master',
  //     'locationMasters',
  //     data.locationCode,
  //     '2'
  //   ]);

  // });

  routerPage() {
    this.route.paramMap.pipe(takeUntil(this.destroy$)).subscribe(params => {
      this.page = params.get('page');
    });
  }

  ngOnDestroy(): void {
    this.newLocCode$.next();
    this.newLocCode$.complete();

    this.destroy$.next();
    this.destroy$.complete();
  }

  showNotification(key: string) {
    this.overlayNotification.close();


    this.translate
      .get(key)
      .pipe(take(1))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMsg,
            time: 2000,
            hasBackdrop: true
          })
          .events.pipe(take(1))
          .subscribe();
      });
  }

  errorHandler(error: CustomErrors) {

    // let key = '';
    // //check if the error is the custom error
    // if (customErrorTranslateKeyMapMasters.has(errorCode)) {
    //   //Obtain the transation key which will be use to obtain the translated error message
    //   //based on the language selected. Default is the english language(refer en.json from asset folder).
    //   key = customErrorTranslateKeyMapMasters.get(errorCode);
    // } else {
    //   key = 'pw.global.genericErrorMessage';
    // }


    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasBackdrop: true,
        hasClose: true,
        error: error
      })
      .events.pipe(take(1))
      .subscribe();
  }

  getLocationDetailsRouteUrl(locationCode: string, page: string) {
    return getLocationDetailsLocationRouteUrl(locationCode, page);
  }
}
