import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  AfterViewInit
} from '@angular/core';

import { Observable, Subject, fromEvent } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

import { PageEvent } from '@angular/material';
import { takeUntil, debounceTime, take } from 'rxjs/operators';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';

import { FormGroup, FormControl } from '@angular/forms';
import { LocationServiceService } from '../../services/location-service.service';
import { LocationMasterFacade } from '../../+state/location-master.facade';
import { LocationMaster } from '../../models/locatonmodel';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventType,
  OverlayNotificationEventRef,
  ErrorEnums
} from '@poss-web/shared';
import { EmitCopyLocationCodePayload } from '../../components/location-listing-item/location-listing-item.component';
import { TranslateService } from '@ngx-translate/core';
import { getLocationMasterDashboardRouteUrl, getLocationDetailsLocationRouteUrl } from '../../../page-route.constants';

@Component({
  selector: 'poss-web-location-listing',
  templateUrl: './location-listing.component.html'
})
export class LocationListingComponent
  implements OnInit, OnDestroy, AfterViewInit {
  locationListingPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };
  pageSize: any;
  minPageSize: any;
  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;
  searchForm = new FormGroup({
    searchValue: new FormControl()
  });
  hasError$: Observable<CustomErrors>;
  destroy$ = new Subject<null>();

  searchErrorCode: string;

  constructor(
    private appsettingFacade: AppsettingFacade,
    private locationService: LocationServiceService,
    private overlayNotification: OverlayNotificationService,
    private route: ActivatedRoute,
    private router: Router,
    private locationMasterFacade: LocationMasterFacade,
    private translate: TranslateService
  ) { }
  isLoadingLocations$: Observable<boolean>;
  locationList$: Observable<LocationMaster[]>;
  totalCount$: Observable<number>;
  isSearching$: Observable<any>;
  loadDetailsPage(locationCode: string) {
    if (locationCode) {
      // this.locationService.getLocationDetailsByLocationCode('AAA');
      this.locationMasterFacade.loadReset();
      this.router.navigate([getLocationDetailsLocationRouteUrl(locationCode, '1')]);
    }
  }
  ngAfterViewInit(): void {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe(event => {
        const searchValue = this.searchForm.value.searchValue;
        if (searchValue) {
          this.search(searchValue);
        } else {
          this.clearSearch();
        }
      });
  }
  addnew() {
    this.locationMasterFacade.loadReset();
    this.router.navigate([getLocationDetailsLocationRouteUrl('NEW', '1')]);
  }

  search(searchValue: string) {
    if (!searchValue.search('^[_A-z0-9]*((-|s)*[_A-z0-9])*$')) {
      this.locationMasterFacade.searchLocation(searchValue);
    }
  }
  clearSearch() {
    this.searchForm.reset();
    this.loadLocationList();
  }

  ngOnInit() {
    this.hasError$ = this.locationMasterFacade.getError();
    this.searchErrorCode = ErrorEnums.ERR_LOC_001;
    this.isLoadingLocations$ = this.locationMasterFacade.getLocationListingLoading();
    this.isSearching$ = this.locationMasterFacade.getIsSerchElements();
    this.appsettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.pageSize = data;
        //this.locationListingPageEvent.pageSize = this.pageSize;
        // this.loadLocationList();
      });
    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.locationListingPageEvent.pageSize = data;
        this.loadLocationList();
      });
    this.locationList$ = this.locationMasterFacade.getLocationListing();
    this.totalCount$ = this.locationMasterFacade.getTotalCount();

    this.locationMasterFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.locationMasterFacade
      .getIsCopySuccess()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        let key = '';
        if (data) {
          this.loadLocationList();
          key = 'pw.locationMasterNotification.locationCopySuccessful';
          this.locationMasterFacade.resetIsCopied();
          this.showNotification(key);
        }
      });
  }

  loadLocationList() {
    this.locationMasterFacade.loadPendingLocationListing(
      this.locationListingPageEvent
    );
  }

  copyLocation(copyLocationDetails: EmitCopyLocationCodePayload) {
    if (
      copyLocationDetails.newLocationCode !== null &&
      copyLocationDetails.oldLocationCode !== null
    ) {
      this.locationMasterFacade.copyLocationDetails(copyLocationDetails);
    }
  }

  showNotification(key: string) {
    this.overlayNotification.close();

    this.translate
      .get(key)
      .pipe(take(1))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMsg,
            time: 2000,
            hasBackdrop: true
          })
          .events.pipe(take(1))
          .subscribe();
      });
  }

  updateIsActive(event) {
    this.locationMasterFacade.saveFormDetails({
      locationCode: event.locationCode,
      data: {
        isActive: event.isActive
      }
    });
  }
  paginate(pageEvent: PageEvent) {
    this.locationListingPageEvent = pageEvent;
    this.loadLocationList();
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
  back() {
    this.router.navigate([getLocationMasterDashboardRouteUrl()]);
  }

  errorHandler(error: CustomErrors) {
    if (error.code === this.searchErrorCode) {
      // We are not showing error for location not found from search.
      return;
    }
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasBackdrop: true,
        hasClose: true,
        error: error
      })
      .events.pipe(take(1))
      .subscribe();
  }
  omit_special_char($event: KeyboardEvent) {
    const pattern = /^[-_A-Za-z0-9]$/;
    return pattern.test($event.key);
  }
}
