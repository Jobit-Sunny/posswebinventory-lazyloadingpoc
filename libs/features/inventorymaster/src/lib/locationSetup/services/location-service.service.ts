import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '@poss-web/core';

import { map, take, tap } from 'rxjs/operators';
import {
  getLocationDetailsUrl,
  getLocationTypeUrl,
  getLocationStateUrl,
  getLocationTownUrl,
  getLocationOwnerTypeUrl,
  getLocationRegionUrl,
  getLocationDetailsByLocationCodeUrl,
  getSaveLocationDetailsUrl,
  getSaveFormDetailsUrl,
  getCopyLocationUrl,
  getBrandUrl,
  getBaseCurrencyUrl,
  getCountryUrl,
  getLocationAllRegionUrl,
  getSearchLocationByLocationCode,
  getLocationFormatUrl
} from '../../endpoint.constants';
import { Observable, of } from 'rxjs';

import {
  LocationListing,
  SaveLocationDetailsPayload,
  SaveFormDetailsPayload,
  BaseCurrencyTypes
} from '../+state/location-master.actions';
import { LocationAdpator } from '../adaptors/location.adaptor';
import { OverlayNotificationService } from '@poss-web/shared';

@Injectable({
  providedIn: 'root'
})
export class LocationServiceService {
  constructor(private apiService: ApiService) { }

  locationDetals: Observable<any>;

  copyLocationDetail(
    sourceLocationCode: string,
    destinationLocationCode: string
  ) {
    const url = getCopyLocationUrl(sourceLocationCode, destinationLocationCode);
    return this.apiService.post(url);
  }
  getLocationDetails(
    pageIndex: number,
    pageSize: number
  ): Observable<LocationListing> {
    const url = getLocationDetailsUrl(pageIndex, pageSize);
    return this.apiService
      .get(url)
      .pipe(map(data => LocationAdpator.getLocationListingDetails(data)));
  }

  getLocationDetailsByLocationCode(locationCode) {
    const url = getLocationDetailsByLocationCodeUrl(locationCode);

    this.locationDetals = this.apiService
      .get(url)
      .pipe(
        map(data => LocationAdpator.getAllLocationDetailsByloactionCode(data))
      );

    return this.locationDetals;
  }

  getNewLocationFeilds() {
    const data = LocationAdpator.getAllLocationDetailsByloactionCode(false);
    return data;
  }

  searchLocationByLocationCode(locationCode: string) {
    const url = getSearchLocationByLocationCode(locationCode);


    return this.apiService
      .get(url)
      .pipe(map(data => LocationAdpator.getSearchResult(data)));
  }

  getCountry() {
    const url = getCountryUrl();
    return this.apiService
      .get(url)
      .pipe(map(data => LocationAdpator.getCountry(data)));
  }


  getLocationFormat(){
    const url=getLocationFormatUrl('LOCATIONFORMAT')
    return this.apiService
    .get(url)
    .pipe(map(data => LocationAdpator.getLocationFormat(data)));
    // return of([
    //   { id: '1', name: 'Large Format Store' },
    //   { id: '2', name: 'Medium Format Store' },
    //   { id: '3', name: 'Small Format Store' },
    //   { id: '4', name: 'Micro Format Store' }
    // ]);
  }

  getPaymentCode(){
    return of([{ id: '1', name: 'RTGS' }, { id: '2', name: 'Cheque' }]);
  }
  getPriceGroup(){
    return of([
      { id: '1', name: 'G1' },
      { id: '2', name: 'G2' },
      { id: '3', name: 'G3' }
    ]);
  }
  getLocationTypes() {
    const url = getLocationTypeUrl('LOCATIONTYPE');
    return this.apiService
      .get(url)
      .pipe(map(data => LocationAdpator.getLocationTypes(data)));
  }

  getLocationOwnerTypes() {
    const url = getLocationTypeUrl('');
    return this.apiService
      .get(url)
      .pipe(map(data => LocationAdpator.getLocationTypes(data)));
  }

  getPersonalStatesData() {
    const url = getLocationStateUrl();
    return this.apiService
      .get(url)
      .pipe(map(data => LocationAdpator.getLocationStates(data)));
  }

  getPersonalTownsData() {
    const url = getLocationTownUrl();
    return this.apiService
      .get(url)
      .pipe(map(data => LocationAdpator.getLocationTowns(data)));
  }

  getOwnerTypeList() {
    const url = getLocationOwnerTypeUrl('OWNERTYPE');
    return this.apiService
      .get(url)
      .pipe(map(data => LocationAdpator.getLocationFormat(data)));
  }

  getRegionList() {
    const url = getLocationAllRegionUrl();
    return this.apiService
      .get(url)
      .pipe(map(data => LocationAdpator.getLocationRegions(data)));
  }

  getBrandList() {
    const url = getBrandUrl();
    return this.apiService
      .get(url)
      .pipe(map(data => LocationAdpator.getBrands(data)));
  }

  saveLocationDetails(saveLocationDetails: SaveLocationDetailsPayload) {
    const url = getSaveLocationDetailsUrl();
    return this.apiService.post(url, saveLocationDetails);
  }

  saveFormDetails(saveFormDetails: SaveFormDetailsPayload) {
    const url = getSaveFormDetailsUrl(saveFormDetails.locationCode);
    return this.apiService.patch(url, saveFormDetails.data);
  }

  getBaseCurrencyData() {
    const url = getBaseCurrencyUrl();
    return this.apiService
      .get(url)
      .pipe(map(data => LocationAdpator.getBaseCurrency(data)));
  }

  getMarketCodeData() {
    return of([
      { id: '1', name: 'BLR' },
      { id: '2', name: 'KRL' },
      { id: '3', name: 'UBL' } // TODO, get from API
    ]);
  }

  // getPersonalGenderData() {
  //   return this.http.get('http://localhost:3001/gender');
  // }

  // getPersonalData() {
  //   return this.http.get('http://localhost:3001/personalDetails');
  // }
  // getOtherDetailsData() {
  //   return this.http.get('http://localhost:3001/otherDetails');
  // }
  // getNumberData() {
  //   return this.http.get('http://localhost:3001/numbers');
  // }
  // getDateDetails() {
  //   return this.http.get('http://localhost:3001/date');
  // }
  // getRemarks() {
  //   return this.http.get('http://localhost:3001/remarks');
  // }
  // getChecks() {
  //   return this.http.get('http://localhost:3001/checks');
  // }
}
