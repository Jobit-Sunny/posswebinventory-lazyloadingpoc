import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { PageEvent } from '@angular/material';
import { AppsettingFacade } from '@poss-web/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { EmitCopyLocationCodePayload } from '../location-listing-item/location-listing-item.component';

@Component({
  selector: 'poss-web-location-list-items',
  templateUrl: './location-list-items.component.html',
  styleUrls: ['./location-list-items.component.scss']
})
export class LocationListItemsComponent
  implements OnInit, OnDestroy, OnChanges {
  @Input() locationList;
  @Input() count;
  @Input() pageEvent: PageEvent;
  @Input() pageSize;
  @Output() paginator = new EventEmitter<PageEvent>();
  @Output() emitCopyLocationData = new EventEmitter<
    EmitCopyLocationCodePayload
  >();
  @Output() locationCode = new EventEmitter<any>();
  @Output() emitToggleValue = new EventEmitter<any>();
  constructor(private appSettingFacade: AppsettingFacade) {}
  pageSizeOptions: number[] = [];
  minPageSize = 0;
  destroy$ = new Subject<null>();

  ngOnChanges(changes: SimpleChanges): void {
    this.pageSizeOptions = this.pageSize;

    this.minPageSize = this.pageSizeOptions.reduce((a: number, b: number) =>
      a < b ? a : b
    );
  }
  ngOnInit() {}

  emitlocationCode(locationCode: string) {
    this.locationCode.emit(locationCode);
  }
  copyDetails(copyDetails: EmitCopyLocationCodePayload) {
    this.emitCopyLocationData.emit(copyDetails);
  }
  updateIsActive(event) {
    this.emitToggleValue.emit(event);
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
