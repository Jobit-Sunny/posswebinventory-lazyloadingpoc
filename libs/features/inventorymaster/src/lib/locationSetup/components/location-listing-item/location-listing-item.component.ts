import {
  Component,
  OnInit,
  Inject,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material/dialog';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'poss-web-location-listing-item',
  templateUrl: './location-listing-item.component.html',
  styleUrls: ['./location-listing-item.component.scss']
})
export class LocationListingItemComponent implements OnInit {
  constructor(public dialog: MatDialog) {}

  @Input() listItem;
  @Output() locationCode = new EventEmitter<any>();
  @Output() copyDetails = new EventEmitter<any>();
  @Output() emitToggle = new EventEmitter<any>();
  checked: boolean;
  loadDetailsPage() {
    this.locationCode.emit(this.listItem.locationCode);
  }

  change(event) {
    const obj = {
      isActive: event.checked,
      locationCode: this.listItem.locationCode
    };
    this.emitToggle.emit(obj);
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(CopyDetailsDialogPopup, {
      width: '450px',
      height: '240px',
      data: this.listItem
    });

    dialogRef.afterClosed().subscribe(data => {
      this.copyDetails.emit(data);
    });
  }

  ngOnInit() {}
}

export interface EmitCopyLocationCodePayload {
  oldLocationCode: string;
  newLocationCode: string;
}
@Component({
  selector: 'poss-web-copy-details-dialog',
  templateUrl: './copy-details-dialoge.component.html',
  styleUrls: ['./location-listing-item.component.scss']
})
export class CopyDetailsDialogPopup {
  @Output() emitCopyLocationCode = new EventEmitter<
    EmitCopyLocationCodePayload
  >();
  form = new FormGroup({
    locationCode: new FormControl()
  });

  constructor(
    public dialogRef: MatDialogRef<CopyDetailsDialogPopup>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}
  clear() {
    this.form.reset();
  }
  create() {
    // this.emitCopyLocationCode.emit({
    //   oldLocationCode: this.data.locationCode,
    //   newLocationCode: this.form.value.locationCode
    // });

    this.dialogRef.close({
      oldLocationCode: this.data.locationCode,
      newLocationCode: this.form.value.locationCode
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  omit_special_char($event : KeyboardEvent) {
    const pattern  = /^[_A-z0-9]*((-|\s)*[_A-z0-9])*$/;
    return pattern.test( $event.key);
  }
}
