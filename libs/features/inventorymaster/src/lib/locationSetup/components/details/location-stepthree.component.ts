import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnDestroy
} from '@angular/core';
import { TEMPLATE5, HelperFunctions } from '@poss-web/shared';
import { FormGroup, Validators } from '@angular/forms';
import { RemarksModel } from '../../models/loaction-third-remarks.model';
import { ThirdDetailsMainModel } from '../../models/location-third-details-main.model';
import { combineLatest, Observable, Subject } from 'rxjs';

import { takeUntil } from 'rxjs/operators';
import { locationStepThreeSecondSubForm, locationStepThreeSecondSubFormremarks, locationStepThreeSecondSubFormSAPcode, locationStepThreeSecondSubFormpaymentModeForRefund, locationTypeCode_BTQ, locationcheckBoxes } from '../../models/locationsetup.constants';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'poss-web-location-stepthree',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="[
        'pw.locationMaster.cancel',
        'pw.locationMaster.saveAndContinue'
      ]"
      (deleteForm)="deleteButton($event)"
      (addForm)="addButton($event)"
      (formGroupCreated)="formGroupCreated($event)"
    >
    </poss-web-dynamic-form>
  `
})
export class LocationStepthreeComponent implements OnInit, OnDestroy {
  public currentStyle: string[];
  public formFields: any;
  public locationTypeCode: string;

  destroy$: Subject<null> = new Subject<null>();
  @Input() locationConfigDetails$: Observable<any>;
  @Input() paymentModeForRefund$: Observable<any>;
  @Output() tabThree: EventEmitter<any> = new EventEmitter();
  constructor(private hf: HelperFunctions, public dialog: MatDialog) { }

  ngOnInit() {
    combineLatest(this.locationConfigDetails$, this.paymentModeForRefund$)
      .pipe(takeUntil(this.destroy$))
      .subscribe(results => {
        this.locationTypeCode = results[0].locationTypeCode;
        const form = this.prepareSet(results[0], results[1]);
        this.formFields = this.getInputs(form);
        this.currentStyle = this.getCssProp();
      });
  }

  prepareSet(locationConfigDetails, paymentModeForRefund) {
    // console.log(locationConfigDetails)
    const locationCheckBox = [{
      id: '1',
      name: 'pw.locationMaster.isActive',
      checked: locationConfigDetails ? locationConfigDetails.configDetails ? locationConfigDetails.configDetails.locationStepThree ? locationConfigDetails.configDetails.locationStepThree.locationCheckbox ? locationConfigDetails.configDetails.locationStepThree.locationCheckbox.locationCheckbox ? locationConfigDetails.configDetails.locationStepThree.locationCheckbox.locationCheckbox.isActive : false : false : false : false : false
    }]
    if (locationConfigDetails.configDetails.locationStepThree === {}) {
      locationConfigDetails.configDetails.locationStepThree = null;
    }
    const locationStepThree = locationConfigDetails.configDetails ? locationConfigDetails.configDetails
      .locationStepThree
      ? locationConfigDetails.configDetails.locationStepThree
        ? locationConfigDetails.configDetails.locationStepThree
        : null
      : null : null;

    // const locationStepThreeRemarks = locationStepThree.remarks
    //   ? locationStepThree.remarks
    //   : {};
    // const locationStepThreeCheckboxes = locationStepThree.checkBoxes
    //   ? locationStepThree.checkBoxes
    //   : null;


    if (locationStepThree) {
      paymentModeForRefund = this.hf.patchValue(
        paymentModeForRefund,
        'id',
        'selected',
        locationStepThree.remarks.paymentModeForRefund,
        true
      );
    }

    const remarksModel = new RemarksModel(
      1,
      locationStepThree
        ? locationStepThree.remarks.remarks
          ? locationStepThree.remarks.remarks
          : ''
        : '',
      locationStepThree
        ? locationStepThree.remarks.sapCode
          ? locationStepThree.remarks.sapCode
          : ''
        : '',
      paymentModeForRefund,
      locationCheckBox
    );

    const locationThirdMain = new ThirdDetailsMainModel(1, remarksModel);

    return locationThirdMain;
  }

  getCssProp() {
    const annot = (LocationStepthreeComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }

  public setFormConfig() {
    return {
      formName: 'Location Master Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE5
    };
  }

  public addButton(formGroup: FormGroup) {
    const locationCheckbox = {
      isActive: formGroup.value[locationStepThreeSecondSubForm][locationcheckBoxes][0]
    }

    const formData = {
      remarks:
        formGroup.value[locationStepThreeSecondSubForm][
        locationStepThreeSecondSubFormremarks
        ],
      SAPcode:
        formGroup.value[locationStepThreeSecondSubForm][
        locationStepThreeSecondSubFormSAPcode
        ],
      paymentModeForRefund:
        formGroup.value[locationStepThreeSecondSubForm][
        locationStepThreeSecondSubFormpaymentModeForRefund
        ],
      locationCheckbox: locationCheckbox
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.tabThree.emit(formData);
      }
    });

  }

  public deleteButton(formGroup: FormGroup) {
    // this.router.navigate([], {
    //   relativeTo: this.activatedRoute,
    //   queryParams: { refresh: new Date().getTime() },
    //   queryParamsHandling: 'merge',
    //   skipLocationChange: true
    // });
    // formGroup.reset()

    // this.ngOnDestroy();
    // this.destroy$ = new Subject<null>();
    // this.ngOnInit();

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public formGroupCreated(formGroup: FormGroup) {
    this.conditionalValidation(formGroup);
  }

  public conditionalValidation(formGroup: FormGroup) {
    if (formGroup) {
      if (this.locationTypeCode === locationTypeCode_BTQ) {
        this.hf.setValidators(formGroup, locationStepThreeSecondSubForm, locationStepThreeSecondSubFormpaymentModeForRefund, [Validators.required])
      } else {
        this.hf.setValidators(formGroup, locationStepThreeSecondSubForm, locationStepThreeSecondSubFormpaymentModeForRefund, [])
      }
    }
  }
}
