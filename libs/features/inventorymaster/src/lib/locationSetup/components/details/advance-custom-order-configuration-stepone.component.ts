import { TEMPLATE5, HelperFunctions } from '@poss-web/shared';
import {
  OnInit,
  Component,
  Input,
  EventEmitter,
  Output,
  OnDestroy
} from '@angular/core';

import { combineLatest, Observable, Subject } from 'rxjs';
import { FormGroup, Validators } from '@angular/forms';
import {
  advanceCustomOrderStepOneSubForm,
  advanceCustomOrderStepOneSubFormCheckBox,
  advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm,
  advanceCustomOrderStepOneSubFormFeildvalidityDaysforAutoClosureInAdvanceBooking,
  advanceCustomOrderStepOneSubFormFeildvalidityDaysforActivateInAdvanceBooking,
  advanceCustomOrderStepOneSubFormFeildvalidityDaysforReleaseInvInAdvancebooking,
  advanceCustomOrderStepOneSubFormFeildvalidityDaysforAutoClosureInCustomerOrder,
  advanceCustomOrderStepOneSubFormFeildvalidityDaysforActivateInCustomerOrder,
  advanceCustomOrderStepOneSubFormFeildvaliditydaysforReleaseInvInCustomerOrder,
  advanceCustomOrderStepOneSubFormFeildgoldRateAttempts,
  advanceCustomOrderStepOneSubFormFeildmanualBillWeightDeviation,
  advanceCustomOrderStepOneSubFormFeildsparewtToleranceforStockItem,
  advanceCustomOrderStepOneSubFormFeildservicewtToleranceforStockItem,
  locationTypeCode_BTQ
} from '../../models/locationsetup.constants';
import { takeUntil } from 'rxjs/operators';
import { AdvanceCustomOrderConfigurationStepThree } from '../../models/advance-custom-order-three-configuration.model';
import { AdvanceCustomOrderConfigurationMainStepThree } from '../../models/advance-custom-order-three-configuration-main.model';
import { AdvanceCustomOrderConfigurationStepThreeCheckBox } from '../../models/advance-custom-order-three-configuration-checkboxes.model';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';
@Component({
  selector: 'poss-web-advance-custom-orders-foc-configuration-stepone',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="[
        'pw.locationMaster.cancel',
        'pw.locationMaster.saveAndContinue'
      ]"
      (deleteForm)="deleteButton($event)"
      (addForm)="addButton($event)"
      (formGroupCreated)="formGroupCreated($event)"
    >
    </poss-web-dynamic-form>
  `
})
export class AdvanceCustomOrdersConfigurationStepOneComponent
  implements OnInit, OnDestroy {
  @Input() advanceAndCustomOrderFoc$: Observable<any>;
  @Output() tabOne = new EventEmitter<any>();
  @Input() locationDetails$: Observable<any>;
  public locationTypeCode: string;

  destroy$: Subject<null> = new Subject<null>();
  constructor(private hf: HelperFunctions,
    public dialog: MatDialog) { }
  public currentStyle: string[];
  public formFields: any;

  ngOnInit() {
    combineLatest(this.advanceAndCustomOrderFoc$, this.locationDetails$)
      .pipe(takeUntil(this.destroy$))
      .subscribe(results => {
        this.locationTypeCode = results[1] ? results[1].locationTypeCode : '';
        const form = this.prepareSet(results[0]);
        // if (results[0] !== null && results[1] !== null && results[2] !== null && results[3] !== null) {
        this.formFields = this.getInputs(form);
        this.currentStyle = this.getCssProp();
        // }
      });
  }

  prepareSet(advanceCustomOrder) {
    const advanceCustomOrderTabOnecheckBoxes = [
      {
        id: '1',
        name: 'pw.locationMaster.cancellationAllowedforAdvanceBooking',
        checked: advanceCustomOrder
          ? advanceCustomOrder.advanceCustomOrderStepOneCheckBox
            .cancellationAllowedforAdvanceBooking
            ? advanceCustomOrder.advanceCustomOrderStepOneCheckBox
              .cancellationAllowedforAdvanceBooking
            : false
          : false
      },
      {
        id: '2',
        name: 'pw.locationMaster.cancellationAllowedforCustomerOrder',
        checked: advanceCustomOrder
          ? advanceCustomOrder.advanceCustomOrderStepOneCheckBox
            .cancellationAllowedforCustomerOrder
            ? advanceCustomOrder.advanceCustomOrderStepOneCheckBox
              .cancellationAllowedforCustomerOrder
            : false
          : false
      },
      {
        id: '3',
        name: 'pw.locationMaster.activateAllowedforAdvanceBooking',
        checked: advanceCustomOrder
          ? advanceCustomOrder.advanceCustomOrderStepOneCheckBox
            .activateAllowedforAdvanceBooking
            ? advanceCustomOrder.advanceCustomOrderStepOneCheckBox
              .activateAllowedforAdvanceBooking
            : false
          : false
      },

      {
        id: '4',
        name: 'pw.locationMaster.activateAllowedforCustomerOrder',
        checked: advanceCustomOrder
          ? advanceCustomOrder.advanceCustomOrderStepOneCheckBox
            .activateAllowedforCustomerOrder
            ? advanceCustomOrder.advanceCustomOrderStepOneCheckBox
              .activateAllowedforCustomerOrder
            : false
          : false
      },
      {
        id: '5',
        name: 'pw.locationMaster.printMandatoryFieldsInReport',
        checked: advanceCustomOrder
          ? advanceCustomOrder.advanceCustomOrderStepOneCheckBox
            .printMandatoryFieldsInReport
            ? advanceCustomOrder.advanceCustomOrderStepOneCheckBox
              .printMandatoryFieldsInReport
            : false
          : false
      }
    ];
    const advanceCustomOrderConfigurationStepOneCheckBox = new AdvanceCustomOrderConfigurationStepThreeCheckBox(
      1,
      advanceCustomOrderTabOnecheckBoxes,
      advanceCustomOrder ? advanceCustomOrder.sparewtToleranceforStockItem
        ? advanceCustomOrder.sparewtToleranceforStockItem
        : '' : '',
      advanceCustomOrder ? advanceCustomOrder.servicewtToleranceforStockItem
        ? advanceCustomOrder.servicewtToleranceforStockItem
        : '' : ''
    );
    const advanceCustomOrderConfiguration = new AdvanceCustomOrderConfigurationStepThree(
      1,

      advanceCustomOrder ? advanceCustomOrder.validityDaysforAutoClosureInAdvanceBooking
        ? advanceCustomOrder.validityDaysforAutoClosureInAdvanceBooking
        : '' : '',
      advanceCustomOrder ? advanceCustomOrder.validityDaysforActivateInAdvanceBooking
        ? advanceCustomOrder.validityDaysforActivateInAdvanceBooking
        : '' : '',
      advanceCustomOrder ? advanceCustomOrder.validityDaysforReleaseInvInAdvancebooking
        ? advanceCustomOrder.validityDaysforReleaseInvInAdvancebooking
        : '' : '',
      advanceCustomOrder ? advanceCustomOrder.validityDaysforAutoClosureInCustomerOrder
        ? advanceCustomOrder.validityDaysforAutoClosureInCustomerOrder
        : '' : '',
      advanceCustomOrder ? advanceCustomOrder.validityDaysforActivateInCustomerOrder
        ? advanceCustomOrder.validityDaysforActivateInCustomerOrder
        : '' : '',
      advanceCustomOrder ? advanceCustomOrder.validitydaysforReleaseInvInCustomerOrder
        ? advanceCustomOrder.validitydaysforReleaseInvInCustomerOrder
        : '' : '',
      advanceCustomOrder ? advanceCustomOrder.goldRateAttempts
        ? advanceCustomOrder.goldRateAttempts
        : '' : '',
      advanceCustomOrder ? advanceCustomOrder.manualBillWeightDeviation
        ? advanceCustomOrder.manualBillWeightDeviation
        : '' : ''
    );
    const advanceCustomOrderMain = new AdvanceCustomOrderConfigurationMainStepThree(
      1,
      advanceCustomOrderConfiguration,
      advanceCustomOrderConfigurationStepOneCheckBox
    );

    return advanceCustomOrderMain;
  }
  getCssProp() {
    const annot = (AdvanceCustomOrdersConfigurationStepOneComponent as any)
      .__annotations__;
    return annot[0].styles;
  }

  public getInputs(forms) {
    return {
      formConfig: this.setFormConfig(),
      formFields: forms.buildFormFields()
    };
  }
  public setFormConfig() {
    return {
      formName: 'Location Master Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE5
    };
  }
  addButton(formGroup: FormGroup) {
    const advanceCustomOrderTabOnecheckBoxes = {
      cancellationAllowedforAdvanceBooking:
        formGroup.value[advanceCustomOrderStepOneSubForm][
        advanceCustomOrderStepOneSubFormCheckBox
        ][0],
      cancellationAllowedforCustomerOrder:
        formGroup.value[advanceCustomOrderStepOneSubForm][
        advanceCustomOrderStepOneSubFormCheckBox
        ][1],
      activateAllowedforAdvanceBooking:
        formGroup.value[advanceCustomOrderStepOneSubForm][
        advanceCustomOrderStepOneSubFormCheckBox
        ][2],
      activateAllowedforCustomerOrder:
        formGroup.value[advanceCustomOrderStepOneSubForm][
        advanceCustomOrderStepOneSubFormCheckBox
        ][3],
      printMandatoryFieldsInReport:
        formGroup.value[advanceCustomOrderStepOneSubForm][
        advanceCustomOrderStepOneSubFormCheckBox
        ][4]
    };

    const formData = {
      advanceCustomOrderTabOnecheckBoxes: advanceCustomOrderTabOnecheckBoxes,
      validityDaysforAutoClosureInAdvanceBooking:
        formGroup.value[
        advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm
        ][
        advanceCustomOrderStepOneSubFormFeildvalidityDaysforAutoClosureInAdvanceBooking
        ],
      validityDaysforActivateInAdvanceBooking:
        formGroup.value[
        advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm
        ][
        advanceCustomOrderStepOneSubFormFeildvalidityDaysforActivateInAdvanceBooking
        ],
      validityDaysforReleaseInvInAdvancebooking:
        formGroup.value[
        advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm
        ][
        advanceCustomOrderStepOneSubFormFeildvalidityDaysforReleaseInvInAdvancebooking
        ],
      validityDaysforAutoClosureInCustomerOrder:
        formGroup.value[
        advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm
        ][
        advanceCustomOrderStepOneSubFormFeildvalidityDaysforAutoClosureInCustomerOrder
        ],
      validityDaysforActivateInCustomerOrder:
        formGroup.value[
        advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm
        ][
        advanceCustomOrderStepOneSubFormFeildvalidityDaysforActivateInCustomerOrder
        ],
      validitydaysforReleaseInvInCustomerOrder:
        formGroup.value[
        advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm
        ][
        advanceCustomOrderStepOneSubFormFeildvaliditydaysforReleaseInvInCustomerOrder
        ],
      goldRateAttempts:
        formGroup.value[
        advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm
        ][advanceCustomOrderStepOneSubFormFeildgoldRateAttempts],
      manualBillWeightDeviation:
        formGroup.value[
        advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm
        ][advanceCustomOrderStepOneSubFormFeildmanualBillWeightDeviation],
      sparewtToleranceforStockItem:
        formGroup.value[advanceCustomOrderStepOneSubForm][
        advanceCustomOrderStepOneSubFormFeildsparewtToleranceforStockItem
        ],
      servicewtToleranceforStockItem:
        formGroup.value[advanceCustomOrderStepOneSubForm][
        advanceCustomOrderStepOneSubFormFeildservicewtToleranceforStockItem
        ]
    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.tabOne.emit(formData);
      }
    });

  }
  deleteButton(formGroup: FormGroup) {
    // this.router.navigate([], {
    //   relativeTo: this.activatedRoute,
    //   queryParams: { refresh: new Date().getTime() },
    //   queryParamsHandling: 'merge',
    //   skipLocationChange: true
    // });
    // formGroup.reset();

    // this.ngOnDestroy();
    // this.destroy$ = new Subject<null>();
    // this.ngOnInit();
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });


  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }


  public formGroupCreated(formGroup: FormGroup) {
    this.conditionalValidation(formGroup);
  }

  public conditionalValidation(formGroup: FormGroup) {
    if (formGroup) {
      if (this.locationTypeCode === locationTypeCode_BTQ) {
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm, advanceCustomOrderStepOneSubFormFeildvalidityDaysforAutoClosureInAdvanceBooking, [Validators.required, Validators.pattern('^[1-9][0-9]*$')])
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm, advanceCustomOrderStepOneSubFormFeildvalidityDaysforActivateInAdvanceBooking, [Validators.required, Validators.pattern('^[1-9][0-9]*$')])
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm, advanceCustomOrderStepOneSubFormFeildvalidityDaysforReleaseInvInAdvancebooking, [Validators.required, Validators.pattern('^[1-9][0-9]*$')])
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm, advanceCustomOrderStepOneSubFormFeildvalidityDaysforAutoClosureInCustomerOrder, [Validators.required, Validators.pattern('^[1-9][0-9]*$')])
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm, advanceCustomOrderStepOneSubFormFeildvalidityDaysforActivateInCustomerOrder, [Validators.required, Validators.pattern('^[1-9][0-9]*$')])
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm, advanceCustomOrderStepOneSubFormFeildvaliditydaysforReleaseInvInCustomerOrder, [Validators.required, Validators.pattern('^[1-9][0-9]*$')])
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm, advanceCustomOrderStepOneSubFormFeildgoldRateAttempts, [Validators.required, Validators.pattern('^[1-9][0-9]*(.{1})?[0-9]{1,3}')])
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm, advanceCustomOrderStepOneSubFormFeildmanualBillWeightDeviation, [Validators.required, Validators.pattern('[0-9]*(.{1})?[0-9]+')])
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubForm, advanceCustomOrderStepOneSubFormFeildsparewtToleranceforStockItem, [Validators.required, Validators.pattern('[+]?([0-9]*[.])?[0-9]+')])
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubForm, advanceCustomOrderStepOneSubFormFeildservicewtToleranceforStockItem, [Validators.required, Validators.pattern('[+]?([0-9]*[.])?[0-9]+')])
      } else {
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm, advanceCustomOrderStepOneSubFormFeildvalidityDaysforAutoClosureInAdvanceBooking, [Validators.pattern('^[1-9]*$')])
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm, advanceCustomOrderStepOneSubFormFeildvalidityDaysforActivateInAdvanceBooking, [Validators.pattern('^[1-9]*$')])
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm, advanceCustomOrderStepOneSubFormFeildvalidityDaysforReleaseInvInAdvancebooking, [Validators.pattern('^[0-9]*$')])
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm, advanceCustomOrderStepOneSubFormFeildvalidityDaysforAutoClosureInCustomerOrder, [Validators.pattern('^[1-9]*$')])
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm, advanceCustomOrderStepOneSubFormFeildvalidityDaysforActivateInCustomerOrder, [Validators.pattern('^[1-9]*$')])
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm, advanceCustomOrderStepOneSubFormFeildvaliditydaysforReleaseInvInCustomerOrder, [Validators.pattern('^[0-9]*$')])
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm, advanceCustomOrderStepOneSubFormFeildgoldRateAttempts, [Validators.pattern('[0-9]*(.{1})?[0-9]{1,3}')])
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm, advanceCustomOrderStepOneSubFormFeildmanualBillWeightDeviation, [Validators.pattern('[+]?([0-9]*[.])?[0-9]+')])
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubForm, advanceCustomOrderStepOneSubFormFeildsparewtToleranceforStockItem, [Validators.pattern('[+]?([0-9]*[.])?[0-9]+')])
        this.hf.setValidators(formGroup, advanceCustomOrderStepOneSubForm, advanceCustomOrderStepOneSubFormFeildservicewtToleranceforStockItem, [Validators.pattern('[+]?([0-9]*[.])?[0-9]+')])
      }
    }
  }
}
