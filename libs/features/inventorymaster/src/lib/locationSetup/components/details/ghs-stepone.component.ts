import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  OnDestroy
} from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { TEMPLATE5, HelperFunctions } from '@poss-web/shared';
import { GHSDayDetailsModelCreditNoteConfig } from '../../models/ghs-one-daydetails.model';
import { GHSDayDetailsModelValidityConfig } from '../../models/ghs-one-daydetails1.model';
import { GHSMainModel } from '../../models/ghs-one-main.model';
import { LocationMasterFacade } from '../../+state/location-master.facade';
import {
  ghsStepOneFirstSubForm,
  ghsStepOneSecondSubForm,
  ghsStepOneFirstSubFormFeildSuspendingCNs,
  ghsStepOneFirstSubFormFeildTransferredCNs,
  ghsStepOneFirstSubFormFeildActivatedCNs,
  ghsStepOneSecondSubFormDDValidityDays,
  ghsStepOneSecondSubFormConsolidateAttempts,
  ghsStepOneSecondSubFormRealisationDays,
  ghsStepOneSecondSubFormValidityDays,
  ghsStepOneFirstSubFormFeildBaseCurrency,
  locationTypeCode_BTQ
} from '../../models/locationsetup.constants';
import { takeUntil } from 'rxjs/operators';
import { combineLatest, Observable, Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'poss-web-ghs-stepone',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="[
        'pw.locationMaster.cancel',
        'pw.locationMaster.saveAndContinue'
      ]"
      (deleteForm)="deleteButton($event)"
      (addForm)="addButton($event)"
      (formGroupCreated)="formGroupCreated($event)"
    >
    </poss-web-dynamic-form>
  `
})
export class GHSSteponeComponent implements OnInit, OnDestroy {
  formFields: any;
  public currentStyle: Observable<any>;
  destroy$: Subject<null> = new Subject<null>();
  public locationTypeCode: string;

  @Input() ghsData$: Observable<any>;
  @Input() baseCurrency$: Observable<any>;
  @Input() locationDetails$: Observable<any>;
  saveButton: string;
  cancleButton: string;

  @Output() tabOne: EventEmitter<{
    baseCurrency: string;
    suspendingCNs: number;
    transferredCNs: number;
    activatedCNs: number;
    ddValidityDays: number;
    consolidateAttempts: number;
    realisationDays: number;
    validityDays: number;
  }> = new EventEmitter();

  ghsData: Observable<any>;
  newLocCode$: Subject<null> = new Subject<null>();
  locationCode: any;

  constructor(
    public loactionMasterFacade: LocationMasterFacade,
    private hf: HelperFunctions,
    public dialog: MatDialog,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    combineLatest(this.ghsData$, this.baseCurrency$, this.locationDetails$)
      .pipe(takeUntil(this.destroy$))
      .subscribe(results => {
        this.locationTypeCode = results[2].locationTypeCode;
        const form = this.prepareSet(results[0], results[1]);
        this.formFields = this.getInputs(form);
        this.currentStyle = this.getCssProp();
      });
  }

  prepareSet(data: any, baseCurrency: any) {
    if (data.baseCurrency) {
      baseCurrency = this.hf.patchValue(
        baseCurrency,
        'id',
        'selected',
        data.baseCurrency,
        true
      );
    }

    const dayDetails: GHSDayDetailsModelCreditNoteConfig = new GHSDayDetailsModelCreditNoteConfig(
      1,
      baseCurrency,
      data.suspendingCNs ? data.suspendingCNs : '',
      data.transferredCNs ? data.transferredCNs : '',
      data.activatedCNs ? data.activatedCNs : ''
    );

    const dayDetaisl1: GHSDayDetailsModelValidityConfig = new GHSDayDetailsModelValidityConfig(
      1,
      data.ddValidityDays ? data.ddValidityDays : '',
      data.consolidateAttempts ? data.consolidateAttempts : '',
      data.realisationDays ? data.realisationDays : '',
      data.validityDays ? data.validityDays : ''
    );

    const GHSMain = new GHSMainModel(1, dayDetails, dayDetaisl1);
    return GHSMain;
  }

  getCssProp() {
    const annot = (GHSSteponeComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form: any) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }

  public addButton(formGroup: FormGroup) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.tabOne.emit({
          baseCurrency:
            formGroup.value[ghsStepOneFirstSubForm][
            ghsStepOneFirstSubFormFeildBaseCurrency
            ],
          suspendingCNs:
            formGroup.value[ghsStepOneFirstSubForm][
            ghsStepOneFirstSubFormFeildSuspendingCNs
            ],
          transferredCNs:
            formGroup.value[ghsStepOneFirstSubForm][
            ghsStepOneFirstSubFormFeildTransferredCNs
            ],
          activatedCNs:
            formGroup.value[ghsStepOneFirstSubForm][
            ghsStepOneFirstSubFormFeildActivatedCNs
            ],

          ddValidityDays:
            formGroup.value[ghsStepOneSecondSubForm][
            ghsStepOneSecondSubFormDDValidityDays
            ],
          consolidateAttempts:
            formGroup.value[ghsStepOneSecondSubForm][
            ghsStepOneSecondSubFormConsolidateAttempts
            ],
          realisationDays:
            formGroup.value[ghsStepOneSecondSubForm][
            ghsStepOneSecondSubFormRealisationDays
            ],
          validityDays:
            formGroup.value[ghsStepOneSecondSubForm][
            ghsStepOneSecondSubFormValidityDays
            ]
        });
      }
    });
  }

  public deleteButton() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });
  }

  public setFormConfig() {
    return {
      formName: 'DAYDETAILS',
      formTemplate: TEMPLATE5
    };
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }


  public formGroupCreated(formGroup: FormGroup) {
    this.conditionalValidation(formGroup);
  }

  public conditionalValidation(formGroup: FormGroup) {
    if (formGroup) {
      if (this.locationTypeCode === locationTypeCode_BTQ) {
        this.hf.setValidators(formGroup, ghsStepOneFirstSubForm, ghsStepOneFirstSubFormFeildSuspendingCNs, [Validators.required, Validators.pattern('^[0-9]*$')])
        this.hf.setValidators(formGroup, ghsStepOneFirstSubForm, ghsStepOneFirstSubFormFeildTransferredCNs, [Validators.required, Validators.pattern('^[0-9]*$')])
        this.hf.setValidators(formGroup, ghsStepOneFirstSubForm, ghsStepOneFirstSubFormFeildActivatedCNs, [Validators.required, Validators.pattern('^[0-9]*$')])
      } else {
        this.hf.setValidators(formGroup, ghsStepOneFirstSubForm, ghsStepOneFirstSubFormFeildSuspendingCNs, [Validators.pattern('^[0-9]*$')])
        this.hf.setValidators(formGroup, ghsStepOneFirstSubForm, ghsStepOneFirstSubFormFeildTransferredCNs, [Validators.pattern('^[0-9]*$')])
        this.hf.setValidators(formGroup, ghsStepOneFirstSubForm, ghsStepOneFirstSubFormFeildActivatedCNs, [Validators.pattern('^[0-9]*$')])
      }
    }
  }
}
