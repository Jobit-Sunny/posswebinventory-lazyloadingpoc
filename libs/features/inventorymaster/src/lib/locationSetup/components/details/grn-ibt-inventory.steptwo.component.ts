import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnDestroy
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LocationServiceService } from '../../services/location-service.service';
import { IBTConfiguration } from '../../models/grn-ibt-inventory-two-configuration.models';
import { GrnIbtInventoryKYCConfiguration } from '../../models/grn-ibt-inventory-two-kyc-configuration.models';
import { GrnIbtULPConfigurationtwo } from '../../models/grn-ibt-inventory-two-ulp-configuartion.model';
import { GrnIBT2Main } from '../../models/grn-ibt-inventory-two-main.model';
import { LocationMasterFacade } from '../../+state/location-master.facade';
import { FormGroup } from '@angular/forms';

import { combineLatest, Observable, Subject } from 'rxjs';
import {
  GRNIBTInventoryStepTwoThirdSubForm,
  GRNIBTInventoryStepTwoThirdSubFormFiledCheckBoxes,
  GRNIBTInventoryStepTwoForthSubForm,
  GRNIBTInventoryStepTwoFirstSubForm,
  GRNIBTInventoryStepTwoFirstSubFormFiledNoOfTimesRequestedInCurrentMonth,
  GRNIBTInventoryStepTwoFirstSubFormFiledTotalValueRequestedInCurrentMonth,
  GRNIBTInventoryStepTwoFirstSubFormFiledNoOfItemsRequestedInCurrentMonth
} from '../../models/locationsetup.constants';
import { TEMPLATE6 } from '@poss-web/shared';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'poss-web-grn-ibt-inventory-step-two',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="[
        'pw.locationMaster.cancel',
        'pw.locationMaster.saveAndContinue'
      ]"
      (deleteForm)="deleteButton($event)"
      (addForm)="addButton($event)"
    >
    </poss-web-dynamic-form>
  `
})
export class GRNIBTInventorySteptwoComponent implements OnInit, OnDestroy {
  @Input() GRNIBTDetails$: Observable<any>;
  destroy$: Subject<null> = new Subject<null>();
  @Output() tabTwo: EventEmitter<{
    noOfTimesrequestedInCurrentMonth: string;
    totalValueRequestedInCurrentMonth: string;
    noOfItemsRequestedInCurrentMonth: string;
    ulpConfiguration: {
      isEncirclePaymentAllowed: boolean;
    };
    kycConfiguration: {
      isUploadDocumentAllowed: boolean;
      isDownloadDocumentAllowed: boolean;
    };
  }> = new EventEmitter();
  public currentStyle: string[];
  public formFields: any;
  grnIBTInventory2main;
  constructor(
    public loactionMasterFacade: LocationMasterFacade,
    public dialog: MatDialog
  ) { }
  locationDetails;
  ngOnInit() {
    combineLatest(this.GRNIBTDetails$).subscribe(results => {
      const form = this.prepareSet(results[0]);
      this.formFields = this.getInputs(form);
      this.currentStyle = this.getCssProp();
    });
  }
  public getInputs(form) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }
  prepareSet(GRNIBTDetails) {
    const KYCConfig = new GrnIbtInventoryKYCConfiguration(1, [
      {
        id: '1',
        name: 'pw.locationMaster.isUploadDocumentAllowed',
        checked: GRNIBTDetails
          ? GRNIBTDetails.kycConfiguration.isUploadDocumentAllowed
            ? GRNIBTDetails.kycConfiguration.isUploadDocumentAllowed
            : false
          : false
      },
      {
        id: '2',
        name: 'pw.locationMaster.isDownloadDocumentAllowed',
        checked: GRNIBTDetails
          ? GRNIBTDetails.kycConfiguration.isDownloadDocumentAllowed
            ? GRNIBTDetails.kycConfiguration.isDownloadDocumentAllowed
            : false
          : false
      }
    ]);
    const ULPConfig = new GrnIbtULPConfigurationtwo(1, [
      {
        id: '1',
        name: 'pw.locationMaster.isEncirclePaymentAllowed',
        checked: GRNIBTDetails
          ? GRNIBTDetails.ulpConfiguration.isEncirclePaymentAllowed
            ? GRNIBTDetails.ulpConfiguration.isEncirclePaymentAllowed
            : false
          : false
      }
    ]);
    const IBTConfig = new IBTConfiguration(
      1,
      GRNIBTDetails
        ? GRNIBTDetails.ibtConfiguration.noOfTimesrequestedInCurrentMonth
          ? GRNIBTDetails.ibtConfiguration.noOfTimesrequestedInCurrentMonth
          : ''
        : '',
      GRNIBTDetails
        ? GRNIBTDetails.ibtConfiguration.totalValueRequestedInCurrentMonth
          ? GRNIBTDetails.ibtConfiguration.totalValueRequestedInCurrentMonth
          : ''
        : '',
      GRNIBTDetails
        ? GRNIBTDetails.ibtConfiguration.noOfItemsRequestedInCurrentMonth
          ? GRNIBTDetails.ibtConfiguration.noOfItemsRequestedInCurrentMonth
          : ''
        : ''
    );
    const grnIBTInventory2main = new GrnIBT2Main(
      1,
      IBTConfig,
      KYCConfig,
      ULPConfig
    );
    return grnIBTInventory2main;
  }

  getCssProp() {
    const annot = (GRNIBTInventorySteptwoComponent as any).__annotations__;
    return annot[0].styles;
  }

  public setFormConfig() {
    return {
      formName: 'Location Master Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE6
    };
  }
  addButton(formGroup: FormGroup) {
    const KYCConfig = {
      isUploadDocumentAllowed:
        formGroup.value[GRNIBTInventoryStepTwoThirdSubForm][
        GRNIBTInventoryStepTwoThirdSubFormFiledCheckBoxes
        ][0],
      isDownloadDocumentAllowed:
        formGroup.value[GRNIBTInventoryStepTwoThirdSubForm][
        GRNIBTInventoryStepTwoThirdSubFormFiledCheckBoxes
        ][1]
    };
    const ULPConfig = {
      isEncirclePaymentAllowed:
        formGroup.value[GRNIBTInventoryStepTwoForthSubForm]['1-checkBoxes'][0]
    };
    const formData = {
      noOfTimesrequestedInCurrentMonth:
        formGroup.value[GRNIBTInventoryStepTwoFirstSubForm][
        GRNIBTInventoryStepTwoFirstSubFormFiledNoOfTimesRequestedInCurrentMonth
        ],
      totalValueRequestedInCurrentMonth:
        formGroup.value[GRNIBTInventoryStepTwoFirstSubForm][
        GRNIBTInventoryStepTwoFirstSubFormFiledTotalValueRequestedInCurrentMonth
        ],
      noOfItemsRequestedInCurrentMonth:
        formGroup.value[GRNIBTInventoryStepTwoFirstSubForm][
        GRNIBTInventoryStepTwoFirstSubFormFiledNoOfItemsRequestedInCurrentMonth
        ],
      kycConfiguration: KYCConfig,
      ulpConfiguration: ULPConfig
    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.tabTwo.emit(formData);
      }
    });

  }
  deleteButton() {
    // this.router.navigate([], {
    //   relativeTo: this.activatedRoute,
    //   queryParams: { refresh: new Date().getTime() },
    //   queryParamsHandling: 'merge',
    //   skipLocationChange: true
    // });
    // formGroup.reset();

    // this.ngOnDestroy();
    // this.destroy$ = new Subject<null>();
    // this.ngOnInit();

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
