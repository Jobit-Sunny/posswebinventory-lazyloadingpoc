import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { TEMPLATE5, HelperFunctions } from '@poss-web/shared';
import { Rtgs } from '../../models/advance-custom-order-one-rtgs.model';
import { Foc } from '../../models/advance-custom-order-one-foc.model';
import { AdvanceCustomOrderMainStepOne } from '../../models/advance-custom-order-one-main.model';
import { combineLatest, Observable, Subject } from 'rxjs';
import { FormGroup, Validators } from '@angular/forms';
import {
  advanceCustomOrderStepTwoFirtSubFormFeild1advanceCustomOrderTabTwoFoccheckBoxes,
  advanceCustomOrderStepTwoFirtSubForm,
  advanceCustomOrderStepTwoSecondSubForm,
  locationStepTwoFirstSubFormFeildadvanceCustomOrderTabTwoRtgscheckBoxes,
  advanceCustomOrderStepTwoFirtSubFormFeildmaxWeightforFOC,
  advanceCustomOrderStepTwoFirtSubFormFeildmaxValueforFOC,
  advanceCustomOrderStepTwoFirtSubFormFeildOTPHelpdeskEmailId,
  advanceCustomOrderStepTwoFirtSubFormFeildminOTPCNValue,
  advanceCustomOrderStepTwoFirtSubFormFeildmaxNoofCN,
  advanceCustomOrderStepTwoFirtSubFormFeildMaxNumberOfDaysForPOlikelyDate,
  advanceCustomOrderStepTwoFirtSubFormFeildServiceTaxGSTRegistrationno,
  locationTypeCode_BTQ
} from '../../models/locationsetup.constants';
import { takeUntil } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'poss-web-advance-custom-orders-foc-steptwo',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="[
        'pw.locationMaster.cancel',
        'pw.locationMaster.saveAndContinue'
      ]"
      (deleteForm)="deleteButton($event)"
      (addForm)="addButton($event)"
      (formGroupCreated)="formGroupCreated($event)"
    >
    </poss-web-dynamic-form>
  `
})
export class AdvanceCustomOrdersFocSteptwoComponent
  implements OnInit, OnDestroy {
  destroy$: Subject<null> = new Subject<null>();
  public currentStyle: string[];
  public formFields: any;
  @Input() advanceAndCustomOrderFoc$: Observable<any>;
  @Input() locationDetails$: Observable<any>;
  public locationTypeCode: string;

  @Output() tabTwo = new EventEmitter<any>();
  constructor(private hf: HelperFunctions, public dialog: MatDialog) { }

  ngOnInit() {
    combineLatest(this.advanceAndCustomOrderFoc$, this.locationDetails$)
      .pipe(takeUntil(this.destroy$))
      .subscribe(results => {
        this.locationTypeCode = results[1].locationTypeCode;
        const form = this.prepareSet(results[0]);
        // if (results[0] !== null && results[1] !== null && results[2] !== null && results[3] !== null) {
        this.formFields = this.getInputs(form);
        this.currentStyle = this.getCssProp();
        // }
      });
  }

  prepareSet(advanceCustomOrderData) {
    const advanceCustomOrderTabTwoFoccheckBoxes = [
      {
        id: '1',
        name: 'pw.locationMaster.bintobintransferallowedforFOCitems',
        checked: advanceCustomOrderData
          ? advanceCustomOrderData.advanceCustomOrderTabTwoFoccheckBoxes
            .bintobintransferallowedforFOCitems
            ? advanceCustomOrderData.advanceCustomOrderTabTwoFoccheckBoxes
              .bintobintransferallowedforFOCitems
            : false
          : false
      },
      {
        id: '2',
        name: 'pw.locationMaster.isTEPsaleableitemsallowedforFOC?',
        checked: advanceCustomOrderData
          ? advanceCustomOrderData.advanceCustomOrderTabTwoFoccheckBoxes
            .isTEPsaleableitemsallowedforFOC
            ? advanceCustomOrderData.advanceCustomOrderTabTwoFoccheckBoxes
              .isTEPsaleableitemsallowedforFOC
            : false
          : false
      },
      {
        id: '3',
        name: 'pw.locationMaster.isTEPallowedforFOCitems?',
        checked: advanceCustomOrderData
          ? advanceCustomOrderData.advanceCustomOrderTabTwoFoccheckBoxes
            .isTEPallowedforFOCitems
            ? advanceCustomOrderData.advanceCustomOrderTabTwoFoccheckBoxes
              .isTEPallowedforFOCitems
            : false
          : false
      },
      {
        id: '4',
        name: 'pw.locationMaster.isFOCitemssaleable?',
        checked: advanceCustomOrderData
          ? advanceCustomOrderData.advanceCustomOrderTabTwoFoccheckBoxes
            .isFOCitemssaleable
            ? advanceCustomOrderData.advanceCustomOrderTabTwoFoccheckBoxes
              .isFOCitemssaleable
            : false
          : false
      }
    ];
    const advanceCustomOrderTabTwoRtgscheckBoxes = [
      {
        id: '1',
        name: 'pw.locationMaster.enableRTGSpayment',
        checked: advanceCustomOrderData
          ? advanceCustomOrderData.advanceCustomOrderTabTwoRtgscheckBoxes
            .enableRTGSPayment
            ? advanceCustomOrderData.advanceCustomOrderTabTwoRtgscheckBoxes
              .enableRTGSPayment
            : false
          : false
      }
    ];

    const foc = new Foc(
      1,
      advanceCustomOrderTabTwoFoccheckBoxes
        ? advanceCustomOrderTabTwoFoccheckBoxes
        : null,
      advanceCustomOrderData.maxWeightforFOC,
      advanceCustomOrderData.maxValueforFOC
    );
    const rtgs = new Rtgs(
      1,
      advanceCustomOrderTabTwoRtgscheckBoxes
        ? advanceCustomOrderTabTwoRtgscheckBoxes
        : null,
      advanceCustomOrderData.minOTPCNValue
        ? advanceCustomOrderData.minOTPCNValue
        : '',

      advanceCustomOrderData.maxNoofCN ? advanceCustomOrderData.maxNoofCN : '',
      advanceCustomOrderData.OTPHelpdeskEmailId
        ? advanceCustomOrderData.OTPHelpdeskEmailId
        : '',
      advanceCustomOrderData.maxNoofdaysforPOLikelyDate
        ? advanceCustomOrderData.maxNoofdaysforPOLikelyDate
        : '',
      advanceCustomOrderData.serviceTaxGSTRegistrationno
        ? advanceCustomOrderData.serviceTaxGSTRegistrationno
        : ''
    );
    const advanceCustomOrderMain = new AdvanceCustomOrderMainStepOne(
      1,
      foc,
      rtgs
    );
    return advanceCustomOrderMain;
  }

  getCssProp() {
    const annot = (AdvanceCustomOrdersFocSteptwoComponent as any)
      .__annotations__;
    return annot[0].styles;
  }

  public getInputs(form) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }
  public setFormConfig() {
    return {
      formName: 'Location Master Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE5
    };
  }
  addButton(formGroup: FormGroup) {
    const advanceCustomOrderTabTwoFoccheckBoxes = {
      bintobintransferallowedforFOCitems:
        formGroup.value[advanceCustomOrderStepTwoFirtSubForm][
        advanceCustomOrderStepTwoFirtSubFormFeild1advanceCustomOrderTabTwoFoccheckBoxes
        ][0],
      isTEPsaleableitemsallowedforFOC:
        formGroup.value[advanceCustomOrderStepTwoFirtSubForm][
        advanceCustomOrderStepTwoFirtSubFormFeild1advanceCustomOrderTabTwoFoccheckBoxes
        ][1],
      isTEPallowedforFOCitems:
        formGroup.value[advanceCustomOrderStepTwoFirtSubForm][
        advanceCustomOrderStepTwoFirtSubFormFeild1advanceCustomOrderTabTwoFoccheckBoxes
        ][2],
      isFOCitemssaleable:
        formGroup.value[advanceCustomOrderStepTwoFirtSubForm][
        advanceCustomOrderStepTwoFirtSubFormFeild1advanceCustomOrderTabTwoFoccheckBoxes
        ][3]
    };
    const advanceCustomOrderTabTwoRtgscheckBoxes = {
      enableRTGSPayment:
        formGroup.value[advanceCustomOrderStepTwoSecondSubForm][
        locationStepTwoFirstSubFormFeildadvanceCustomOrderTabTwoRtgscheckBoxes
        ][0]
    };
    const formData = {
      advanceCustomOrderTabTwoRtgscheckBoxes: advanceCustomOrderTabTwoRtgscheckBoxes,
      advanceCustomOrderTabTwoFoccheckBoxes: advanceCustomOrderTabTwoFoccheckBoxes,
      maxWeightforFOC:
        formGroup.value[advanceCustomOrderStepTwoFirtSubForm][
        advanceCustomOrderStepTwoFirtSubFormFeildmaxWeightforFOC
        ],
      maxValueforFOC:
        formGroup.value[advanceCustomOrderStepTwoFirtSubForm][
        advanceCustomOrderStepTwoFirtSubFormFeildmaxValueforFOC
        ],
      OTPHelpdeskEmailId:
        formGroup.value[advanceCustomOrderStepTwoSecondSubForm][
        advanceCustomOrderStepTwoFirtSubFormFeildOTPHelpdeskEmailId
        ],

      minOTPCNValue:
        formGroup.value[advanceCustomOrderStepTwoSecondSubForm][
        advanceCustomOrderStepTwoFirtSubFormFeildminOTPCNValue
        ],
      maxNoofCN:
        formGroup.value[advanceCustomOrderStepTwoSecondSubForm][
        advanceCustomOrderStepTwoFirtSubFormFeildmaxNoofCN
        ],
      maxnoofdaysforPOlikelydate:
        formGroup.value[advanceCustomOrderStepTwoSecondSubForm][
        advanceCustomOrderStepTwoFirtSubFormFeildMaxNumberOfDaysForPOlikelyDate
        ],
      serviceTaxGSTRegistrationno:
        formGroup.value[advanceCustomOrderStepTwoSecondSubForm][
        advanceCustomOrderStepTwoFirtSubFormFeildServiceTaxGSTRegistrationno
        ]
    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.tabTwo.emit(formData);
      }
    });

  }
  deleteButton(formGroup: FormGroup) {
    // this.router.navigate([], {
    //   relativeTo: this.activatedRoute,
    //   queryParams: { refresh: new Date().getTime() },
    //   queryParamsHandling: 'merge',
    //   skipLocationChange: true
    // });
    // formGroup.reset();

    // this.ngOnDestroy();
    // this.destroy$ = new Subject<null>();
    // this.ngOnInit();
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public formGroupCreated(formGroup: FormGroup) {
    this.conditionalValidation(formGroup);
  }

  public conditionalValidation(formGroup: FormGroup) {
    if (formGroup) {
      if (this.locationTypeCode === locationTypeCode_BTQ) {
        this.hf.setValidators(
          formGroup,
          advanceCustomOrderStepTwoSecondSubForm,
          advanceCustomOrderStepTwoFirtSubFormFeildMaxNumberOfDaysForPOlikelyDate,
          [Validators.required, Validators.pattern('^[0-9]*$')]
        );
      } else {
        this.hf.setValidators(
          formGroup,
          advanceCustomOrderStepTwoSecondSubForm,
          advanceCustomOrderStepTwoFirtSubFormFeildMaxNumberOfDaysForPOlikelyDate,
          [Validators.pattern('^[0-9]*$')]
        );
      }
    }
  }
}
