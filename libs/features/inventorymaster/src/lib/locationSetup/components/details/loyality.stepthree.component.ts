import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnDestroy
} from '@angular/core';

import { TEMPLATE5 } from '@poss-web/shared';
import { Tep } from '../../models/loyality-three-tep.model';
import { Loyality3Model } from '../../models/loyality-three-main.model';
import { FormGroup } from '@angular/forms';

import { Observable, combineLatest, Subject } from 'rxjs';
import {
  loyalityStepThreeFirstSubForm,
  loyalityStepThreeFirstSubFormFieldsNoOfDaysForFVTPassword,
  loyalityStepThreeFirstSubFormFieldsEnableRTGSRefund
} from '../../models/locationsetup.constants';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'poss-web-loyality-step-three',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="[
        'pw.locationMaster.cancel',
        'pw.locationMaster.saveAndContinue'
      ]"
      (deleteForm)="deleteButton($event)"
      (addForm)="addButton($event)"
    >
    </poss-web-dynamic-form>
  `
})
export class LoyalityStepthreeComponent implements OnInit, OnDestroy {
  @Output() tabThree: EventEmitter<{
    noOfDaysForFVTPassword: string;
    enableRTGSRefund: boolean;
  }> = new EventEmitter();
  @Input() loyalityDetails$: Observable<any>;
  destroy$: Subject<null> = new Subject<null>();

  constructor(public dialog: MatDialog) { }
  public currentStyle: string[];
  public formFields: any;
  ngOnInit() {
    combineLatest(this.loyalityDetails$).subscribe(results => {
      const form = this.prepareSet(results[0]);
      this.formFields = this.getInputs(form);
      this.currentStyle = this.getCssProp();
    });
  }
  prepareSet(loyalityDetails) {
    const tep = new Tep(
      1,
      loyalityDetails
        ? loyalityDetails.tep.noOfDaysForFVTPassword
          ? loyalityDetails.tep.noOfDaysForFVTPassword
          : ''
        : '',
      [
        {
          id: '1',
          name: 'pw.locationMaster.enableRTGSRefund',
          checked: loyalityDetails
            ? loyalityDetails.tep.enableRTGSrefund
              ? loyalityDetails.tep.enableRTGSrefund
              : false
            : false
        }
      ]
    );
    const loyalitymain = new Loyality3Model(1, tep);
    return loyalitymain;
  }
  getCssProp() {
    const annot = (LoyalityStepthreeComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }
  public setFormConfig() {
    return {
      formName: 'Location Master Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE5
    };
  }
  addButton(formGroup: FormGroup) {

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.tabThree.emit({
          noOfDaysForFVTPassword:
            formGroup.value[loyalityStepThreeFirstSubForm][
            loyalityStepThreeFirstSubFormFieldsNoOfDaysForFVTPassword
            ],
          enableRTGSRefund:
            formGroup.value[loyalityStepThreeFirstSubForm][
            loyalityStepThreeFirstSubFormFieldsEnableRTGSRefund
            ][0]
        });
      }
    });

  }
  deleteButton(formGroup: FormGroup) {
    // this.router.navigate([], {
    //   relativeTo: this.activatedRoute,
    //   queryParams: { refresh: new Date().getTime() },
    //   queryParamsHandling: 'merge',
    //   skipLocationChange: true
    // });
    // formGroup.reset();

    // this.ngOnDestroy();
    // this.destroy$ = new Subject<null>();
    // this.ngOnInit();

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
