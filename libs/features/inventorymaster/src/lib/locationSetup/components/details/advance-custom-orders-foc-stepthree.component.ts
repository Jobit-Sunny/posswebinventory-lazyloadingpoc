import { TEMPLATE5, HelperFunctions } from '@poss-web/shared';
import {
  OnInit,
  Component,
  Input,
  EventEmitter,
  Output,
  OnDestroy
} from '@angular/core';

import { CheckItOut } from '../../models/advance-custom-order-checkitout.model';
import { AdvanceCustomOrderMainStepTwo } from '../../models/advance-custom-order-two-main.model';
import { combineLatest, Observable, Subject, of } from 'rxjs';
import { FormGroup } from '@angular/forms';
import {

  advanceCustomOrderStepThreeSubForm,
  advanceCustomOrderStepThreeSubFormFeildadvanceCustomOrderTabTwocheckBoxes,
  advanceCustomOrderStepThreeSubFormtwo,
  advanceCustomOrderStepThreeSubFormFeildconfigurePaymentModesCheckBoxes,
  advanceCustomOrderStepThreeSubFormtwoFeildpaymentMode,
  advanceCustomOrderStepThreeSubFormThree,
  advanceCustomOrderStepThreeSubFormThreeFeildpriceGroupTypeCode,
  advanceCustomOrderStepThreeFirtSubFormFeildpriceGroupCode
} from '../../models/locationsetup.constants';
import { takeUntil } from 'rxjs/operators';
import { ConfigurePaymentMode } from '../../models/advance-custom-order-two-configure-payment-mode.models';
import { LocationPricegroupmapping } from '../../models/advance-custom-order-two-location-price-grouping.models';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';


@Component({
  selector: 'poss-web-advance-custom-orders-foc-stepthree',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="[
        'pw.locationMaster.cancel',
        'pw.locationMaster.saveAndContinue'
      ]"
      (deleteForm)="deleteButton($event)"
      (addForm)="addButton($event)"
    >
    </poss-web-dynamic-form>
  `
})
export class AdvanceCustomOrdersFocStepthreeComponent
  implements OnInit, OnDestroy {
  @Input() advanceAndCustomOrderFoc$: Observable<any>;
  @Input() priceGroupCode$: Observable<any>;
  @Output() tabThree = new EventEmitter<any>();

  destroy$: Subject<null> = new Subject<null>();
  constructor(private hf: HelperFunctions,
    public dialog: MatDialog) { }
  public currentStyle: string[];
  public formFields: any;

  ngOnInit() {
    combineLatest(this.advanceAndCustomOrderFoc$, this.priceGroupCode$)
      .pipe(takeUntil(this.destroy$))
      .subscribe(results => {
        const form = this.prepareSet(results[0], results[1]);
        // if (results[0] !== null && results[1] !== null && results[2] !== null && results[3] !== null) {
        this.formFields = this.getInputs(form);
        this.currentStyle = this.getCssProp();
        // }
      });
  }

  prepareSet(advanceCustomOrder, priceGroupCodeData) {
    const advanceCustomOrderTabThreecheckBoxes = [
      {
        id: '1',
        name: 'pw.locationMaster.isOTPallowedASSM?',
        checked: advanceCustomOrder
          ? advanceCustomOrder.advanceStepThreeotpConfigurations ? advanceCustomOrder.advanceStepThreeotpConfigurations.advanceCustomOrderTabThreecheckBoxes ? advanceCustomOrder.advanceStepThreeotpConfigurations.advanceCustomOrderTabThreecheckBoxes.isOTPallowedASSM
            : false : false
          : false
      },
      {
        id: '2',
        name: 'pw.locationMaster.isOTPallowedCM?',
        checked: advanceCustomOrder
          ? advanceCustomOrder.advanceStepThreeotpConfigurations ? advanceCustomOrder.advanceStepThreeotpConfigurations.advanceCustomOrderTabThreecheckBoxes ? advanceCustomOrder.advanceStepThreeotpConfigurations.advanceCustomOrderTabThreecheckBoxes.isOTPallowedCM
            : false : false
          : false
      },
      {
        id: '3',
        name: 'pw.locationMaster.isOTPallowedAdvance?',
        checked: advanceCustomOrder
          ? advanceCustomOrder.advanceStepThreeotpConfigurations ? advanceCustomOrder.advanceStepThreeotpConfigurations.advanceCustomOrderTabThreecheckBoxes ? advanceCustomOrder.advanceStepThreeotpConfigurations.advanceCustomOrderTabThreecheckBoxes.isOTPallowedAdvance
            : false : false
          : false
      },

      {
        id: '4',
        name: 'pw.locationMaster.isOTPallowedAB?',
        checked: advanceCustomOrder
          ? advanceCustomOrder.advanceStepThreeotpConfigurations ? advanceCustomOrder.advanceStepThreeotpConfigurations.advanceCustomOrderTabThreecheckBoxes ? advanceCustomOrder.advanceStepThreeotpConfigurations.advanceCustomOrderTabThreecheckBoxes.isOTPallowedAB
            : false : false
          : false
      },
      {
        id: '5',
        name: 'pw.locationMaster.isOTPallowedGHS?',
        checked: advanceCustomOrder
          ? advanceCustomOrder.advanceStepThreeotpConfigurations ? advanceCustomOrder.advanceStepThreeotpConfigurations.advanceCustomOrderTabThreecheckBoxes ? advanceCustomOrder.advanceStepThreeotpConfigurations.advanceCustomOrderTabThreecheckBoxes.isOTPallowedGHS
            : false : false
          : false
      },
      {
        id: '6',
        name: 'pw.locationMaster.isOTPallowedCO?',
        checked: advanceCustomOrder
          ? advanceCustomOrder.advanceStepThreeotpConfigurations ? advanceCustomOrder.advanceStepThreeotpConfigurations.advanceCustomOrderTabThreecheckBoxes ? advanceCustomOrder.advanceStepThreeotpConfigurations.advanceCustomOrderTabThreecheckBoxes.isOTPallowedCO
            : false : false
          : false
      },
      {
        id: '7',
        name: 'pw.locationMaster.isOTPrequiredforGC?',
        checked: advanceCustomOrder
          ? advanceCustomOrder.advanceStepThreeotpConfigurations ? advanceCustomOrder.advanceStepThreeotpConfigurations.advanceCustomOrderTabThreecheckBoxes ? advanceCustomOrder.advanceStepThreeotpConfigurations.advanceCustomOrderTabThreecheckBoxes.isOTPrequiredforGC
            : false : false
          : false
      }
    ];


    const configurePaymentModesCheckBoxes = [
      {
        id: '1',
        name: 'pw.locationMaster.isApplicableforLocation',
        checked: advanceCustomOrder
          ? advanceCustomOrder.configurePaymentModeCheckBoxes
            ? advanceCustomOrder.configurePaymentModeCheckBoxes.configurePaymentModeCheckBoxes ? advanceCustomOrder.configurePaymentModeCheckBoxes.configurePaymentModeCheckBoxes.configurePaymentModeCheckBoxes ? advanceCustomOrder.configurePaymentModeCheckBoxes.configurePaymentModeCheckBoxes.configurePaymentModeCheckBoxes.isApplicableforLocation

              : false : false : false
          : false
      },
      {
        id: '2',
        name: 'pw.locationMaster.isApplicableforReversal',
        checked: advanceCustomOrder
          ? advanceCustomOrder.configurePaymentModeCheckBoxes
            ? advanceCustomOrder.configurePaymentModeCheckBoxes.configurePaymentModeCheckBoxes ? advanceCustomOrder.configurePaymentModeCheckBoxes.configurePaymentModeCheckBoxes.configurePaymentModeCheckBoxes ? advanceCustomOrder.configurePaymentModeCheckBoxes.configurePaymentModeCheckBoxes.configurePaymentModeCheckBoxes.isApplicableforReversal

              : false : false : false
          : false
      }
    ];



    const priceGroupCode = this.hf.patchValue(
      priceGroupCodeData,
      'id',
      'selected',
      advanceCustomOrder.priceGroupCode
        ? advanceCustomOrder.priceGroupCode
        : '',
      true
    );

    const configurePaymentMode = new ConfigurePaymentMode(
      1,
      configurePaymentModesCheckBoxes,
      advanceCustomOrder.paymentCode ? advanceCustomOrder.paymentCode : ''
    );
    const locatiopnPriceMapping = new LocationPricegroupmapping(
      1,
      advanceCustomOrder.priceGroupTypeCode
        ? advanceCustomOrder.priceGroupTypeCode
        : '',
      priceGroupCode
    );
    const checkitout = new CheckItOut(1, advanceCustomOrderTabThreecheckBoxes);
    const advanceOrderMainStepThree = new AdvanceCustomOrderMainStepTwo(
      1,
      checkitout,
      configurePaymentMode,
      locatiopnPriceMapping
    );

    return advanceOrderMainStepThree;
  }
  getCssProp() {
    const annot = (AdvanceCustomOrdersFocStepthreeComponent as any)
      .__annotations__;
    return annot[0].styles;
  }

  public getInputs(forms) {
    return {
      formConfig: this.setFormConfig(),
      formFields: forms.buildFormFields()
    };
  }
  public setFormConfig() {
    return {
      formName: 'Location Master Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE5
    };
  }
  addButton(formGroup: FormGroup) {
    const advanceCustomOrderTabThreecheckBoxes = {
      isOTPallowedASSM:
        formGroup.value[advanceCustomOrderStepThreeSubForm][
        advanceCustomOrderStepThreeSubFormFeildadvanceCustomOrderTabTwocheckBoxes
        ][0],
      isOTPallowedCM:
        formGroup.value[advanceCustomOrderStepThreeSubForm][
        advanceCustomOrderStepThreeSubFormFeildadvanceCustomOrderTabTwocheckBoxes
        ][1],
      isOTPallowedAdvance:
        formGroup.value[advanceCustomOrderStepThreeSubForm][
        advanceCustomOrderStepThreeSubFormFeildadvanceCustomOrderTabTwocheckBoxes
        ][2],
      isOTPallowedAB:
        formGroup.value[advanceCustomOrderStepThreeSubForm][
        advanceCustomOrderStepThreeSubFormFeildadvanceCustomOrderTabTwocheckBoxes
        ][3],
      isOTPallowedGHS:
        formGroup.value[advanceCustomOrderStepThreeSubForm][
        advanceCustomOrderStepThreeSubFormFeildadvanceCustomOrderTabTwocheckBoxes
        ][4],
      isOTPallowedCO:
        formGroup.value[advanceCustomOrderStepThreeSubForm][
        advanceCustomOrderStepThreeSubFormFeildadvanceCustomOrderTabTwocheckBoxes
        ][5],
      isOTPrequiredforGC:
        formGroup.value[advanceCustomOrderStepThreeSubForm][
        advanceCustomOrderStepThreeSubFormFeildadvanceCustomOrderTabTwocheckBoxes
        ][6]
    };
    const configurePaymentModeCheckBoxes = {
      isApplicableforLocation:
        formGroup.value[advanceCustomOrderStepThreeSubFormtwo][
        advanceCustomOrderStepThreeSubFormFeildconfigurePaymentModesCheckBoxes
        ][0],
      isApplicableforReversal:
        formGroup.value[advanceCustomOrderStepThreeSubFormtwo][
        advanceCustomOrderStepThreeSubFormFeildconfigurePaymentModesCheckBoxes
        ][1]
    };

    const formData = {
      advanceCustomOrderTabThreecheckBoxes: advanceCustomOrderTabThreecheckBoxes,
      configurePaymentModeCheckBoxes: configurePaymentModeCheckBoxes,
      paymentCode:
        formGroup.value[advanceCustomOrderStepThreeSubFormtwo][
        advanceCustomOrderStepThreeSubFormtwoFeildpaymentMode
        ],
      priceGroupTypeCode:
        formGroup.value[advanceCustomOrderStepThreeSubFormThree][
        advanceCustomOrderStepThreeSubFormThreeFeildpriceGroupTypeCode
        ],
      priceGroupCode:
        formGroup.value[advanceCustomOrderStepThreeSubFormThree][
        advanceCustomOrderStepThreeFirtSubFormFeildpriceGroupCode
        ]
    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.tabThree.emit(formData);
      }
    });

  }
  deleteButton(formGroup: FormGroup) {
    // this.router.navigate([], {
    //   relativeTo: this.activatedRoute,
    //   queryParams: { refresh: new Date().getTime() },
    //   queryParamsHandling: 'merge',
    //   skipLocationChange: true
    // });
    // formGroup.reset();

    // this.ngOnDestroy();
    // this.destroy$ = new Subject<null>();
    // this.ngOnInit();
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });


  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
