import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  OnDestroy
} from '@angular/core';

import { TEMPLATE5 } from '@poss-web/shared';

import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Checks2Model } from '../../models/print-one-checkitout2';
import { CheckMainModel } from '../../models/print-one-checkitmain.model';
import { ChecksModel } from '../../models/print-one-checkitout1.model';
import { LocationMasterFacade } from '../../+state/location-master.facade';
import { Observable, combineLatest, Subject } from 'rxjs';
import {
  printStepOneFirstSubForm,
  printStepOneFirstSubFormFiled,
  printStepOneFirstSubFormMakingWastage,
  printStepOneSecondSubForm,
  printStepOneSecondSubFormFiled,
  printStepTwoForm,
  printStepTwoFormFieldfreeTextForGrams,
  printStepTwoFormFieldnoOfInvoicecopies
} from '../../models/locationsetup.constants';
import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'poss-web-print-stepone',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="[
        'pw.locationMaster.cancel',
        'pw.locationMaster.saveAndContinue'
      ]"
      (deleteForm)="deleteButton($event)"
      (addForm)="addButton($event)"
    >
    </poss-web-dynamic-form>
  `
})
export class PrintSteponeComponent implements OnInit, OnDestroy {
  destroy$: Subject<null> = new Subject<null>();
  printData: any;
  public currentStyle: string[];
  public formFields: any;
  saveButton: string;
  cancleButton: string;

  @Input() printDetails$: Observable<any>;
  @Output() tabOne: EventEmitter<{
    firstForm: {
      printMakingCharges: boolean;
      printStoneValue: boolean;
      printPrice: boolean;
      printWastageComponent: boolean;
      printWastagePercent: boolean;
      printWastageCharge: boolean;
      makingWastageCharge: number;
    };

    secondForm: {
      printCustomerNumberinReport: boolean;
      printCashMemo: boolean;
      printGuaranteeCard: boolean;
      printOtherStoneWtinGuaranteeCard: boolean;
      printOtherStoneWeightinAnnexure: boolean;
      printImage: boolean;
      printGoldValue: boolean;
      freeTextForGrams: string;
      noOfInvoicecopiesforRegularOrQuickCM;
    };
  }> = new EventEmitter();

  constructor(
    public loactionMasterFacade: LocationMasterFacade,
    private translate: TranslateService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    combineLatest(this.printDetails$)
      .pipe(takeUntil(this.destroy$))
      .subscribe(results => {
        const form = this.prepareSet(results[0]);
        this.formFields = this.getInputs(form);
        this.currentStyle = this.getCssProp();
      });

    this.translatemethod1('pw.locationMaster.save&continue');
    this.translatemethod2('pw.locationMaster.cancel');
  }

  translatemethod1(key: string) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.saveButton = translatedMessage;
      });
  }

  translatemethod2(key: string) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.cancleButton = translatedMessage;
      });
  }

  prepareSet(printStepOne) {
    const checks1 = new ChecksModel(
      1,
      [
        {
          id: '1',
          name: 'pw.locationMaster.printmakingCharges',
          checked: printStepOne.printCheckbox1.printMakingCharges
        },

        {
          id: '2',
          name: 'pw.locationMaster.printStoneValue',
          checked: printStepOne.printCheckbox1.printStoneValue
        },

        {
          id: '3',
          name: 'pw.locationMaster.printPrice',
          checked: printStepOne.printCheckbox1.printPrice
        },

        {
          id: '4',
          name: 'pw.locationMaster.printWastage',
          checked: printStepOne.printCheckbox1.printWastageComponent
        },

        {
          id: '5',
          name: 'pw.locationMaster.printWastagePercent',
          checked: printStepOne.printCheckbox1.printWastagePercent
        },

        {
          id: '6',
          name: 'pw.locationMaster.pritnWastageCharge',
          checked: printStepOne.printCheckbox1.printWastageCharge
        }
      ],
      printStepOne.makingChargesorWastageHeading
    );

    //  const checks2 = new Checks2Model(1, [

    //  {id: '1', name: 'pw.locationMaster.printCustomerNo',
    //   checked: printStepOne.printCheckbox2.printCustomerNumberinReport },

    //   {id: '2', name: 'pw.locationMaster.printCashMemo',
    //   checked: printStepOne.printCheckbox2.printCashMemo },

    //   {id: '3', name: 'pw.locationMaster.printGuaranteeCard',
    //   checked: printStepOne.printCheckbox2.printGuaranteeCard },

    //   {id: '4', name: 'pw.locationMaster.printOtherStoneInGuarantee',
    //   checked: printStepOne.printCheckbox2.printOtherStoneWtinGuaranteeCard},

    //   {id: '5', name: 'pw.locationMaster.printOtherStoneInAnnexure',
    //   checked: printStepOne.printCheckbox2.printOtherStoneWeightinAnnexure },

    //   {id: '6', name: 'pw.locationMaster.printImage',
    //   checked: printStepOne.printCheckbox2.printImage },

    //   {id: '7', name: 'pw.locationMaster.printGoldValue',
    //   checked: printStepOne.printCheckbox2.printGoldValue} ],

    const checks2 = new Checks2Model(
      1,
      [
        {
          id: '1',
          name: 'pw.locationMaster.printCustomerNo',
          checked: printStepOne.printCheckbox2.printCustomerNumberinReport
        },

        {
          id: '2',
          name: 'pw.locationMaster.printCashMemo',
          checked: printStepOne.printCheckbox2.printCashMemo
        },

        {
          id: '3',
          name: 'pw.locationMaster.printGuaranteeCard',
          checked: printStepOne.printCheckbox2.printGuaranteeCard
        },

        {
          id: '4',
          name: 'pw.locationMaster.printOtherStoneInGuarantee',
          checked: printStepOne.printCheckbox2.printOtherStoneWtinGuaranteeCard
        },

        {
          id: '5',
          name: 'pw.locationMaster.printOtherStoneInAnnexure',
          checked: printStepOne.printCheckbox2.printOtherStoneWeightinAnnexure
        },

        {
          id: '6',
          name: 'pw.locationMaster.printImage',
          checked: printStepOne.printCheckbox2.printImage
        },

        {
          id: '7',
          name: 'pw.locationMaster.printGoldValue',
          checked: printStepOne.printCheckbox2.printGoldValue
        }
      ],

      printStepOne.freeTextForGrams,
      printStepOne.noOfInvoicecopiesforRegularOrQuickCM
    );

    const printMainModel = new CheckMainModel(1, checks1, checks2);

    return printMainModel;
  }

  getCssProp() {
    const annot = (PrintSteponeComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form: any) {
    return {
      formConfig: this.setFormConfig(),
      // formFields:this.checkitOut.buildFormFields()
      formFields: form.buildFormFields()
    };
  }

  public setFormConfig() {
    return {
      formName: 'Location Master Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE5
    };
  }

  addButton(event: FormGroup) {

    const firstForm = {
      printMakingCharges:
        event.value[printStepOneFirstSubForm][printStepOneSecondSubFormFiled][0],

      printStoneValue:
        event.value[printStepOneFirstSubForm][printStepOneFirstSubFormFiled][1],

      printPrice:
        event.value[printStepOneFirstSubForm][printStepOneFirstSubFormFiled][2],

      printWastageComponent:
        event.value[printStepOneFirstSubForm][printStepOneFirstSubFormFiled][3],

      printWastagePercent:
        event.value[printStepOneFirstSubForm][printStepOneFirstSubFormFiled][4],

      printWastageCharge:
        event.value[printStepOneFirstSubForm][printStepOneFirstSubFormFiled][5],

      makingWastageCharge:
        event.value[printStepOneFirstSubForm][
        printStepOneFirstSubFormMakingWastage
        ]
    };

    const secondForm = {
      printCustomerNumberinReport:
        event.value[printStepOneSecondSubForm][
        printStepOneSecondSubFormFiled
        ][0],

      printCashMemo:
        event.value[printStepOneSecondSubForm][
        printStepOneSecondSubFormFiled
        ][1],

      printGuaranteeCard:
        event.value[printStepOneSecondSubForm][
        printStepOneSecondSubFormFiled
        ][2],

      printOtherStoneWtinGuaranteeCard:
        event.value[printStepOneSecondSubForm][
        printStepOneSecondSubFormFiled
        ][3],

      printOtherStoneWeightinAnnexure:
        event.value[printStepOneSecondSubForm][
        printStepOneSecondSubFormFiled
        ][4],

      printImage:
        event.value[printStepOneSecondSubForm][
        printStepOneSecondSubFormFiled
        ][5],

      printGoldValue:
        event.value[printStepOneSecondSubForm][
        printStepOneSecondSubFormFiled
        ][6],

      freeTextForGrams:
        event.value[printStepOneSecondSubForm][
        printStepTwoFormFieldfreeTextForGrams
        ],

      noOfInvoicecopiesforRegularOrQuickCM:
        event.value[printStepOneSecondSubForm][
        printStepTwoFormFieldnoOfInvoicecopies
        ]
    };

    const form = {
      firstForm: firstForm,
      secondForm: secondForm
    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.tabOne.emit(form);
      }
    });

  }

  deleteButton(form: FormGroup) {
    // this.router.navigate([], {
    //   relativeTo: this.activatedRoute,
    //   queryParams: { refresh: new Date().getTime() },
    //   queryParamsHandling: 'merge',
    //   skipLocationChange: true
    // });
    // form.reset();

    // this.ngOnDestroy();
    // this.destroy$ = new Subject<null>();
    // this.ngOnInit();

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
