import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { TEMPLATE5, HelperFunctions } from '@poss-web/shared';
import { Observable, combineLatest, Subject } from 'rxjs';

import { LocationModel } from '../../models/location-one-details.model';
import { PersonalModel } from '../../models/location-one-address.model';
import { LocationMainModel } from '../../models/location-one-main.model';

import { takeUntil, take, filter } from 'rxjs/operators';
import { Towns } from '../../+state/location-master.actions';
import { locationStepOneSecondSubForm, addressTwo, locationStepOneFirstSubForm, locationShortName, phoneNumberTwo, subBrandName, subRegionCode, brandCode, addressOne, locationCode, locationType, description, locationFormat, factoryCodeValue, pinCode, registrationNo, mobileNumber, country, state, cityTown, emailId, regionCode, phoneNumberOne, ownerInfo, loactionNamefeild } from '../../models/locationsetup.constants';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';


@Component({
  selector: 'poss-web-location-stepone',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="['pw.locationMaster.cancel', 'pw.locationMaster.saveAndContinue']"
      (addForm)="addButton($event)"
      (deleteForm)="deleteButton($event)"
      (formGroupCreated)="formGroupCreated($event)"
    >
    </poss-web-dynamic-form>
  `
})
export class LocationSteponeComponent implements OnInit, OnDestroy {
  @Input() locationTypes$: Observable<any>;
  @Input() ownerInfo$: Observable<any>;
  @Input() locationStates$: Observable<any>;
  @Input() locationTowns$: Observable<any>;
  @Input() locationDetails$: Observable<any>;
  @Input() locationFormat$: Observable<any>;
  @Input() regions$: Observable<any>;
  @Input() brandName$: Observable<any>;
  @Input() subBrandName$: Observable<any>;
  @Input() country$: Observable<any>;

  @Output() tabOne = new EventEmitter<any>();

  locationCode: string;

  destroy$: Subject<null> = new Subject<null>();
  public currentStyle: string[];
  public formFields: any;

  constructor(private hf: HelperFunctions,
    public dialog: MatDialog) {
    // this.router.routeReuseStrategy.shouldReuseRoute = function () {
    //   return true;
    // };

    // this.mySubscription = this.router.events.subscribe((event) => {
    //   if (event instanceof NavigationEnd) {
    //     // Trick the Router into believing it's last link wasn't previously loaded
    //     this.router.navigated = false;
    //   }
    // });
  }

  ngOnInit() {
    // this.combine();
    // this.router.events.pipe(
    //   filter((event: RouterEvent) => event instanceof NavigationEnd),
    //   takeUntil(this.destroy$)
    // ).subscribe(() => {

    //   this.combine();
    // });

    combineLatest(
      this.locationDetails$,
      this.locationTypes$,
      this.ownerInfo$,
      this.brandName$,
      this.subBrandName$,
      this.locationStates$,
      this.locationTowns$,
      this.regions$,
      this.locationFormat$,
      this.country$
    )
      .pipe(takeUntil(this.destroy$))
      .subscribe(results => {
        const form = this.prepareSet(
          results[0],
          results[1],
          results[2],
          results[3],
          results[4],
          results[5],
          results[6],
          results[7],
          results[8],
          results[9]
        );

        this.formFields = this.getInputs(form);
        this.currentStyle = this.getCssProp();
      });
  }


  prepareSet(
    locationDetails: any,
    locationTypes: any,
    ownerInfoData: any,
    brandName: any,
    subBrandNameData: any,
    locationStates: any,
    locationTowns: Towns[],
    regions: any,
    locationFormatData: any,
    countryData: any
  ) {
    const locationStepOneLocationDetails = locationDetails ? locationDetails.configDetails ? locationDetails.configDetails.locationStepOne
      ? locationDetails.configDetails.locationStepOne.locationDetails
        ? locationDetails.configDetails.locationStepOne.locationDetails
        : null
      : null : null : null;
    const locationStepOnePersonalDetails = locationDetails ? locationDetails.configDetails ? locationDetails.configDetails.locationStepOne
      ? locationDetails.configDetails.locationStepOne.personalDetails
        ? locationDetails.configDetails.locationStepOne.personalDetails
        : null
      : null : null : null;

    if (locationDetails) {
      locationTypes = this.hf.patchValue(locationTypes, 'id', 'selected', locationDetails.locationTypeCode, true);
    }

    if (locationDetails) {
      locationStates = this.hf.patchValue(locationStates, 'id', 'selected', locationDetails.stateCode, true);
    }

    if (locationDetails) {
      locationTowns = this.hf.patchValue(locationTowns, 'id', 'selected', locationDetails.townCode, true);
    }

    if (locationDetails) {
      brandName = brandName.filter(d => d.brandName_id === '');

      brandName = this.hf.patchValue(brandName, 'id', 'selected', locationDetails.brandCode, true);
    }
    // subBrandName = this.hf.patchValue(
    //   subBrandName,
    //   'id',
    //   'selected',
    //   locationDetails.brandCode,
    //   true
    // );

    subBrandNameData = this.hf.patchValue(
      subBrandNameData,
      'id',
      'selected',
      locationStepOnePersonalDetails
        ? locationStepOneLocationDetails.subBrandName
          ? locationStepOneLocationDetails.subBrandName
          : ''
        : '',
      true
    );

    ownerInfoData = this.hf.patchValue(
      ownerInfoData,
      'id',
      'selected',
      locationDetails ? (locationDetails.ownerTypeCode ? locationDetails.ownerTypeCode : '') : '',
      true
    );
    countryData = this.hf.patchValue(
      countryData,
      'id',
      'selected',
      locationDetails ? locationDetails.countryCode ? locationDetails.countryCode : '' : '',
      true

    )

    // if (locationStepOneLocationDetails) {
    //   baseCurrency = this.hf.patchValue(
    //     baseCurrency,
    //     'id',
    //     'selected',
    //     locationStepOneLocationDetails.currencyCode
    //       ? locationStepOneLocationDetails.currencyCode
    //       : '',
    //     true
    //   );
    // }

    let subRegions = regions;
    if (locationDetails) {
      regions = regions.filter(d => d.regionCode_id === '');
      regions = this.hf.patchValue(regions, 'id', 'selected', locationDetails.regionCode, true);
    }

    subRegions = this.hf.patchValue(
      subRegions,
      'id',
      'selected',
      locationStepOnePersonalDetails
        ? locationStepOnePersonalDetails.subRegion
          ? locationStepOnePersonalDetails.subRegion
          : ''
        : '',
      true
    );
    if (locationDetails) {
      locationFormatData = this.hf.patchValue(locationFormatData, 'id', 'selected', locationDetails.locationFormat, true);
    }

    this.locationCode = locationDetails ? locationDetails.locationCode ? locationDetails.locationCode : '' : '';
    const location = new LocationModel(
      1,
      locationDetails ? locationDetails.locationCode ? locationDetails.locationCode : '' : '',

      locationTypes,
      locationStepOneLocationDetails
        ? locationStepOneLocationDetails.locationShortName
          ? locationStepOneLocationDetails.locationShortName
          : ''
        : '',
      brandName,
      subBrandNameData,
      locationDetails ? locationDetails.factoryCodeValue ? locationDetails.factoryCodeValue : '' : '',
      locationDetails ? locationDetails.registrationNo ? locationDetails.registrationNo : '' : '',
      ownerInfoData,
      locationFormatData,
      locationDetails ? locationDetails.description ? locationDetails.description : '' : ''
    );

    const personal = new PersonalModel(
      1,
      locationStepOnePersonalDetails
        ? locationStepOnePersonalDetails.name
          ? locationStepOnePersonalDetails.name
          : ''
        : '',

      locationDetails ? locationDetails.address ? locationDetails.address : '' : '',
      locationStepOnePersonalDetails
        ? locationStepOnePersonalDetails.adressTwo
          ? locationStepOnePersonalDetails.adressTwo
          : ''
        : '',
      locationDetails ? locationDetails.contactNo ? locationDetails.contactNo : '' : '',
      locationStepOnePersonalDetails
        ? locationStepOnePersonalDetails.phoneNumberTwo
          ? locationStepOnePersonalDetails.phoneNumberTwo
          : ''
        : '',

      locationDetails ? locationDetails.phoneNo ? locationDetails.phoneNo : '' : '',
      locationDetails ? locationDetails.pincode ? locationDetails.pincode : '' : '',
      locationDetails ? locationDetails.locationEmail ? locationDetails.locationEmail : '' : '',
      countryData,
      locationStates,
      locationTowns,

      regions,
      subRegions
    );

    const detailsmain = new LocationMainModel(1, location, personal);

    return detailsmain;
  }

  getCssProp() {
    const annot = (LocationSteponeComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form: any) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }

  public setFormConfig() {
    return {
      formName: 'Location Master Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE5
    };
  }

  public addButton(formGroup: FormGroup) {
    formGroup = formGroup.getRawValue();

    const configDetails = {
      adressTwo:
        formGroup[locationStepOneSecondSubForm][
        addressTwo
        ],
      name:
        formGroup[locationStepOneSecondSubForm][
        loactionNamefeild
        ],
      locationShortName:
        formGroup[locationStepOneFirstSubForm][
        locationShortName
        ],
      phoneNumberTwo:
        formGroup[locationStepOneSecondSubForm][
        phoneNumberTwo
        ],

      subBrandName:
        formGroup[locationStepOneFirstSubForm][
        subBrandName
        ],
      subRegionCode: formGroup[locationStepOneSecondSubForm][
        subRegionCode
      ]
    };
    const formData = {
      configDetails: configDetails,

      brandCode:
        formGroup[locationStepOneFirstSubForm][
        brandCode
        ],
      addressOne:
        formGroup[locationStepOneSecondSubForm][
        addressOne
        ],
      locationCode:
        formGroup[locationStepOneFirstSubForm][
        locationCode
        ],
      locationType:
        formGroup[locationStepOneFirstSubForm][
        locationType
        ],

      description:
        formGroup[locationStepOneFirstSubForm][
        description
        ],

      locationFormat:
        formGroup[locationStepOneFirstSubForm][
        locationFormat
        ],

      factoryCodeValue:
        formGroup[locationStepOneFirstSubForm][
        factoryCodeValue
        ],
      registrationNo:
        formGroup[locationStepOneFirstSubForm][
        registrationNo
        ],

      pinCode:
        formGroup[locationStepOneSecondSubForm][
        pinCode
        ],

      phoneNumber:
        formGroup[locationStepOneSecondSubForm][
        phoneNumberOne
        ],
      mobileNumber:
        formGroup[locationStepOneSecondSubForm][
        mobileNumber
        ],
      countryCode: formGroup[locationStepOneSecondSubForm][country],
      state:
        formGroup[locationStepOneSecondSubForm][
        state
        ],
      city:
        formGroup[locationStepOneSecondSubForm][
        cityTown
        ],
      email:
        formGroup[locationStepOneSecondSubForm][
        emailId
        ],

      ownerTypeCode:
        formGroup[locationStepOneFirstSubForm][
        ownerInfo
        ],
      regionCode:
        formGroup[locationStepOneSecondSubForm][
        regionCode
        ]
    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.tabOne.emit(formData);
      }
    });


  }

  public deleteButton(formGroup: FormGroup) {
    // this.ngOnDestroy();
    // this.destroy$ = new Subject<null>();
    // this.ngOnInit();

    // const control = formGroup.get(locationStepOneFirstSubForm).get(locationStepOneFirstSubFormregistrationNo)
    // if (!this.req) {
    //   this.req = true;
    //   control.setValidators([Validators.required]);
    // } else {
    //   this.req = false;
    //   control.setValidators([]);
    // }
    // control.updateValueAndValidity();
    // formGroup.reset();

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });

  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  // public patchValue(obj: any, selector: string, idVal: string, otherFalse?: boolean) {
  //   const objClone = obj.map((a: { id: string; }) => {
  //     const returnValue = { ...a };
  //     if (otherFalse) {
  //       returnValue[selector] = false;
  //     }
  //     if (a.id.toString() === idVal.toString()) {
  //       returnValue[selector] = true;
  //     }
  //     return returnValue
  //   });
  //   return objClone;
  // }

  public formGroupCreated(formGroup: FormGroup) {
    if (this.locationCode) {
      formGroup.get(locationStepOneFirstSubForm).get(locationCode).disable({ onlySelf: true });
    }
  }
}
