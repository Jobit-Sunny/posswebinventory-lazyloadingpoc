import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  OnDestroy
} from '@angular/core';
import { TEMPLATE5, HelperFunctions } from '@poss-web/shared';
import { FormGroup } from '@angular/forms';
import { OtherModel } from '../../models/location-two-other.model';
import { OtherDetailsMainModel } from '../../models/location-two-other-details-main.model';
import { combineLatest, Observable, Subject } from 'rxjs';

import { takeUntil } from 'rxjs/operators';
import { ChecksModel } from '../../models/location-two-checks.model';
import { locationStepThreeFirstSubForm, locationStepThreeFirstSubFormFeildregdOffice, locationStepTwoFirstSubForm, locationStepTwoFirstSubFormFeildregdOffice, locationStepTwoFirstSubFormFeildCINNumber, locationStepTwoFirstSubFormFeildcorporateAddress, locationStepTwoFirstSubFormFeildmarketCode } from '../../models/locationsetup.constants';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'poss-web-location-steptwo',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="[
        'pw.locationMaster.cancel',
        'pw.locationMaster.saveAndContinue'
      ]"
      (deleteForm)="deleteButton($event)"
      (addForm)="addButton($event)"
    >
    </poss-web-dynamic-form>
  `
})
export class LocationSteptwoComponent implements OnInit, OnDestroy {
  @Input() locationConfigDetails$: Observable<any>;
  @Input() locationSize$: Observable<any>;
  @Input() marketCode$: Observable<any>;
  @Output() tabTwo: EventEmitter<any> = new EventEmitter();


  destroy$: Subject<null> = new Subject<null>();
  public currentStyle: string[];
  public formFields: any;
  constructor(private hf: HelperFunctions, public dialog: MatDialog) { }
  ngOnInit() {
    combineLatest(
      this.locationConfigDetails$,
      this.locationSize$,
      this.marketCode$
    )
      .pipe(takeUntil(this.destroy$))
      .subscribe(results => {
        const form = this.preapareset(results[0], results[1], results[2]);
        this.formFields = this.getInputs(form);
        this.currentStyle = this.getCssProp();
        // }
      });
  }

  preapareset(locationConfigDetails: any, locationSize: any, marketCode: any) {
    const locationStepTwo = locationConfigDetails.configDetails
      ? locationConfigDetails.configDetails.locationStepTwo
        ? locationConfigDetails.configDetails.locationStepTwo
        : null
      : null;

    const locationcheckbox = [
      {
        id: '1',
        name: 'pw.locationMaster.isPasswordMandatory',
        checked: locationStepTwo
          ? locationStepTwo.configurationDetails
            ? locationStepTwo.configurationDetails.checkBoxes
              .isPasswordMandatory
              ? locationStepTwo.configurationDetails.checkBoxes
                .isPasswordMandatory
              : false
            : false
          : false
      },
      {
        id: '2',
        name: 'pw.locationMaster.isFeedbackallowedforCM',
        checked: locationStepTwo
          ? locationStepTwo.configurationDetails
            ? locationStepTwo.configurationDetails.checkBoxes
              .iFeedbackAllowedForCM
              ? locationStepTwo.configurationDetails.checkBoxes
                .iFeedbackAllowedForCM
              : false
            : false
          : false
      },
      {
        id: '3',
        name: 'pw.locationMaster.isFeedbackallowedforASSM',
        checked: locationStepTwo
          ? locationStepTwo.configurationDetails
            ? locationStepTwo.configurationDetails.checkBoxes
              .isFeedbackAllowedForAssm
              ? locationStepTwo.configurationDetails.checkBoxes
                .isFeedbackAllowedForAssm
              : false
            : false
          : false
      },
      {
        id: '4',
        name: 'pw.locationMaster.isStuddedsplitallowed',
        checked: locationStepTwo
          ? locationStepTwo.configurationDetails
            ? locationStepTwo.configurationDetails.checkBoxes
              .isStuddedSplitAllowed
              ? locationStepTwo.configurationDetails.checkBoxes
                .isStuddedSplitAllowed
              : false
            : false
          : false
      },
      {
        id: '5',
        name: 'pw.locationMaster.enableCashDeposit',
        checked: locationStepTwo
          ? locationStepTwo.configurationDetails
            ? locationStepTwo.configurationDetails.checkBoxes.enableCashDeposit
              ? locationStepTwo.configurationDetails.checkBoxes
                .enableCashDeposit
              : false
            : false
          : false
      },
      {
        id: '6',
        name: 'pw.locationMaster.enableChequeDeposit',
        checked: locationStepTwo
          ? locationStepTwo.configurationDetails
            ? locationStepTwo.configurationDetails.checkBoxes.enableChequeDeposit
              ? locationStepTwo.configurationDetails.checkBoxes
                .enableChequeDeposit
              : false
            : false
          : false
      },
      {
        id: '7',
        name: 'pw.locationMaster.digitalSignatureenable',
        checked: locationStepTwo
          ? locationStepTwo.configurationDetails
            ? locationStepTwo.configurationDetails.checkBoxes
              .digitalSignatureEnable
              ? locationStepTwo.configurationDetails.checkBoxes
                .digitalSignatureEnable
              : false
            : false
          : false
      }
    ];
    const locationstepTwoOtherDetails = locationConfigDetails.configDetails
      .locationStepTwo
      ? locationConfigDetails.configDetails.locationStepTwo.otherDetails
        ? locationConfigDetails.configDetails.locationStepTwo.otherDetails
        : {}
      : {};
    // const locationTabTwo = configDetails.locationStepTwo
    //   ? configDetails.locationStepTwo
    //   : {};

    // if (locationstepTwoOtherDetails.locationSize) {
    //   locationSize = this.hf.patchValue(
    //     locationSize,
    //     'id',
    //     'selected',
    //     locationstepTwoOtherDetails.locationSize,
    //     true
    //   );
    // }

    if (locationstepTwoOtherDetails.marketCode) {
      marketCode = this.hf.patchValue(
        marketCode,
        'id',
        'selected',
        locationstepTwoOtherDetails.marketCode,
        true
      );
    }
    // console.log(marketCode)
    const other = new OtherModel(
      1,
      locationstepTwoOtherDetails.regdOffice
        ? locationstepTwoOtherDetails.regdOffice
        : '',
      locationstepTwoOtherDetails.cinNumber
        ? locationstepTwoOtherDetails.cinNumber
        : '',
      locationstepTwoOtherDetails.corporateAddress
        ? locationstepTwoOtherDetails.corporateAddress
        : '',
      marketCode
    );
    const check = new ChecksModel(1, locationcheckbox);
    const otherDetailsMain = new OtherDetailsMainModel(1, other, check);

    return otherDetailsMain;
  }

  getCssProp() {
    const annot = (LocationSteptwoComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form: any) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }

  public setFormConfig() {
    return {
      formName: 'Location Master Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE5
    };
  }

  public addButton(formGroup: FormGroup) {

    const checkBoxes = {
      isPasswordMandatory:
        formGroup.value[locationStepThreeFirstSubForm][
        locationStepThreeFirstSubFormFeildregdOffice
        ][0],
      iFeedbackAllowedForCM:
        formGroup.value[locationStepThreeFirstSubForm][
        locationStepThreeFirstSubFormFeildregdOffice
        ][1],
      isFeedbackAllowedForAssm:
        formGroup.value[locationStepThreeFirstSubForm][
        locationStepThreeFirstSubFormFeildregdOffice
        ][2],
      isStuddedSplitAllowed:
        formGroup.value[locationStepThreeFirstSubForm][
        locationStepThreeFirstSubFormFeildregdOffice
        ][3],
      enableCashDeposit:
        formGroup.value[locationStepThreeFirstSubForm][
        locationStepThreeFirstSubFormFeildregdOffice
        ][4],
      enableChequeDeposit:
        formGroup.value[locationStepThreeFirstSubForm][
        locationStepThreeFirstSubFormFeildregdOffice
        ][5],
      digitalSignatureEnable:
        formGroup.value[locationStepThreeFirstSubForm][
        locationStepThreeFirstSubFormFeildregdOffice
        ][6]
    };
    const formData = {
      checkBoxes: checkBoxes,
      regdOffice:
        formGroup.value[locationStepTwoFirstSubForm][
        locationStepTwoFirstSubFormFeildregdOffice
        ],
      CINNumber:
        formGroup.value[locationStepTwoFirstSubForm][
        locationStepTwoFirstSubFormFeildCINNumber
        ],
      corporateAddress:
        formGroup.value[locationStepTwoFirstSubForm][
        locationStepTwoFirstSubFormFeildcorporateAddress
        ],
      // locationSize:
      //   formGroup.value[locationStepTwoFirstSubForm][
      //     locationStepTwoFirstSubFormFeildlocationSize
      //   ],
      marketCode:
        formGroup.value[locationStepTwoFirstSubForm][
        locationStepTwoFirstSubFormFeildmarketCode
        ]
    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.tabTwo.emit(formData);
      }
    });


  }

  public deleteButton(formGroup: FormGroup) {
    // this.ngOnDestroy();
    // this.destroy$ = new Subject<null>();
    // this.ngOnInit();
    //  formGroup.reset()

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
