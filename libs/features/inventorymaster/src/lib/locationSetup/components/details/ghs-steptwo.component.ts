import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  OnDestroy
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TEMPLATE5 } from '@poss-web/shared';
import { GHSCheckItOutModel } from '../../models/ghs-two-checkitout.model';

import { combineLatest, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CheckItOutMain2 } from '../../models/ghs-two-checkitout-main.model';
import { TranslateService } from '@ngx-translate/core';
import { ghsStepTwoFirstSubForm } from '../../models/locationsetup.constants';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'poss-web-ghs-steptwo',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="[
        'pw.locationMaster.cancel',
        'pw.locationMaster.saveAndContinue'
      ]"
      (deleteForm)="deleteButton($event)"
      (addForm)="addButton($event)"
    >
    </poss-web-dynamic-form>
  `
})
export class GHSSteptwoComponent implements OnInit, OnDestroy {
  formFields: any;
  public currentStyle: string[];
  destroy$: Subject<null> = new Subject<null>();
  saveButton: string;
  cancleButton: string;
  @Input() ghsdata2$: Observable<any>;
  @Output() tabTwo: EventEmitter<{
    checkBoxes: {
      isConsentLetterUploadMandatory: boolean;
      isClubbingGHSMandatory: boolean;
      eghsredemption: boolean;
      eghsrevenue: boolean;
    };
  }> = new EventEmitter();

  constructor(private translate: TranslateService,
    public dialog: MatDialog) { }

  ngOnInit() {
    combineLatest(this.ghsdata2$)
      .pipe(takeUntil(this.destroy$))
      .subscribe(results => {
        const form = this.prepareSet(results[0]);
        this.formFields = this.getInputs(form);
        this.currentStyle = this.getCssProp();
      });
  }



  prepareSet(data) {
    const ghsStepTwo = data.ghsIbtCheckBox ? data.ghsIbtCheckBox : null;

    const checkoutDetails = new GHSCheckItOutModel(1, [
      {
        name: 'pw.locationMaster.consentLetter',
        id: '1',
        checked: ghsStepTwo.isConsentLetterUploadMandatory
      },

      {
        name: 'pw.locationMaster.clubbingGHS',
        id: '2',
        checked: ghsStepTwo.isClubbingGHSMandatory
      },

      {
        name: 'pw.locationMaster.eGHSRedemption',
        id: '3',
        checked: ghsStepTwo.eghsredemption
      },

      {
        name: 'pw.locationMaster.eGHSRevenue',
        id: '4',
        checked: ghsStepTwo.eghsrevenue
      }
    ]);

    const checkoutDetails1 = new CheckItOutMain2(1, checkoutDetails);

    return checkoutDetails1;
  }

  getCssProp() {
    const annot = (GHSSteptwoComponent as any).__annotations__;
    return annot[0].styles;
  }
  public addButton(formGroup: FormGroup) {

    const checkBoxes = {
      isConsentLetterUploadMandatory: formGroup.value['1-ghs'][ghsStepTwoFirstSubForm][0],

        isClubbingGHSMandatory: formGroup.value['1-ghs'][ghsStepTwoFirstSubForm][1],

        eghsredemption: formGroup.value['1-ghs'][ghsStepTwoFirstSubForm][2],

      eghsrevenue: formGroup.value['1-ghs'][ghsStepTwoFirstSubForm][3]
    };

    const form = {
      checkBoxes: checkBoxes
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.tabTwo.emit(form);
      }
    });
  }

  public deleteButton(formGroup: FormGroup) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });
  }
  public getInputs(form: any) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }
  public setFormConfig() {
    return {
      formName: 'DAYDETAILS',
      formTemplate: TEMPLATE5
    };
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
