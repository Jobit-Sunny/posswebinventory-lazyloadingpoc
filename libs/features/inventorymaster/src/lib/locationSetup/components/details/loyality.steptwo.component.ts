import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  OnDestroy
} from '@angular/core';
import { TEMPLATE7 } from '@poss-web/shared';
import { Ccpayment } from '../../models/loyality-two-ccpayment.model';
import { EmployeeDiscount } from '../../models/loyality-two-employee-discount.model';
import { GiftCardConfiguration } from '../../models/loyality-two-giftconfiguration.model';
import { Loyality2Main } from '../../models/loyality-two-main.model';
import { FormGroup } from '@angular/forms';

import { combineLatest, Observable, Subject } from 'rxjs';
import {
  loyalityStepTwoFirstSubForm,
  loyalityStepTwoFirstSubFormFieldCheckBoxes,
  loyalityStepTwoSecondSubForm,
  loyalityStepTwoSecondSubFormFieldCheckBoxes,
  loyalityStepTwoThirdSubForm,
  loyalityStepTwoThirdSubFormFieldsCheckBoxes,
  loyalityStepTwoThirdSubFormFieldsMaximumAmount,
  loyalityStepTwoThirdSubFormFieldsMimimumAmount,
  loyalityStepTwoThirdSubFormFieldsmultiplesValues
} from '../../models/locationsetup.constants';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'poss-web-loyality-step-two',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="[
        'pw.locationMaster.cancel',
        'pw.locationMaster.saveAndContinue'
      ]"
      (deleteForm)="deleteButton($event)"
      (addForm)="addButton($event)"
    >
    </poss-web-dynamic-form>
  `
})
export class LoyalitySteptwoComponent implements OnInit, OnDestroy {
  @Input() loyalityDetails$: Observable<any>;
  destroy$: Subject<null> = new Subject<null>();
  @Output() tabTwo: EventEmitter<{
    ccPayment: {
      enableUniPay: boolean;
    };
    employeeDiscount: {
      enableEmployeeDiscount: boolean;
    };

    maximumAmount: string;
    minimumAmount: string;
    multiplesValue: string;
    giftCardConfiguration: {
      isCardCancellationAllowed: string;
      isCardInwardingAllowed: string;
      isCardActivationAllowed: string;
      isCardReedemAlowed: string;
    };
  }> = new EventEmitter();
  public currentStyle: string[];
  public formFields: any;
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    combineLatest(this.loyalityDetails$).subscribe(results => {
      const form = this.prepareSet(results[0]);
      this.formFields = this.getInputs(form);
      this.currentStyle = this.getCssProp();
    });
  }
  prepareSet(loyalityDetails) {
    const ccpayment = new Ccpayment(1, [
      {
        id: '1',
        name: 'pw.locationMaster.enableUniPay',
        checked: loyalityDetails
          ? loyalityDetails.ccPayment.enableUniPay
            ? loyalityDetails.ccPayment.enableUniPay
            : false
          : false
      }
    ]);
    const employeeDiscount = new EmployeeDiscount(1, [
      {
        id: '1',
        name: 'pw.locationMaster.enableEmployeeDiscount',
        checked: loyalityDetails
          ? loyalityDetails.employeeDiscount.enableEmployeeDiscount
            ? loyalityDetails.employeeDiscount.enableEmployeeDiscount
            : false
          : false
      }
    ]);
    const giftconfig = new GiftCardConfiguration(
      1,
      loyalityDetails
        ? loyalityDetails.giftCardConfiguration.maximumAmount
          ? loyalityDetails.giftCardConfiguration.maximumAmount
          : ''
        : '',
      loyalityDetails
        ? loyalityDetails.giftCardConfiguration.minimumAmount
          ? loyalityDetails.giftCardConfiguration.minimumAmount
          : ''
        : '',
      loyalityDetails
        ? loyalityDetails.giftCardConfiguration.multiplesValue
          ? loyalityDetails.giftCardConfiguration.multiplesValue
          : ''
        : '',
      [
        {
          id: '1',
          name: 'pw.locationMaster.isCardCancellationAllowed',
          checked: loyalityDetails
            ? loyalityDetails.giftCardConfigurationCheckBoxes
              .isCardCancellationAllowed
              ? loyalityDetails.giftCardConfigurationCheckBoxes
                .isCardCancellationAllowed
              : false
            : false
        },
        {
          id: '2',
          name: 'pw.locationMaster.isCardInwardingAllowed',
          checked: loyalityDetails
            ? loyalityDetails.giftCardConfigurationCheckBoxes
              .isCardInwardingAllowed
              ? loyalityDetails.giftCardConfigurationCheckBoxes
                .isCardInwardingAllowed
              : false
            : false
        },
        {
          id: '3',
          name: 'pw.locationMaster.isCardActivationAllowed',
          checked: loyalityDetails
            ? loyalityDetails.giftCardConfigurationCheckBoxes
              .isCardActivationAllowed
              ? loyalityDetails.giftCardConfigurationCheckBoxes
                .isCardActivationAllowed
              : false
            : false
        },
        {
          id: '4',
          name: 'pw.locationMaster.isCardReedemAllowed',
          checked: loyalityDetails
            ? loyalityDetails.giftCardConfigurationCheckBoxes.isCardReedemAlowed
              ? loyalityDetails.giftCardConfigurationCheckBoxes
                .isCardReedemAlowed
              : false
            : false
        }
      ]
    );
    const loyality2main = new Loyality2Main(
      1,
      ccpayment,
      giftconfig,
      employeeDiscount
    );
    return loyality2main;
  }
  getCssProp() {
    const annot = (LoyalitySteptwoComponent as any).__annotations__;
    return annot[0].styles;
  }
  public getInputs(form) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }

  public setFormConfig() {
    return {
      formName: 'Location Master Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE7
    };
  }
  addButton(formGroup: FormGroup) {
    const CCPaymentCheckBoxes = {
      enableUniPay:
        formGroup.value[loyalityStepTwoFirstSubForm][
        loyalityStepTwoFirstSubFormFieldCheckBoxes
        ][0]
    };
    const employeeDiscountCheckBoxes = {
      enableEmployeeDiscount:
        formGroup.value[loyalityStepTwoSecondSubForm][
        loyalityStepTwoSecondSubFormFieldCheckBoxes
        ][0]
    };
    const giftCardConfigurationCheckBoxes = {
      isCardCancellationAllowed:
        formGroup.value[loyalityStepTwoThirdSubForm][
        loyalityStepTwoThirdSubFormFieldsCheckBoxes
        ][0],
      isCardInwardingAllowed:
        formGroup.value[loyalityStepTwoThirdSubForm][
        loyalityStepTwoThirdSubFormFieldsCheckBoxes
        ][1],
      isCardActivationAllowed:
        formGroup.value[loyalityStepTwoThirdSubForm][
        loyalityStepTwoThirdSubFormFieldsCheckBoxes
        ][2],
      isCardReedemAlowed:
        formGroup.value[loyalityStepTwoThirdSubForm][
        loyalityStepTwoThirdSubFormFieldsCheckBoxes
        ][3]
    };
    const formData = {
      ccPayment: CCPaymentCheckBoxes,
      employeeDiscount: employeeDiscountCheckBoxes,
      maximumAmount:
        formGroup.value[loyalityStepTwoThirdSubForm][
        loyalityStepTwoThirdSubFormFieldsMaximumAmount
        ],
      minimumAmount:
        formGroup.value[loyalityStepTwoThirdSubForm][
        loyalityStepTwoThirdSubFormFieldsMimimumAmount
        ],
      multiplesValue:
        formGroup.value[loyalityStepTwoThirdSubForm][
        loyalityStepTwoThirdSubFormFieldsmultiplesValues
        ],
      giftCardConfiguration: giftCardConfigurationCheckBoxes
    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.tabTwo.emit(formData);
      }
    });

  }
  deleteButton(formGroup: FormGroup) {
    // this.router.navigate([], {
    //   relativeTo: this.activatedRoute,
    //   queryParams: { refresh: new Date().getTime() },
    //   queryParamsHandling: 'merge',
    //   skipLocationChange: true
    // });
    // formGroup.reset();

    // this.ngOnDestroy();
    // this.destroy$ = new Subject<null>();
    //this.ngOnInit();
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
