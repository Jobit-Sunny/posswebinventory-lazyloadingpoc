import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnDestroy
} from '@angular/core';
import { TEMPLATE5, HelperFunctions } from '@poss-web/shared';

import { GrnIbtInventoryOne } from '../../models/grn-ibt-inventory-one.model';
import { GrnIbtInventoryWalkIns } from '../../models/grn-ibt-inventory-one-walkins.models';
import { GrnInventoryMainModel } from '../../models/grn-ibt-inventory-one-main.model';
import { GRNConfiguration } from '../../models/grn-ibt-inventory-one-grnconfiguration';
import { LocationMasterFacade } from '../../+state/location-master.facade';
import { FormGroup, Validators } from '@angular/forms';

import { Observable, combineLatest, Subject } from 'rxjs';
import {
  GRNIBTInventoryStepOneFirstSubForm,
  GRNIBTInventoryStepTwoForthSubFormFiledCheckBoxes,
  GRNIBTInventoryStepOneSecondSubForm,
  GRNIBTInventoryStepOneFirstSubFormFieldNoOfDaysGRNAllowed,
  GRNIBTInventoryStepOneFirstSubFormFieldMaximumNoOfDaysForApprovedGRN,
  GRNIBTInventoryStepOneFirstSubFormFieldNoOfDaySToProtectGoldRateForGRN,
  GRNIBTInventoryStepOneFirstSubFormFieldMinimumUtilization,
  GRNIBTInventoryStepOneThirdSubForm,
  GRNIBTInventoryStepOneThirdSubFormFieldNumbersOfDays,
  GRNIBTInventoryStepOneThirdSubFormFieldNumbersOfDatesToDisplay,
  locationTypeCode_VENDOR
} from '../../models/locationsetup.constants';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'poss-web-grn-ibt-inventory-step-one',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="[
        'pw.locationMaster.cancel',
        'pw.locationMaster.saveAndContinue'
      ]"
      (deleteForm)="deleteButton($event)"
      (addForm)="addButton($event)"
      (formGroupCreated)="formGroupCreated($event)"
    >
    </poss-web-dynamic-form>
  `
})
export class GRNIBTInventorySteponeComponent implements OnInit, OnDestroy {
  @Input() GRNIBTDetails$: Observable<any>;
  destroy$: Subject<null> = new Subject<null>();
  @Output() tabOne: EventEmitter<{
    noOfDaysGRNAllowed: string;
    maximumNoOfDaysForApprovedGRN: string;
    noOfDaysToProtectGoldRateForGRN: string;
    minimumUtilization: string;
    grnGrfCheckBoxes: {
      isGRFAllowed: boolean;
      isInterBoutiqueGRNAllowed: boolean;
      isGRFAllowedInCM: boolean;
      isGRFAllowedInAdavnceBooking: boolean;
      isGRFAllowedInCustomerOrder: boolean;
    };

    maximumNoOfDaysForPhysicalReceiptDate: string;
    configurationAmountForStuddedSplit: string;
    maximumNoOfDaysForSTNCancellation: string;
    inventoryCheckBoxes: {
      isAcceptTepGepDisputeStocks: boolean;
      isStockTransferForBoutique: boolean;
      isConversionRestricted: boolean;
      isSTNcancellationAllowed: boolean;
    };
    isWaliknsDetailsMandatory: boolean;
    noOfDays: string;
    numberOfDaysToDisplay: string;
  }> = new EventEmitter();

  @Input() locationDetails$: Observable<any>;
  public locationTypeCode: string;

  public currentStyle: string[];
  public formFields: any;
  constructor(
    public loactionMasterFacade: LocationMasterFacade,
    private hf: HelperFunctions,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    combineLatest(this.GRNIBTDetails$, this.locationDetails$).subscribe(
      results => {
        this.locationTypeCode = results[1].locationTypeCode
          ? results[1].locationTypeCode
          : '';
        const form = this.prepareSet(results[0]);
        this.formFields = this.getInputs(form);
        this.currentStyle = this.getCssProp();
      }
    );
  }
  prepareSet(GRNIBTDetails) {
    const grnConfiguration = new GRNConfiguration(
      1,
      GRNIBTDetails
        ? GRNIBTDetails.grnGrfConfiguration.noOfDaysGRNAllowed
          ? GRNIBTDetails.grnGrfConfiguration.noOfDaysGRNAllowed
          : ''
        : '',
      GRNIBTDetails
        ? GRNIBTDetails.grnGrfConfiguration.maximumNoOfDaysForApprovedGRN
          ? GRNIBTDetails.grnGrfConfiguration.maximumNoOfDaysForApprovedGRN
          : ''
        : '',
      GRNIBTDetails
        ? GRNIBTDetails.grnGrfConfiguration.noOfDaysToProtectGoldRateForGRN
          ? GRNIBTDetails.grnGrfConfiguration.noOfDaysToProtectGoldRateForGRN
          : ''
        : '',
      GRNIBTDetails
        ? GRNIBTDetails.grnGrfConfiguration.minimumUtilization
          ? GRNIBTDetails.grnGrfConfiguration.minimumUtilization
          : ''
        : '',
      [
        {
          id: '1',
          name: 'pw.locationMaster.isGRFAllowed',
          checked: GRNIBTDetails
            ? GRNIBTDetails.grnGrfCheckBoxes.isGRFAllowed
              ? GRNIBTDetails.grnGrfCheckBoxes.isGRFAllowed
              : false
            : false
        },
        {
          id: '2',
          name: 'pw.locationMaster.isInterBoutiqueGRNAllowed',
          checked: GRNIBTDetails
            ? GRNIBTDetails.grnGrfCheckBoxes.isInterBoutiqueGRNAllowed
              ? GRNIBTDetails.grnGrfCheckBoxes.isInterBoutiqueGRNAllowed
              : false
            : false
        },
        {
          id: '3',
          name: 'pw.locationMaster.isGRFAllowdInCM',
          checked: GRNIBTDetails
            ? GRNIBTDetails.grnGrfCheckBoxes.isGRFAllowedInCM
              ? GRNIBTDetails.grnGrfCheckBoxes.isGRFAllowedInCM
              : false
            : false
        },
        {
          id: '4',
          name: 'pw.locationMaster.isGRFAllowedInAdvanceBooking',
          checked: GRNIBTDetails
            ? GRNIBTDetails.grnGrfCheckBoxes.isGRFAllowedInAdavnceBooking
              ? GRNIBTDetails.grnGrfCheckBoxes.isGRFAllowedInAdavnceBooking
              : false
            : false
        },
        {
          id: '5',
          name: 'pw.locationMaster.isGRFAllowedIncustomerOrder',
          checked: GRNIBTDetails
            ? GRNIBTDetails.grnGrfCheckBoxes.isGRFAllowedInCustomerOrder
              ? GRNIBTDetails.grnGrfCheckBoxes.isGRFAllowedInCustomerOrder
              : false
            : false
        }
      ]
    );
    const inventory = new GrnIbtInventoryOne(
      1,
      GRNIBTDetails
        ? GRNIBTDetails.inventory.maximumNoOfDaysForPhysicalReceiptDate
          ? GRNIBTDetails.inventory.maximumNoOfDaysForPhysicalReceiptDate
          : ''
        : '',
      GRNIBTDetails
        ? GRNIBTDetails.inventory.configurationAmountForStuddedSplit
          ? GRNIBTDetails.inventory.configurationAmountForStuddedSplit
          : ''
        : '',
      GRNIBTDetails
        ? GRNIBTDetails.inventory.maximumNoOfDaysForSTNCancellation
          ? GRNIBTDetails.inventory.maximumNoOfDaysForSTNCancellation
          : ''
        : '',
      [
        {
          id: '1',
          name: 'pw.locationMaster.isAcceptTEP/GEP/Dispute',
          checked: GRNIBTDetails
            ? GRNIBTDetails.inventoryCheckBoxes.isAcceptTepGepDisputeStocks
              ? GRNIBTDetails.inventoryCheckBoxes.isAcceptTepGepDisputeStocks
              : false
            : false
        },
        {
          id: '2',
          name: 'pw.locationMaster.isStockTransfer',
          checked: GRNIBTDetails
            ? GRNIBTDetails.inventoryCheckBoxes.isStockTransferForBoutique
              ? GRNIBTDetails.inventoryCheckBoxes.isStockTransferForBoutique
              : false
            : false
        },
        {
          id: '3',
          name: 'pw.locationMaster.isConversionRestricted',
          checked: GRNIBTDetails
            ? GRNIBTDetails.inventoryCheckBoxes.isConversionRestricted
              ? GRNIBTDetails.inventoryCheckBoxes.isConversionRestricted
              : false
            : false
        },
        {
          id: '4',
          name: 'pw.locationMaster.isSTNCancellationAllowed',
          checked: GRNIBTDetails
            ? GRNIBTDetails.inventoryCheckBoxes.isSTNcancellationAllowed
              ? GRNIBTDetails.inventoryCheckBoxes.isSTNcancellationAllowed
              : false
            : false
        }
      ]
    );
    const walkins = new GrnIbtInventoryWalkIns(
      1,
      [
        {
          id: '1',
          name: 'pw.locationMaster.isWalkinsDetailsMandatory',
          checked: GRNIBTDetails
            ? GRNIBTDetails.walkins.isWaliknsDetailsMandatory
              ? GRNIBTDetails.walkins.isWaliknsDetailsMandatory
              : false
            : false
        }
      ],
      GRNIBTDetails
        ? GRNIBTDetails.walkins.noOfDays
          ? GRNIBTDetails.walkins.noOfDays
          : ''
        : '',
      GRNIBTDetails
        ? GRNIBTDetails.walkins.numberOfDaysToDisplay
          ? GRNIBTDetails.walkins.numberOfDaysToDisplay
          : ''
        : ''
    );
    const grnInventoryMainModel = new GrnInventoryMainModel(
      1,
      grnConfiguration,
      inventory,
      walkins
    );
    return grnInventoryMainModel;
  }
  getCssProp() {
    const annot = (GRNIBTInventorySteponeComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }
  public setFormConfig() {
    return {
      formName: 'Location Master Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE5
    };
  }
  addButton(formGroup: FormGroup) {
    const grnGrfCheckBoxes = {
      isGRFAllowed:
        formGroup.value[GRNIBTInventoryStepOneFirstSubForm][
        GRNIBTInventoryStepTwoForthSubFormFiledCheckBoxes
        ][0],
      isInterBoutiqueGRNAllowed:
        formGroup.value[GRNIBTInventoryStepOneFirstSubForm][
        GRNIBTInventoryStepTwoForthSubFormFiledCheckBoxes
        ][1],
      isGRFAllowedInCM:
        formGroup.value[GRNIBTInventoryStepOneFirstSubForm][
        GRNIBTInventoryStepTwoForthSubFormFiledCheckBoxes
        ][2],
      isGRFAllowedInAdavnceBooking:
        formGroup.value[GRNIBTInventoryStepOneFirstSubForm][
        GRNIBTInventoryStepTwoForthSubFormFiledCheckBoxes
        ][3],
      isGRFAllowedInCustomerOrder:
        formGroup.value[GRNIBTInventoryStepOneFirstSubForm][
        GRNIBTInventoryStepTwoForthSubFormFiledCheckBoxes
        ][4]
    };
    const inventoryCheckBoxes = {
      isAcceptTepGepDisputeStocks:
        formGroup.value[GRNIBTInventoryStepOneSecondSubForm][
        GRNIBTInventoryStepTwoForthSubFormFiledCheckBoxes
        ][0],
      isStockTransferForBoutique:
        formGroup.value[GRNIBTInventoryStepOneSecondSubForm][
        GRNIBTInventoryStepTwoForthSubFormFiledCheckBoxes
        ][0],
      isConversionRestricted:
        formGroup.value[GRNIBTInventoryStepOneSecondSubForm][
        GRNIBTInventoryStepTwoForthSubFormFiledCheckBoxes
        ][0],
      isSTNcancellationAllowed:
        formGroup.value[GRNIBTInventoryStepOneSecondSubForm][
        GRNIBTInventoryStepTwoForthSubFormFiledCheckBoxes
        ][0]
    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {

        this.tabOne.emit({
          noOfDaysGRNAllowed:
            formGroup.value[GRNIBTInventoryStepOneFirstSubForm][
            GRNIBTInventoryStepOneFirstSubFormFieldNoOfDaysGRNAllowed
            ],
          maximumNoOfDaysForApprovedGRN:
            formGroup.value[GRNIBTInventoryStepOneFirstSubForm][
            GRNIBTInventoryStepOneFirstSubFormFieldMaximumNoOfDaysForApprovedGRN
            ],
          noOfDaysToProtectGoldRateForGRN:
            formGroup.value[GRNIBTInventoryStepOneFirstSubForm][
            GRNIBTInventoryStepOneFirstSubFormFieldNoOfDaySToProtectGoldRateForGRN
            ],
          minimumUtilization:
            formGroup.value[GRNIBTInventoryStepOneFirstSubForm][
            GRNIBTInventoryStepOneFirstSubFormFieldMinimumUtilization
            ],
          grnGrfCheckBoxes: grnGrfCheckBoxes,
          maximumNoOfDaysForPhysicalReceiptDate:
            formGroup.value['1-inventory']['1-maxNoOfDaysForPhysicalReceiptDate'],
          configurationAmountForStuddedSplit:
            formGroup.value['1-inventory']['1-configurationAmountForStuddedSplit'],
          maximumNoOfDaysForSTNCancellation:
            formGroup.value['1-inventory']['1-maxNoOfDaysForSTNCancellation'],
          inventoryCheckBoxes: inventoryCheckBoxes,
          noOfDays:
            formGroup.value[GRNIBTInventoryStepOneThirdSubForm][
            GRNIBTInventoryStepOneThirdSubFormFieldNumbersOfDays
            ],
          numberOfDaysToDisplay:
            formGroup.value[GRNIBTInventoryStepOneThirdSubForm][
            GRNIBTInventoryStepOneThirdSubFormFieldNumbersOfDatesToDisplay
            ],
          isWaliknsDetailsMandatory:
            formGroup.value[GRNIBTInventoryStepOneThirdSubForm][
            '1-isWalkinsDetailsMandatory'
            ][0]
        });
      }
    });
  }
  deleteButton() {
    // this.router.navigate([], {
    //   relativeTo: this.activatedRoute,
    //   queryParams: { refresh: new Date().getTime() },
    //   queryParamsHandling: 'merge',
    //   skipLocationChange: true
    // });
    // formGroup.reset();
    // this.ngOnDestroy();
    // this.destroy$ = new Subject<null>();
    // this.ngOnInit();

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public formGroupCreated(formGroup: FormGroup) {
    this.conditionalValidation(formGroup);
  }

  public conditionalValidation(formGroup: FormGroup) {
    if (formGroup) {
      if (this.locationTypeCode === locationTypeCode_VENDOR) {
        this.hf.setValidators(
          formGroup,
          GRNIBTInventoryStepOneFirstSubForm,
          GRNIBTInventoryStepOneFirstSubFormFieldNoOfDaysGRNAllowed,
          [Validators.required, Validators.pattern('^[1-9][0-9]*$')]
        );
        this.hf.setValidators(
          formGroup,
          GRNIBTInventoryStepOneFirstSubForm,
          GRNIBTInventoryStepOneFirstSubFormFieldMaximumNoOfDaysForApprovedGRN,
          [Validators.required, Validators.pattern('^[1-9][0-9]*$')]
        );
      } else {
        this.hf.setValidators(
          formGroup,
          GRNIBTInventoryStepOneFirstSubForm,
          GRNIBTInventoryStepOneFirstSubFormFieldNoOfDaysGRNAllowed,
          [Validators.pattern('^[1-9][0-9]*$')]
        );
        this.hf.setValidators(
          formGroup,
          GRNIBTInventoryStepOneFirstSubForm,
          GRNIBTInventoryStepOneFirstSubFormFieldMaximumNoOfDaysForApprovedGRN,
          [Validators.pattern('^[1-9][0-9]*$')]
        );
      }
    }
  }
}
