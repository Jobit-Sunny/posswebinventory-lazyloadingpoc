import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnDestroy
} from '@angular/core';
import { TEMPLATE7, HelperFunctions } from '@poss-web/shared';
import { Loyality } from '../../models/loyality-one-model';
import { PersonalDetails } from '../../models/loyality-one-personaldetails';
import { Gvpayment } from '../../models/loyality-one-gvpayment.model';
import { LoyalityMain } from '../../models/loyality-one-main.model';

import { FormGroup, Validators } from '@angular/forms';
import { LocationMasterFacade } from '../../+state/location-master.facade';
import { Observable, combineLatest, Subject } from 'rxjs';
import {
  loyalityStepOneFirstSubForm,
  loyalityStepOneFirstSubFormFieldCheckBoxes,
  loyalityStepOneThirdSubForm,
  loyalityStepOneThirdSubFormFieldCheckBoxes,
  loyalityStepOneSecondSubForm,
  loyalityStepOneSecondSubFormFieldGEPPureGoldPurity,
  loyalityStepOneSecondSubFormFieldGEPPureSilverPurity,
  loyalityStepOneSecondSubFormFieldGEPPurePlatinumPurity,
  loyalityStepOneSecondSubFormFieldGEPStandaredDeductionGold,
  loyalityStepOneSecondSubFormFieldGEPStandaredDeductionSilver,
  loyalityStepOneSecondSubFormFieldGEPStandaredDeductionPlatinum,
  loyalityStepOneSecondSubFormFieldEnableGEPSale,
  locationTypeCode_BTQ
} from '../../models/locationsetup.constants';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'poss-web-loyality-step-one',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="[
        'pw.locationMaster.cancel',
        'pw.locationMaster.saveAndContinue'
      ]"
      (deleteForm)="deleteButton($event)"
      (addForm)="addButton($event)"
      (formGroupCreated)="formGroupCreated($event)"
    >
    </poss-web-dynamic-form>
  `
})
export class LoyalitySteponeComponent implements OnInit, OnDestroy {
  constructor(
    private hf: HelperFunctions,
    public loactionMasterFacade: LocationMasterFacade,
    public dialog: MatDialog
  ) { }

  public currentStyle: string[];
  public formFields: any;
  loyalityMain: any;
  destroy$: Subject<null> = new Subject<null>();

  @Input() loyalityDetails$: Observable<any>;
  locationDetails: any;

  @Output() tabOne: EventEmitter<{
    loyality: {
      isSynchronised: boolean;
      //goldRateEditable: boolean;
      isBankingMandatory: boolean;
    };
    gvPayment: {
      enableCNCancellationForGVPayment: boolean;
      enableCNTarnsferForGVpayment: boolean;
    };
    gepPureGoldPurity: string;
    gepPureSilverPurity: string;
    gepPurePlatinumPurity: string;
    gepStandardDeductionGold: string;
    gepStandardDeductionSilver: string;
    gepStandardDeductionPlatinum: string;
    enableGEPSale: boolean;
  }> = new EventEmitter();
  @Input() locationDetails$: Observable<any>;
  public locationTypeCode: string;

  ngOnInit() {
    combineLatest(this.loyalityDetails$, this.locationDetails$).subscribe(
      results => {
        this.locationTypeCode = results[1].locationTypeCode
          ? results[1].locationTypeCode
          : '';
        const form = this.prepareSet(results[0]);
        this.formFields = this.getInputs(form);
        this.currentStyle = this.getCssProp();
      }
    );
  }

  prepareSet(loyalityDetails) {
    const loyality = new Loyality(1, [
      {
        id: '1',
        name: 'pw.locationMaster.isSynchronised',
        checked: loyalityDetails
          ? loyalityDetails.loyality.isSynchronised
            ? loyalityDetails.loyality.isSynchronised
            : false
          : false
      },
      // {
      //   id: '3',
      //   name: 'pw.locationMaster.isGoldRateEditable',
      //   checked: loyalityDetails
      //     ? loyalityDetails.loyality.goldRateEditable
      //       ? loyalityDetails.loyality.goldRateEditable
      //       : false
      //     : false
      // },
      {
        id: '2',
        name: 'pw.locationMaster.isBankingMandatory',
        checked: loyalityDetails
          ? loyalityDetails.loyality.isBankingMandatory
            ? loyalityDetails.loyality.isBankingMandatory
            : false
          : false
      }
    ]);

    const personalDetail = new PersonalDetails(
      1,
      loyalityDetails
        ? loyalityDetails.personalDetails.gepPureGoldPurity
          ? loyalityDetails.personalDetails.gepPureGoldPurity
          : ''
        : '',
      loyalityDetails
        ? loyalityDetails.personalDetails.gepPureSilverPurity
          ? loyalityDetails.personalDetails.gepPureSilverPurity
          : ''
        : '',
      loyalityDetails
        ? loyalityDetails.personalDetails.gepPurePlatinumPurity
          ? loyalityDetails.personalDetails.gepPurePlatinumPurity
          : ''
        : '',
      loyalityDetails
        ? loyalityDetails.personalDetails.gepStandardDeductionGold
          ? loyalityDetails.personalDetails.gepStandardDeductionGold
          : ''
        : '',
      loyalityDetails
        ? loyalityDetails.personalDetails.gepStandardDeductionSilver
          ? loyalityDetails.personalDetails.gepStandardDeductionSilver
          : ''
        : '',
      loyalityDetails
        ? loyalityDetails.personalDetails.gepStandardDeductionPlatinum
          ? loyalityDetails.personalDetails.gepStandardDeductionPlatinum
          : ''
        : '',
      [
        {
          id: '1',
          name: 'pw.locationMaster.enableGEPSale',
          checked: loyalityDetails
            ? loyalityDetails.personalDetails.enableGEPSale
              ? loyalityDetails.personalDetails.enableGEPSale
              : false
            : false
        }
      ]
    );

    const gvpayment = new Gvpayment(1, [
      {
        id: '1',
        name: 'pw.locationMaster.enableCNCancellationForGVPayment',
        checked: loyalityDetails
          ? loyalityDetails.gvPayment.enableCNCancellationForGVPayment
            ? loyalityDetails.gvPayment.enableCNCancellationForGVPayment
            : false
          : false
      },
      {
        id: '2',
        name: 'pw.locationMaster.enableCNTransferFORGVPayment',
        checked: loyalityDetails
          ? loyalityDetails.gvPayment.enableCNTarnsferForGVpayment
            ? loyalityDetails.gvPayment.enableCNTarnsferForGVpayment
            : false
          : false
      }
    ]);
    const loyalityMain = new LoyalityMain(
      1,
      loyality,
      gvpayment,
      personalDetail
    );
    return loyalityMain;
  }

  getCssProp() {
    const annot = (LoyalitySteponeComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }
  public setFormConfig() {
    return {
      formName: 'Location Master Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE7
    };
  }
  addButton(formGroup: FormGroup) {
    const loyality = {
      isSynchronised:
        formGroup.value[loyalityStepOneFirstSubForm][
        loyalityStepOneFirstSubFormFieldCheckBoxes
        ][0],
      isBankingMandatory:
        formGroup.value[loyalityStepOneFirstSubForm][
        loyalityStepOneFirstSubFormFieldCheckBoxes
        ][1]
    };
    const gvPayment = {
      enableCNCancellationForGVPayment:
        formGroup.value[loyalityStepOneThirdSubForm][
        loyalityStepOneThirdSubFormFieldCheckBoxes
        ][0],
      enableCNTarnsferForGVpayment:
        formGroup.value[loyalityStepOneThirdSubForm][
        loyalityStepOneThirdSubFormFieldCheckBoxes
        ][1]
    };
    const formData = {
      loyality: loyality,
      gvPayment: gvPayment,
      gepPureGoldPurity:
        formGroup.value[loyalityStepOneSecondSubForm][
        loyalityStepOneSecondSubFormFieldGEPPureGoldPurity
        ],
      gepPureSilverPurity:
        formGroup.value[loyalityStepOneSecondSubForm][
        loyalityStepOneSecondSubFormFieldGEPPureSilverPurity
        ],
      gepPurePlatinumPurity:
        formGroup.value[loyalityStepOneSecondSubForm][
        loyalityStepOneSecondSubFormFieldGEPPurePlatinumPurity
        ],
      gepStandardDeductionGold:
        formGroup.value[loyalityStepOneSecondSubForm][
        loyalityStepOneSecondSubFormFieldGEPStandaredDeductionGold
        ],

      gepStandardDeductionSilver:
        formGroup.value[loyalityStepOneSecondSubForm][
        loyalityStepOneSecondSubFormFieldGEPStandaredDeductionSilver
        ],
      gepStandardDeductionPlatinum:
        formGroup.value[loyalityStepOneSecondSubForm][
        loyalityStepOneSecondSubFormFieldGEPStandaredDeductionPlatinum
        ],
      enableGEPSale:
        formGroup.value[loyalityStepOneSecondSubForm][
        loyalityStepOneSecondSubFormFieldEnableGEPSale
        ][0]
    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.tabOne.emit(formData);
      }
    });


  }
  deleteButton() {
    // this.router.navigate([], {
    //   relativeTo: this.activatedRoute,
    //   queryParams: { refresh: new Date().getTime() },
    //   queryParamsHandling: 'merge',
    //   skipLocationChange: true
    // });
    // // formGroup.reset();

    // this.ngOnDestroy();
    // this.destroy$ = new Subject<null>();
    // this.ngOnInit();

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public formGroupCreated(formGroup: FormGroup) {
    this.conditionalValidation(formGroup);
  }

  public conditionalValidation(formGroup: FormGroup) {
    if (formGroup) {
      if (this.locationTypeCode === locationTypeCode_BTQ) {
        this.hf.setValidators(
          formGroup,
          loyalityStepOneSecondSubForm,
          loyalityStepOneSecondSubFormFieldGEPPureGoldPurity,
          [Validators.required, Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')]
        );
        this.hf.setValidators(
          formGroup,
          loyalityStepOneSecondSubForm,
          loyalityStepOneSecondSubFormFieldGEPPureSilverPurity,
          [Validators.required, Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')]
        );
        this.hf.setValidators(
          formGroup,
          loyalityStepOneSecondSubForm,
          loyalityStepOneSecondSubFormFieldGEPPurePlatinumPurity,
          [Validators.required, Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')]
        );
        this.hf.setValidators(
          formGroup,
          loyalityStepOneSecondSubForm,
          loyalityStepOneSecondSubFormFieldGEPStandaredDeductionGold,
          [Validators.required, Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')]
        );
        this.hf.setValidators(
          formGroup,
          loyalityStepOneSecondSubForm,
          loyalityStepOneSecondSubFormFieldGEPStandaredDeductionSilver,
          [Validators.required, Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')]
        );
        this.hf.setValidators(
          formGroup,
          loyalityStepOneSecondSubForm,
          loyalityStepOneSecondSubFormFieldGEPStandaredDeductionPlatinum,
          [Validators.required, Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')]
        );
      } else {
        this.hf.setValidators(
          formGroup,
          loyalityStepOneSecondSubForm,
          loyalityStepOneSecondSubFormFieldGEPPureGoldPurity,
          [Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')]
        );
        this.hf.setValidators(
          formGroup,
          loyalityStepOneSecondSubForm,
          loyalityStepOneSecondSubFormFieldGEPPureSilverPurity,
          [Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')]
        );
        this.hf.setValidators(
          formGroup,
          loyalityStepOneSecondSubForm,
          loyalityStepOneSecondSubFormFieldGEPPurePlatinumPurity,
          [Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')]
        );
        this.hf.setValidators(
          formGroup,
          loyalityStepOneSecondSubForm,
          loyalityStepOneSecondSubFormFieldGEPStandaredDeductionGold,
          [Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')]
        );
        this.hf.setValidators(
          formGroup,
          loyalityStepOneSecondSubForm,
          loyalityStepOneSecondSubFormFieldGEPStandaredDeductionSilver,
          [Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')]
        );
        this.hf.setValidators(
          formGroup,
          loyalityStepOneSecondSubForm,
          loyalityStepOneSecondSubFormFieldGEPStandaredDeductionPlatinum,
          [Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')]
        );
      }
    }
  }
}
