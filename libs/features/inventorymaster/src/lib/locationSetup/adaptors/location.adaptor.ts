import {
  LocationListing,
  SaveLocationDetailsPayload
} from '../+state/location-master.actions';

import { Observable, of } from 'rxjs';
import {
  LocationMaster,
  Ghs,
  AdvanceCustomOrder,
  Location,
  Print,
  LoyalityGEPGCTEP,
  GRNIBTInventory
} from '../models/locatonmodel';

export class LocationAdpator {
  static location: Location[] = [];
  static saveLocationDetails: SaveLocationDetailsPayload;
  static locationBylocationCode: Location;
  static locationList: LocationListing;
  static getLocationTypes(data: any): { id: string; name: string }[] {
    const locationType: { id: string; name: string }[] = [];
    for (const locationdata of data.values) {
      locationType.push({
        id: locationdata.code,
        name: locationdata.value
      });
    }

    return locationType;
  }

  static getLocationStates(
    data: any
  ): { id: string; name: string; countryCode: number }[] {
    const stateType: { id: string; name: string; countryCode: number }[] = [];
    for (const locationdata of data.results) {
      stateType.push({
        id: locationdata.stateCode,
        name: locationdata.description,
        countryCode: locationdata.countryCode
      });
    }

    return stateType;
  }

  static getLocationTowns(data: any): { id: string; name: string }[] {
    const townType: { id: string; name: string; state_id: string }[] = [];
    for (const locationdata of data.results) {
      townType.push({
        id: locationdata.townCode,
        name: locationdata.description,
        state_id: locationdata.stateCode
      });
    }
    return townType;
  }

  static getBaseCurrency(data: any): { id: string; name: string }[] {
    const baseCurrencyType: { id: string; name: string }[] = [];
    for (const baseCurrency of data.results) {
      baseCurrencyType.push({
        id: baseCurrency.countryCode,
        name: baseCurrency.currencyCode
      });
    }
    return baseCurrencyType;
  }

  // static getLocationOwnerTypes(data: any): { id: string; name: string }[] {
  //   const ownerType: { id: string; name: string }[] = [];

  //   for (const locationdata of data.values) {
  //     ownerType.push({
  //       id: locationdata.code,
  //       name: locationdata.value
  //     });
  //   }
  //   return ownerType;
  // } // Duplicate, getLocationFormat

  static getLocationFormat(data: any): { id: string; name: string }[] {
    const ownerType: { id: string; name: string }[] = [];

    for (const locationdata of data.values) {
      ownerType.push({
        id: locationdata.code,
        name: locationdata.value
      });
    }
    return ownerType;
  }

  static getLocationRegions(
    data: any
  ): { id: string; name: string; regionCode_id: string }[] {
    const regions: { id: string; name: string; regionCode_id: string }[] = [];
    for (const locationdata of data.results) {
      regions.push({
        id: locationdata.regionCode,
        name: locationdata.description,
        regionCode_id: locationdata.parentRegionCode
      });
    }
    return regions;
  }

  static getBrands(
    data: any
  ): { id: string; name: string; brandName_id: string }[] {
    const barnds: { id: string; name: string; brandName_id: string }[] = [];
    for (const locationdata of data.results) {
      barnds.push({
        id: locationdata.brandCode,
        name: locationdata.brandCode,
        brandName_id: locationdata.parentBrandCode
      });
    }
    return barnds;
  }

  static getCountry(data: any): { id: string; name: string }[] {
    const countryType: { id: string; name: string }[] = [];
    for (const countryData of data.results) {
      countryType.push({
        id: countryData.countryCode,
        name: countryData.description
      });
    }
    return countryType;
  }

  static getOnlyAdvanceCustom(data: any): AdvanceCustomOrder {
    if (data) {
      const advanceCustomOrderData = data.configDetails
        ? data.configDetails
        : {};
      const advanceStepTwoFoc = advanceCustomOrderData.advanceStepTwo
        ? advanceCustomOrderData.advanceStepTwo.foc
          ? advanceCustomOrderData.advanceStepTwo.foc
          : {}
        : {};

      const advanceStepTwoRtgs = advanceCustomOrderData.advanceStepTwo
        ? advanceCustomOrderData.advanceStepTwo.rtgs
          ? advanceCustomOrderData.advanceStepTwo.rtgs
          : {}
        : {};

      const advanceStepThreeotpConfigurations = advanceCustomOrderData.advanceStepThree
        ? advanceCustomOrderData.advanceStepThree.otpConfigurations
          ? advanceCustomOrderData.advanceStepThree.otpConfigurations
          : {}
        : {};

      const configurePaymentModeCheckBoxes = advanceCustomOrderData.advanceStepThree
        ? advanceCustomOrderData.advanceStepThree.configurePaymentMode
          ? advanceCustomOrderData.advanceStepThree.configurePaymentMode
          : {}
        : {};
      const advanceStepOneAdvanceCustomOrderConfiguration = advanceCustomOrderData.advanceStepOne
        ? advanceCustomOrderData.advanceStepOne.advanceCustomOrderConfiguration
          ? advanceCustomOrderData.advanceStepOne
            .advanceCustomOrderConfiguration
          : {}
        : {};

      let advanceCustomOrder = null;
      // if (data) {
      advanceCustomOrder = {
        configurePaymentModeCheckBoxes: configurePaymentModeCheckBoxes,
        paymentCode: advanceCustomOrderData.advanceStepThree
          ? advanceCustomOrderData.advanceStepThree.configurePaymentMode
            ? advanceCustomOrderData.advanceStepThree.configurePaymentMode
              .paymentCode
            : null
          : null,
        priceGroupTypeCode: advanceCustomOrderData.advanceStepThree
          ? advanceCustomOrderData.advanceStepThree.locationPriceMapping
            ? advanceCustomOrderData.advanceStepThree.locationPriceMapping
              .priceGroupTypeCode
            : null
          : null,
        priceGroupCode: advanceCustomOrderData.advanceStepThree
          ? advanceCustomOrderData.advanceStepThree.locationPriceMapping
            ? advanceCustomOrderData.advanceStepThree.locationPriceMapping
              .priceGroupCode
            : null
          : null,
        advanceCustomOrderTabTwoFoccheckBoxes: advanceStepTwoFoc.advanceCustomOrderTabTwoFoccheckBoxes
          ? advanceStepTwoFoc.advanceCustomOrderTabTwoFoccheckBoxes
          : {},

        advanceCustomOrderTabTwoRtgscheckBoxes: advanceStepTwoRtgs.advanceCustomOrderTabTwoRtgscheckBoxes
          ? advanceStepTwoRtgs.advanceCustomOrderTabTwoRtgscheckBoxes
          : {},

        maxWeightforFOC: advanceStepTwoFoc.maxWeightforFOC
          ? advanceStepTwoFoc.maxWeightforFOC
          : '',
        maxValueforFOC: advanceStepTwoFoc.maxValueforFOC
          ? advanceStepTwoFoc.maxValueforFOC
          : '',
        GEPPureGoldPurity: advanceStepTwoRtgs.GEPPureGoldPurity
          ? advanceStepTwoRtgs.GEPPureGoldPurity
          : '',
        maximumamount: advanceStepTwoRtgs.maximumamount
          ? advanceStepTwoRtgs.maximumamount
          : '',
        minimunamount: advanceStepTwoRtgs.minimunamount
          ? advanceStepTwoRtgs.minimunamount
          : '',
        minOTPCNValue: advanceStepTwoRtgs.minOTPCNValue
          ? advanceStepTwoRtgs.minOTPCNValue
          : '',
        OTPHelpdeskEmailId: advanceStepTwoRtgs.otpHelpdeskEmailId
          ? advanceStepTwoRtgs.otpHelpdeskEmailId
          : '',
        maxNoofCN: advanceStepTwoRtgs.maxNoofCN
          ? advanceStepTwoRtgs.maxNoofCN
          : '',
        maxNoofdaysforPOLikelyDate: advanceStepTwoRtgs.maxNoofdaysforPOLikelyDate
          ? advanceStepTwoRtgs.maxNoofdaysforPOLikelyDate
          : '',
        serviceTaxGSTRegistrationno: advanceStepTwoRtgs.serviceTaxGSTRegistrationno
          ? advanceStepTwoRtgs.serviceTaxGSTRegistrationno
          : '',
        advanceStepThreeotpConfigurations: advanceStepThreeotpConfigurations,
        advanceCustomOrderStepOneCheckBox: advanceStepOneAdvanceCustomOrderConfiguration.advanceCustomOrderStepOneCheckBox
          ? advanceStepOneAdvanceCustomOrderConfiguration.advanceCustomOrderStepOneCheckBox
          : '',
        validityDaysforAutoClosureInAdvanceBooking: advanceStepOneAdvanceCustomOrderConfiguration.validityDaysforAutoClosureInAdvanceBooking
          ? advanceStepOneAdvanceCustomOrderConfiguration.validityDaysforAutoClosureInAdvanceBooking
          : '',
        validityDaysforActivateInAdvanceBooking: advanceStepOneAdvanceCustomOrderConfiguration.validityDaysforActivateInAdvanceBooking
          ? advanceStepOneAdvanceCustomOrderConfiguration.validityDaysforActivateInAdvanceBooking
          : '',
        validityDaysforReleaseInvInAdvancebooking: advanceStepOneAdvanceCustomOrderConfiguration.validityDaysforReleaseInvInAdvancebooking
          ? advanceStepOneAdvanceCustomOrderConfiguration.validityDaysforReleaseInvInAdvancebooking
          : '',
        validityDaysforAutoClosureInCustomerOrder: advanceStepOneAdvanceCustomOrderConfiguration.validityDaysforAutoClosureInCustomerOrder
          ? advanceStepOneAdvanceCustomOrderConfiguration.validityDaysforAutoClosureInCustomerOrder
          : '',
        validityDaysforActivateInCustomerOrder: advanceStepOneAdvanceCustomOrderConfiguration.validityDaysforActivateInCustomerOrder
          ? advanceStepOneAdvanceCustomOrderConfiguration.validityDaysforActivateInCustomerOrder
          : '',
        validitydaysforReleaseInvInCustomerOrder: advanceStepOneAdvanceCustomOrderConfiguration.validitydaysforReleaseInvInCustomerOrder
          ? advanceStepOneAdvanceCustomOrderConfiguration.validitydaysforReleaseInvInCustomerOrder
          : '',
        goldRateAttempts: advanceStepOneAdvanceCustomOrderConfiguration.goldRateAttempts
          ? advanceStepOneAdvanceCustomOrderConfiguration.goldRateAttempts
          : '',
        manualBillWeightDeviation: advanceStepOneAdvanceCustomOrderConfiguration.manualBillWeightDeviation
          ? advanceStepOneAdvanceCustomOrderConfiguration.manualBillWeightDeviation
          : '',
        sparewtToleranceforStockItem: advanceStepOneAdvanceCustomOrderConfiguration.sparewtToleranceforStockItem
          ? advanceStepOneAdvanceCustomOrderConfiguration.sparewtToleranceforStockItem
          : '',
        servicewtToleranceforStockItem: advanceStepOneAdvanceCustomOrderConfiguration.servicewtToleranceforStockItem
          ? advanceStepOneAdvanceCustomOrderConfiguration.servicewtToleranceforStockItem
          : ''
      };

      // console.log(advanceCustomOrder)
      return advanceCustomOrder;
      // }
    }
  }
  static getAllLocationDetailsByloactionCode(data: any): any {
    let locationBylocationCode = {};
    if (data) {
      locationBylocationCode = {
        locationCode: data.locationCode ? data.locationCode : '',
        description: data.description ? data.description : '',
        address: data.address ? data.address : '',
        pincode: data.pincode ? data.pincode : '',
        phoneNo: data.phoneNo ? data.phoneNo : '',
        contactNo: data.contactNo ? data.contactNo : '',
        fax: data.fax ? data.fax : '',
        locationEmail: data.locationEmail ? data.locationEmail : '',
        locationTypeCode: data.locationTypeCode ? data.locationTypeCode : '',
        registrationNo: data.registrationNo ? data.registrationNo : '',
        currencyCode: data.currencyCode ? data.currencyCode : '',
        countryCode: data.countryCode ? data.countryCode : '',
        townCode: data.townCode ? data.townCode : '',
        stateCode: data.stateCode ? data.stateCode : '',
        regionCode: data.regionCode ? data.regionCode : '',
        oldFactoryCode: data.oldFactoryCode ? data.oldFactoryCode : '',
        ownerTypeCode: data.ownerTypeCode ? data.ownerTypeCode : '',
        factoryCodeValue: data.factoryCodeValue ? data.factoryCodeValue : '',
        locationFormat: data.locationFormat ? data.locationFormat : '',
        brandCode: data.brandCode ? data.brandCode : '',

        isActive: data.isActive ? data.isActive : '',
        marketCode: data.marketCode ? data.marketCode : '',
        configDetails: data.configDetails ? data.configDetails : ''
        // ghsStepOne : {
        //   marketCode: data.configDetails.ghsStepOne.marketCode ? data.configDetails.ghsStepOne.marketCode : '',
        //   baseCurrency: data.configDetails.ghsStepOne.baseCurrency ? data.configDetails.ghsStepOne.baseCurrency : '',
        //   suspendingCNs: data.configDetails.ghsStepOne.suspendingCNs ? data.configDetails.ghsStepOne.suspendingCNs : '',
        //   transferredCNs: data.configDetails.ghsStepOne.transferredCNs ? data.configDetails.ghsStepOne.transferredCNs : '',
        //   activatedCNs: data.configDetails.ghsStepOne.activatedCNs ? data.configDetails.ghsStepOne.activatedCNs : '',
        //   DDvaliditydays: data.configDetails.ghsStepOne.DDvalidityDays ? data.configDetails.ghsStepOne.DDvalidityDays : '',
        //   consolidateAttempts: data.configDetails.ghsStepOne.consolidateAttempts ? data.configDetails.ghsStepOne.consolidateAttempts : '',
        //   reversalDays: data.configDetails.ghsStepOne.reversalDays ? data.configDetails.ghsStepOne.reversalDays : '',
        //   realisationDays: data.configDetails.ghsStepOne.realisationDays? data.configDetails.ghsStepOne.realisationDays : '',
        //   validityDays: data.configDetails.ghsStepOne.validityDays? data.configDetails.ghsStepOne.validityDays : '',
        // },

        // ghsStepTwo: {
        //   ghsIbtCheckBox: data.configDetails.ghsStepTwo.ghsIbtCheckBox
        //   ? data.configDetails.ghsStepTwo.ghsIbtCheckBox
        //   : ''
        // },

        // printStepOne: {
        //   printCheckbox1: data.configDetails.printStepOne.checkItOut1
        //               ? data.configDetails.printStepOne.checkItOut1 : '',
        //   printCheckbox2:data.configDetails.printStepOne.checkItOut2
        //               ? data.configDetails.printStepOne.checkItOut2 : '',
        //   makingChargesorWastageHeading: data.configDetails.printStepOne.makingWastageCharge
        //               ? data.configDetails.printStepOne.makingWastageCharge : '',
        // },

        // printStepTwo: {
        //   freeTextForGrams: data.configDetails.printStepTwo.printCheckbox
        //   ? data.configDetails.printStepTwo.inputDetails.printCheckbox : '',

        //   noOfInvoicecopiesforRegularOrQuickCM: data.configDetails.printStepTwo.noOfInvoicecopiesforRegularOrQuickCM
        //   ? data.configDetails.printStepTwo.inputDetails.noOfInvoicecopiesforRegularOrQuickCM : '',

        //   CINNumber: data.configDetails.printStepOne.printCheckbox
        //   ? data.configDetails.printStepTwo.inputDetails.printCheckbox : '',

        //   corporateAddress: data.configDetails.printStepOne.makingChargesorWastageHeading
        //   ? data.configDetails.printStepTwo.inputDetails.makingChargesorWastageHeading : '',
        // },

        // locationShortName: data.locationShortName
        //   ? data.locationShortName
        //   : '',

        // locationIbtCheckBox: data.configDetails.locationIbtCheckBox
        //   ? data.locationIbtCheckBox
        //   : '',

        // freeTextForGrams: data.configDetails.freeTextForGrams,
        // noOfInvoicecopiesforRegularOrQuickCM: data.configDetails
        //   .noOfInvoicecopiesforRegularOrQuickCM
        //   ? data.configDetails.noOfInvoicecopiesforRegularOrQuickCM
        //   : '',
        // CINNumber: data.configDetails.CINNumber
        //   ? data.configDetails.CINNumber
        //   : '',
        // corporateAddress: data.configDetails.corporateAddress
        //   ? data.configDetails.corporateAddress
        //   : '',
        // grnIbtCheckBox: data.configDetails.grnIbtCheckBox
        //   ? data.configDetails.grnIbtCheckBox
        //   : '',
        // noofdaysGRNallowed: data.noofdaysGRNallowed
        //   ? data.noofdaysGRNallowed
        //   : '',
        // maximumnoofdaysforapprovedGRN: data.maximumnoofdaysforapprovedGRN
        //   ? data.configDetails.maximumnoofdaysforapprovedGRN
        //   : '',
        // noofdaystoprotectgoldrateforGRN: data.configDetails
        //   .noofdaystoprotectgoldrateforGRN
        //   ? data.configDetails.noofdaystoprotectgoldrateforGRN
        //   : '',
        // minimumUtilization: data.configDetails.minimumUtilization
        //   ? data.configDetails.minimumUtilization
        //   : '',
        // maxnoofdaysforphysicalreceiptdate: data.configDetails
        //   .maxnoofdaysforphysicalreceiptdate
        //   ? data.configDetails.maxnoofdaysforphysicalreceiptdate
        //   : '',
        // configurationamountforStuddedsplit: data.configDetails
        //   .configurationamountforStuddedsplit
        //   ? data.configurationamountforStuddedsplit
        //   : '',
        // maxnoofdaysforSTNcancellation: data.configDetails
        //   .maxnoofdaysforSTNcancellation
        //   ? data.configDetails.maxnoofdaysforSTNcancellation
        //   : '',
        // numbersofdays: data.configDetails.numbersofdays
        //   ? data.configDetails.numbersofdays
        //   : '',

        // numbersofdaysTodisplay: data.configDetails.numbersofdaysTodisplay
        //   ? data.configDetails.numbersofdaysTodisplay
        //   : '',
        // nooftimesrequestedincurrentmonth: data.configDetails
        //   .noofitemsrequestedincurrentmonth
        //   ? data.configDetails.noofitemsrequestedincurrentmonth
        //   : '',
        // totalvaluerequestedincurrentmonth: data.configDetails
        //   .totalvaluerequestedincurrentmonth
        //   ? data.configDetails.totalvaluerequestedincurrentmonth
        //   : '',
        // noofitemsrequestedincurrentmonth: data.configDetails
        //   .noofitemsrequestedincurrentmonth
        //   ? data.configDetails.noofitemsrequestedincurrentmonth
        //   : '',
        // loyalityCheckbox: data.configDetails.loyalityCheckbox
        //   ? data.configDetails.loyalityCheckbox
        //   : '',
        // GEPpuregoldpurity: data.configDetails.GEPPureGoldPurity
        //   ? data.configDetails.GEPPureGoldPurity
        //   : '',
        // GEPpuresilverpurity: data.configDetails.GEPpuresilverpurity
        //   ? data.configDetails
        //   : '',
        // GEPpureplatinumpurity: data.configDetails.GEPpureplatinumpurity
        //   ? data.configDetails.GEPpureplatinumpurity
        //   : '',
        // GEPStandaredDeductiongold: data.configDetails
        //   .GEPStandaredDeductiongold
        //   ? data.configDetails.GEPStandaredDeductiongold
        //   : '',
        // GEPStandaredDeductionsilver: data.configDetails
        //   .GEPStandaredDeductionsilver
        //   ? data.configDetails.GEPStandaredDeductionsilver
        //   : '',
        // GEPStandaredDeductionplatinum: data.configDetails
        //   .GEPStandaredDeductionplatinum
        //   ? data.configDetails.GEPStandaredDeductionplatinum
        //   : '',
        // noofdaysforFVTpassword: data.configDetails.noofdaysforFVTpassword
        //   ? data.configDetails.noofdaysforFVTpassword
        //   : '',
        // rtgsMaximumamount: data.configDetails.rtgsMaximumamount
        //   ? data.configDetails.rtgsMaximumamount
        //   : '',
        // rtgsMinimunamount: data.configDetails.rtgsMinimunamount
        //   ? data.configDetails.rtgsMinimunamount
        //   : '',
        // multiplevalues: data.configDetails.multiplevalues
        //   ? data.configDetails.multiplevalues
        //   : '',
        // advanceCustomCheckbox: data.configDetails.advanceCustomCheckbox
        //   ? data.configDetails.advanceCustomCheckbox
        //   : '',
        // maxWeightforFOC: data.configDetails.maxWeightforFOC
        //   ? data.configDetails.maxWeightforFOC
        //   : '',
        // maxValueforFOC: data.configDetails.maxValueforFOC
        //   ? data.configDetails.maxValueforFOC
        //   : '',
        // GEPPureGoldPurity: data.configDetails.GEPPureGoldPurity
        //   ? data.configDetails.GEPPureGoldPurity
        //   : '',
        // giftCardmaximumamount: data.configDetails.giftCardmaximumamount
        //   ? data.configDetails.giftCardmaximumamount
        //   : '',
        // giftCardsminimunamount: data.configDetails.giftCardsminimunamount
        //   ? data.configDetails.giftCardsminimunamount
        //   : '',
        // minOTPCNValue: data.configDetails.minOTPCNValue
        //   ? data.configDetails.minOTPCNValue
        //   : '',
        // maxnoofOFCN: data.configDetails.maxnoofOFCN
        //   ? data.configDetails.maxnoofOFCN
        //   : '',
        // maxnoofdaysforPOlikelydate: data.configDetails
        //   .maxnoofdaysforPOlikelydate
        //   ? data.configDetails.maxnoofdaysforPOlikelydate
        //   : '',
        // serviceTaxGSTRegistrationNumber: data.configDetails
        //   .serviceTaxGSTRegistrationNumber
        //   ? data.configDetails.serviceTaxGSTRegistrationNumber
        //   : ''
        //}
      };
    } else {
      locationBylocationCode = {
        locationCode: data.locationCode ? data.locationCode : '',
        description: data.description ? data.description : '',
        address: data.address ? data.address : '',
        pincode: data.pincode ? data.pincode : 0,
        phoneNo: data.phoneNo ? data.phoneNo : '',
        contactNo: data.contactNo ? data.contactNo : '',
        fax: data.fax ? data.fax : '',
        locationEmail: data.locationEmail ? data.locationEmail : '',
        locationTypeCode: data.locationTypeCode ? data.locationTypeCode : '',
        registrationNo: data.registrationNo ? data.registrationNo : '',
        currencyCode: data.currencyCode ? data.currencyCode : '',
        countryCode: data.countryCode ? data.countryCode : '',
        townCode: data.townCode ? data.townCode : '',
        stateCode: data.stateCode ? data.stateCode : '',
        regionCode: data.regionCode ? data.regionCode : '',
        oldFactoryCode: data.oldFactoryCode ? data.oldFactoryCode : '',
        ownerTypeCode: data.ownerTypeCode ? data.ownerTypeCode : '',
        factoryCodeValue: data.factoryCodeValue ? data.factoryCodeValue : '',
        locationFormat: data.locationFormat ? data.locationFormat : '',
        brandCode: data.brandCode ? data.brandCode : '',
        isActive: data.isActive ? data.isActive : '',
        marketCode: data.marketCode ? data.marketCode : '',
        configDetails: {
          locationStepOne: {},
          // locationStepTwo: {},
          // locationStepThree: {},
          ghsStepOne: {},
          ghsStepTwo: {},
          printStepOne: {},
          printStepTwo: {},
          grnStepOne: {},
          grnStepTwo: {},
          loyaltyStepOne: {},
          loyaltyStepTwo: {},
          loyaltyStepThree: {}
          // advanceStepOne: {},
          // advanceStepTwo: {}
        }
      };
    }

    return locationBylocationCode;
  }
  static getsavedLocationDetails(data: any): SaveLocationDetailsPayload {
    this.saveLocationDetails = {
      cfaCodeValue: data.locationCode,
      address: data.address,
      brandCode: data.brandCode,
      configDetails: data.configDetails,
      contactNo: data.contactNo,
      countryCode: data.countryCode,
      //currencyCode: data.currencyCode,
      description: data.description,
      factoryCodeValue: data.factoryCodeValue,
      //fax: data.fax,
      isActive: data.isActive,
      locationCode: data.locationCode,
      locationEmail: data.locationEmail,
      locationFormat: data.locationFormat,
      locationTypeCode: data.locationTypeCode,
      ownerTypeCode: data.ownerTypeCode,
      phoneNo: data.phoneNo,
      pincode: data.pincode,
      regionCode: data.regionCode,
      registrationNo: data.registrationNo,
      stateCode: data.stateCode,
      townCode: data.townCode
    };
    return this.saveLocationDetails;
  }

  static getLocationListingDetails(data: any): LocationListing {
    const locationListResult: LocationMaster[] = [];
    for (const listItem of data.results) {
      locationListResult.push({
        address: listItem.address,
        brandCode: listItem.brandCode,
        configDetails: listItem.configDetails,
        contactNo: listItem.contactNo,
        currencyCode: listItem.currencyCode,
        description: listItem.description,
        factoryCodeValue: listItem.factoryCodeValue,
        fax: listItem.fax,
        isActive: listItem.isActive,
        locationCode: listItem.locationCode,
        locationEmail: listItem.locationEmail,
        locationFormat: listItem.locationFormat,
        locationTypeCode: listItem.locationTypeCode,
        oldFactoryCode: listItem.oldFactoryCode,
        ownerTypeCode: listItem.ownerTypeCode,
        phoneNo: listItem.phoneNo,
        pincode: listItem.pincode,
        regionCode: listItem.regionCode,
        registrationNo: listItem.registrationNo,
        stateCode: listItem.stateCode,
        townCode: listItem.townCode
      });
    }
    this.locationList = {
      results: locationListResult,
      totalElements: data.totalElements
    };

    return this.locationList;
  }

  static getSearchResult(data: any): LocationListing {
    let locationList: LocationListing;
    const locationListResult: LocationMaster[] = [];
    const listItem = data;
    locationListResult.push({
      address: listItem.address,
      brandCode: listItem.brandCode,
      configDetails: listItem.configDetails,
      contactNo: listItem.contactNo,
      currencyCode: listItem.currencyCode,
      description: listItem.description,
      factoryCodeValue: listItem.factoryCodeValue,
      fax: listItem.fax,
      isActive: listItem.isActive,
      locationCode: listItem.locationCode,
      locationEmail: listItem.locationEmail,
      locationFormat: listItem.locationFormat,
      locationTypeCode: listItem.locationTypeCode,
      oldFactoryCode: listItem.oldFactoryCode,
      ownerTypeCode: listItem.ownerTypeCode,
      phoneNo: listItem.phoneNo,
      pincode: listItem.pincode,
      regionCode: listItem.regionCode,
      registrationNo: listItem.registrationNo,
      stateCode: listItem.stateCode,
      townCode: listItem.townCode
    });
    let totalElements;
    if (data) {
      totalElements = 1;
    } else {
      totalElements = 0;
    }
    locationList = {
      results: locationListResult,
      totalElements: totalElements
    };

    return locationList;
  }

  static getOnlyLocationDetails(data: any): Location {
    if (data === undefined) {
      data = null;
    }
    let locationBylocationCode = null;

    if (data) {
      locationBylocationCode = {
        locationCode: data.locationCode ? data.locationCode : '',
        description: data.description ? data.description : '',
        address: data.address ? data.address : '',
        pincode: data.pincode ? data.pincode : '',
        phoneNo: data.phoneNo ? data.phoneNo : '',
        contactNo: data.contactNo ? data.contactNo : '',
        fax: data.fax ? data.fax : '',
        locationEmail: data.locationEmail ? data.locationEmail : '',
        locationTypeCode: data.locationTypeCode ? data.locationTypeCode : '',
        registrationNo: data.registrationNo ? data.registrationNo : '',
        countryCode: data.countryCode ? data.countryCode : '',
        townCode: data.townCode ? data.townCode : '',
        stateCode: data.stateCode ? data.stateCode : '',
        regionCode: data.regionCode ? data.regionCode : '',
        ownerTypeCode: data.ownerTypeCode ? data.ownerTypeCode : '',
        factoryCodeValue: data.factoryCodeValue ? data.factoryCodeValue : '',
        locationFormat: data.locationFormat ? data.locationFormat : '',
        brandCode: data.brandCode ? data.brandCode : '',
        configDetails: data.configDetails ? data.configDetails : {},
        currencyCode: data.currencyCode ? data.currencyCode : '',
        isActive: data.isActive ? data.isActive : ''
      };
    }

    return locationBylocationCode;
  }

  static getOnlyGHSDetails(data: any): Ghs {
    if (data === undefined) {
      data = null;
    }

    let dataset = null;
    if (data) {
      const configDetails = data.configDetails ? data.configDetails : {};
      const ghsStepOne = configDetails.ghsStepOne
        ? configDetails.ghsStepOne
        : {};

      const dayDetails = ghsStepOne.dayDetails ? ghsStepOne.dayDetails : {};

      const ghsStepTwo = configDetails.ghsStepTwo
        ? configDetails.ghsStepTwo
        : {};

      dataset = {
        baseCurrency: dayDetails.baseCurrency ? dayDetails.baseCurrency : '',
        suspendingCNs: dayDetails.suspendingCNs ? dayDetails.suspendingCNs : '',
        transferredCNs: dayDetails.transferredCNs
          ? dayDetails.transferredCNs
          : '',
        activatedCNs: dayDetails.activatedCNs ? dayDetails.activatedCNs : '',
        ddValidityDays: dayDetails.ddValidityDays
          ? dayDetails.ddValidityDays
          : '',
        consolidateAttempts: dayDetails.consolidateAttempts
          ? dayDetails.consolidateAttempts
          : '',

        realisationDays: dayDetails.realisationDays
          ? dayDetails.realisationDays
          : '',
        validityDays: dayDetails.validityDays ? dayDetails.validityDays : '',
        ghsIbtCheckBox: ghsStepTwo.ghsIbtCheckBox
          ? ghsStepTwo.ghsIbtCheckBox
          : {}
      };
      return dataset;
    }
  }

  static getOnlyPrintDetails(data: any): Print {
    if (data === undefined) {
      data = null;
    }
    let printDataset = null;
    if (data) {
      const configDetails = data.configDetails ? data.configDetails : {};
      const printStepOne = configDetails.printStepOne
        ? configDetails.printStepOne
        : {};

      const check1 = printStepOne.checkItOut1 ? printStepOne.checkItOut1 : {};
      const check2 = printStepOne.checkItOut2 ? printStepOne.checkItOut2 : {};

      printDataset = {
        printCheckbox1: check1 ? check1 : {},

        printCheckbox2: check2 ? check2 : {},

        makingChargesorWastageHeading: printStepOne.makingWastageCharge
          ? printStepOne.makingWastageCharge
          : '',

        freeTextForGrams: check2.freeTextForGrams
          ? check2.freeTextForGrams
          : '',
        noOfInvoicecopiesforRegularOrQuickCM: check2.noOfInvoicecopiesforRegularOrQuickCM
          ? check2.noOfInvoicecopiesforRegularOrQuickCM
          : ''
      };
      return printDataset;
    }
  }

  static getOnlyGRNIBTDetails(data: any): GRNIBTInventory {
    if (data === undefined) data = null;
    let GRNIBTDataset = null;
    if (data) {
      const configDetails = data.configDetails ? data.configDetails : {};
      const grnIBTInventoryStepOne = configDetails.grnIBTInventoryStepOne
        ? configDetails.grnIBTInventoryStepOne
        : {};
      const grnIBTInventoryStepTwo = configDetails.grnIBTInventoryStepTwo
        ? configDetails.grnIBTInventoryStepTwo
        : {};
      const grnGrfConfiguration = grnIBTInventoryStepOne.grnGrfConfiguration
        ? grnIBTInventoryStepOne.grnGrfConfiguration
        : {};
      const inventory = grnIBTInventoryStepOne.inventory
        ? grnIBTInventoryStepOne.inventory
        : {};

      GRNIBTDataset = {
        grnGrfConfiguration: grnGrfConfiguration,
        grnGrfCheckBoxes: grnGrfConfiguration.grnGrfCheckBoxes
          ? grnGrfConfiguration.grnGrfCheckBoxes
          : {},
        inventory: inventory,
        inventoryCheckBoxes: inventory.inventoryCheckBoxes
          ? inventory.inventoryCheckBoxes
          : {},
        walkins: grnIBTInventoryStepOne.walkins
          ? grnIBTInventoryStepOne.walkins
          : {},
        ibtConfiguration: grnIBTInventoryStepTwo.ibtConfiguration
          ? grnIBTInventoryStepTwo.ibtConfiguration
          : {},
        kycConfiguration: grnIBTInventoryStepTwo.kycConfiguration
          ? grnIBTInventoryStepTwo.kycConfiguration
          : {},
        ulpConfiguration: grnIBTInventoryStepTwo.ulpConfiguration
          ? grnIBTInventoryStepTwo.ulpConfiguration
          : {}
      };

      return GRNIBTDataset;
    }
  }

  static getOnlyLoyalityDetails(data: any): LoyalityGEPGCTEP {
    if (data === undefined) data = null;

    let loyalityDataset = null;
    if (data) {
      const configDetails = data.configDetails ? data.configDetails : {};
      const loyalityStepOne = configDetails.loyalityStepOne
        ? data.configDetails.loyalityStepOne
        : {};
      const loyalityStepTwo = configDetails.loyalityStepTwo
        ? data.configDetails.loyalityStepTwo
        : {};
      const loyalityStepThree = configDetails.loyalityStepThree
        ? data.configDetails.loyalityStepThree
        : {};
      const giftCardConfiguration = loyalityStepTwo.giftCardConfiguration
        ? loyalityStepTwo.giftCardConfiguration
        : {};
      loyalityDataset = {
        loyality: loyalityStepOne.loyality ? loyalityStepOne.loyality : {},
        gvPayment: loyalityStepOne.gvPayment ? loyalityStepOne.gvPayment : {},
        personalDetails: loyalityStepOne.personalDetails
          ? loyalityStepOne.personalDetails
          : {},
        ccPayment: loyalityStepTwo.ccPayment ? loyalityStepTwo.ccPayment : {},
        employeeDiscount: loyalityStepTwo.employeeDiscount
          ? loyalityStepTwo.employeeDiscount
          : {},
        giftCardConfiguration: giftCardConfiguration,
        giftCardConfigurationCheckBoxes: giftCardConfiguration.giftCardConfigurationCheckBoxes
          ? giftCardConfiguration.giftCardConfigurationCheckBoxes
          : '',
        tep: loyalityStepThree.tep ? loyalityStepThree.tep : {}
      };
      return loyalityDataset;
    }
  }
}
