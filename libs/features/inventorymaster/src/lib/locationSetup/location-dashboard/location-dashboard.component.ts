import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { getInventoryMasterRouteUrl, getLocationMasterListRouteUrl, getCorporateTownListRouteUrl } from '../../page-route.constants';

@Component({
  selector: 'poss-web-location-dashboard',
  templateUrl: './location-dashboard.component.html',
  styleUrls: ['./location-dashboard.component.scss']
})
export class LocationDashboardComponent implements OnInit {

  locationListUrl = getLocationMasterListRouteUrl();
  corporateTownListUrl = getCorporateTownListRouteUrl();
  constructor(private router: Router) { }

  ngOnInit() {
  }

  backArrow() {
    this.router.navigate([getInventoryMasterRouteUrl()]);
  }

}
