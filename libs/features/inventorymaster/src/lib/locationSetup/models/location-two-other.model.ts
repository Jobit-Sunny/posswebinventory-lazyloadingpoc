import { Validators } from '@angular/forms';
import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';

export class OtherModel extends DynamicFormFieldsBuilder {
  private id: number;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.regdOffice'
  })
  private regdOffice: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.CINNumber',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:  'pw.inventoryMasterValidation.CINNumber_pattern' }]
  })
  @Validation({ validators: [Validators.required,Validators.pattern('^[0-9A-Za-z]*$')] })
  private CINNumber: string;



  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.corporateAddress'
  })
  @Validation({ validators: [Validators.required] })
  private corporateAddress: string;

  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'selected'
    },
    label: 'pw.locationMaster.marketCode'
  })
  @Validation({ validators: [Validators.required] })
  private marketCode: { id: string; name: string }[];

  // @FormField({
  //   fieldType: FormFieldType.SELECT,
  //   selectOptionKeys: { labelKey: 'name', valueKey: 'id', selectedKey: 'selected' }, label: 'payment Mode For Refund  '
  // })
  // @Validation({ validators: [Validators.required] })
  // private paymentModeForRefund: string;

  constructor(
    id: number,
    regdOffice: string,
    CINNumber: string,
    corporateAddress: string,
    marketCode: { id: string; name: string }[]
  ) {
    super();
    this.id = id;
    this.regdOffice = regdOffice;
    this.CINNumber = CINNumber;
    this.corporateAddress = corporateAddress;
    this.marketCode = marketCode;
  }
}
