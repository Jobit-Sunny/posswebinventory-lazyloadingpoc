import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class LocationPricegroupmapping extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.priceGroupTypeCode',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:  'pw.inventoryMasterValidation.validitydaysforReleaseInvInCustomerOrder_pattern'}]
  })
  @Validation({ validators: [Validators.required,Validators.pattern('^[+]?[0-9]*$')] })
  private priceGroupTypeCode: string;

  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'selected'
    },
    label: 'pw.locationMaster.priceGroupCode',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.validitydaysforReleaseInvInCustomerOrder_pattern' }]
  })
  private priceGroupCode: { id: string; name: string; selected?: boolean }[];


  constructor(id: number, priceGroupTypeCode: string, priceGroupCode: { id: string; name: string; selected?: boolean }[]) {
    super();
    this.id = id;
    this.priceGroupTypeCode = priceGroupTypeCode;
    this.priceGroupCode = priceGroupCode;
  }

}
