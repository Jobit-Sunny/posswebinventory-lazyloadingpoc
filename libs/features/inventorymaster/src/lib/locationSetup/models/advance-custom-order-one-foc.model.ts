import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class Foc extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Class({ className: ['row'] })
  private advanceCustomOrderTabOneFoccheckBoxes: {
    id: string;
    name: string;
    checked?: boolean;
  }[];


  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.maxWeightforFOC(gms)',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.maxWeightforFOC_pattern' }]
  })

  @Validation({
    validators: [
      Validators.required,
      Validators.pattern('[+]?([0-9]*[.])?[0-9]+')
    ]
  })
  private maxWeightforFOC: string;


  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.maxValueforFOC(gms)',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.maxValueforFOC_pattern' }]
  })
  @Validation({
    validators: [
      Validators.required,
      Validators.pattern('[+]?([0-9]*[.])?[0-9]+')
    ]
  })
  private maxValueforFOC: string;

  constructor(
    id: number,
    checkBoxes: { id: string; name: string; checked?: boolean }[],
    maxWeightforFOC: string,
    maxValueforFOC: string
  ) {
    super();
    this.id = id;
    this.advanceCustomOrderTabOneFoccheckBoxes = checkBoxes;
    this.maxWeightforFOC = maxWeightforFOC;
    this.maxValueforFOC = maxValueforFOC;
  }
}
