import { Validators } from '@angular/forms';
import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';

export class LocationModel extends DynamicFormFieldsBuilder {
  private id: number;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.locationCode'
  })
  @Validation({ validators: [Validators.required] })
  private locationCode: string;

  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'selected'
    },
    label: 'pw.locationMaster.locationType'
  })
  @Validation({ validators: [Validators.required] })
  private locationType: { id: string; name: string }[];

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.factoryCode'
  })
  @Validation({ validators: [Validators.required] })
  private factoryCode: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.locationShortName'
  })
  @Validation({ validators: [Validators.required] })
  private locationShortName: string;

  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'selected'
    },
    label: 'pw.locationMaster.brandName'
  })
  @Validation({ validators: [Validators.required] })
  private brandName: { id: string; name: string; selected?: boolean }[];

  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      foreignKey: 'brandName_id',
      selectedKey: 'selected'
    },
    label: 'pw.locationMaster.subBrandName',
    dependsOn: '1-brandName'
  })
  @Validation({ validators: [Validators.required] })
  private subBrandCode: { id: string; name: string; selected?: boolean }[];

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.registrationNo',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.registrationNo_pattern' }]
  })
  @Validation({ validators: [Validators.pattern('^[0-9]{15}$')] })
  private registrationNo: string;

  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'selected'
    },
    label: 'pw.locationMaster.locationFormat'
  })
  @Validation({ validators: [Validators.required] })
  private locationFormat: { id: string; name: string; selected?: boolean }[];

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.description'
  })
  @Validation({ validators: [Validators.required] })
  private description: string;

  @Validation({ validators: [Validators.required] })
  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'selected'
    },
    label: 'pw.locationMaster.ownerinfo'
  })
  private ownerInfo: { id: string; name: string; selected?: boolean }[];
  constructor(
    id: number,
    locationCode: string,
    locationType: { id: string; name: string; selected?: boolean }[],
    locationShortName: string,
    brandName: { id: string; name: string; selected?: boolean }[],
    subBrandCode: { id: string; name: string; selected?: boolean }[],
    factoryCode: string,
    registrationNo: string,
    ownerInfo: { id: string; name: string; selected?: boolean }[],
    locationFormat: { id: string; name: string; selected?: boolean }[],
    descrption: string
  ) {
    super();
    this.id = id;
    this.locationCode = locationCode;
    this.locationType = locationType;
    this.locationShortName = locationShortName;
    this.brandName = brandName;
    this.subBrandCode = subBrandCode;
    this.factoryCode = factoryCode;
    this.registrationNo = registrationNo;
    this.ownerInfo = ownerInfo;
    this.locationFormat = locationFormat;
    this.description = descrption;
  }
}
