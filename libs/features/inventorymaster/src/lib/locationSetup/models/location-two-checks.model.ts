import { Validators } from '@angular/forms';
import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';

export class ChecksModel extends DynamicFormFieldsBuilder {
  private id: number;

  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked',
      foreignKey: 'foreignKey'
    },
    label: ''
  })
  @Class({ className: ['row'] })
  private locationcheckBoxes: { id: string; name: string; checked?: boolean }[];

  constructor(
    id: number,
    locationcheckBoxes: {
      id: string;
      name: string;
      checked?: boolean;
      foreignKey?: string;
    }[]
  ) {
    super();
    this.id = id;
    this.locationcheckBoxes = locationcheckBoxes;
  }
}
