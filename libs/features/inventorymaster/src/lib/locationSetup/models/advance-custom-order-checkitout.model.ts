import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class CheckItOut extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Class({ className: ['row'] })
  private advanceCustomOrderTabTwocheckBoxes: {
    id: string;
    name: string;
    checked?: boolean;
  }[];
  constructor(
    id: number,
    checkbox: { id: string; name: string; checked?: boolean }[]
  ) {
    super();
    this.id = id;
    this.advanceCustomOrderTabTwocheckBoxes = checkbox;
  }
}
