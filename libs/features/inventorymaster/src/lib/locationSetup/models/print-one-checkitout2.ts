import { DynamicFormFieldsBuilder, FormField, FormFieldType, Validation, Class } from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class Checks2Model extends DynamicFormFieldsBuilder {

  private id: number;

  @FormField({ fieldType: FormFieldType.CHECKBOX, selectOptionKeys: { labelKey: 'name', valueKey: 'id', selectedKey: 'checked' }, label: '' })
  @Class({ className: ['row'] })
  private checkBoxes: { id: string, name: string, checked?: boolean }[];



  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.freeTextForGrams'
  })
  private freeTextForGrams: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.noOfInvoiceCopies',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.invoiceDigits' }]
  })
  @Validation({ validators: [Validators.pattern('^[0-9]*$')] })
  private noOfInvoicecopiesforRegularOrQuickCM: number;


  constructor(
    id: number,
    checkBoxes: { id: string, name: string, checked?: boolean }[],
    freeTextForGrams: string,
    noOfInvoicecopiesforRegularOrQuickCM: number
  ) {
    super();
    this.id = id;
    this.checkBoxes = checkBoxes;
    this.freeTextForGrams = freeTextForGrams;
    this.noOfInvoicecopiesforRegularOrQuickCM = noOfInvoicecopiesforRegularOrQuickCM;
  }
}
