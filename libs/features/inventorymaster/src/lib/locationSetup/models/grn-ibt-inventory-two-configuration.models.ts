import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class IBTConfiguration extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.noOfTimesRequestedInCurrentMonth',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.noOFDaysGRNAllowed_pattern' }]
  })
  @Validation({
    validators: [Validators.pattern('^[0-9]*')]
  })
  private noOfTimesRequestedInCurrentMonth: string;

  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.totalValueRequestedInCurrentMonth',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.noOFDaysGRNAllowed_pattern' }]
  })
  @Validation({
    validators: [Validators.pattern('[+]?([0-9]*[.])?[0-9]+')]
  })
  private totalValueRequestedInCurrentMonth: string;

  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.noOfItemsRequestedInCurrentMonth',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.noOFDaysGRNAllowed_pattern' }]
  })
  @Validation({
    validators: [Validators.pattern('[+]?[0-9]*')]
  })
  private noOfItemsRequestedInCurrentMonth: string;
  constructor(
    id: number,
    noOfTimesRequestedInCurrentMonth: string,
    totalValueRequestedInCurrentMonth: string,
    noOfItemsRequestedInCurrentMonth: string
  ) {
    super();
    this.id = id;
    this.noOfTimesRequestedInCurrentMonth = noOfTimesRequestedInCurrentMonth;
    this.totalValueRequestedInCurrentMonth = totalValueRequestedInCurrentMonth;
    this.noOfItemsRequestedInCurrentMonth = noOfItemsRequestedInCurrentMonth;
  }
}
