//locationStepOne
export const locationStepOneFirstSubForm = '1-location';
export const locationStepOneSecondSubForm = '1-personal';
export const locationCode = '1-locationCode';
export const locationType = '1-locationType';

export const currencyCode = '1-currencyCode';
export const description = '1-description';

export const locationFormat =
  '1-locationFormat';
export const factoryCodeValue =
  '1-factoryCode';
export const registrationNo = '1-registrationNo';
export const pinCode = '1-pinCode';

export const loactionNamefeild = '1-name';
export const addressTwo = '1-addressTwo';
export const locationShortName =
  '1-locationShortName';
export const phoneNumberTwo =
  '1-phoneNumberTwo';
export const brandName = '1-brandName';
export const subBrandName = '1-subBrandCode';
export const subbrandCode = '1-subbrandCode';

export const brandCode = '1-brandName';
export const brand = '1-brandCode';
export const addressOne = '1-addressOne';
export const descrption = '1-descrption';

export const phoneNumberOne =
  '1-phoneNumberOne';
export const mobileNumber =
  '1-mobileNumber';
  export const country =
  '1-country';

export const state = '1-state';
export const cityTown = '1-cityTown';
export const oldFactoryCode = '1-oldFactoryCode';

export const personalCityTown =
  '1-personalCityTown';

export const gender = '1-gender';
export const emailId = '1-emailId';
export const ownerInfo = '1-ownerInfo';
export const regionCode = '1-regionCode';
export const subRegionCode = '1-subRegionCode'

//locationStepTwo
export const locationStepTwoFirstSubForm = '1-other';
export const locationStepTwoFirstSubFormFeildregdOffice = '1-regdOffice';
export const locationStepTwoFirstSubFormFeildlocationSize = '1-locationSize';
export const locationStepTwoFirstSubFormFeildcorporateAddress =
  '1-corporateAddress';
export const locationStepTwoFirstSubFormFeildCINNumber = '1-CINNumber';
export const locationStepTwoFirstSubFormFeildmarketCode = '1-marketCode'



//locationStepThree
export const locationStepThreeFirstSubForm = '1-checks';
export const locationStepThreeFirstSubFormFeildregdOffice =
  '1-locationcheckBoxes';

export const locationStepThreeSecondSubForm = '1-remarks';
export const locationStepThreeSecondSubFormremarks = '1-remarks';
export const locationStepThreeSecondSubFormSAPcode = '1-SAPCode';
export const locationStepThreeSecondSubFormpaymentModeForRefund =
  '1-paymentModeForRefund';
  export const locationcheckBoxes='1-locationcheckBoxes'

//loyality.........................................................................
export const loyalityStepOneFirstSubForm = '1-loyality'; //sub-form

export const loyalityStepOneFirstSubFormFieldCheckBoxes = '1-checkBoxes';

export const loyalityStepOneSecondSubForm = '1-personalDetails'; //sub-form

export const loyalityStepOneSecondSubFormFieldGEPPureGoldPurity =
  '1-GEPPureGoldPurity';
export const loyalityStepOneSecondSubFormFieldGEPPureSilverPurity =
  '1-GEPPureSilverPurity';
export const loyalityStepOneSecondSubFormFieldGEPPurePlatinumPurity =
  '1-GEPPurePlatinumPurity';
export const loyalityStepOneSecondSubFormFieldGEPStandaredDeductionGold =
  '1-GEPStandaredDeductionGold';
export const loyalityStepOneSecondSubFormFieldGEPStandaredDeductionSilver =
  '1-GEPStandaredDeductionSilver';
export const loyalityStepOneSecondSubFormFieldGEPStandaredDeductionPlatinum =
  '1-GEPStandaredDeductionPlatinum';
export const loyalityStepOneSecondSubFormFieldEnableGEPSale = '1-enableGEPSale';

export const loyalityStepOneThirdSubForm = '1-gvPayment'; //sub-form

export const loyalityStepOneThirdSubFormFieldCheckBoxes = '1-checkBoxes';

// export const loyalityStepTwoFirstSubForm = '1-ccpayment';
// export const loyalityStepTwoSecondSubForm = '1-employeeDiscount';
// export const loyalityStepTwoThirdSubForm = '1-giftCardConfiguration';



//advance custom order step Two
export const advanceCustomOrderStepTwoFirtSubForm = '1-foc';
export const advanceCustomOrderStepTwoFirtSubFormFeild1advanceCustomOrderTabTwoFoccheckBoxes =
  '1-advanceCustomOrderTabOneFoccheckBoxes'
// export const locationStepThreeFirstSubFormFeildisTEPsaleableitemsallowedforFOC =
//   '1-isTEPsaleableitemsallowedforFOC';
// export const locationStepThreeFirstSubFormFeildisTEPallowedforFOCitems =
//   '1-isTEPallowedforFOCitems';
// export const locationStepThreeFirstSubFormFeildisFOCitemssaleable =
//   '1-isFOCitemssaleable';
export const advanceCustomOrderStepTwoFirtSubFormFeildmaxWeightforFOC =
  '1-maxWeightforFOC';
export const advanceCustomOrderStepTwoFirtSubFormFeildmaxValueforFOC =
  '1-maxValueforFOC';
export const advanceCustomOrderStepTwoFirtSubFormFeildOTPHelpdeskEmailId = '1-OTPHelpdeskEmailId'
export const advanceCustomOrderStepTwoSecondSubForm = '1-rtgs';
export const advanceCustomOrderStepTwoSecondSubFormFeildMinimumAmount = '1-minimumamount'

export const advanceCustomOrderStepTwoFirtSubFormFeildmaximumamount =
  '1-maximumamount';
export const advanceCustomOrderStepTwoFirtSubFormFeildminimunamount =
  '1-minimunamount';
export const advanceCustomOrderStepTwoFirtSubFormFeildminOTPCNValue =
  '1-minOTPCNValue';
export const advanceCustomOrderStepTwoFirtSubFormFeildmaxNoofCN =
  '1-maxNoofCN';
export const advanceCustomOrderStepTwoFirtSubFormFeildGEPPureGoldPurity =
  '1-GEPPureGoldPurity';

export const locationStepTwoFirstSubFormFeildadvanceCustomOrderTabTwoRtgscheckBoxes =
  '1-advanceCustomOrderTabOneRtgscheckBoxes';
// export const locationStepThreeFirstSubFormFeildisRRnumbervalidationrequired =
//   '1-isRRnumbervalidationrequired';

//advance custom order step three
export const advanceCustomOrderStepThreeFirtSubForm = '1-RTGSStepTwo';
export const advanceCustomOrderStepTwoFirtSubFormFeildMaxNumberOfDaysForPOlikelyDate = '1-maxnoofdaysforPOlikelydate';
export const advanceCustomOrderStepTwoFirtSubFormFeildServiceTaxGSTRegistrationno = '1-serviceTaxGSTRegistrationNumber'



export const advanceCustomOrderStepThreeSubForm = '1-checkItOut';
export const advanceCustomOrderStepThreeSubFormFeildadvanceCustomOrderTabTwocheckBoxes = '1-advanceCustomOrderTabTwocheckBoxes';
export const advanceCustomOrderStepThreeSubFormFeildisOTPallowedCM = '1-isOTPallowedCM';
export const advanceCustomOrderStepThreeSubFormFeildisOTPallowedAdvance = '1-isOTPallowedAdvance';
export const advanceCustomOrderStepThreeSubFormFeildisOTPallowedAB = '1-isOTPallowedAB';
export const advanceCustomOrderStepThreeSubFormFeildisOTPallowedGHS = '1-isOTPallowedGHS';
export const advanceCustomOrderStepThreeSubFormFeildisOTPallowedCO = '1-isOTPallowedCO';
export const advanceCustomOrderStepThreeSubFormFeildisOTPrequiredforGC = '1-isOTPrequiredforGC';

export const advanceCustomOrderStepThreeSubFormtwo = '1-configurePaymentMode'
export const advanceCustomOrderStepThreeSubFormThree = '1-locationPricegroupmapping'
export const advanceCustomOrderStepThreeSubFormFeildconfigurePaymentModesCheckBoxes = '1-configurePaymentModeCheckBoxes'
export const advanceCustomOrderStepThreeSubFormtwoFeildpaymentMode = '1-paymentCode'
export const advanceCustomOrderStepThreeSubFormThreeFeildpriceGroupTypeCode = '1-priceGroupTypeCode'
export const advanceCustomOrderStepThreeFirtSubFormFeildpriceGroupCode = '1-priceGroupCode'


//advance custom order step one
export const advanceCustomOrderStepOneSubForm = '1-advanceCustomOrderConfigurationCheckBox';
export const advanceCustomOrderStepOneSubFormCheckBox = '1-advanceCustomOrderStpeThreecheckBoxes';
export const advanceCustomOrderStepOneSubFormFeildvalidityDaysforAutoClosureInAdvanceBooking = '1-validityDaysforAutoClosureInAdvanceBooking'
export const advanceCustomOrderStepOneSubFormFeildvalidityDaysforActivateInAdvanceBooking = '1-validityDaysforActivateInAdvanceBooking';
export const advanceCustomOrderStepOneSubFormFeildvalidityDaysforReleaseInvInAdvancebooking = '1-validityDaysforReleaseInvInAdvancebooking';
export const advanceCustomOrderStepOneSubFormFeildvalidityDaysforActivateInCustomerOrder = '1-validityDaysforActivateInCustomerOrder';
export const advanceCustomOrderStepOneSubFormFeildvaliditydaysforReleaseInvInCustomerOrder = '1-validitydaysforReleaseInvInCustomerOrder';
export const advanceCustomOrderStepOneSubFormFeildgoldRateAttempts = '1-goldRateAttempts';
export const advanceCustomOrderStepOneSubFormFeildmanualBillWeightDeviation = '1-manualBillWeightDeviation';
export const advanceCustomOrderStepOneSubFormFeildsparewtToleranceforStockItem = '1-sparewtToleranceforStockItem';
export const advanceCustomOrderStepOneSubFormFeildservicewtToleranceforStockItem = '1-servicewtToleranceforStockItem';

export const advanceCustomOrderStepOneSubFormFeildvalidityDaysforAutoClosureInCustomerOrder = '1-validityDaysforAutoClosureInCustomerOrder';
export const advanceCustomOrderStepOneSubFormFeildadvanceCustomOrderStepThreeSubForm = '1-advanceCustomOrderConfiguration';



// export const loyalityStepTwoFirstSubForm = '1-ccpayment';
// export const loyalityStepTwoSecondSubForm = '1-employeeDiscount';
// export const loyalityStepTwoThirdSubForm = '1-giftCardConfiguration';


//ghs
export const ghsStepOneFirstSubForm = '1-dayDetails1';
export const ghsStepOneSecondSubForm = '1-dayDetails2';
export const ghsStepOneFirstSubFormFeildMarketCode = '1-marketCode';
export const ghsStepOneFirstSubFormFeildBaseCurrency = '1-baseCurrency';
export const ghsStepOneFirstSubFormFeildSuspendingCNs = '1-suspendingCNs';
export const ghsStepOneFirstSubFormFeildTransferredCNs = '1-transferredCNs';
export const ghsStepOneFirstSubFormFeildActivatedCNs = '1-activatedCNs';

export const ghsStepOneSecondSubFormDDValidityDays = '1-ddValidityDays';
export const ghsStepOneSecondSubFormConsolidateAttempts =
  '1-consolidatedAttempts';
export const ghsStepOneSecondSubFormReversalDays = '1-reversalDays';
export const ghsStepOneSecondSubFormRealisationDays = '1-realisationDays';
export const ghsStepOneSecondSubFormValidityDays = '1-validityDays';

export const loyalityStepTwoFirstSubForm = '1-ccpayment'; //sub-form

export const loyalityStepTwoFirstSubFormFieldCheckBoxes = '1-checkBoxes';

export const loyalityStepTwoSecondSubForm = '1-employeeDiscount'; //sub-form

export const loyalityStepTwoSecondSubFormFieldCheckBoxes = '1-checkBoxes';

export const loyalityStepTwoThirdSubForm = '1-giftCardConfiguration'; //sub-form

export const loyalityStepTwoThirdSubFormFieldsMaximumAmount = '1-maximumAmount';
export const loyalityStepTwoThirdSubFormFieldsMimimumAmount = '1-minimumAmount';
export const loyalityStepTwoThirdSubFormFieldsmultiplesValues =
  '1-multiplesValues';
export const loyalityStepTwoThirdSubFormFieldsCheckBoxes = '1-checkBoxes';

export const loyalityStepThreeFirstSubForm = '1-tep'; //sub-form

export const loyalityStepThreeFirstSubFormFieldsNoOfDaysForFVTPassword =
  '1-noOfDaysForFVTPassword';
export const loyalityStepThreeFirstSubFormFieldsEnableRTGSRefund =
  '1-enableRTGSRefund';

//IBT.......................................................................
export const GRNIBTInventoryStepOneFirstSubForm = '1-GRNConfiguration'; //sub-form

export const GRNIBTInventoryStepOneFirstSubFormFieldNoOfDaysGRNAllowed =
  '1-noOfDaysGRNAllowed';
export const GRNIBTInventoryStepOneFirstSubFormFieldMaximumNoOfDaysForApprovedGRN =
  '1-maximumNoOfDaysForApprovedGRN';
export const GRNIBTInventoryStepOneFirstSubFormFieldNoOfDaySToProtectGoldRateForGRN =
  '1-noOfDaySToProtectGoldRateForGRN';
export const GRNIBTInventoryStepOneFirstSubFormFieldMinimumUtilization =
  '1-minimumUtilization';
export const GRNIBTInventoryStepOneFirstSubFormFieldinterBoutiqueGRNAllowed =
  '1-isInterBoutiqueGRNAllowed';

export const GRNIBTInventoryStepOneSecondSubForm = '1-inventory'; //sub-form

export const GRNIBTInventoryStepOneSecondSubFormFieldMaxNoOfDaysForPhysicalReceiptDate =
  '1-maxNoOfDaysForPhysicalReceiptDate';
export const GRNIBTInventoryStepOneSecondSubFormFieldConfigurationAmountForStuddedSplit =
  '1-configurationAmountForStuddedSplit';
export const GRNIBTInventoryStepOneSecondSubFormFieldMaxNoOfDaysForSTNCancellation =
  '1-maxNoOfDaysForSTNCancellation';

export const GRNIBTInventoryStepOneThirdSubForm = '1-walkIns'; //sub-form

export const GRNIBTInventoryStepOneThirdSubFormFieldNumbersOfDays =
  '1-numbersOfDays';

export const GRNIBTInventoryStepOneThirdSubFormFieldNumbersOfDatesToDisplay =
  '1-numbersOfDatesToDisplay';

export const GRNIBTInventoryStepTwoFirstSubForm = '1-IBTConfiguration'; //sub-form

export const GRNIBTInventoryStepTwoFirstSubFormFiledNoOfTimesRequestedInCurrentMonth =
  '1-noOfTimesRequestedInCurrentMonth';
export const GRNIBTInventoryStepTwoFirstSubFormFiledTotalValueRequestedInCurrentMonth =
  '1-totalValueRequestedInCurrentMonth';
export const GRNIBTInventoryStepTwoFirstSubFormFiledNoOfItemsRequestedInCurrentMonth =
  '1-noOfItemsRequestedInCurrentMonth';

export const GRNIBTInventoryStepTwoSecondSubForm = '1-inventory'; //sub-form

export const GRNIBTInventoryStepTwoSecondSubFormFiledCheckBoxes =
  '1-checkBoxes';

export const GRNIBTInventoryStepTwoThirdSubForm = '1-KYCConfiguration'; //sub-form

export const GRNIBTInventoryStepTwoThirdSubFormFiledCheckBoxes = '1-checkBoxes';

export const GRNIBTInventoryStepTwoForthSubForm = '1-ULPConfiguration'; //sub-form

export const GRNIBTInventoryStepTwoForthSubFormFiledCheckBoxes = '1-checkBoxes';

export const ghsStepTwoFirstSubForm = '1-checkBoxes';

export const printStepOneFirstSubForm = '1-checks1';
export const printStepOneFirstSubFormFiled = '1-checkBoxes';
export const printStepOneFirstSubFormMakingWastage = '1-makingChargesorWastageHeading'

export const printStepOneSecondSubForm = '1-check2';
export const printStepOneSecondSubFormFiled = '1-checkBoxes';

export const printStepTwoForm = '1-inputDetails';
export const printStepTwoFormFieldCINNumber = '1-CINNumber';
export const printStepTwoFormFieldCorporateAddress = '1-CorporateAddress';
export const printStepTwoFormFieldfreeTextForGrams = '1-freeTextForGrams';
export const printStepTwoFormFieldnoOfInvoicecopies = '1-noOfInvoicecopiesforRegularOrQuickCM';

export const locationTypeCode_FAC = 'FAC';
export const locationTypeCode_ENTP = 'ENTP';
export const locationTypeCode_VENDOR = 'VENDOR';
export const locationTypeCode_BTQ = 'BTQ';
export const locationTypeCode_REGOFF = 'REGOFF';
export const locationTypeCode_CFA = 'CFA';
