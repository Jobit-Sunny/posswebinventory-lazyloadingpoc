import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Class
} from '@poss-web/shared';
import { ChecksModel } from './location-two-checks.model';

import { RemarksModel } from './loaction-third-remarks.model';

export class ThirdDetailsMainModel extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.SUB_FORM,
    label: 'pw.locationMaster.remarks',
    hide: false
  })
  private remarks: RemarksModel;

  constructor(id: number,  remarks: RemarksModel) {
    super();
    this.id = id;
    this.remarks = remarks;
  }
}
