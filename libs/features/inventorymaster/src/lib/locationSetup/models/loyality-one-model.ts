import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class Loyality extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Validation({ validators: [] })
  @Class({ className: ['row'] })
  private checkBoxes: { id: string; name: string; checked?: boolean }[];

  constructor(
    id: number,
    checkBoxes: { id: string; name: string; checked?: boolean }[]
  ) {
    super();
    this.id = id;
    this.checkBoxes = checkBoxes;
  }
}
