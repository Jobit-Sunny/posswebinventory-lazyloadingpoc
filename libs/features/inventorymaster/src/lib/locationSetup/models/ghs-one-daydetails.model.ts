import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class GHSDayDetailsModelCreditNoteConfig extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'selected'
    },
    label: 'pw.locationMaster.baseCurrency'
  })
  @Validation({ validators: [Validators.required] })
  private baseCurrency: { id: string; name: string }[];


  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.suspendingCNs',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:   'pw.inventoryMasterValidation.suspendingCNsdigis' }]
  })
  @Validation({ validators: [Validators.required ,Validators.pattern('^[0-9]*$')]})
  private suspendingCNs: number;
  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.transferredCNs',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:  'pw.inventoryMasterValidation.transferredCNsdigis' }]
  })
  @Validation({ validators: [Validators.required , Validators.pattern('^[0-9]*$')] })
  private transferredCNs: number;

  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.activatedCNs',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:    'pw.inventoryMasterValidation.activatedCNsdigis' }]
  })
  @Validation({ validators: [Validators.required, Validators.pattern('^[0-9]*$') ] })
  private activatedCNs: number;
  constructor(
    id: number,
    baseCurrency: { id: string; name: string; selected?: boolean }[],
    suspendingCNs: number,
    transferredCNs: number,
    activatedCNs: number
  ) {
    super();
    this.id = id;
    this.baseCurrency = baseCurrency;
    this.suspendingCNs = suspendingCNs;
    this.transferredCNs = transferredCNs;
    this.activatedCNs = activatedCNs;
  }
}
