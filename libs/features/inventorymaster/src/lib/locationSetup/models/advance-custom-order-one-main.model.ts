import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType
} from '@poss-web/shared';
import { Foc } from './advance-custom-order-one-foc.model';
import { Rtgs } from './advance-custom-order-one-rtgs.model';

export class AdvanceCustomOrderMainStepOne extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.SUB_FORM,
    label: 'pw.locationMaster.foc',
    hide: false
  })
  private foc: Foc;

  @FormField({
    fieldType: FormFieldType.SUB_FORM,
    label: 'pw.locationMaster.rtgs',
    hide: false
  })
  private rtgs: Rtgs;

  constructor(id: number, foc: Foc, rtgs: Rtgs) {
    super();
    this.id = id;
    this.foc = foc;
    this.rtgs = rtgs;
  }
}
