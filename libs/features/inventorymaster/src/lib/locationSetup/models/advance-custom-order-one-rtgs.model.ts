import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class Rtgs extends DynamicFormFieldsBuilder {
  private id: number;

  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Class({ className: ['row'] })
  private advanceCustomOrderTabOneRtgscheckBoxes: {
    id: string;
    name: string;
    checked?: boolean;
  }[];

  @Validation({
    validators: [
      Validators.required,
      Validators.pattern(
        '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'
      )

    ]
  })
  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.OTPHelpdeskEmailId',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.OTPHelpdeskEmailId_pattern' }]
  })
  @Validation({
    validators: [Validators.required]
  })
  private OTPHelpdeskEmailId: string;


  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.maxNoofCN',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.validitydaysforReleaseInvInCustomerOrder_pattern' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9]*$')]
  })
  private maxNoofCN: string;


  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.minOTPCNValue',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.minOTPCNValue_pattern' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9]*$')]
  })
  private minOTPCNValue: string;

  @Validation({
    validators: [Validators.required, Validators.pattern('^(0[1-9]|[1-9]|[12][0-9]|3[01])-(0[1-9]|[1-9]|1[012])-(19|20)\\d\\d$')]
  })
  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.maxnoofdaysforPOlikelydate',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.maxnoofdaysforPOlikelydate_pattern' }]
  })
  private maxnoofdaysforPOlikelydate: string;

  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9]*$')]
  })
  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.serviceTaxGSTRegistrationNumber',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.serviceTaxGSTRegistrationNumber_pattern' }]
  })
  private serviceTaxGSTRegistrationNumber: string;

  constructor(
    id: number,
    checkBox: { id: string; name: string; checked?: boolean }[],
    minOTPCNValue: string,
    maxNoofCN: string,
    OTPHelpdeskEmailId: string,
    maxnoofdaysforPOlikelydate: string,
    serviceTaxGSTRegistrationNumber: string
  ) {
    super();
    this.id = id;
    this.advanceCustomOrderTabOneRtgscheckBoxes = checkBox;
    this.maxNoofCN = maxNoofCN;
    this.OTPHelpdeskEmailId = OTPHelpdeskEmailId;
    this.minOTPCNValue = minOTPCNValue;
    this.maxnoofdaysforPOlikelydate = maxnoofdaysforPOlikelydate;
    this.serviceTaxGSTRegistrationNumber = serviceTaxGSTRegistrationNumber;
  }
}
