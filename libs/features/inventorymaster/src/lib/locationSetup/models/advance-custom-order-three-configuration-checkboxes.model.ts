import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class AdvanceCustomOrderConfigurationStepThreeCheckBox extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Class({ className: ['row'] })
  private advanceCustomOrderStpeThreecheckBoxes: {
    id: string;
    name: string;
    checked?: boolean;
  }[];

  @Validation({
    validators: [Validators.required, Validators.pattern('[+]?([0-9]*[.])?[0-9]+')]
  })
  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.sparewtToleranceforStockItem',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.goldRateAttempts_pattern' }]
  })
  private sparewtToleranceforStockItem: string;

  @Validation({
    validators: [Validators.required, Validators.pattern('[+]?([0-9]*[.])?[0-9]+')]
  })

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.servicewtToleranceforStockItem',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.goldRateAttempts_pattern' }]
  })
  private servicewtToleranceforStockItem: string;


  constructor(
    id: number,
    advanceCustomOrderStpeThreecheckBoxes: { id: string; name: string; checked?: boolean }[],

    sparewtToleranceforStockItem: string,
    servicewtToleranceforStockItem: string,
  ) {
    super();
    this.id = id;
    this.advanceCustomOrderStpeThreecheckBoxes = advanceCustomOrderStpeThreecheckBoxes;
    this.sparewtToleranceforStockItem = sparewtToleranceforStockItem;
    this.servicewtToleranceforStockItem = servicewtToleranceforStockItem;
  }
}
