import { Validators } from '@angular/forms';
import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation
} from '@poss-web/shared';
import { Towns, Country } from '../+state/location-master.actions';

export class PersonalModel extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({ fieldType: FormFieldType.TEXT, label: 'pw.locationMaster.name' })
  @Validation({ validators: [Validators.required] })
  private name: string;

  // @FormField({
  //   fieldType: FormFieldType.TEXT,
  //   label: 'pw.locationMaster.descrption'
  // })
  // @Validation({ validators: [Validators.required] })
  // private descrption: string;

  @FormField({
    fieldType: FormFieldType.TEXT_AREA,
    label: 'pw.locationMaster.addressOne'
  })
  @Validation({ validators: [Validators.required] })
  private addressOne: string;

  @FormField({
    fieldType: FormFieldType.TEXT_AREA,
    label: 'pw.locationMaster.addressTwo'
  })
  private addressTwo: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.phoneNumberOne',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.phoneNumberOne_pattren' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9]{10}$')]
  })
  private phoneNumberOne: string;

  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9]{10}$')]
  })
  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.phoneNumberTwo',
    validationErrorMessages: [
      { errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.phoneNumberTwo_pattren' }
    ]
  })
  private phoneNumberTwo: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.mobileNumber',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.mobileNumber_pattern' }]
  })
  @Validation({
    validators: [Validators.pattern('^[0-9]{10}$')]
  })
  private mobileNumber: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.emailID',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.emailId_pattern' }]
  })
  @Validation({
    validators: [
      Validators.required,
      Validators.pattern(
        '^[a-zA-Z0-9.!#$%&*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$'
      )
    ]
  })
  private emailId: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.pinCode',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.pinCode_pattern' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9]{6}$')]
  })
  private pinCode: string;


  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'selected'
    },
    label: 'pw.locationMaster.country'
  })
  @Validation({ validators: [Validators.required] })
  private country: { id: string; name: string }[];

  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'selected',
      foreignKey: 'countryCode'
    },
    label: 'pw.locationMaster.state',
    dependsOn: '1-country'
  })
  @Validation({ validators: [Validators.required] })
  private state: { id: string; name: string }[];

  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      foreignKey: 'state_id',
      selectedKey: 'selected'
    },
    label: 'pw.locationMaster.City/Town',
    dependsOn: '1-state'
  })
  @Validation({ validators: [Validators.required] })
  private cityTown: { id: string; name: string; state_id: string }[];

  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'selected'
    },
    label: 'pw.locationMaster.regionCode'
  })
  @Validation({ validators: [Validators.required] })
  private regionCode: { id: string; name: string; selected?: boolean }[];

  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      foreignKey: 'regionCode_id',
      selectedKey: 'selected'
    },
    label: 'pw.locationMaster.subRegionCode',
    dependsOn: '1-regionCode'
  })
  @Validation({ validators: [Validators.required] })
  private subRegionCode: {
    id: string;
    name: string;
    selected?: boolean;
  }[];

  constructor(
    id: number,
    name: string,
    addressOne: string,
    addressTwo: string,
    phoneNumberOne: string,
    phoneNumberTwo: string,
    mobileNumber: string,
    pinCode: string,
    emailId: string,
    country: Country[],
    state: { id: string; name: string; selected?: boolean }[],
    cityTown: Towns[],
    regionCode: { id: string; name: string; selected?: boolean }[],
    subRegionCode: { id: string; name: string; selected?: boolean }[],

  ) {
    super();
    this.id = id;
    this.name = name;

    this.addressOne = addressOne;
    this.addressTwo = addressTwo;
    this.phoneNumberOne = phoneNumberOne;
    this.phoneNumberTwo = phoneNumberTwo;
    this.mobileNumber = mobileNumber;
    this.emailId = emailId;
    this.pinCode = pinCode;
    this.country = country
    this.state = state;
    this.cityTown = cityTown;

    this.regionCode = regionCode;
    this.subRegionCode = subRegionCode;

  }
}
