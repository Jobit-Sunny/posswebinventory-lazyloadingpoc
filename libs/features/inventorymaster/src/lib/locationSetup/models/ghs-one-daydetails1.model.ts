import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class GHSDayDetailsModelValidityConfig extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.ddValidity',
       validationErrorMessages: [{ errorType: 'pattern', errorMessage:'pw.inventoryMasterValidation.ddValidityDigits' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9]*$')]
  })
  private ddValidityDays: number;
  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.consolidatedAttempts',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.consolidatedAttemptsDigits' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9]*$')]
  })
  private consolidatedAttempts: number;
  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.reversalDays',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:  'pw.inventoryMasterValidation.reversalDaysDigits' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9]*$')]
  })
  private reversalDays: number;

  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.realisationDays',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.realisationDaysDigits' }]
  })
  @Validation({ validators: [Validators.pattern('^[0-9]*$')] })
  private realisationDays: number;

  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.validityDays',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:'pw.inventoryMasterValidation.validityDaysDigits' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9]*$')]
  })
  private validityDays: number;

  constructor(
    id: number,
    ddValidityDates: number,
    consolidatedAttempts: number,
    realisationDays: number,
    validityDays: number
  ) {
    super();
    this.id = id;
    this.ddValidityDays = ddValidityDates;
    this.consolidatedAttempts = consolidatedAttempts;
    this.realisationDays = realisationDays;
    this.validityDays = validityDays;
  }
}
