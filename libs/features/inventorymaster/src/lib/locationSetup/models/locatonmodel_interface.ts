interface ConfigDetails {
  locationStepOne?: LocationStepOne;
  locationStepTwo?: LocationStepTwo;
  locationStepThree?: LocationStepThree;
  ghsStepOne?: GhsStepOne;
  ghsStepTwo?: GhsStepTwo;
  printStepOne?: PrintStepOne;
  advanceStepOne?: AdvanceStepOne;
  advanceStepTwo?: AdvanceStepTwo;
  advanceStepThree?: AdvanceStepThree;
  grnIBTInventoryStepOne?: GrnIBTInventoryStepOne;
  grnIBTInventoryStepTwo?: GrnIBTInventoryStepTwo;
  loyalityStepOne?: LoyalityStepOne;
  loyalityStepTwo?: LoyalityStepTwo;
  loyalityStepThree?: LoyalityStepThree;
}

interface LoyalityStepThree {
  tep: Tep;
}

interface Tep {
  enableRTGSrefund: boolean;
  noOfDaysForFVTPassword: number;
}

interface LoyalityStepTwo {
  ccPayment: CcPayment;
  employeeDiscount: EmployeeDiscount;
  giftCardConfiguration: GiftCardConfiguration;
}

interface GiftCardConfiguration {
  giftCardConfigurationCheckBoxes: GiftCardConfigurationCheckBoxes;
  maximumAmount: number;
  minimumAmount: number;
  multiplesValue: number;
}

interface GiftCardConfigurationCheckBoxes {
  isCardActivationAllowed: boolean;
  isCardCancellationAllowed: boolean;
  isCardInwardingAllowed: boolean;
  isCardReedemAlowed: boolean;
}

interface EmployeeDiscount {
  enableEmployeeDiscount: boolean;
}

interface CcPayment {
  enableUniPay: boolean;
}

interface LoyalityStepOne {
  gvPayment: GvPayment;
  loyality: Loyality;
  personalDetails: PersonalDetails2;
}

interface PersonalDetails2 {
  enableGEPSale: boolean;
  gepPureGoldPurity: string;
  gepPurePlatinumPurity: string;
  gepPureSilverPurity: string;
  gepStandardDeductionGold: string;
  gepStandardDeductionPlatinum: string;
  gepStandardDeductionSilver: string;
}

interface Loyality {
  isBankingMandatory: boolean;
  isSynchronised: boolean;
}

interface GvPayment {
  enableCNCancellationForGVPayment: boolean;
  enableCNTarnsferForGVpayment: boolean;
}

interface GrnIBTInventoryStepTwo {
  ibtConfiguration: IbtConfiguration;
  kycConfiguration: KycConfiguration;
  ulpConfiguration: UlpConfiguration;
}

interface UlpConfiguration {
  isEncirclePaymentAllowed: boolean;
}

interface KycConfiguration {
  isDownloadDocumentAllowed: boolean;
  isUploadDocumentAllowed: boolean;
}

interface IbtConfiguration {
  noOfItemsRequestedInCurrentMonth: number;
  noOfTimesrequestedInCurrentMonth: number;
  totalValueRequestedInCurrentMonth: number;
}

interface GrnIBTInventoryStepOne {
  grnGrfConfiguration: GrnGrfConfiguration;
  inventory: Inventory;
  walkins: Walkins;
}

interface Walkins {
  isWaliknsDetailsMandatory: boolean;
  noOfDays: number;
  numberOfDaysToDisplay: number;
}

interface Inventory {
  configurationAmountForStuddedSplit: string;
  inventoryCheckBoxes: InventoryCheckBoxes;
  maximumNoOfDaysForPhysicalReceiptDate: number;
  maximumNoOfDaysForSTNCancellation: number;
}

interface InventoryCheckBoxes {
  isAcceptTepGepDisputeStocks: boolean;
  isConversionRestricted: boolean;
  isSTNcancellationAllowed: boolean;
  isStockTransferForBoutique: boolean;
}

interface GrnGrfConfiguration {
  grnGrfCheckBoxes: GrnGrfCheckBoxes;
  maximumNoOfDaysForApprovedGRN: number;
  minimumUtilization: string;
  noOfDaysGRNAllowed: number;
  noOfDaysToProtectGoldRateForGRN: number;
}

interface GrnGrfCheckBoxes {
  isGRFAllowed: boolean;
  isGRFAllowedInAdavnceBooking: boolean;
  isGRFAllowedInCM: boolean;
  isGRFAllowedInCustomerOrder: boolean;
  isInterBoutiqueGRNAllowed: boolean;
}

interface AdvanceStepThree {
  configurePaymentMode: ConfigurePaymentMode;
  locationPriceMapping: LocationPriceMapping;
  otpConfigurations: OtpConfigurations;
}

interface OtpConfigurations {
  advanceCustomOrderTabThreecheckBoxes: AdvanceCustomOrderTabThreecheckBoxes;
}

interface AdvanceCustomOrderTabThreecheckBoxes {
  isOTPallowedAB: boolean;
  isOTPallowedASSM: boolean;
  isOTPallowedAdvance: boolean;
  isOTPallowedCM: boolean;
  isOTPallowedCO: boolean;
  isOTPallowedGHS: boolean;
  isOTPrequiredforGC: boolean;
}

interface LocationPriceMapping {
  priceGroupCode: string;
  priceGroupTypeCode: string;
}

interface ConfigurePaymentMode {
  configurePaymentModeCheckBoxes: ConfigurePaymentModeCheckBoxes2;
  paymentCode: string;
}

interface ConfigurePaymentModeCheckBoxes2 {
  configurePaymentModeCheckBoxes: ConfigurePaymentModeCheckBoxes;
}

interface ConfigurePaymentModeCheckBoxes {
  isApplicableforLocation: boolean;
  isApplicableforReversal: boolean;
}

interface AdvanceStepTwo {
  foc: Foc;
  rtgs: Rtgs;
}

interface Rtgs {
  advanceCustomOrderTabTwoRtgscheckBoxes: AdvanceCustomOrderTabTwoRtgscheckBoxes;
  maxNoofCN: number;
  maxNoofdaysforPOLikelyDate: number;
  minOTPCNValue: string;
  otpHelpdeskEmailId: string;
  serviceTaxGSTRegistrationno: string;
}

interface AdvanceCustomOrderTabTwoRtgscheckBoxes {
  enableRTGSPayment: boolean;
}

interface Foc {
  advanceCustomOrderTabTwoFoccheckBoxes: AdvanceCustomOrderTabTwoFoccheckBoxes;
  maxValueforFOC: number;
  maxWeightforFOC: number;
}

interface AdvanceCustomOrderTabTwoFoccheckBoxes {
  bintobintransferallowedforFOCitems: boolean;
  isFOCitemssaleable: boolean;
  isTEPallowedforFOCitems: boolean;
  isTEPsaleableitemsallowedforFOC: boolean;
}

interface AdvanceStepOne {
  advanceCustomOrderConfiguration: AdvanceCustomOrderConfiguration;
}

interface AdvanceCustomOrderConfiguration {
  advanceCustomOrderStepOneCheckBox: AdvanceCustomOrderStepOneCheckBox;
  goldRateAttempts: string;
  manualBillWeightDeviation: number;
  servicewtToleranceforStockItem: number;
  sparewtToleranceforStockItem: number;
  validityDaysforActivateInAdvanceBooking: number;
  validityDaysforActivateInCustomerOrder: number;
  validityDaysforAutoClosureInAdvanceBooking: number;
  validityDaysforAutoClosureInCustomerOrder: number;
  validityDaysforReleaseInvInAdvancebooking: number;
  validitydaysforReleaseInvInCustomerOrder: number;
}

interface AdvanceCustomOrderStepOneCheckBox {
  activateAllowedforAdvanceBooking: boolean;
  activateAllowedforCustomerOrder: boolean;
  cancellationAllowedforAdvanceBooking: boolean;
  cancellationAllowedforCustomerOrder: boolean;
  printMandatoryFieldsInReport: boolean;
}

interface PrintStepOne {
  checkItOut1: CheckItOut1;
  checkItOut2: CheckItOut2;
  makingWastageCharge: number;
}

interface CheckItOut2 {
  freeTextForGrams: string;
  noOfInvoicecopiesforRegularOrQuickCM: number;
  printCashMemo: boolean;
  printCustomerNumberinReport: boolean;
  printGoldValue: boolean;
  printGuaranteeCard: boolean;
  printImage: boolean;
  printOtherStoneWeightinAnnexure: boolean;
  printOtherStoneWtinGuaranteeCard: boolean;
}

interface CheckItOut1 {
  printMakingCharges: boolean;
  printPrice: boolean;
  printStoneValue: boolean;
  printWastageCharge: boolean;
  printWastageComponent: boolean;
  printWastagePercent: boolean;
}

interface GhsStepTwo {
  ghsIbtCheckBox: GhsIbtCheckBox;
}

interface GhsIbtCheckBox {
  eghsredemption: boolean;
  eghsrevenue: boolean;
  isClubbingGHSMandatory: boolean;
  isConsentLetterUploadMandatory: boolean;
}

interface GhsStepOne {
  dayDetails: DayDetails;
}

interface DayDetails {
  activatedCNs: number;
  baseCurrency: string;
  consolidateAttempts: number;
  ddValidityDays: number;
  realisationDays: number;
  suspendingCNs: number;
  transferredCNs: number;
  validityDays: number;
}

interface LocationStepThree {
  locationCheckbox: LocationCheckbox2;
  remarks: Remarks;
}

interface Remarks {
  paymentModeForRefund: string;
  remarks: string;
  sapCode: string;
}

interface LocationCheckbox2 {
  locationCheckbox: LocationCheckbox;
}

interface LocationCheckbox {
  isActive: boolean;
}

interface LocationStepTwo {
  configurationDetails: ConfigurationDetails;
  otherDetails: OtherDetails;
}

interface OtherDetails {
  cinNumber: string;
  corporateAddress: string;
  marketCode: string;
  regdOffice: string;
}

interface ConfigurationDetails {
  checkBoxes: CheckBoxes;
}

interface CheckBoxes {
  digitalSignatureEnable: boolean;
  enableCashDeposit: boolean;
  enableChequeDeposit: boolean;
  ifeedbackAllowedForCM: boolean;
  isFeedbackAllowedForAssm: boolean;
  isPasswordMandatory: boolean;
  isStuddedSplitAllowed: boolean;
}

interface LocationStepOne {
  locationDetails: LocationDetails;
  personalDetails: PersonalDetails;
}

interface PersonalDetails {
  phoneNumberTwo: string;
  adressTwo: string;
  name: string;
  subRegion: string;
}

interface LocationDetails {
  locationShortName: string;
  subBrandName: string;
}