import { DynamicFormFieldsBuilder, FormField, FormFieldType, Validation, Class } from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class GRNConfiguration extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.noOfDaysGRNAllowed',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:'pw.inventoryMasterValidation.noOFDaysGRNAllowed_pattern' }]
  })
  @Validation({
    validators: [Validators.pattern('^[0-9]*')]
  })
  private noOfDaysGRNAllowed: string;

  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.maximumNoOfDaysForApprovedGRN',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:'pw.inventoryMasterValidation.noOFDaysGRNAllowed_pattern'}]
  })
  @Validation({
    validators: [Validators.pattern('^[0-9]*')]
  })
  private maximumNoOfDaysForApprovedGRN: string;

  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.noOfDaySToProtectGoldRateForGRN',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:'pw.inventoryMasterValidation.noOFDaysGRNAllowed_pattern' }]
  })
  @Validation({
    validators: [Validators.pattern('[+]?([0-9]*[.])?[0-9]+')]
  })
  private noOfDaySToProtectGoldRateForGRN: string;

  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.minimumUtilization',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:'pw.inventoryMasterValidation.noOFDaysGRNAllowed_pattern' }]
  })
  @Validation({
    validators: [Validators.pattern('[+]?([0-9]*[.])?[0-9]+')]
  })
  private minimumUtilization: string;

  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Validation({ validators: [] })
  @Class({ className: ['row'] })
  private checkBoxes: { id: string; name: string; checked?: boolean }[];

  constructor(
    id: number,
    noOfDaysGRNAllowed: string,
    maximumNoOfDaysForApprovedGRN: string,
    noOfDaySToProtectGoldRateForGRN: string,
    minimumUtilization: string,
    checkBoxes: { id: string; name: string; checked?: boolean }[]
  ) {
    super();
    this.id = id;
    this.noOfDaysGRNAllowed = noOfDaysGRNAllowed;
    this.maximumNoOfDaysForApprovedGRN = maximumNoOfDaysForApprovedGRN;
    this.noOfDaySToProtectGoldRateForGRN = noOfDaySToProtectGoldRateForGRN;
    this.minimumUtilization = minimumUtilization;
    this.checkBoxes = checkBoxes;
  }
}
