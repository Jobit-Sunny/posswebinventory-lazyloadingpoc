import { DynamicFormFieldsBuilder, FormField, FormFieldType, Validation, Class } from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class PersonalDetails extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.GEPPureGoldPurity',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.totalValueRequestedeInCurrentMonth_pattern' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('[0-9]*(.{1})?[0-9]{1,2}')]
  })
  private GEPPureGoldPurity: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.GEPPureSilverPurity',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.totalValueRequestedeInCurrentMonth_pattern' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('[0-9]*(.{1})?[0-9]{1,2}')]
  })
  private GEPPureSilverPurity: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.GEPPurePlatinumPurity',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.totalValueRequestedeInCurrentMonth_pattern' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('[0-9]*(.{1})?[0-9]{1,2}')]
  })
  private GEPPurePlatinumPurity: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.GEPStandaredDeductionGold',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.totalValueRequestedeInCurrentMonth_pattern' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('[0-9]*(.{1})?[0-9]{1,2}')]
  })
  private GEPStandaredDeductionGold: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.GEPStandaredDeductionSilver',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.totalValueRequestedeInCurrentMonth_pattern' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('[0-9]*(.{1})?[0-9]{1,2}')]
  })
  private GEPStandaredDeductionSilver: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.GEPStandaredDeductionPlatinum',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.totalValueRequestedeInCurrentMonth_pattern' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('[0-9]*(.{1})?[0-9]{1,2}')]
  })
  private GEPStandaredDeductionPlatinum: string;

  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Validation({ validators: [] })
  @Class({ className: ['row'] })
  private enableGEPSale: { id: string; name: string; checked?: boolean }[];
  constructor(
    id: number,
    GEPPureGoldPurity: string,
    GEPPureSilverPurity: string,
    GEPPurePlatinumPurity: string,
    GEPStandaredDeductionGold: string,
    GEPStandaredDeductionSilver: string,
    GEPStandaredDeductionPlatinum: string,
    enableGEPSale: { id: string; name: string; checked?: boolean }[]
  ) {
    super();
    this.id = id;
    this.GEPPureGoldPurity = GEPPureGoldPurity;
    this.GEPPureSilverPurity = GEPPureSilverPurity;
    this.GEPPurePlatinumPurity = GEPPurePlatinumPurity;
    this.GEPStandaredDeductionGold = GEPStandaredDeductionGold;
    this.GEPStandaredDeductionSilver = GEPStandaredDeductionSilver;
    this.GEPStandaredDeductionPlatinum = GEPStandaredDeductionPlatinum;
    this.enableGEPSale = enableGEPSale;
  }
}
