import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class AdvanceCustomOrderConfigurationStepThree extends DynamicFormFieldsBuilder {
  private id: number;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.validityDaysforAutoClosureInAdvanceBooking',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.validityDaysforAutoClosureInAdvanceBooking_pattern' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[+]?[1-9]*$')]
  })
  private validityDaysforAutoClosureInAdvanceBooking: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.validityDaysforActivateInAdvanceBooking',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.validityDaysforActivateInAdvanceBooking_pattern' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9]*$')]
  })
  private validityDaysforActivateInAdvanceBooking: string;

  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9]*$')]
  })
  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.validityDaysforReleaseInvInAdvancebooking',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.validityDaysforReleaseInvInAdvancebooking_pattern' }]
  })
  private validityDaysforReleaseInvInAdvancebooking: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.validityDaysforAutoClosureInCustomerOrder',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.validityDaysforAutoClosureInCustomerOrder_pattern' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9]*$')]
  })
  private validityDaysforAutoClosureInCustomerOrder: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.validityDaysforActivateInCustomerOrder',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.validityDaysforActivateInCustomerOrder_pattern' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9]*$')]
  })
  private validityDaysforActivateInCustomerOrder: string;

  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9]*$')]
  })
  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.validitydaysforReleaseInvInCustomerOrder',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.validitydaysforReleaseInvInCustomerOrder_pattern' }]
  })
  private validitydaysforReleaseInvInCustomerOrder: string;

  @Validation({
    validators: [
      Validators.required,
      Validators.pattern('[0-9]*(.{1})?[0-9]{1,3}')
    ]
  })
  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.goldRateAttempts',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.goldRateAttempts_pattern' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('[0-9]*(.{1})?[0-9]{1,3}')]
  })
  private goldRateAttempts: string;

  @Validation({
    validators: [
      Validators.required,
      Validators.pattern('[0-9]*(.{1})?[0-9]{1,3}')
    ]
  })
  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.manualBillWeightDeviation',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.manualBillWeightDeviation_pattern' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('[0-9]*(.{1})?[0-9]{1,3}')]
  })
  private manualBillWeightDeviation: string;

  constructor(
    id: number,
    validityDaysforAutoClosureInAdvanceBooking: string,
    validityDaysforActivateInAdvanceBooking: string,
    validityDaysforReleaseInvInAdvancebooking: string,
    validityDaysforAutoClosureInCustomerOrder: string,
    validityDaysforActivateInCustomerOrder: string,
    validitydaysforReleaseInvInCustomerOrder: string,
    goldRateAttempts: string,
    manualBillWeightDeviation: string
  ) {
    super();
    this.id = id;

    this.validityDaysforAutoClosureInAdvanceBooking = validityDaysforAutoClosureInAdvanceBooking;
    this.validityDaysforActivateInAdvanceBooking = validityDaysforActivateInAdvanceBooking;
    this.validityDaysforReleaseInvInAdvancebooking = validityDaysforReleaseInvInAdvancebooking;
    this.validityDaysforAutoClosureInCustomerOrder = validityDaysforAutoClosureInCustomerOrder;
    this.validityDaysforActivateInCustomerOrder = validityDaysforActivateInCustomerOrder;
    this.validitydaysforReleaseInvInCustomerOrder = validitydaysforReleaseInvInCustomerOrder;
    this.goldRateAttempts = goldRateAttempts;
    this.manualBillWeightDeviation = manualBillWeightDeviation;
  }
}
