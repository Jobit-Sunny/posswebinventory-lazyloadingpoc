import { DynamicFormFieldsBuilder, FormField, FormFieldType, Validation, Class } from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class ChecksModel extends DynamicFormFieldsBuilder {

  private id: number;

  @FormField({ fieldType: FormFieldType.CHECKBOX, selectOptionKeys: { labelKey: 'name', valueKey: 'id', selectedKey: 'checked' }, label: '' })
  @Class({ className: ['row'] })
  private checkBoxes: { id: string, name: string, checked?: boolean }[];

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.makingCharges',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.makingChargesDigits' }]
  })
  @Validation({ validators: [Validators.pattern('^[0-9]*$')] })
  private makingChargesorWastageHeading : number;


  constructor(
      id: number,
      checkBoxes: { id: string, name: string, checked?: boolean }[],
      makingChargesorWastageHeading:number
  ) {
      super();
      this.id = id;
      this.checkBoxes = checkBoxes;
      this.makingChargesorWastageHeading=makingChargesorWastageHeading;

  }
}
