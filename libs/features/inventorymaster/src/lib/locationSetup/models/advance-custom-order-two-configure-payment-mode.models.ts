import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class ConfigurePaymentMode extends DynamicFormFieldsBuilder {
  private id: number;

  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Class({ className: ['row'] })
  private configurePaymentModeCheckBoxes: {
    id: string;
    name: string;
    checked?: boolean;
  }[];


  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.paymentCode',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:  'pw.inventoryMasterValidation.validitydaysforReleaseInvInCustomerOrder_pattern' }]
  })
  @Validation({
    validators: [Validators.required,Validators.pattern('^[0-9]*$')]
  })
  private paymentCode: string;

  constructor(
    id: number,
    configurePaymentModeCheckBoxes: {
      id: string;
      name: string;
      checked?: boolean;
    }[],
    paymentCode: string
  ) {
    super();
    this.id = id;
    this.configurePaymentModeCheckBoxes = configurePaymentModeCheckBoxes;
    this.paymentCode = paymentCode;
  }
}
