import { DynamicFormFieldsBuilder, FormField, FormFieldType, Validation, Class } from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class GrnIbtInventoryOne extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.maxNoOfDaysForPhysicalReceiptDate',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:'pw.inventoryMasterValidation.maximumNoOfDaysForPhysicalReceiptDatae_pattern'}]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[1-9][0-9]*')]
  })
  private maxNoOfDaysForPhysicalReceiptDate: string;

  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.configurationAmountForStuddedSplit',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:'pw.inventoryMasterValidation.totalValueRequestedeInCurrentMonth_pattern'}]
  })
  @Validation({
    validators: [Validators.pattern('[0-9]*(.{1})?[0-9]{1,2}')]
  })
  private configurationAmountForStuddedSplit: string;

  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.maxNoOfDaysForSTNCancellation',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage:'pw.inventoryMasterValidation.noOFDaysGRNAllowed_pattern'}]
  })
  @Validation({
    validators: [Validators.pattern('^[0-9]*')]
  })
  private maxNoOfDaysForSTNCancellation: string;
  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Validation({ validators: [] })
  @Class({ className: ['row'] })
  private checkBoxes: { id: string; name: string; checked?: boolean }[];

  constructor(
    id: number,
    maxNoOfDaysForPhysicalReceiptDate: string,
    configurationAmountForStuddedSplit: string,
    maxNoOfDaysForSTNCancellation: string,
    checkBoxes: { id: string; name: string; checked?: boolean }[]
  ) {
    super();
    this.id = id;
    this.maxNoOfDaysForPhysicalReceiptDate = maxNoOfDaysForPhysicalReceiptDate;
    this.configurationAmountForStuddedSplit = configurationAmountForStuddedSplit;
    this.maxNoOfDaysForSTNCancellation = maxNoOfDaysForSTNCancellation;
    this.checkBoxes = checkBoxes;
  }
}
