import {
  DynamicFormFieldsBuilder,
  FormField,
  Validation,
  FormFieldType,
  Class
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class Tep extends DynamicFormFieldsBuilder {
  private id: number;

  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.noOfDaysForFVTPassword',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.noOFDaysGRNAllowed_pattern' }]
  })
  @Validation({
    validators: [Validators.required, Validators.pattern('^[0-9]*')]
  })
  private noOfDaysForFVTPassword: string;

  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Validation({ validators: [] })
  @Class({ className: ['row'] })
  private enableRTGSRefund: { id: string; name: string; checked?: boolean }[];
  constructor(
    id: number,
    noOfDaysForFVTPassword: string,
    enableRTGSRefund: { id: string; name: string; checked?: boolean }[]
  ) {
    super();
    this.id = id;
    this.noOfDaysForFVTPassword = noOfDaysForFVTPassword;
    this.enableRTGSRefund = enableRTGSRefund;
  }
}
