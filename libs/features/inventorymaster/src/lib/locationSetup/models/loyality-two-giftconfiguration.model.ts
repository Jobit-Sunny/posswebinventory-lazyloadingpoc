import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class GiftCardConfiguration extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.maximumAmount',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.totalValueRequestedeInCurrentMonth_pattern' }]
  })
  @Validation({
    validators: [Validators.pattern('^[+]?([0-9]*[.])?[0-9]+')]
  })
  private maximumAmount: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.minimumAmount',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.totalValueRequestedeInCurrentMonth_pattern' }]
  })
  @Validation({
    validators: [Validators.pattern('^[+]?([0-9]*[.])?[0-9]+')]
  })
  private minimumAmount: string;

  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.locationMaster.multiplesValues',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.noOFDaysGRNAllowed_pattern' }]
  })
  @Validation({
    validators: [Validators.pattern('^[0-9]*')]
  })
  private multiplesValues: string;

  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Validation({ validators: [] })
  @Class({ className: ['row'] })
  private checkBoxes: { id: string; name: string; checked?: boolean }[];

  constructor(
    id: number,
    maximumAmount: string,
    minimumAmount: string,
    multiplesValues: string,
    checkboxes: { id: string; name: string; checked?: boolean }[]
  ) {
    super();
    this.id = id;
    this.maximumAmount = maximumAmount;
    this.minimumAmount = minimumAmount;
    this.multiplesValues = multiplesValues;
    this.checkBoxes = checkboxes;
  }
}
