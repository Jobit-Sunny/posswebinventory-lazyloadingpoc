import { DynamicFormFieldsBuilder, FormField, FormFieldType, Validation, Class } from '@poss-web/shared';
import { Validators } from '@angular/forms';

export class GrnIbtInventoryWalkIns extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Validation({ validators: [] })
  @Class({ className: ['row'] })
  private isWalkinsDetailsMandatory: {
    id: string;
    name: string;
    checked?: boolean;
  }[];
  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.numbersOfDays',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.noOFDaysGRNAllowed_pattern' }]
  })
  @Validation({
    validators: [Validators.pattern('^[0-9]*')]
  })
  private numbersOfDays: string;

  @FormField({
    fieldType: FormFieldType.OUTLINE,
    label: 'pw.locationMaster.numbersOfDatesToDisplay',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.inventoryMasterValidation.noOFDaysGRNAllowed_pattern' }]
  })
  @Validation({
    validators: [Validators.pattern('^[0-9]*')]
  })
  private numbersOfDatesToDisplay: string;
  constructor(
    id: number,
    isWalkinsDetailsMandatory: {
      id: string;
      name: string;
      checked?: boolean;
    }[],
    numbersOfDays: string,
    numbersOfDatesToDisplay: string
  ) {
    super();
    this.id = id;
    this.isWalkinsDetailsMandatory = isWalkinsDetailsMandatory;
    this.numbersOfDays = numbersOfDays;
    this.numbersOfDatesToDisplay = numbersOfDatesToDisplay;
  }
}
