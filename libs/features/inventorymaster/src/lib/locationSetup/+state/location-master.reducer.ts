import { locationListAdapter } from './location-master.entity';
import { LocationMasterState } from './location-master.state';
import {
  LocationMasterActions,
  LocationMasterActionTypes
} from './location-master.actions';

/**
 * The initial state of the store
 */
const initialState: LocationMasterState = {
  locationListing: locationListAdapter.getInitialState(),
  isLoadingLocationListing: false,
  isLoadingLocation: false,
  totalCount: 0,
  location: null,
  stateTypes: null,
  towns: null,
  locationTypes: null,
  ownerInfo: null,
  regions: null,
  isNewLocation: false,
  error: null,
  saveMasterDetails: null,
  baseCurrencyTypes: null,
  marketTypes: null,
  brands: null,
  isCopySuccess: false,
  isSearchElements: true,
  country: null,
  paymentCode: null,
  priceGroup: null,
  locationFormat: null,
  isSaved: false
};

/**
 * The reducer function which manipulates the store for respective Action
 */
export function LocationMasterReducer(
  state: LocationMasterState = initialState,
  action: LocationMasterActions
): LocationMasterState {
  switch (action.type) {
    case LocationMasterActionTypes.LOAD_COUNTRY:
      return {
        ...state
      };
    case LocationMasterActionTypes.LOAD_COUNTRY_SUCCESS:
      return {
        ...state,
        country: action.payload
      };
    case LocationMasterActionTypes.LOAD_STATES:
    case LocationMasterActionTypes.LOAD_LOCATION_FORMAT:
    case LocationMasterActionTypes.LOAD_PRICE_GROUP:
    case LocationMasterActionTypes.LOAD_PAYMENT_CODE:
    case LocationMasterActionTypes.LOAD_TOWNS:
    case LocationMasterActionTypes.LOAD_BRAND:
    case LocationMasterActionTypes.LOAD_MARKET_CODE:
    case LocationMasterActionTypes.LOAD_LOCATION_TYPES:
    case LocationMasterActionTypes.LOAD_LOCATION_BY_LOCATION_CODE:
      return {
        ...state,
        isLoadingLocation: true,
        error: null
      };
    case LocationMasterActionTypes.LOAD_STATES_SUCCESS:
      return {
        ...state,
        stateTypes: action.payload,
        isLoadingLocation: false,
        error: null
      };

    case LocationMasterActionTypes.LOAD_STATES_FAILURE:
    case LocationMasterActionTypes.LOAD_BASE_CURRENCY_FAILURE:
    case LocationMasterActionTypes.LOAD_REGION_FAILURE:
    case LocationMasterActionTypes.LOAD_OWNER_INFO_FAILURE:
    case LocationMasterActionTypes.LOAD_LOCATION_BY_LOCATION_CODE_FAILURE:
    case LocationMasterActionTypes.LOAD_LOCAITON_TYPES_FAILURE:
    case LocationMasterActionTypes.LOAD_MARKET_CODE_FAILURE:
    return {
        ...state,
        error: action.payload,
        isLoadingLocation: false
      };

    // case LocationMasterActionTypes.LOAD_LOCATION_FORMAT:
    //   return {
    //     ...state,
    //     isLoadingLocation: true,
    //     error: null
    //   };

    case LocationMasterActionTypes.LOAD_LOCATION_FORMAT_SUCCESS:
      return {
        ...state,
        locationFormat: action.payload,
        isLoadingLocation: false
      };
    case LocationMasterActionTypes.LOAD_LOCATION_FORMAT_FAILURE:
      return { ...state, error: action.payload, isLoadingLocation: false };

    // case LocationMasterActionTypes.LOAD_STATES_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload,
    //     isLoadingLocation: false
    //   };

    // case LocationMasterActionTypes.LOAD_LOCATION_FORMAT:
    //   return {
    //     ...state,
    //     isLoadingLocation: true,
    //     error: null
    //   };

    // case LocationMasterActionTypes.LOAD_LOCATION_FORMAT_SUCCESS:
    //   return {
    //     ...state,
    //     locationFormat: action.payload,
    //     isLoadingLocation: false
    //   };
    // case LocationMasterActionTypes.LOAD_LOCATION_FORMAT_FAILURE:
    //   return { ...state, error: action.payload, isLoadingLocation: false };

    // case LocationMasterActionTypes.LOAD_PRICE_GROUP:
    //   return {
    //     ...state,
    //     isLoadingLocation: true,
    //     error: null
    //   };

    case LocationMasterActionTypes.LOAD_PRICE_GROUP_SUCCESS:
      return {
        ...state,
        priceGroup: action.payload,
        isLoadingLocation: false
      };
    case LocationMasterActionTypes.LOAD_PRICE_GROUP_FAILURE:
      return { ...state, error: action.payload, isLoadingLocation: false };

    // case LocationMasterActionTypes.LOAD_PAYMENT_CODE:
    //   return {
    //     ...state,
    //     isLoadingLocation: true,
    //     error: null
    //   };

    case LocationMasterActionTypes.LOAD_PAYMENT_CODE_SUCCESS:
      return {
        ...state,
        paymentCode: action.payload,
        isLoadingLocation: false
      };
    case LocationMasterActionTypes.LOAD_PAYMENT_CODE_FAILURE:
      return { ...state, error: action.payload, isLoadingLocation: false };

    // case LocationMasterActionTypes.LOAD_TOWNS:
    //   return {
    //     ...state,
    //     isLoadingLocation: true,
    //     error: null
    //   };

    case LocationMasterActionTypes.LOAD_TOWNS_SUCCESS:
      return {
        ...state,
        towns: action.payload,
        isLoadingLocation: false
      };
    case LocationMasterActionTypes.LOAD_TOWNS_FAILURE:
      return { ...state, error: action.payload, isLoadingLocation: false };

    // case LocationMasterActionTypes.LOAD_BRAND:
    //   return {
    //     ...state,
    //     isLoadingLocation: true,
    //     error: null
    //   };

    case LocationMasterActionTypes.LOAD_BRAND_SUCCESS:
      return {
        ...state,
        brands: action.payload,
        isLoadingLocation: false,
        error: null
      };

    case LocationMasterActionTypes.LOAD_BRAND_FAILURE:
      return { ...state, error: action.payload, isLoadingLocation: false };

    // case LocationMasterActionTypes.LOAD_MARKET_CODE:
    //   return {
    //     ...state,
    //     isLoadingLocation: true,
    //     error: null
    //   };

    case LocationMasterActionTypes.LOAD_MARKET_CODE_SUCCESS:
      return {
        ...state,
        marketTypes: action.payload,
        isLoadingLocation: false,
        error: null
      };

    // case LocationMasterActionTypes.LOAD_MARKET_CODE_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload,
    //     isLoadingLocation: false
    //   };

    // case LocationMasterActionTypes.LOAD_LOCATION_TYPES:
    //   return {
    //     ...state,
    //     isLoadingLocation: true,
    //     error: null
    //   };
    case LocationMasterActionTypes.LOAD_LOCATION_TYPES_SUCCESS:
      return {
        ...state,
        locationTypes: action.payload,
        isLoadingLocation: false,
        error: null
      };

    // case LocationMasterActionTypes.LOAD_LOCAITON_TYPES_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload,
    //     isLoadingLocation: false
    //   };
    case LocationMasterActionTypes.LOAD_LOCAITON_LISTING:
      return {
        ...state,
        isLoadingLocationListing: true,
        error: null
      };

    case LocationMasterActionTypes.LOAD_LOCAITON_LISTING_SUCCESS:
      return {
        ...state,

        locationListing: locationListAdapter.addAll(
          action.payload.results,
          state.locationListing
        ),
        isLoadingLocationListing: false,
        totalCount: action.payload.totalElements,
        error: null,
        isSearchElements: true
      };

    case LocationMasterActionTypes.LOAD_LOCAITON_LISTING_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingLocationListing: false,
        isSearchElements: true
      };

    // case LocationMasterActionTypes.LOAD_LOCATION_BY_LOCATION_CODE:
    //   return {
    //     ...state,
    //     isLoadingLocation: true,
    //     error: null
    //   };
    case LocationMasterActionTypes.LOAD_LOCATION_BY_LOCATION_CODE_SUCCESS:
      return {
        ...state,
        location: action.payload,
        isLoadingLocation: false,
        error: null
      };
    // case LocationMasterActionTypes.LOAD_LOCATION_BY_LOCATION_CODE_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload,
    //     isLoadingLocation: false
    //   };
    case LocationMasterActionTypes.SEARCH_LOCATION_BY_LOCATIONCODE:
      return {
        ...state,
        error: null
      };
    case LocationMasterActionTypes.SEARCH_LOCATION_BY_LOCATIONCODE_SUCCESS:
      return {
        ...state,
        locationListing: locationListAdapter.addAll(
          action.payload.results,
          state.locationListing
        ),
        isSearchElements: true,
        totalCount: action.payload.totalElements,
        error: null
      };

    case LocationMasterActionTypes.SEARCH_LOCATION_BY_LOCATIONCODE_FAILURE:
      return {
        ...state,
        error: action.payload,
        isSearchElements: false,
        locationListing: locationListAdapter.removeAll(state.locationListing)
      };
    case LocationMasterActionTypes.LOAD_NEW_LOCATION_ACTION:
      return {
        ...state,
        isNewLocation: true,
        isLoadingLocation: false,
        error: null
      };
    case LocationMasterActionTypes.LOAD_NEW_ACTION_SUCCESS:
      return {
        ...state,
        location: action.payload,
        isLoadingLocation: false
      };
    case LocationMasterActionTypes.RESET_ERROR:
      return {
        ...state,
        location: null,
        isSaved: false,
        error: null
      };
    case LocationMasterActionTypes.SAVE_LOCATION_DETAILS:
    case LocationMasterActionTypes.SAVE_FORM_DETAILS:
      return {
        ...state,
        isLoadingLocation: true,
        isSaved: false,
        error: null
      };
    case LocationMasterActionTypes.SAVE_LOCATION_DETAILS_SUCCESS:
    case LocationMasterActionTypes.SAVE_FORM_DETAILS_SUCCESS:
      return {
        ...state,
        location: action.payload,
        isSaved: true,
        isLoadingLocation: false,
        error: null
      };

    case LocationMasterActionTypes.SAVE_LOCATION_DETAILS_FAILURE:
    case LocationMasterActionTypes.SAVE_FORM_DETAILS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isSaved: false,
        isLoadingLocation: false
      };
    // case LocationMasterActionTypes.SAVE_FORM_DETAILS:
    //   return {
    //     ...state,
    //     isLoadingLocation: true,
    //     isSaved: false,
    //     error: null
    //   };
    // case LocationMasterActionTypes.SAVE_FORM_DETAILS_SUCCESS:
    //   return {
    //     ...state,
    //     location: action.payload,
    //     isSaved: true,
    //     isLoadingLocation: false,
    //     error: null
    //   };
    // case LocationMasterActionTypes.SAVE_FORM_DETAILS_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload,
    //     isSaved: false,
    //     isLoadingLocation: false
    //   };
    case LocationMasterActionTypes.LOAD_OWNER_INFO:
    case LocationMasterActionTypes.LOAD_BASE_CURRENCY:
    case LocationMasterActionTypes.LOAD_REGION:
      return {
        ...state,
        isLoadingLocation: true
      };
    case LocationMasterActionTypes.LOAD_OWNER_INFO_SUCCESS:
      return {
        ...state,
        ownerInfo: action.payload,
        isLoadingLocation: false
      };
    // case LocationMasterActionTypes.LOAD_OWNER_INFO_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload,
    //     isLoadingLocation: false
    //   };
    // case LocationMasterActionTypes.LOAD_REGION:
    //   return {
    //     ...state,
    //     isLoadingLocation: true
    //   };
    case LocationMasterActionTypes.LOAD_REGION_SUCCESS:
      return {
        ...state,
        regions: action.payload,
        isLoadingLocation: false
      };
    // case LocationMasterActionTypes.LOAD_REGION_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload,
    //     isLoadingLocation: false
    //   };
    case LocationMasterActionTypes.COPY_DETAILS:
      return {
        ...state,
        error: null,
        isCopySuccess: false
      };
    case LocationMasterActionTypes.COPY_DETAILS_SUCCESS:
      return {
        ...state,
        isCopySuccess: true,
        error: null
      };

    case LocationMasterActionTypes.COPY_DETAILS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isCopySuccess: false
      };

    // case LocationMasterActionTypes.LOAD_BASE_CURRENCY:
    //   return {
    //     ...state,
    //     isLoadingLocation: true
    //   };

    case LocationMasterActionTypes.LOAD_BASE_CURRENCY_SUCCESS:
      return {
        ...state,
        baseCurrencyTypes: action.payload,
        isLoadingLocation: false,
        error: null
      };

    // case LocationMasterActionTypes.LOAD_BASE_CURRENCY_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload,
    //     isLoadingLocation: false
    //   };
    case LocationMasterActionTypes.RESET_ISSAVED:
      return {
        ...state,
        isSaved: false
      };
    case LocationMasterActionTypes.RESET_ISCOPIED:
      return {
        ...state,
        isCopySuccess: false
      };
    default:
      return state;
  }
}
