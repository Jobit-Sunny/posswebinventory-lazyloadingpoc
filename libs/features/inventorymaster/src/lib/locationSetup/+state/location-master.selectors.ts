import { createSelector, select } from '@ngrx/store';
import { selectInventoryMasters } from '../../inventorymasters.state';
import { locationSelector } from './location-master.entity';

/**
 * The selectors for Stock-Receive store
 */

const locationListingSelector = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.locationListing
);

const selectLoadedLocationList = createSelector(
  locationListingSelector,
  locationSelector.selectAll
);
const selectTotalCount = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.totalCount
);
const selectLocationListingLoading = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.isLoadingLocationListing
);
const selectLocationLoading = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.isLoadingLocation
);
const selectIsNewLocation = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.isNewLocation
);

const selectNewLocation = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.location
);

const selectLocation = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.location
);

const selectStates = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.stateTypes
);
const selectTowns = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.towns
);
const selectLocationTypes = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.locationTypes
);
const selectBrands = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.brands
);

const selectMarketCode = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.marketTypes
);

const selectOwnerInfo = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.ownerInfo
);
const selectRegions = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.regions
)

const selectCountry = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.country
)

const selectBaseCurrencyTypes = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.baseCurrencyTypes
);

const selectIscopySuccess = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.isCopySuccess
)
const selectError = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.error
)
const selectIssearchElements = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.isSearchElements
)

const selectIsSaved = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.isSaved
)



const selectPriceGroup = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.priceGroup
)
const selectPaymentCode = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.paymentCode
)

const selectLocationFormat = createSelector(
  selectInventoryMasters,
  state => state.locationListingState.locationFormat
)






// const nonVerifiedItems = createSelector(
//   selectInventory,
//   state => state.stockReceive.nonVerifiedItems
// );

// const selectIsNonVerifiedItemsLoading = createSelector(
//   selectInventory,
//   state => state.stockReceive.isNonVerifiedItemsLoading
// );

export const LocationMasterSelectors = {
  selectLoadedLocationList,
  selectLocationListingLoading,
  selectLocationLoading,
  selectTotalCount,
  selectIsNewLocation,
  selectNewLocation,
  selectLocation,
  selectStates,
  selectTowns,
  selectOwnerInfo,
  selectLocationTypes,
  selectRegions,
  selectBaseCurrencyTypes,
  selectMarketCode,
  selectBrands,
  selectIscopySuccess,
  selectError,
  selectIssearchElements,
  selectCountry,
  selectIsSaved,
  selectPriceGroup,
  selectPaymentCode,
  selectLocationFormat
};
