import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { LocationMaster } from '../models/locatonmodel';



export interface LocationListingEntity extends EntityState<LocationMaster> { }

export const locationListAdapter = createEntityAdapter<LocationMaster>({
  selectId: locationList => locationList.locationCode
});

export const locationSelector = locationListAdapter.getSelectors();




