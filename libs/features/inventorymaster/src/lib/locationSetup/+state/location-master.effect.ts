import { CustomErrorAdaptor } from '@poss-web/core';

import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';

import { map, tap } from 'rxjs/operators';

import { DataPersistence } from '@nrwl/angular';
import { NotificationService, CustomErrors } from '@poss-web/core';
import { HttpErrorResponse } from '@angular/common/http';
import {
  LocationMasterActionTypes,
  LocationListing,
  StateTypes,
  BaseCurrencyTypes,
  Country,
  PriceGroup,
  LocationFormat,
  PaymentMode
} from './location-master.actions';
import * as LocationMasterActions from './location-master.actions';
import { LocationServiceService } from '../services/location-service.service';
import { Action } from 'rxjs/internal/scheduler/Action';
import { LocationMaster } from '../models/locatonmodel';

/**
 * Location Master Effects
 */
@Injectable()
export class LocationMasterEffect {
  constructor(
    private dataPersistence: DataPersistence<any>,
    private notificationService: NotificationService,
    private locationService: LocationServiceService
  ) { }
  /**
   *  The effect which handles the loadPendingFactorySTN Action
   */

  @Effect()
  loadLocationListing$ = this.dataPersistence.fetch(
    LocationMasterActionTypes.LOAD_LOCAITON_LISTING,
    {
      run: (action: LocationMasterActions.LoadLocationListing) => {
        return this.locationService
          .getLocationDetails(action.payload.pageIndex, action.payload.pageSize)
          .pipe(
            map(
              (locationList: LocationListing) =>
                new LocationMasterActions.LoadLocationListingSuccess(
                  locationList
                )
            )
          );
      },
      onError: this.dispatchLocationFailure()
    }
  );

  @Effect()
  loadLocationByLocationCode$ = this.dataPersistence.fetch(
    LocationMasterActionTypes.LOAD_LOCATION_BY_LOCATION_CODE,
    {
      run: (action: LocationMasterActions.LoadLocationByLocationCode) => {
        return this.locationService
          .getLocationDetailsByLocationCode(action.payload)
          .pipe(
            map(
              (locationDetailByLocationCode: LocationMaster) =>
                new LocationMasterActions.LoadLocationByLocationCodeSuccess(
                  locationDetailByLocationCode
                )
            )
          );
      },
      onError: (
        action: LocationMasterActions.LoadLocationByLocationCode,
        error: HttpErrorResponse
      ) => {
        return new LocationMasterActions.LoadLocationByLocationCodeFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  saveLocationDetails$ = this.dataPersistence.pessimisticUpdate(
    LocationMasterActionTypes.SAVE_LOCATION_DETAILS,
    {
      run: (action: LocationMasterActions.SaveLocationDetails) => {
        return this.locationService.saveLocationDetails(action.payload).pipe(
          map((locationDetailByLocationCode: LocationMaster) => {
            return new LocationMasterActions.SaveLocationDetailsSuccess(
              locationDetailByLocationCode
            );
          })
        );
      },

      onError: (
        action: LocationMasterActions.SaveLocationDetails,
        error: HttpErrorResponse
      ) => {
        return new LocationMasterActions.SaveLocationDetailsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  saveFormDetails$ = this.dataPersistence.pessimisticUpdate(
    LocationMasterActionTypes.SAVE_FORM_DETAILS,
    {
      run: (action: LocationMasterActions.SaveFormDetails) => {
        return this.locationService.saveFormDetails(action.payload).pipe(
          map((locationDetailByLocationCode: LocationMaster) => {
            return new LocationMasterActions.SaveFormDetailsSuccess(
              locationDetailByLocationCode
            );
          })
        );
      },
      onError: (
        action: LocationMasterActions.SaveFormDetails,
        error: HttpErrorResponse
      ) => {
        return new LocationMasterActions.SaveFormDetailsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadNewLocation$ = this.dataPersistence.fetch(
    LocationMasterActionTypes.LOAD_NEW_LOCATION_ACTION,
    {
      run: (action: LocationMasterActions.LoadNewLocationAction) => {
        return new LocationMasterActions.LoadLocationByLocationCodeSuccess(
          this.locationService.getNewLocationFeilds()
        );
      },
      onError: this.dispatchLocationFailure()
    }
  );

  @Effect()
  loadLocationTypes$ = this.dataPersistence.fetch(
    LocationMasterActionTypes.LOAD_LOCATION_TYPES,
    {
      run: (action: LocationMasterActions.LoadLocationTypes) => {
        return this.locationService
          .getLocationTypes()
          .pipe(
            map(
              (locationTypes: any) =>
                new LocationMasterActions.LoadLocationTypesSuccess(
                  locationTypes
                )
            )
          );
      },
      onError: (
        action: LocationMasterActions.LoadLocationTypes,
        error: HttpErrorResponse
      ) => {
        return new LocationMasterActions.LoadLocationTypesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadStates$ = this.dataPersistence.fetch(
    LocationMasterActionTypes.LOAD_STATES,
    {
      run: (action: LocationMasterActions.LoadStates) => {
        return this.locationService
          .getPersonalStatesData()
          .pipe(
            map(
              (states: any) =>
                new LocationMasterActions.LoadStatesSuccess(states)
            )
          );
      },
      onError: (
        action: LocationMasterActions.LoadStates,
        error: HttpErrorResponse
      ) => {
        return new LocationMasterActions.LoadStatesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadTowns$ = this.dataPersistence.fetch(
    LocationMasterActionTypes.LOAD_TOWNS,
    {
      run: (action: LocationMasterActions.LoadTowns) => {
        return this.locationService
          .getPersonalTownsData()
          .pipe(
            map(
              (towns: any) => new LocationMasterActions.LoadTownsSuccess(towns)
            )
          );
      },
      onError: this.dispatchLocationFailure()
    }
  );

  @Effect()
  loadOwnerInfo$ = this.dataPersistence.fetch(
    LocationMasterActionTypes.LOAD_OWNER_INFO,
    {
      run: (action: LocationMasterActions.LoadOwnerInfo) => {
        return this.locationService
          .getOwnerTypeList()
          .pipe(
            map(
              (ownerInfo: any) =>
                new LocationMasterActions.LoadOwnerInfoSuccess(ownerInfo)
            )
          );
      },
      onError: (
        action: LocationMasterActions.LoadOwnerInfo,
        error: HttpErrorResponse
      ) => {
        return new LocationMasterActions.LoadTownsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadRegions$ = this.dataPersistence.fetch(
    LocationMasterActionTypes.LOAD_REGION,
    {
      run: (action: LocationMasterActions.LoadRegion) => {
        return this.locationService
          .getRegionList()
          .pipe(
            map(
              (region: any) =>
                new LocationMasterActions.LoadRegionSuccess(region)
            )
          );
      },
      onError: (
        action: LocationMasterActions.LoadRegion,
        error: HttpErrorResponse
      ) => {
        return new LocationMasterActions.LoadRegionFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadBrands$ = this.dataPersistence.fetch(
    LocationMasterActionTypes.LOAD_BRAND,
    {
      run: (action: LocationMasterActions.LoadBrand) => {
        return this.locationService
          .getBrandList()
          .pipe(
            map(
              (brands: any) =>
                new LocationMasterActions.LoadBrandSuccess(brands)
            )
          );
      },
      onError: (
        action: LocationMasterActions.LoadBrand,
        error: HttpErrorResponse
      ) => {
        return new LocationMasterActions.LoadBrandFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  copyLocationDetails$ = this.dataPersistence.fetch(
    LocationMasterActionTypes.COPY_DETAILS,
    {
      run: (action: LocationMasterActions.CopyDetails) => {
        return this.locationService
          .copyLocationDetail(
            action.payload.oldLocationCode,
            action.payload.newLocationCode
          )
          .pipe(map(() => new LocationMasterActions.CopyDetailsSuccess()));
      },
      onError: (
        action: LocationMasterActions.CopyDetails,
        error: HttpErrorResponse
      ) => {
        return new LocationMasterActions.CopyDetailsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadMarketCode$ = this.dataPersistence.fetch(
    LocationMasterActionTypes.LOAD_MARKET_CODE,
    {
      run: (action: LocationMasterActions.LoadMarketCode) => {
        return this.locationService
          .getMarketCodeData()
          .pipe(
            map(
              (marketCode: any) =>
                new LocationMasterActions.LoadMarketCodeSuccess(marketCode)
            )
          );
      },
      onError: (
        action: LocationMasterActions.LoadMarketCode,
        error: HttpErrorResponse
      ) => {
        return new LocationMasterActions.LoadMarketCodeFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadBaseCurrency$ = this.dataPersistence.fetch(
    LocationMasterActionTypes.LOAD_BASE_CURRENCY,
    {
      run: (action: LocationMasterActions.LoadBaseCurrency) => {
        return this.locationService
          .getBaseCurrencyData()
          .pipe(
            map(
              (baseCurrency: any) =>
                new LocationMasterActions.LoadBaseCurrencySuccess(baseCurrency)
            )
          );
      },
      onError: (
        action: LocationMasterActions.LoadBaseCurrency,
        error: HttpErrorResponse
      ) => {
        return new LocationMasterActions.LoadBaseCurrencyFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  searchLocationByLocationCode$ = this.dataPersistence.fetch(
    LocationMasterActionTypes.SEARCH_LOCATION_BY_LOCATIONCODE,
    {
      run: (action: LocationMasterActions.SearchLocationByLocationCode) => {
        return this.locationService
          .searchLocationByLocationCode(action.payload)
          .pipe(
            map(
              (locationList: LocationListing) =>
                new LocationMasterActions.SearchLocationByLocationCodeSuccess(
                  locationList
                )
            )
          );
      },
      onError: (
        action: LocationMasterActions.SearchLocationByLocationCode,
        error: HttpErrorResponse
      ) => {
        return new LocationMasterActions.SearchLocationByLocationCodeFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadCountry$ = this.dataPersistence.fetch(
    LocationMasterActionTypes.LOAD_COUNTRY,
    {
      run: (action: LocationMasterActions.LoadCountry) => {
        return this.locationService
          .getCountry()

          .pipe(
            map(
              (country: Country[]) =>
                new LocationMasterActions.LoadCountrySuccess(country)
            )
          );
      },
      onError: (
        action: LocationMasterActions.LoadCountry,
        error: HttpErrorResponse
      ) => {
        return new LocationMasterActions.LoadCountryFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadPriceGroup$ = this.dataPersistence.fetch(
    LocationMasterActionTypes.LOAD_COUNTRY,
    {
      run: (action: LocationMasterActions.LoadPriceGroup) => {
        return this.locationService
          .getPriceGroup()

          .pipe(
            map(
              (priceGroup: PriceGroup[]) =>
                new LocationMasterActions.LoadPriceGroupSuccess(priceGroup)
            )
          );
      },
      onError: (
        action: LocationMasterActions.LoadPriceGroup,
        error: HttpErrorResponse
      ) => {
        return new LocationMasterActions.LoadPriceGroupFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadLocationFormat$ = this.dataPersistence.fetch(
    LocationMasterActionTypes.LOAD_LOCATION_FORMAT,
    {
      run: (action: LocationMasterActions.LoadLocationFormat) => {
        return this.locationService
          .getLocationFormat()

          .pipe(
            map(
              (locationFormat: LocationFormat[]) =>
                new LocationMasterActions.LoadLocationFormatSuccess(
                  locationFormat
                )
            )
          );
      },
      onError: (
        action: LocationMasterActions.LoadLocationFormat,
        error: HttpErrorResponse
      ) => {
        return new LocationMasterActions.LoadLocationFormatFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadPaymentCodey$ = this.dataPersistence.fetch(
    LocationMasterActionTypes.LOAD_PAYMENT_CODE,
    {
      run: (action: LocationMasterActions.LoadPaymentCode) => {
        return this.locationService
          .getPaymentCode()

          .pipe(
            map(
              (country: PaymentMode[]) =>
                new LocationMasterActions.LoadPaymentCodeSuccess(country)
            )
          );
      },
      onError: (
        action: LocationMasterActions.LoadPaymentCode,
        error: HttpErrorResponse
      ) => {
        return new LocationMasterActions.LoadPaymentCodeFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }


  private dispatchLocationFailure(): (a: any, e: any) => any {
    return (action: any, error: HttpErrorResponse) => {
      return new LocationMasterActions.LoadLocationListingFailure(
        this.errorHandler(error)
      );
    };
  }
}

