import { CustomErrors } from '@poss-web/core';

import { LocationListingEntity } from './location-master.entity';
import { LocationMaster } from '../models/locatonmodel';

import {
  LocationListingResult,
  SaveLocationDetailsPayload,
  StateTypes,
  LocationTypes,
  Towns,
  OwnerTypes,
  Regions,
  BaseCurrencyTypes,
  MarketCodeTypes,
  BrandName,
  Country,
  PaymentMode,
  PriceGroup,
  LocationFormat
} from './location-master.actions';
// import { LocationListing } from './location-master.actions';

export interface LocationMasterState {
  locationListing: LocationListingEntity;
  location: LocationMaster;
  isLoadingLocationListing: boolean;
  isLoadingLocation: boolean;
  isNewLocation: boolean;
  totalCount: number;
  error: CustomErrors;
  stateTypes: StateTypes[];
  locationTypes: LocationTypes;
  country: Country[]
  marketTypes: MarketCodeTypes;
  towns: Towns;
  ownerInfo: OwnerTypes;
  regions: Regions[];
  saveMasterDetails: SaveLocationDetailsPayload;
  baseCurrencyTypes: BaseCurrencyTypes[];
  brands: BrandName[];
  isCopySuccess: boolean;
  isSearchElements: boolean;
  isSaved: boolean;
  paymentCode:PaymentMode[];
  priceGroup:PriceGroup[];
  locationFormat:LocationFormat[]
}
