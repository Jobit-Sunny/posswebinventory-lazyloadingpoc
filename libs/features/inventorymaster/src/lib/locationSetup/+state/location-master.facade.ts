import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { LocationMasterSelectors } from './location-master.selectors';
import { State } from '../../inventorymasters.state';
import * as LocationMasterActions from './location-master.actions';
import {
  LocationListPayload,
  SaveLocationDetailsPayload,
  SaveFormDetailsPayload,
  CopyDetailsPayload,
  LoadCourierDetailsListingPayload
} from './location-master.actions';
import { LocationAdpator } from '../adaptors/location.adaptor';
import { map, tap } from 'rxjs/operators';
import { of } from 'rxjs';

/**
 * Stock Receive Facade for accesing Stock-Receive-State
 * */
@Injectable()
export class LocationMasterFacade {
  private location$ = this.store.select(LocationMasterSelectors.selectLocation);

  private newLocation$ = this.store.select(
    LocationMasterSelectors.selectNewLocation
  );

  private isSearchElements$ = this.store.select(
    LocationMasterSelectors.selectIssearchElements
  );
  private erro$ = this.store.select(LocationMasterSelectors.selectError);
  private isNewLocation$ = this.store.select(
    LocationMasterSelectors.selectIsNewLocation
  );

  private locationListing$ = this.store.select(
    LocationMasterSelectors.selectLoadedLocationList
  );

  private locationListingLoading$ = this.store.select(
    LocationMasterSelectors.selectLocationListingLoading
  );
  private locationLoading$ = this.store.select(
    LocationMasterSelectors.selectLocationLoading
  );

  private totalCount$ = this.store.select(
    LocationMasterSelectors.selectTotalCount
  );

  private locationTypes$ = this.store.select(
    LocationMasterSelectors.selectLocationTypes
  );

  private marketCode$ = this.store.select(
    LocationMasterSelectors.selectMarketCode
  );

  private states$ = this.store.select(LocationMasterSelectors.selectStates);
  private towns$ = this.store.select(LocationMasterSelectors.selectTowns);

  private ownerInfo$ = this.store.select(
    LocationMasterSelectors.selectOwnerInfo
  );
  private regions$ = this.store.select(LocationMasterSelectors.selectRegions);

  private baseCurrencyTypes$ = this.store.select(
    LocationMasterSelectors.selectBaseCurrencyTypes
  );
  private brands$ = this.store.select(LocationMasterSelectors.selectBrands);

  constructor(private store: Store<State>) { }
  private isCopySuccess$ = this.store.select(
    LocationMasterSelectors.selectIscopySuccess
  );
  private country$ = this.store.select(LocationMasterSelectors.selectCountry);
  private isSaved$ = this.store.select(LocationMasterSelectors.selectIsSaved);

  private selectLocationFormat$ = this.store.select(
    LocationMasterSelectors.selectLocationFormat
  );
  private selectPaymentCode$ = this.store.select(LocationMasterSelectors.selectPaymentCode);
  private selectPriceGroup$ = this.store.select(LocationMasterSelectors.selectPriceGroup);



  /**
   * Access for the State selectors
   */
  getCountry() {
    return this.country$;
  }
  getIsCopySuccess() {
    return this.isCopySuccess$;
  }
  getPaymentModeRefund() {
    return this.selectPaymentCode$
  }
  getLocationSize() {
    return of([{ id: '1', name: 'LSF' }, { id: '2', name: 'RSF' }]);
  }
  getBrandName() {
    //return of([{ id: '1', name: 'Tanishq' }]);
    return this.brands$;
  }
  getSubBrandName() {
    return this.brands$;
    //return of([{ id: '1', name: 'miya' }, { id: '2', name: 'zoya' }]);
  }
  getRegions() {
    return this.regions$;
  }
  getOwnerInfo() {
    return this.ownerInfo$;
    // return of([{ id: '1', name: 'Titan' }]);
  }
  getLocationFormat() {
    return this.selectLocationFormat$
  }
  getPriceGroupCode() {
    return this.selectPriceGroup$
  }

  getStates() {
    return this.states$;
  }
  getTowns() {
    return this.towns$;
  }
  getLocationTypes() {
    return this.locationTypes$;
  }

  getMarketCode() {
    return this.marketCode$;
  }

  getBaseCurrencyTypes() {
    return this.baseCurrencyTypes$;
  }

  getLocationByLocationCode() {
    return this.location$;
  }

  getLocationOnlyByLocationCode() {
    return this.location$.pipe(
      map(data => LocationAdpator.getOnlyLocationDetails(data))
    );
  }

  getGhsByLocationCode() {
    return this.location$.pipe(
      map(data => LocationAdpator.getOnlyGHSDetails(data))
    );
  }

  getAdvanceCustomOrder() {
    return this.location$.pipe(
      map(data => LocationAdpator.getOnlyAdvanceCustom(data))
    );
  }
  getPrintByLocationCode() {
    return this.location$.pipe(
      map(data => LocationAdpator.getOnlyPrintDetails(data))
    );
  }

  getGRNIBTByLocationCode() {
    return this.location$.pipe(
      map(data => LocationAdpator.getOnlyGRNIBTDetails(data))
    );
  }

  getLoyalityByLocationCode() {
    return this.location$.pipe(
      map(data => LocationAdpator.getOnlyLoyalityDetails(data))
    );
  }

  // loadNewLocation() {
  //   return this.newLocation$
  // }
  getLocationListing() {
    return this.locationListing$;
  }

  getLocationListingLoading() {
    return this.locationListingLoading$;
  }

  getLocationLoading() {
    return this.locationLoading$;
  }

  getTotalCount() {
    return this.totalCount$;
  }
  getIsNewLocation() {
    return this.isNewLocation$;
  }
  getIsSerchElements() {
    return this.isSearchElements$;
  }
  getIsSaved() {
    return this.isSaved$;
  }
  getError() {
    return this.erro$;
  }
  /**
   * Dispatch Action for loading STN from Factory (L1/L2)
   * @param loadPendingSTNPayload payload with transfer type , pageIndex and pageSize
   */
  loadRegion() {
    this.store.dispatch(new LocationMasterActions.LoadRegion());
  }
  loadOwnerInfo() {
    this.store.dispatch(new LocationMasterActions.LoadOwnerInfo());
  }
  loadBrands() {
    this.store.dispatch(new LocationMasterActions.LoadBrand());
  }
  loadLocationByLocationCode(locationCode) {
    this.store.dispatch(
      new LocationMasterActions.LoadLocationByLocationCode(locationCode)
    );
  }
  loadPendingLocationListing(locationListPayload: LocationListPayload) {
    this.store.dispatch(
      new LocationMasterActions.LoadLocationListing(locationListPayload)
    );
  }

  loadNewLocationAction() {
    this.store.dispatch(new LocationMasterActions.LoadNewLocationAction());
  }

  loadReset() {
    this.store.dispatch(new LocationMasterActions.ResetError());
  }

  loadStates() {
    this.store.dispatch(new LocationMasterActions.LoadStates());
  }
  loadTowns() {
    this.store.dispatch(new LocationMasterActions.LoadTowns());
  }

  loadMarketCode() {
    this.store.dispatch(new LocationMasterActions.LoadMarketCode());
  }

  loadPriceGroup() {
    this.store.dispatch(new LocationMasterActions.LoadPriceGroup());
  }
  loadPaymentCode() {
    this.store.dispatch(new LocationMasterActions.LoadPaymentCode());
  }

  loadLocationFormat() {
    this.store.dispatch(new LocationMasterActions.LoadLocationFormat());
  }



  loadBaseCurrency() {
    this.store.dispatch(new LocationMasterActions.LoadBaseCurrency());
  }

  loadlocationTypes() {
    this.store.dispatch(new LocationMasterActions.LoadLocationTypes());
  }
  saveLocationDetails(saveLocationDetails: SaveLocationDetailsPayload) {
    this.store.dispatch(
      new LocationMasterActions.SaveLocationDetails(saveLocationDetails)
    );
  }
  saveFormDetails(saveFormDetails: SaveFormDetailsPayload) {
    this.store.dispatch(
      new LocationMasterActions.SaveFormDetails(saveFormDetails)
    );
  }
  copyLocationDetails(payload: CopyDetailsPayload) {
    this.store.dispatch(new LocationMasterActions.CopyDetails(payload));
  }

  searchLocation(locationCode: string) {
    this.store.dispatch(
      new LocationMasterActions.SearchLocationByLocationCode(locationCode)
    );
  }
  loadCountry() {
    this.store.dispatch(new LocationMasterActions.LoadCountry());
  }

  resetIsSaved() {
    this.store.dispatch(new LocationMasterActions.ResetIsSaved());
  }

  resetIsCopied() {
    this.store.dispatch(new LocationMasterActions.ResetIsCopied());
  }
}
