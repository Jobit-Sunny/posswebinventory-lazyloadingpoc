import { Action } from '@ngrx/store';
import { CustomErrors } from '@poss-web/core';
import { LocationMaster } from '../models/locatonmodel';

/**
 * The interface for Action payload
 */
export interface LocationListing {
  results: LocationMaster[];
  totalElements: number;
}
export interface CopyDetailsPayload {
  oldLocationCode: string;
  newLocationCode: string;
}
export interface LocationListPayload {
  pageIndex: number;
  pageSize: number;
}

export interface SaveLocationDetailsPayload {
  cfaCodeValue: string,
  address: string;
  countryCode: number;
  brandCode: string;
  configDetails: any;
  contactNo: string;
  // currencyCode:string;
  description: string;
  factoryCodeValue: string;
  //fax: string;
  isActive: boolean;
  locationCode: string;
  locationEmail: string;
  locationFormat: string;
  locationTypeCode: string;
  ownerTypeCode: string;
  phoneNo: string;
  pincode: number;
  regionCode: string;
  registrationNo: string;
  stateCode: number;
  townCode: number;
}
export interface LocationListingResult {
  address: string;
  brandCode: string;
  configDetails: {};
  contactNo: string;
  currencyCode: string;
  description: string;
  factoryCodeValue: string;
  fax: string;
  isActive: boolean;
  locationCode: string;
  locationEmail: string;
  locationFormat: string;
  locationTypeCode: string;
  oldFactoryCode: string;
  ownerTypeCode: string;
  phoneNo: string;
  pincode: number;
  regionCode: string;
  registrationNo: string;
  stateCode: number;
  townCode: number;
}
export interface SaveFormDetailsPayload {
  locationCode: string;
  data: any;
}

export interface StateTypes {
  id: string;
  name: string;
}
export interface Towns {
  id: string;
  name: string;
  state_id: string;
}
export interface LocationTypes {
  code: string;
  value: string;
}

export interface MarketCodeTypes {
  id: string;
  name: string;
}

export interface OwnerTypes {
  id: string;
  name: string;
}
export interface Country {
  id: string;
  name: string;
}


export interface PaymentMode {
  id: string;
  name: string;
}

export interface PriceGroup {
  id: string;
  name: string;
}

export interface LocationFormat {
  id: string;
  name: string;
}

export interface Regions {
  regionCode: string;
  description: string;
  configDetails: number;
  isActive: boolean;
}

export interface BrandName {
  brandCode: string;
  parentBrandCode: string;
}
export interface BaseCurrencyTypes {
  id: string;
  name: string;
}
export interface LoadCourierDetailsListingPayload {
  pageIndex: number;
  pageSize: number;
}

/**
 * The  enum defined for  list of actions for location listing
 */
export enum LocationMasterActionTypes {
  LOAD_LOCAITON_LISTING = '[ Location-listing ] Load location listing',
  LOAD_LOCAITON_LISTING_SUCCESS = '[ Location-listing ] Load location listing Success',
  LOAD_LOCAITON_LISTING_FAILURE = '[ Location-listing ] Load location listing Failure',



  LOAD_LOCATION_BY_LOCATION_CODE = '[Location-listing] Load loaction By loacation Code',
  LOAD_LOCATION_BY_LOCATION_CODE_SUCCESS = '[Location-listing] Load Location By Location Code Success',
  LOAD_LOCATION_BY_LOCATION_CODE_FAILURE = '[Location-listing] Load Location By Location Code Failure',

  COPY_DETAILS = '[Location-listing] Copy Details',
  COPY_DETAILS_SUCCESS = '[Location-listing] Copy Details Success',
  COPY_DETAILS_FAILURE = '[location-listing] Copy Details Failure',

  SEARCH_LOCATION_BY_LOCATIONCODE = '[Location-listing] Search Location By Location Code',
  SEARCH_LOCATION_BY_LOCATIONCODE_SUCCESS = '[Location-listing] Search Location By Location Code Success',
  SEARCH_LOCATION_BY_LOCATIONCODE_FAILURE = '[Location-listing] Search Location By Location Code Failure',

  LOAD_STATES = '[Location-listing] Load States',
  LOAD_STATES_SUCCESS = '[Location-listing] Load States Success',
  LOAD_STATES_FAILURE = '[Location-listing] Load States Failure',



  LOAD_COUNTRY = '[Location-listing] Load Country',
  LOAD_COUNTRY_SUCCESS = '[Location-listing] Load Country Success',
  LOAD_COUNTRY_FAILURE = '[Location-listing] Load Country Failure',

  LOAD_BRAND = '[Location-listing] Load Brands',
  LOAD_BRAND_SUCCESS = '[Location-listing] Load Brands Success',
  LOAD_BRAND_FAILURE = '[Location-listing] Load Brands Failure',

  LOAD_MARKET_CODE = '[ghs-checkout] Load States',
  LOAD_MARKET_CODE_SUCCESS = '[ghs-checkout] Load States Success',
  LOAD_MARKET_CODE_FAILURE = '[ghs-checkout] Load States Failure',

  LOAD_TOWNS = '[Location-listing] Load towns',
  LOAD_TOWNS_SUCCESS = '[Location-listing] Load Towns Success',
  LOAD_TOWNS_FAILURE = '[Location-listing] Load Towns Failure',

  LOAD_OWNER_INFO = '[Location-listing] Load Owner Info',
  LOAD_OWNER_INFO_SUCCESS = '[Location-listing] Load Owner Info Success',
  LOAD_OWNER_INFO_FAILURE = '[Location-listing]Load Owner Info Failure',

  LOAD_LOCATION_TYPES = '[Location-listing] Load Location Types',
  LOAD_LOCATION_TYPES_SUCCESS = '[Location-listing] Load Location Types Success',
  LOAD_LOCAITON_TYPES_FAILURE = '[Location-listing] Load Location Types Failure',

  LOAD_REGION = '[Location-listing] Load Region',
  LOAD_REGION_SUCCESS = '[Location-listing] Load Region Success',
  LOAD_REGION_FAILURE = '[Location-listing] Load Region Failure',

  LOAD_NEW_LOCATION_ACTION = '[Location-listing] Load New Location Action',
  LOAD_NEW_ACTION_SUCCESS = '[Location-listing] Load New Action Success',

  RESET_ERROR = '[ Location-listing ] Reset Error',
  RESET_ISSAVED = '[ Location-listing ] Reset Is Saved',
  RESET_ISCOPIED = '[ Location-listing ] Reset Is Copied',

  SAVE_LOCATION_DETAILS = '[ Location ] SaveLocation Details',
  SAVE_LOCATION_DETAILS_SUCCESS = '[ Location ] Save Location Details Success',
  SAVE_LOCATION_DETAILS_FAILURE = '[ Location] Save Location Details Failure',

  SAVE_FORM_DETAILS = '[ Location ] SaveForm Details',
  SAVE_FORM_DETAILS_SUCCESS = '[ Location ] SaveForm Details Success',
  SAVE_FORM_DETAILS_FAILURE = '[ Location ] SaveForm Details Failure',

  LOAD_BASE_CURRENCY = '[ghs-checkout] Load BaseCurrency',
  LOAD_BASE_CURRENCY_SUCCESS = '[ghs-checkout] Load BaseCurrency Success',
  LOAD_BASE_CURRENCY_FAILURE = '[ghs-checkout] Load BaseCurrency Failure',


  LOAD_PRICE_GROUP = '[advance-custom-order] Load Price Group',
  LOAD_PRICE_GROUP_SUCCESS = '[advance-custom-order] Load Price Group Success',
  LOAD_PRICE_GROUP_FAILURE = '[advance-custom-order] Load Price Group Failure',

  LOAD_PAYMENT_CODE = '[Location] Load Payment Code',
  LOAD_PAYMENT_CODE_SUCCESS = '[Location]  Load Payment Code Success',
  LOAD_PAYMENT_CODE_FAILURE = '[Location]  Load Payment Code Failure',

  LOAD_LOCATION_FORMAT = '[Location] Load Location Format',
  LOAD_LOCATION_FORMAT_SUCCESS = '[Location] Load Location Format Success',
  LOAD_LOCATION_FORMAT_FAILURE = '[Location]  Load Location Format Failure'
}

/**
 * location listing Actions
 */


export class LoadPriceGroup implements Action {
  readonly type = LocationMasterActionTypes.LOAD_PRICE_GROUP;

}

export class LoadPriceGroupSuccess implements Action {
  readonly type = LocationMasterActionTypes.LOAD_PRICE_GROUP_SUCCESS;
  constructor(public payload: PriceGroup[]) { }
}

export class LoadPriceGroupFailure implements Action {
  readonly type = LocationMasterActionTypes.LOAD_PRICE_GROUP_FAILURE;
  constructor(public payload: CustomErrors) { }
}


export class LoadPaymentCode implements Action {
  readonly type = LocationMasterActionTypes.LOAD_PAYMENT_CODE;

}

export class LoadPaymentCodeSuccess implements Action {
  readonly type = LocationMasterActionTypes.LOAD_PAYMENT_CODE_SUCCESS;
  constructor(public payload: PaymentMode[]) { }
}

export class LoadPaymentCodeFailure implements Action {
  readonly type = LocationMasterActionTypes.LOAD_PAYMENT_CODE_FAILURE;
  constructor(public payload: CustomErrors) { }
}



export class LoadLocationFormat implements Action {
  readonly type = LocationMasterActionTypes.LOAD_LOCATION_FORMAT;

}

export class LoadLocationFormatSuccess implements Action {
  readonly type = LocationMasterActionTypes.LOAD_LOCATION_FORMAT_SUCCESS;
  constructor(public payload: LocationFormat[]) { }
}

export class LoadLocationFormatFailure implements Action {
  readonly type = LocationMasterActionTypes.LOAD_LOCATION_FORMAT_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadCountry implements Action {
  readonly type = LocationMasterActionTypes.LOAD_COUNTRY;

}

export class LoadCountrySuccess implements Action {
  readonly type = LocationMasterActionTypes.LOAD_COUNTRY_SUCCESS;
  constructor(public payload: Country[]) { }
}

export class LoadCountryFailure implements Action {
  readonly type = LocationMasterActionTypes.LOAD_COUNTRY_FAILURE;
  constructor(public payload: CustomErrors) { }
}


export class LoadLocationListing implements Action {
  readonly type = LocationMasterActionTypes.LOAD_LOCAITON_LISTING;
  constructor(public payload: LocationListPayload) { }
}

export class LoadLocationListingSuccess implements Action {
  readonly type = LocationMasterActionTypes.LOAD_LOCAITON_LISTING_SUCCESS;
  constructor(public payload: LocationListing) { }
}

export class LoadLocationListingFailure implements Action {
  readonly type = LocationMasterActionTypes.LOAD_LOCAITON_LISTING_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class ResetError implements Action {
  readonly type = LocationMasterActionTypes.RESET_ERROR;
}

export class CopyDetails implements Action {
  readonly type = LocationMasterActionTypes.COPY_DETAILS;
  constructor(public payload: CopyDetailsPayload) { }
}
export class CopyDetailsSuccess implements Action {
  readonly type = LocationMasterActionTypes.COPY_DETAILS_SUCCESS;
  constructor() { }
}

export class CopyDetailsFailure implements Action {
  readonly type = LocationMasterActionTypes.COPY_DETAILS_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class SearchLocationByLocationCode implements Action {
  readonly type = LocationMasterActionTypes.SEARCH_LOCATION_BY_LOCATIONCODE;
  constructor(public payload: string) { }
}

export class SearchLocationByLocationCodeSuccess implements Action {
  readonly type =
    LocationMasterActionTypes.SEARCH_LOCATION_BY_LOCATIONCODE_SUCCESS;
  constructor(public payload: LocationListing) { }
}
export class SearchLocationByLocationCodeFailure implements Action {
  readonly type =
    LocationMasterActionTypes.SEARCH_LOCATION_BY_LOCATIONCODE_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadNewLocationAction implements Action {
  readonly type = LocationMasterActionTypes.LOAD_NEW_LOCATION_ACTION;
}
export class LoadNewActionSuccess implements Action {
  readonly type = LocationMasterActionTypes.LOAD_NEW_ACTION_SUCCESS;
  constructor(public payload: LocationMaster) { }
}

export class LoadLocationByLocationCode implements Action {
  readonly type = LocationMasterActionTypes.LOAD_LOCATION_BY_LOCATION_CODE;
  constructor(public payload: string) { }
}
export class LoadLocationByLocationCodeSuccess implements Action {
  readonly type =
    LocationMasterActionTypes.LOAD_LOCATION_BY_LOCATION_CODE_SUCCESS;
  constructor(public payload: LocationMaster) { }
}

export class LoadLocationByLocationCodeFailure implements Action {
  readonly type =
    LocationMasterActionTypes.LOAD_LOCATION_BY_LOCATION_CODE_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadStates implements Action {
  readonly type = LocationMasterActionTypes.LOAD_STATES;
}

export class LoadStatesSuccess implements Action {
  readonly type = LocationMasterActionTypes.LOAD_STATES_SUCCESS;
  constructor(public payload: StateTypes[]) { }
}

export class LoadStatesFailure implements Action {
  readonly type = LocationMasterActionTypes.LOAD_STATES_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadBaseCurrency implements Action {
  readonly type = LocationMasterActionTypes.LOAD_BASE_CURRENCY;
}

export class LoadBaseCurrencySuccess implements Action {
  readonly type = LocationMasterActionTypes.LOAD_BASE_CURRENCY_SUCCESS;
  constructor(public payload: BaseCurrencyTypes[]) { }
}

export class LoadBaseCurrencyFailure implements Action {
  readonly type = LocationMasterActionTypes.LOAD_BASE_CURRENCY_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadTowns implements Action {
  readonly type = LocationMasterActionTypes.LOAD_TOWNS;
}

export class LoadTownsSuccess implements Action {
  readonly type = LocationMasterActionTypes.LOAD_TOWNS_SUCCESS;
  constructor(public payload: Towns) { }
}

export class LoadTownsFailure implements Action {
  readonly type = LocationMasterActionTypes.LOAD_TOWNS_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadBrand implements Action {
  readonly type = LocationMasterActionTypes.LOAD_BRAND;
}

export class LoadBrandSuccess implements Action {
  readonly type = LocationMasterActionTypes.LOAD_BRAND_SUCCESS;
  constructor(public payload: BrandName[]) { }
}

export class LoadBrandFailure implements Action {
  readonly type = LocationMasterActionTypes.LOAD_BRAND_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadMarketCode implements Action {
  readonly type = LocationMasterActionTypes.LOAD_MARKET_CODE;
}

export class LoadMarketCodeSuccess implements Action {
  readonly type = LocationMasterActionTypes.LOAD_MARKET_CODE_SUCCESS;
  constructor(public payload: MarketCodeTypes) { }
}

export class LoadMarketCodeFailure implements Action {
  readonly type = LocationMasterActionTypes.LOAD_MARKET_CODE_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadLocationTypes implements Action {
  readonly type = LocationMasterActionTypes.LOAD_LOCATION_TYPES;
}

export class LoadLocationTypesSuccess implements Action {
  readonly type = LocationMasterActionTypes.LOAD_LOCATION_TYPES_SUCCESS;
  constructor(public payload: LocationTypes) { }
}

export class LoadLocationTypesFailure implements Action {
  readonly type = LocationMasterActionTypes.LOAD_LOCAITON_TYPES_FAILURE;
  constructor(public payload: CustomErrors) { }
}
export class SaveLocationDetails implements Action {
  readonly type = LocationMasterActionTypes.SAVE_LOCATION_DETAILS;
  constructor(public payload: SaveLocationDetailsPayload) { }
}
export class SaveLocationDetailsSuccess implements Action {
  readonly type = LocationMasterActionTypes.SAVE_LOCATION_DETAILS_SUCCESS;
  constructor(public payload: LocationMaster) { }
}
export class SaveLocationDetailsFailure implements Action {
  readonly type = LocationMasterActionTypes.SAVE_LOCATION_DETAILS_FAILURE;
  constructor(public payload: CustomErrors) { }
}
export class SaveFormDetails implements Action {
  readonly type = LocationMasterActionTypes.SAVE_FORM_DETAILS;
  constructor(public payload: SaveFormDetailsPayload) { }
}
export class SaveFormDetailsSuccess implements Action {
  readonly type = LocationMasterActionTypes.SAVE_FORM_DETAILS_SUCCESS;
  constructor(public payload: LocationMaster) { }
}

export class SaveFormDetailsFailure implements Action {
  readonly type = LocationMasterActionTypes.SAVE_FORM_DETAILS_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadOwnerInfo implements Action {
  readonly type = LocationMasterActionTypes.LOAD_OWNER_INFO;
}
export class LoadOwnerInfoSuccess implements Action {
  readonly type = LocationMasterActionTypes.LOAD_OWNER_INFO_SUCCESS;
  constructor(public payload: OwnerTypes) { }
}

export class LoadOwnerInfoFailure implements Action {
  readonly type = LocationMasterActionTypes.LOAD_OWNER_INFO_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadRegion implements Action {
  readonly type = LocationMasterActionTypes.LOAD_REGION;
}
export class LoadRegionSuccess implements Action {
  readonly type = LocationMasterActionTypes.LOAD_REGION_SUCCESS;
  constructor(public payload: Regions[]) { }
}

export class LoadRegionFailure implements Action {
  readonly type = LocationMasterActionTypes.LOAD_REGION_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class ResetIsSaved implements Action {
  readonly type = LocationMasterActionTypes.RESET_ISSAVED;
}

export class ResetIsCopied implements Action {
  readonly type = LocationMasterActionTypes.RESET_ISCOPIED;
}

/**
 * Stock Receive Actions Type
 */
export type LocationMasterActions =
  | LoadLocationByLocationCode
  | LoadLocationByLocationCodeSuccess
  | LoadLocationByLocationCodeFailure
  | LoadLocationListing
  | LoadLocationListingSuccess
  | LoadLocationListingFailure
  | SearchLocationByLocationCode
  | SearchLocationByLocationCodeSuccess
  | SearchLocationByLocationCodeFailure
  | LoadNewLocationAction
  | LoadNewActionSuccess
  | ResetError
  | SaveLocationDetails
  | SaveLocationDetailsSuccess
  | SaveLocationDetailsFailure
  | SaveFormDetails
  | LoadStates
  | LoadStatesSuccess
  | LoadStatesFailure
  | LoadTowns
  | LoadTownsSuccess
  | LoadTownsFailure
  | LoadLocationTypes
  | LoadLocationTypesSuccess
  | LoadLocationTypesFailure
  | LoadOwnerInfo
  | LoadOwnerInfoSuccess
  | LoadOwnerInfoFailure
  | LoadRegion
  | LoadRegionSuccess
  | LoadRegionFailure
  | CopyDetails
  | CopyDetailsSuccess
  | CopyDetailsFailure
  | SaveFormDetails
  | SaveFormDetailsSuccess
  | SaveFormDetailsFailure
  | LoadBaseCurrency
  | LoadBaseCurrencySuccess
  | LoadBaseCurrencyFailure
  | LoadMarketCode
  | LoadMarketCodeSuccess
  | LoadMarketCodeFailure
  | LoadBrand
  | LoadBrandSuccess
  | LoadBrandFailure
  | LoadCountry
  | LoadCountrySuccess
  | LoadCountryFailure
  | ResetIsSaved
  | ResetIsCopied
  |LoadPriceGroup
  |LoadPriceGroupSuccess
  |LoadPriceGroupFailure
  |LoadPaymentCode
  |LoadPaymentCodeSuccess
  |LoadPaymentCodeFailure
  |LoadLocationFormat
  |LoadLocationFormatSuccess
  |LoadLocationFormatFailure;
