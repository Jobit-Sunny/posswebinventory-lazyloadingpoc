import { Component, OnInit } from '@angular/core';
import { getLocationMasterDashboardRouteUrl, getInventoryMasterDashboardRouteUrl, getProductMasterDashboardRouteUrl, getLovMasterRouterLink } from '../page-route.constants';

@Component({
  selector: 'poss-web-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  locationRouterLink = getLocationMasterDashboardRouteUrl();
  inventoryRouterLink = getInventoryMasterDashboardRouteUrl();
  productMasterRouterLink = getProductMasterDashboardRouteUrl();
  lovMasterRouterLink = getLovMasterRouterLink();

  constructor() { }
  productMaster = false;
  locationMaster = true;
  active = ['active', '', '', '', ''];
  ngOnInit() { }
  locationMasters() {
    this.locationMaster = false;
    this.productMaster = true;
    this.active = ['', 'active', '', '', ''];
  }
  productMasters() {
    this.locationMaster = true;
    this.productMaster = false;
    this.active = ['active', '', '', '', ''];
  }
}
