import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { getCourierDetailsListRouteUrl, getBinGroupRouteUrl, getBinCodeRouteUrl, getInventoryMasterRouteUrl, getWeighttoleranceRouteUrl } from '../../page-route.constants';

@Component({
  selector: 'poss-web-inventory-dashboard',
  templateUrl: './inventory-dashboard.component.html',
  styleUrls: ['./inventory-dashboard.component.scss']
})
export class InventoryDashboardComponent implements OnInit {
  constructor(private router: Router) { }

  ngOnInit() { }

  courierDetail() {
    this.router.navigate([getCourierDetailsListRouteUrl()]);
  }

  binGroupDetails() {
    this.router.navigate([getBinGroupRouteUrl()]);
  }

  binCodeDetails() {
    this.router.navigate([getBinCodeRouteUrl()]);
  }

  backArrow() {
    this.router.navigate([getInventoryMasterRouteUrl()]);
  }
  weightTolerance() {
    this.router.navigate([getWeighttoleranceRouteUrl()]);
  }

}
