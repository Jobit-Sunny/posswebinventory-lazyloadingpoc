import { Validators } from '@angular/forms';
import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';

export class BinGroupMaster extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'Bin Group Code',
    validationErrorMessages: [{ errorType: 'pattern',
    errorMessage: 'pw.corporateTown.binCodelengthError'}]
  })
  @Validation({ validators: [Validators.required,
                             Validators.pattern('^[a-zA-Z_ ]{3,20}$')] })
  @Class({ className: ['col-12'] })
  private BinGroupCode: string;

  @FormField({
    fieldType: FormFieldType.TEXT_AREA,
    label: 'Description',
    validationErrorMessages: [{ errorType: 'pattern',
    errorMessage: 'pw.corporateTown.binGrouplengthError' }]
  })
  @Validation({ validators: [Validators.required,
                             Validators.pattern('^[a-zA-Z\-\_/(/)_ ]{3,250}$')] })
  @Class({ className: ['col-12'] })
  private desctiption: string;

  constructor(id: number, BinGroupCode: string, desctiption: string) {
    super();
    this.id = id;
    this.BinGroupCode = BinGroupCode;
    this.desctiption = desctiption;
  }
}
