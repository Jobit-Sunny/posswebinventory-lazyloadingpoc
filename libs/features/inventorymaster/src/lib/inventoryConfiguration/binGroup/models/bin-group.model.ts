

export interface BinGroupDetails {
  binGroupCode: string;
  description: string;
  isActive: boolean
}

export interface BinGroups{
  binGroupCode: string,
  isActive: boolean
}

export enum BinGroupEnum{
  NEW = 'NEW',
  new = 'new',
  edit = 'edit'
}

