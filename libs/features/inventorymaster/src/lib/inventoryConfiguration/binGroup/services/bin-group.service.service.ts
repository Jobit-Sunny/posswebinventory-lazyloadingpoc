import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { ApiService } from '@poss-web/core';
import { map } from 'rxjs/operators';
import { BinGroupAdaptor } from '../adaptors/bin-group.adaptor';
import {
  LoadBinGroupDetailsListingSuccessPayload,
  LoadBinGroupDetailsListingPayload,
  SaveBinGroupFormDetailsPayload
} from '../+state/bin-group.actions';
import {
  getBinGroupByBinGroupCode,
  getBinGroupSaveFormDetailsUrl,
  getBinGroupEditedFormDetailsUrl,
  getBinGroupDetailsListingUrl
} from '../../../endpoint.constants';
import { BinGroupDetails } from '../models/bin-group.model';

@Injectable({
  providedIn: 'root'
})
export class BinGroupService {
  constructor(private apiService: ApiService) {}

  getbinGroupDetails(
    loadBinGroupDetailsPayload: LoadBinGroupDetailsListingPayload
  ): Observable<LoadBinGroupDetailsListingSuccessPayload> {
    const url = getBinGroupDetailsListingUrl(
      loadBinGroupDetailsPayload.pageIndex,
      loadBinGroupDetailsPayload.pageSize
    );
    return this.apiService
      .get(url)
      .pipe(map(data => BinGroupAdaptor.getBinGroupDetailsListing(data)));
  }

  getBinGroupByBinGroupCode(binGroupCode: string): Observable<BinGroupDetails> {
    const url = getBinGroupByBinGroupCode(binGroupCode);
    return this.apiService.get(url);
  }

  saveBinGroupFormDetails(saveForm: SaveBinGroupFormDetailsPayload) {
    const url = getBinGroupSaveFormDetailsUrl();
    return this.apiService.post(url, saveForm);
  }

  editBinGroupFormDetails(editedForm: SaveBinGroupFormDetailsPayload) {
    const url = getBinGroupEditedFormDetailsUrl(editedForm.binGroupCode);
    return this.apiService.patch(url, editedForm);
  }

  searchBinGroupByBinGroupCode(binGroupCode): Observable<any> {
    const url = getBinGroupByBinGroupCode(binGroupCode);
    return this.apiService
      .get(url)
      .pipe(map(data => BinGroupAdaptor.getSearchDetailsListing(data)));
  }
}
