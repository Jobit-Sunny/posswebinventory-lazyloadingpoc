import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import * as BinGroupActions from './bin-group.actions';
import { SaveBinGroupFormDetailsPayload } from './bin-group.actions';
import { State } from '../../../inventorymasters.state';
import { BinGroupSelectors } from './bin-group.selectors';
import { Observable } from 'rxjs';

@Injectable()
export class BinGroupFacade {
  constructor(private store: Store<State>) {}

  private binGroupDetailsListing$ = this.store.select(BinGroupSelectors.selectLoadedBinGroupListing);

  private binGroupDetailsByBinGroupCode$ = this.store.select(BinGroupSelectors.selectbinGroupDetailsByBinGroupCode);

  private isLoading$ = this.store.select(BinGroupSelectors.selectIsLoading);

  private isBinGroupSaved$ = this.store.select(BinGroupSelectors.selectSaveBinGroupFormResponse);

  private isBinGroupEdited$ = this.store.select(BinGroupSelectors.selectEditBinGroupFormResponse);

  private totalBinGroupDetails$ = this.store.select(BinGroupSelectors.selectTotalBinGroupDetailsCount);

  private binGroupError$ = this.store.select(BinGroupSelectors.selectError);

  private isSearchElements$ = this.store.select(BinGroupSelectors.selectIssearchElements);


  // private SearchElements$ = this.store.select(BinGroupSelectors.selectSearchElements);


  // private hasSearched$ = this.store.select(BinGroupSelectors.hasSearchedElements);



  getBinGroupSaveResponse() {
    return this.isBinGroupSaved$;
  }

  getBinGroupEditResponse() {
    return this.isBinGroupEdited$;
  }

  getisLoading() {
    return this.isLoading$;
  }

  getTotalBinGroupDetails() {
    return this.totalBinGroupDetails$;
  }

  getBinGroupDetailsListing() {
    return this.binGroupDetailsListing$;
  }

  getBinGroupDetailsByBinGroupCode() {
    return this.binGroupDetailsByBinGroupCode$;
  }

  getError() {
    return this.binGroupError$;
  }

  getIsSerchElements() {
    return this.isSearchElements$;
  }

  // getSerchElements() {
  //   return this.SearchElements$;
  // }

  // hasSearchedBinGroup(){
  //   return this.hasSearched$;
  // }

  loadBinGroupDetailsByBinGroupCode(binGroup: string) {
    this.store.dispatch(new BinGroupActions.LoadBinGroupByBinGroupCode(binGroup));
  }

  loadBinGroupDetailsListing(loadbinGroupDetailsListingPayload: BinGroupActions.LoadBinGroupDetailsListingPayload) {
    this.store.dispatch(new BinGroupActions.LoadBinGroupDetails(loadbinGroupDetailsListingPayload));
  }

  resetBinGroupDialogData() {
    this.store.dispatch(new BinGroupActions.ResetBinGroupDialog());
  }

  searchBinGroup(binGroupCode: string){
    this.store.dispatch(new BinGroupActions.SearchByBinGroupCode(binGroupCode));
  }

  clearSearch(){
    this.store.dispatch(new BinGroupActions.SearchClear());
  }


  editBinGroupFormDetails(editFormDetails: SaveBinGroupFormDetailsPayload) {
    this.store.dispatch(new BinGroupActions.EditBinGroupFormDetails(editFormDetails));
  }

  saveBinGroupFormDetails(saveFormDetails: SaveBinGroupFormDetailsPayload) {
    this.store.dispatch(new BinGroupActions.SaveBinGroupFormDetails(saveFormDetails));
  }
}
