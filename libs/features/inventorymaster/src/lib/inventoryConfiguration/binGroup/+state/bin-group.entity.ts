import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { BinGroupDetails } from '../models/bin-group.model';




export interface BinGroupEntity extends EntityState<BinGroupDetails> { }

export const binGrouptAdapter = createEntityAdapter<BinGroupDetails>({
  selectId: binGroup => binGroup.binGroupCode
});

export const binGroupSelector = binGrouptAdapter.getSelectors();

