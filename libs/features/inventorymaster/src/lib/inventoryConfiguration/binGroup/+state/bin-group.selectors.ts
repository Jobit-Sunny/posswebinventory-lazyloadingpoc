import { createSelector } from '@ngrx/store';
import { selectInventoryMasters } from '../../../inventorymasters.state';
import { binGroupSelector } from './bin-group.entity';





const selectBinGroupDetailsListing = createSelector(
  selectInventoryMasters,
  state => state.binGroupState.binGroupDetailsListing
);

const selectLoadedBinGroupListing = createSelector(
  selectBinGroupDetailsListing,
   binGroupSelector.selectAll
);

const selectTotalBinGroupDetailsCount = createSelector(
  selectInventoryMasters,
  state => state.binGroupState.totalBinGroupDetails
);


const selectbinGroupDetailsByBinGroupCode = createSelector(
  selectInventoryMasters,
  state => state.binGroupState.binGroupDetails
);

const selectIsLoading = createSelector(
  selectInventoryMasters,
  state => state.binGroupState.isLoading
);


const selectSaveBinGroupFormResponse = createSelector(
  selectInventoryMasters,
  state => state.binGroupState.saveBinGroupResponses
);

const selectEditBinGroupFormResponse = createSelector(
  selectInventoryMasters,
  state => state.binGroupState.editBinGroupResponses
);

const selectError = createSelector(
selectInventoryMasters,
state => state.binGroupState.error
);

const selectIssearchElements=createSelector(
  selectInventoryMasters,
  state=>state.binGroupState.isSearchElements
)




// const selectSearchElements=createSelector(
//   selectInventoryMasters,
//   state=>state.binGroupState.searchBinGroup
// )

// const hasSearchedElements=createSelector(
//   selectInventoryMasters,
//   state=>state.binGroupState.searchBinGroup
// )

export const BinGroupSelectors = {
  //selectBinGroupDetailsListing,
  selectLoadedBinGroupListing,
  selectbinGroupDetailsByBinGroupCode,
  selectIsLoading,
  selectError,
  selectSaveBinGroupFormResponse,
  selectEditBinGroupFormResponse,
  selectTotalBinGroupDetailsCount,
  selectIssearchElements,
  // selectSearchElements,
  // hasSearchedElements
};
