
import { CustomErrors } from '@poss-web/core';
import { BinGroupDetails } from '../models/bin-group.model';
import { BinGroupEntity } from './bin-group.entity';


export interface BinGroupState {
  error: CustomErrors;
  binGroupDetailsListing: BinGroupEntity;
  binGroupDetails: BinGroupDetails;
  totalBinGroupDetails: number;
  isLoading: boolean;
  saveBinGroupResponses: BinGroupDetails;
  editBinGroupResponses: BinGroupDetails;
  isSearchElements: boolean
}
