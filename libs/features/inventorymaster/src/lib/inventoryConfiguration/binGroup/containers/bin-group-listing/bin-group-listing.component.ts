import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ElementRef,
  ViewChild,
  Output,
  EventEmitter
} from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject, fromEvent } from 'rxjs';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import { PageEvent, MatDialog } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { takeUntil, debounceTime } from 'rxjs/operators';
import { BinGroupDetailsComponent } from '../../components/bin-group-details/bin-group-details.component';
import { TranslateService } from '@ngx-translate/core';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationEventType,
  ErrorEnums
} from '@poss-web/shared';
import { BinGroupFacade } from '../../+state/bin-group.facade';
import { BinGroupDetails, BinGroupEnum } from '../../models/bin-group.model';
import { getInventoryMasterDashboardRouteUrl } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-bin-group-listing',
  templateUrl: './bin-group-listing.component.html',
  styleUrls: ['./bin-group-listing.component.scss']
})
export class BinGroupListingComponent
  implements OnInit, AfterViewInit, OnDestroy {
  binGroupListing$: Observable<BinGroupDetails[]>;
  binGroupCount$: Observable<number>;
  destroy$ = new Subject<null>();
  @Output() formEmit = new EventEmitter<any>();

  binGroupDetailsForm: FormGroup;
  groupBinDetailsByCode$: BinGroupDetails;
  dialogData: BinGroupDetails;
  binGroupCode: string;
  isLoading$: Observable<boolean>;
  binGroupSaveResponse$: Observable<BinGroupDetails>;
  binGroupEditResponse$: Observable<BinGroupDetails>;
  searchElement$: Observable<BinGroupDetails[]>;
  hasSearchedBinGroup$: Observable<BinGroupDetails[]>;
  isSearching$: Observable<boolean>;
  error: CustomErrors;
  noSearchFound: boolean;

  constructor(
    public dialog: MatDialog,
    public router: Router,
    private translate: TranslateService,
    private appsettingFacade: AppsettingFacade,
    private overlayNotification: OverlayNotificationService,
    private binGroupFacade: BinGroupFacade
  ) { }

  binGroupPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };

  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;
  searchForm = new FormGroup({
    searchValue: new FormControl()
  });

  ngOnInit() {
    this.binGroupFacade.resetBinGroupDialogData();
    this.binGroupFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.error = error;
          this.errorHandler(error);
        }
      });

    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        const pageSize = JSON.parse(data);
        this.binGroupPageEvent.pageSize = pageSize;
        this.loadbinGroupDetails();
      });
    this.isLoading$ = this.binGroupFacade.getisLoading();
    this.isSearching$ = this.binGroupFacade.getIsSerchElements();
    this.binGroupListing$ = this.binGroupFacade.getBinGroupDetailsListing();
    this.binGroupCount$ = this.binGroupFacade.getTotalBinGroupDetails();
    this.binGroupFacade
      .getBinGroupDetailsByBinGroupCode()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          this.groupBinDetailsByCode$ = data;
          const dialogRef = this.dialog.open(BinGroupDetailsComponent, {
            width: '500px',
            height: 'auto',
            data: this.groupBinDetailsByCode$
          });

          dialogRef.afterClosed().subscribe(formData => {
            if (formData) {
              this.createBinGroupFormDetails(formData);
            }
          });
        }
      });

    this.isLoading$ = this.binGroupFacade.getisLoading();

    this.binGroupSaveResponse$ = this.binGroupFacade.getBinGroupSaveResponse();
    this.binGroupSaveResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          const key = 'pw.inventoryConfiguration.successMsg';
          this.translate
            .get(key)
            .pipe(takeUntil(this.destroy$))
            .subscribe((translatedMessage: string) => {
              this.overlayNotification.show({
                type: OverlayNotificationType.TIMER,
                message: translatedMessage,
                hasClose: true,
                hasBackdrop: true
              });
              this.loadbinGroupDetails();
            });
        }
      });

    this.binGroupEditResponse$ = this.binGroupFacade.getBinGroupEditResponse();
    this.binGroupEditResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          const key = 'pw.inventoryConfiguration.editSuccessMsg';
          this.translate
            .get(key)
            .pipe(takeUntil(this.destroy$))
            .subscribe((translatedMessage: string) => {
              this.overlayNotification.show({
                type: OverlayNotificationType.TIMER,
                message: translatedMessage,
                hasBackdrop: true
              });
              if (this.searchForm.value.searchValue) {
                this.search(this.searchForm.value.searchValue);
              } else {
                this.loadbinGroupDetails();
              }
            });
        }
      });
  }

  loadbinGroupDetails() {
    this.binGroupFacade.loadBinGroupDetailsListing(this.binGroupPageEvent);
  }

  ngAfterViewInit(): void {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe(event => {
        const searchValue = this.searchForm.value.searchValue;
        if (searchValue) {
          this.search(searchValue);
        } else {
          this.clearSearch();
        }
      });
  }

  search(searchValue: string) {
    if (searchValue.search('^[^`~!@#$%^&*()_+={}[]|\\:;“’<,>.?๐฿ ]*$')) {
      this.noSearchFound = true;
    } else {
      this.binGroupFacade.searchBinGroup(searchValue);
    }
  }

  paginate(pageEvent: PageEvent) {
    this.binGroupPageEvent = pageEvent;
    this.loadbinGroupDetails();
  }

  getBinGroupName(binGroupCode: string) {
    if (binGroupCode !== BinGroupEnum.NEW) {
      this.binGroupFacade.loadBinGroupDetailsByBinGroupCode(binGroupCode);
    } else if (binGroupCode === BinGroupEnum.NEW) {
      const newFormData: BinGroupDetails = {
        binGroupCode: BinGroupEnum.NEW,
        description: '',
        isActive: false
      };
      const dialogRef = this.dialog.open(BinGroupDetailsComponent, {
        width: '500px',
        height: 'auto',
        data: newFormData
      });
      dialogRef.afterClosed().subscribe(data => {
        if (data) {
          this.createBinGroupFormDetails(data);
        }
      });
    }
  }

  createBinGroupFormDetails(data: any) {
    if (data.mode === BinGroupEnum.new) {
      this.binGroupFacade.saveBinGroupFormDetails({
        binGroupCode: data.binGroupCode,
        description: data.description,
        isActive: data.isActive
      });
    } else if (data.mode === BinGroupEnum.edit) {
      this.binGroupFacade.editBinGroupFormDetails({
        binGroupCode: data.binGroupCode,
        description: data.description,
        isActive: data.isActive
      });
    }
  }

  errorHandler(error: CustomErrors) {
    if (error.code === ErrorEnums.ERR_INV_029) {
      return;
    } else {
      this.overlayNotification
        .show({
          type: OverlayNotificationType.ERROR,
          hasClose: true,
          error: error,
          hasBackdrop: true
        })
        .events.pipe(takeUntil(this.destroy$))
        .subscribe((event: OverlayNotificationEventRef) => { });
    }
  }

  backArrow() {
    this.searchForm.reset();
    this.binGroupFacade.resetBinGroupDialogData();
    this.router.navigate([getInventoryMasterDashboardRouteUrl()]);
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
  clearSearch() {
    // this.binGroupFacade.clearSearch();
    this.noSearchFound = false;
    this.searchForm.reset();
    this.loadbinGroupDetails();
  }
  omit_special_char($event: KeyboardEvent) {
    const pattern = /^[-_A-Za-z0-9]$/;
    return pattern.test($event.key);
  }
}
