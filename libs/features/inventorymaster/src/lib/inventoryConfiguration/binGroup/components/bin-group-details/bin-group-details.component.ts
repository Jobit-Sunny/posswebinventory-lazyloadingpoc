import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import { Observable, Subject, from } from 'rxjs';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { BinGroupDetails, BinGroupEnum } from '../../models/bin-group.model';

import { BinGroupMaster } from '../../models/bin-group-master.model';
import { TEMPLATE8 } from '@poss-web/shared';
import { ConfirmDialogComponent } from '../../../../master/common/confirm-dialog/confirm-dialog.component';
import { takeUntil } from 'rxjs/operators';
@Component({
  selector: 'poss-web-bin-group-details',
  templateUrl: './bin-group-details.component.html',
  styleUrls: ['./bin-group-details.component.scss']
})
export class BinGroupDetailsComponent implements OnInit {
  dialogData: BinGroupDetails;
  binGroupDetailsForm: FormGroup;
  groupBinDetails$: Observable<BinGroupDetails>;
  isLoading$: Observable<boolean>;
  binGroupSaveResponse$: Observable<BinGroupDetails>;
  binGroupCode: string;
  destroy$: Subject<null> = new Subject<null>();
  readOnly: boolean;
  binGroupEnum: BinGroupEnum;

  public currentStyle: string[];
  public formFields: any;
  description: string;

  constructor(
    public dialogRef: MatDialogRef<BinGroupDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    fb: FormBuilder,
    public dialog: MatDialog
  ) {
    this.dialogData = data;
  }

  ngOnInit() {
    if (this.dialogData.binGroupCode !== BinGroupEnum.NEW) {
      this.readOnly = true;
    }
    const form = this.prepareSet();
    this.formFields = this.getInputs(form);
    this.currentStyle = this.getCssProp();
  }

  onCreate() {
    let mode = '';
    if (this.dialogData.binGroupCode !== BinGroupEnum.NEW) {
      mode = BinGroupEnum.edit;
    } else {
      mode = BinGroupEnum.new;
    }
    this.dialogRef.close({
      binGroupCode: this.binGroupCode,
      description: this.description,
      isActive: this.dialogData.isActive,
      mode: mode
    });
  }

  onClose() {
    this.dialogRef.close();
  }

  onClear() {
    if (this.dialogData.binGroupCode !== BinGroupEnum.NEW) {
      this.binGroupDetailsForm.controls['1-description'].reset();
    } else {
      this.binGroupDetailsForm.reset();
    }
  }
  prepareSet() {
    const binGroup = new BinGroupMaster(
      1,
      this.dialogData.binGroupCode === 'NEW'
        ? ''
        : this.dialogData.binGroupCode,
      this.dialogData.description
    );
    return binGroup;
  }

  getCssProp() {
    const annot = (BinGroupDetailsComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }
  public setFormConfig() {
    return {
      formName: 'Bin Group Form',
      formDesc: 'Bin Group',
      formTemplate: TEMPLATE8
    };
  }

  addButton(formGroup: FormGroup) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed()
    .pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
          const formValues = formGroup.getRawValue();
          this.binGroupCode = formValues['1-BinGroupCode'];
          this.description = formValues['1-desctiption'];
          this.onCreate();
        }
    });
  }
  deleteButton() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ngOnInit();
      }
    });
  }

  public formGroupCreated(formGroup: FormGroup) {
    if (this.dialogData.binGroupCode !== 'NEW') {
      formGroup.get('1-BinGroupCode').disable({ onlySelf: true });
    }
  }
}
