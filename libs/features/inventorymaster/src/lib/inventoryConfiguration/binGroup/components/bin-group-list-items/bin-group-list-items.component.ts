import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { PageEvent } from '@angular/material';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AppsettingFacade } from '@poss-web/core';

@Component({
  selector: 'poss-web-bin-group-list-items',
  templateUrl: './bin-group-list-items.component.html',
  styleUrls: ['./bin-group-list-items.component.scss']
})
export class BinGroupListItemsComponent implements OnInit,OnDestroy {
  @Input() binGroupDetailsList;
  @Input() count;
  @Input() pageEvent: PageEvent;
  @Output() binGroupName = new EventEmitter<any>();
  destroy$ = new Subject<null>();
  pageSizeOptions: number[] = [];
  @Output() paginator = new EventEmitter<PageEvent>();
  minPageSize = 0;

  constructor(private appSettingFacade: AppsettingFacade) { }

  ngOnInit() {
    this.appSettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.pageSizeOptions = data;
        this.minPageSize = this.pageSizeOptions.reduce((a: number, b: number) =>
          a < b ? a : b
        );
      });
  }

  emitBinGroupName(binGroupCode) {
    this.binGroupName.emit(binGroupCode);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
