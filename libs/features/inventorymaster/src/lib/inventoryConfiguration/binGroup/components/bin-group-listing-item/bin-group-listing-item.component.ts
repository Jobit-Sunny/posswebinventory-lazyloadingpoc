import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'poss-web-bin-group-listing-item',
  templateUrl: './bin-group-listing-item.component.html',
  styleUrls: ['./bin-group-listing-item.component.scss']
})
export class BinGroupListingItemComponent implements OnInit {

  @Input() binGroupDetailsList;
  @Output() binGroupName = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  getBinGroupName(binGroupCode: string) {
    this.binGroupName.emit(binGroupCode);
  }

}
