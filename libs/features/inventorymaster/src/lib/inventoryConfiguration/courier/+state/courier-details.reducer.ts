import {
  CourierDetailsActionTypes,
  CourierDetailsActions
} from './courier-details.actions';
import { CourierDetailsState } from './courier-details.state';
import { courierDetailsAdaptor } from './courier-details.entity';

const initialState: CourierDetailsState = {
  courierDetailsListing: null,
  totalCourierDetails: 0,
  courierDetails: null,
  error: null,
  isSaving: null,
  hasSaved: null,
  hasUpdated: null,
  hasLoaded: null,
  hasSearrched: false,
  selectedLocations: null,
  isLocationMapping: null,
  isLoadingCourierDetails: null
};
export function CourierDetailsReducer(
  state: CourierDetailsState = initialState,
  action: CourierDetailsActions
): CourierDetailsState {
  switch (action.type) {
    case CourierDetailsActionTypes.LOAD_COURIER_DETAILS:
      return {
        ...state,
        hasLoaded: true,
        error: null
      };
    case CourierDetailsActionTypes.LOAD_COURIER_DETAILS_SUCCESS:
      return {
        ...state,
        hasLoaded: false,
        courierDetailsListing: action.payload.courierDetailsListing,
        totalCourierDetails: action.payload.totalElements
      };
    case CourierDetailsActionTypes.LOAD_COURIER_DETAILS_FAILURE:
      return {
        ...state,
        hasLoaded: false,
        error: action.payload
      };
    case CourierDetailsActionTypes.LOAD_COURIER_DETAILS_BASED_ON_COURIERNAME:
      return {
        ...state,
        isLoadingCourierDetails: true
      };
    case CourierDetailsActionTypes.LOAD_COURIER_DETAILS_BASED_ON_COURIERNAME_SUCCESS:
      return {
        ...state,
        courierDetails: action.payload,
        isLoadingCourierDetails: false
      };
    case CourierDetailsActionTypes.LOAD_COURIER_DETAILS_BASED_ON_COURIERNAME_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingCourierDetails: false
      };
    case CourierDetailsActionTypes.RESET_COURIER_DETAILS:
      return {
        ...state,
        courierDetails: null,
        hasSaved: null,
        hasUpdated: null,
        error: null,
        //hasLoaded: null,
        hasSearrched: null,
        isLocationMapping: null
        //sisLoadingCourierDetails: null
      };
    case CourierDetailsActionTypes.SAVE_COURIER_DETAILS:
      return {
        ...state,
        hasSaved: false
      };
    case CourierDetailsActionTypes.SAVE_COURIER_DETAILS_SUCCESS:
      return {
        ...state,
        hasSaved: true
      };
    case CourierDetailsActionTypes.SAVE_COURIER_DETAILS_FAILURE:
      return {
        ...state,
        error: action.payload,
        hasSaved: false
      };
    case CourierDetailsActionTypes.UPDATE_COURIER_DETAILS:
      return {
        ...state,
        hasUpdated: false
      };
    case CourierDetailsActionTypes.UPDATE_COURIER_DETAILS_SUCCESS:
      return {
        ...state,
        hasUpdated: true
      };
    case CourierDetailsActionTypes.UPDATE_COURIER_DETAILS_FAILURE:
      return {
        ...state,
        error: action.payload,
        hasUpdated: false
      };
    case CourierDetailsActionTypes.SEARCH_COURIER_NAME:
      return {
        ...state,
        hasSearrched: null
      };
    case CourierDetailsActionTypes.SEARCH_COURIER_NAME_SUCCESS:
      return {
        ...state,
        hasSearrched: true,
        courierDetailsListing: action.payload
      };
    case CourierDetailsActionTypes.SEARCH_COURIER_NAME_FAILURE:
      return {
        ...state,
        hasSearrched: false,
        error: action.payload,
        courierDetailsListing: null,
        totalCourierDetails: null
      };
    case CourierDetailsActionTypes.UPDATE_COURIER_STATUS:
    case CourierDetailsActionTypes.UPDATE_COURIER_STATUS_SUCCESS:
    case CourierDetailsActionTypes.SELECTED_LOCATIONS:
      return {
        ...state
      };
    // case CourierDetailsActionTypes.UPDATE_COURIER_STATUS_SUCCESS:
    //   return {
    //     ...state
    //     //TODO
    //   };
    case CourierDetailsActionTypes.UPDATE_COURIER_STATUS_FAILURE:
    case CourierDetailsActionTypes.SELECTED_LOCATIONS_FAILURE:

      return {
        ...state,
        error: action.payload
        //TODO
      };
    // case CourierDetailsActionTypes.SELECTED_LOCATIONS:
    //   return {
    //     ...state
    //   };
    case CourierDetailsActionTypes.SELECTED_LOCATIONS_SUCCEESS:
      return {
        ...state,
        selectedLocations: action.payload
      };
    // case CourierDetailsActionTypes.SELECTED_LOCATIONS_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload
    //   };
    case CourierDetailsActionTypes.LOCATION_MAPPING:
    case CourierDetailsActionTypes.LOCATION_MAPPING_FAILURE:
      return {
        ...state,
        isLocationMapping: false
      };
    case CourierDetailsActionTypes.LOCATION_MAPPING_SUCCESS:
      return {
        ...state,
        isLocationMapping: true
      };
    // case CourierDetailsActionTypes.LOCATION_MAPPING_FAILURE:
    //   return {
    //     ...state,
    //     isLocationMapping: false
    //   };

    default:
      return state;
  }
}
