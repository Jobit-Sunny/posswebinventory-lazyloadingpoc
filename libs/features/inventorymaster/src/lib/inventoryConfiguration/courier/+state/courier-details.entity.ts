import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { CourierDetails } from '../models/courier-details.model';

export interface CourierDetailsEntity extends EntityState<CourierDetails> {}
export const courierDetailsAdaptor = createEntityAdapter<CourierDetails>({
  selectId: courierDetails => courierDetails.courierName
});

export const CourierSelector = courierDetailsAdaptor.getSelectors();
