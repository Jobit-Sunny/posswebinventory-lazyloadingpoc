import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as CourierDetailsActions from './courier-details.actions';
import {
  LoadCourierDetailsListingPayload,
  SaveOrUpdateCourierDetailsPayload,
  LocationMappingPayload
} from './courier-details.actions';
import { State } from '../../../inventorymasters.state';
import { CourierDetailsSelectors } from './courier-details.selectors';

@Injectable()
export class CourierDetailsFacade {
  constructor(private store: Store<State>) {}
  private courierDetailsListing$ = this.store.select(
    CourierDetailsSelectors.selectCourierDetailsListing
  );
  private totalCourierDetails$ = this.store.select(
    CourierDetailsSelectors.selectTotalCourierDetailsCount
  );
  private courierDetailsBasedOnCourierName$ = this.store.select(
    CourierDetailsSelectors.selectCourierDetailsBasedOnCourierName
  );
  private hasSaved$ = this.store.select(CourierDetailsSelectors.selectHasSaved);
  private hasUpdated$ = this.store.select(
    CourierDetailsSelectors.selectHasUpdated
  );
  private error$ = this.store.select(CourierDetailsSelectors.selectError);
  private hasLoaded$ = this.store.select(
    CourierDetailsSelectors.selectHasLoaded
  );
  private hasSearched$ = this.store.select(
    CourierDetailsSelectors.selectHasSearched
  );
  private selectedLocations$ = this.store.select(
    CourierDetailsSelectors.selectSelectedLocations
  );
  private isLocationMapping$ = this.store.select(
    CourierDetailsSelectors.selecIsLocationMapping
  );
  private isLoadingCourierDetails$ = this.store.select(
    CourierDetailsSelectors.selecIsLoadingCourierDetails
  );

  getCourierDetailsListing() {
    return this.courierDetailsListing$;
  }
  getTotalCourierDetails() {
    return this.totalCourierDetails$;
  }
  getCourierDetailsBasedOnCourierName() {
    return this.courierDetailsBasedOnCourierName$;
  }

  getHasSaved() {
    return this.hasSaved$;
  }
  getHasUpdated() {
    return this.hasUpdated$;
  }
  getError() {
    return this.error$;
  }
  getHasLoaded() {
    return this.hasLoaded$;
  }
  getHasSearched() {
    return this.hasSearched$;
  }
  getSelectedLocations() {
    return this.selectedLocations$;
  }
  getIsLocationMapping() {
    return this.isLocationMapping$;
  }
  getIsLoadingCourierDetails() {
    return this.isLoadingCourierDetails$;
  }
  loadCourierDetailsListing(
    loadCourierDetailsListingPayload: LoadCourierDetailsListingPayload
  ) {
    this.store.dispatch(
      new CourierDetailsActions.LoadCourierDetails(
        loadCourierDetailsListingPayload
      )
    );
  }
  loadCourierDetailsBasedOnCourierName(courierName: string) {
    this.store.dispatch(
      new CourierDetailsActions.LoadCourierDetailsBasedOnCourierName(
        courierName
      )
    );
  }
  searchCourierName(courierName: string) {
    this.store.dispatch(
      new CourierDetailsActions.SearchCourierName(courierName)
    );
  }
  resetCourierDetails() {
    this.store.dispatch(new CourierDetailsActions.ResetCourierDetails());
  }
  saveCourierDetails(
    saveCourierDetailsPayload: SaveOrUpdateCourierDetailsPayload
  ) {
    this.store.dispatch(
      new CourierDetailsActions.SaveCourierDetails(saveCourierDetailsPayload)
    );
  }
  updateCourierDetails(
    updateCourierDetails: SaveOrUpdateCourierDetailsPayload
  ) {
    this.store.dispatch(
      new CourierDetailsActions.UpdateCourierDetails(updateCourierDetails)
    );
  }
  updateCourierStatus(updateCourierStatus: SaveOrUpdateCourierDetailsPayload) {
    this.store.dispatch(
      new CourierDetailsActions.UpdateCourierStatus(updateCourierStatus)
    );
  }
  LoadSelectedLocations(courierName: string) {
    this.store.dispatch(
      new CourierDetailsActions.SelectedLocations(courierName)
    );
  }
  LoadMapping(locationMappingPayload: LocationMappingPayload) {
    this.store.dispatch(
      new CourierDetailsActions.LocationMapping(locationMappingPayload)
    );
  }
}
