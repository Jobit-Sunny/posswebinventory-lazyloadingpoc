import { CourierDetails } from '../models/courier-details.model';
import { CustomErrors } from '@poss-web/core';
import { CourierDetailsEntity } from './courier-details.entity';

export interface CourierDetailsState {
  courierDetailsListing: CourierDetails[];
  courierDetails: CourierDetails;
  totalCourierDetails: number;
  error: CustomErrors;
  isSaving: boolean;
  hasSaved: boolean;
  hasUpdated: boolean;
  hasLoaded: boolean;
  hasSearrched: boolean;
  selectedLocations: [];
  isLocationMapping: boolean;
  isLoadingCourierDetails: boolean;
}
