import { createSelector } from '@ngrx/store';
import { selectInventoryMasters } from '../../../inventorymasters.state';
import { CourierSelector } from './courier-details.entity';
const selectCourierDetailsListing = createSelector(
  selectInventoryMasters,
  state => state.courierDetailsState.courierDetailsListing
);
// const selectCourierDetailsListing = createSelector(
//   LoadCourierDetailsListing,
//   CourierSelector.selectAll
// );
const selectTotalCourierDetailsCount = createSelector(
  selectInventoryMasters,
  state => state.courierDetailsState.totalCourierDetails
);
const selectCourierDetailsBasedOnCourierName = createSelector(
  selectInventoryMasters,
  state => state.courierDetailsState.courierDetails
);

const selectHasSaved = createSelector(
  selectInventoryMasters,
  state => state.courierDetailsState.hasSaved
);
const selectHasUpdated = createSelector(
  selectInventoryMasters,
  state => state.courierDetailsState.hasUpdated
);
const selectError = createSelector(
  selectInventoryMasters,
  state => state.courierDetailsState.error
);
const selectHasLoaded = createSelector(
  selectInventoryMasters,
  state => state.courierDetailsState.hasLoaded
);
const selectHasSearched = createSelector(
  selectInventoryMasters,
  state => state.courierDetailsState.hasSearrched
);
const selectSelectedLocations = createSelector(
  selectInventoryMasters,
  state => state.courierDetailsState.selectedLocations
);
const selecIsLocationMapping = createSelector(
  selectInventoryMasters,
  state => state.courierDetailsState.isLocationMapping
);
const selecIsLoadingCourierDetails = createSelector(
  selectInventoryMasters,
  state => state.courierDetailsState.isLoadingCourierDetails
);

export const CourierDetailsSelectors = {
  selectCourierDetailsListing,
  selectTotalCourierDetailsCount,
  selectCourierDetailsBasedOnCourierName,
  selectHasSaved,
  selectHasUpdated,
  selectError,
  selectHasLoaded,
  selectHasSearched,
  selectSelectedLocations,
  selecIsLocationMapping,
  selecIsLoadingCourierDetails
};
