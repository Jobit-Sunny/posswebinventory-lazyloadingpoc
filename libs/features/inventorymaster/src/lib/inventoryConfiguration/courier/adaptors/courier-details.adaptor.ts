import { LoadCourireDetailsListingSuccessPayload } from '../+state/courier-details.actions';
import { CourierDetails } from '../models/courier-details.model';

export class CourierDetailsAdaptor {
  static courierDetailsListing: LoadCourireDetailsListingSuccessPayload;
  static getCourierDetailsListing(
    data: any
  ): LoadCourireDetailsListingSuccessPayload {
    const courierDetailsListing: CourierDetails[] = [];
    for (const listItem of data.results) {
      courierDetailsListing.push({
        courierName: listItem.courierName,
        address: listItem.address,
        stateCode: listItem.stateCode,
        townCode: listItem.townCode,
        mailId: listItem.mailId,
        phoneNumber: listItem.phoneNumber,
        mobileNumber: listItem.mobileNumber,
        contactPerson: listItem.contactPerson,
        isActive: listItem.isActive
      });
    }
    this.courierDetailsListing = {
      courierDetailsListing: courierDetailsListing,
      totalElements: data.totalElements
    };
    return this.courierDetailsListing;
  }

  static getCourierDetails(data: any): CourierDetails[] {
    const courirerDetailsBasedOnCourierName: CourierDetails[] = [];

    courirerDetailsBasedOnCourierName.push({
      courierName: data.courierName,
      address: data.address,
      stateCode: data.stateCode,
      townCode: data.townCode,
      mailId: data.mailId,
      phoneNumber: data.phoneNumber,
      mobileNumber: data.mobileNumber,
      contactPerson: data.contactPerson,
      isActive: data.isActive
    });

    return courirerDetailsBasedOnCourierName;
  }
  static getCourierDetailsBasedOnCourierName(): CourierDetails {
    const courirerDetailsBasedOnCourierName: CourierDetails = {
      courierName: '',
      address: '',
      stateCode: '',
      townCode: '',
      mailId: '',
      phoneNumber: '',
      mobileNumber: '',
      contactPerson: '',
      isActive: false
    };

    return courirerDetailsBasedOnCourierName;
  }
  static getSelectedLocations(
    data: any
  ): { id: string; description: string }[] {
    const selectedLocations: { id: string; description: string }[] = [];
    for (const locationCodes of data) {
      selectedLocations.push({
        id: locationCodes,
        description: locationCodes
      });
    }
    return selectedLocations;
  }
}
