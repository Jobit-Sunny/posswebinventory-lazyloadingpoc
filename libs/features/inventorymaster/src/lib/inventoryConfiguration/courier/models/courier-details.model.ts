export interface CourierDetails {
  courierName?: string;
  address?: string;
  stateCode?: string;
  townCode?: string;
  mailId?: string;
  phoneNumber?: string;
  mobileNumber?: string;
  contactPerson?: string;
  isActive?: boolean;
}
