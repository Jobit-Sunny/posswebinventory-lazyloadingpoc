import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'poss-web-courier-details-listing-item',
  templateUrl: './courier-details-listing-item.component.html'
})
export class CourierDetailsListingItemComponent implements OnInit {
  constructor() {}
  @Input() courierDetailsList;
  @Output() courierName = new EventEmitter<any>();
  @Output() isActive = new EventEmitter<{
    isActive: boolean;
    courierName: string;
  }>();
  ngOnInit() {}
  getCourierName(courierName: string) {
    this.courierName.emit(courierName);
  }
  change(isActive: boolean) {
    this.isActive.emit({
      isActive: isActive,
      courierName: this.courierDetailsList.courierName
    });
  }
}
