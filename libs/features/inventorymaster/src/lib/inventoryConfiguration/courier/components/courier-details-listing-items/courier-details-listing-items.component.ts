


import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { PageEvent } from '@angular/material';
import { Subject } from 'rxjs';

@Component({
  selector: 'poss-web-courier-details-listing-items',
  templateUrl: './courier-details-listing-items.component.html'
})
export class CourierDetailsListingItemsComponent implements OnInit, OnDestroy {
  @Input() courierDetailsList;
  @Input() count;
  @Input() pageEvent: PageEvent;
  @Output() courierName = new EventEmitter<any>();
  @Output() isActive = new EventEmitter<{
    isActive: boolean;
    courierName: string;
  }>();
  @Input() pageSizeOptions: number[];
  @Input() minPageSize = 0;
  destroy$ = new Subject<null>();

  @Output() paginator = new EventEmitter<PageEvent>();

  constructor() {}

  ngOnInit() {}
  emitCourierName(courierName) {
    this.courierName.emit(courierName);
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
  emitIsActive($event) {
    this.isActive.emit($event);
  }
}
