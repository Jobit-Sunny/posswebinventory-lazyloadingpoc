import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { Observable, Subject, combineLatest } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { HelperFunctions, TEMPLATE5, TEMPLATE9 } from '@poss-web/shared';
import { takeUntil } from 'rxjs/operators';
import { CourierDetails } from '../../models/courier-details.model';
import { CourierDetailsModel } from '../../../../master/models/courier-details.model';
import { CourierDetailsFacade } from '../../+state/courier-details.facade';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../../master/common/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'poss-web-courier-details-stepone',
  template: `
    <poss-web-dynamic-form
      *ngIf="formFields"
      [style]="currentStyle"
      [formFields]="formFields"
      [disabled]="false"
      [enableSubmitOnInvalid]="true"
      [buttonNames]="[
        'pw.courierDetails.cancel',
        'pw.courierDetails.saveAndContinue'
      ]"
      (addForm)="addButton($event)"
      (deleteForm)="deleteButton($event)"
      (formGroupCreated)="formGroupCreated($event)"
    >
    </poss-web-dynamic-form>
  `,
  styles: [``]
})
export class CourierDetailsSteponeComponent implements OnInit, OnDestroy {
  constructor(private hf: HelperFunctions, private dialog: MatDialog) {}
  @Input() courierDetails$: Observable<CourierDetails>;
  @Input() states$: Observable<any>;
  @Input() towns$: Observable<any>;
  destroy$: Subject<null> = new Subject<null>();
  courier: any;
  @Output() courierDetails: EventEmitter<{
    courierName: string;
    address: string;
    stateCode: string;
    townCode: string;
    mailId: string;
    phoneNumber: string;
    mobileNumber: string;
    contactPerson: string;
    isActive: boolean;
  }> = new EventEmitter();
  public currentStyle: string[];
  public formFields: any;
  ngOnInit() {
    combineLatest(this.states$, this.towns$, this.courierDetails$)
      .pipe(takeUntil(this.destroy$))
      .subscribe(results => {
        const form = this.prepareSet(results[0], results[1], results[2]);

        this.formFields = this.getInputs(form);
        this.currentStyle = this.getCssProp();
      });
  }
  prepareSet(states: any, towns: any, courierDetails: CourierDetails) {
    this.courier = courierDetails;
    if (courierDetails) {
      states = this.hf.patchValue(
        states,
        'id',
        'selected',
        courierDetails.stateCode,
        true
      );
    }
    if (courierDetails) {
      towns = this.hf.patchValue(
        towns,
        'id',
        'selected',
        courierDetails.townCode,
        true
      );
    }

    const courierDetailsModel = new CourierDetailsModel(
      1,
      courierDetails
        ? courierDetails.courierName
          ? courierDetails.courierName
          : ''
        : '',
      [
        {
          id: '1',
          name: '',
          checked: courierDetails
            ? courierDetails.isActive
              ? courierDetails.isActive
              : false
            : false
        }
      ],
      courierDetails
        ? courierDetails.address
          ? courierDetails.address
          : ''
        : '',
      states,
      towns,
      courierDetails
        ? courierDetails.mailId
          ? courierDetails.mailId
          : ''
        : '',
      courierDetails
        ? courierDetails.phoneNumber
          ? courierDetails.phoneNumber
          : ''
        : '',
      courierDetails
        ? courierDetails.mobileNumber
          ? courierDetails.mobileNumber
          : ''
        : '',
      courierDetails
        ? courierDetails.contactPerson
          ? courierDetails.contactPerson
          : ''
        : ''
    );
    return courierDetailsModel;
  }
  getCssProp() {
    const annot = (CourierDetailsSteponeComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form: any) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }
  public setFormConfig() {
    return {
      formName: 'Courier Details Form',
      formDesc: 'Add location',
      formTemplate: TEMPLATE9
    };
  }
  addButton(formGroup: FormGroup) {
    // console.log('add', formGroup);
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.courierDetails.emit({
          courierName: formGroup.value['1-courierName'],
          address: formGroup.value['1-address'],
          stateCode: formGroup.value['1-state'],
          townCode: formGroup.value['1-city'],
          mailId: formGroup.value['1-emailId'],
          phoneNumber: formGroup.value['1-phoneNumber'],
          mobileNumber: formGroup.value['1-mobileNumber'],
          contactPerson: formGroup.value['1-contactPerson'],
          isActive: formGroup.value['1-isActive'][0]
        });
      }
    });
  }
  deleteButton(formGroup: FormGroup) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ngOnDestroy();
        this.destroy$ = new Subject<null>();
        this.ngOnInit();
      }
    });
  }
  public formGroupCreated(formGroup: FormGroup) {
    if (this.courier.courierName !== '') {
      // console.log('aaa', this.courier);
      formGroup.get('1-courierName').disable({ onlySelf: true });
    }
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
