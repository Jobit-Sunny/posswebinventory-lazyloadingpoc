import { Injectable } from '@angular/core';
import {
  LoadCourierDetailsListingPayload,
  LoadCourireDetailsListingSuccessPayload,
  SaveOrUpdateCourierDetailsPayload,
  LocationMappingPayload
} from '../+state/courier-details.actions';
import { Observable, of } from 'rxjs';
import { ApiService } from '@poss-web/core';
import {
  getCourierDetailsListingUrl,
  getCourierDetailsBasedOnCourierNameUrl,
  getSaveCourierDetailsUrl,
  getUpdateCourierDetailsUrl,
  getSelectedLocationsUrl,
  getLocationMappingUrl
} from '../../../endpoint.constants';
import { CourierDetailsAdaptor } from '../adaptors/courier-details.adaptor';
import { CourierDetails } from '../models/courier-details.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CourierDetailsService {
  constructor(private apiService: ApiService) {}
  getCourierDetails(
    loadCourierDetailsPayload: LoadCourierDetailsListingPayload
  ): Observable<LoadCourireDetailsListingSuccessPayload> {
    const url = getCourierDetailsListingUrl(
      loadCourierDetailsPayload.pageIndex,
      loadCourierDetailsPayload.pageSize
    );
    // console.log('url in service', url);
    return this.apiService
      .get(url)
      .pipe(map(data => CourierDetailsAdaptor.getCourierDetailsListing(data)));
  }
  getCourierDetailsBasedOnCourierName(
    courierName: string
  ): Observable<CourierDetails> {
    if (courierName === 'new') {
      return of(CourierDetailsAdaptor.getCourierDetailsBasedOnCourierName());
    } else {
      const url = getCourierDetailsBasedOnCourierNameUrl(courierName);
      // console.log('url', url);
      return this.apiService.get(url);
    }
  }
  searchCourierName(courierName: string) {
    const url = getCourierDetailsBasedOnCourierNameUrl(courierName);
    // console.log('url', url);
    return this.apiService
      .get(url)
      .pipe(map(data => CourierDetailsAdaptor.getCourierDetails(data)));
  }
  saveCourierDetails(
    saveCourierDetails: SaveOrUpdateCourierDetailsPayload
  ): Observable<CourierDetails> {
    const url = getSaveCourierDetailsUrl();
    // console.log('save url', url);
    return this.apiService.post(url, saveCourierDetails.courierDetails);
  }
  updateCourierDetails(
    updateCourierDetails: SaveOrUpdateCourierDetailsPayload
  ): Observable<CourierDetails> {
    const url = getUpdateCourierDetailsUrl(updateCourierDetails.courierName);
    return this.apiService.patch(url, updateCourierDetails.courierDetails);
  }
  updateCourierStatus(
    courierName: string,
    isActive: boolean
  ): Observable<CourierDetails> {
    const url = getUpdateCourierDetailsUrl(courierName);
    return this.apiService.patch(url, { isActive: isActive });
  }
  selectedLocations(courierName: string) {
    const url = getSelectedLocationsUrl(courierName);
    return this.apiService
      .get(url)
      .pipe(map(data => CourierDetailsAdaptor.getSelectedLocations(data)));
  }
  locationMapping(locationMappingPayload: LocationMappingPayload) {
    const url = getLocationMappingUrl(locationMappingPayload.courierName);
    return this.apiService.post(url, locationMappingPayload.locationMapping);
  }
}
