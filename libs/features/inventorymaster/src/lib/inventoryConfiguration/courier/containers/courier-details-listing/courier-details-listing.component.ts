import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy
} from '@angular/core';
import { PageEvent } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { Subject, fromEvent, Observable } from 'rxjs';
import { debounceTime, takeUntil, take } from 'rxjs/operators';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import { LocationMasterFacade } from '../../../../locationSetup/+state/location-master.facade';

import { Router, ActivatedRoute } from '@angular/router';
import { CourierDetailsFacade } from '../../+state/courier-details.facade';
import {
  StateTypes,
  Towns
} from '../../../../locationSetup/+state/location-master.actions';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventType,
  OverlayNotificationEventRef,
  ErrorEnums
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { getCourierDetailsListRouteUrl, getInventoryMasterDashboardRouteUrl } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-courier-details-listing',
  templateUrl: './courier-details-listing.component.html'
})
export class CourierDetailsListingComponent
  implements OnInit, AfterViewInit, OnDestroy {
  courierDetailsListing$: Observable<any>;
  totalCourierDetailsCount$: Observable<number>;
  allStates$: Observable<StateTypes[]>;
  allTowns$: Observable<Towns[]>;
  hasLoaded$: Observable<boolean>;
  hasSearched$: Observable<boolean>;
  pageSizeOptions: number[] = [];
  minPageSize = 0;
  hasSearched: Observable<boolean>;
  hasError$: Observable<CustomErrors>;

  constructor(
    private appsettingFacade: AppsettingFacade,
    private courierDetailsFacade: CourierDetailsFacade,
    private router: Router,
    private locationMasterFacade: LocationMasterFacade,
    private overlayNotification: OverlayNotificationService,
    private appSettingFacade: AppsettingFacade,
    private route: ActivatedRoute,
    private translate: TranslateService
  ) { }
  courierDetailsPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };

  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;
  searchForm = new FormGroup({
    searchValue: new FormControl()
  });

  destroy$ = new Subject<null>();
  searchErrorCode: string;

  ngOnInit() {
    this.searchErrorCode = ErrorEnums.ERR_INV_029;

    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        const pageSize = JSON.parse(data);
        this.courierDetailsPageEvent.pageSize = pageSize;
        this.loadCourierDetails();
      });
    this.locationMasterFacade.loadStates();
    this.locationMasterFacade.loadTowns();
    this.courierDetailsListing$ = this.courierDetailsFacade.getCourierDetailsListing();
    this.totalCourierDetailsCount$ = this.courierDetailsFacade.getTotalCourierDetails();
    this.hasLoaded$ = this.courierDetailsFacade.getHasLoaded();
    this.hasSearched$ = this.courierDetailsFacade.getHasSearched();
    this.appSettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.pageSizeOptions = data;
        this.minPageSize = this.pageSizeOptions.reduce((a: number, b: number) =>
          a < b ? a : b
        );
      });
    this.hasError$ = this.courierDetailsFacade.getError();

    this.hasError$
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });
  }

  ngAfterViewInit(): void {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        const searchValue = this.searchForm.value.searchValue;
        if (searchValue)
          this.courierDetailsFacade.searchCourierName(searchValue);
        else this.clearSearch();
      });
  }
  paginate(pageEvent: PageEvent) {
    this.courierDetailsPageEvent = pageEvent;
    this.loadCourierDetails();
  }

  addnew() {
    this.courierDetailsFacade.resetCourierDetails();
    this.router.navigate(
      [getCourierDetailsListRouteUrl('new')]
    );
  }
  clearSearch() {
    this.searchForm.reset();
    this.loadCourierDetails();
  }
  loadCourierDetails() {
    this.courierDetailsFacade.loadCourierDetailsListing(
      this.courierDetailsPageEvent
    );
  }
  getCourierName(courierName: string) {
    this.courierDetailsFacade.resetCourierDetails();
    this.router.navigate(
      [getCourierDetailsListRouteUrl(courierName)]
    );

    this.courierDetailsFacade.loadCourierDetailsBasedOnCourierName(courierName);
  }
  back() {
    this.router.navigate([getInventoryMasterDashboardRouteUrl()]);
    this.courierDetailsFacade.resetCourierDetails();
  }
  isActive($event) {
    // this.courierDetailsFacade.updateCourierDetails({
    //   courierName: $event.courierName,
    //   courierDetails: {
    //     isActive: $event.isActive
    //   }
    // });
    this.courierDetailsFacade.updateCourierStatus({
      courierName: $event.courierName,
      isActive: $event.isActive
    });
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  showNotification(key: string) {
    this.overlayNotification.close();

    this.translate
      .get(key)
      .pipe(take(1))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMsg,
            hasBackdrop: true
          })
          .events.pipe(take(1))
          .subscribe();
      });
  }

  errorHandler(error: CustomErrors) {
    if (error.code === this.searchErrorCode) {
      return;
    }

    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasBackdrop: true,
        hasClose: true,
        error: error
      })
      .events.pipe(take(1))
      .subscribe();
  }
  omit_special_char($event: KeyboardEvent) {
    const pattern = /^[-_A-Za-z0-9]$/;
    return pattern.test($event.key);
  }
}
