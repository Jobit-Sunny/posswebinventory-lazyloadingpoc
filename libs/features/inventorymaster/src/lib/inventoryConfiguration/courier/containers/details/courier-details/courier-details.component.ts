import { LocationMasterFacade } from './../../../../../locationSetup/+state/location-master.facade';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import {
  OverlayNotificationService,
  OverlayNotificationEventType,
  OverlayNotificationType,
  OverlayNotificationEventRef
} from '@poss-web/shared';
import { takeUntil, take } from 'rxjs/operators';
import {
  CustomErrors,
  LocationMappingService,
  LocationMappingServiceResponse
} from '@poss-web/core';
import { TranslateService } from '@ngx-translate/core';
import { CourierDetails } from '../../../models/courier-details.model';
import { Subject, Observable } from 'rxjs';
// import {
//   StateTypes,
//   Towns
// } from 'libs/features/inventorymaster/src/lib/locationSetup/+state/location-master.actions';
import { CourierDetailsFacade } from '../../../+state/courier-details.facade';
import { getCourierDetailsListRouteUrl } from 'libs/features/inventorymaster/src/lib/page-route.constants';

//import { LocationMasterFacade } from 'libs/features/inventorymaster/src/lib/locationSetup/+state/location-master.facade';

@Component({
  selector: 'poss-web-courier-details',
  templateUrl: './courier-details.component.html',
  styleUrls: ['./courier-details.component.scss']
})
export class CourierDetailsComponent implements OnInit, OnDestroy {
  courierName: string;
  destroy$ = new Subject<null>();
  town: any;
  locationStates: any;
  hasSaved: boolean;
  courierDetails$: Observable<CourierDetails>;
  states$: Observable<any>;
  towns$: Observable<any>;
  isNew: boolean;
  page: string;
  locationMappingContentTemplateRef: any;
  selectedLocations: [] = [];
  newlySelectedLocations = [];
  isLocationMapping: boolean;
  removedLocations = [];
  show = false;
  isLoadingCourierDetails$: Observable<boolean>;

  constructor(
    private courierDetailsFacade: CourierDetailsFacade,
    private locationMasterFacade: LocationMasterFacade,
    private route: ActivatedRoute,
    private router: Router,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService,
    private locationMappingService: LocationMappingService
  ) { }

  ngOnInit() {
    this.routerPage();
    this.locationMasterFacade.loadStates();
    this.locationMasterFacade.loadTowns();
    const fromPath = this.route.pathFromRoot[2];
    this.courierName = fromPath.snapshot.params['courierName'];
    this.isLoadingCourierDetails$ = this.courierDetailsFacade.getIsLoadingCourierDetails();

    this.courierDetailsFacade.loadCourierDetailsBasedOnCourierName(
      this.courierName
    );
    if (this.courierName !== 'new') {
      this.show = true;
      this.courierDetailsFacade.LoadSelectedLocations(this.courierName);
    }
    this.courierDetailsFacade
      .getIsLocationMapping()
      .subscribe(isLocationMapping => {
        if (isLocationMapping === true) {
          this.showNotifications(
            'pw.courierDetails.locationMappingSuccessMessage'
          );
        }
      });
    this.courierDetailsFacade
      .getHasUpdated()
      .pipe(takeUntil(this.destroy$))
      .subscribe(hasUpdated => {
        if (hasUpdated) {
          this.showNotifications('pw.courierDetails.updatedSuccessMessage');
        }
      });
    this.courierDetailsFacade
      .getHasSaved()
      .pipe(takeUntil(this.destroy$))
      .subscribe(hasSaved => {
        if (hasSaved) {
          this.showNotifications('pw.courierDetails.savedSuccessMessage');
        }
      });
    this.courierDetailsFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          // console.log('error', error.code);
          this.errorHandler(error);
        }
      });
    this.courierDetailsFacade
      .getSelectedLocations()
      .pipe(takeUntil(this.destroy$))
      .subscribe(selectedLocations => {
        this.selectedLocations = selectedLocations;
      });
    this.courierDetails$ = this.courierDetailsFacade.getCourierDetailsBasedOnCourierName();
    this.states$ = this.locationMasterFacade.getStates();
    this.towns$ = this.locationMasterFacade.getTowns();
    this.courierDetails$.pipe(takeUntil(this.destroy$)).subscribe(data => {
      if (this.courierName === 'NEW' && data.courierName !== 'NEW') {
        this.isNew = false;
        setTimeout(() => {
          this.router.navigate([getCourierDetailsListRouteUrl(this.courierName)]);
        }, 2000);
      }
    });
  }

  back() {
    this.courierDetailsFacade.resetCourierDetails();
    this.router.navigate([getCourierDetailsListRouteUrl()]);
    this.overlayNotification.close();
  }

  routerPage() {
    this.route.paramMap.pipe(takeUntil(this.destroy$)).subscribe(params => {
      this.page = params.get('page');
    });
  }
  saveCourierDetails(courierDetails) {
    if (this.courierName !== 'new') {
      this.courierDetailsFacade.updateCourierDetails({
        courierName: this.courierName,
        courierDetails: {
          address: courierDetails.address,
          contactPerson: courierDetails.contactPerson,
          mailId: courierDetails.mailId,
          mobileNumber: courierDetails.mobileNumber,
          phoneNumber: courierDetails.phoneNumber,
          stateCode: courierDetails.stateCode,
          townCode: courierDetails.townCode,
          isActive: courierDetails.isActive
        }
      });
    } else {
      this.courierDetailsFacade.saveCourierDetails({
        courierDetails: {
          address: courierDetails.address,
          contactPerson: courierDetails.contactPerson,
          courierName: courierDetails.courierName,
          mailId: courierDetails.mailId,
          mobileNumber: courierDetails.mobileNumber,
          phoneNumber: courierDetails.phoneNumber,
          stateCode: courierDetails.stateCode,
          townCode: courierDetails.townCode,
          isActive: courierDetails.isActive
        }
      });
    }
  }
  showNotifications(key) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMessage,
            hasBackdrop: true
          })
          .events.subscribe((eventType: OverlayNotificationEventType) => {
            this.courierDetailsFacade.resetCourierDetails();
            this.router.navigate([getCourierDetailsListRouteUrl()]);
          });
      });
  }

  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasBackdrop: true,
        hasClose: true,
        error: error
      })
      .events.pipe(take(1))
      .subscribe();
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  locationMapping() {
    this.locationMappingService
      .open({
        selectedLocations: this.selectedLocations
      })
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: LocationMappingServiceResponse) => {
        if (res.type === 'apply') {
          this.newlySelectedLocations = [];
          this.removedLocations = [];
          if (res.data.addedLocations) {
            res.data.addedLocations.forEach(Addedlocation => {
              this.newlySelectedLocations.push(Addedlocation.id);
            });
          }
          if (res.data.removedLocations) {
            res.data.removedLocations.forEach(removedLocation => {
              this.removedLocations.push(removedLocation.id);
            });
          }
          this.courierDetailsFacade.LoadMapping({
            courierName: this.courierName,
            locationMapping: {
              addLocations: this.newlySelectedLocations,
              removeLocations: this.removedLocations
            }
          });
        }
      });
  }
}
