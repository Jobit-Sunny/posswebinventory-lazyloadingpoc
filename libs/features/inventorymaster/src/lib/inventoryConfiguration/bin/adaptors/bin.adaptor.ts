import { BinCodeList, BinCodesByBinGroup, BinCodeSaveNewModel, LocationMapping, LocationList } from '../models/bin.model';
import { LoadBinCodeDetailsListingSuccessPayload, LoadBinCodeByBinGroupPayload } from '../+state/bin.actions';

export class BinAdaptor {
  static binCodeDetailsListing: LoadBinCodeDetailsListingSuccessPayload;
  // static binCodesByBinGroupCode: BinCodesByBinGroup[];


  static getBinCodeDetailsListing(
    data: any
  ): LoadBinCodeDetailsListingSuccessPayload {
    const binCodeDetailsListing: BinCodeList[] = [];
    for (const listItem of data.results) {
      binCodeDetailsListing.push({
        locationCode: listItem.locationCode,
        brandCode: listItem.brandCode,
        regionCode: listItem.regionCode,
        isActive: listItem.isActive,
      });
    }
    this.binCodeDetailsListing = {
      binCodeDetailsListing: binCodeDetailsListing,
      totalElements: data.totalElements
    };
    return this.binCodeDetailsListing;
  }

 static binCodesByBinGroup(
    data: any
  ): BinCodesByBinGroup[] {
    const binCodesByBinGroupCode: BinCodesByBinGroup[] = [];
    for (const listItem of data.results) {
      binCodesByBinGroupCode.push({
        binCode: listItem.binCode,
        description: listItem.description,
        isActive: listItem.isActive,
      });
    }
    // this.binCodesByBinGroupCode = {
    //   binCodeDetailsListing: binCodeDetailsListing,
    // };
    return binCodesByBinGroupCode;
  }

static getBinDetailsByBinNameUrl(data: any): BinCodesByBinGroup[] {
  const binDetailsByBinName: BinCodesByBinGroup[] = [];

  binDetailsByBinName.push({
  binCode: data.binCode,
  description: data.binGroups.descritpion,
  isActive: data.binGroups.isActive
  });

  return binDetailsByBinName;
}


static locationsByBinGroupAndCode (data: any): LocationList[]{
     const locationList: LocationList[] = [];
     for (const listItem of data) {
      locationList.push({
       id: listItem.locationCode,
       description: listItem.locationCode
      });
    }
    return locationList;
}
}
