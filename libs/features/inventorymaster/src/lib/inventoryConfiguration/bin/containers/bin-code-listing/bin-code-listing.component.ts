import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy,
  Inject,
  Output,
  EventEmitter,
  Input,
  TemplateRef
} from '@angular/core';
import { Router } from '@angular/router';
import {
  AppsettingFacade,
  CustomErrors,
  LocationMappingService,
  LocationMappingServiceResponse
} from '@poss-web/core';
import {
  PageEvent,
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from '@angular/forms';
import { takeUntil, debounceTime } from 'rxjs/operators';
import { Subject, fromEvent, Observable } from 'rxjs';

import { TranslateService } from '@ngx-translate/core';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationEventType
} from '@poss-web/shared';
import { Location } from '@angular/common';
import {
  BinCodeEditedFormPayload,
  SaveBinCodeFormPayload
} from '../../+state/bin.actions';
import {
  BinCodeSaveModel,
  BinCodesByBinGroup,
  BinCodeSaveNewModel,
  BinCodeList,
  BinCodeEditModel,
  LocationList,
  LocationMappingPost
} from '../../models/bin.model';
import { BinGroupDetails } from '../../../binGroup/models/bin-group.model';
import { BinFacade } from '../../+state/bin.facade';
import { BinGroupFacade } from '../../../binGroup/+state/bin-group.facade';
import { BinCodeDialogComponent } from '../../components/bin-code-dialog/bin-code-dialog.component';
import { ConfirmDialogComponent } from '../../../../master/common/confirm-dialog/confirm-dialog.component';
import { getInventoryMasterDashboardRouteUrl } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-bin-code-listing',
  templateUrl: './bin-code-listing.component.html',
  styleUrls: ['./bin-code-listing.component.scss']
})
export class BinCodeListingComponent
  implements OnInit, AfterViewInit, OnDestroy {
  destroy$ = new Subject<null>();
  binCodeNewFormDetails: SaveBinCodeFormPayload;
  @Output() binGroupDropdown = new EventEmitter<BinGroupDetails[]>();
  binGroupDropdownPopUp: BinGroupDetails[];

  binCodeSaveNewResponse$: Observable<SaveBinCodeFormPayload>;
  binCodeEditResponse$: Observable<BinCodeEditModel>;
  binCodeList$: Observable<BinCodeList[]>;
  editClicked = false;
  mode: string;
  binCodeCount$: Observable<number>;
  locationData: BinCodeList;
  rowClicked = false;
  location: Location;
  readOnly: boolean;
  isBinCodeLoading$: Observable<boolean>;
  binCodeEditedFormDetails: BinCodeSaveModel;
  binCodesByBinGroup$: Observable<BinCodesByBinGroup[]>;
  dropdownSelection: any;
  binCodes: BinCodesByBinGroup[];
  isToggleChanged: boolean;
  error: CustomErrors;
  search$: void;
  clearClicked: boolean;
  binCodesForLocationMapping: string[] = [];
  locationMappingList$: Observable<LocationList[]>;
  selectedLocations: LocationList[];
  locationMappingSelected: boolean;
  mappingData: LocationMappingPost;
  addedLocation: string[] = [];
  removedLocation: string[] = [];
  searchElement: BinCodesByBinGroup[] = [];

  @ViewChild('locationMappingContentTemplate', { static: true })
  locationMappingContentTemplateRef: TemplateRef<any>;

  locationList: { id: string; description: string }[];
  //searchElement: BinCodesByBinGroup[];
  noSearchFound: boolean;
  //defaultDropdownSelection: string;
  locationMappingResponse$: Observable<LocationMappingPost>;
  isLoading$: Observable<boolean>;
  checked: boolean;

  constructor(
    private router: Router,
    public dialog: MatDialog,
    private translate: TranslateService,
    private appsettingFacade: AppsettingFacade,
    private overlayNotification: OverlayNotificationService,
    private binFacade: BinFacade,
    private binGroupFacade: BinGroupFacade,
    private locationMappingService: LocationMappingService
  ) { }

  binCodePageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };

  binGroupPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };

  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;
  searchForm = new FormGroup({
    searchValue: new FormControl()
  });

  ngOnInit() {
    this.noSearchFound = false;
    this.isToggleChanged = false;
    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        const pageSize = JSON.parse(data);
        this.binCodePageEvent.pageSize = pageSize;
      });

    this.binFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.binCodes = [];
          this.error = error;
          this.errorHandler(error);
        }
      });

    this.binGroupFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.error = error;
          this.errorHandler(error);
        }
      });

    this.loadbinCodeDetails();
    // this.binCodeCount$ = this.binFacade.getTotalBinCodeDetails();
    this.isBinCodeLoading$ = this.binFacade.getBinCodeIsLoading();
    this.isLoading$ = this.binGroupFacade.getisLoading();
    // this.binFacade.loadBinCodeDetailsListing(
    //   this.binCodePageEvent
    // );
    //  this.binCodeList$ = this.binFacade.getBinCodeDetailsListing();
    this.binCodesByBinGroup$ = this.binFacade.getBinCodesByBinGroup();
    this.binCodesByBinGroup$.subscribe(data => {
      this.binCodes = data;
    });
    //this.binCodesByBinGroup$ = this.binFacade.getBinCodesByBinGroup();
    this.binFacade.resetBinCodeDialogData();
    this.binGroupFacade
      .getBinGroupDetailsListing()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          this.binGroupDropdownPopUp = data;
        }
      });

    this.binCodeSaveNewResponse$ = this.binFacade.getBinCodeNewSaveResponse();
    this.binCodeSaveNewResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          const key = 'pw.inventoryConfiguration.successMsgBinCode';
          this.translate
            .get(key)
            .pipe(takeUntil(this.destroy$))
            .subscribe((translatedMessage: string) => {
              this.showTimerNotification(translatedMessage);
              this.binFacade.loadBinCodesByBinGroupCode(this.dropdownSelection);
            });
        }
      });

    this.binCodeEditResponse$ = this.binFacade.getBinCodeEditSaveResponse();
    this.binCodeEditResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(result => {
        if (result) {
          if (this.isToggleChanged) {
            this.binFacade.loadBinCodesByBinGroupCode(this.dropdownSelection);
          } else {
            const key = 'pw.inventoryConfiguration.editSuccessMsgBinCode';
            this.translate
              .get(key)
              .pipe(takeUntil(this.destroy$))
              .subscribe((translatedMessage: string) => {
                this.showTimerNotification(translatedMessage);
                this.binFacade.loadBinCodesByBinGroupCode(
                  this.dropdownSelection
                );
              });
          }
        }
      });

    this.locationMappingList$ = this.binFacade.getLocationsByBinCodesAndBinGroup();
    this.locationMappingList$.subscribe(data => {
      this.locationList = data;
      if (this.locationMappingSelected) {
        this.locationMappingService
          .open({
            selectedLocations: this.locationList,
            template: this.locationMappingContentTemplateRef
          })
          .pipe(takeUntil(this.destroy$))
          .subscribe((res: LocationMappingServiceResponse) => {
            //  console.log('LMT apply response ==>', res);
            if (res.type === 'apply') {
              this.selectedLocations = res.data.selectedLocations;
              for (const item of res.data.addedLocations) {
                this.addedLocation = Object.assign([], this.addedLocation);
                this.addedLocation.push(item.id);
              }
              for (const item of res.data.removedLocations) {
                this.removedLocation = Object.assign([], this.removedLocation);
                this.removedLocation.push(item.id);
              }

              this.mappingData = {
                addLocations: this.addedLocation,
                binCodes: this.binCodesForLocationMapping,
                removeLocations: this.removedLocation
              };
              const mappingDetails = {
                binGroup: this.dropdownSelection,
                data: this.mappingData
              };
              this.binFacade.saveLocationMappingDetails(mappingDetails);
            }
            this.addedLocation = [];
            this.removedLocation = [];
          });
      }
    });

    this.locationMappingResponse$ = this.binFacade.getLocationMappingResponse();
    this.locationMappingResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(result => {
        if (result) {
          this.binCodesForLocationMapping = null;
          const key = 'pw.inventoryConfiguration.locationMappingMsg';
          this.translate
            .get(key)
            .pipe(takeUntil(this.destroy$))
            .subscribe((translatedMessage: string) => {
              this.showTimerNotification(translatedMessage);
              this.binFacade.loadBinCodesByBinGroupCode(this.dropdownSelection);
            });
        }
      });
  }

  errorHandler(error: CustomErrors) {
    if (this.dropdownSelection) {
      this.binCodesByBinGroup$
        .pipe(takeUntil(this.destroy$))
        .subscribe(data => {
          if (this.isToggleChanged) {
            this.isToggleChanged = false;
            this.binFacade.loadBinCodesByBinGroupCode(this.dropdownSelection);
          } else if (data !== null) {
            this.binCodes = data;
          } else {
            this.binCodes = null;
          }
        });
    }
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error,
        hasBackdrop: true
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => { });
  }

  ngAfterViewInit(): void {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .pipe(takeUntil(this.destroy$))
      .subscribe(event => {
        let searchValue = '';
        this.noSearchFound = false;
        searchValue = this.searchForm.value.searchValue;
        if (searchValue && this.dropdownSelection) {
          //this.binFacade.searchBinName(searchValue);
          this.binCodesByBinGroup$.subscribe(data => {
            const binCodelist = data;
            if (binCodelist && searchValue !== null) {
              this.searchElement = binCodelist.filter(
                bin => bin.binCode.toLowerCase() === searchValue.toLowerCase()
              );
            }

            if (searchValue !== null && this.searchElement.length === 0) {
              searchValue = null;
              this.searchElement = null;
              this.noSearchFound = true;
            } else if (this.searchElement != null) {
              if (this.searchElement.length > 0) {
                this.noSearchFound = false;
                this.binCodes = this.searchElement;
                this.searchElement = null;
                searchValue = null;
              }
            }
          });
        } else if (searchValue && this.dropdownSelection === undefined) {
          searchValue = null;
          this.searchElement = null;
          this.noSearchFound = true;
        } else if (searchValue === '') {
          this.binCodesByBinGroup$.subscribe(data => {
            this.binCodes = data;
          });
        }
      });
  }

  paginate(pageEvent: PageEvent) {
    this.binCodePageEvent = pageEvent;
    this.binFacade.loadBinCodeDetailsListing(this.binCodePageEvent);
  }

  selectedDropdown(event: any) {
    this.binFacade.resetBinCodeDialogData();
    this.locationMappingSelected = false;
    this.binCodesForLocationMapping = [];
    this.searchForm.reset();
    this.dropdownSelection = event.value;
    this.noSearchFound = false;
    this.searchElement = null;
    if (event.value) {
      this.binFacade.loadBinCodesByBinGroupCode(event.value);
    }
  }

  loadbinCodeDetails() {
    this.binGroupFacade.loadBinGroupDetailsListing(this.binGroupPageEvent);
  }

  addNew() {
    //this.loadbinCodeDetails();
    this.mode = 'new';
    if (this.binGroupDropdownPopUp && this.mode === 'new') {
      const dialogRef = this.dialog.open(BinCodeDialogComponent, {
        width: '500px',
        height: 'auto',
        data: { editData: null }
      });
      if (this.mode === 'new') {
        dialogRef
          .afterClosed()
          .pipe(takeUntil(this.destroy$))
          .subscribe(formdata => {
            if (formdata) {
              this.binCodeNewFormDetails = {
                binCode: formdata.binCode,
                binGroups: [formdata.binGroupCode],
                description: formdata.description
              };
              this.binFacade.saveBinCodeNewFormDetails(
                this.binCodeNewFormDetails
              );
              this.mode = '';
            }
          });
      }
    }
  }

  editDropdownSelection(selectedDropdown: string) {
    this.binFacade.loadBinCodesByBinGroupCode(selectedDropdown);
  }

  onEdit(binCode) {
    const dialogRef = this.dialog.open(BinCodeDialogComponent, {
      width: '500px',
      height: 'auto',
      data: {
        dropDown: this.binGroupDropdownPopUp,
        editData: binCode,
        binGroup: this.dropdownSelection
      }
    });
    dialogRef.afterClosed().subscribe(formdata => {
      if (formdata) {
        this.binFacade.saveBinCodeEditedFormDetails(formdata);
      }
    });
  }

  onToggleChange(event, selectedBinCode) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed().subscribe(formdata => {
      if (formdata) {
        this.isToggleChanged = true;
        const toggleData = {
          binGroups: null,
          binCode: selectedBinCode.binCode,
          binGroupCode: this.dropdownSelection,
          isActive: [event.checked],
          description: selectedBinCode.description
        };
        this.binFacade.saveBinCodeEditedFormDetails(toggleData);
      } else {
        this.binFacade.loadBinCodesByBinGroupCode(this.dropdownSelection);
      }
    });
  }

  onCheckbox(event, selectedBinCode: BinCodeSaveModel) {
    if (event === true) {
      this.binCodesForLocationMapping = Object.assign(
        [],
        this.binCodesForLocationMapping
      );
      this.binCodesForLocationMapping.push(selectedBinCode.binCode);
    } else {
      this.binCodesForLocationMapping = this.binCodesForLocationMapping.filter(
        removeBinCode => removeBinCode !== selectedBinCode.binCode
      );
    }
  }

  onLocationMapping() {
    const payload = {
      binGroup: this.dropdownSelection,
      binCodes: this.binCodesForLocationMapping
    };
    this.locationMappingSelected = true;
    if (this.binCodesForLocationMapping !== null) {
      if (this.binCodesForLocationMapping.length > 0) {
        this.binFacade.loadLocationsByBinCodesAndBinGroup(payload);
      } else {
        const key = 'pw.inventoryConfiguration.locationMappingErrorMsg';
        this.translate
          .get(key)
          .pipe(takeUntil(this.destroy$))
          .subscribe((translatedMessage: string) => {
            this.showSimpleNotification(translatedMessage);
          });
      }
    } else {
      const key = 'pw.inventoryConfiguration.locationMappingErrorMsg';
      this.translate
        .get(key)
        .pipe(takeUntil(this.destroy$))
        .subscribe((translatedMessage: string) => {
          this.showSimpleNotification(translatedMessage);
        });
    }
  }

  showTimerNotification(translatedMessage: string) {
    this.overlayNotification.show({
      type: OverlayNotificationType.TIMER,
      message: translatedMessage,
      hasBackdrop: true
    });
  }

  showSimpleNotification(translatedMessage: string) {
    this.overlayNotification.show({
      type: OverlayNotificationType.SIMPLE,
      message: translatedMessage,
      hasClose: true,
      hasBackdrop: true
    });
  }

  clearSearch() {
    this.noSearchFound = false;
    this.binFacade.resetBinCodeDialogData();
    this.searchForm.reset();
    if (this.dropdownSelection) {
      this.binFacade.loadBinCodesByBinGroupCode(this.dropdownSelection);
    }
  }

  backArrow() {
    this.searchForm.reset();
    this.binGroupFacade.resetBinGroupDialogData();
    this.binFacade.resetBinCodeDialogData();
    this.ngOnDestroy();
    this.router.navigate([getInventoryMasterDashboardRouteUrl()]);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
  omit_special_char($event: KeyboardEvent) {
    const pattern = /^[-_A-Za-z0-9]$/;
    return pattern.test($event.key);
  }
}
