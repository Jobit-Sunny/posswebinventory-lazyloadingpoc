import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ApiService } from '@poss-web/core';
import { map } from 'rxjs/operators';
import {
  SaveBinCodeFormPayload,
  LoadBinCodeDetailsListingSuccessPayload,
  BinCodeEditedFormPayload,
  LoadLocationsByBinGroupAndBinCode,
  LocationsByBinGroupAndBinCodePayload,
  LocationMappingPostPayload
} from '../+state/bin.actions';
import {
  getBinCodeSaveNewFormDetailsUrl,
  getBinCodeEditedFormDetailsUrl,
  getBinCodesByBinGroupCode,
  getBinDetailsByBinNameUrl,
  getLocationsByBinGroupAndBinCode,
  getLocationMappingUpdateUrl
} from '../../../endpoint.constants';
import {
  BinCodesByBinGroup,
  BinCodeEditModel,
  BinCodeSaveModel,
  LocationMapping,
  LocationList
} from '../models/bin.model';
import { BinAdaptor } from '../adaptors/bin.adaptor';
import { LoadBinGroupDetailsListingPayload } from '../../binGroup/+state/bin-group.actions';

@Injectable({
  providedIn: 'root'
})
export class BinService {
  constructor(private apiService: ApiService) {}

  saveBinCodeNewFormDetails(saveForm: SaveBinCodeFormPayload) {
    const url = getBinCodeSaveNewFormDetailsUrl();
    return this.apiService.post(url, saveForm);
  }

  // getbinCodeDetails(
  //   loadBinCodeDetailsPayload: LoadBinGroupDetailsListingPayload
  // ): Observable<LoadBinCodeDetailsListingSuccessPayload> {
  //   const url = getBinCodeDetailsListingUrl(
  //     loadBinCodeDetailsPayload.pageIndex,
  //     loadBinCodeDetailsPayload.pageSize
  //   );
  //   return this.apiService
  //     .get(url)
  //     .pipe(map(data => BinAdaptor.getBinCodeDetailsListing(data)));
  // }

  getBinCodesByBinGroupCode(
    binGroupCode: string
  ): Observable<BinCodesByBinGroup[]> {
    const url = getBinCodesByBinGroupCode(binGroupCode);
    return this.apiService
      .get(url)
      .pipe(map(data => BinAdaptor.binCodesByBinGroup(data)));
  }

  saveBinCodeEditedFormDetails(saveForm) {
    const editedForm = {
      binGroupCode: saveForm.binGroupCode,
      isActive: saveForm.isActive[0],
      description: saveForm.description
    };
    const url = getBinCodeEditedFormDetailsUrl(saveForm.binCode);
    return this.apiService.patch(url, editedForm);
  }

  searchBinName(binName: string) {
    const url = getBinDetailsByBinNameUrl(binName);
    return this.apiService
      .get(url)
      .pipe(map(data => BinAdaptor.getBinDetailsByBinNameUrl(data)));
  }

  getLocationsByBinGroupAndBinCode(
    location: LocationsByBinGroupAndBinCodePayload
  ): Observable<LocationList[]> {
    const url = getLocationsByBinGroupAndBinCode(location);
    return this.apiService
      .get(url)
      .pipe(map(data => BinAdaptor.locationsByBinGroupAndCode(data)));
  }

  saveLocationMapping(saveForm: LocationMappingPostPayload) {
    const url = getLocationMappingUpdateUrl(saveForm.binGroup);
    return this.apiService.patch(url, saveForm.data);
  }
}
