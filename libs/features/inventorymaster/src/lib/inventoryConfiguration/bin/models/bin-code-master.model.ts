import { Validators } from '@angular/forms';
import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';

export class BinCodeMaster extends DynamicFormFieldsBuilder {
  private id: number;


  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'binGroupCode',
      valueKey: 'binGroupCode',
      selectedKey: 'selected'
    },
    label: 'Select bin group'
  })
  @Validation({ validators: [Validators.required] })
  @Class({ className: ['col-12'] })
  private binGroup: { binGroupCode: string; description: string}[];



  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'Bin code',
    validationErrorMessages: [{ errorType: 'pattern',
    errorMessage: 'pw.corporateTown.binCodelengthError' }]
  })
  @Validation({ validators: [Validators.required,
                             Validators.pattern('^[a-zA-Z_ ]{3,20}$')]
                             })
  @Class({ className: ['col-12'] })
  private binCode: string;


  @FormField({
    fieldType: FormFieldType.TEXT_AREA,
    label: 'Description',
    validationErrorMessages: [{ errorType: 'pattern',
    errorMessage: 'pw.corporateTown.townNamelengthError' }]
  })
  @Validation({ validators: [Validators.required,
                             Validators.pattern('^[a-zA-Z_ ]{3,100}$')]
                            })
  @Class({ className: ['col-12'] })
  private description: string;



  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Class({ className: ['col-12'] })
  private IsActive: { id: string; name: string; checked?:   boolean }[];

  constructor(
    id: number,
    binGroup: { binGroupCode: string; description: string; selected?: boolean}[],
    binCode: string,
    desctiption: string,
    IsActive: { id: string; name: string; checked?: boolean }[]
  ) {
    super();
    this.id = id;
    this.binGroup = binGroup;
    this.binCode = binCode;
    this.description = desctiption;
    this.IsActive = IsActive;
  }
}
