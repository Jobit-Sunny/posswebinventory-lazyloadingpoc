

export interface BinCodeSaveNewModel {
  binCode: string,
  binGroups: BinGroups[],
  description: string;
}

export interface BinCodeSaveEditedModel {
  binGroups: BinGroups[],
  description: string;
}

export interface BinCodeList {
  locationCode: string,
  brandCode: string,
  regionCode: string,
  isActive: boolean,
}

export interface BinCodesByBinGroup {
  binCode: string,
  description: string;
}

export interface BinGroups{
  binGroupCode: string,
  isActive: boolean
}

export interface BinCodeSaveModel {
  binCode: string,
  binGroups: BinGroups[],
  description: string;
 }

 export interface BinCodeEditModel {
  binGroups: BinGroups[],
  description: string;
 }

 export interface BinCodesByBinGroup{
  binCode: string,
  description: string,
  isActive: boolean
 }

 export interface LocationMapping{
   locationCode: string,
   isActive: boolean
 }

 export interface LocationList{
   id: string,
   description: string
 }

 export interface LocationMappingPost{
  addLocations: string[],
  binCodes: string[],
  removeLocations: string[]
 }

export const BinGroupDropdown = [
  {
    binGroupCode:'STN',
  description: 'STN',
  isActive: true
},
 {
   binGroupCode:'PURCFA',
  description: 'PURCFA',
  isActive: true
}
]




