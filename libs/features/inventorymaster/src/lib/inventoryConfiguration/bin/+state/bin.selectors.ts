import { createSelector } from '@ngrx/store';
import { selectInventoryMasters } from '../../../inventorymasters.state';



const selectTotalBinCodeDetailsCount = createSelector(
  selectInventoryMasters,
  state => state.binState.totalBinCodeDetails
);

const selectIsBinCodeLoading = createSelector(
  selectInventoryMasters,
  state => state.binState.isBinCodeLoading
);


const selectBinCodeNewFormResponse = createSelector(
  selectInventoryMasters,
  state => state.binState.binCodeSaveNewResponses
);


const selectBinCodeEditFormResponse = createSelector(
  selectInventoryMasters,
  state => state.binState.editBinCodeResponses
);

const selectLocationMappingResponse = createSelector(
  selectInventoryMasters,
  state => state.binState.locationMappingResponse
);

const selectBinCodeDetailsListing = createSelector(
  selectInventoryMasters,
  state => state.binState.binCodeDetailsListing
);


const selectbinCodesByBinGroupCode = createSelector(
  selectInventoryMasters,
  state => state.binState.binCodesByBinGroup
);


const selectLocationsByBinCodesAndBinGroup = createSelector(
  selectInventoryMasters,
  state => state.binState.locationsByBinCodesAndBinGroup
);

const selectError = createSelector(
  selectInventoryMasters,
  state => state.binState.error
  );

export const BinSelectors = {
  selectBinCodeNewFormResponse,
  selectBinCodeDetailsListing,
  selectTotalBinCodeDetailsCount,
  selectbinCodesByBinGroupCode,
  selectIsBinCodeLoading,
  selectBinCodeEditFormResponse,
  selectError,
  selectLocationsByBinCodesAndBinGroup,
  selectLocationMappingResponse
};
