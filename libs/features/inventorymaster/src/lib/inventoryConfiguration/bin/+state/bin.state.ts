
import { CustomErrors } from '@poss-web/core';
import { BinCodeSaveModel, BinCodeList, BinCodesByBinGroup, BinCodeEditModel, LocationMapping, LocationList, LocationMappingPost } from '../models/bin.model';
import { SaveBinCodeFormPayload } from './bin.actions';


export interface BinState {
  error: CustomErrors,
  binCodeSaveNewResponses: SaveBinCodeFormPayload;
  binCodeDetailsListing: BinCodeList[];
  totalBinCodeDetails: number;
  binCodesByBinGroup: BinCodesByBinGroup[];
  isBinCodeLoading: boolean;
  editBinCodeResponses: BinCodeEditModel;
  locationsByBinCodesAndBinGroup: LocationList[];
  locationMappingResponse: LocationMappingPost;
}
