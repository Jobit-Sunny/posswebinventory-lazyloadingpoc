import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import * as BinActions from './bin.actions';

import * as BinGroupActions from '../../binGroup/+state/bin-group.actions'
import {
          SaveBinCodeFormPayload,
          BinCodeEditedFormPayload,
          LoadLocationsByBinGroupAndBinCode,
          LocationsByBinGroupAndBinCodePayload,
          LocationMappingPostPayload
        } from './bin.actions';
import { State } from '../../../inventorymasters.state';
import { BinSelectors } from './bin.selectors';
import { LocationMappingPost } from '../models/bin.model';

@Injectable()
export class BinFacade {
  constructor(private store: Store<State>) {}

  private isBinCodeLoading$ = this.store.select(
    BinSelectors.selectIsBinCodeLoading
  );


  private isBinCodeNewSaved$ = this.store.select(
    BinSelectors.selectBinCodeNewFormResponse
  );


  private isBinCodeEditSaved$ = this.store.select(
    BinSelectors.selectBinCodeEditFormResponse
  );


  private isLocationMappingSaved$ = this.store.select(
    BinSelectors.selectLocationMappingResponse
  );

  private binCodeDetailsListing$ = this.store.select(
    BinSelectors.selectBinCodeDetailsListing
  );

  private totalBinCodeDetails$ = this.store.select(
    BinSelectors.selectTotalBinCodeDetailsCount
  );

  private binCodesByBinGroupCode$ = this.store.select(
    BinSelectors.selectbinCodesByBinGroupCode
  );


  private locationsByBinCodesAndBinGroup$ = this.store.select(
    BinSelectors.selectLocationsByBinCodesAndBinGroup
  );


  private binCodeError$ = this.store.select(
    BinSelectors.selectError
    );



  getBinCodeIsLoading() {
    return this.isBinCodeLoading$;
  }

  getBinCodeNewSaveResponse() {
    return this.isBinCodeNewSaved$;
  }


  getBinCodeEditSaveResponse() {
    return this.isBinCodeEditSaved$;
  }


  getTotalBinCodeDetails() {
    return this.totalBinCodeDetails$;
  }


  getBinCodeDetailsListing() {
    return this.binCodeDetailsListing$;
  }

  getBinCodesByBinGroup() {
    return this.binCodesByBinGroupCode$;
  }

  getLocationsByBinCodesAndBinGroup() {
    return this.locationsByBinCodesAndBinGroup$;
  }

  getError() {
    return this.binCodeError$;
  }

  getLocationMappingResponse() {
    return this.isLocationMappingSaved$;
  }

  loadBinCodeDetailsListing(
    loadbinCodeDetailsListingPayload: BinGroupActions.LoadBinGroupDetailsListingPayload
  ) {
    this.store.dispatch(
      new BinActions.LoadBinCodeDetails(
        loadbinCodeDetailsListingPayload
      )
    );
  }


  loadBinCodesByBinGroupCode(binGroupCode: string) {
    this.store.dispatch(
      new BinActions.LoadBinCodesByBinGroupCode(
        binGroupCode
      )
    );
  }

  resetBinCodeDialogData() {
      this.store.dispatch(new BinActions.ResetBinCodeDialog());
      }


 saveBinCodeNewFormDetails(saveFormDetails: SaveBinCodeFormPayload ){
  this.store.dispatch(
    new BinActions.SaveBinCodeNewFormDetails(saveFormDetails)
  );
 }


 saveBinCodeEditedFormDetails(saveFormDetails: BinCodeEditedFormPayload){
  this.store.dispatch(
    new BinActions.EditBinCodeFormDetails(saveFormDetails)
  );
 }

 searchBinName(binName: string) {
  this.store.dispatch(new BinActions.SearchBinName(binName));
}

loadLocationsByBinCodesAndBinGroup(location:LocationsByBinGroupAndBinCodePayload) {
  this.store.dispatch(
    new BinActions.LoadLocationsByBinGroupAndBinCode(location));
}


saveLocationMappingDetails(saveMappings: LocationMappingPostPayload ){
  this.store.dispatch(
    new BinActions.SaveLocationMappingDetails(saveMappings)
  );
 }



}
