import {  BinState } from './bin.state';
import { BinActions, BinActionTypes } from './bin.actions';


const initialState: BinState = {
  error: null,
  binCodeSaveNewResponses: null,
  binCodeDetailsListing: null,
  totalBinCodeDetails: 0,
  binCodesByBinGroup: null,
  isBinCodeLoading: false,
  editBinCodeResponses: null,
  locationsByBinCodesAndBinGroup: null,
  locationMappingResponse: null,
};
export function BinReducer(
  state: BinState = initialState,
  action: BinActions
): BinState {
  switch (action.type) {
    case BinActionTypes.RESET_BINCODE_DIALOG_DATA:
      return {
        ...state,
        error:null,
        binCodeDetailsListing: null,
        binCodeSaveNewResponses: null,
        editBinCodeResponses: null,
        binCodesByBinGroup: null,
        locationMappingResponse: null,
      };

    case BinActionTypes.SAVE_BINCODE_FORM_DETAILS:
    case BinActionTypes.LOAD_BIN_CODE_DETAILS:
    case BinActionTypes.EDIT_BINCODE_FORM_DETAILS:
    case BinActionTypes.LOAD_BIN_CODES_BY_BIN_GROUPCODE:
    case BinActionTypes.LOAD_LOCATIONS_BY_BINGROUP_AND_BINCODE:
    case BinActionTypes.SAVE_LOCATION_MAPPING_DETAILS:
      return {
        ...state,
        isBinCodeLoading: true
      };

    case BinActionTypes.SAVE_BINCODE_FORM_DETAILS_SUCCESS:
      return {
        ...state,
        binCodeSaveNewResponses: action.payload,
        isBinCodeLoading: false
      };

    // case BinActionTypes.SAVE_BINCODE_FORM_DETAILS_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload,
    //     isBinCodeLoading: false
    //   };

      // case BinActionTypes.LOAD_BIN_CODE_DETAILS:
      // case BinActionTypes.EDIT_BINCODE_FORM_DETAILS:
      //   return {
      //     ...state,
      //     isBinCodeLoading: true
      //   };

      case BinActionTypes.LOAD_BIN_CODE_DETAILS_SUCCESS:
        return {
          ...state,
          binCodeDetailsListing: action.payload.binCodeDetailsListing,
          totalBinCodeDetails: action.payload.totalElements,
          isBinCodeLoading: false
        };

      case BinActionTypes.LOAD_BIN_CODE_DETAILS_FAILURE:
      case BinActionTypes.EDIT_BINCODE_FORM_DETAILS_FAILURE:
      case BinActionTypes.LOAD_LOCATIONS_BY_BINGROUP_AND_BINCODE_FAILURE:
      case BinActionTypes.LOAD_BIN_CODES_BY_BIN_GROUPCODE_FAILURE:
      case BinActionTypes.SAVE_LOCATION_MAPPING_DETAILS_FAILURE:
      case BinActionTypes.SAVE_BINCODE_FORM_DETAILS_FAILURE:
        return {
          ...state,
          error: action.payload,
          isBinCodeLoading: false
        };


      //   case BinActionTypes.LOAD_BIN_CODES_BY_BIN_GROUPCODE:
      // return {
      //   ...state,
      //   isBinCodeLoading: true
      // };

    case BinActionTypes.LOAD_BIN_CODES_BY_BIN_GROUPCODE_SUCCESS:
      return {
        ...state,
        binCodesByBinGroup: action.payload,
        isBinCodeLoading: false
      };

    // case BinActionTypes.LOAD_BIN_CODES_BY_BIN_GROUPCODE_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload,
    //     isBinCodeLoading: false
    //   };

      // case BinActionTypes.EDIT_BINCODE_FORM_DETAILS:
      //   return {
      //     ...state,
      //     isBinCodeLoading: true
      //   };

      case BinActionTypes.EDIT_BINCODE_FORM_DETAILS_SUCCESS:
        return {
          ...state,
          editBinCodeResponses: action.payload,
          isBinCodeLoading: false
        };

      // case BinActionTypes.EDIT_BINCODE_FORM_DETAILS_FAILURE:
      //   return {
      //     ...state,
      //     error: action.payload,
      //     isBinCodeLoading: false
      //   };


        // case BinActionTypes.LOAD_LOCATIONS_BY_BINGROUP_AND_BINCODE:
        //   return {
        //     ...state,
        //     isBinCodeLoading: true
        //   };

        case BinActionTypes.LOAD_LOCATIONS_BY_BINGROUP_AND_BINCODE_SUCCESS:
          return {
            ...state,
            locationsByBinCodesAndBinGroup: action.payload,
            isBinCodeLoading: false
          };

        // case BinActionTypes.LOAD_LOCATIONS_BY_BINGROUP_AND_BINCODE_FAILURE:
        //   return {
        //     ...state,
        //     error: action.payload,
        //     isBinCodeLoading: false
        //   };

          // case BinActionTypes.SAVE_LOCATION_MAPPING_DETAILS:
          //   return {
          //     ...state,
          //     isBinCodeLoading: true
          //   };

          case BinActionTypes.SAVE_LOCATION_MAPPING_DETAILS_SUCCESS:
            return {
              ...state,
              locationMappingResponse: action.payload,
              isBinCodeLoading: false
            };
          // case BinActionTypes.SAVE_LOCATION_MAPPING_DETAILS_FAILURE:
          //   return {
          //     ...state,
          //     error: action.payload,
          //     isBinCodeLoading: false
          //   };

    default:
      return state;
  }
}
