import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BinGroupDetails } from '../../../binGroup/models/bin-group.model';

import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { BinCodeSaveModel, BinGroupDropdown } from '../../models/bin.model';
import { TEMPLATE8, HelperFunctions } from '@poss-web/shared';
import { BinCodeMaster } from '../../models/bin-code-master.model';
import { Subject } from 'rxjs';
import { ConfirmDialogComponent } from '../../../../master/common/confirm-dialog/confirm-dialog.component';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'poss-web-bin-code-dialog',
  templateUrl: './bin-code-dialog.component.html',
  styleUrls: ['./bin-code-dialog.component.scss']
})
export class BinCodeDialogComponent implements OnInit {
  binCodeNewForm: FormGroup;
  binGrouipDropdown: BinGroupDetails[];
  readOnly: boolean;
  binCodeSave: BinCodeSaveModel;

  public currentStyle: string[];
  public formFields: any;
  binGroup: any;
  binCode: any;
  description: string;
  isActive: boolean;
  destroy$: Subject<null> = new Subject<null>();
  binGroupDropDownList = BinGroupDropdown;

  constructor(
    public dialogRef: MatDialogRef<BinCodeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any,
    fb: FormBuilder,
    public dialog: MatDialog,
    private hf: HelperFunctions
  ) {}

  ngOnInit() {
    let form;
    if(this.dialogData.editData){
      form = this.prepareSet(this.dialogData, this.dialogData.dropDown);
    } else{
      form = this.prepareSet(this.dialogData, this.binGroupDropDownList);
    }

    this.formFields = this.getInputs(form);
    this.currentStyle = this.getCssProp();
  }


  prepareSet(data, binGroup) {
    if (data.binGroup) {
      binGroup = this.hf.patchValue(
        binGroup,
        'binGroupCode',
        'selected',
        data.binGroup,
        true
      );
    }

    if(this.dialogData.editData){
      const binCode = new BinCodeMaster(
        1,
        binGroup,
        this.dialogData.editData.binCode ? this.dialogData.editData.binCode : '',
        this.dialogData.editData.description
          ? this.dialogData.editData.description
          : '',
          [{ id: '1',
             name: 'pw.productCategory.isActiveLabel',
            checked: this.dialogData.editData.isActive}
         ]);
         return binCode;
    }
    else {
      const binCode = new BinCodeMaster(
      1,
      binGroup,
      '',
      '',
        [{ id: '1',
           name: 'pw.productCategory.isActiveLabel',
          checked: false}
       ]);
      return binCode;
    }
  }

  onConfirm() {
    let mode = ''
    if (this.dialogData.editData) {
      mode = 'edit'
    } else {
      mode = 'new'
    }
      this.dialogRef.close({
             binCode: this.binCode,
             binGroupCode: this.binGroup,
             isActive: this.isActive,
             description: this.description,
             mode : mode
        });
  }

  getCssProp() {
    const annot = (BinCodeDialogComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }
  public setFormConfig() {
    return {
      formName: 'Bin Code Form',
      formDesc: 'Bin Code',
      formTemplate: TEMPLATE8
    };
  }

  addButton(formGroup: FormGroup) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef.afterClosed()
    .pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
    const formValues = formGroup.getRawValue();
    if(this.dialogData.defaultDropdown) {
    this.binGroup = this.dialogData.defaultDropdown;
    this.binCode = formValues['1-binCode'];
    this.isActive = formValues['1-IsActive'];
    this.description = formValues['1-description'];
  }  else  {
    this.binGroup = formValues['1-binGroup'];
    this.binCode = formValues['1-binCode'];
    this.isActive = formValues['1-IsActive'];
    this.description = formValues['1-description'];
   }
    this.onConfirm();
 }
});
}

  public formGroupCreated(formGroup: FormGroup) {
    if (this.dialogData.editData !== null) {
       formGroup.get('1-binGroup').disable({ onlySelf: true });
       formGroup.get('1-binCode').disable({ onlySelf: true });
    }
  }

  onClose() {
    this.dialogRef.close();
  }

  deleteButton() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ngOnInit();
      }
    });
  }

}
