import { Injectable } from '@angular/core';
import { ApiService } from '@poss-web/core';
import {
  getProductCategoryCodeUrl,
  getOrgCodeUrl,
  getConfigDetailsUrl,
  getselectedConfigDetailsUrl,
  getSaveConfigDetailsUrl,
  getSearchConfigDetailsByConfigIdUrl,
  getSaveWeightToleranceUrl,
  getUpdateWeightToleranceUrl,
  getRangeWeightUrl,
  getLoadProductCodeUrl,
  getSaveLocationMappingUrl,
  getLoadMappedLocationUrl,
  getUpdateIsActiveUrl

  // getSaveLocationMappingUrl,
  // getLoadMappedLocationUrl
} from '../../../endpoint.constants';
import { map, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { WeightToleranceAdaptor } from '../adaptors/weight-tolerance-adaptors';
import {
  ConfigDetails,
  SaveTolerance
} from '../models/weight-tolerance.models';
import {
  LocationMapping,
  LocationPayload
} from '../+state/weight-tolerance.actions';

@Injectable({
  providedIn: 'root'
})
export class WeightToleranceService {
  constructor(private apiService: ApiService) {}

  getProductCategoryCode() {
    const url = getProductCategoryCodeUrl();
    return this.apiService
      .get(url)
      .pipe(map(data => WeightToleranceAdaptor.getProductCategoryCodes(data)));
  }

  saveLocationMapping(locations: LocationPayload, configId: number) {
    const url = getSaveLocationMappingUrl(configId);
    return this.apiService.post(url, locations);
  }
  loadMappedLocation(configId) {
    const url = getLoadMappedLocationUrl(configId);

    return this.apiService
      .get(url)
      .pipe(map(data => WeightToleranceAdaptor.getMappedLocations(data)));
  }

  updateIsActive(configId: string, isActive: boolean) {
    const url = getUpdateIsActiveUrl(configId);
    return this.apiService.patch(url, { isActive: isActive });
  }
  getOrgCodes() {
    //const url = getOrgCodeUrl();
    // return this.apiService
    //   .get(url)
    //   .pipe(
    //     map(data => InventoryConfigurationAdaptor.getOrgCodes(data))
    //   ); //TODO replace with api
    return of(['T', 'TW', 'TWS', 'TEW', 'TJ', 'TT']);
  }

  getConfigDetailsList(pageIndex, pageSize) {
    const url = getConfigDetailsUrl('WEIGHT_TOLERANCE', pageIndex, pageSize);
    return this.apiService
      .get(url)
      .pipe(map(data => WeightToleranceAdaptor.getConfigDetailsListData(data)));
  }

  getSelectedConfigDetails(configId: string) {
    const url = getselectedConfigDetailsUrl(configId);
    return this.apiService
      .get(url)
      .pipe(map(data => WeightToleranceAdaptor.getSelectedConfigData(data)));
  }

  saveConfigDetails(configDetails: ConfigDetails) {
    const url = getSaveConfigDetailsUrl();
    return this.apiService.post(url, configDetails);
  }

  searchConfigDetailsByConfigId(configId) {
    const url = getSearchConfigDetailsByConfigIdUrl(configId);
    return this.apiService
      .get(url)
      .pipe(
        map(data =>
          WeightToleranceAdaptor.getSearchConfigDetailsByConfigIdData(data)
        )
      );
  }
  saveWeightTolerance(configId: number, tolerance: SaveTolerance) {
    const url = getSaveWeightToleranceUrl(configId);

    return this.apiService.post(url, tolerance);
  }

  getWeightTolerance(configId: any) {
    const url = getSaveWeightToleranceUrl(configId);
    return this.apiService
      .get(url)
      .pipe(map(data => WeightToleranceAdaptor.getWeightTolerances(data)));
  }

  updateWeightTolerance(
    tolerance: SaveTolerance,
    configId: any,
    configProductId: any
  ) {
    const url = getUpdateWeightToleranceUrl(configId, configProductId);

    return this.apiService
      .patch(url, tolerance)
      .pipe(map(data => WeightToleranceAdaptor.getUpdatedTolerance(data)));
  }
  loadRangeWeight() {
    const url = getRangeWeightUrl();

    return this.apiService
      .get(url)
      .pipe(map(data => WeightToleranceAdaptor.getRangeWeights(data)));
  }

  loadProductCode() {
    const url = getLoadProductCodeUrl();

    return this.apiService
      .get(url)
      .pipe(map(data => WeightToleranceAdaptor.getProductGroupCode(data)));
  }
}
