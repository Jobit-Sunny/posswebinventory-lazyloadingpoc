export interface ConfigDetails {
  configId: string;
  description: string;
  configType: string;
  orgCode: string;
  productCategoryCode: number;
  isActive:boolean

}

export interface ConfigDetailsList {
  configDetails: ConfigDetails[];
  totalConfigDetails: number;
}
export interface Tolerance
{
  id:any
  productGroupCode: string,
  rangeId: number
  value: number

}

export interface SaveTolerance{
  productGroupCode?: string,
  rangeId?: number
  value?: number
}
export interface RangeWeight{
  id:number;
  fromRange:number;
  toRange:number
}

export interface ProductGroupCode{
  productGroupCode:number;
  description:string
}
