import { ConfigDetailsList, ConfigDetails, Tolerance, RangeWeight, ProductGroupCode } from '../models/weight-tolerance.models';
import { UpdateTolerancePayloadSuccess } from '../+state/weight-tolerance.actions';



export class WeightToleranceAdaptor {


  static getMappedLocations(data:any){

    const selectedLocations:{id:string,description:string}[]=[]

    for(const locations of data){
      selectedLocations.push(
        {
          id:locations,
          description:locations
        }
      )
     }

     return selectedLocations
  }

  static getProductCategoryCodes(data: any) {

    //TODO change Type Any
    const productCategoryCodes: any = [];
    for (const productCategoryCode of data.results) {
      productCategoryCodes.push({
        productCategoryCode: productCategoryCode.productCategoryCode,
        description: productCategoryCode.description,
        orgCode: null,
        configDetails: {}
      });
    }

    return productCategoryCodes;
  }

  static getSearchConfigDetailsByConfigIdData(data): ConfigDetailsList {
    let configDetailsList: ConfigDetailsList;
    const configDetails: ConfigDetails[] = [];

    configDetails.push({
      configId: data.configId,
      configType: data.configType,
      description: data.description,
      productCategoryCode: data.productCategoryCode,
      orgCode: data.orgCode,
      isActive:data.isActive
    });
    let totalElements;
    if (data) {
      totalElements = 1;
    } else {
      totalElements = 0;
    }
    configDetailsList = {
      configDetails: configDetails,
      totalConfigDetails: totalElements
    };
    return configDetailsList;
  }
  static getOrgCodes(data: any) {
    const orgCodes: any = [];
    for (const orgCode of orgCodes) {
      orgCodes.push({
        //TODO Construct array
      });
    }
    return orgCodes;
  }


  static getConfigDetailsListData(data: any): ConfigDetailsList {

    const configDetais: ConfigDetails[] = [];
    let configDetailsList: ConfigDetailsList;
    for (const configData of data.results) {
      configDetais.push({
        configId: configData.configId,
        description: configData.description,
        configType: configData.configType,
        orgCode: configData.orgCode,
        productCategoryCode: configData.productCategoryCode,
        isActive:configData.isActive
      });
    }
    configDetailsList = {
      configDetails: configDetais,
      totalConfigDetails: data.totalElements
    };
    return configDetailsList;
  }
  static getSelectedConfigData(data: any): ConfigDetails {
    let configData: ConfigDetails;
    configData = {
      configId: data.configId,
        description: data.description,
        configType: data.configType,
        orgCode: data.orgCode,
        productCategoryCode: data.productCategoryCode,
        isActive:data.isActive
    };

    return configData;
  }

  static getWeightTolerances(data: any): Tolerance[] {
    const tolerance: Tolerance[] = [];
    for (const toleranceData of data) {
      tolerance.push({
        id: toleranceData.id,
        productGroupCode: toleranceData.productGroupCode,
        rangeId: toleranceData.rangeId,
        value: toleranceData.value
      });
    }

    return tolerance;
  }

  static getUpdatedTolerance(data: any) {

    let updatedToleranceData: UpdateTolerancePayloadSuccess;
    updatedToleranceData = {
      configProductId: data.id,
      newTolerance: data
    };

    return updatedToleranceData;
  }
  static getRangeWeights(data: any): RangeWeight[] {
    const rangeWeight: RangeWeight[] = [];
    for (const rangeWeightData of data.results) {
      rangeWeight.push({
        id: rangeWeightData.id,
        fromRange: rangeWeightData.fromRange,
        toRange: rangeWeightData.toRange
      });
    }

    return rangeWeight;
  }

  static getProductGroupCode(data: any): ProductGroupCode[] {

    const productGroupCode: ProductGroupCode[] = [];
    for (const productGroupCodeData of data.results) {
      productGroupCode.push({
        productGroupCode: productGroupCodeData.productGroupCode,
        description:productGroupCodeData.description
      });
    }
    return productGroupCode;
  }
}
