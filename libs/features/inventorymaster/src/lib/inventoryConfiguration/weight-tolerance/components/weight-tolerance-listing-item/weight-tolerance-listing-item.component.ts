import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'poss-web-weight-tolerance-listing-item',
  templateUrl: './weight-tolerance-listing-item.component.html',
  styleUrls: ['./weight-tolerance-listing-item.component.scss']
})
export class WeightToleranceListingItemComponent implements OnInit {
  @Input() configDetailsItem;

  @Output() loadSelectedWeightTolerance = new EventEmitter<any>();
  @Output() emittoggle = new EventEmitter<any>();
  constructor() { }

  ngOnInit() { }

  editWeightTolerance() {
    this.loadSelectedWeightTolerance.emit(this.configDetailsItem.configId);
  }
  change(event) {
    const obj = {
      isActive: event,
      configId: this.configDetailsItem.configId
    };
    this.emittoggle.emit(obj);
  }
}
