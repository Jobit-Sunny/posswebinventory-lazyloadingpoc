import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PageEvent } from '@angular/material';
import { Subject } from 'rxjs';

@Component({
  selector: 'poss-web-weight-tolerance-listing-items',
  templateUrl: './weight-tolerance-listing-items.component.html',
  styleUrls: ['./weight-tolerance-listing-items.component.scss']
})
export class WeightToleranceListingItemsComponent implements OnInit {
  @Input() pageEvent: PageEvent;
  @Input() totalCount;
  @Input() configDetails;
  @Input() pageSize;
  @Output() paginator = new EventEmitter<PageEvent>();
  @Output() loadSelectedConfigId = new EventEmitter<any>();
  @Output() toggleValue=new EventEmitter<any>()
  pageSizeOptions: number[] = [];
  minPageSize = 0;
  destroy$ = new Subject<null>();
  constructor() {}
  emitSelectedToleranceId(configId: any) {
    this.loadSelectedConfigId.emit(configId);
  }
  ngOnInit() {
    this.pageSizeOptions = this.pageSize;
    this.minPageSize = this.pageSizeOptions.reduce((a: number, b: number) =>
      a < b ? a : b
    );
  }

  emitToggle(toggleValue){
    this.toggleValue.emit(toggleValue)
  }
}
