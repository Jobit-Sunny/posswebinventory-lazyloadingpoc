import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import {
  InventoryConfigurationListingPayload,
  UpdateTolerancePayload,
  LocationMapping,
  UpadateIsActivePayload,

} from './weight-tolerance.actions';
import { ConfigDetails } from '../models/weight-tolerance.models';
import { State } from '../../../inventorymasters.state';
import { WeightToleranceSelectors } from './weight-tolerance.selectors';
import * as WeightToleranceActions from './weight-tolerance.actions';

@Injectable()
export class WeightToleranceFacade {
  constructor(private store: Store<State>) { }
  private isLoading$ = this.store.select(
    WeightToleranceSelectors.selectisLoading
  );

  private mappedLocatons$ = this.store.select(
    WeightToleranceSelectors.selectMappedLocations
  );
  private totalConfigDetail$ = this.store.select(
    WeightToleranceSelectors.selectTotalConfigDetails
  );

  private configDetails$ = this.store.select(
    WeightToleranceSelectors.selectConfigDetails
  );

  private selectedConfigId$ = this.store.select(
    WeightToleranceSelectors.selectConfigDetailsByconfigId
  );
  private selectToleranceItem$ = this.store.select(
    WeightToleranceSelectors.selectToleranceItem
  );

  private selectProductGroupCode$ = this.store.select(
    WeightToleranceSelectors.selectProductGroupCode
  );
  private selectRangeWeight$ = this.store.select(
    WeightToleranceSelectors.selectRangeWeight
  );

  private selectProductCategoryCode$ = this.store.select(
    WeightToleranceSelectors.selectProductCategoryCode
  );

  private selectOrgCodes$ = this.store.select(
    WeightToleranceSelectors.selectOrgCode
  );

  private selectHasSaved$ = this.store.select(
    WeightToleranceSelectors.selecthasSaved
  );

  private selectSearchedResults$ = this.store.select(
    WeightToleranceSelectors.selectSearchedItem
  );

  private selectIsEditEnable$ = this.store.select(
    WeightToleranceSelectors.selectIsEditEnable
  );

  private selectItemsIds$ = this.store.select(
    WeightToleranceSelectors.selectItemsIds
  );

  private selectIsupdated$ = this.store.select(
    WeightToleranceSelectors.selectIsupdated
  );

  private selectError$ = this.store.select(
    WeightToleranceSelectors.selectError
  );
  private isSearching$ = this.store.select(
    WeightToleranceSelectors.selectIsSearching
  );

  private selectislocationMappingSuccess$ = this.store.select(
    WeightToleranceSelectors.selectislocationMappingSuccess
  );

  private selectproductCodeCount$ = this.store.select(
    WeightToleranceSelectors.selectproductCodeCount
  );
  private isActiveToggle$ = this.store.select(
    WeightToleranceSelectors.selectIsActiveToggle
  );

  getProductCategoryCodeCount() {
    return this.selectproductCodeCount$;
  }
  getSelectIsLocationMappingSuccess() {
    return this.selectislocationMappingSuccess$;
  }
  getMappedLocations() {
    return this.mappedLocatons$;
  }
  getIsSearching() {
    return this.isSearching$;
  }
  getError() {
    return this.selectError$;
  }
  getIsLoading() {
    return this.isLoading$;
  }
  getIsUpdated() {
    return this.selectIsupdated$;
  }
  getSelectToleranceItemsIds() {
    return this.selectItemsIds$;
  }
  getIsEditEnable() {
    return this.selectIsEditEnable$;
  }

  getSearchedResult() {
    return this.selectSearchedResults$;
  }
  getHasSaved() {
    return this.selectHasSaved$;
  }
  getOrgCode() {
    return this.selectOrgCodes$;
  }
  getProductCategoryCode() {
    return this.selectProductCategoryCode$;
  }
  getProductGroupCode() {
    return this.selectProductGroupCode$;
  }
  getRangeWeight() {
    return this.selectRangeWeight$;
  }

  getToleranceItemByConfigId() {
    return this.selectToleranceItem$;
  }
  getSelectedConfigId() {
    return this.selectedConfigId$;
  }
  getTotalConfigDetail() {
    return this.totalConfigDetail$;
  }
  getConfigDetails() {
    return this.configDetails$;
  }

  getIsActiveToggle() {
    return this.isActiveToggle$;
  }

  saveLocationMapping(locations: LocationMapping) {
    this.store.dispatch(
      new WeightToleranceActions.SaveLocationMapping(locations)
    );
  }
  loadMappedLocations(configId) {
    this.store.dispatch(
      new WeightToleranceActions.LoadMappedLocation(configId)
    );
  }

  loadConfigDetails(
    inventoryConfigurationListingPayload: InventoryConfigurationListingPayload
  ) {
    this.store.dispatch(
      new WeightToleranceActions.LoadConfigDetails(
        inventoryConfigurationListingPayload
      )
    );
  }

  loadSelectedConfigDetails(configId: any) {
    this.store.dispatch(
      new WeightToleranceActions.LoadSelectedConfigDetails(configId)
    );
  }

  loadWeightToleranceByConfigId(configId) {
    this.store.dispatch(
      new WeightToleranceActions.LoadWeightToleranceByConfigid(configId)
    );
  }
  saveConfigDetails(configDetails: ConfigDetails) {
    this.store.dispatch(
      new WeightToleranceActions.SaveConfigDetails(configDetails)
    );
  }
  loadProductGroupCode() {
    this.store.dispatch(new WeightToleranceActions.LoadProductCode());
  }
  loadRangeWeight() {
    this.store.dispatch(new WeightToleranceActions.LoadRangeWeight());
  }

  loadProductCategoryCode(

  ) {

    this.store.dispatch(
      new WeightToleranceActions.LoadProductCategoryCode(

      )
    );
  }

  loadOrgCode() {
    this.store.dispatch(new WeightToleranceActions.LoadOrgCode());
  }

  searchConfigDetailsByConfigId(configId) {
    this.store.dispatch(
      new WeightToleranceActions.SearchConfigDetailsByConfigId(configId)
    );
  }
  saveWeightTolerance(tolerance) {
    this.store.dispatch(
      new WeightToleranceActions.SaveWeightTolerance(tolerance)
    );
  }
  loadEditEnable() {
    this.store.dispatch(new WeightToleranceActions.LoadEditEnable());
  }

  updateToleranceData(updateToleranceData: UpdateTolerancePayload) {
    this.store.dispatch(
      new WeightToleranceActions.UpdateWeightTolerance(updateToleranceData)
    );
  }
  updateIsActive(updtaeIsActivePaylod: UpadateIsActivePayload) {
    this.store.dispatch(
      new WeightToleranceActions.UpdateIsActive(updtaeIsActivePaylod)
    );
  }
  loadProductCategoryCount() {
    this.store.dispatch(
      new WeightToleranceActions.LoadProductCategoryCodeCount()
    );
  }
  loadReset() {
    this.store.dispatch(new WeightToleranceActions.LoadReset());
  }
  resetIsActiveToggle() {
    this.store.dispatch(new WeightToleranceActions.ResetIsActive());
  }

}
