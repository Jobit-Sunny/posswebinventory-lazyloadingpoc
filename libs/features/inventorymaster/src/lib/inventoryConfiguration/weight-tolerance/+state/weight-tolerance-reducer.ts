import {
  WeightToleranceActionTypes,
  WeightToleranceActions
} from './weight-tolerance.actions';
import { ToleranceAdaptor } from './weight-tolerance.entity';
import { WeightToleranceState } from './weight-tolerances-state';

const initialState: WeightToleranceState = {
  configDetails: null,
  tolerance: ToleranceAdaptor.getInitialState(),
  isConfigDetailsLoading: true,
  totalConfigDetails: null,
  selectedConfigIdDetails: null,
  searchResults: null,
  productGropuCode: null,
  rangeWeight: null,
  productCategoryCode: null,
  orgCodes: null,
  hasSaved: null,
  productCodeCount: null,
  isEditEnable: false,
  error: null,
  hasUpdated: null,
  isSearching: null,
  mappedLocations: null,
  islocationMappingSuccess: null,
  isActiveUpdated: null
};
export function WeightToleranceReducer(
  state: WeightToleranceState = initialState,
  action: WeightToleranceActions
): WeightToleranceState {
  switch (action.type) {
    case WeightToleranceActionTypes.LOAD_CONFIG_DETAILS:
      return {
        ...state,
        isConfigDetailsLoading: true,
        error: null
      };
    case WeightToleranceActionTypes.LOAD_CONFIG_DETAILS_SUCCESS:
      return {
        ...state,
        configDetails: action.payload.configDetails,
        totalConfigDetails: action.payload.totalConfigDetails,
        isConfigDetailsLoading: false,
        error: null
      };
    case WeightToleranceActionTypes.LOAD_CONFIG_DETAILS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isConfigDetailsLoading: false
      };

    case WeightToleranceActionTypes.LOAD_SELECTED_CONFIG_DETAILS:
      return {
        ...state,
        isConfigDetailsLoading: true
      };
    case WeightToleranceActionTypes.LOAD_SELECTED_CONFIG_DETAILS_SUCCESSS:
      return {
        ...state,
        isEditEnable: false,
        isConfigDetailsLoading: false,
        selectedConfigIdDetails: action.payload
      };

    case WeightToleranceActionTypes.LOAD_WEIGHT_TOLERANCE_BY_CONFIG_ID:
    case WeightToleranceActionTypes.LOAD_RANGE_WEIGHT:
    case WeightToleranceActionTypes.LOAD_PRODUCT_CODE:
      return {
        ...state
      };
    case WeightToleranceActionTypes.LOAD_WEIGHT_TOLERANCE_BY_CONFIG_ID_SUCCESS:
      return {
        ...state,

        tolerance: ToleranceAdaptor.addAll(action.payload, state.tolerance)
      };
    case WeightToleranceActionTypes.LOAD_WEIGHT_TOLERANCE_BY_CONFIG_ID_FAILURE:
    case WeightToleranceActionTypes.LOAD_RANGE_WEIGHT_FAILURE:
    case WeightToleranceActionTypes.LOAD_PRODUCT_CODE_FAILURE:
    case WeightToleranceActionTypes.LOAD_PRODUCT_CATEGORY_CODE_FAILURE:
    case WeightToleranceActionTypes.LOAD_PRODUCT_CATEGORY_CODE_COUNT_FAILURE:
    case WeightToleranceActionTypes.LOAD_ORG_CODE_FAILURE:
    case WeightToleranceActionTypes.LOAD_MAPPED_LOCATION_FAILURE:
      return {
        ...state,
        error: action.payload
      };

    case WeightToleranceActionTypes.LOAD_RANGE_WEIGHT_SUCCESS:
      return {
        ...state,
        rangeWeight: action.payload
      };

    case WeightToleranceActionTypes.LOAD_PRODUCT_CODE_SUCCESS:
      return {
        ...state,
        productGropuCode: action.payload
      };

    case WeightToleranceActionTypes.LOAD_PRODUCT_CATEGORY_CODE:
    case WeightToleranceActionTypes.LOAD_PRODUCT_CATEGORY_CODE_COUNT:
    case WeightToleranceActionTypes.LOAD_ORG_CODE:
    case WeightToleranceActionTypes.SEARCH_CONFIG_DETAILS_BY_CONFIG_ID:
    case WeightToleranceActionTypes.LOAD_MAPPED_LOCATION:
      return {
        ...state,
        error: null
      };
    case WeightToleranceActionTypes.LOAD_PRODUCT_CATEGORY_CODE_SUCCESS:
      return {
        ...state,
        error: null,
        productCategoryCode: action.payload
      };

    case WeightToleranceActionTypes.LOAD_PRODUCT_CATEGORY_CODE_COUNT_SUCCESS:
      return {
        ...state,
        error: null,
        productCodeCount: action.payload
      };

    case WeightToleranceActionTypes.LOAD_ORG_CODE_SUCCESS:
      return {
        ...state,
        orgCodes: action.payload
      };

    case WeightToleranceActionTypes.SAVE_CONFIG_DETAILS:
    case WeightToleranceActionTypes.SAVE_WEIGHT_TOLERANCE_DETAILS:
      return {
        ...state,
        error: null,
        hasSaved: false
      };
    case WeightToleranceActionTypes.SAVE_CONFIG_DETAILS_SUCCESS:
      return {
        ...state,
        error: null,
        selectedConfigIdDetails: action.payload,
        hasSaved: true
      };
    case WeightToleranceActionTypes.SAVE_CONFIG_DETAILS_FAILURE:
      return {
        ...state,
        error: action.payload,
        selectedConfigIdDetails: null,
        hasSaved: false
      };

    case WeightToleranceActionTypes.SEARCH_CONFIG_DETAILS_BY_CONFIG_ID_SUCCESS:
      return {
        ...state,
        error: null,
        configDetails: action.payload.configDetails,
        totalConfigDetails: action.payload.totalConfigDetails
      };

    case WeightToleranceActionTypes.SEARCH_CONFIG_DETAILS_BY_CONFIG_ID_FAILURE:
      return {
        ...state,
        configDetails: null,
        error: action.payload,
        totalConfigDetails: null
      };

    case WeightToleranceActionTypes.SAVE_WEIGHT_TOLERANCE_DETAILS_SUCCESS:
      return {
        ...state,

        hasSaved: true
      };
    case WeightToleranceActionTypes.SAVE_WEIGHT_TOLERANCE_DETAILS_FAILURE:
      return {
        ...state,
        error: action.payload,
        hasSaved: false
      };

    case WeightToleranceActionTypes.UPDATE_IS_ACTIVE:
      return {
        ...state,
        error: null,
        isConfigDetailsLoading: true,
        isActiveUpdated: false
      };
    case WeightToleranceActionTypes.UPDATE_IS_ACTIVE_SUCCESS:
      return {
        ...state,
        isConfigDetailsLoading: false,
        isActiveUpdated: true
        //TODO
      };
    case WeightToleranceActionTypes.UPDATE_IS_ACTIVE_FAILURE:
      return {
        ...state,
        error: action.payload,
        isConfigDetailsLoading: false,
        isActiveUpdated: false
      };

    case WeightToleranceActionTypes.UPADTE_WEIGHT_TOLERANCE:
      return {
        ...state,
        error: null,
        isEditEnable: true,
        hasUpdated: false
      };
    case WeightToleranceActionTypes.UPADTE_WEIGHT_TOLERANCE_SUCCESS:
      return {
        ...state,
        isEditEnable: false,
        error: null,
        tolerance: ToleranceAdaptor.updateOne(
          {
            id: action.payload.configProductId,
            changes: {
              productGroupCode: action.payload.newTolerance.productGroupCode,
              value: action.payload.newTolerance.value,
              rangeId: action.payload.newTolerance.rangeId
            }
          },
          state.tolerance
        ),
        hasUpdated: true
      };

    case WeightToleranceActionTypes.UPADTE_WEIGHT_TOLERANCE_FAILURE:
      return {
        ...state,
        isEditEnable: false,
        tolerance: ToleranceAdaptor.updateOne(
          {
            id: action.payload.id,
            changes: {
              productGroupCode: action.payload.actualTolerance.productGroupCode,
              value: action.payload.actualTolerance.value,
              rangeId: action.payload.actualTolerance.rangeId
            }
          },
          state.tolerance
        ),
        error: action.payload.error,
        hasUpdated: false
      };

    case WeightToleranceActionTypes.LOAD_RESET:
      return {
        ...state,
        isConfigDetailsLoading: null,
        hasUpdated: null,
        selectedConfigIdDetails: null,
        hasSaved: null,
        isSearching: null,
        islocationMappingSuccess: null,
        error: null
      };

    case WeightToleranceActionTypes.SAVE_LOCATION_MAPPING:
      return {
        ...state,
        islocationMappingSuccess: false,
        error: null
      };

    case WeightToleranceActionTypes.SAVE_LOCATION_MAPPING_SUCCESS:
      return {
        ...state,
        islocationMappingSuccess: true,
        error: null
      };

    case WeightToleranceActionTypes.SAVE_LOCATION_MAPPING_FAILURE:
      return {
        ...state,
        islocationMappingSuccess: false,
        error: action.payload
      };

    case WeightToleranceActionTypes.LOAD_MAPPED_LOCATION_SUCCESS:
      return {
        ...state,
        mappedLocations: action.payload,
        error: null
      };

    case WeightToleranceActionTypes.LOAD_EDIT_ENABLE:
      return {
        ...state,
        isEditEnable: true
      };

    case WeightToleranceActionTypes.RESET_IS_ACTIVE:
      return {
        ...state,
        isActiveUpdated: false
      };

    default:
      return state;
  }
}
