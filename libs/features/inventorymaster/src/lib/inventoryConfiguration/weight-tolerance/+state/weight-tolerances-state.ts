import { ToleranceEntity } from './weight-tolerance.entity';
import {
  ConfigDetails,
  ProductGroupCode,
  RangeWeight
} from '../models/weight-tolerance.models';
import { CustomErrors } from '@poss-web/core';

export interface WeightToleranceState {
  //configDetails
  productCodeCount: number;
  configDetails: ConfigDetails[];
  selectedConfigIdDetails: ConfigDetails;
  searchResults: ConfigDetails;
  totalConfigDetails: number;
  isConfigDetailsLoading: boolean;
  tolerance: ToleranceEntity;
  productGropuCode: ProductGroupCode[];
  rangeWeight: RangeWeight[];
  productCategoryCode: any; //TODO Chane type any
  orgCodes: any; //TODO change type any
  hasSaved: boolean;
  hasUpdated: boolean;
  isEditEnable: boolean;
  error: CustomErrors;
  isSearching: boolean;
  islocationMappingSuccess: boolean;
  mappedLocations: { id: string; description: string }[];
  isActiveUpdated: boolean;
}
