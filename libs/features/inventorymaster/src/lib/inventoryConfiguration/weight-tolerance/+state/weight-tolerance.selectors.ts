import { createSelector } from '@ngrx/store';

import { toleranceSelector } from './weight-tolerance.entity';
import { selectInventoryMasters } from '../../../inventorymasters.state';


const selectproductCodeCount = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.productCodeCount
)
const selectMappedLocations = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.mappedLocations
)
const selectisLoading = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.isConfigDetailsLoading
)
const selectError = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.error
)
const selectConfigDetails = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.configDetails
)
const selectTotalConfigDetails = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.totalConfigDetails
)
const selectConfigDetailsByconfigId = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.selectedConfigIdDetails
)

const toleranceItem = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.tolerance
)

const selectToleranceItem = createSelector(
  toleranceItem,
  toleranceSelector.selectAll
)
const selectProductGroupCode = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.productGropuCode
)
const selectRangeWeight = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.rangeWeight
)


const selectProductCategoryCode = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.productCategoryCode
)
const selectOrgCode = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.orgCodes
)
const selecthasSaved = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.hasSaved
)


const selectSearchedItem = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.searchResults
)

const selectIsEditEnable = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.isEditEnable
)

const selectItemsIds = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.tolerance.ids
);
const selectIsupdated = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.hasUpdated
)
const selectIsSearching = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.isSearching
)
const selectislocationMappingSuccess = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.islocationMappingSuccess
)

const selectIsActiveToggle = createSelector(
  selectInventoryMasters,
  state => state.weightToleranceState.isActiveUpdated
)
export const WeightToleranceSelectors = {
  selectMappedLocations,
  selectConfigDetails,
  selectTotalConfigDetails,
  selectConfigDetailsByconfigId,
  selectToleranceItem,
  selectProductGroupCode,
  selectRangeWeight,
  selectProductCategoryCode,
  selectOrgCode,
  selecthasSaved,
  selectSearchedItem,
  selectIsEditEnable,
  selectItemsIds,
  selectIsupdated,
  selectisLoading,
  selectError,
  selectIsSearching,
  selectislocationMappingSuccess,
  selectproductCodeCount,
  selectIsActiveToggle
};
