import { DataPersistence } from '@nrwl/angular';
import {
  NotificationService,
  CustomErrors,
  CustomErrorAdaptor
} from '@poss-web/core';

import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';

import { HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';

import * as WeightToleranceActionTypesActions from './weight-tolerance.actions';
import {
  WeightToleranceActionTypes,
  UpdateTolerancePayloadSuccess
} from './weight-tolerance.actions';
import {
  ConfigDetails,
  ConfigDetailsList,
  Tolerance
} from '../models/weight-tolerance.models';
import { WeightToleranceService } from '../services/weight-tolerance.service';

@Injectable()
export class WeightToleranceEffect {
  constructor(
    private dataPersistence: DataPersistence<any>,
    private notificationService: NotificationService,
    private weightToleranceService: WeightToleranceService
  ) {}

  // TODO, Change type any
  @Effect()
  loadProductCategoryCode$ = this.dataPersistence.fetch(
    WeightToleranceActionTypes.LOAD_PRODUCT_CATEGORY_CODE,
    {
      run: (
        action: WeightToleranceActionTypesActions.LoadProductCategoryCode
      ) => {

        return this.weightToleranceService
          .getProductCategoryCode()
          .pipe(
            map(
              (productCategoryCodes: any) =>
                new WeightToleranceActionTypesActions.LoadProductCategoryCodeSuccess(
                  productCategoryCodes
                )
            )
          );
      },
      onError: (
        action: WeightToleranceActionTypesActions.LoadProductCategoryCode,
        error: HttpErrorResponse
      ) => {
        return new WeightToleranceActionTypesActions.LoadProductCategoryCodeFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  // TODO, Change type any
  @Effect()
  loadOrgCodes$ = this.dataPersistence.fetch(
    WeightToleranceActionTypes.LOAD_ORG_CODE,
    {
      run: (action: WeightToleranceActionTypesActions.LoadOrgCode) => {
        return this.weightToleranceService
          .getOrgCodes()
          .pipe(
            map(
              (orgCodes: any) =>
                new WeightToleranceActionTypesActions.LoadOrgCodeSucccess(
                  orgCodes
                )
            )
          );
      },
      onError: (
        action: WeightToleranceActionTypesActions.LoadOrgCode,
        error: HttpErrorResponse
      ) => {
        return new WeightToleranceActionTypesActions.LoadProductCategoryCodeSuccess(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  saveLocationMapping$ = this.dataPersistence.fetch(
    WeightToleranceActionTypes.SAVE_LOCATION_MAPPING,
    {
      run: (action: WeightToleranceActionTypesActions.SaveLocationMapping) => {
        return this.weightToleranceService
          .saveLocationMapping(action.payload.locations,action.payload.configId)
          .pipe(
            map(
              (orgCodes: any) =>
                new WeightToleranceActionTypesActions.SaveLocationMappingSuccess()
            )
          );
      },
      onError: (
        action: WeightToleranceActionTypesActions.SaveLocationMapping,
        error: HttpErrorResponse
      ) => {
        return new WeightToleranceActionTypesActions.SaveLocationMappingFailure(
          this.errorHandler(error)
        );
      }
    }
  );


  @Effect()
  loadMappedLocation$ = this.dataPersistence.fetch(
    WeightToleranceActionTypes.LOAD_MAPPED_LOCATION,
    {
      run: (action: WeightToleranceActionTypesActions.LoadMappedLocation) => {

        return this.weightToleranceService
          .loadMappedLocation(action.payload)
          .pipe(
            map(
              (locationCodes: {id:string,description:string}[]) =>
                new WeightToleranceActionTypesActions.LoadMappedLocationSuccess(locationCodes)
            )
          );
      },
      onError: (
        action: WeightToleranceActionTypesActions.LoadMappedLocation,
        error: HttpErrorResponse
      ) => {
        return new WeightToleranceActionTypesActions.LoadMappedLocationFailure(
          this.errorHandler(error)
        );
      }
    }
  );


  @Effect()
  updateIsActive$ = this.dataPersistence.fetch(
    WeightToleranceActionTypes.UPDATE_IS_ACTIVE,
    {
      run: (action: WeightToleranceActionTypesActions.UpdateIsActive) => {

        return this.weightToleranceService
          .updateIsActive(action.payload.configId,action.payload.isActive)
          .pipe(
            map(
              (locationCodes: {id:string,description:string}[]) =>
                new WeightToleranceActionTypesActions.UpdateIsActiveSuccess('')
            )
          );
      },
      onError: (
        action: WeightToleranceActionTypesActions.UpdateIsActive,
        error: HttpErrorResponse
      ) => {
        return new WeightToleranceActionTypesActions.UpdateIsActiveFailure(
          this.errorHandler(error)
        );
      }
    }
  );
















  @Effect()
  loadConfigDetails$ = this.dataPersistence.fetch(
    WeightToleranceActionTypes.LOAD_CONFIG_DETAILS,
    {
      run: (action: WeightToleranceActionTypesActions.LoadConfigDetails) => {
        return this.weightToleranceService
          .getConfigDetailsList(
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              configDetailsList =>
                new WeightToleranceActionTypesActions.LoadConfigDetailsSuccess(
                  configDetailsList
                )
            )
          );
      },
      onError: (
        action: WeightToleranceActionTypesActions.LoadConfigDetails,
        error: HttpErrorResponse
      ) => {
        return new WeightToleranceActionTypesActions.LoadConfigDetailsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadSelectedConfigDetails$ = this.dataPersistence.fetch(
    WeightToleranceActionTypes.LOAD_SELECTED_CONFIG_DETAILS,
    {
      run: (
        action: WeightToleranceActionTypesActions.LoadSelectedConfigDetails
      ) => {
        return this.weightToleranceService
          .getSelectedConfigDetails(action.payload)
          .pipe(
            map(
              selectedConfigData =>
                new WeightToleranceActionTypesActions.LoadSelectedConfigDetailsSuccess(
                  selectedConfigData
                )
            )
          );
      },
      onError: (
        action: WeightToleranceActionTypesActions.LoadSelectedConfigDetails,
        error: HttpErrorResponse
      ) => {
        return new WeightToleranceActionTypesActions.LoadSelectedConfigDetailsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  SaveConfigDetails$ = this.dataPersistence.fetch(
    WeightToleranceActionTypes.SAVE_CONFIG_DETAILS,
    {
      run: (action: WeightToleranceActionTypesActions.SaveConfigDetails) => {
        return this.weightToleranceService
          .saveConfigDetails(action.payload)
          .pipe(
            map(
              (configDetails: ConfigDetails) =>
                new WeightToleranceActionTypesActions.SaveConfigDetailsSuccess(
                  configDetails
                )
            )
          );
      },
      onError: (
        action: WeightToleranceActionTypesActions.SaveConfigDetails,
        error: HttpErrorResponse
      ) => {
        return new WeightToleranceActionTypesActions.SaveConfigDetailsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  SearchConfigDetailsByConfigId$ = this.dataPersistence.fetch(
    WeightToleranceActionTypes.SEARCH_CONFIG_DETAILS_BY_CONFIG_ID,
    {
      run: (
        action: WeightToleranceActionTypesActions.SearchConfigDetailsByConfigId
      ) => {
        return this.weightToleranceService
          .searchConfigDetailsByConfigId(action.payload)
          .pipe(
            map(
              (configDetails: ConfigDetailsList) =>
                new WeightToleranceActionTypesActions.SearchConfigDetailsByConfigIdSuccess(
                  configDetails
                )
            )
          );
      },
      onError: (
        action: WeightToleranceActionTypesActions.SearchConfigDetailsByConfigId,
        error: HttpErrorResponse
      ) => {
        return new WeightToleranceActionTypesActions.SearchConfigDetailsByConfigIdFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  saveWeightTolerances$ = this.dataPersistence.fetch(
    WeightToleranceActionTypes.SAVE_WEIGHT_TOLERANCE_DETAILS,
    {
      run: (action: WeightToleranceActionTypesActions.SaveWeightTolerance) => {
        return this.weightToleranceService
          .saveWeightTolerance(
            action.payload.configId,
            action.payload.tolerance
          )
          .pipe(
            map(
              () =>
                new WeightToleranceActionTypesActions.SaveWeightToleranceSuccess()
            )
          );
      },
      onError: (
        action: WeightToleranceActionTypesActions.SaveWeightTolerance,
        error: HttpErrorResponse
      ) => {
        return new WeightToleranceActionTypesActions.SaveWeightToleranceFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadWeightToleranceByConfigId$ = this.dataPersistence.fetch(
    WeightToleranceActionTypes.LOAD_WEIGHT_TOLERANCE_BY_CONFIG_ID,
    {
      run: (
        action: WeightToleranceActionTypesActions.LoadWeightToleranceByConfigid
      ) => {
        return this.weightToleranceService
          .getWeightTolerance(action.payload)
          .pipe(
            map(
              (tolerance: Tolerance[]) =>
                new WeightToleranceActionTypesActions.LoadWeightToleranceByConfigidSuccess(
                  tolerance
                )
            )
          );
      },
      onError: (
        action: WeightToleranceActionTypesActions.LoadWeightToleranceByConfigid,
        error: HttpErrorResponse
      ) => {
        return new WeightToleranceActionTypesActions.LoadWeightToleranceByConfigidFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  updateTolerance$ = this.dataPersistence.pessimisticUpdate(
    WeightToleranceActionTypes.UPADTE_WEIGHT_TOLERANCE,
    {
      run: (
        action: WeightToleranceActionTypesActions.UpdateWeightTolerance
      ) => {
        return this.weightToleranceService
          .updateWeightTolerance(
            action.payload.newTolerance,
            action.payload.configId,
            action.payload.configProductId
          )
          .pipe(
            map(
              (updatedTolerance: UpdateTolerancePayloadSuccess) =>
                new WeightToleranceActionTypesActions.UpdateWeightToleranceSuccess(
                  updatedTolerance
                )
            )
          );
      },
      onError: (
        action: WeightToleranceActionTypesActions.UpdateWeightTolerance,
        error: HttpErrorResponse
      ) => {
        return new WeightToleranceActionTypesActions.UpdateWeightToleranceFailure(
          {
            id: action.payload.configProductId,
            actualTolerance: action.payload.actualTolerance,
            error: this.errorHandler(error)
          }
        );
      }
    }
  );

  @Effect()
  loadRangeWeight$ = this.dataPersistence.fetch(
    WeightToleranceActionTypes.LOAD_RANGE_WEIGHT,
    {
      run: (action: WeightToleranceActionTypesActions.LoadRangeWeight) => {
        return this.weightToleranceService
          .loadRangeWeight()

          .pipe(
            map(
              rangeWeight =>
                new WeightToleranceActionTypesActions.LoadRangeWeightSuccesss(
                  rangeWeight
                )
            )
          );
      },
      onError: (
        action: WeightToleranceActionTypesActions.LoadRangeWeight,
        error: HttpErrorResponse
      ) => {
        return new WeightToleranceActionTypesActions.LoadRangeWeightFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadProductCode$ = this.dataPersistence.fetch(
    WeightToleranceActionTypes.LOAD_PRODUCT_CODE,
    {
      run: (action: WeightToleranceActionTypesActions.LoadProductCode) => {
        return this.weightToleranceService
          .loadProductCode()

          .pipe(
            map(
              productGroupCode =>
                new WeightToleranceActionTypesActions.LoadProductCodeSuccess(
                  productGroupCode
                )
            )
          );
      },
      onError: (
        action: WeightToleranceActionTypesActions.LoadProductCode,
        error: HttpErrorResponse
      ) => {
        return new WeightToleranceActionTypesActions.LoadProductCodeFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
}
