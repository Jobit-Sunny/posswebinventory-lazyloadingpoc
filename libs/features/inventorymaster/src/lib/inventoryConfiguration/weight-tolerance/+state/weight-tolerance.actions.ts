import { Action } from '@ngrx/store';
import { CustomErrors } from '@poss-web/core';
import {
  SaveTolerance,
  ConfigDetailsList,
  ConfigDetails,
  Tolerance
} from '../models/weight-tolerance.models';
import { LocationMappingOption } from '@poss-web/shared';

export interface SaveTolerancePayload {
  tolerance: SaveTolerance;
  configId: any;
}
export interface LocationPayload {
  addLocations: string[];
  removeLocations: string[];
}
export interface LocationMapping {
  locations: LocationPayload;
  configId: number;
}

export interface UpdateTolerancePayloadSuccess {
  configProductId: number;
  newTolerance: SaveTolerance;
}
export interface UpdateTolerancePayload {
  configId: any;
  newTolerance: SaveTolerance;
  actualTolerance: SaveTolerance;
  configProductId: any;
}

export interface UpdateToleranceFailurePayload {
  id: any;
  actualTolerance: SaveTolerance;
  error: CustomErrors;
}
export interface InventoryConfigurationListingPayload {
  pageIndex: number;
  pageSize: number;
}
export interface LoadSelectedConfigDetailsPayload {
  configId: string;
}

export interface UpadateIsActivePayload {
  configId: string;
  isActive: boolean;
}
export enum WeightToleranceActionTypes {
  LOAD_COURIER_DETAILS = '[Load-courierDetails] Load Courier Details',
  LOAD_COURIER_DETAILS_SUCCESS = '[Load-CourierDetails] Load Courier Details Success',
  LOAD_COURIER_DETAILS_FAILURE = '[Load-CourierDetails] Load Courier Details Failure',
  LOAD_COURIER_DETAILS_BASED_ON_COURIERNAME = '[Load-CourierDetails] Load Courier Details Based On CorierName',
  LOAD_COURIER_DETAILS_BASED_ON_COURIERNAME_SUCCESS = '[Load-CourierDetails] Load Courier Details Based On CourierName Success',
  LOAD_COURIER_DETAILS_BASED_ON_COURIERNAME_FAILURE = '[Load-CourierDetails] Load Courier Details Based On CourierName Failure',

  LOAD_CONFIG_DETAILS = '[Weight-tolerance-listing] Load Config Details',
  LOAD_CONFIG_DETAILS_SUCCESS = '[Weight-tolerance-listing] Load Config Details Success',
  LOAD_CONFIG_DETAILS_FAILURE = '[Weight-tolerance-listing] Load Config Details Failure',

  LOAD_SELECTED_CONFIG_DETAILS = '[Weight-tolerance-edit] Load Selected ConfigDetails',
  LOAD_SELECTED_CONFIG_DETAILS_SUCCESSS = '[Weight-tolerance-edit] Load Selected Config Details Success',
  LOAD_SELECTED_CONFIG_DETAILS_FAILURE = '[Weight-tolerance-edit] Load Selected Config Details Failure',

  SAVE_CONFIG_DETAILS = '[Weight-tolerance-detail] Save Config Details',
  SAVE_CONFIG_DETAILS_SUCCESS = '[Weight-tolerance-detail] Save Config Details Success',
  SAVE_CONFIG_DETAILS_FAILURE = '[Weight-tolerance-detail] Save Config Details Failure',

  SAVE_WEIGHT_TOLERANCE_DETAILS = '[Weight-tolerance-listing] Save Weight Tolerance Details',
  SAVE_WEIGHT_TOLERANCE_DETAILS_SUCCESS = '[Weight-tolerance-listing] Save Weight Tolerance Details Success',
  SAVE_WEIGHT_TOLERANCE_DETAILS_FAILURE = '[Weight-tolerance-listing] Save Weight Tolerance Details Failure',

  LOAD_WEIGHT_TOLERANCE_BY_CONFIG_ID = '[Weight-tolerance-edit] Load Weight Tolerance',
  LOAD_WEIGHT_TOLERANCE_BY_CONFIG_ID_SUCCESS = '[Weight-tolerance-edit] Load Weight Tolerance Success',
  LOAD_WEIGHT_TOLERANCE_BY_CONFIG_ID_FAILURE = '[Weight-tolerance-edit] Load Weight Tolerance Failure',

  UPADTE_WEIGHT_TOLERANCE = '[Weight-tolerance-edit] Upadate Weight Tolerance',
  UPADTE_WEIGHT_TOLERANCE_SUCCESS = '[Weight-tolerance-edit] Update Weight Tolerance Success',
  UPADTE_WEIGHT_TOLERANCE_FAILURE = '[Weight-tolerance-edit] Update Weight Tolerance Failure',

  UPDATE_IS_ACTIVE = '[Weight-tolerance-list] Update Is Active',
  UPDATE_IS_ACTIVE_SUCCESS = '[Weight-tolerance-list] Update Is Active Success',
  UPDATE_IS_ACTIVE_FAILURE = '[Weight-tolerance-list]Update Is Active Failure',

  LOAD_PRODUCT_CODE = '[Weight-tolerance-detail] Load Product Code',
  LOAD_PRODUCT_CODE_SUCCESS = '[Weight-tolerance-detail] Load Product Code Success',
  LOAD_PRODUCT_CODE_FAILURE = '[Weight-tolerance-details] Load Product Code Failure',

  LOAD_RANGE_WEIGHT = '[Weight-tolerance-detail] Load Range Weight',
  LOAD_RANGE_WEIGHT_SUCCESS = '[Weight-tolerance-detail] Load Range Weight Success',
  LOAD_RANGE_WEIGHT_FAILURE = '[Weight-tolerance-detail] Load Range Weight Failre',

  LOAD_PRODUCT_CATEGORY_CODE = '[Weight-tolerance-detail] Load Product Category Code',
  LOAD_PRODUCT_CATEGORY_CODE_SUCCESS = '[Weight-tolerance-detail] Load Product Category Code Success',
  LOAD_PRODUCT_CATEGORY_CODE_FAILURE = '[Weight-tolerance-detail] Load Product Category Code Failure',

  LOAD_ORG_CODE = '[Weight-tolerance-detail] Load Org Code',
  LOAD_ORG_CODE_SUCCESS = '[Weight-tolerance-detail] Load Org Code Success',
  LOAD_ORG_CODE_FAILURE = '[Weight-tolerance-detail] Load Org Code Failure',

  LOAD_PRODUCT_CATEGORY_CODE_COUNT = '[Weight-tolerance-detail] Load ProductCategory Code Count',
  LOAD_PRODUCT_CATEGORY_CODE_COUNT_SUCCESS = '[Weight-tolerance-detail] Load ProductCategory Code Count Success',
  LOAD_PRODUCT_CATEGORY_CODE_COUNT_FAILURE = '[Weight-tolerance-detail] Load ProductCategory Code Count Failure',

  SEARCH_CONFIG_DETAILS_BY_CONFIG_ID = '[Weight-tolerance-detail] Search Config Details By Config Id',
  SEARCH_CONFIG_DETAILS_BY_CONFIG_ID_SUCCESS = '[Weight-tolerance-detail] Search Config Details By Config Id Success',
  SEARCH_CONFIG_DETAILS_BY_CONFIG_ID_FAILURE = '[Weight-tolerance-detail] Search Config Details By Config Id Failure',

  SAVE_LOCATION_MAPPING = '[Weight-tolerance-edit] Save Location Mapping',
  SAVE_LOCATION_MAPPING_SUCCESS = '[Weight-tolerance-edit] Save Location Mapping Success',
  SAVE_LOCATION_MAPPING_FAILURE = '[Weight-tolerance-edit] Save Location Mapping Failure',

  LOAD_MAPPED_LOCATION = '[Weight-tolerance-edit] Load Mapped Location',
  LOAD_MAPPED_LOCATION_SUCCESS = '[Weight-tolerance-edit] Load Mapped Location Success',
  LOAD_MAPPED_LOCATION_FAILURE = '[Weight-tolerance-edit] Load Mapped Location Failure',

  LOAD_RESET = '[Weight-tolerance-list]Load Reset',

  LOAD_EDIT_ENABLE = '[Weight-tolerance-detail] Load Edit Enable',
  RESET_IS_ACTIVE = '[Weight-tolerance-detail] Reset Is Active'
}

export class LoadProductCategoryCodeCount implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_PRODUCT_CATEGORY_CODE_COUNT
}

export class LoadProductCategoryCodeCountSuccess implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_PRODUCT_CATEGORY_CODE_COUNT_SUCCESS;
  constructor(public payload: any) { }
}

export class LoadProductCategoryCodeCountFailure implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_PRODUCT_CATEGORY_CODE_COUNT_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class UpdateIsActive implements Action {
  readonly type = WeightToleranceActionTypes.UPDATE_IS_ACTIVE;
  constructor(public payload: UpadateIsActivePayload) { }
}

export class UpdateIsActiveSuccess implements Action {
  readonly type = WeightToleranceActionTypes.UPDATE_IS_ACTIVE_SUCCESS;
  constructor(public payload: any) { }
}

export class UpdateIsActiveFailure implements Action {
  readonly type = WeightToleranceActionTypes.UPDATE_IS_ACTIVE_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadReset implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_RESET;
}
export class ResetIsActive implements Action {
  readonly type = WeightToleranceActionTypes.RESET_IS_ACTIVE;
}
export class LoadCourierDetails implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_COURIER_DETAILS;
  constructor(public payload: InventoryConfigurationListingPayload) { }
}

export class LoadCourierDetailsFailure implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_COURIER_DETAILS_FAILURE;
  constructor(public payload: CustomErrors) { }
}
export class LoadCourierDetailsBasedOnCourierName implements Action {
  readonly type =
    WeightToleranceActionTypes.LOAD_COURIER_DETAILS_BASED_ON_COURIERNAME;
  constructor(public payload: string) { }
}

export class LoadCourierDetailsBasedOnCourierNameFailure implements Action {
  readonly type =
    WeightToleranceActionTypes.LOAD_COURIER_DETAILS_BASED_ON_COURIERNAME_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadConfigDetails implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_CONFIG_DETAILS;
  constructor(public payload: InventoryConfigurationListingPayload) { }
}

export class LoadConfigDetailsSuccess implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_CONFIG_DETAILS_SUCCESS;
  constructor(public payload: ConfigDetailsList) { }
}
export class LoadConfigDetailsFailure implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_CONFIG_DETAILS_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadSelectedConfigDetails implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_SELECTED_CONFIG_DETAILS;
  constructor(public payload: string) { }
}

export class LoadSelectedConfigDetailsSuccess implements Action {
  readonly type =
    WeightToleranceActionTypes.LOAD_SELECTED_CONFIG_DETAILS_SUCCESSS;
  constructor(public payload: ConfigDetails) { }
}
export class LoadSelectedConfigDetailsFailure implements Action {
  readonly type =
    WeightToleranceActionTypes.LOAD_SELECTED_CONFIG_DETAILS_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class SaveConfigDetails implements Action {
  readonly type = WeightToleranceActionTypes.SAVE_CONFIG_DETAILS;
  constructor(public payload: ConfigDetails) { }
}

export class SaveConfigDetailsSuccess implements Action {
  readonly type = WeightToleranceActionTypes.SAVE_CONFIG_DETAILS_SUCCESS;
  constructor(public payload: ConfigDetails) { }
}
export class SaveConfigDetailsFailure implements Action {
  readonly type = WeightToleranceActionTypes.SAVE_CONFIG_DETAILS_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class SaveWeightTolerance implements Action {
  readonly type = WeightToleranceActionTypes.SAVE_WEIGHT_TOLERANCE_DETAILS;
  constructor(public payload: SaveTolerancePayload) { }
}

export class SaveWeightToleranceSuccess implements Action {
  readonly type =
    WeightToleranceActionTypes.SAVE_WEIGHT_TOLERANCE_DETAILS_SUCCESS;
  constructor() { }
}
export class SaveWeightToleranceFailure implements Action {
  readonly type =
    WeightToleranceActionTypes.SAVE_WEIGHT_TOLERANCE_DETAILS_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadWeightToleranceByConfigid implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_WEIGHT_TOLERANCE_BY_CONFIG_ID;
  constructor(public payload: string) { }
}

export class LoadWeightToleranceByConfigidSuccess implements Action {
  readonly type =
    WeightToleranceActionTypes.LOAD_WEIGHT_TOLERANCE_BY_CONFIG_ID_SUCCESS;
  constructor(public payload: Tolerance[]) { }
}
export class LoadWeightToleranceByConfigidFailure implements Action {
  readonly type =
    WeightToleranceActionTypes.LOAD_WEIGHT_TOLERANCE_BY_CONFIG_ID_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class UpdateWeightTolerance implements Action {
  readonly type = WeightToleranceActionTypes.UPADTE_WEIGHT_TOLERANCE;
  constructor(public payload: UpdateTolerancePayload) { }
}

export class UpdateWeightToleranceSuccess implements Action {
  readonly type = WeightToleranceActionTypes.UPADTE_WEIGHT_TOLERANCE_SUCCESS;
  constructor(public payload: UpdateTolerancePayloadSuccess) { }
}
export class UpdateWeightToleranceFailure implements Action {
  readonly type = WeightToleranceActionTypes.UPADTE_WEIGHT_TOLERANCE_FAILURE;
  constructor(public payload: UpdateToleranceFailurePayload) { }
}

export class LoadProductCode implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_PRODUCT_CODE;
}

export class LoadProductCodeSuccess implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_PRODUCT_CODE_SUCCESS;
  constructor(public payload: any) { }
}
export class LoadProductCodeFailure implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_PRODUCT_CODE_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadRangeWeight implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_RANGE_WEIGHT;
}

export class LoadRangeWeightSuccesss implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_RANGE_WEIGHT_SUCCESS;
  constructor(public payload: any) { }
}
export class LoadRangeWeightFailure implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_RANGE_WEIGHT_FAILURE;
  constructor(public payload: CustomErrors) { }
}
export class LoadProductCategoryCode implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_PRODUCT_CATEGORY_CODE;
  constructor() { }
}

export class LoadProductCategoryCodeSuccess implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_PRODUCT_CATEGORY_CODE_SUCCESS;
  constructor(public payload: any) { }
}
export class LoadProductCategoryCodeFailure implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_PRODUCT_CATEGORY_CODE_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadOrgCode implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_ORG_CODE;
}
export class LoadOrgCodeSucccess implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_ORG_CODE_SUCCESS;
  //TODO change type
  constructor(public payload: any) { }
}

export class LoadOrgCodeFailure implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_ORG_CODE_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class SearchConfigDetailsByConfigId implements Action {
  readonly type = WeightToleranceActionTypes.SEARCH_CONFIG_DETAILS_BY_CONFIG_ID;
  constructor(public payload: string) { }
}

export class SearchConfigDetailsByConfigIdSuccess implements Action {
  readonly type =
    WeightToleranceActionTypes.SEARCH_CONFIG_DETAILS_BY_CONFIG_ID_SUCCESS;
  constructor(public payload: ConfigDetailsList) { }
}

export class SearchConfigDetailsByConfigIdFailure implements Action {
  readonly type =
    WeightToleranceActionTypes.SEARCH_CONFIG_DETAILS_BY_CONFIG_ID_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class LoadEditEnable implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_EDIT_ENABLE;
}

export class LoadMappedLocation implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_MAPPED_LOCATION;
  //TODO change type
  constructor(public payload: any) { }
}

export class LoadMappedLocationSuccess implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_MAPPED_LOCATION_SUCCESS;
  constructor(public payload: { id: string; description: string }[]) { }
}

export class LoadMappedLocationFailure implements Action {
  readonly type = WeightToleranceActionTypes.LOAD_MAPPED_LOCATION_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class SaveLocationMapping implements Action {
  readonly type = WeightToleranceActionTypes.SAVE_LOCATION_MAPPING;
  constructor(public payload: LocationMapping) { }
}

export class SaveLocationMappingSuccess implements Action {
  readonly type = WeightToleranceActionTypes.SAVE_LOCATION_MAPPING_SUCCESS;
}

export class SaveLocationMappingFailure implements Action {
  readonly type = WeightToleranceActionTypes.SAVE_LOCATION_MAPPING_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export type WeightToleranceActions =
  | UpdateIsActiveSuccess
  | UpdateIsActiveFailure
  | UpdateIsActive
  | LoadCourierDetails
  | LoadCourierDetailsFailure
  | LoadCourierDetailsBasedOnCourierName
  | LoadCourierDetailsBasedOnCourierNameFailure
  | LoadConfigDetails
  | LoadConfigDetailsSuccess
  | LoadConfigDetailsFailure
  | LoadSelectedConfigDetails
  | LoadSelectedConfigDetailsSuccess
  | LoadSelectedConfigDetailsFailure
  | SaveConfigDetails
  | SaveConfigDetailsSuccess
  | SaveConfigDetailsFailure
  | SaveWeightTolerance
  | SaveWeightToleranceSuccess
  | SaveWeightToleranceFailure
  | LoadWeightToleranceByConfigid
  | LoadWeightToleranceByConfigidSuccess
  | LoadWeightToleranceByConfigidFailure
  | UpdateWeightTolerance
  | UpdateWeightToleranceSuccess
  | UpdateWeightToleranceFailure
  | LoadRangeWeight
  | LoadRangeWeightSuccesss
  | LoadRangeWeightFailure
  | LoadProductCode
  | LoadProductCodeSuccess
  | LoadProductCodeFailure
  | LoadProductCategoryCode
  | LoadProductCategoryCodeSuccess
  | LoadProductCategoryCodeFailure
  | LoadOrgCode
  | LoadOrgCodeSucccess
  | LoadOrgCodeFailure
  | SearchConfigDetailsByConfigId
  | SearchConfigDetailsByConfigIdSuccess
  | SearchConfigDetailsByConfigIdFailure
  | LoadEditEnable
  | LoadReset
  | LoadMappedLocation
  | LoadMappedLocationSuccess
  | LoadMappedLocationFailure
  | SaveLocationMapping
  | SaveLocationMappingSuccess
  | SaveLocationMappingFailure
  | LoadProductCategoryCodeCount
  | LoadProductCategoryCodeCountSuccess
  | LoadProductCategoryCodeCountFailure
  | ResetIsActive;
