import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { Tolerance } from '../models/weight-tolerance.models';


export interface ToleranceEntity extends EntityState<Tolerance>{

}

export const ToleranceAdaptor=createEntityAdapter<Tolerance>({selectId:toleranceItem=>toleranceItem.id})


export const toleranceSelector=ToleranceAdaptor.getSelectors()
