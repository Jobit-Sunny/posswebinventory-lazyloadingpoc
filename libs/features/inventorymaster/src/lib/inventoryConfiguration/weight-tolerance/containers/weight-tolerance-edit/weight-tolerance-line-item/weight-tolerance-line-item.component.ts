import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { UpdateTolerancePayload } from '../../../+state/weight-tolerance.actions';
import { WeightToleranceFacade } from '../../../+state/weight-tolerance-facade';
import { ConfirmDialogComponent } from 'libs/features/inventorymaster/src/lib/master/common/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';
@Component({
  selector: 'poss-web-weight-tolerance-line-item',
  templateUrl: './weight-tolerance-line-item.component.html',
  styleUrls: ['./weight-tolerance-line-item.component.scss']
})
export class WeightToleranceLineItemComponent implements OnInit, OnDestroy {
  @Input() toleranceItem;
  @Input() configDetails;
  @Input() productGroupCode;
  @Input() rangeWeight;

  @Output() updateToleranceValue = new EventEmitter<UpdateTolerancePayload>();
  @Output() isEditEnable = new EventEmitter<any>();
  rangeData: any;
  destroy$ = new Subject();
  editenable$: Observable<any>;
  editenable;
  selected1:string
  selected2:string

  description: string;
  productGroupCodedescription: string;
  formData = new FormGroup({
    productGroupCode: new FormControl(),
    description: new FormControl(),
    rangeWeightValue: new FormControl(),
    toleranceValue: new FormControl('', [
      Validators.pattern('^(0(.[0-9]+)?|1(.0+)?)$')
    ])
  });
  formGroup: any;
  SaveTolerance: any;

  constructor(private inventoryConfigurationFacade: WeightToleranceFacade,  public dialog: MatDialog) {}


  ngOnInit() {
    this.selected1=this.toleranceItem.productGroupCode
  if(this.selected1){
    const obj = this.productGroupCode.filter(
      data => data.productGroupCode === this.selected1
    );

    this.description = obj[0]?obj[0].description:'';
  }
    this.selected2=this.toleranceItem.rangeId
    const productGroupCode = this.productGroupCode.filter(
      data => data.productGroupCode === this.toleranceItem.productGroupCode
    );

    const rangeObject = this.rangeWeight.filter(
      data => data.id === this.toleranceItem.rangeId
    );
    if (rangeObject[0]) {
      this.rangeData = {
        fromRange: rangeObject[0].fromRange,
        toRange: rangeObject[0].toRange
      };
    }
    if (productGroupCode[0]) {
      this.productGroupCodedescription = productGroupCode[0].description
        ? productGroupCode[0].description
        : '';
    }
    this.editenable = false;
    this.editenable$ = this.inventoryConfigurationFacade.getIsEditEnable();
  }

  change(event) {
    const obj = this.productGroupCode.filter(
      data => data.productGroupCode === event.value
    );

    this.description = obj[0]?obj[0].description:'';
  }
  edit() {
    // this.isEditEnable.emit(true);
    this.editenable = true;
  }

  updateTolerance(formGroup : FormGroup) {
    const data = {
      configId: this.configDetails.configId,
      newTolerance: {
        productGroupCode: this.formData.value['productGroupCode']?this.formData.value['productGroupCode']: this.toleranceItem.productGroupCode,
        rangeId: this.formData.value['rangeWeightValue']?this.formData.value['rangeWeightValue']:this.toleranceItem.rangeId,
        value: this.formData.value['toleranceValue']?this.formData.value['toleranceValue']:this.toleranceItem.value
      },
      actualTolerance: {
        productGroupCode: this.toleranceItem.productGroupCode,
        rangeId: this.toleranceItem.rangeId,
        value: this.toleranceItem.value
      },
      configProductId: this.toleranceItem.id
    };



    // this.editenable$.pipe(takeUntil(this.destroy$)).subscribe(editdata => {
    //   if (!editdata) {
    //     this.editenable = false;
    //   }
    // });
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.editenable=false;
        console.log('inside the pop up'+ data)
        this.updateToleranceValue.emit(data);
      }
      else{

        this.editenable=false;
      }
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
