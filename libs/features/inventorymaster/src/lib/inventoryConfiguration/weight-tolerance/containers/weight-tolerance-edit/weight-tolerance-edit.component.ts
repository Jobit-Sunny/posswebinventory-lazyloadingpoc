import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Observable, Subject } from 'rxjs';

import { takeUntil, take } from 'rxjs/operators';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationEventType
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { ProductGroupCode } from '../../models/weight-tolerance.models';
import { WeightToleranceFacade } from '../../+state/weight-tolerance-facade';
import {
  LocationMappingService,
  LocationMappingServiceResponse,
  CustomErrors
} from '@poss-web/core';
import { getWeighttoleranceRouteUrl, getWeighttoleranceDetailsRouteUrl } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-weight-tolerance-edit',
  templateUrl: './weight-tolerance-edit.component.html',
  styleUrls: ['./weight-tolerance-edit.component.scss']
})
export class WeightToleranceEditComponent implements OnInit, OnDestroy {
  configId: any;
  isConfigId;
  hasNotification;
  destroy$ = new Subject();
  productGroupCode: ProductGroupCode[] = [];
  rangeWeight: any; //TODO Change type any
  configDetails$: Observable<any>;
  toleranceItem$: Observable<any>;
  isWeightToleranceSuccess: boolean;
  locationMappingSuccess: boolean;
  count: number[] | string[];
  isUpdated: boolean;
  isLoading$: Observable<boolean>;
  locationMappingContentTemplateRef: any;
  selectedLocations: { id: string; description: string }[];
  productCategoryCode: any;
  constructor(
    private translate: TranslateService,
    private locationMappingService: LocationMappingService,
    private router: Router,
    private params: ActivatedRoute,
    private weightToleranceFacade: WeightToleranceFacade,
    private route: Router,
    private overlayNotification: OverlayNotificationService
  ) { }

  ngOnInit() {
    this.isLoading$ = this.weightToleranceFacade.getIsLoading();
    this.configId = this.params.snapshot.params['configId'];
    this.weightToleranceFacade.loadProductCategoryCode();
    this.weightToleranceFacade.loadMappedLocations(this.configId);
    this.weightToleranceFacade.loadProductGroupCode();
    this.weightToleranceFacade.loadRangeWeight();
    this.weightToleranceFacade.loadSelectedConfigDetails(this.configId);
    this.weightToleranceFacade.loadWeightToleranceByConfigId(this.configId);
    this.configDetails$ = this.weightToleranceFacade.getSelectedConfigId();
    this.toleranceItem$ = this.weightToleranceFacade.getToleranceItemByConfigId();
    this.productCategoryCode = this.weightToleranceFacade.getProductCategoryCode();

    this.weightToleranceFacade
      .getMappedLocations()
      .pipe(takeUntil(this.destroy$))
      .subscribe(locations => {
        this.selectedLocations = locations;
      });
    this.weightToleranceFacade
      .getSelectToleranceItemsIds()
      .subscribe(count => (this.count = count));

    this.weightToleranceFacade
      .getProductGroupCode()
      .subscribe(data => (this.productGroupCode = data));

    this.weightToleranceFacade
      .getRangeWeight()
      .subscribe(data => (this.rangeWeight = data));

    this.weightToleranceFacade
      .getSelectIsLocationMappingSuccess()
      .pipe(takeUntil(this.destroy$))
      .subscribe(locationMappingSuccess => {
        this.locationMappingSuccess = locationMappingSuccess;
        if (this.locationMappingSuccess === true) {
          const key = 'pw.weighttolerance.locationMappingSuccessMsg';
          this.weightToleranceFacade.loadMappedLocations(this.configId);
          this.showNotification(key);
        } else {
          this.overlayNotification.close();
        }
      });
    this.weightToleranceFacade
      .getIsUpdated()
      .pipe(takeUntil(this.destroy$))
      .subscribe(hasUpdated => {
        if (hasUpdated === true) {
          this.showNotification('pw.weighttolerance.toleranceUpdateMsg');
        }
      });
    this.weightToleranceFacade
      .getHasSaved()
      .pipe(takeUntil(this.destroy$))
      .subscribe(hasSaved => {
        this.isWeightToleranceSuccess = hasSaved;
        if (this.isWeightToleranceSuccess === true) {
          this.showNotification('pw.weighttolerance.toleranceSaveMsg');
          this.weightToleranceFacade.loadWeightToleranceByConfigId(
            this.configId
          );
        } else if (hasSaved === false) {
          this.overlayNotification.close();
        }
      });

    this.weightToleranceFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe(error => {
        if (error) {
          this.errorHandler(error);
        }
      });
  }

  updateIsActive(updateIsActive) {
    this.weightToleranceFacade.updateIsActive(updateIsActive);
  }
  openLocationMapping() {
    this.locationMappingService
      .open({
        selectedLocations: this.selectedLocations
      })
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: LocationMappingServiceResponse) => {
        if (res.type === 'apply') {
          const addedLocation = [];
          const removedLocation = [];

          for (const data of res.data.addedLocations) {
            addedLocation.push(data.description);
          }
          for (const data of res.data.removedLocations) {
            removedLocation.push(data.description);
          }

          const locationMappingPayload = {
            locations: {
              addLocations: addedLocation,
              removeLocations: removedLocation
            },
            configId: this.configId
          };
          this.weightToleranceFacade.saveLocationMapping(
            locationMappingPayload
          );
        }
      });
  }

  back() {
    this.weightToleranceFacade.loadReset();
    this.route.navigate([getWeighttoleranceRouteUrl()]);
  }

  enableEdit(event) {
    if (event) {
      this.weightToleranceFacade.loadEditEnable();
    }
  }
  saveTolerance(savetolerance: any) {
    if (
      savetolerance.tolerance.productGroupCode !== null &&
      savetolerance.tolerance.rangeId !== null &&
      savetolerance.tolerance.value !== null
    ) {
      if (this.count.length === 50) {
        this.showNotification('pw.weighttolerance.tolerancelimitExecdedMsg.');
      } else {
        this.weightToleranceFacade.saveWeightTolerance(savetolerance);
      }
    } else {
      // const key = 'Please Enter all the fields';
      // this.showNotification(key);
    }
  }
  updateTolerance(updatePayload) {
    const newChanges = updatePayload.newTolerance;
    const actualChanges = updatePayload.actualTolerance;
    let newTolerance;
    if (
      newChanges.productGroupCode !== null &&
      newChanges.rangeId !== null &&
      newChanges.value !== null
    ) {
      newTolerance = {
        productGroupCode: newChanges.productGroupCode,
        rangeId: newChanges.rangeId,
        value: newChanges.value
      };
    }

    this.weightToleranceFacade.updateToleranceData({
      configId: updatePayload.configId,
      newTolerance: newTolerance,
      actualTolerance: actualChanges,
      configProductId: updatePayload.configProductId
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  showNotification(key: any) {
    this.hasNotification = false;
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMessage,
            time: 2000,
            hasBackdrop: true
          })
          .events.subscribe((eventType: OverlayNotificationEventType) => { });
      });
  }
  errorHandler(error: CustomErrors) {
    this.overlayNotification.show({
      type: OverlayNotificationType.ERROR,
      hasClose: true,
      error: error
    });
  }
  addNew() {
    this.router.navigate([getWeighttoleranceDetailsRouteUrl()]);
  }
}
