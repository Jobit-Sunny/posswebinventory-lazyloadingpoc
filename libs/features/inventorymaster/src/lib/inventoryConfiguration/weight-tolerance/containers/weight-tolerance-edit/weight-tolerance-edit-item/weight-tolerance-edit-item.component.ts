import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  AfterViewInit,
  OnDestroy
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SaveTolerancePayload } from '../../../+state/weight-tolerance.actions';
// tslint:disable-next-line: nx-enforce-module-boundaries
import { ConfirmDialogComponent } from 'libs/features/inventorymaster/src/lib/master/common/confirm-dialog/confirm-dialog.component';
import { takeUntil } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { Subject } from 'rxjs';

@Component({
  selector: 'poss-web-weight-tolerance-edit-item',
  templateUrl: './weight-tolerance-edit-item.component.html',
  styleUrls: ['./weight-tolerance-edit-item.component.scss']
})
export class WeightToleranceEditItemComponent implements OnInit {
  destroy$: Subject<null> = new Subject<null>();

  @Input() configDetails;
  @Input() toleranceItem;
  @Input() productGroupCode;
  @Input() rangeWeight;
  @Input() productCategoryCode;

  @Output() emitToleranceData = new EventEmitter<SaveTolerancePayload>();
  @Output() emitEditEnable = new EventEmitter<any>();
  @Output() updateToleranceData = new EventEmitter<any>();
  @Output() updateIsActive = new EventEmitter<any>();

  productCategoryValue;
  description;
  formData = new FormGroup({
    productGroupCode: new FormControl([Validators.required]),
    descrption: new FormControl([Validators.required]),
    rangeWeight: new FormControl([Validators.required]),
    toleranceValue: new FormControl('', [
      Validators.required,
      Validators.pattern('^(0(.[0-9]+)?|1(.0+)?)$')
    ])
  });
  constructor(public dialog: MatDialog) {}

  ngOnInit() {
    const obj = this.productCategoryCode.filter(
      data =>
        data.productCategoryCode === this.configDetails.productCategoryCode
    );
    this.productCategoryValue = obj[0] ? obj[0].description : '';
  }

  enableEdit(event) {
    this.emitEditEnable.emit(event);
  }
  saveWeightTolerances() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(result => {
        if (result) {
          const formValues = {
            configId: this.configDetails.configId,
            tolerance: {
              productGroupCode: this.formData.value['productGroupCode'],
              rangeId: this.formData.value['rangeWeight'],
              value: this.formData.value['toleranceValue']
            }
          };
          this.emitToleranceData.emit(formValues);
        }
      });
  }
  updateToleranceItem(event) {

    this.updateToleranceData.emit(event);
  }
  cancel() {
    // this.formData.reset();
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(result => {
        if (result) {
          this.ngOnInit();
        }
      });
  }
  selctionChange(event) {
    const obj = this.productGroupCode.filter(
      data => data.productGroupCode === event.value
    );

    this.description = obj[0].description;
  }
  change(value) {
    const obj = {
      configId: this.configDetails.configId,
      isActive: value
    };
    this.updateIsActive.emit(obj);
  }
}
