import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Observable, Subject, pipe } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationEventType
} from '@poss-web/shared';
import { RangeWeight } from '../../models/weight-tolerance.models';
import { WeightToleranceFacade } from '../../+state/weight-tolerance-facade';
import { PageEvent, MatDialog } from '@angular/material';
import { CustomErrors } from '@poss-web/core';
import { ConfirmDialogComponent } from '../../../../master/common/confirm-dialog/confirm-dialog.component';
import { getWeighttoleranceRouteUrl, getWeighttoleranceEditRouteUrl } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-weight-tolerance-detail',
  templateUrl: './weight-tolerance-detail.component.html',
  styleUrls: ['./weight-tolerance-detail.component.scss']
})
export class WeightToleranceDetailComponent implements OnInit {
  destroy$: Subject<null> = new Subject<null>();
  configId: any;
  configIdParam: any;
  hasNotification = false;
  isActive: boolean;
  productCategoryCode: any; //TODO Change type
  orgCodes: any; //TODO Change  type
  rangeWeight: RangeWeight[] = [];
  selectConfigId$: Observable<any>;
  form = new FormGroup({
    configId: new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-zA-Z0-9]{1,5}$')
    ]),
    description: new FormControl('', [Validators.required]),
    productCategoryCode: new FormControl('', [Validators.required]),
    OrganisationCode: new FormControl(),
    configType: new FormControl()
  });
  // dialog: any;

  constructor(
    private routerActivate: ActivatedRoute,
    private router: Router,
    private weightToleranceFacade: WeightToleranceFacade,
    private translate: TranslateService,
    private overlayNotification: OverlayNotificationService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.configId = this.routerActivate.snapshot.params['configId'];
    this.weightToleranceFacade.loadRangeWeight();
    this.weightToleranceFacade.loadProductCategoryCode();
    this.weightToleranceFacade.loadOrgCode();
    this.selectConfigId$ = this.weightToleranceFacade.getSelectedConfigId();
    this.weightToleranceFacade
      .getProductCategoryCode()
      .subscribe(data => (this.productCategoryCode = data));

    this.weightToleranceFacade
      .getRangeWeight()
      .subscribe(data => (this.rangeWeight = data));
    this.weightToleranceFacade
      .getOrgCode()
      .subscribe(data => (this.orgCodes = data));
    this.weightToleranceFacade.getHasSaved().subscribe(hasSaved => {
      if (hasSaved === true) {
        this.selectConfigId$.pipe(takeUntil(this.destroy$)).subscribe(data => {
          if (data) {
            this.destroy$.next();
            this.destroy$.complete();
            this.router.navigate([getWeighttoleranceEditRouteUrl(data.configId)]);
            this.weightToleranceFacade.loadReset();
          }
        });
      }
    });
    this.weightToleranceFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });
  }

  change(value) {
    this.isActive = value;
  }
  cancel() {
    // this.form.reset();
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(result => {
        if (result) {
          this.ngOnInit();
        }
      });
  }
  back() {
    this.weightToleranceFacade.loadReset();
    this.router.navigate([getWeighttoleranceRouteUrl()]);
  }
  saveConfigDetials() {
    const data = {
      configId: this.form.value.configId,
      description: this.form.value.description,
      configType: 'WEIGHT_TOLERANCE',
      OrganisationCode: 'TJ',
      productCategoryCode: this.form.value.productCategoryCode
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(result => {
        if (
          result &&
          data.configId !== null &&
          data.description !== null &&
          data.productCategoryCode !== null
        ) {
          this.weightToleranceFacade.saveConfigDetails({
            configId: data.configId,
            description: data.description,
            configType: 'WEIGHT_TOLERANCE',
            orgCode: 'TJ',
            productCategoryCode: data.productCategoryCode,
            isActive: this.isActive ? this.isActive : false
          });
        }
      });
  }

  errorHandler(error) {
    this.overlayNotification.show({
      type: OverlayNotificationType.ERROR,
      hasClose: true,
      error: error
    });
  }
}
