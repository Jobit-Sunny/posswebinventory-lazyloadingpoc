import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy
} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { fromEvent, Subject, Observable } from 'rxjs';
import { debounceTime, takeUntil, take } from 'rxjs/operators';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import { PageEvent } from '@angular/material';
import { Router } from '@angular/router';
import { ConfigDetails } from '../../models/weight-tolerance.models';
import { WeightToleranceFacade } from '../../+state/weight-tolerance-facade';

import {
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationEventType,
  OverlayNotificationService,
  ErrorEnums
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { getInventoryMasterDashboardRouteUrl, getWeighttoleranceDetailsRouteUrl, getWeighttoleranceEditRouteUrl } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-weight-tolerance-listing',
  templateUrl: './weight-tolerance-listing.component.html',
  styleUrls: ['./weight-tolerance-listing.component.scss']
})
export class WeightToleranceListingComponent
  implements OnInit, AfterViewInit, OnDestroy {
  searchErrorCode: string;
  pageSizeOptions: number[] = [];
  minPageSize = 0;
  pageSize;
  isSearching$: Observable<boolean>;
  configDetails$: Observable<any>;
  totalCount$: Observable<any>;
  configDetailsListingPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };
  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;
  searchForm = new FormGroup({
    searchValue: new FormControl()
  });
  destroy$ = new Subject<null>();
  isLoading$: Observable<boolean>;
  searchedData$: Observable<ConfigDetails>;
  hasError$: Observable<CustomErrors>;
  constructor(
    private appSettingFacade: AppsettingFacade,
    private weightToleranceFacade: WeightToleranceFacade,
    private router: Router,
    private translate: TranslateService,
    private overlayNotification: OverlayNotificationService
  ) { }

  ngOnInit() {
    this.hasError$ = this.weightToleranceFacade.getError();
    this.isSearching$ = this.weightToleranceFacade.getIsSearching();
    this.searchErrorCode = ErrorEnums.ERR_INV_029;
    this.appSettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.pageSize = data;
      });
    this.appSettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.configDetailsListingPageEvent.pageSize = data;
        this.loadConfigDetails();
      });

    this.configDetails$ = this.weightToleranceFacade.getConfigDetails();
    this.totalCount$ = this.weightToleranceFacade.getTotalConfigDetail();
    this.isLoading$ = this.weightToleranceFacade.getIsLoading();
    this.weightToleranceFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.weightToleranceFacade
      .getIsActiveToggle()
      .pipe(takeUntil(this.destroy$))
      .subscribe(d => {
        if (d) {
          this.showNotification('pw.weighttolerance.toleranceUpdateMsg');
          this.weightToleranceFacade.resetIsActiveToggle();
        }
      });
  }

  ngAfterViewInit(): void {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe(event => {
        const searchValue = this.searchForm.value.searchValue;

        if (searchValue) {
          this.search(searchValue);
        } else {
          this.clearSearch();
        }
      });
  }

  updateIsActive(updateIsActive) {
    this.weightToleranceFacade.updateIsActive(updateIsActive);
  }
  search(configId) {
    this.weightToleranceFacade.searchConfigDetailsByConfigId(configId);
  }
  clearSearch() {
    this.weightToleranceFacade.loadReset();
    this.searchForm.reset();
    this.loadConfigDetails();
  }

  addNew() {
    this.router.navigate([getWeighttoleranceDetailsRouteUrl()]);
  }
  loadConfigDetails() {
    this.weightToleranceFacade.loadConfigDetails(
      this.configDetailsListingPageEvent
    );
  }
  loadSelectedWeightTolerance(configId: string) {
    this.router.navigate([getWeighttoleranceEditRouteUrl(configId)]);
  }

  errorHandler(error: CustomErrors) {
    // let key = '';
    // //check if the error is the custom error
    // if (customErrorTranslateKeyMapMasters.has(error.code)) {
    //   //Obtain the transation key which will be use to obtain the translated error message
    //   //based on the language selected. Default is the english language(refer en.json from asset folder).
    //   key = customErrorTranslateKeyMapMasters.get(error.code);
    // } else {
    //   key = 'pw.global.genericErrorMessage';
    // }

    if (error.code === this.searchErrorCode) {
      // We are not showing error for location not found from search.
      return;
    }
    this.overlayNotification.show({
      type: OverlayNotificationType.ERROR,
      hasClose: true,
      error: error
    });
  }
  paginate(pageEvent: PageEvent) {
    this.configDetailsListingPageEvent = pageEvent;
    this.loadConfigDetails();
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
  back() {
    this.router.navigate([getInventoryMasterDashboardRouteUrl()]);
    this.weightToleranceFacade.loadReset();
  }
  omit_special_char($event: KeyboardEvent) {
    const pattern = /^[-_A-Za-z0-9]$/;
    return pattern.test($event.key);
  }

  showNotification(key: string) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMessage,
            time: 2000,
            hasBackdrop: true
          })
          .events.subscribe((eventType: OverlayNotificationEventType) => {
            this.overlayNotification.close();
          });
      });
  }
}
