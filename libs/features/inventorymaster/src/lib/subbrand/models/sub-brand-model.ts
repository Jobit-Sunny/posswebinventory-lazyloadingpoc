export interface BrandMaster {
  brandCode: string;
  description: string;
  parentBrandCode: string;
  configDetails: any;
  orgCode: string;
  isActive: boolean;
}

export interface SubBrandMaster{
  brandCode:string;
  description:string,
  configDetails:any

}
export enum subBrandEnum {
  NEW = 'NEW',
  new = 'new',
  edit = 'edit'
}
