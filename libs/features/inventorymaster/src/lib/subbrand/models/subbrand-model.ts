import { Validators } from '@angular/forms';
import {
  DynamicFormFieldsBuilder,
  FormField,
  FormFieldType,
  Validation,
  Class
} from '@poss-web/shared';

export class SubBrandModel extends DynamicFormFieldsBuilder {
  private id: number;
  @FormField({
    fieldType: FormFieldType.TEXT,
    label: 'pw.brandMaster.subBrandCode',
    validationErrorMessages: [{ errorType: 'pattern', errorMessage: 'pw.subBrand.subBrand_pattern'}]
  })
  @Validation({ validators: [Validators.required,Validators.pattern('^[A-Za-z0-9]{5,20}$')] })
  @Class({ className: ['col'] })
  private subbrandCode: string;

  @FormField({
    fieldType: FormFieldType.TEXT_AREA,
    label: 'pw.brandMaster.description'
  })
  @Validation({ validators: [Validators.required] })
  @Class({ className: ['col-12'] })
  private description: string;

  @FormField({
    fieldType: FormFieldType.CHECKBOX,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'checked'
    },
    label: ''
  })
  @Class({ className: ['col-4', 'pl-0', 'align-self-center'] })
  private isActive: { id: string; name: string; checked?: boolean }[];


  @FormField({
    fieldType: FormFieldType.SELECT,
    selectOptionKeys: {
      labelKey: 'name',
      valueKey: 'id',
      selectedKey: 'selected'
    },
    label: 'pw.brandMaster.parentBrandCode'
  })
  @Class({ className: ['col-8'] })
  @Validation({ validators: [Validators.required] })
  private parentBrandCode: { id: string; name: string, selected?: boolean }[];
  constructor(
    id: number,
    brandCode: string,
    description: string,
    isActive: { id: string; name: string; checked?: boolean }[],
    parentBrandCode: { id: string; name: string; selected?: boolean }[]
  ) {
    super();
    this.id = id;
    this.subbrandCode = brandCode;
    this.description = description;
    this.isActive = isActive;
    this.parentBrandCode = parentBrandCode
  }
}
