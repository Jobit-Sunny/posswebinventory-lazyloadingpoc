import { Injectable } from '@angular/core';
import { getBrandListUrl } from '../../endpoint.constants';
import { ApiService } from '@poss-web/core';
import { map } from 'rxjs/operators';
import { BrandMasterAdaptors } from '../../brand-master/adaptors/brand-master-adaptors';

@Injectable({
  providedIn: 'root'
})
export class SubbrandService {
  constructor(private apiservice: ApiService) {}
  getBrandMasterList(pageIndex, pageSize) {
    const url = getBrandListUrl(pageIndex, pageSize);
    return this.apiservice
      .get(url)
      .pipe(map(data => BrandMasterAdaptors.getBrandMasterList(data)));
  }
}
