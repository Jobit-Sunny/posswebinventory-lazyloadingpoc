import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'poss-web-subbrand-list-items',
  templateUrl: './subbrand-list-items.component.html',
  styleUrls: ['./subbrand-list-items.component.scss']
})
export class SubbrandListItemsComponent implements OnInit {
  @Input() SubbrandList;
  @Input() count;
  @Input() pageEvent;
  @Input() pageSize;

  @Output() paginator = new EventEmitter<PageEvent>();
  @Output() brandCode = new EventEmitter<any>();
  @Output() emitToggleValue = new EventEmitter<any>();
  pageSizeOptions: number[] = [];
  minPageSize = 0;
  constructor() {}

  ngOnInit() {
    this.pageSizeOptions = this.pageSize;
    this.minPageSize = this.pageSizeOptions.reduce((a: number, b: number) =>
      a < b ? a : b
    );
  }

  emitBrandCode(brandCode) {
    this.brandCode.emit(brandCode);
  }
  emitToggle(event) {
    this.emitToggleValue.emit(event);
  }
}
