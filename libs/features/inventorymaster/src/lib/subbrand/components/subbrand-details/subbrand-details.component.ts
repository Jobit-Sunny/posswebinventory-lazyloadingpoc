import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { subBrandEnum } from '../../models/sub-brand-model';
import { Subject } from 'rxjs';
import { SubBrandModel } from '../../models/subbrand-model';
import { TEMPLATE8, HelperFunctions } from '@poss-web/shared';
import { ConfirmDialogComponent } from '../../../master/common/confirm-dialog/confirm-dialog.component';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'poss-web-subbrand-details',
  templateUrl: './subbrand-details.component.html',
  styleUrls: ['./subbrand-details.component.scss']
})
export class SubbrandDetailsComponent implements OnInit, OnDestroy {
  dialogData: any;

  destroy$: Subject<null> = new Subject<null>();
  readOnly: boolean;
  subBrandEnum: subBrandEnum;

  brandCode: string;
  description: string;
  isActive: boolean;
  parentBrandCode: string;
  /// below is dynamic form specific code
  public currentStyle: string[];
  public formFields: any;
  /// above is dynamic form specific code
  constructor(
    private hf: HelperFunctions,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<SubbrandDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    fb: FormBuilder
  ) {
    this.dialogData = data;
  }

  ngOnInit() {
    const form = this.prepareSet();
    this.formFields = this.getInputs(form);
    this.currentStyle = this.getCssProp();
  }
  onClose() {
    this.dialogRef.close();
  }

  prepareSet() {
    const parentBrands = this.prepareParentBrands(
      this.dialogData.parentBrandDetails
    );

    const subBrand = new SubBrandModel(
      1,
      this.dialogData.subBrandDetails
        ? this.dialogData.subBrandDetails.brandCode === 'NEW'
          ? ''
          : this.dialogData.subBrandDetails.brandCode
        : '',

      this.dialogData
        ? this.dialogData.subBrandDetails
          ? this.dialogData.subBrandDetails.description
          : ''
        : '',
      [
        {
          id: '1',
          name: 'pw.subBrand.isActiveLabel',

          checked: this.dialogData
            ? this.dialogData.subBrandDetails
              ? this.dialogData.subBrandDetails.configDetails
                ? this.dialogData.subBrandDetails.configDetails.subBrandConfig
                  ? this.dialogData.subBrandDetails.configDetails.subBrandConfig
                      .isActive
                    ? this.dialogData.subBrandDetails.configDetails
                        .subBrandConfig.isActive.isActive
                    : false
                  : false
                : false
              : false
            : false
        }
      ],
      parentBrands
    );

    return subBrand;
  }

  prepareParentBrands(parentBrands) {
    const array = parentBrands.filter(
      parentBrandCode => parentBrandCode.parentBrandCode === ''
    );

    const parentBrandMasters: { id: string; name: string }[] = [];
    for (const parentBranddata of array) {
      parentBrandMasters.push({
        id: parentBranddata.brandCode,
        name: parentBranddata.brandCode
      });
    }

    const parentBrand = this.hf.patchValue(
      parentBrandMasters,
      'id',
      'selected',
      this.dialogData.subBrandDetails
        ? this.dialogData.subBrandDetails.parentBrandCode
        : '',
      true
    );

    return parentBrand;
  }
  getCssProp() {
    const annot = (SubbrandDetailsComponent as any).__annotations__;
    return annot[0].styles;
  }

  public getInputs(form) {
    return {
      formConfig: this.setFormConfig(),
      formFields: form.buildFormFields()
    };
  }
  public setFormConfig() {
    return {
      formName: 'Sub Brand Form',
      formDesc: 'Sub Brand',
      formTemplate: TEMPLATE8
    };
  }

  addButton(formGroup: FormGroup) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.saveConfirmation'
    });
    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(result => {
        if (result) {
          const formValues = formGroup.getRawValue();
          this.brandCode = formValues['1-subbrandCode'];
          this.description = formValues['1-description'];
          this.isActive = formValues['1-isActive'][0];
          this.parentBrandCode = formValues['1-parentBrandCode'];
          this.onCreate();
        }
      });
    // this.tabOne.emit(formData);
  }
  onCreate() {
    let mode = '';
    if (this.dialogData.subBrandDetails.brandCode !== subBrandEnum.NEW) {
      mode = subBrandEnum.edit;
    } else {
      mode = subBrandEnum.new;
    }

    this.dialogRef.close({
      brandCode: this.brandCode,
      description: this.description,
      configDetails: {
        isActive: this.isActive
      },
      parentBrandCode: this.parentBrandCode,
      mode: mode
    });
  }
  deleteButton() {
    // this.ngOnDestroy();
    // this.destroy$ = new Subject<null>();
    // this.ngOnInit();
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: 'pw.inventoryMasters.cancelConfirmation'
    });
    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(result => {
        if (result) {
          this.ngOnDestroy();
          this.destroy$ = new Subject<null>();
          this.ngOnInit();
        }
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
  public formGroupCreated(formGroup: FormGroup) {
    if (this.dialogData.subBrandDetails.brandCode !== 'NEW') {
      formGroup.get('1-subbrandCode').disable({ onlySelf: true });
    }
  }
}
