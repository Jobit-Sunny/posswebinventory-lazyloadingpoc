import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'poss-web-subbrand-list-item',
  templateUrl: './subbrand-list-item.component.html',
  styleUrls: ['./subbrand-list-item.component.scss']
})
export class SubbrandListItemComponent implements OnInit {
  @Input() listItem;
  @Output() emitBrandCode = new EventEmitter<any>();
  @Output() emitToggle = new EventEmitter<any>();

  checked: false;

  constructor() {}

  ngOnInit() {
    this.checked = this.listItem.isActive;
  }

  change(event) {
    const obj = {
      isActive: event.checked,
      brandCode: this.listItem.brandCode
    };
    this.emitToggle.emit(obj);
  }
  getSubBrandCode(brandCode) {
    this.emitBrandCode.emit(brandCode);
  }
}
