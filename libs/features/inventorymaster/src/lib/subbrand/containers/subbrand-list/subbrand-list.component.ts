import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy
} from '@angular/core';
import { Observable, Subject, fromEvent } from 'rxjs';
import { BrandMaster } from '../../../brand-master/models/brand-model';
import { PageEvent, MatDialog } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { BrandMasterFacade } from '../../../brand-master/+state/brand-master.facade';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import { debounceTime, takeUntil, take } from 'rxjs/operators';

import { subBrandEnum, SubBrandMaster } from '../../models/sub-brand-model';
import { SubbrandDetailsComponent } from '../../components/subbrand-details/subbrand-details.component';
import { TranslateService } from '@ngx-translate/core';
import {
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationEventType,
  OverlayNotificationService,
  ErrorEnums
} from '@poss-web/shared';
import { getProductMasterDashboardRouteUrl } from '../../../page-route.constants';

@Component({
  selector: 'poss-web-subbrand-list',
  templateUrl: './subbrand-list.component.html',
  styleUrls: ['./subbrand-list.component.scss']
})
export class SubbrandListComponent implements OnInit, AfterViewInit, OnDestroy {
  SubbrandList$: Observable<BrandMaster[]>;
  hasError$: Observable<CustomErrors>;
  brandListLoading$: Observable<boolean>;
  destroy$ = new Subject<null>();
  searchErrorCode: string;
  pageSize: number[];
  totalElements$: Observable<number>;
  brandDetails: BrandMaster;
  parentBrands: BrandMaster[];
  isLoading$: Observable<boolean>;
  dialogRef: any;
  subbrandMasterListingPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };
  hasUpdated$: Observable<boolean>;
  hasSaved$: Observable<boolean>;
  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;
  searchForm = new FormGroup({
    searchValue: new FormControl()
  });

  constructor(
    private translate: TranslateService,
    private dialog: MatDialog,
    private subBrandMasterFacade: BrandMasterFacade,
    private router: Router,
    private appsettingFacade: AppsettingFacade,
    private overlayNotification: OverlayNotificationService
  ) { }

  ngAfterViewInit(): void {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe(event => {
        const searchValue = this.searchForm.value.searchValue;
        if (searchValue) {
          this.search(searchValue);
        } else {
          this.clearSearch();
        }
      });
  }

  search(searchValue: string) {
    if (!searchValue.search('^[_A-z0-9]*((-|s)*[_A-z0-9])*$')) {
      this.subBrandMasterFacade.searchBrand(searchValue);
    }
  }
  clearSearch() {
    this.searchForm.reset();
    this.loadSubBrandMasterList();
  }
  ngOnInit() {
    this.loadSubBrandMasterList();
    this.subBrandMasterFacade.loadParentMasterList();
    this.searchErrorCode = ErrorEnums.ERR_PRO_001;
    this.hasError$ = this.subBrandMasterFacade.getError();
    this.isLoading$ = this.subBrandMasterFacade.getIsloading();
    this.subBrandMasterFacade
      .getParentBrandList()
      .pipe(takeUntil(this.destroy$))
      .subscribe(parentBrands => {
        this.parentBrands = parentBrands;
      });
    this.subBrandMasterFacade
      .getHasSaved()
      .pipe(takeUntil(this.destroy$))
      .subscribe(hasSaved => {
        if (hasSaved) {
          this.showSuccessMessageNotification('pw.subBrand.successMsg');
        } else this.overlayNotification.close();
      });
    this.subBrandMasterFacade
      .getHasUpdated()
      .pipe(takeUntil(this.destroy$))
      .subscribe(hasUpdated => {
        if (hasUpdated) {
          this.loadSubBrandMasterList();
          this.showSuccessMessageNotification('pw.subBrand.updatedMsg');
        } else this.overlayNotification.close();
      });
    this.subBrandMasterFacade
      .getSubBrandDetails()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          this.dialogRef = this.dialog.open(SubbrandDetailsComponent, {
            width: '500px',
            height: 'auto',
            data: {
              subBrandDetails: data,
              parentBrandDetails: this.parentBrands
            }
          });
          this.dialogRef
            .afterClosed()
            .pipe(takeUntil(this.destroy$))
            .subscribe(formData => {
              if (formData) {
                // this.subBrandMasterFacade.loadReset()
                this.subBrandFormDetails(formData);
              }
            });
        }
        // else if(this.dialogRef){
        //     this.dialogRef.close();
        //     console.log("rama");
        //   }
      });

    this.appsettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.pageSize = data;
      });

    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.subbrandMasterListingPageEvent = {
          ...this.subbrandMasterListingPageEvent,
          pageSize: data
        };
        this.loadSubBrandMasterList();
        this.SubbrandList$ = this.subBrandMasterFacade.getBrandMasterList();
        this.totalElements$ = this.subBrandMasterFacade.getTotalElements();
      });

    this.subBrandMasterFacade
      .getIsActiveToggle()
      .pipe(takeUntil(this.destroy$))
      .subscribe(d => {
        if (d) {
          this.showNotification('pw.subBrand.updatedMsg');
          this.subBrandMasterFacade.resetIsActiveToggle();
        }
      });

    this.subBrandMasterFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });
  }

  emitToggleValue(obj) {
    this.subBrandMasterFacade.updateBrandMasterDetails({
      brandCode: obj.brandCode,
      data: {
        isActive: obj.isActive
      }
    });
  }

  showSuccessMessageNotification(key: any) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMsg,
            time: 2000,
            hasBackdrop: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => { });
      });
  }
  loadSubBrandMasterList() {
    this.subBrandMasterFacade.loadBrandMasterList(
      this.subbrandMasterListingPageEvent
    );
  }
  paginate(pageEvent: PageEvent) {
    this.subbrandMasterListingPageEvent = pageEvent;
    this.loadSubBrandMasterList();
  }

  addnewSubBrand(subBrandCode: string) {
    if (subBrandCode !== subBrandEnum.NEW) {
      this.subBrandMasterFacade.loadSubBrandDetailsByBrandCode(subBrandCode);
    } else {
      const newFormData: SubBrandMaster = {
        brandCode: subBrandEnum.NEW,
        description: '',
        configDetails: {
          isActive: false
        }
      };
      const dialogRef = this.dialog.open(SubbrandDetailsComponent, {
        width: '500px',
        height: 'auto',
        data: {
          subBrandDetails: newFormData,
          parentBrandDetails: this.parentBrands
        }
      });
      dialogRef
        .afterClosed()
        .pipe(takeUntil(this.destroy$))
        .subscribe(data => {
          if (data) {
            this.subBrandFormDetails(data);
          }
        });
    }
  }

  subBrandFormDetails(data: any) {
    if (data.mode === subBrandEnum.new) {
      this.subBrandMasterFacade.saveSubBrandMasterDetails({
        brandCode: data.brandCode,
        description: data.description,
        parentBrandCode: data.parentBrandCode,
        orgCode: '',
        isActive: true,
        configDetails: {
          subBrandDetails: {
            isActive: data.configDetails.isActive
          }
        }
      });
    } else if (data.mode === subBrandEnum.edit) {
      this.subBrandMasterFacade.updateSubBrandMasterDetails({
        brandCode: data.brandCode,
        data: {
          description: data.description,
          parentBrandCode: data.parentBrandCode,
          orgCode: '',
          isActive: true,
          configDetails: {
            subBrandDetails: {
              isActive: data.configDetails.isActive
            }
          }
        }
      });
    }
  }

  updateToggleValue(toggleValue) {
    this.subBrandMasterFacade.upDateIsactive({
      brandCode: toggleValue.brandCode,

      isActive: toggleValue.isActive
    });
  }

  errorHandler(error: CustomErrors) {
    if (error.code === this.searchErrorCode) {
      // We are not showing error for location not found from search.
      return;
    }

    this.overlayNotification.show({
      type: OverlayNotificationType.ERROR,
      hasClose: true,
      error: error
    });
  }

  back() {
    // if(this.dialogRef){
    //   this.dialogRef.close();
    //   console.log("rama");
    // }
    this.subBrandMasterFacade.loadReset();
    this.router.navigate([getProductMasterDashboardRouteUrl()]);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
  omit_special_char($event: KeyboardEvent) {
    const pattern = /^[-_A-Za-z0-9]$/;
    return pattern.test($event.key);
  }

  showNotification(key: string) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMessage,
            time: 2000,
            hasBackdrop: true
          })
          .events.subscribe((eventType: OverlayNotificationEventType) => {
            this.overlayNotification.close();
          });
      });
  }
}
