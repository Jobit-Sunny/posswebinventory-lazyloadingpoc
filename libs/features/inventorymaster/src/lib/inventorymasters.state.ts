import { AppState } from '@poss-web/core';
import { createFeatureSelector, ActionReducerMap } from '@ngrx/store';
import { LocationMasterState } from './locationSetup/+state/location-master.state';
import { LocationMasterReducer } from './locationSetup/+state/location-master.reducer';
import { CorporateTownState } from './corporate-townSetup/+state/corporate-town.state';
import { CorporateTownReducer } from './corporate-townSetup/+state/corporate-town.reducer';
import { BinGroupState } from './inventoryConfiguration/binGroup/+state/bin-group.state';
import { BinGroupReducer } from './inventoryConfiguration/binGroup/+state/bin-group.reducer';
import { BinState } from './inventoryConfiguration/bin/+state/bin.state';
import { BinReducer } from './inventoryConfiguration/bin/+state/bin.reducer';

import { CourierDetailsState } from './inventoryConfiguration/courier/+state/courier-details.state';
import { CourierDetailsReducer } from './inventoryConfiguration/courier/+state/courier-details.reducer';
import { WeightToleranceState } from './inventoryConfiguration/weight-tolerance/+state/weight-tolerances-state';
import { WeightToleranceReducer } from './inventoryConfiguration/weight-tolerance/+state/weight-tolerance-reducer';

import { BrandMasterState } from './brand-master/+state/brand-master.state';
import { BrandMasterReducer } from './brand-master/+state/brand-master.reducer';
import { ProductCategoryReducer } from './productmaster/productCategory/+state/product-category.reducer';
import { ProductCategoryState } from './productmaster/productCategory/+state/product-category.state';
import { LovMasterState } from './lovmaster/+state/lovmaster.state';
import { LovMasterReducer } from './lovmaster/+state/lovmaster.reducer';
import { CFAProductCodeReducer } from './productMaster/CFAProductCode/+state/cfa-product-code.reducer';
import { CFAProductCodeState } from './productmaster/CFAProductCode/+state/cfa-product-code.state';

import { ItemListingState } from './productmaster/item/+state/item.state';
import { ItemReducer } from './productmaster/item/+state/item.reducer';

export const FEATURE_NAME = 'inventorymasters';

export interface InventoryMastersState {
  locationListingState: LocationMasterState;
  courierDetailsState: CourierDetailsState;
  corporateTownState: CorporateTownState;
  binGroupState: BinGroupState;
  binState: BinState;
  productCategoryState: ProductCategoryState;
  weightToleranceState: WeightToleranceState;
  inventoryConfigurationState: CourierDetailsState;

  brandMasterState: BrandMasterState;
  lovMasterState: LovMasterState;

  CFAProductCodeState: CFAProductCodeState;

  itemListingState: ItemListingState;
}

export interface State extends AppState {
  inventorymasters: InventoryMastersState;
}

export const selectInventoryMasters = createFeatureSelector<
  State,
  InventoryMastersState
>(FEATURE_NAME);

export const reducers: ActionReducerMap<InventoryMastersState> = {
  locationListingState: LocationMasterReducer,
  corporateTownState: CorporateTownReducer,
  binGroupState: BinGroupReducer,
  binState: BinReducer,
  productCategoryState: ProductCategoryReducer,
  courierDetailsState: CourierDetailsReducer,

  weightToleranceState: WeightToleranceReducer,
  inventoryConfigurationState: CourierDetailsReducer,

  brandMasterState: BrandMasterReducer,
  lovMasterState: LovMasterReducer,
  CFAProductCodeState: CFAProductCodeReducer,
  itemListingState: ItemReducer
};
