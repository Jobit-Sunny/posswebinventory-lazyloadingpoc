/**
 * Base Url for User Role Management API
 */

const getUserRoleMgmntBaseUrl = (): string => '/user/v1';

const getLocationBaseUrl = (): string => '/location/v1';

const getUserUrlbyRoleType = (isBTQUser: boolean): string =>
  isBTQUser ? '/store/user' : '/corp/user';

export const getUsersListUrl = (
  isBTQUser: boolean,
  page: number,
  pageSize: number,
  employeeCode: string,
  roleCodes?: string[],
  locationCodes?: string[]
): string => {
  let url =
    getUserRoleMgmntBaseUrl() +
    getUserUrlbyRoleType(isBTQUser) +
    `?page=${page}&size=${pageSize}&sort=isActive,desc`;
  roleCodes.forEach(val => (url = url + `&roleCodes=${val}`));
  locationCodes.forEach(val => (url = url + `&locationCodes=${val}`));
  return employeeCode.length > 0 ? url + `&searchField=${employeeCode}` : url;
};

export const getUserDataUrl = (
  isBTQUser: boolean,
  employeeCode: string
): string => {
  return (
    getUserRoleMgmntBaseUrl() +
    getUserUrlbyRoleType(isBTQUser) +
    `/${employeeCode}`
  );
};

export const addUserDataUrl = (isBTQUser: boolean): string => {
  return getUserRoleMgmntBaseUrl() + getUserUrlbyRoleType(isBTQUser);
};

export const getStateUrl = (countryCode: string): string => {
  return (
    getLocationBaseUrl() + `/states?isActive=true&countryCode=${countryCode}`
  );
};

export const getCountryUrl = (): string => {
  return getLocationBaseUrl() + `/countries?isActive=true`;
};

export const getlLocationCodeUrl = (locationCode: string): string => {
  return getLocationBaseUrl() + `/locations/${locationCode}`;
};

export const getEmailMobileCheckUrl = (type: string, value: string): string => {
  return (
    getUserRoleMgmntBaseUrl() +
    `/user/unique-check?uniqueType=${type}&value=${value}`
  );
};

export const getRoleDetailsUrl = (roleCode: string): string => {
  return getUserRoleMgmntBaseUrl() + `/corp/roles/${roleCode}`;
};

export const addRoleUrl = (): string => {
  return getUserRoleMgmntBaseUrl() + `/corp/roles`;
};

export const getUserRoleUrl = (
  isBTQUser?: boolean,
  roleType?: string,
  locationCode?: string
): string => {
  let url =
    getUserRoleMgmntBaseUrl() +
    (isBTQUser
      ? '/store/roles?isActive=true&corpAccess=false&'
      : '/corp/roles?isActive=true&corpAccess=true&');
  if (roleType !== undefined) {
    url = url + (roleType === 'BTQ' ? 'roleType=BTQ&' : 'roleType=CORP&');
  }
  url = locationCode ? url + `locationCode=${locationCode}` : url;
  return url;
};

export const getActiveRolesUrl = (isBTQUser: boolean): string =>
  getUserRoleMgmntBaseUrl() +
  (isBTQUser ? `/store/roles` : `/corp/roles?isActive=true`);

export const getAllRolesUrl = (
  page: number,
  pageSize: number,
  roleCode: string
): string => {
  let url =
    getUserRoleMgmntBaseUrl() +
    `/corp/roles?page=${page}&size=${pageSize}&sort=roleCode,asc`;
  if (roleCode) {
    url = url + `&roleCode=${roleCode}`;
  }
  return url;
};

export const getUpdateRoleUrl = (roleCode: string): string => {
  return getUserRoleMgmntBaseUrl() + `/corp/roles/${roleCode}`;
};

export const getListModulesUrl = (
  module?: string,
  subModule?: string,
  roleCode?: string
): string => {
  let url = getUserRoleMgmntBaseUrl() + `/acl`;
  url = module ? url + `/${module}` : url;
  url = subModule ? url + `/${subModule}` : url;
  url = roleCode ? url + `?roleCode=${roleCode}` : url;
  return url;
};
