export enum RoleTypes {
  Boutique = 'BTQ',
  Corporate = 'CORP',
  Regional = 'REG'
}

export enum StoreTypes {
  LargeFormatStoreType = 'L1',
  MediumFormatStoreType = 'L2',
  SmallFormatStoreType = 'L3',
  RegionalStoreType = 'REG',
  CorporateStoreTypes = 'ORG'
}
