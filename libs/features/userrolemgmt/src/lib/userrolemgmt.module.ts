import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { SharedModule } from '@poss-web/shared';
import { LoginManagementComponent } from './login/container/login-management.component';
import { UserrolemgmtRoutingModule } from './userrolemgmt.routing';
import {
  DashboardComponent,
  CardComponent
} from './dashboard/dashboard.component';
import { FEATURE_NAME, reducers } from './userrolemgmt.state';
import { UserManagmentEffect } from './user/+state/user-management.effect';
import { UserManagementFacade } from './user/+state/user-management.facade';
import { UserListingComponent } from './user/containers/user-listing/user-listing.component';
import { UserDetailComponent } from './user/containers/user-detail/user-detail.component';
import { UserListComponent } from './user/components/user-list/user-list.component';
import { UserFormComponent } from './user/components/user-form/user-form.component';
import { UserListItemComponent } from './user/components/user-list-item/user-list-item.component';
import { RoleListComponent } from './role/components/role-list/role-list.component';
import { RoleListItemComponent } from './role/components/role-list-item/role-list-item.component';
import { RoleFormComponent } from './role/components/role-form/role-form.component';
import { RoleManagementComponent } from './role/containers/role-management.component';
import { RoleManagementFacade } from './role/+state/role-management.facade';
import { RoleManagementEffect } from './role/+state/role-management.effect';
import { AccessControlManagementComponent } from './access-control/containers/access-control-management.component';
import { AccessControlListComponent } from './access-control/components/access-control-list.component';
import { AccessControlManagementEffect } from './access-control/+state/access-control-management.effect';
import { AccessControlManagementFacade } from './access-control/+state/access-control-management.facade';
import { LoginFormComponent } from './login/components/login-form/login-form.component';
import { OtpGenerationFormComponent } from './login/components/otp-generation-form/otp-generation-form.component';
import { OtpVerificationFormComponent } from './login/components/otp-verification-form/otp-verification-form.component';
import { LoginManagementEffect } from './login/+state/login-management.effect';
import { LoginManagementFacade } from './login/+state/login-management.facade';
import { NewUserActivationFormComponent } from './login/components/new-user-activation-form/new-user-activation-form.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    UserrolemgmtRoutingModule,
    StoreModule.forFeature(FEATURE_NAME, reducers),
    EffectsModule.forFeature([
      UserManagmentEffect,
      RoleManagementEffect,
      AccessControlManagementEffect,
      LoginManagementEffect
    ])
  ],
  declarations: [
    LoginManagementComponent,
    DashboardComponent,
    CardComponent,
    UserListingComponent,
    UserDetailComponent,
    RoleManagementComponent,
    UserListComponent,
    UserFormComponent,
    UserListItemComponent,
    RoleListComponent,
    RoleListItemComponent,
    RoleFormComponent,
    AccessControlManagementComponent,
    AccessControlListComponent,
    LoginFormComponent,
    OtpGenerationFormComponent,
    OtpVerificationFormComponent,
    NewUserActivationFormComponent
  ],
  providers: [
    UserManagementFacade,
    RoleManagementFacade,
    AccessControlManagementFacade,
    LoginManagementFacade
  ],
  entryComponents: [RoleFormComponent]
})
export class UserrolemgmtModule {}
