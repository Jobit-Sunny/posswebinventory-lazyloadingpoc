import { selectUserRoleMgmnt } from '../../userrolemgmt.state';
import { countrySelector } from './user-management.entity';

import { createSelector } from '@ngrx/store';

const UsersList = createSelector(
  selectUserRoleMgmnt,
  state => state.userManagement.users
);

const RolesListSelector = createSelector(
  selectUserRoleMgmnt,
  state => state.userManagement.roles
);

const fetchStates = createSelector(
  selectUserRoleMgmnt,
  state => state.userManagement.states
);

const CountrySelector = createSelector(
  selectUserRoleMgmnt,
  state => state.userManagement.countries
);

const fetchCountries = createSelector(
  CountrySelector,
  countrySelector.selectAll
);

const CountryEntities = createSelector(
  CountrySelector,
  countrySelector.selectEntities
);

const fetchCountry = id => createSelector(
  CountryEntities,
  (entities) => entities[id]
);

const fetchLocation = createSelector(
  selectUserRoleMgmnt,
  state => state.userManagement.location
);

const fetchEmailMobileValidation = createSelector(
  selectUserRoleMgmnt,
  state => state.userManagement.checkMobileEmail
);

const selectError = createSelector(
  selectUserRoleMgmnt,
  state => state.userManagement.error
);

const totalUsers = createSelector(
  selectUserRoleMgmnt,
  state => state.userManagement.totalUsers
);

const fetchSelectedUser = createSelector(
  selectUserRoleMgmnt,
  state => state.userManagement.selectedUser
);

const UserUpdated = createSelector(
  selectUserRoleMgmnt,
  state => state.userManagement.updateUser
);

const isLoading = createSelector(
  selectUserRoleMgmnt,
  state => state.userManagement.isLoading
);

export const UserManagementSelectors = {
  UsersList,
  totalUsers,
  selectError,
  fetchSelectedUser,
  fetchCountries,
  fetchStates,
  fetchLocation,
  RolesListSelector,
  UserUpdated,
  fetchEmailMobileValidation,
  isLoading,
  fetchCountry
};
