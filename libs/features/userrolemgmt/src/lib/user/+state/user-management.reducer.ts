import {
  UserManagementActions,
  UserManagementActionTypes
} from './user-management.actions';
import { UserManagementState } from './user-management.state';
import { countryAdapter } from './user-management.entity';

const initialState: UserManagementState = {
  users: [],
  totalUsers: 0,
  selectedUser: null,
  states: [],
  countries: countryAdapter.getInitialState(),
  location: null,
  roles: [],
  updateUser: false,
  checkMobileEmail: true,
  error: null,
  isLoading: false
};

export function UserManagementReducer(
  state: UserManagementState = initialState,
  action: UserManagementActions
): UserManagementState {
  switch (action.type) {
    case UserManagementActionTypes.LOAD_USERS:
    case UserManagementActionTypes.UPDATE_USER:
    case UserManagementActionTypes.ADD_USER:
      return {
        ...state,
        isLoading: true,
        updateUser: false,
        error: null
      };

    case UserManagementActionTypes.LOAD_USERS_SUCCESS:
      return {
        ...state,
        users: action.payload.users,
        totalUsers: action.payload.totalUsers,
        isLoading: false
      };

    case UserManagementActionTypes.LOAD_USERS_FAILURE:
      return {
        ...state,
        users: [],
        isLoading: false,
        error: action.payload
      };

    case UserManagementActionTypes.FETCH_USER:
      return {
        ...state,
        isLoading: true,
        error: null
      };

    case UserManagementActionTypes.FETCH_USER_SUCCESS:
      return {
        ...state,
        selectedUser: action.payload,
        isLoading: false
      };

    case UserManagementActionTypes.FETCH_USER_FAILURE:
      return {
        ...state,
        isLoading: false,
        selectedUser: null,
        error: action.payload
      };

    case UserManagementActionTypes.LOAD_COUNTRY_SUCCESS:
      return {
        ...state,
        countries: countryAdapter.addAll(action.payload, state.countries),
        error: null
      };

    case UserManagementActionTypes.LOAD_COUNTRY_FAILURE:
      return {
        ...state,
        countries: countryAdapter.addAll([], state.countries)
      };

    case UserManagementActionTypes.LOAD_STATE_SUCCESS:
      return {
        ...state,
        states: action.payload,
        error: null
      };

    case UserManagementActionTypes.LOAD_STATE_FAILURE:
      return {
        ...state,
        states: []
      };

    case UserManagementActionTypes.FETCH_LOCATION_SUCCESS:
      return {
        ...state,
        location: action.payload,
        error: null
      };

    case UserManagementActionTypes.FETCH_LOCATION_FAILURE:
      return {
        ...state,
        location: { countryCode: '', locationCode: '' }
      };

    case UserManagementActionTypes.VALIDATE_MOBILE_EMAIL_SUCCESS:
      return {
        ...state,
        checkMobileEmail: action.payload,
        error: null
      };

    case UserManagementActionTypes.VALIDATE_MOBILE_EMAIL_FAILURE:
      return {
        ...state,
        checkMobileEmail: false
      };

    case UserManagementActionTypes.LOAD_ROLES_SUCCESS:
    case UserManagementActionTypes.LOAD_FILTER_ROLES_SUCCESS:
      return {
        ...state,
        roles: action.payload,
        error: null
      };

    case UserManagementActionTypes.LOAD_ROLES_FAILURE:
    case UserManagementActionTypes.LOAD_FILTER_ROLES_FAILURE:
      return {
        ...state,
        roles: []
      };

    case UserManagementActionTypes.UPDATE_USER_SUCCESS:
    case UserManagementActionTypes.ADD_USER_SUCCESS:
      return {
        ...state,
        updateUser: true,
        isLoading: false
      };

    case UserManagementActionTypes.UPDATE_USER_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };

    case UserManagementActionTypes.ADD_USER_FAILURE:
      return {
        ...state,
        updateUser: false,
        isLoading: false,
        error: action.payload
      };

    default:
      return state;
  }
}
