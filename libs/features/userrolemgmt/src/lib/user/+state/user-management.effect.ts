import { map } from 'rxjs/operators';
import { Effect } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { DataPersistence } from '@nrwl/angular';
import { HttpErrorResponse } from '@angular/common/http';

import {
  NotificationService,
  CustomErrors,
  CustomErrorAdaptor
} from '@poss-web/core';
import * as UserManagementActions from './user-management.actions';
import { UserManagementActionTypes } from './user-management.actions';
import { UserManagementService } from '../services/userManagement.service';
import { UserManagementState } from './user-management.state';
import { User, UserDetail } from '../models/user.model';
import { CountryData, LocationData } from '../models/data.model';
import { TranslateService } from '@ngx-translate/core';
import { Role } from '../../common/model/userroleMgmnt.model';

@Injectable()
export class UserManagmentEffect {
  constructor(
    private service: UserManagementService,
    private dataPersistence: DataPersistence<UserManagementState>,
    private notificationService: NotificationService,
    private translate: TranslateService
  ) {}

  @Effect() loadUsers = this.dataPersistence.fetch(
    UserManagementActionTypes.LOAD_USERS,
    {
      run: (action: UserManagementActions.LoadUsers) => {
        return this.service
          .getUsersList(
            action.payload.isBTQUser,
            action.payload.pageNumber,
            action.payload.pageSize,
            action.payload.employeeCode,
            action.payload.roleCodes,
            action.payload.locationCodes
          )
          .pipe(
            map(
              (Users: UserDetail) =>
                new UserManagementActions.LoadUsersSuccess(Users)
            )
          );
      },

      onError: (
        action: UserManagementActions.LoadUsers,
        error: HttpErrorResponse
      ) => {
        return new UserManagementActions.LoadUsersFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() fetchUser = this.dataPersistence.fetch(
    UserManagementActionTypes.FETCH_USER,
    {
      run: (action: UserManagementActions.FetchUser) => {
        return this.service
          .getUser(action.payload.isBTQUser, action.payload.employeeCode)
          .pipe(
            map(
              (user: User) => new UserManagementActions.FetchUserSuccess(user)
            )
          );
      },

      onError: (
        action: UserManagementActions.FetchUser,
        error: HttpErrorResponse
      ) => {
        return new UserManagementActions.FetchUserFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadStates = this.dataPersistence.fetch(
    UserManagementActionTypes.LOAD_STATE,
    {
      run: (action: UserManagementActions.LoadState) => {
        return this.service
          .loadStates(action.payload)
          .pipe(
            map(
              (states: string[]) =>
                new UserManagementActions.LoadStateSuccess(states)
            )
          );
      },

      onError: (
        action: UserManagementActions.LoadState,
        error: HttpErrorResponse
      ) => {
        return new UserManagementActions.LoadStateFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadCountries = this.dataPersistence.fetch(
    UserManagementActionTypes.LOAD_COUNTRY,
    {
      run: (action: UserManagementActions.LoadCountry) => {
        return this.service
          .loadCountries()
          .pipe(
            map(
              (countries: CountryData[]) =>
                new UserManagementActions.LoadCountrySuccess(countries)
            )
          );
      },

      onError: (
        action: UserManagementActions.LoadCountry,
        error: HttpErrorResponse
      ) => {
        return new UserManagementActions.LoadCountryFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() validateLocation = this.dataPersistence.fetch(
    UserManagementActionTypes.FETCH_LOCATION,
    {
      run: (action: UserManagementActions.FetchLocation) => {
        return this.service
          .loadlocations(action.payload)
          .pipe(
            map(
              (location: LocationData) =>
                new UserManagementActions.FetchLocationSuccess(location)
            )
          );
      },

      onError: (
        action: UserManagementActions.FetchLocation,
        error: HttpErrorResponse
      ) => {
        return new UserManagementActions.FetchLocationFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() validateEmailMobile = this.dataPersistence.fetch(
    UserManagementActionTypes.VALIDATE_MOBILE_EMAIL,
    {
      run: (action: UserManagementActions.ValidateMobileEmail) => {
        return this.service
          .checkMobileEmail(action.payload.fieldtype, action.payload.value)
          .pipe(
            map(
              (validemailmobile: boolean) =>
                new UserManagementActions.ValidateMobileEmailSuccess(
                  validemailmobile
                )
            )
          );
      },

      onError: (
        action: UserManagementActions.ValidateMobileEmail,
        error: HttpErrorResponse
      ) => {
        return new UserManagementActions.ValidateMobileEmailFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadRoles = this.dataPersistence.fetch(
    UserManagementActionTypes.LOAD_ROLES,
    {
      run: (action: UserManagementActions.LoadRoles) => {
        return this.service
          .loadUserRoles(
            action.payload.isBTQUser,
            action.payload.roleType,
            action.payload.locationCode
          )
          .pipe(
            map(
              (roles: Role[]) =>
                new UserManagementActions.LoadRolesSuccess(roles)
            )
          );
      },

      onError: (
        action: UserManagementActions.LoadRoles,
        error: HttpErrorResponse
      ) => {
        return new UserManagementActions.LoadRolesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadFilterRoles = this.dataPersistence.fetch(
    UserManagementActionTypes.LOAD_FILTER_ROLES,
    {
      run: (action: UserManagementActions.LoadFilterRoles) => {
        return this.service
          .loadActiveRoles(action.payload)
          .pipe(
            map(
              (roles: Role[]) =>
                new UserManagementActions.LoadFilterRolesSuccess(roles)
            )
          );
      },

      onError: (
        action: UserManagementActions.LoadFilterRoles,
        error: HttpErrorResponse
      ) => {
        return new UserManagementActions.LoadFilterRolesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() UpdateUser = this.dataPersistence.fetch(
    UserManagementActionTypes.UPDATE_USER,
    {
      run: (action: UserManagementActions.UpdateUser) => {
        return this.service
          .updateUser(
            action.payload.isBTQUser,
            action.payload.employeeCode,
            action.payload.data
          )
          .pipe(map(_ => new UserManagementActions.UpdateUserSuccess()));
      },

      onError: (
        action: UserManagementActions.UpdateUser,
        error: HttpErrorResponse
      ) => {
        return new UserManagementActions.UpdateUserFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() AddUser = this.dataPersistence.fetch(
    UserManagementActionTypes.ADD_USER,
    {
      run: (action: UserManagementActions.AddUser) => {
        return this.service
          .addUser(action.payload.isBTQUser, action.payload.data)
          .pipe(map(_ => new UserManagementActions.AddUserSuccess()));
      },

      onError: (
        action: UserManagementActions.AddUser,
        error: HttpErrorResponse
      ) => {
        return new UserManagementActions.AddUserFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
}
