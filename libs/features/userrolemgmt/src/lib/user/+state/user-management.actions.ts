import { Action } from '@ngrx/store';

import { CustomErrors } from '@poss-web/core';
import { User, UsersPage, UserDetail } from '../models/user.model';
import { RolesRequest, UserRequest } from '../models/request.model';
import {
  CountryData,
  LocationData,
  EmailMobileData
} from '../models/data.model';
import { Role } from '../../common/model/userroleMgmnt.model';

export enum UserManagementActionTypes {
  LOAD_USERS = '[ user-management ] Load Users',
  LOAD_USERS_SUCCESS = '[ user-management ] Load Users Success',
  LOAD_USERS_FAILURE = '[ user-management ]  Load Users Failure',

  FETCH_USER = '[ user-management ] Fetch User',
  FETCH_USER_SUCCESS = '[ user-management ] Fetch User Success',
  FETCH_USER_FAILURE = '[ user-management ]  Fetch User Failure',

  UPDATE_USER = '[ user-management ] Update User',
  UPDATE_USER_SUCCESS = '[ user-management ] Update User Success',
  UPDATE_USER_FAILURE = '[ user-management ] Update User Failure',

  ADD_USER = '[ user-management ] Add User',
  ADD_USER_SUCCESS = '[ user-management ] Add User Success',
  ADD_USER_FAILURE = '[ user-management ] Add User Failure',

  LOAD_ROLES = '[ user-management ] Load User Roles',
  LOAD_ROLES_SUCCESS = '[ user-management ] Load User Roles Success',
  LOAD_ROLES_FAILURE = '[ user-management ]  Load User Roles Failure',

  LOAD_FILTER_ROLES = '[ user-management ] Load Filter Roles',
  LOAD_FILTER_ROLES_SUCCESS = '[ user-management ] Load Filter Roles Success',
  LOAD_FILTER_ROLES_FAILURE = '[ user-management ]  Load Filter Roles Failure',

  LOAD_STATE = '[ user-management ] Load State',
  LOAD_STATE_SUCCESS = '[ user-management ] Load State Success',
  LOAD_STATE_FAILURE = '[ user-management ]  Load State Failure',

  LOAD_COUNTRY = '[ user-management ] Load Country',
  LOAD_COUNTRY_SUCCESS = '[ user-management ] Load Country Success',
  LOAD_COUNTRY_FAILURE = '[ user-management ]  Load Country Failure',

  FETCH_LOCATION = '[ user-management ] Validate Location',
  FETCH_LOCATION_SUCCESS = '[ user-management ] Validate Location Success',
  FETCH_LOCATION_FAILURE = '[ user-management ] Validate Location Failure',

  VALIDATE_MOBILE_EMAIL = '[ user-management ] Validate Mobile & Email',
  VALIDATE_MOBILE_EMAIL_SUCCESS = '[ user-management ] Validate Mobile & Email Success',
  VALIDATE_MOBILE_EMAIL_FAILURE = '[ user-management ] Validate Mobile & Email Failure'
}

export class LoadUsers implements Action {
  readonly type = UserManagementActionTypes.LOAD_USERS;
  constructor(public readonly payload: UsersPage) {}
}

export class LoadUsersSuccess implements Action {
  readonly type = UserManagementActionTypes.LOAD_USERS_SUCCESS;
  constructor(public readonly payload: UserDetail) {}
}
export class LoadUsersFailure implements Action {
  readonly type = UserManagementActionTypes.LOAD_USERS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class FetchUser implements Action {
  readonly type = UserManagementActionTypes.FETCH_USER;
  constructor(public readonly payload: UserRequest) {}
}

export class FetchUserSuccess implements Action {
  readonly type = UserManagementActionTypes.FETCH_USER_SUCCESS;
  constructor(public readonly payload: User) {}
}

export class FetchUserFailure implements Action {
  readonly type = UserManagementActionTypes.FETCH_USER_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadState implements Action {
  readonly type = UserManagementActionTypes.LOAD_STATE;
  constructor(public readonly payload: string) {}
}

export class LoadStateSuccess implements Action {
  readonly type = UserManagementActionTypes.LOAD_STATE_SUCCESS;
  constructor(public readonly payload: string[]) {}
}

export class LoadStateFailure implements Action {
  readonly type = UserManagementActionTypes.LOAD_STATE_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadCountry implements Action {
  readonly type = UserManagementActionTypes.LOAD_COUNTRY;
}

export class LoadCountrySuccess implements Action {
  readonly type = UserManagementActionTypes.LOAD_COUNTRY_SUCCESS;
  constructor(public readonly payload: CountryData[]) {}
}

export class LoadCountryFailure implements Action {
  readonly type = UserManagementActionTypes.LOAD_COUNTRY_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class FetchLocation implements Action {
  readonly type = UserManagementActionTypes.FETCH_LOCATION;
  constructor(public readonly payload: string) {}
}

export class FetchLocationSuccess implements Action {
  readonly type = UserManagementActionTypes.FETCH_LOCATION_SUCCESS;
  constructor(public readonly payload: LocationData) {}
}

export class FetchLocationFailure implements Action {
  readonly type = UserManagementActionTypes.FETCH_LOCATION_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class ValidateMobileEmail implements Action {
  readonly type = UserManagementActionTypes.VALIDATE_MOBILE_EMAIL;
  constructor(public readonly payload: EmailMobileData) {}
}

export class ValidateMobileEmailSuccess implements Action {
  readonly type = UserManagementActionTypes.VALIDATE_MOBILE_EMAIL_SUCCESS;
  constructor(public readonly payload: boolean) {}
}

export class ValidateMobileEmailFailure implements Action {
  readonly type = UserManagementActionTypes.VALIDATE_MOBILE_EMAIL_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadRoles implements Action {
  readonly type = UserManagementActionTypes.LOAD_ROLES;
  constructor(public readonly payload: RolesRequest) {}
}

export class LoadRolesSuccess implements Action {
  readonly type = UserManagementActionTypes.LOAD_ROLES_SUCCESS;
  constructor(public readonly payload: Role[]) {}
}

export class LoadRolesFailure implements Action {
  readonly type = UserManagementActionTypes.LOAD_ROLES_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadFilterRoles implements Action {
  readonly type = UserManagementActionTypes.LOAD_FILTER_ROLES;
  constructor(public readonly payload: boolean) {}
}

export class LoadFilterRolesSuccess implements Action {
  readonly type = UserManagementActionTypes.LOAD_FILTER_ROLES_SUCCESS;
  constructor(public readonly payload: Role[]) {}
}

export class LoadFilterRolesFailure implements Action {
  readonly type = UserManagementActionTypes.LOAD_FILTER_ROLES_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class UpdateUser implements Action {
  readonly type = UserManagementActionTypes.UPDATE_USER;
  constructor(public readonly payload: UserRequest) {}
}

export class UpdateUserSuccess implements Action {
  readonly type = UserManagementActionTypes.UPDATE_USER_SUCCESS;
}

export class UpdateUserFailure implements Action {
  readonly type = UserManagementActionTypes.UPDATE_USER_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class AddUser implements Action {
  readonly type = UserManagementActionTypes.ADD_USER;
  constructor(public readonly payload: UserRequest) {}
}

export class AddUserSuccess implements Action {
  readonly type = UserManagementActionTypes.ADD_USER_SUCCESS;
}

export class AddUserFailure implements Action {
  readonly type = UserManagementActionTypes.ADD_USER_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export type UserManagementActions =
  | LoadUsers
  | LoadUsersSuccess
  | LoadUsersFailure
  | FetchUser
  | FetchUserSuccess
  | FetchUserFailure
  | LoadStateSuccess
  | LoadStateFailure
  | LoadCountrySuccess
  | LoadCountryFailure
  | LoadRolesSuccess
  | LoadRolesFailure
  | LoadFilterRolesSuccess
  | LoadFilterRolesFailure
  | UpdateUser
  | UpdateUserSuccess
  | UpdateUserFailure
  | AddUser
  | AddUserSuccess
  | AddUserFailure
  | FetchLocationSuccess
  | FetchLocationFailure
  | ValidateMobileEmailSuccess
  | ValidateMobileEmailFailure;
