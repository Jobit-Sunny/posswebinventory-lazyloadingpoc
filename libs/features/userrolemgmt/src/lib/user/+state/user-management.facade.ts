import { State } from '../../userrolemgmt.state';
import * as UserRoleMgmntActions from './user-management.actions';
import { UserManagementSelectors } from './user-management.selectors';
import { UsersPage } from '../models/user.model';

import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

@Injectable()
export class UserManagementFacade {
  private UsersList$ = this.store.select(UserManagementSelectors.UsersList);

  private StatesList$ = this.store.select(UserManagementSelectors.fetchStates);

  private CountriesList$ = this.store.select(
    UserManagementSelectors.fetchCountries
  );

  private fetchLocation$ = this.store.select(
    UserManagementSelectors.fetchLocation
  );

  private EmailMobileValidation$ = this.store.select(
    UserManagementSelectors.fetchEmailMobileValidation
  );

  private RolesList$ = this.store.select(
    UserManagementSelectors.RolesListSelector
  );

  private UsersListSize$ = this.store.select(
    UserManagementSelectors.totalUsers
  );

  private selectError$ = this.store.select(UserManagementSelectors.selectError);

  private userUpdated$ = this.store.select(UserManagementSelectors.UserUpdated);

  private fetchSelectedUser$ = this.store.select(
    UserManagementSelectors.fetchSelectedUser
  );

  private isLoading$ = this.store.select(UserManagementSelectors.isLoading);

  getCountry$ = id =>
    this.store.select(UserManagementSelectors.fetchCountry(id));

  constructor(private store: Store<State>) {}

  getUsersList() {
    return this.UsersList$;
  }

  getStatesList() {
    return this.StatesList$;
  }

  getCountriesList() {
    return this.CountriesList$;
  }

  getCountry(id: string) {
    return this.getCountry$(id);
  }

  getLocation() {
    return this.fetchLocation$;
  }

  getEmailMobileValidation() {
    return this.EmailMobileValidation$;
  }

  getRolesList() {
    return this.RolesList$;
  }

  getTotalUsers() {
    return this.UsersListSize$;
  }

  getError() {
    return this.selectError$;
  }

  getSelectedUser() {
    return this.fetchSelectedUser$;
  }

  isLoading() {
    return this.isLoading$;
  }

  userUpdated() {
    return this.userUpdated$;
  }

  loadUsers(pageinfo: UsersPage) {
    this.store.dispatch(new UserRoleMgmntActions.LoadUsers(pageinfo));
  }

  loadSelectedUser(isBTQUser: boolean, employeeCode: string) {
    this.store.dispatch(
      new UserRoleMgmntActions.FetchUser({ isBTQUser, employeeCode })
    );
  }

  loadCountrylist() {
    this.store.dispatch(new UserRoleMgmntActions.LoadCountry());
  }

  loadStatelist(countryCode: string) {
    this.store.dispatch(new UserRoleMgmntActions.LoadState(countryCode));
  }

  loadRoles(isBTQUser: boolean, roleType: string, locationCode?: string) {
    this.store.dispatch(
      new UserRoleMgmntActions.LoadRoles({ isBTQUser, roleType, locationCode })
    );
  }

  loadFilterRoles(isBTQUser: boolean) {
    this.store.dispatch(new UserRoleMgmntActions.LoadFilterRoles(isBTQUser));
  }

  validateLocation(locatiocode: string) {
    this.store.dispatch(new UserRoleMgmntActions.FetchLocation(locatiocode));
  }

  validateEmailMobile(fieldtype: string, value: string) {
    this.store.dispatch(
      new UserRoleMgmntActions.ValidateMobileEmail({ fieldtype, value })
    );
  }

  updateUser(isBTQUser: boolean, employeeCode: string, data: any) {
    this.store.dispatch(
      new UserRoleMgmntActions.UpdateUser({ isBTQUser, employeeCode, data })
    );
  }

  addUser(isBTQUser: boolean, data: any) {
    this.store.dispatch(
      new UserRoleMgmntActions.AddUser({
        isBTQUser,
        employeeCode: undefined,
        data
      })
    );
  }
}
