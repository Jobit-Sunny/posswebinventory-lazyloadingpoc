import { CountryData } from '../models/data.model';

import { EntityState, createEntityAdapter } from '@ngrx/entity';

export interface CountryEntity extends EntityState<CountryData> {}
export const countryAdapter = createEntityAdapter<CountryData>({
  selectId: country => country.description
});
export const countrySelector = countryAdapter.getSelectors();
