import { User } from '../models/user.model';
import { LocationData } from '../models/data.model';
import { CountryEntity } from './user-management.entity';
import { Role } from '../../common/model/userroleMgmnt.model';
import { CustomErrors } from '@poss-web/core';

export interface UserManagementState {
  users: User[];
  totalUsers: number;
  selectedUser: User;
  states: string[];
  countries: CountryEntity;
  location: LocationData;
  roles: Role[];
  updateUser: boolean;
  error: CustomErrors;
  checkMobileEmail: boolean;
  isLoading: boolean;
}
