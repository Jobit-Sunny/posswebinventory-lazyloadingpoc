import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  SimpleChanges,
  OnChanges,
  ViewChild
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import {
  DateAdapter,
  MAT_DATE_LOCALE,
  MAT_DATE_FORMATS,
  MatSelectChange
} from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import * as moment from 'moment';

import { User } from '../../models/user.model';
import {
  AddressData,
  CountryData,
  LocationData
} from '../../models/data.model';
import {
  RoleTypes,
  StoreTypes
} from '../../../common/model/userroleMgmnt.enum';
import { Role } from '../../../common/model/userroleMgmnt.model';

const DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY'
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM-YYYY'
  }
};

@Component({
  selector: 'poss-web-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    { provide: MAT_DATE_FORMATS, useValue: DATE_FORMATS }
  ]
})
export class UserFormComponent implements OnInit, OnChanges {
  @Input() statesList: string[] = [];
  @Input() countriesList: CountryData[] = [];
  @Input() rolesList: Role[] = [];
  @Input() user: User;
  @Input() isBTQUser = false;
  @Input() validLocation: LocationData = null;
  @Input() validContact = null;
  @Input() userLocationCode = '';
  @Input() mobilenomaxlength = 0;

  @Output() addNewUser = new EventEmitter<any>();
  @Output() updateUser = new EventEmitter<any>();
  @Output() loadRoles = new EventEmitter<any>();
  @Output() validateLocation = new EventEmitter<string>();
  @Output() validateEmailMobile = new EventEmitter<any>();
  @Output() loadStates = new EventEmitter<string>();

  @ViewChild('country', { static: true }) countryfield;
  @ViewChild('role', { static: true }) rolefield;
  @ViewChild('secondaryrole', { static: true }) secondaryrolefield;

  userForm: FormGroup;
  validEmail = true;
  validMobile = true;
  validLocationCode = true;
  validemailpattern = false;
  currentDate = moment();
  maxPastDate = moment().subtract(100, 'year');
  private storetype = Array(
    StoreTypes.LargeFormatStoreType.toString(),
    StoreTypes.MediumFormatStoreType.toString(),
    StoreTypes.SmallFormatStoreType.toString()
  );
  //locationCodes = [];

  constructor(private formbuilder: FormBuilder) {}

  ngOnInit(): void {
    this.createUserForm(this.user);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['user'] && changes['user'].currentValue) {
      this.createUserForm(this.user);
    }
    if (changes['validLocation'] && changes['validLocation'].currentValue) {
      if (this.user) {
        this.user = {
          ...this.user,
          address: {
            ...this.user.address,
            country: this.validLocation.countryCode
          }
        };
      }
      if (this.validLocation && this.validLocation.countryCode) {
        this.userForm.patchValue({
          country: this.validLocation.countryCode
        });
        this.validLocationCode = true;
      } else {
        this.validLocationCode = false;
      }

      if (this.userForm.controls['locationcode'].value.length > 0) {
        this.userForm.controls['locationcode'].updateValueAndValidity();
      }
    }
    if (changes['validContact'] && changes['validContact'].currentValue) {
      if (this.validContact) {
        this.validContact['email'] !== undefined
          ? (this.validEmail = this.validContact['email'])
          : (this.validMobile = this.validContact['mobile']);
        this.userForm.controls['mobile'].updateValueAndValidity();
        this.userForm.controls['email'].updateValueAndValidity();
      }
    }
  }

  createUserForm(user: User) {
    this.userForm = this.formbuilder.group({
      empid: [
        user ? user.employeeCode : '',
        [
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9]*$'),
          Validators.maxLength(15)
        ]
      ],
      fullname: [
        user ? user.empName.trim() : '',
        [
          Validators.pattern("^[a-zA-Z0-9 ',-]*$"),
          Validators.maxLength(80),
          Validators.required
        ]
      ],
      joiningdate: [
        user && user.joiningDate.isValid() ? user.joiningDate.toDate() : ''
      ],
      dateofbirth: [
        user && user.birthDate.isValid() ? user.birthDate.toDate() : ''
      ],
      fromdate: [
        user && user.secondarystarttime.isValid() && user.secondaryRole
          ? user.secondarystarttime.toDate()
          : ''
      ],
      tilldate: [
        user && user.secondaryendtime.isValid() && user.secondaryRole
          ? user.secondaryendtime.toDate()
          : ''
      ],
      resignationdate: [
        user && user.resignationDate.isValid()
          ? user.resignationDate.toDate()
          : ''
      ],
      mobile: [
        user ? user.mobileNo : '',
        [
          Validators.required,
          Validators.maxLength(this.mobilenomaxlength),
          Validators.minLength(this.mobilenomaxlength),
          Validators.pattern('^[0-9]*$'),
          (mobile: FormControl): { [key: string]: boolean } =>
            this.validMobile ? null : { duplicate: true }
        ]
      ],
      email: [user ? user.emailId : ''],
      address: [
        user && user.address ? user.address.line1 : '',
        [
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9 .#;/(),-]*$'),
          Validators.maxLength(100)
        ]
      ],
      city: [
        user && user.address ? user.address.city : '',
        [
          Validators.pattern('^[a-zA-Z]*$'),
          Validators.maxLength(50),
          Validators.required
        ]
      ],
      pincode: [
        user && user.address ? user.address.pincode : '',
        [
          Validators.pattern('^[a-zA-Z0-9]*$'),
          Validators.maxLength(10),
          Validators.required
        ]
      ],
      state: [
        user && user.address ? user.address.state.toUpperCase() : '',
        [Validators.required]
      ],
      country: [
        user && user.address ? user.address.country : '',
        [Validators.required]
      ],
      roletype: [
        this.isBTQUser || (user && this.storetype.includes(user.userType))
          ? RoleTypes.Boutique
          : RoleTypes.Corporate,
        Validators.required
      ],
      role: [user ? user.primaryRole : ''],
      roleName: [user ? user.roleName : '', Validators.required],
      secondaryrole: [user ? user.secondaryRole : ''],
      secondaryroleName: [user ? user.secondaryRoleName : ''],
      locationcode: [
        this.isBTQUser ? this.userLocationCode : user ? user.locationCode : '',
        [
          Validators.required,
          (locationcode: FormControl): { [key: string]: boolean } =>
            this.validLocationCode ? null : { invalidLocation: true }
        ]
      ],
      isLoginActive: user ? user.isLoginActive.toString() : 'true',
      islocked: user ? user.isLocked : false
    });

    this.userForm.controls['email'].setValidators([
      (email: FormControl) => {
        return this.userForm.controls['roletype'].value ===
          RoleTypes.Boutique &&
          new RegExp('(?:btq|mgr|bos).{3}@titan.(co.in|com)', 'i').test(
            email.value
          )
          ? { pattern: true }
          : null;
      },
      (email: FormControl) => (this.validEmail ? null : { duplicate: true }),
      Validators.email
    ]);

    this.userForm.controls['secondaryroleName'].setValidators(
      (control: FormControl) =>
        this.userForm.controls['secondaryrole'].value &&
        this.userForm.controls['role'].value ===
          this.userForm.controls['secondaryrole'].value
          ? { samerole: true }
          : null
    );

    this.userForm.controls['fromdate'].setValidators((control: FormControl) =>
      this.userForm.controls['secondaryrole'].value &&
      control.value.length === 0
        ? { required: true }
        : null
    );

    this.userForm.controls['tilldate'].setValidators((control: FormControl) =>
      this.userForm.controls['secondaryrole'].value &&
      control.value.length === 0
        ? { required: true }
        : null
    );
  }

  validateForm() {
    if (this.userForm.controls['roletype'].value === RoleTypes.Corporate) {
      this.userForm.controls['locationcode'].clearValidators();
      this.userForm.controls['locationcode'].updateValueAndValidity();
    }

    if (this.userForm.valid) {
      const address: AddressData = {
        line1: this.userForm.controls['address'].value,
        line2: '',
        city: this.userForm.controls['city'].value,
        country: this.userForm.controls['country'].value,
        pincode: this.userForm.controls['pincode'].value,
        state: this.userForm.controls['state'].value
      };

      const userdata = {
        ...((this.user
          ? !this.user.address ||
            JSON.stringify(address) !== JSON.stringify(this.user.address)
          : true) && {
          address: {
            data: { ...address, country: this.countryfield.selected.viewValue },
            type: 'address'
          }
        }),
        ...((this.user
          ? this.userForm.controls['dateofbirth'].value.toString() &&
            this.user.birthDate.toDate().toString() !==
              this.userForm.controls['dateofbirth'].value.toString()
          : this.userForm.controls['dateofbirth'].value.toString()) && {
          birthDate: this.userForm.controls['dateofbirth'].value
        }),
        ...((this.user
          ? this.user.empName !== this.userForm.controls['fullname'].value
          : true) && { empName: this.userForm.controls['fullname'].value }),
        ...((this.user
          ? this.user.isLocked !== this.userForm.controls['islocked'].value
          : true) && {
          isLocked: this.userForm.controls['islocked'].value
        }),
        ...((this.user
          ? this.user.isLoginActive.toString() !==
            this.userForm.controls['isLoginActive'].value
          : true) && {
          isLoginActive:
            this.userForm.controls['isLoginActive'].value === 'true'
        }),
        ...(!this.user && {
          employeeCode: this.userForm.controls['empid'].value
        }),
        ...(this.userForm.controls['roletype'].value === RoleTypes.Boutique &&
          !this.user && {
            locationCode: this.userForm.controls['locationcode'].value
          }),
        ...(this.userForm.controls['roletype'].value === RoleTypes.Corporate &&
          !this.user && {
            organizationCode: this.userLocationCode
          }),
        ...((this.user
          ? this.userForm.controls['joiningdate'].value.toString() &&
            this.user.joiningDate.toDate().toString() !==
              this.userForm.controls['joiningdate'].value.toString()
          : this.userForm.controls['joiningdate'].value.toString()) && {
          joiningDate: this.userForm.controls['joiningdate'].value
        }),
        ...((this.user
          ? this.userForm.controls['resignationdate'].value.toString() &&
            this.user.resignationDate.toDate().toString() !==
              this.userForm.controls['resignationdate'].value.toString()
          : this.userForm.controls['resignationdate'].value.toString()) && {
          resignationDate: this.userForm.controls['resignationdate'].value
        }),
        ...((this.user
          ? this.userForm.controls['email'].value &&
            this.user.emailId !== this.userForm.controls['email'].value
          : this.userForm.controls['email'].value) && {
          emailId: this.userForm.controls['email'].value
        }),
        ...((this.user
          ? this.user.mobileNo !== this.userForm.controls['mobile'].value
          : true) && { mobileNo: this.userForm.controls['mobile'].value }),
        ...((this.user
          ? this.user.primaryRole !== this.userForm.controls['role'].value
          : true) && { primaryRoleCode: this.userForm.controls['role'].value }),
        ...(!this.user &&
          this.userForm.controls['secondaryrole'].value && {
            tempRoleCodes: [this.userForm.controls['secondaryrole'].value],
            expiryTime: this.userForm.controls['tilldate'].value,
            startTime: this.userForm.controls['fromdate'].value
          }),
        ...(this.user &&
          this.userForm.controls['secondaryrole'].value !==
            this.user.secondaryRole && {
            ...(this.userForm.controls['secondaryrole'].value && {
              addTempRoleCodes: [this.userForm.controls['secondaryrole'].value]
            }),
            ...(this.user.secondaryRole && {
              removeTempRoleCodes: [this.user.secondaryRole]
            }),
            ...(this.userForm.controls['secondaryrole'].value && {
              expiryTime: this.userForm.controls['tilldate'].value,
              startTime: this.userForm.controls['fromdate'].value
            })
          }),
        ...(this.user &&
          this.user.secondaryRole &&
          this.userForm.controls['secondaryrole'].value ===
            this.user.secondaryRole &&
          this.user.secondaryendtime.toDate().toString() !==
            this.userForm.controls['tilldate'].value.toString() && {
            updateTempExpiryTime: this.userForm.controls['tilldate'].value,
            updateTempRoleCode: this.user.secondaryRole
          }),
        ...(this.user &&
          this.user.secondaryRole &&
          this.userForm.controls['secondaryrole'].value ===
            this.user.secondaryRole &&
          this.user.secondarystarttime.toDate().toString() !==
            this.userForm.controls['fromdate'].value.toString() && {
            updateTempStartTime: this.userForm.controls['fromdate'].value,
            updateTempRoleCode: this.user.secondaryRole
          })
      };

      this.user
        ? this.updateUser.emit(userdata)
        : this.addNewUser.emit(userdata);
    } else {
      this.userForm.markAllAsTouched();
    }
  }

  locationCodeChange(event: any) {
    this.roleTypeChanged(this.userForm.get('roletype').value);
    this.validateLocation.emit(event.target.value);
    this.rolefield.value = '';
    this.secondaryrolefield.value = '';
  }

  secondaryRoleCodeChange(event: MatSelectChange) {
    this.userForm.patchValue({
      secondaryrole: event.value,
      secondaryroleName: event.source.triggerValue,
      ...(!event.value
        ? {
            fromdate: '',
            tilldate: ''
          }
        : {
            fromdate:
              this.user &&
              this.user.secondarystarttime.isValid() &&
              this.user.secondaryRole
                ? this.user.secondarystarttime.toDate()
                : '',
            tilldate:
              this.user &&
              this.user.secondaryendtime.isValid() &&
              this.user.secondaryRole
                ? this.user.secondaryendtime.toDate()
                : ''
          })
    });
  }

  roleTypeChanged(type: string) {
    this.userForm.patchValue({
      role: '',
      roleName: '',
      secondaryrole: '',
      secondaryroleName: ''
    });
    this.userForm.controls['email'].updateValueAndValidity();
    this.userForm.controls['role'].updateValueAndValidity();
    this.loadRoles.emit({
      roleType: type,
      locationCode: this.userForm.controls['locationcode'].value
    });
    if (this.userForm.controls['locationcode'].value === '') {
      this.rolesList = [];
    }
  }
}
