import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material';

import { User } from '../../models/user.model';
import { StoreTypes } from '../../../common/model/userroleMgmnt.enum';

@Component({
  selector: 'poss-web-user-list-item',
  templateUrl: './user-list-item.component.html',
  styleUrls: ['./user-list-item.component.scss']
})
export class UserListItemComponent {
  @Output() toggleUserLock = new EventEmitter<any>();
  @Output() navigate = new EventEmitter<string>();

  @Input() user: User;

  storetype = Array(
    StoreTypes.LargeFormatStoreType.toString(),
    StoreTypes.MediumFormatStoreType.toString(),
    StoreTypes.SmallFormatStoreType.toString()
  );

  constructor() {}

  toggleUserLockevent = (event: any) =>
    this.toggleUserLock.emit({
      id: event.target.id,
      value: !this.user.isLocked
    });
}
