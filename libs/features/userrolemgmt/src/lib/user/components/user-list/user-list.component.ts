import {
  Component,
  Output,
  EventEmitter,
  Input,
  ViewChild,
  ElementRef,
  OnDestroy,
  OnInit
} from '@angular/core';
import { PageEvent } from '@angular/material';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { Subject, fromEvent } from 'rxjs';

import { User } from '../../models/user.model';

@Component({
  selector: 'poss-web-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, OnDestroy {
  @Input() isSearch = false;
  @Input() isBTQUser = true;
  @Input() usersList: User[] = [];
  @Input() userListSize = 0;
  @Input() pageSizeOptions: number[] = [];
  @Input() minPageSize = 0;
  @Input() pageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };

  @Output() searchUser = new EventEmitter<string>();
  @Output() toggleUserLock = new EventEmitter<boolean>();
  @Output() paginateUsersList = new EventEmitter<PageEvent>();
  @Output() navigate = new EventEmitter<string>();
  @Output() filter = new EventEmitter();
  @Output() locationSelection = new EventEmitter();

  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;

  destroy$: Subject<null> = new Subject<null>();

  constructor() {}

  ngOnInit(): void {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe((event: any) => this.searchUser.emit(event.target.value));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
