import { Component, OnInit, OnDestroy } from '@angular/core';
import { take, skip, tap, filter, map, takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of, Subject, forkJoin } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import { AuthFacade, AppsettingFacade, CustomErrors } from '@poss-web/core';
import { UserManagementFacade } from '../../+state/user-management.facade';
import {
  OverlayNotificationService,
  OverlayNotificationType
} from '@poss-web/shared';
import { User } from '../../models/user.model';
import { CountryData, LocationData } from '../../models/data.model';
import {
  RoleTypes,
  StoreTypes
} from '../../../common/model/userroleMgmnt.enum';
import { Role } from '../../../common/model/userroleMgmnt.model';

@Component({
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit, OnDestroy {
  isBTQUser = false;
  private storetype = Array(
    StoreTypes.LargeFormatStoreType.toString(),
    StoreTypes.MediumFormatStoreType.toString(),
    StoreTypes.SmallFormatStoreType.toString()
  );
  userLocationCode = '';
  mobileMaxLength = 0;
  employeeCode = '';
  user: Observable<User>;
  isLoading$: Observable<boolean>;
  rolesList: Observable<Role[]>;
  statesList: Observable<string[]>;
  countriesList$: Observable<CountryData[]>;
  validLocation: Observable<LocationData>;
  hasNotification = false;
  validContact: Observable<{ [key: string]: boolean }>;
  username = '';
  destroy$: Subject<null> = new Subject<null>();

  constructor(
    public router: Router,
    private overlayNotification: OverlayNotificationService,
    private authfacade: AuthFacade,
    private activatedRoute: ActivatedRoute,
    private userManagementFacade: UserManagementFacade,
    private appsettingFacade: AppsettingFacade,
    private translate: TranslateService
  ) {
    forkJoin(
      this.appsettingFacade.getStoreType().pipe(take(1)),
      this.appsettingFacade.getMobileMaxLength().pipe(take(1)),
      this.authfacade.getLocationCode().pipe(take(1))
    ).subscribe(val => {
      this.isBTQUser = this.storetype.includes(val[0]);
      this.mobileMaxLength = val[1];
      this.userLocationCode = val[2];
    });
  }

  ngOnInit() {
    this.isLoading$ = this.userManagementFacade.isLoading();
    this.userManagementFacade.loadCountrylist();
    this.countriesList$ = this.userManagementFacade.getCountriesList();

    this.employeeCode = this.activatedRoute.snapshot.params['id'] || '';
    if (this.employeeCode !== 'create') {
      this.userManagementFacade.loadSelectedUser(
        this.isBTQUser,
        this.employeeCode
      );
      this.user = this.userManagementFacade.getSelectedUser().pipe(
        skip(1),
        tap((user: User) => {
          this.loadRoles(
            this.isBTQUser
              ? undefined
              : user && this.storetype.includes(user.userType)
              ? RoleTypes.Boutique
              : RoleTypes.Corporate,
            !this.isBTQUser && (user && this.storetype.includes(user.userType))
              ? user.locationCode
              : ''
          );
          if (user.address && user.address.country) {
            this.userManagementFacade
              .getCountry(user.address.country)
              .pipe(
                filter(data => !!data),
                take(1)
              )
              .subscribe(data => {
                this.validLocation = of({
                  locationCode: user.locationCode,
                  countryCode: data.countryCode
                });
                this.loadStates(data.countryCode);
              });
          }
        })
      );
    } else {
      this.loadRoles(this.isBTQUser ? undefined : RoleTypes.Corporate);
      this.checkLocation(this.userLocationCode);
    }

    this.userManagementFacade
      .userUpdated()
      .pipe(
        takeUntil(this.destroy$),
        filter(updated => updated)
      )
      .subscribe(_ => {
        this.showNotification(
          undefined,
          this.translate.instant(
            this.employeeCode === 'create'
              ? 'pw.usermanagementformNotification.usercreationsuccessMessage'
              : 'pw.usermanagementformNotification.usereditsuccessMessage',
            {
              username: this.username
            }
          ),
          true
        );
      });

    this.userManagementFacade
      .getError()
      .pipe(
        takeUntil(this.destroy$),
        filter(errorVal => !!errorVal)
      )
      .subscribe(errorVal => this.showNotification(errorVal));
  }

  checkLocation(locationCode: string) {
    this.userManagementFacade.validateLocation(
      locationCode ? locationCode : this.userLocationCode
    );
    this.validLocation = this.userManagementFacade.getLocation().pipe(
      skip(1),
      tap(data => this.loadStates(data.countryCode))
    );
  }

  loadStates(countryCode: string) {
    this.userManagementFacade.loadStatelist(countryCode);
    this.statesList = this.userManagementFacade.getStatesList();
  }

  checkUniqueEmailMobile(type: any) {
    const fieldType = type['EMAIL'] ? 'EMAIL' : 'MOBILE';
    this.userManagementFacade.validateEmailMobile(fieldType, type[fieldType]);
    this.validContact = this.userManagementFacade
      .getEmailMobileValidation()
      .pipe(
        map(validContact =>
          type['EMAIL'] ? { email: validContact } : { mobile: validContact }
        )
      );
  }

  loadRoles(roleType: string, locationCode?: string) {
    this.userManagementFacade.loadRoles(this.isBTQUser, roleType, locationCode);
    this.rolesList = this.userManagementFacade.getRolesList().pipe(skip(1));
  }

  addNewUser(userdata: any) {
    this.userManagementFacade.addUser(this.isBTQUser, userdata);
    this.username = userdata.employeeCode;
  }

  updateUser(userdata: any) {
    this.userManagementFacade.updateUser(
      this.isBTQUser,
      this.employeeCode,
      userdata
    );
    this.username = this.employeeCode;
  }

  showNotification(error?: CustomErrors, message?: string, backDrop?: boolean) {
    this.hasNotification = true;
    this.overlayNotification
      .show({
        type: !!error
          ? OverlayNotificationType.ERROR
          : OverlayNotificationType.TIMER,
        error: error,
        message: message,
        hasBackdrop: backDrop,
        hasClose: !!error
      })
      .events.pipe(take(1))
      .subscribe(() => {
        this.hasNotification = false;
        if (backDrop) {
          this.router.navigate(['user-management/users']);
        }
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
