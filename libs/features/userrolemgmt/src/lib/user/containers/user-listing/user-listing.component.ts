import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { take, filter, takeUntil } from 'rxjs/operators';
import { Observable, Subject, forkJoin } from 'rxjs';
import { PageEvent } from '@angular/material';

import { UserManagementFacade } from '../../+state/user-management.facade';
import {
  AppsettingFacade,
  CustomErrors,
  LocationMappingService,
  LocationMappingServiceResponse
} from '@poss-web/core';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  FilterService,
  Filter,
  FilterActions
} from '@poss-web/shared';
import { User } from '../../models/user.model';
import { StoreTypes } from '../../../common/model/userroleMgmnt.enum';

@Component({
  templateUrl: './user-listing.component.html',
  styleUrls: ['./user-listing.component.scss']
})
export class UserListingComponent implements OnInit, OnDestroy {
  isBTQUser = false;
  employeeCode = '';
  error = '';
  private storetype = Array(
    StoreTypes.LargeFormatStoreType,
    StoreTypes.MediumFormatStoreType,
    StoreTypes.SmallFormatStoreType
  );
  usersList$: Observable<User[]>;
  isLoading$: Observable<boolean>;
  userListSize$: Observable<number>;
  hasNotification = false;
  pageSizeOptions: number[] = [];
  minPageSize = 0;
  destroy$: Subject<null> = new Subject<null>();
  filterRolelist: { [key: string]: Filter[] };
  maxFilterLimit = 0;

  selectedLocations: { id: string; description: string }[] = [];

  usersPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };

  constructor(
    private userManagementFacade: UserManagementFacade,
    private appsettingFacade: AppsettingFacade,
    public router: Router,
    private overlayNotification: OverlayNotificationService,
    private filterService: FilterService,
    private locationMappingService: LocationMappingService
  ) {
    forkJoin(
      appsettingFacade.getPageSize().pipe(take(1)),
      appsettingFacade.getStoreType().pipe(take(1)),
      appsettingFacade.getPageSizeOptions().pipe(take(1)),
      appsettingFacade.getMaxFilterLimit().pipe(take(1))
    ).subscribe(val => {
      this.usersPageEvent.pageSize = val[0];
      this.minPageSize = +val[0];
      this.isBTQUser = this.storetype.includes(val[1]);
      userManagementFacade.loadFilterRoles(this.isBTQUser);
      this.pageSizeOptions = val[2];
      this.maxFilterLimit = val[3];
    });
    this.filterRolelist = { Roles: [] };
    this.loadUsers();
  }

  ngOnInit() {
    this.usersList$ = this.userManagementFacade.getUsersList();
    this.userListSize$ = this.userManagementFacade.getTotalUsers();
    this.isLoading$ = this.userManagementFacade.isLoading();

    this.userManagementFacade
      .userUpdated()
      .pipe(
        takeUntil(this.destroy$),
        filter(updated => updated)
      )
      .subscribe(_ => this.loadUsers());

    this.userManagementFacade
      .getError()
      .pipe(
        takeUntil(this.destroy$),
        filter(errorVal => !!errorVal)
      )
      .subscribe(errorVal => {
        this.showNotification(errorVal);
        this.error = errorVal.message;
      });

    this.userManagementFacade
      .getRolesList()
      .pipe(
        filter(list => list.length > 0),
        take(1)
      )
      .subscribe(
        list =>
          (this.filterService.DataSource = {
            Roles: list.map(item => ({
              id: item.roleCode,
              description: item.roleName,
              selected: false
            }))
          })
      );
  }

  searchUser(event: string) {
    this.employeeCode = event;
    this.usersPageEvent.pageIndex = 0;
    this.loadUsers();
  }

  openFilterDialog() {
    this.filterService
      .openDialog(this.maxFilterLimit, this.filterRolelist, undefined, true)
      .pipe(filter(response => response.actionfrom === FilterActions.APPLY))
      .subscribe(responseData => {
        this.filterRolelist['Roles'] = responseData.data['Roles'];
        this.usersPageEvent.pageIndex = 0;
        this.loadUsers();
      });
  }

  openLocationSelection() {
    this.locationMappingService
      .open({
        selectedLocations: this.selectedLocations
      })
      .pipe(take(1))
      .subscribe((res: LocationMappingServiceResponse) => {
        if (res.type === 'apply') {
          this.selectedLocations = res.data.selectedLocations;
          this.usersPageEvent.pageIndex = 0;
          this.loadUsers();
        }
      });
  }

  loadUsers = () =>
    this.userManagementFacade.loadUsers({
      isBTQUser: this.isBTQUser,
      employeeCode: this.employeeCode,
      pageNumber: this.usersPageEvent.pageIndex,
      pageSize: this.usersPageEvent.pageSize,
      roleCodes: this.filterRolelist['Roles'].map(item => item.id as string),
      locationCodes: this.selectedLocations.map(location => location.id)
    });

  paginateUsersList(event: PageEvent) {
    this.usersPageEvent = event;
    this.loadUsers();
  }

  toggleUserLock(event: any) {
    this.userManagementFacade.updateUser(this.isBTQUser, event.id, {
      isLocked: event.value
    });
  }

  navigate = (event?: string) =>
    this.router.navigate([
      'user-management/users/' + (event === 'create' ? 'create' : event)
    ]);

  showNotification(error: CustomErrors) {
    this.hasNotification = true;
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        error: error,
        hasClose: true
      })
      .events.pipe(take(1))
      .subscribe(() => (this.hasNotification = false));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
