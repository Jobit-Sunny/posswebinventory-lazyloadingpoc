import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ApiService } from '@poss-web/core';
import {
  getUsersListUrl,
  getStateUrl,
  getCountryUrl,
  getlLocationCodeUrl,
  getUserRoleUrl,
  addUserDataUrl,
  getEmailMobileCheckUrl,
  getActiveRolesUrl,
  getUserDataUrl
} from '../../endpoints.constants';
import { UserManagementHelper } from '../helpers/userManagement.helper';
import { UserManagementAdaptor } from '../adaptors/userManagement.adaptor';
import { User, UserDetail } from '../models/user.model';
import { CountryData, LocationData } from '../models/data.model';
import { Role } from '../../common/model/userroleMgmnt.model';

@Injectable({
  providedIn: 'root'
})
export class UserManagementService {
  constructor(private apiService: ApiService) {}

  getUsersList = (
    isBTQUser: boolean,
    page: number,
    pageSize: number,
    employeeCode: string,
    roleCodes: string[],
    locationCodes: string[]
  ): Observable<UserDetail> => {
    return this.apiService
      .get(
        getUsersListUrl(
          isBTQUser,
          page,
          pageSize,
          employeeCode,
          roleCodes,
          locationCodes
        )
      )
      .pipe(map((data: any) => UserManagementHelper.getUserData(data)));
  };

  getUser = (isBTQUser: boolean, employeeCode: string): Observable<User> => {
    return this.apiService
      .get(getUserDataUrl(isBTQUser, employeeCode))
      .pipe(map((data: any) => UserManagementAdaptor.usersfromJson(data)));
  };

  loadStates = (countryCode: string): Observable<string[]> => {
    return this.apiService
      .get(getStateUrl(countryCode))
      .pipe(map((data: any) => UserManagementHelper.getStateData(data)));
  };

  loadCountries = (): Observable<CountryData[]> => {
    return this.apiService
      .get(getCountryUrl())
      .pipe(map((data: any) => UserManagementHelper.getCountryData(data)));
  };

  loadlocations = (locationCode: string): Observable<LocationData> => {
    return this.apiService
      .get(getlLocationCodeUrl(locationCode))
      .pipe(map((data: any) => UserManagementHelper.getLocationCodeData(data)));
  };

  loadUserRoles = (
    isBTQUser: boolean,
    roleType: string,
    locationCode?: string
  ): Observable<Role[]> => {
    return this.apiService
      .get(getUserRoleUrl(isBTQUser, roleType, locationCode))
      .pipe(map((data: any) => UserManagementHelper.getRolesData(data)));
  };

  loadActiveRoles = (isBTQUser: boolean): Observable<Role[]> => {
    return this.apiService
      .get(getActiveRolesUrl(isBTQUser))
      .pipe(map((data: any) => UserManagementHelper.getRolesData(data)));
  };

  updateUser = (
    isBTQUser: boolean,
    employeeCode: string,
    data: any
  ): Observable<string[]> => {
    return this.apiService.patch(
      getUserDataUrl(isBTQUser, employeeCode),
      data
    );
  };

  addUser = (isBTQUser: boolean, data: any): Observable<string[]> => {
    return this.apiService.post(addUserDataUrl(isBTQUser), data);
  };

  checkMobileEmail = (type: string, value: string): Observable<boolean> => {
    return this.apiService.get(getEmailMobileCheckUrl(type, value));
  };
}
