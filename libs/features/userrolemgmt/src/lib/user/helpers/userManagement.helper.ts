import { UserManagementAdaptor } from '../adaptors/userManagement.adaptor';
import { User, UserDetail } from '../models/user.model';
import { CountryData, LocationData } from '../models/data.model';
import { Role } from '../../common/model/userroleMgmnt.model';

export class UserManagementHelper {
  static getUserData(data: any): UserDetail {
    const users: User[] = [];
    for (const user of data.results) {
      users.push(UserManagementAdaptor.usersfromJson(user));
    }
    return {
      users,
      totalUsers: data.totalElements
    };
  }

  static getStateData(data: any): string[] {
    const dropdowndata: string[] = [];
    for (const item of data.results) {
      dropdowndata.push(item.description);
    }
    return dropdowndata;
  }

  static getCountryData(data: any): CountryData[] {
    const dropdowndata: CountryData[] = [];
    for (const item of data.results) {
      dropdowndata.push({
        description: item.description,
        countryCode: item.countryCode
      });
    }
    return dropdowndata;
  }

  static getLocationCodeData = (data: any): LocationData => {
    return {
      countryCode: data.countryCode,
      locationCode: data.locationCode
    };
  };

  static getRolesData(data: any): Role[] {
    const dropdowndata: Role[] = [];
    for (const item of data.results) {
      dropdowndata.push({ roleCode: item.roleCode, roleName: item.roleName });
    }
    return dropdowndata;
  }
}
