import * as moment from 'moment';

import { User } from '../models/user.model';
import { AddressData } from '../models/data.model';

export class UserManagementAdaptor {
  static usersfromJson(user: any): User {
    const address: AddressData =
      user && user.address ? user.address['data'] : '';
    const roledetails: any = this.userPrimaryRole(
      user && user.roles ? user.roles : []
    );

    if (address) {
      address.line1 =
        user.address['data'].line1 + ', ' + user.address['data'].line2;
      address.city = user.address['data'].city;
      address.state = user.address['data'].state;
      address.pincode = user.address['data'].pincode;
      address.country = user.address['data'].country;
    }

    return {
      emailId: user.emailId ? user.emailId : '',
      empName: user.empName ? user.empName : '',
      employeeCode: user.employeeCode ? user.employeeCode : '',
      hasLoginAccess: user.hasLoginAccess ? user.hasLoginAccess : false,
      isActive: user.isActive ? user.isActive : false,
      isLocked: user.isLocked ? user.isLocked : false,
      isLoginActive: user.isLoginActive ? user.isLoginActive : false,
      locationCode: user.locationCode ? user.locationCode : '',
      mobileNo: user.mobileNo ? user.mobileNo : '',
      userType: user.userType ? user.userType : '',
      address: address,
      birthDate: moment(user.birthDate),
      joiningDate: moment(user.joiningDate),
      resignationDate: moment(user.resignationDate),
      primaryRole: user.primaryRoleCode
        ? user.primaryRole
        : roledetails.primaryroledetails
        ? roledetails.primaryroledetails.role
        : '',
      roleName: user.primaryRoleName
        ? user.primaryRoleName
        : roledetails.primaryroledetails
        ? roledetails.primaryroledetails.roleName
        : '',
      secondaryRole: roledetails.secondaryroledetails
        ? roledetails.secondaryroledetails.role
        : '',
      secondaryRoleName: roledetails.secondaryroledetails
        ? roledetails.secondaryroledetails.roleName
        : '',
      secondaryendtime: moment(
        roledetails.secondaryroledetails
          ? roledetails.secondaryroledetails.endtime
          : ''
      ),
      secondarystarttime: moment(
        roledetails.secondaryroledetails
          ? roledetails.secondaryroledetails.starttime
          : ''
      )
    };
  }

  static userPrimaryRole(roles: any[]): any {
    const primaryroledetails = {};
    const secondaryroledetails = {};
    for (const role of roles) {
      if (role.isPrimary) {
        primaryroledetails['role'] = role.roleCode;
        primaryroledetails['roleName'] = role.roleName;
      } else {
        secondaryroledetails['role'] = role.roleCode;
        secondaryroledetails['roleName'] = role.roleName;
        secondaryroledetails['starttime'] = role.startTime;
        secondaryroledetails['endtime'] = role.expiryTime;
      }
    }
    return { secondaryroledetails, primaryroledetails };
  }
}
