export interface RolesRequest {
  isBTQUser: boolean;
  roleType: string;
  locationCode?: string;
}

export interface UserRequest {
  isBTQUser: boolean,
  employeeCode?: string;
  data?: any;
}
