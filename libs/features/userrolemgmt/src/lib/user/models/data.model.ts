export interface AddressData {
  line1: string;
  line2: string;
  city: string;
  state: string;
  pincode: string;
  country: string;
}

export interface CountryData {
  description: string;
  countryCode: string;
}

export interface EmailMobileData {
  fieldtype: string;
  value: string;
}

export interface LocationData {
  locationCode: string;
  countryCode: string;
}
