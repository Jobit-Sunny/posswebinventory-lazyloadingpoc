import { Moment } from 'moment';

import { AddressData } from './data.model';

export interface User {
  address: AddressData;
  emailId: string;
  empName: string;
  employeeCode: string;
  hasLoginAccess: boolean;
  isActive: boolean;
  isLocked: boolean;
  isLoginActive: boolean;
  joiningDate: Moment;
  locationCode: string;
  mobileNo: string;
  resignationDate: Moment;
  primaryRole: string;
  userType: string;
  birthDate: Moment;
  roleName: string;
  secondaryRole: string;
  secondaryRoleName: string;
  secondarystarttime: Moment;
  secondaryendtime: Moment;
}

export interface UserDetail {
  totalUsers: number;
  users: User[];
}

export interface UsersPage {
  isBTQUser: boolean;
  pageNumber: number;
  pageSize: number;
  employeeCode: string;
  roleCodes: string[];
  locationCodes: string[];
}
