import {
  Component,
  ViewChildren,
  QueryList,
  AfterViewInit,
  ElementRef
} from '@angular/core';
import { FocusKeyManager, FocusableOption } from '@angular/cdk/a11y';
import { Router } from '@angular/router';

@Component({
  selector: 'poss-web-uam-card',
  template: '<ng-content></ng-content>',
  styleUrls: ['./dashboard.component.scss']
})
export class CardComponent implements FocusableOption {
  constructor(private element: ElementRef) {}

  focus = (): void => this.element.nativeElement.focus();
}

@Component({
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements AfterViewInit {
  selectedtab = '';
  @ViewChildren(CardComponent) private cards: QueryList<CardComponent>;
  keyManager: FocusKeyManager<CardComponent>;

  constructor(private router: Router) {}

  ngAfterViewInit(): void {
    this.keyManager = new FocusKeyManager<CardComponent>(this.cards)
      .withHorizontalOrientation('ltr')
      .withWrap();
    this.keyManager.setFirstItemActive();
  }

  navigate = (path: string) =>
    this.router.navigate(['user-management/' + path]);
}
