import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ApiService } from '@poss-web/core';
import {
  getUpdateRoleUrl,
  addRoleUrl,
  getRoleDetailsUrl,
  getAllRolesUrl
} from '../../endpoints.constants';
import { RoleManagementHelper } from '../helpers/role-management.helper';
import { RoleManagementAdaptor } from '../adaptors/role-management.adaptor';
import { Role, RolesPage, RoleDetail } from '../models/role.model';

@Injectable({
  providedIn: 'root'
})
export class RoleManagementService {
  constructor(private apiService: ApiService) {}

  loadRoles = (pageDetails: RolesPage): Observable<RoleDetail> => {
    return this.apiService
      .get(
        getAllRolesUrl(
          pageDetails.pageNumber,
          pageDetails.pageSize,
          pageDetails.roleCode
        )
      )
      .pipe(map((data: any) => RoleManagementHelper.getRolesData(data)));
  };

  fetchRole = (roleCode: string): Observable<Role> => {
    return this.apiService
      .get(getRoleDetailsUrl(roleCode))
      .pipe(map((data: any) => RoleManagementAdaptor.getRoleData(data)));
  };

  updateRole = (roleCode: string, data: any): Observable<string[]> => {
    return this.apiService.patch(getUpdateRoleUrl(roleCode), data);
  };

  addRole = (data: any): Observable<string[]> => {
    return this.apiService.post(addRoleUrl(), data);
  };
}
