import { RoleManagementAdaptor } from '../adaptors/role-management.adaptor';
import { Role, RoleDetail } from '../models/role.model';

export class RoleManagementHelper {
  static getRolesData(data: any): RoleDetail {
    const roles: Role[] = [];
    for (const item of data.results) {
      roles.push(RoleManagementAdaptor.getRoleData(item));
    }
    return { roles, totalRoles: data.totalElements };
  }
}
