import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { Role } from '../../models/role.model';
import { RoleTypes } from '../../../common/model/userroleMgmnt.enum';

@Component({
  selector: 'poss-web-role-form',
  templateUrl: './role-form.component.html',
  styleUrls: ['./role-form.component.scss']
})
export class RoleFormComponent {
  roleForm: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Role,
    private formbuilder: FormBuilder,
    private roleFormDialog: MatDialogRef<RoleFormComponent>
  ) {
    roleFormDialog.disableClose = true;
    this.createRoleForm(this.data);
  }

  submitForm = () =>
    this.roleForm.valid
      ? this.roleFormDialog.close(this.roleForm)
      : this.roleForm.markAllAsTouched();

  createRoleForm(data: Role) {
    this.roleForm = this.formbuilder.group({
      roleType: [
        {
          value: data && data.roleType ? data.roleType : RoleTypes.Corporate,
          disabled: data && data.roleType
        },
        Validators.required
      ],
      roleName: [
        data && data.roleName ? data.roleName : '',
        [
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9 ]*$'),
          Validators.maxLength(100)
        ]
      ],
      roleCode: [
        {
          value: data && data.roleCode ? data.roleCode : '',
          disabled: data && data.roleCode
        },
        [
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9 ]*$'),
          Validators.maxLength(100)
        ]
      ],
      description: [
        data && data.description ? data.description : '',
        [
          Validators.required,
          Validators.maxLength(250),
          Validators.pattern('^[a-zA-Z0-9 ]*$')
        ]
      ]
    });
  }
}
