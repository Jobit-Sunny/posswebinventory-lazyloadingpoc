import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ViewChild,
  ElementRef,
  OnDestroy,
  AfterViewInit
} from '@angular/core';
import { MatSlideToggleChange, PageEvent } from '@angular/material';

import { Role } from '../../models/role.model';
import { fromEvent, Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'poss-web-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.scss']
})
export class RoleListComponent implements OnChanges, OnDestroy, AfterViewInit {
  @Input() rolesList: Role[] = [];
  @Input() rolesListSize = 0;
  @Input() pageSizeOptions: number[] = [];
  @Input() pageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };

  @Output() paginateRolesList = new EventEmitter<PageEvent>();
  @Output() updateRole = new EventEmitter<string>();
  @Output() toggleRole = new EventEmitter<MatSlideToggleChange>();
  @Output() searchRole = new EventEmitter<string>();

  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;

  minPageSize = 0;

  destroy$: Subject<null> = new Subject<null>();

  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['pageSizeOptions']) {
      this.minPageSize = this.pageSizeOptions.reduce((a: number, b: number) =>
        a < b ? a : b
      );
    }
  }

  ngAfterViewInit() {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe((event: any) => this.searchRole.emit(event.target.value));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
