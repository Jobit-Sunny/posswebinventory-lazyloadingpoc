import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Role } from '../../models/role.model';
import { MatSlideToggleChange } from '@angular/material';

@Component({
  selector: 'poss-web-role-list-item',
  templateUrl: './role-list-item.component.html',
  styleUrls: ['./role-list-item.component.scss']
})
export class RoleListItemComponent {
  @Input() role: Role;

  @Output() toggleRole = new EventEmitter<MatSlideToggleChange>();
  @Output() updateRole = new EventEmitter<string>();

  constructor() {}

}
