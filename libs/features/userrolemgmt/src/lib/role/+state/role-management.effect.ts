import { map } from 'rxjs/operators';
import { Effect } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { DataPersistence } from '@nrwl/angular';
import { HttpErrorResponse } from '@angular/common/http';

import {
  NotificationService,
  CustomErrors,
  CustomErrorAdaptor
} from '@poss-web/core';
import * as RoleManagementActions from '../+state/role-management.actions';
import { RoleManagementActionTypes } from '../+state/role-management.actions';
import { RoleManagementService } from '../services/role-management.service';
import { RoleManagementState } from '../+state/role-management.state';
import { Role, RoleDetail } from '../models/role.model';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class RoleManagementEffect {
  constructor(
    private service: RoleManagementService,
    private dataPersistence: DataPersistence<RoleManagementState>,
    private notificationService: NotificationService,
    private translate: TranslateService
  ) {}

  @Effect() loadRoles = this.dataPersistence.fetch(
    RoleManagementActionTypes.LOAD_ROLES,
    {
      run: (action: RoleManagementActions.LoadRoles) => {
        return this.service
          .loadRoles(action.payload)
          .pipe(
            map(
              (roledetails: RoleDetail) =>
                new RoleManagementActions.LoadRolesSuccess(roledetails)
            )
          );
      },

      onError: (
        action: RoleManagementActions.LoadRoles,
        error: HttpErrorResponse
      ) => {
        return new RoleManagementActions.LoadRolesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() fetchRole = this.dataPersistence.fetch(
    RoleManagementActionTypes.FETCH_ROLE,
    {
      run: (action: RoleManagementActions.FetchRole) => {
        return this.service
          .fetchRole(action.payload)
          .pipe(
            map(
              (role: Role) => new RoleManagementActions.FetchRoleSuccess(role)
            )
          );
      },

      onError: (
        action: RoleManagementActions.FetchRole,
        error: HttpErrorResponse
      ) => {
        return new RoleManagementActions.FetchRoleFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() UpdateRole = this.dataPersistence.pessimisticUpdate(
    RoleManagementActionTypes.UPDATE_ROLE,
    {
      run: (action: RoleManagementActions.UpdateRole) => {
        return this.service
          .updateRole(action.payload.roleCode, action.payload.data)
          .pipe(
            map(
              _ =>
                new RoleManagementActions.UpdateRoleSuccess(
                  action.payload.roleCode
                )
            )
          );
      },

      onError: (
        action: RoleManagementActions.UpdateRole,
        error: HttpErrorResponse
      ) => {
        return new RoleManagementActions.UpdateRoleFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() addRole = this.dataPersistence.pessimisticUpdate(
    RoleManagementActionTypes.ADD_ROLE,
    {
      run: (action: RoleManagementActions.AddRole) => {
        return this.service
          .addRole(action.payload)
          .pipe(
            map(
              _ =>
                new RoleManagementActions.AddRoleSuccess(
                  action.payload.roleCode
                )
            )
          );
      },

      onError: (
        action: RoleManagementActions.AddRole,
        error: HttpErrorResponse
      ) => {
        return new RoleManagementActions.AddRoleFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
}
