import { Role } from '../models/role.model';
import { CustomErrors } from '@poss-web/core';

export interface RoleManagementState {
  roles: Role[];
  updatedRole: string;
  fetchRole: Role;
  totalRoles: number;
  error: CustomErrors;
  isLoading: boolean;
}
