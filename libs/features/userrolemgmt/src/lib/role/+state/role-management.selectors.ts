import { selectUserRoleMgmnt } from '../../userrolemgmt.state';

import { createSelector } from '@ngrx/store';

const loadRoles = createSelector(
  selectUserRoleMgmnt,
  state => state.roleManagement.roles
);

const totalRoles = createSelector(
  selectUserRoleMgmnt,
  state => state.roleManagement.totalRoles
);

const fetchRole = createSelector(
  selectUserRoleMgmnt,
  state => state.roleManagement.fetchRole
);

const selectError = createSelector(
  selectUserRoleMgmnt,
  state => state.roleManagement.error
);

const addUpdateRole = createSelector(
  selectUserRoleMgmnt,
  state => state.roleManagement.updatedRole
);

const isLoading = createSelector(
  selectUserRoleMgmnt,
  state => state.roleManagement.isLoading
);

export const RoleManagementSelectors = {
  selectError,
  loadRoles,
  addUpdateRole,
  fetchRole,
  totalRoles,
  isLoading
};
