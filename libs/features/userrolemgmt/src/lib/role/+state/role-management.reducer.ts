import {
  RoleManagementActions,
  RoleManagementActionTypes
} from '../+state/role-management.actions';
import { RoleManagementState } from '../+state/role-management.state';

const initialState: RoleManagementState = {
  roles: [],
  updatedRole: null,
  fetchRole: null,
  totalRoles: 0,
  error: null,
  isLoading: false
};

export function RoleManagementReducer(
  state: RoleManagementState = initialState,
  action: RoleManagementActions
): RoleManagementState {
  switch (action.type) {
    case RoleManagementActionTypes.LOAD_ROLES:
    case RoleManagementActionTypes.FETCH_ROLE:
      return {
        ...state,
        isLoading: true,
        error: null
      };

    case RoleManagementActionTypes.LOAD_ROLES_SUCCESS:
      return {
        ...state,
        roles: action.payload.roles,
        totalRoles: action.payload.totalRoles,
        isLoading: false
      };

    case RoleManagementActionTypes.LOAD_ROLES_FAILURE:
      return {
        ...state,
        isLoading: false,
        roles: [],
        totalRoles: 0,
        error: action.payload
      };

    case RoleManagementActionTypes.FETCH_ROLE_SUCCESS:
      return {
        ...state,
        fetchRole: action.payload,
        isLoading: false
      };

    case RoleManagementActionTypes.FETCH_ROLE_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };

    case RoleManagementActionTypes.UPDATE_ROLE:
    case RoleManagementActionTypes.ADD_ROLE:
      return {
        ...state,
        isLoading: true,
        updatedRole: '',
        error: null
      };

    case RoleManagementActionTypes.UPDATE_ROLE_SUCCESS:
    case RoleManagementActionTypes.ADD_ROLE_SUCCESS:
      return {
        ...state,
        updatedRole: action.payload,
        isLoading: false
      };

    case RoleManagementActionTypes.UPDATE_ROLE_FAILURE:
    case RoleManagementActionTypes.ADD_ROLE_FAILURE:
      return {
        ...state,
        updatedRole: '',
        isLoading: false,
        error: action.payload
      };

    default:
      return state;
  }
}
