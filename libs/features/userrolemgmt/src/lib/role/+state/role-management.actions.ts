import { Action } from '@ngrx/store';

import { CustomErrors } from '@poss-web/core';
import { Role, RolesPage, RoleDetail } from '../models/role.model';
import { RoleRequest } from '../models/request.model';

export enum RoleManagementActionTypes {
  LOAD_ROLES = '[ role-management ] Load Roles',
  LOAD_ROLES_SUCCESS = '[ role-management ] Load Roles Success',
  LOAD_ROLES_FAILURE = '[ role-management ]  Load Roles Failure',

  UPDATE_ROLE = '[ role-management ] Update Role',
  UPDATE_ROLE_SUCCESS = '[ role-management ] Update Role Success',
  UPDATE_ROLE_FAILURE = '[ role-management ] Update Role Failure',

  ADD_ROLE = '[ role-management ] Add Role',
  ADD_ROLE_SUCCESS = '[ role-management ] Add Role Success',
  ADD_ROLE_FAILURE = '[ role-management ] Add Role Failure',

  FETCH_ROLE = '[ role-management ] Fetch Role',
  FETCH_ROLE_SUCCESS = '[ role-management ] Fetch Role Success',
  FETCH_ROLE_FAILURE = '[ role-management ] Fetch Role Failure'
}

export class FetchRole implements Action {
  readonly type = RoleManagementActionTypes.FETCH_ROLE;
  constructor(public readonly payload: string) {}
}

export class FetchRoleSuccess implements Action {
  readonly type = RoleManagementActionTypes.FETCH_ROLE_SUCCESS;
  constructor(public readonly payload: Role) {}
}

export class FetchRoleFailure implements Action {
  readonly type = RoleManagementActionTypes.FETCH_ROLE_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadRoles implements Action {
  readonly type = RoleManagementActionTypes.LOAD_ROLES;
  constructor(public readonly payload: RolesPage) {}
}

export class LoadRolesSuccess implements Action {
  readonly type = RoleManagementActionTypes.LOAD_ROLES_SUCCESS;
  constructor(public readonly payload: RoleDetail) {}
}

export class LoadRolesFailure implements Action {
  readonly type = RoleManagementActionTypes.LOAD_ROLES_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class UpdateRole implements Action {
  readonly type = RoleManagementActionTypes.UPDATE_ROLE;
  constructor(public readonly payload: RoleRequest) {}
}

export class UpdateRoleSuccess implements Action {
  readonly type = RoleManagementActionTypes.UPDATE_ROLE_SUCCESS;
  constructor(public readonly payload: string) {}
}

export class UpdateRoleFailure implements Action {
  readonly type = RoleManagementActionTypes.UPDATE_ROLE_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class AddRole implements Action {
  readonly type = RoleManagementActionTypes.ADD_ROLE;
  constructor(public readonly payload: any) {}
}

export class AddRoleSuccess implements Action {
  readonly type = RoleManagementActionTypes.ADD_ROLE_SUCCESS;
  constructor(public readonly payload: string) {}
}

export class AddRoleFailure implements Action {
  readonly type = RoleManagementActionTypes.ADD_ROLE_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export type RoleManagementActions =
  | LoadRoles
  | LoadRolesSuccess
  | LoadRolesFailure
  | UpdateRole
  | UpdateRoleSuccess
  | UpdateRoleFailure
  | AddRole
  | AddRoleSuccess
  | AddRoleFailure
  | FetchRole
  | FetchRoleSuccess
  | FetchRoleFailure;
