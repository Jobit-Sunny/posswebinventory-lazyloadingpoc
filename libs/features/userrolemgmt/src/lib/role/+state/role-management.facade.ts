import { State } from '../../userrolemgmt.state';
import * as RoleManagementActions from '../+state/role-management.actions';
import { RoleManagementSelectors } from '../+state/role-management.selectors';
import { RolesPage } from '../models/role.model';

import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

@Injectable()
export class RoleManagementFacade {
  private rolesList$ = this.store.select(RoleManagementSelectors.loadRoles);

  private totalRoles$ = this.store.select(RoleManagementSelectors.totalRoles);

  private selectError$ = this.store.select(RoleManagementSelectors.selectError);

  private fetchRole$ = this.store.select(RoleManagementSelectors.fetchRole);

  private isLoading$ = this.store.select(
    RoleManagementSelectors.isLoading
  );

  private addUpdateRole$ = this.store.select(
    RoleManagementSelectors.addUpdateRole
  );

  constructor(private store: Store<State>) {}

  getRolesList() {
    return this.rolesList$;
  }

  getTotalRoles() {
    return this.totalRoles$;
  }

  fetchRoleDetails() {
    return this.fetchRole$;
  }

  getAddUpdateRoleStatus() {
    return this.addUpdateRole$;
  }

  isLoading() {
    return this.isLoading$;
  }

  getError() {
    return this.selectError$;
  }

  loadRoles = (rolesPage: RolesPage) =>
    this.store.dispatch(new RoleManagementActions.LoadRoles(rolesPage));

  updateRole = (roleCode: string, data: any) =>
    this.store.dispatch(
      new RoleManagementActions.UpdateRole({ roleCode, data })
    );

  addRole = (data: any) =>
    this.store.dispatch(new RoleManagementActions.AddRole(data));

  fetchRole = (data: string) =>
    this.store.dispatch(new RoleManagementActions.FetchRole(data));
}
