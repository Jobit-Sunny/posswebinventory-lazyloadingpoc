import { Role } from '../models/role.model';

export class RoleManagementAdaptor {
  static getRoleData(data: any): Role {
    return {
      roleCode: data.roleCode,
      isActive: data.isActive,
      roleName: data.roleName,
      roleType: data.roleType,
      description: data.description,
      corpAccess: data.corpAccess
    };
  }
}
