export interface Role {
  roleCode: string;
  roleName: string;
  isActive: boolean;
  roleType: string;
  description: string;
  corpAccess: boolean;
}

export interface RoleDetail {
  totalRoles: number;
  roles: Role[];
}

export interface RolesPage {
  pageNumber: number;
  pageSize: number;
  roleCode: string;
}
