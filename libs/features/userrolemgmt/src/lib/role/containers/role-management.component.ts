import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subject, forkJoin } from 'rxjs';
import { filter, take, skip, takeUntil } from 'rxjs/operators';
import { MatDialog, MatSlideToggleChange, PageEvent } from '@angular/material';

import { Role } from '../models/role.model';
import { RoleManagementFacade } from '../+state/role-management.facade';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import { RoleFormComponent } from '../components/role-form/role-form.component';
import {
  OverlayNotificationType,
  OverlayNotificationService,
  OverlayNotificationEventType
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { StoreTypes, RoleTypes } from '../../common/model/userroleMgmnt.enum';

@Component({
  templateUrl: './role-management.component.html',
  styleUrls: ['./role-management.component.scss']
})
export class RoleManagementComponent implements OnInit, OnDestroy {
  rolesList$: Observable<Role[]>;
  private storetype = Array(
    StoreTypes.LargeFormatStoreType.toString(),
    StoreTypes.MediumFormatStoreType.toString(),
    StoreTypes.SmallFormatStoreType.toString()
  );
  isBTQUser = false;
  isLoading$: Observable<boolean>;
  hasNotification = false;
  pageSizeOptions: number[] = [];
  roleListSize$: Observable<number>;
  destroy$: Subject<null> = new Subject<null>();
  message = null;
  // error = null;

  rolesPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };

  searchRoleCode: string;

  constructor(
    private roleMgmntfacade: RoleManagementFacade,
    private dialog: MatDialog,
    private appsettingFacade: AppsettingFacade,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService
  ) {
    forkJoin(
      this.appsettingFacade.getStoreType().pipe(take(1)),
      this.appsettingFacade.getPageSize().pipe(take(1)),
      this.appsettingFacade.getPageSizeOptions().pipe(take(1))
    ).subscribe(val => {
      this.isBTQUser = this.storetype.includes(val[0]);
      this.rolesPageEvent.pageSize = val[1];
      this.pageSizeOptions = val[2];
    });

    this.loadRoles();
  }

  ngOnInit() {
    this.isLoading$ = this.roleMgmntfacade.isLoading();

    this.rolesList$ = this.roleMgmntfacade.getRolesList();
    this.roleListSize$ = this.roleMgmntfacade.getTotalRoles();

    this.roleMgmntfacade
      .getAddUpdateRoleStatus()
      .pipe(
        takeUntil(this.destroy$),
        filter(updated => !!updated)
      )
      .subscribe(role => {
        this.loadRoles();
        this.showNotification(undefined, this.message, true);
      });

    this.roleMgmntfacade
      .getError()
      .pipe(
        takeUntil(this.destroy$),
        filter(errorVal => !!errorVal)
      )
      .subscribe(errorVal => this.showNotification(errorVal));
  }

  editAddRole(roleCode: string) {
    if (roleCode) {
      this.roleMgmntfacade.fetchRole(roleCode);
      this.roleMgmntfacade
        .fetchRoleDetails()
        .pipe(
          skip(1),
          take(1)
        )
        .subscribe(roledetails => this.opendialog(roledetails));
    } else {
      this.opendialog(null);
    }
  }

  opendialog(roleDetails: Role) {
    const dialogref = this.dialog.open(RoleFormComponent, {
      width: '500px',
      data: roleDetails ? roleDetails : null
    });

    dialogref
      .afterClosed()
      .pipe(filter(value => value !== 'close'))
      .subscribe(form => {
        if (roleDetails) {
          this.updateRole({
            roleCode: form.controls['roleCode'].value,
            roleName: form.controls['roleName'].value,
            description: form.controls['description'].value
          });
          this.message = this.translate.instant(
            'pw.rolemanagementlistNotificationMessages.updaterolesuccessfullMessage',
            { rolename: form.controls['roleName'].value }
          );
        } else {
          this.roleMgmntfacade.addRole({
            ...form.value,
            ...((form.controls['roleType'].value === RoleTypes.Corporate ||
              form.controls['roleType'].value === RoleTypes.Regional) && {
              corpAccess: true
            }),
            ...(form.controls['roleType'].value === RoleTypes.Boutique && {
              corpAccess: false
            })
          });
          this.message = this.translate.instant(
            'pw.rolemanagementlistNotificationMessages.addrolesuccessfullMessage',
            { rolename: form.controls['roleName'].value }
          );
        }
      });
  }

  toggleRole(event: MatSlideToggleChange) {
    this.overlayConfirmation(
      event.source.name,
      event.source.id,
      event.checked,
      event.checked
        ? 'pw.rolemanagementlistNotificationMessages.activaterolewarningMessage'
        : 'pw.rolemanagementlistNotificationMessages.deactivaterolewarningMessage'
    );
    this.message = this.translate.instant(
      event.checked
        ? 'pw.rolemanagementlistNotificationMessages.activaterolesuccessfullMessage'
        : 'pw.rolemanagementlistNotificationMessages.deactivaterolesuccessfullMessage',
      { rolename: event.source.name }
    );
  }

  overlayConfirmation(
    roleName: string,
    roleCode: string,
    isActive: boolean,
    alertMessage: string
  ) {
    this.hasNotification = true;
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ACTION,
        message: this.translate.instant(alertMessage, {
          rolename: roleName
        }),
        hasBackdrop: true,
        buttonText: isActive ? 'Activate' : 'Deactivate',
        hasClose: true
      })
      .events.pipe(take(1))
      .subscribe(overlayEvent => {
        this.hasNotification = false;
        overlayEvent.eventType === OverlayNotificationEventType.TRUE
          ? this.updateRole({
              roleCode: roleCode,
              isActive: isActive
            })
          : this.loadRoles();
      });
  }

  updateRole = (roledetails: any) =>
    this.roleMgmntfacade.updateRole(roledetails.roleCode, {
      ...(roledetails.isActive !== undefined && {
        isActive: roledetails.isActive
      }),
      ...(roledetails.roleName && { roleName: roledetails.roleName }),
      ...(roledetails.description && { description: roledetails.description })
    });

  searchRole(event: string) {
    this.searchRoleCode = event;
    this.rolesPageEvent.pageIndex = 0;
    this.loadRoles();
  }
  paginateRolesList(event: PageEvent) {
    this.rolesPageEvent = event;
    this.loadRoles();
  }

  loadRoles = () =>
    this.roleMgmntfacade.loadRoles({
      pageNumber: this.rolesPageEvent.pageIndex,
      pageSize: this.rolesPageEvent.pageSize,
      roleCode: this.searchRoleCode
    });

  showNotification(error?: CustomErrors, message?: string, backDrop?: boolean) {
    this.hasNotification = true;
    this.overlayNotification
      .show({
        type: !!error
          ? OverlayNotificationType.ERROR
          : OverlayNotificationType.TIMER,
        message: message,
        error: error,
        hasBackdrop: backDrop,
        hasClose: !!error
      })
      .events.pipe(take(1))
      .subscribe(() => (this.hasNotification = false));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
