import { createFeatureSelector, ActionReducerMap } from '@ngrx/store';

import { AppState } from '@poss-web/core';
import { UserManagementState } from './user/+state/user-management.state';
import { UserManagementReducer } from './user/+state/user-management.reducer';
import { RoleManagementState } from './role/+state/role-management.state';
import { RoleManagementReducer } from './role/+state/role-management.reducer';
import { AccessControlManagementState } from './access-control/+state/access-control-management.state';
import { AccessControlManagementReducer } from './access-control/+state/access-control-management.reducer';
import { LoginManagementState } from './login/+state/login-management.state';
import { LoginManagementReducer } from './login/+state/login-management.reducer';

export const FEATURE_NAME = 'userrolemgmnt';

export interface UserRoleMgmntState {
  userManagement: UserManagementState;
  roleManagement: RoleManagementState;
  accessControlManagement: AccessControlManagementState;
  loginManagement: LoginManagementState;
}

export interface State extends AppState {
  userrolemgmnt: UserRoleMgmntState;
}

export const reducers: ActionReducerMap<UserRoleMgmntState> = {
  userManagement: UserManagementReducer,
  roleManagement: RoleManagementReducer,
  accessControlManagement: AccessControlManagementReducer,
  loginManagement: LoginManagementReducer
};

export const selectUserRoleMgmnt = createFeatureSelector<
  State,
  UserRoleMgmntState
>(FEATURE_NAME);
