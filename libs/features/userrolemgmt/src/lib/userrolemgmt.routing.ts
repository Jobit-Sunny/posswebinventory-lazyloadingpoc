import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginManagementComponent } from './login/container/login-management.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from '@poss-web/core';
import { UserListingComponent } from './user/containers/user-listing/user-listing.component';
import { UserDetailComponent } from './user/containers/user-detail/user-detail.component';
import { RoleManagementComponent } from './role/containers/role-management.component';
import { AccessControlManagementComponent } from './access-control/containers/access-control-management.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginManagementComponent
  },
  {
    path: 'generate-otp',
    component: LoginManagementComponent
  },
  {
    path: 'verify-otp',
    component: LoginManagementComponent
  },
  {
    path: 'activate-user',
    component: LoginManagementComponent
  },
  {
    path: '',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'users',
    component: UserListingComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'users/:id',
    component: UserDetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'roles',
    component: RoleManagementComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'access-control',
    component: AccessControlManagementComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserrolemgmtRoutingModule {}
