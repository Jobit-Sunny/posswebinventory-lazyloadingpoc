import { selectUserRoleMgmnt } from '../../userrolemgmt.state';

import { createSelector } from '@ngrx/store';

const generatedOtp = createSelector(
  selectUserRoleMgmnt,
  state => state.loginManagement.generatedOtp
);

const verifiedOtp = createSelector(
  selectUserRoleMgmnt,
  state => state.loginManagement.verifiedOtp
);

const fetchUsername = createSelector(
  selectUserRoleMgmnt,
  state => state.loginManagement.username
);

const selectError = createSelector(
  selectUserRoleMgmnt,
  state => state.loginManagement.error
);

const isLoading = createSelector(
  selectUserRoleMgmnt,
  state => state.loginManagement.isLoading
);

export const LoginManagementSelectors = {
  selectError,
  isLoading,
  generatedOtp,
  verifiedOtp,
  fetchUsername
};
