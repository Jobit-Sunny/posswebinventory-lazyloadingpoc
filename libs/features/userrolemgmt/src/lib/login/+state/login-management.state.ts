import { CustomErrors } from '@poss-web/core';

export interface LoginManagementState {
  generatedOtp: boolean;
  verifiedOtp: boolean;
  username: string;
  error: CustomErrors;
  isLoading: boolean;
}
