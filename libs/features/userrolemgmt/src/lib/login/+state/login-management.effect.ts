import { map, tap } from 'rxjs/operators';
import { Effect } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { DataPersistence } from '@nrwl/angular';
import { HttpErrorResponse } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';

import {
  NotificationService,
  CustomErrors,
  CustomErrorAdaptor,
  OneTimePasswordService,
  AuthFacade
} from '@poss-web/core';
import * as LoginActions from '../+state/login-management.actions';
import { LoginManagementState } from './login-management.state';
import { LoginManagementActionTypes } from '../+state/login-management.actions';

@Injectable()
export class LoginManagementEffect {
  constructor(
    private service: OneTimePasswordService,
    private dataPersistence: DataPersistence<LoginManagementState>,
    private notificationService: NotificationService,
    private translate: TranslateService
  ) {}

  @Effect() generateOtp = this.dataPersistence.fetch(
    LoginManagementActionTypes.GENERATE_OTP,
    {
      run: (action: LoginActions.GenerateOtp) => {
        return this.service
          .generateOtp(action.payload)
          .pipe(
            map(
              (otpgenerated: boolean) =>
                new LoginActions.GenerateOtpSuccess(otpgenerated)
            )
          );
      },

      onError: (action: LoginActions.GenerateOtp, error: HttpErrorResponse) => {
        return new LoginActions.GenerateOtpFailure(this.errorHandler(error));
      }
    }
  );

  @Effect() verifyOtp = this.dataPersistence.fetch(
    LoginManagementActionTypes.VERIFY_OTP,
    {
      run: (action: LoginActions.VerifyOtp) => {
        return this.service
          .verifyOtp(action.payload)
          .pipe(
            map(
              (otpverified: boolean) =>
                new LoginActions.VerifyOtpSuccess(otpverified)
            )
          );
      },

      onError: (action: LoginActions.VerifyOtp, error: HttpErrorResponse) => {
        return new LoginActions.VerifyOtpFailure(this.errorHandler(error));
      }
    }
  );

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
}
