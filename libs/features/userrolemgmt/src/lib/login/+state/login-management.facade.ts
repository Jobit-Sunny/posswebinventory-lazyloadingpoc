import { State } from '../../userrolemgmt.state';
import * as LoginManagementAction from './login-management.actions';
import { LoginManagementSelectors } from './login-management.selectors';
import { OtpDetails } from '../model/otpdetails.model';

import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

@Injectable()
export class LoginManagementFacade {
  private verifiedOtp$ = this.store.select(
    LoginManagementSelectors.verifiedOtp
  );

  private generatedOtp$ = this.store.select(
    LoginManagementSelectors.generatedOtp
  );

  private fetchUsername$ = this.store.select(
    LoginManagementSelectors.fetchUsername
  );

  private isLoading$ = this.store.select(LoginManagementSelectors.isLoading);

  private selectError$ = this.store.select(
    LoginManagementSelectors.selectError
  );

  constructor(private store: Store<State>) {}

  getOtpGenerated() {
    return this.generatedOtp$;
  }

  fetchUsername() {
    return this.fetchUsername$;
  }

  getOtpVerified() {
    return this.verifiedOtp$;
  }

  isLoading() {
    return this.isLoading$;
  }

  getError() {
    return this.selectError$;
  }

  generateOtp = (username: string) =>
    this.store.dispatch(new LoginManagementAction.GenerateOtp(username));

  verifyOtp = (otpdetails: OtpDetails) =>
    this.store.dispatch(new LoginManagementAction.VerifyOtp(otpdetails));
}
