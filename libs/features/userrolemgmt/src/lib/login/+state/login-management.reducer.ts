import { LoginManagementState } from './login-management.state';
import {
  LoginManagementActions,
  LoginManagementActionTypes
} from './login-management.actions';

const initialState: LoginManagementState = {
  generatedOtp: false,
  verifiedOtp: false,
  username: '',
  error: null,
  isLoading: false
};

export function LoginManagementReducer(
  state: LoginManagementState = initialState,
  action: LoginManagementActions
): LoginManagementState {
  switch (action.type) {
    case LoginManagementActionTypes.GENERATE_OTP:
      return {
        ...state,
        isLoading: true,
        username: action.payload,
        generatedOtp: false,
        error: null
      };

    case LoginManagementActionTypes.GENERATE_OTP_SUCCESS:
      return {
        ...state,
        generatedOtp: true,
        verifiedOtp: false,
        isLoading: false
      };

    case LoginManagementActionTypes.GENERATE_OTP_FAILURE:
      return {
        ...state,
        isLoading: false,
        generatedOtp: false,
        username: '',
        error: action.payload
      };

    case LoginManagementActionTypes.VERIFY_OTP:
      return {
        ...state,
        isLoading: true,
        verifiedOtp: false,
        error: null
      };

    case LoginManagementActionTypes.VERIFY_OTP_SUCCESS:
      return {
        ...state,
        verifiedOtp: true,
        isLoading: false
      };

    case LoginManagementActionTypes.VERIFY_OTP_FAILURE:
      return {
        ...state,
        isLoading: false,
        verifiedOtp: false,
        error: action.payload
      };

    default:
      return state;
  }
}
