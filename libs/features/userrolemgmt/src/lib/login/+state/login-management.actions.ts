import { Action } from '@ngrx/store';

import { CustomErrors } from '@poss-web/core';
import { OtpDetails } from '../model/otpdetails.model';

export enum LoginManagementActionTypes {
  GENERATE_OTP = '[ login ] Generate Otp',
  GENERATE_OTP_SUCCESS = '[ login ] Generate Otp Success',
  GENERATE_OTP_FAILURE = '[ login ] Generate Otp Failure',

  VERIFY_OTP = '[ login ] Verify Otp',
  VERIFY_OTP_SUCCESS = '[ login ] Verify Otp Success',
  VERIFY_OTP_FAILURE = '[ login ] Verify Otp Failure'
}

export class GenerateOtp implements Action {
  readonly type = LoginManagementActionTypes.GENERATE_OTP;
  constructor(public readonly payload: string) {}
}

export class GenerateOtpSuccess implements Action {
  readonly type = LoginManagementActionTypes.GENERATE_OTP_SUCCESS;
  constructor(public readonly payload: boolean) {}
}

export class GenerateOtpFailure implements Action {
  readonly type = LoginManagementActionTypes.GENERATE_OTP_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class VerifyOtp implements Action {
  readonly type = LoginManagementActionTypes.VERIFY_OTP;
  constructor(public readonly payload: OtpDetails) {}
}

export class VerifyOtpSuccess implements Action {
  readonly type = LoginManagementActionTypes.VERIFY_OTP_SUCCESS;
  constructor(public readonly payload: boolean) {}
}

export class VerifyOtpFailure implements Action {
  readonly type = LoginManagementActionTypes.VERIFY_OTP_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export type LoginManagementActions =
  | GenerateOtp
  | GenerateOtpSuccess
  | GenerateOtpFailure
  | VerifyOtp
  | VerifyOtpSuccess
  | VerifyOtpFailure;
