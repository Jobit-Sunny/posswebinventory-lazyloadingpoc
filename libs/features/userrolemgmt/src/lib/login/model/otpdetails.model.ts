export interface OtpDetails {
    newPassword: string;
    otp: string;
    otpType: string;
    empCode: string;
}
