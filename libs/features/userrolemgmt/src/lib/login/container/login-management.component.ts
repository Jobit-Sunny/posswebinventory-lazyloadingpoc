import { Component, OnInit, OnDestroy } from '@angular/core';
import { filter, take, tap, takeUntil } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';

import {
  OverlayNotificationService,
  OverlayNotificationType
} from '@poss-web/shared';
import { AuthFacade, CustomErrors } from '@poss-web/core';
import { Location } from '@angular/common';
import { LoginManagementFacade } from '../+state/login-management.facade';
import { TranslateService } from '@ngx-translate/core';
import { OtpDetails } from '../model/otpdetails.model';

@Component({
  templateUrl: './login-management.component.html',
  styleUrls: ['./login-management.component.scss']
})
export class LoginManagementComponent implements OnInit, OnDestroy {
  loginLoad$: Observable<boolean>;
  otpLoad$: Observable<boolean>;
  otpVerified$: Observable<boolean>;
  username$: Observable<string>;
  returnUrl = 'inventory';
  pageUrl = 'login';
  destroy$: Subject<null> = new Subject<null>();

  constructor(
    //@Inject(DOCUMENT) private document: any,
    private route: ActivatedRoute,
    private router: Router,
    private notification: OverlayNotificationService,
    private authfacade: AuthFacade,
    private location: Location,
    private loginfacade: LoginManagementFacade,
    private translate: TranslateService
  ) {
    // Authorization
    // if (
    //   new RegExp(
    //     '(?:^Authorization|;\\s*Authorization)=(.*?)(?:;|$)',
    //     'g'
    //   ).test(this.document.cookie)
    // ) {
    //   this.router.navigate(['inventory']);
    // }

    this.loginLoad$ = authfacade.isLoading();
    this.otpLoad$ = loginfacade.isLoading();
    this.otpVerified$ = loginfacade.getOtpVerified();
    this.username$ = this.loginfacade.fetchUsername();

    this.authfacade
      .getLoginError()
      .pipe(
        takeUntil(this.destroy$),
        filter(error => !!error)
      )
      .subscribe(error => this.showNotification(error));

    this.loginfacade
      .getError()
      .pipe(
        takeUntil(this.destroy$),
        filter(val => !!val)
      )
      .subscribe(error => this.showNotification(error));
  }

  ngOnInit() {
    this.pageUrl = this.router.url;
    // get return url from route parameters or default to '/'
    this.returnUrl =
      this.route.snapshot.queryParams['returnUrl'] || 'inventory';
    this.location.replaceState('/login');
  }

  login(event: any) {
    this.authfacade.login({
      userName: event.name,
      authorizationCode: event.password
    });

    // redirect to home if already logged in
    this.authfacade
      .isUserLoggedIn()
      .pipe(
        filter(userloggedIn => !!userloggedIn),
        take(1)
      )
      .subscribe(__ => this.router.navigate([this.returnUrl]));
  }

  generateOtp(event: string) {
    this.loginfacade.generateOtp(event);
    this.loginfacade
      .getOtpGenerated()
      .pipe(
        filter(val => !!val),
        take(1)
      )
      .subscribe(val =>
        this.showNotification(
          undefined,
          this.translate.instant(
            'pw.otpNotificationMessages.otpgenerationsuccessfullMessage'
          ),
          'user-management/verify-otp',
          true
        )
      );
  }

  validateOtp = (event: OtpDetails) => this.loginfacade.verifyOtp(event);

  validateNewUserOtp(event: OtpDetails) {
    this.loginfacade.verifyOtp(event);
    this.otpVerified$
      .pipe(
        filter(val => !!val),
        tap(otpverified =>
          this.authfacade.login({
            userName: event.empCode,
            authorizationCode: event.newPassword
          })
        ),
        take(1)
      )
      .subscribe(val =>
        this.showNotification(
          undefined,
          this.translate.instant(
            'pw.otpNotificationMessages.newuseractivatedsuccessfullMessage'
          ),
          this.returnUrl,
          true
        )
      );
  }

  showNotification(
    error?: CustomErrors,
    message?: string,
    navigationUrl?: string,
    backDrop?: boolean
  ) {
    this.notification
      .show({
        type: !!error
          ? OverlayNotificationType.ERROR
          : OverlayNotificationType.TIMER,
        message: message,
        error: error,
        hasBackdrop: backDrop,
        hasClose: !!error
      })
      .events.pipe(take(1))
      .subscribe(() => {
        if (backDrop) {
          this.router.navigate([navigationUrl]);
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
