import { Component, Output, EventEmitter } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';

import { OtpDetails } from '../../model/otpdetails.model';

@Component({
  selector: 'poss-web-new-user-activation-form',
  templateUrl: './new-user-activation-form.component.html',
  styleUrls: ['./new-user-activation-form.component.scss']
})
export class NewUserActivationFormComponent {
  @Output() validateOtp = new EventEmitter<OtpDetails>();

  form: FormGroup;

  constructor(private formbuilder: FormBuilder) {
    this.form = this.formbuilder.group({
      empCode: [
        '',
        [
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9]*$'),
          Validators.maxLength(15)
        ]
      ],
      otp: [
        '',
        [Validators.required, Validators.minLength(6), Validators.maxLength(6)]
      ],
      newPassword: ['', [Validators.required, Validators.minLength(8)]],
      retypepassword: ['', Validators.required]
    });

    this.form.controls['retypepassword'].setValidators(
      (fieldControl: FormControl) =>
        !this.form.get('newPassword').invalid
          ? fieldControl.value === this.form.get('newPassword').value
            ? null
            : { retype: true }
          : { parent: true }
    );
  }

  submitform() {
    const formvalue = this.form.value;
    this.validateOtp.emit({
      otp: formvalue.otp,
      newPassword: formvalue.newPassword,
      empCode: formvalue.empCode,
      otpType: 'INVITED'
    });
  }
}
