import { Component, Output, EventEmitter, Input } from '@angular/core';
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder
} from '@angular/forms';
import { OtpDetails } from '../../model/otpdetails.model';

@Component({
  selector: 'poss-web-otp-verification-form',
  templateUrl: './otp-verification-form.component.html',
  styleUrls: ['./otp-verification-form.component.scss']
})
export class OtpVerificationFormComponent {
  @Input() otpVerified: boolean;
  @Input() userName: string;

  @Output() validateOtp = new EventEmitter<OtpDetails>();
  @Output() generateOtp = new EventEmitter<string>();

  form: FormGroup;

  constructor(private formbuilder: FormBuilder) {
    this.form = this.formbuilder.group({
      otp: [
        '',
        [Validators.required, Validators.minLength(6), Validators.maxLength(6)]
      ],
      newPassword: ['', [Validators.required, Validators.minLength(8)]],
      retypepassword: ['']
    });

    this.form.controls['retypepassword'].setValidators([
      (fieldControl: FormControl) =>
        !this.form.get('newPassword').invalid
          ? fieldControl.value === this.form.get('newPassword').value
            ? null
            : { retype: true }
          : { parent: true },
      Validators.required
    ]);
  }

  submitform() {
    const formvalue = this.form.value;
    this.validateOtp.emit({
      otp: formvalue.otp,
      newPassword: formvalue.newPassword,
      empCode: this.userName,
      otpType: 'FORGOT_PASSWORD'
    });
  }
}
