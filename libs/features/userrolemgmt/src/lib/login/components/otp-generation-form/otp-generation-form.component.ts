import { Component, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'poss-web-otp-generation-form',
  templateUrl: './otp-generation-form.component.html',
  styleUrls: ['./otp-generation-form.component.scss']
})
export class OtpGenerationFormComponent {
  @Output() generateOtp = new EventEmitter<string>();

  name = new FormControl('', [
    Validators.required,
    Validators.pattern('^[a-zA-Z0-9]*$'),
    Validators.maxLength(15)
  ]);

  constructor() {}
}
