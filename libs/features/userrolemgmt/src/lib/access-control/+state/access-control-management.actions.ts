import { Action } from '@ngrx/store';

import { CustomErrors } from '@poss-web/core';
import { ACLDetails, ModuleDetails } from '../models/access-control.model';
import { ACLRequest } from '../models/request.model';
import { Role } from '../../common/model/userroleMgmnt.model';

export enum AccessControlManagementActionTypes {
  LOAD_ROLES = '[ access-control-management ] Load Roles',
  LOAD_ROLES_SUCCESS = '[ access-control-management ] Load Roles Success',
  LOAD_ROLES_FAILURE = '[ access-control-management ]  Load Roles Failure',

  LOAD_MODULES = '[ access-control-management ] Load Modules',
  LOAD_MODULES_SUCCESS = '[ access-control-management ] Load Modules Success',
  LOAD_MODULES_FAILURE = '[ access-control-management ] Load Modules Failure',

  LOAD_SUB_MODULES = '[ access-control-management ] Load Sub Modules',
  LOAD_SUB_MODULES_SUCCESS = '[ access-control-management ] Load Sub Modules Success',
  LOAD_SUB_MODULES_FAILURE = '[ access-control-management ] Load Sub Modules Failure',

  LOAD_ACL = '[ access-control-management ] Load Acl',
  LOAD_ACL_SUCCESS = '[ access-control-management ] Load Acl Success',
  LOAD_ACL_FAILURE = '[ access-control-management ] Load Acl Failure'
}

export class LoadAcl implements Action {
  readonly type = AccessControlManagementActionTypes.LOAD_ACL;
  constructor(public readonly payload: ACLRequest) {}
}

export class LoadAclSuccess implements Action {
  readonly type = AccessControlManagementActionTypes.LOAD_ACL_SUCCESS;
  constructor(public readonly payload: ACLDetails[]) {}
}

export class LoadAclFailure implements Action {
  readonly type = AccessControlManagementActionTypes.LOAD_ACL_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadSubModules implements Action {
  readonly type = AccessControlManagementActionTypes.LOAD_SUB_MODULES;
  constructor(public readonly payload: string) {}
}

export class LoadSubModulesSuccess implements Action {
  readonly type = AccessControlManagementActionTypes.LOAD_SUB_MODULES_SUCCESS;
  constructor(public readonly payload: ModuleDetails[]) {}
}

export class LoadSubModulesFailure implements Action {
  readonly type = AccessControlManagementActionTypes.LOAD_SUB_MODULES_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadModules implements Action {
  readonly type = AccessControlManagementActionTypes.LOAD_MODULES;
}

export class LoadModulesSuccess implements Action {
  readonly type = AccessControlManagementActionTypes.LOAD_MODULES_SUCCESS;
  constructor(public readonly payload: ModuleDetails[]) {}
}

export class LoadModulesFailure implements Action {
  readonly type = AccessControlManagementActionTypes.LOAD_MODULES_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadRoles implements Action {
  readonly type = AccessControlManagementActionTypes.LOAD_ROLES;
  constructor(public readonly payload: boolean) {}
}

export class LoadRolesSuccess implements Action {
  readonly type = AccessControlManagementActionTypes.LOAD_ROLES_SUCCESS;
  constructor(public readonly payload: Role[]) {}
}

export class LoadRolesFailure implements Action {
  readonly type = AccessControlManagementActionTypes.LOAD_ROLES_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export type AccessControlManagementActions =
  | LoadRoles
  | LoadRolesSuccess
  | LoadRolesFailure
  | LoadModules
  | LoadModulesSuccess
  | LoadModulesFailure
  | LoadSubModules
  | LoadSubModulesSuccess
  | LoadSubModulesFailure
  | LoadAcl
  | LoadAclFailure
  | LoadAclSuccess;
