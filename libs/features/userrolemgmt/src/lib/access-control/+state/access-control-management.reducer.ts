import { AccessControlManagementState } from './access-control-management.state';
import {
  AccessControlManagementActions,
  AccessControlManagementActionTypes
} from './access-control-management.actions';

const initialState: AccessControlManagementState = {
  error: null,
  modules: [],
  roles: [],
  isLoading: false,
  submodules: [],
  acl: []
};

export function AccessControlManagementReducer(
  state: AccessControlManagementState = initialState,
  action: AccessControlManagementActions
): AccessControlManagementState {
  switch (action.type) {
    case AccessControlManagementActionTypes.LOAD_ROLES:
    case AccessControlManagementActionTypes.LOAD_SUB_MODULES:
    case AccessControlManagementActionTypes.LOAD_ACL:
      return {
        ...state,
        isLoading: true,
        error: ''
      };

    case AccessControlManagementActionTypes.LOAD_ROLES_SUCCESS:
      return {
        ...state,
        roles: action.payload,
        isLoading: false
      };

    case AccessControlManagementActionTypes.LOAD_ROLES_FAILURE:
      return {
        ...state,
        isLoading: false,
        roles: [],
        error: action.payload.message
      };

    case AccessControlManagementActionTypes.LOAD_MODULES:
      return {
        ...state,
        isLoading: true,
        submodules: [],
        error: ''
      };

    case AccessControlManagementActionTypes.LOAD_MODULES_SUCCESS:
      return {
        ...state,
        modules: action.payload,
        isLoading: false
      };

    case AccessControlManagementActionTypes.LOAD_MODULES_FAILURE:
      return {
        ...state,
        isLoading: false,
        modules: [],
        error: action.payload.message
      };

    case AccessControlManagementActionTypes.LOAD_SUB_MODULES_SUCCESS:
      return {
        ...state,
        submodules: action.payload,
        isLoading: false
      };

    case AccessControlManagementActionTypes.LOAD_SUB_MODULES_FAILURE:
      return {
        ...state,
        isLoading: false,
        submodules: [],
        error: action.payload.message
      };

    case AccessControlManagementActionTypes.LOAD_ACL_SUCCESS:
      return {
        ...state,
        acl: action.payload,
        isLoading: false
      };

    case AccessControlManagementActionTypes.LOAD_ACL_FAILURE:
      return {
        ...state,
        isLoading: false,
        acl: [],
        error: action.payload.message
      };

    default:
      return state;
  }
}
