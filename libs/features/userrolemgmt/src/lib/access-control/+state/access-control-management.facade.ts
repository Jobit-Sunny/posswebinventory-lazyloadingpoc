import { State } from '../../userrolemgmt.state';
import { AccessControlManagementSelectors } from './access-control-management.selectors';
import * as AccessControlManagementActions from './access-control-management.actions';

import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { ACLRequest } from '../models/request.model';

@Injectable()
export class AccessControlManagementFacade {
  private rolesList$ = this.store.select(
    AccessControlManagementSelectors.loadRoles
  );

  private modulesList$ = this.store.select(
    AccessControlManagementSelectors.loadModules
  );

  private submodulesList$ = this.store.select(
    AccessControlManagementSelectors.loadSubModules
  );

  private aclList$ = this.store.select(
    AccessControlManagementSelectors.loadACL
  );

  private isLoading$ = this.store.select(
    AccessControlManagementSelectors.isLoading
  );

  private selectError$ = this.store.select(
    AccessControlManagementSelectors.selectError
  );

  constructor(private store: Store<State>) {}

  getError() {
    return this.selectError$;
  }

  isLoading() {
    return this.isLoading$;
  }

  getRolesList() {
    return this.rolesList$;
  }

  getModulesList() {
    return this.modulesList$;
  }

  getSubModulesList() {
    return this.submodulesList$;
  }

  getACLList() {
    return this.aclList$;
  }

  loadRoles = (isBTQUser: boolean) =>
    this.store.dispatch(new AccessControlManagementActions.LoadRoles(isBTQUser));

  loadModules = () =>
    this.store.dispatch(new AccessControlManagementActions.LoadModules());

  loadACL = (data: ACLRequest) =>
    this.store.dispatch(new AccessControlManagementActions.LoadAcl(data));

  loadSubModules = (subModule: string) =>
    this.store.dispatch(
      new AccessControlManagementActions.LoadSubModules(subModule)
    );
}
