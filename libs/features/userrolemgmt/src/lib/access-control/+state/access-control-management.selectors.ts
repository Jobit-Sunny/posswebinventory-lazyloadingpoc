import { selectUserRoleMgmnt } from '../../userrolemgmt.state';

import { createSelector } from '@ngrx/store';

const isLoading = createSelector(
  selectUserRoleMgmnt,
  state => state.accessControlManagement.isLoading
);

const selectError = createSelector(
  selectUserRoleMgmnt,
  state => state.accessControlManagement.error
);

const loadRoles = createSelector(
  selectUserRoleMgmnt,
  state => state.accessControlManagement.roles
);

const loadModules = createSelector(
  selectUserRoleMgmnt,
  state => state.accessControlManagement.modules
);

const loadSubModules = createSelector(
  selectUserRoleMgmnt,
  state => state.accessControlManagement.submodules
);

const loadACL = createSelector(
  selectUserRoleMgmnt,
  state => state.accessControlManagement.acl
);

export const AccessControlManagementSelectors = {
  selectError,
  loadRoles,
  loadModules,
  isLoading,
  loadSubModules,
  loadACL
};
