import { ACLDetails, ModuleDetails } from '../models/access-control.model';
import { Role } from '../../common/model/userroleMgmnt.model';

export interface AccessControlManagementState {
  roles: Role[];
  modules: ModuleDetails[];
  submodules: ModuleDetails[];
  error: string;
  isLoading: boolean;
  acl: ACLDetails[];
}
