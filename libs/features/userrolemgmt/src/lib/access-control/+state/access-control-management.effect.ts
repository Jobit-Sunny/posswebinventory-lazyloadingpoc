import { map } from 'rxjs/operators';
import { Effect } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { DataPersistence } from '@nrwl/angular';
import { HttpErrorResponse } from '@angular/common/http';

import {
  NotificationService,
  CustomErrors,
  CustomErrorAdaptor
} from '@poss-web/core';
import { AccessControlManagementState } from './access-control-management.state';
import { AccessControlManagementActionTypes } from './access-control-management.actions';
import { AccessControlManagementService } from '../services/access-control-management.service';
import * as AccessControlManagementActions from './access-control-management.actions';
import { ACLDetails, ModuleDetails } from '../models/access-control.model';
import { TranslateService } from '@ngx-translate/core';
import { Role } from '../../common/model/userroleMgmnt.model';

@Injectable()
export class AccessControlManagementEffect {
  constructor(
    private service: AccessControlManagementService,
    private notificationService: NotificationService,
    private dataPersistence: DataPersistence<AccessControlManagementState>,
    private translate: TranslateService
  ) {}

  @Effect() loadRoles = this.dataPersistence.fetch(
    AccessControlManagementActionTypes.LOAD_ROLES,
    {
      run: (action: AccessControlManagementActions.LoadRoles) => {
        return this.service
          .loadRoles(action.payload)
          .pipe(
            map(
              (roles: Role[]) =>
                new AccessControlManagementActions.LoadRolesSuccess(roles)
            )
          );
      },

      onError: (
        action: AccessControlManagementActions.LoadRoles,
        error: HttpErrorResponse
      ) => {
        return new AccessControlManagementActions.LoadRolesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadModules = this.dataPersistence.fetch(
    AccessControlManagementActionTypes.LOAD_MODULES,
    {
      run: (action: AccessControlManagementActions.LoadModules) => {
        return this.service
          .loadModuleslist()
          .pipe(
            map(
              (modules: ModuleDetails[]) =>
                new AccessControlManagementActions.LoadModulesSuccess(modules)
            )
          );
      },

      onError: (
        action: AccessControlManagementActions.LoadModules,
        error: HttpErrorResponse
      ) => {
        return new AccessControlManagementActions.LoadModulesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadSubModules = this.dataPersistence.fetch(
    AccessControlManagementActionTypes.LOAD_SUB_MODULES,
    {
      run: (action: AccessControlManagementActions.LoadSubModules) => {
        return this.service
          .loadModuleslist(action.payload)
          .pipe(
            map(
              (submodules: ModuleDetails[]) =>
                new AccessControlManagementActions.LoadSubModulesSuccess(
                  submodules
                )
            )
          );
      },

      onError: (
        action: AccessControlManagementActions.LoadSubModules,
        error: HttpErrorResponse
      ) => {
        return new AccessControlManagementActions.LoadSubModulesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadACL = this.dataPersistence.fetch(
    AccessControlManagementActionTypes.LOAD_ACL,
    {
      run: (action: AccessControlManagementActions.LoadAcl) => {
        return this.service
          .loadModuleslist(
            action.payload.module,
            action.payload.submodule,
            action.payload.rolecode
          )
          .pipe(
            map(
              (acl: ACLDetails[]) =>
                new AccessControlManagementActions.LoadAclSuccess(acl)
            )
          );
      },

      onError: (
        action: AccessControlManagementActions.LoadAcl,
        error: HttpErrorResponse
      ) => {
        return new AccessControlManagementActions.LoadAclFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
}
