import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ApiService } from '@poss-web/core';
import {
  getActiveRolesUrl,
  getListModulesUrl
} from '../../endpoints.constants';
import { AccessControlManagementHelper } from '../helpers/access-control-management.helper';
import { ACLDetails, ModuleDetails } from '../models/access-control.model';
import { Role } from '../../common/model/userroleMgmnt.model';

@Injectable({
  providedIn: 'root'
})
export class AccessControlManagementService {
  constructor(private apiService: ApiService) {}

  loadRoles = (isBTQUser: boolean): Observable<Role[]> => {
    return this.apiService
      .get(getActiveRolesUrl(isBTQUser))
      .pipe(
        map((data: any) => AccessControlManagementHelper.getRolesData(data))
      );
  };

  loadModuleslist = (
    module?: string,
    subModule?: string,
    roleCode?: string
  ): Observable<ModuleDetails[] | ACLDetails[]> => {
    return this.apiService
      .get(getListModulesUrl(module, subModule, roleCode))
      .pipe(
        map((data: any) =>
          roleCode
            ? AccessControlManagementHelper.getACLData(data)
            : AccessControlManagementHelper.getModulesList(data)
        )
      );
  };
}
