import { ACLDetails, ModuleDetails } from '../models/access-control.model';
import { Role } from '../../common/model/userroleMgmnt.model';

export class AccessControlManagementHelper {
  static getRolesData(data: any): Role[] {
    const roles: Role[] = [];
    for (const item of data.results) {
      roles.push({ roleCode: item.roleCode, roleName: item.roleName });
    }
    return roles;
  }

  static getACLData(data: any): ACLDetails[] {
    const acl: ACLDetails[] = [];
    for (const aclitem of data.results) {
      acl.push({
        aclCode: aclitem.aclCode,
        description: aclitem.description,
        isAssigned: aclitem.isAssigned
      });
    }
    return acl;
  }

  static getModulesList(data: any): ModuleDetails[] {
    const modules: ModuleDetails[] = [];
    for (const module of data.results) {
      modules.push({
        code: module.aclGroupCode,
        description: module.description
      });
    }
    return modules;
  }
}
