import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { FormControl } from '@angular/forms';

import { ACLDetails, ModuleDetails } from '../models/access-control.model';
import { ACLRequest } from '../models/request.model';
import { MatCheckboxChange } from '@angular/material';
import { Role } from '../../common/model/userroleMgmnt.model';

@Component({
  selector: 'poss-web-access-control-list',
  templateUrl: './access-control-list.component.html',
  styleUrls: ['./access-control-list.component.scss']
})
export class AccessControlListComponent implements OnChanges {
  @Input() rolesList: Role[] = [];
  @Input() modulesList: ModuleDetails[] = [];
  @Input() subModulesList: ModuleDetails[] = [];
  @Input() aclList: ACLDetails[] = [];

  @Output() loadModules = new EventEmitter<string>();
  @Output() loadSubModules = new EventEmitter<string>();
  @Output() loadACL = new EventEmitter<ACLRequest>();

  role = new FormControl();
  selectedmodule = '';
  selectedsubmodule = '';
  addACL = new Set<string>();
  removeACL = new Set<string>();

  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes['subModulesList'] &&
      changes['subModulesList'].currentValue &&
      this.subModulesList
    ) {
      this.loadACL.emit({
        module: this.selectedmodule,
        submodule: this.subModulesList[0].code,
        rolecode: this.role.value
      });
    }
  }

  loadSubModulesevent(subModule: string) {
    this.selectedsubmodule = subModule;
    this.loadACL.emit({
      module: this.selectedmodule,
      submodule: subModule,
      rolecode: this.role.value
    });
  }

  isAllACLSelected(): boolean {
    let count = 0;
    this.aclList.forEach(acl => {
      if (acl.isAssigned) {
        count++;
      }
    });
    return this.aclList.length === count;
  }

  updateACL(event: MatCheckboxChange, aclCode?: string) {
    aclCode
      ? event.checked
        ? this.addtoACL(aclCode)
        : this.removefromACL(aclCode)
      : this.aclList.forEach(acl => {
          acl.isAssigned = event.checked;
          event.checked
            ? this.addtoACL(acl.aclCode)
            : this.removefromACL(acl.aclCode);
        });
  }

  addtoACL(aclCode: string) {
    this.addACL.add(aclCode);
    this.removeACL.delete(aclCode);
  }

  removefromACL(aclCode: string) {
    this.addACL.delete(aclCode);
    this.removeACL.add(aclCode);
  }
}
