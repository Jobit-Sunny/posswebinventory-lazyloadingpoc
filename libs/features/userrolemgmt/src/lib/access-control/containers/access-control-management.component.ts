import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { filter, take, takeUntil } from 'rxjs/operators';

import { AccessControlManagementFacade } from '../+state/access-control-management.facade';
import { ACLDetails, ModuleDetails } from '../models/access-control.model';
import {
  OverlayNotificationType,
  OverlayNotificationService
} from '@poss-web/shared';
import { ACLRequest } from '../models/request.model';
import { StoreTypes } from '../../common/model/userroleMgmnt.enum';
import { AppsettingFacade } from '@poss-web/core';
import { Role } from '../../common/model/userroleMgmnt.model';

@Component({
  templateUrl: './access-control-management.component.html',
  styleUrls: ['./access-control-management.component.scss']
})
export class AccessControlManagementComponent implements OnInit, OnDestroy {
  rolesList$: Observable<Role[]>;
  modulesList$: Observable<ModuleDetails[]>;
  submodulesList$: Observable<ModuleDetails[]>;
  aclList$: Observable<ACLDetails[]>;
  isLoading$: Observable<boolean>;
  hasNotification = false;
  destroy$: Subject<null> = new Subject<null>();
  selectedRole: string;
  private storetype = Array(
    StoreTypes.LargeFormatStoreType.toString(),
    StoreTypes.MediumFormatStoreType.toString(),
    StoreTypes.SmallFormatStoreType.toString()
  );
  isBTQUser: boolean;

  constructor(
    private accesscontrolfacade: AccessControlManagementFacade,
    private overlayNotification: OverlayNotificationService,
    private appsettingFacade: AppsettingFacade
  ) {
    this.appsettingFacade
      .getStoreType()
      .pipe(take(1))
      .subscribe(val => (this.isBTQUser = this.storetype.includes(val)));
    accesscontrolfacade.loadRoles(this.isBTQUser);
  }

  ngOnInit() {
    this.isLoading$ = this.accesscontrolfacade.isLoading();
    this.rolesList$ = this.accesscontrolfacade.getRolesList();

    this.accesscontrolfacade
      .getError()
      .pipe(
        takeUntil(this.destroy$),
        filter(errorVal => !!errorVal)
      )
      .subscribe(errorVal => this.showNotification(errorVal));
  }

  loadModules(event: string) {
    this.selectedRole = event;
    this.accesscontrolfacade.loadModules();
    this.modulesList$ = this.accesscontrolfacade.getModulesList();
  }

  loadSubModules(event: string) {
    this.accesscontrolfacade.loadSubModules(event);
    this.submodulesList$ = this.accesscontrolfacade.getSubModulesList();
  }

  loadACL(event: ACLRequest) {
    this.accesscontrolfacade.loadACL(event);
    this.aclList$ = this.accesscontrolfacade.getACLList();
  }

  showNotification(message: string) {
    this.hasNotification = true;
    this.overlayNotification
      .show({
        type: OverlayNotificationType.TIMER,
        message: message
      })
      .events.pipe(take(1))
      .subscribe(() => (this.hasNotification = false));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
