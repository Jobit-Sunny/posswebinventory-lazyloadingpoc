export interface ACLDetails {
  aclCode: string;
  description: string;
  isAssigned: boolean;
}

export interface ModuleDetails {
  code: string;
  description: string;
}
