export interface ACLRequest {
  module: string;
  submodule: string;
  rolecode: string;
}
