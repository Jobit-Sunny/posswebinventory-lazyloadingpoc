import { Injectable } from '@angular/core';
import { ApiService } from '@poss-web/core';
import { DashboardAdaptor } from '../adaptors/dashboard.adaptor';
import { map } from 'rxjs/operators';
import {
  getStockTransferNoteCountEndpointUrl,
  getReceiveInvoiceEndpointUrl
} from '../../endpoints.constants';
import { Observable } from 'rxjs';
import {
  LoadSTNCountPayload,
  LoadReceiveInvoicePayload
} from '../+state/dashboard.actions';

@Injectable()
export class DashboardService {
  constructor(private apiService: ApiService) {}
  getSTNCount(): Observable<LoadSTNCountPayload> {
    const STNCountUrl = getStockTransferNoteCountEndpointUrl();
    return this.apiService
      .get(STNCountUrl)
      .pipe(map((data: any) => DashboardAdaptor.STNCountFromJson(data)));
  }

  getReceiveInvoice(): Observable<LoadReceiveInvoicePayload> {
    const receiveInvoiceUrl = getReceiveInvoiceEndpointUrl();
    return this.apiService
      .get(receiveInvoiceUrl)
      .pipe(map((data: any) => DashboardAdaptor.ReceiveInvoiceFromJson(data)));
  }
}
