import { createSelector } from '@ngrx/store';
import { selectInventory } from '../../inventory.state';

const selectPendingFactorySTNCount = createSelector(
  selectInventory,
  state => state.dashboard.pendingFactorySTNCount
);

const selectPendingBoutiqueSTNCount = createSelector(
  selectInventory,
  state => state.dashboard.pendingBoutiqueSTNCount
);

const selectPendingMerchandiseSTNcount = createSelector(
  selectInventory,
  state => state.dashboard.pendingMerchandiseSTNcount
);

const selectPendingCFASTNCount = createSelector(
  selectInventory,
  state => state.dashboard.pendingCFASTNCount
);

const selectIsLoadingCount = createSelector(
  selectInventory,
  state => state.dashboard.isLoadingCount
);

const selectError = createSelector(
  selectInventory,
  state => state.dashboard.error
);

export const DashboardSelectors = {
  selectPendingFactorySTNCount,
  selectPendingBoutiqueSTNCount,
  selectPendingMerchandiseSTNcount,
  selectPendingCFASTNCount,
  selectIsLoadingCount,
  selectError
};
