import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { DashboardSelectors } from './dashboard.selectors';
import { State } from '../../inventory.state';
import * as DashboardActions from './dashboard.actions';
import { of } from 'rxjs';
import { combineAll, map } from 'rxjs/operators';

@Injectable()
export class DashboardFacade {
  private pendingFactorySTNCount$ = this.store.select(
    DashboardSelectors.selectPendingFactorySTNCount
  );

  private pendingBoutiqueSTNCount$ = this.store.select(
    DashboardSelectors.selectPendingBoutiqueSTNCount
  );

  private pendingMerchandiseSTNcount$ = this.store.select(
    DashboardSelectors.selectPendingMerchandiseSTNcount
  );
  private pendingCFASTNCount$ = this.store.select(
    DashboardSelectors.selectPendingCFASTNCount
  );

  private isLoadingCount$ = this.store.select(
    DashboardSelectors.selectIsLoadingCount
  );

  private error$ = this.store.select(DashboardSelectors.selectError);

  constructor(private store: Store<State>) {}

  loadStockTransferNoteCount() {
    this.store.dispatch(new DashboardActions.LoadSTNCount());
  }

  loadReceiveInvoiceCount() {
    this.store.dispatch(new DashboardActions.LoadReceiveInvoice());
  }

  resetError() {
    this.store.dispatch(new DashboardActions.ResetError());
  }

  getPendingFactorySTNCount() {
    return this.pendingFactorySTNCount$;
  }

  getPendingBoutiqueSTNCount() {
    return this.pendingBoutiqueSTNCount$;
  }

  getPendingMerchandiseSTNcount() {
    return this.pendingMerchandiseSTNcount$;
  }

  getPendingCFASTNCount() {
    return this.pendingCFASTNCount$;
  }

  getIsLoadingCount() {
    return this.isLoadingCount$;
  }

  getPendingSTNCount() {
    const source = of(
      this.pendingFactorySTNCount$,
      this.pendingBoutiqueSTNCount$,
      this.pendingMerchandiseSTNcount$
    );
    return source.pipe(
      combineAll(),
      map(numbers => numbers.reduce((sum, n) => sum + n, 0))
    );
  }

  getReceiveInvoiceCount() {
    const source = of(this.pendingCFASTNCount$);
    return source.pipe(
      combineAll(),
      map(numbers => numbers.reduce((sum, n) => sum + n, 0))
    );
  }

  getError() {
    return this.error$;
  }
}
