import { CustomErrors } from '@poss-web/core';
import { Action } from '@ngrx/store';

export interface LoadSTNCountPayload {
  pendingFactorySTNCount: number;
  pendingBoutiqueSTNCount: number;
  pendingMerchandiseSTNcount: number;
}

export interface LoadReceiveInvoicePayload {
  pendingCFASTNCount: number;
}

export enum DashboardActionTypes {
  LoadSTNCount = '[Dashboard] Load STNCount',
  LoadSTNCountSuccess = '[Dashboard] Load STNCount Success',
  LoadSTNCountFailure = '[Dashboard] Load STNCount Failure',
  LoadReceiveInvoice = '[Dashboard] Load  ReceiveInvoice',
  LoadReceiveInvoiceSuccess = '[Dashboard] Load  ReceiveInvoice Success',
  LoadReceiveInvoiceFailure = '[Dashboard] Load  ReceiveInvoice Failure',
  RESET_ERROR = '[Dashboard] Reset Error'
}

export class LoadSTNCount implements Action {
  readonly type = DashboardActionTypes.LoadSTNCount;
}

export class LoadSTNCountSuccess implements Action {
  readonly type = DashboardActionTypes.LoadSTNCountSuccess;
  constructor(public payload: LoadSTNCountPayload) {}
}

export class LoadSTNCountFailure implements Action {
  readonly type = DashboardActionTypes.LoadSTNCountFailure;
  constructor(public payload: CustomErrors) {}
}

export class LoadReceiveInvoice implements Action {
  readonly type = DashboardActionTypes.LoadReceiveInvoice;
}

export class LoadReceiveInvoiceSuccess implements Action {
  readonly type = DashboardActionTypes.LoadReceiveInvoiceSuccess;
  constructor(public payload: LoadReceiveInvoicePayload) {}
}

export class LoadReceiveInvoiceFailure implements Action {
  readonly type = DashboardActionTypes.LoadReceiveInvoiceFailure;
  constructor(public payload: CustomErrors) {}
}

export class ResetError implements Action {
  readonly type = DashboardActionTypes.RESET_ERROR;
}

export type DashboardActions =
  | LoadSTNCount
  | LoadSTNCountSuccess
  | LoadSTNCountFailure
  | LoadReceiveInvoice
  | LoadReceiveInvoiceSuccess
  | LoadReceiveInvoiceFailure
  | ResetError;
