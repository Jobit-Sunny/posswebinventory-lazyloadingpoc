import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import { DashboardState } from './dashboard.state';
import {
  DashboardActionTypes,
  LoadSTNCount,
  LoadSTNCountSuccess,
  LoadSTNCountFailure,
  LoadSTNCountPayload,
  LoadReceiveInvoice,
  LoadReceiveInvoicePayload,
  LoadReceiveInvoiceFailure,
  LoadReceiveInvoiceSuccess
} from './dashboard.actions';
import { DataPersistence } from '@nrwl/angular';
import { DashboardService } from '../services/dashboard.service';
import {
  NotificationService,
  CustomErrors,
  CustomErrorAdaptor
} from '@poss-web/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';

@Injectable()
export class DashboardEffects {
  constructor(
    private dataPersistence: DataPersistence<DashboardState>,
    private dashboardService: DashboardService,
    private notificationService: NotificationService
  ) {}

  @Effect() loadStockTransferNoteCount$: Observable<
    Action
  > = this.dataPersistence.fetch(DashboardActionTypes.LoadSTNCount, {
    // provides an action and the current state of the store
    run: (action: LoadSTNCount, state: DashboardState) => {
      return this.dashboardService
        .getSTNCount()
        .pipe(
          map((data: LoadSTNCountPayload) => new LoadSTNCountSuccess(data))
        );
    },

    onError: (action: LoadSTNCount, error: HttpErrorResponse) => {
      return new LoadSTNCountFailure(this.errorHandler(error));
    }
  });

  @Effect() loadReceiveInvoiceCount$: Observable<
    Action
  > = this.dataPersistence.fetch(DashboardActionTypes.LoadReceiveInvoice, {
    // provides an action and the current state of the store
    run: (action: LoadReceiveInvoice, state: DashboardState) => {
      return this.dashboardService
        .getReceiveInvoice()
        .pipe(
          map(
            (data: LoadReceiveInvoicePayload) =>
              new LoadReceiveInvoiceSuccess(data)
          )
        );
    },

    onError: (action: LoadReceiveInvoice, error: HttpErrorResponse) => {
      return new LoadReceiveInvoiceFailure(this.errorHandler(error));
    }
  });

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
}
