import { DashboardState } from './dashboard.state';
import { DashboardActions, DashboardActionTypes } from './dashboard.actions';

export const initialState: DashboardState = {
  pendingFactorySTNCount: 0,
  pendingBoutiqueSTNCount: 0,
  pendingMerchandiseSTNcount: 0,
  pendingCFASTNCount: 0,
  isLoadingCount: false,
  error: null
};

export function DashboardReducer(
  state = initialState,
  action: DashboardActions
): DashboardState {
  switch (action.type) {
    case DashboardActionTypes.LoadSTNCount:
    case DashboardActionTypes.LoadReceiveInvoice:
      return {
        ...state,
        error: null,
        isLoadingCount: true
      };

    case DashboardActionTypes.LoadSTNCountFailure:
    case DashboardActionTypes.LoadReceiveInvoiceFailure:
      return {
        ...state,
        error: action.payload,
        isLoadingCount: false
      };

    case DashboardActionTypes.LoadSTNCountSuccess:
      return {
        ...state,
        pendingFactorySTNCount: action.payload.pendingFactorySTNCount,
        pendingBoutiqueSTNCount: action.payload.pendingBoutiqueSTNCount,
        pendingMerchandiseSTNcount: action.payload.pendingMerchandiseSTNcount,
        isLoadingCount: false
      };

    case DashboardActionTypes.LoadReceiveInvoiceSuccess:
      return {
        ...state,
        pendingCFASTNCount: action.payload.pendingCFASTNCount,
        isLoadingCount: false
      };

    case DashboardActionTypes.RESET_ERROR:
      return {
        ...state,
        error: null
      };
  }
  return state;
}
