import { CustomErrors } from '@poss-web/core';

export interface DashboardState {
  pendingFactorySTNCount: number;
  pendingBoutiqueSTNCount: number;
  pendingMerchandiseSTNcount: number;
  pendingCFASTNCount: number;
  isLoadingCount: boolean;
  error: CustomErrors;
}
