import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { DashboardFacade } from '../../+state/dashboard.facade';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import {
  getInStockRouteUrl,
  getStockReceiveRouteUrl,
  getStockIssueRouteUrl
} from '../../../page-route.constants';
import { DataUploadFacade } from '../../../data-upload/+state/data-upload.facade';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { FileTypesEnum } from '../../../data-upload/models/data-upload.enum';
import { StockIssueFacade } from '../../../issue/stock-issue/+state/stock-issue.facade';
import { StockIssueTypesEnum } from '../../../issue/stock-issue/models/stock-issue.enum';
import { StockReceiveTypesEnum } from '../../../receive-stock/stock-receive/models/stock-receive.enum';

const CSVExtn = 'csv';
const reqfileKey = 'reqfile';
const stockFileKey = 'stockFile';

@Component({
  selector: 'poss-web-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  stockTransferNoteCount$: Observable<number>;
  receiveInvoiceCount$: Observable<number>;
  storeType: string;
  defaultStockReceivePath = '';
  destroy$: Subject<null> = new Subject<null>();

  defaultStockIssuePath = '';
  issueTransferNoteCount$: Observable<number>;
  issueInvoiceCount$: Observable<number>;

  // data-upload

  FIRFileUploadResponse$: Observable<boolean>;
  MERFileUploadResponse$: Observable<boolean>;
  InvoiceUploadResponse$: Observable<any>;
  STNUploadResponse$: Observable<any>;
  isLoading$: Observable<boolean>;

  constructor(
    private dashboardFacade: DashboardFacade,
    private appsettingFacade: AppsettingFacade,
    private route: Router,
    private stockIssueFacade: StockIssueFacade,
    private dataUploadFacade: DataUploadFacade,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService,
    private router: Router
  ) {
    this.appsettingFacade
      .getStoreType()
      .pipe(takeUntil(this.destroy$))
      .subscribe((storeType: string) => {
        if (storeType) {
          this.storeType = storeType;
          this.componentInit();
        }
      });
  }
  ngOnInit() {
    this.dataUploadFacade.clearResponse();
    this.componentInit1();
  }

  componentInit() {
    if (this.isL1L2Store()) {
      this.defaultStockReceivePath = StockReceiveTypesEnum.FAC_BTQ;
      this.dashboardFacade.loadStockTransferNoteCount();
      this.stockTransferNoteCount$ = this.dashboardFacade.getPendingSTNCount();

      this.defaultStockIssuePath = StockIssueTypesEnum.FAC_BTQ;
      this.stockIssueFacade.LoadIssueSTNCount();
      this.issueTransferNoteCount$ = this.stockIssueFacade.getPendingIssueSTNCount();
    }

    if (this.isL3Store()) {
      this.defaultStockReceivePath = StockReceiveTypesEnum.CFA_BTQ;
      this.dashboardFacade.loadReceiveInvoiceCount();
      this.receiveInvoiceCount$ = this.dashboardFacade.getReceiveInvoiceCount();

      this.defaultStockIssuePath = StockIssueTypesEnum.BTQ_CFA;
      // this.stockIssueFacade.LoadIssueInvoiceCount();
      // this.issueInvoiceCount$ = this.stockIssueFacade.getIssueInvoiceCount();
    }
  }

  stockReceiveUrl() {
    this.router.navigate([
      getStockReceiveRouteUrl(this.defaultStockReceivePath)
    ]);
  }

  inStockUrl() {
    this.router.navigate([getInStockRouteUrl()]);
  }

  stockIssueUrl() {
    this.router.navigate([getStockIssueRouteUrl(this.defaultStockIssuePath)]);
  }

  componentInit1() {
    this.FIRFileUploadResponse$ = this.dataUploadFacade.getFIRFileUploadResponse();
    this.MERFileUploadResponse$ = this.dataUploadFacade.getMERFileUploadResponse();
    this.InvoiceUploadResponse$ = this.dataUploadFacade.getInvoiceUploadResponse();
    this.STNUploadResponse$ = this.dataUploadFacade.getSTNUploadResponse();
    this.isLoading$ = this.dataUploadFacade.getIsLoading();

    this.dataUploadFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.FIRFileUploadResponse$.pipe(takeUntil(this.destroy$)).subscribe(
      data => {
        if (data === true) {
          const successKey = 'pw.fileUpload.FIRFileUploadSuccessMessage';
          this.showNotifications(successKey);
        }
      }
    );

    this.MERFileUploadResponse$.pipe(takeUntil(this.destroy$)).subscribe(
      data => {
        if (data === true) {
          const successKey = 'pw.fileUpload.MERFileUploadSuccessMessage';
          this.showNotifications(successKey);
        }
      }
    );

    this.InvoiceUploadResponse$.pipe(takeUntil(this.destroy$)).subscribe(
      data => {
        this.loadNotifications(data);
      }
    );

    this.STNUploadResponse$.pipe(takeUntil(this.destroy$)).subscribe(data => {
      this.loadNotifications(data);
    });
  }

  loadNotifications(data) {
    if (data !== null) {
      this.showNotifications(data.message);
    }
  }

  isL1L2Store(): boolean {
    return this.storeType === 'L1' || this.storeType === 'L2';
  }

  isL3Store(): boolean {
    return this.storeType === 'L3';
  }

  isISCMUser(): boolean {
    return this.storeType === 'ORG';
  }

  accessCheck() {
    if (!this.isL3Store()) {
      this.route.navigate([getInStockRouteUrl()]);
    }
  }

  uploadFile(event) {
    this.dataUploadFacade.clearResponse();
    this.overlayNotification.close();
    const fileList: FileList = event.target.files;
    const formData: FormData = new FormData();
    if (fileList.length > 0) {
      const file: File = fileList[0];
      if (file.size > 10000000) {
        const errorKey = 'pw.fileUpload.maximumFileSizeErrorMessage';
        this.showNotifications(errorKey);
      }
      const extn = file.name.split('.').pop();
      if (extn !== CSVExtn) {
        const errorKey = 'pw.fileUpload.CSVFileTypeErrorMessage';
        this.showNotifications(errorKey);
      }

      const type = file.name.substring(0, 3);
      formData.append(reqfileKey, file);
      if (extn === CSVExtn && file.size < 10000000) {
        if (type === FileTypesEnum.FIR) {
          this.dataUploadFacade.loadFIRFileUpload(formData);
        }
        if (type === FileTypesEnum.MER) {
          this.dataUploadFacade.loadMERFileUpload(formData);
        }
      }
    }
  }

  invoiceUpload(event) {
    this.dataUploadFacade.clearResponse();
    this.overlayNotification.close();
    const fileList: FileList = event.target.files;
    const formData: FormData = new FormData();
    if (fileList.length > 0) {
      const file: File = fileList[0];
      formData.append(stockFileKey, file);
    }
    this.dataUploadFacade.loadInvoiceUpload(formData);
  }

  STNUpload(event) {
    this.dataUploadFacade.clearResponse();
    this.overlayNotification.close();
    const fileList: FileList = event.target.files;
    const formData: FormData = new FormData();
    if (fileList.length > 0) {
      const file: File = fileList[0];
      formData.append(stockFileKey, file);
    }
    this.dataUploadFacade.loadSTNUpload(formData);
  }

  showNotifications(key: string) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification.show({
          type: OverlayNotificationType.SIMPLE,
          message: translatedMsg,
          hasClose: true
        });
      });
  }

  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true, // optional
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        // Action based event
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
