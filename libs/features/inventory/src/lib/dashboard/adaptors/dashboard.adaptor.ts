import {
  LoadSTNCountPayload,
  LoadReceiveInvoicePayload
} from '../+state/dashboard.actions';
import { StockReceiveAPITypesEnum } from '../../receive-stock/stock-receive/models/stock-receive.enum';

export class DashboardAdaptor {
  static STNCountFromJson(data: any): LoadSTNCountPayload {
    let pendingFactorySTNCount = 0;
    let pendingBoutiqueSTNCount = 0;
    let pendingMerchandiseSTNcount = 0;

    for (const stnCount of data.results) {
      if (stnCount.type === StockReceiveAPITypesEnum.FAC_BTQ) {
        pendingFactorySTNCount = stnCount.count;
      }
      if (stnCount.type === StockReceiveAPITypesEnum.BTQ_BTQ) {
        pendingBoutiqueSTNCount = stnCount.count;
      }
      if (stnCount.type === StockReceiveAPITypesEnum.MER_BTQ) {
        pendingMerchandiseSTNcount = stnCount.count;
      }
    }

    return {
      pendingFactorySTNCount,
      pendingBoutiqueSTNCount,
      pendingMerchandiseSTNcount
    };
  }

  static ReceiveInvoiceFromJson(data: any): LoadReceiveInvoicePayload {
    let pendingCFASTNCount = 0;

    for (const CFACount of data.results) {
      if (CFACount.type === StockReceiveAPITypesEnum.CFA_BTQ) {
        pendingCFASTNCount = CFACount.count;
      }
    }

    return {
      pendingCFASTNCount
    };
  }
}
