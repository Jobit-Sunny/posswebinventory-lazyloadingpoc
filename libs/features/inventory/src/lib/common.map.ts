import { InterBoutiqueTransferStatusColorsEnum } from './in-stock-management/inter-boutique-transfer/models/inter-boutique-transfer.enum';

export const commonTranslateKeyMap = new Map();

commonTranslateKeyMap.set('REQUESTED', {
  status: 'pw.statusMessages.REQUESTED',
  statusColor: InterBoutiqueTransferStatusColorsEnum.REQUESTED
});
commonTranslateKeyMap.set('ACCEPTED', {
  status: 'pw.statusMessages.ACCEPTED',
  statusColor: InterBoutiqueTransferStatusColorsEnum.ACCEPTED
});
commonTranslateKeyMap.set('ACPT_REJECTED', {
  status: 'pw.statusMessages.ACPT_REJECTED',
  statusColor: InterBoutiqueTransferStatusColorsEnum.ACPT_REJECTED
});
commonTranslateKeyMap.set('APPROVED', {
  status: 'pw.statusMessages.APPROVED',
  statusColor: InterBoutiqueTransferStatusColorsEnum.APPROVED
});
commonTranslateKeyMap.set('APVL_PENDING', {
  status: 'pw.statusMessages.APVL_PENDING',
  statusColor: InterBoutiqueTransferStatusColorsEnum.APVL_PENDING
});
commonTranslateKeyMap.set('APVL_REJECTED', {
  status: 'pw.statusMessages.APVL_REJECTED',
  statusColor: InterBoutiqueTransferStatusColorsEnum.APVL_REJECTED
});
commonTranslateKeyMap.set('CANCELLED', {
  status: 'pw.statusMessages.CANCELLED',
  statusColor: InterBoutiqueTransferStatusColorsEnum.CANCELLED
});
commonTranslateKeyMap.set('EXPIRED', {
  status: 'pw.statusMessages.EXPIRED',
  statusColor: InterBoutiqueTransferStatusColorsEnum.EXPIRED
});
commonTranslateKeyMap.set('SELECTED', {
  status: 'pw.statusMessages.SELECTED',
  statusColor: InterBoutiqueTransferStatusColorsEnum.SELECTED
});
commonTranslateKeyMap.set('ISSUED', {
  status: 'pw.statusMessages.ISSUED',
  statusColor: InterBoutiqueTransferStatusColorsEnum.ISSUED
});

export enum commonEnums {
  Requested = 'REQUESTED',
  Accepted = 'ACCEPTED',
  Accept_Rejected = 'ACPT_REJECTED',
  Approved = 'APPROVED',
  Approval_Pending = 'APVL_PENDING',
  Approval_Rejected = 'APVL_REJECTED',
  Cancelled = 'CANCELLED',
  Expired = 'EXPIRED',
  Selected = 'SELECTED',
  Issued = 'ISSUED'
}
