import { StockAdaptor } from '../adaptors/stock.adaptor';
import { LoadItemsTotalCountSuccessPayload } from '../+state/stock-receive.actions';
import { ItemAdaptor } from '../adaptors/item.adaptor';
import { ItemHelper } from '../helpers/item.helper';
import {
  Stock,
  ItemUpdate,
  Item,
  ConfirmReceive
} from '../models/stock-receive.model';
import { ApiService } from '@poss-web/core';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import {
  getStockReceiveByPaginationEndpointUrl,
  getStockReceiveBySrcDocNoEndpointUrl,
  getPurchaseInvoiceByPaginationEndpointUrl,
  getPurchaseInvoiceBySrcDocNoEndpointUrl,
  getStockReceiveConfirmSTNEndpointUrl,
  getStockReceiveVerifyItemEndpointUrl,
  getStockReceiveUpdateAllItemsEndpointUrl,
  getStockByIdEndpointUrl,
  getInvociePurchaseByIdEndpointUrl,
  getStockReceiveItemsEndpointUrl,
  getStockReceiveValidateItemEndpointUrl
} from '../../../endpoints.constants';
import {
  StockItemStatusEnum,
  StockTypeField
} from '../models/stock-receive.enum';
import { StockHelper } from '../helpers/stock.helper';

const tolerance = 0.03;

/**
 * Stcok Receive Service
 */
@Injectable()
export class StockReceiveService {
  constructor(private apiService: ApiService) {}
  /**
   * Service to Get stocks from the server
   * @param type : type of transfer
   * @param pageIndex : next page index to be loaded
   * @param pageSize : Page Size to be loade
   */
  getStocks(
    type: string,
    pageIndex: number,
    pageSize: number
  ): Observable<Stock[]> {
    const url = getStockReceiveByPaginationEndpointUrl(
      type,
      pageIndex,
      pageSize
    );
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) =>
          StockHelper.getStocks(data.results, StockTypeField.STN)
        )
      );
  }

  /**
   * Service to Get Invoices from the server
   * @param type : type of transfer
   * @param pageIndex : next page index to be loaded
   * @param pageSize : Page Size to be loade
   */

  getInvoices(
    type: string,
    pageIndex: number,
    pageSize: number
  ): Observable<Stock[]> {
    const url = getPurchaseInvoiceByPaginationEndpointUrl(
      type,
      pageIndex,
      pageSize
    );
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) =>
          StockHelper.getStocks(data.results, StockTypeField.INVOICE)
        )
      );
  }

  /**
   * Service to Search stocks from the server by srcDocNumber and  type of transfer (L1/L2)
   * @param srcdocno : srcdocno
   * @param type : type of transfer
   */
  searchStocks(srcdocno: number, type: string): Observable<Stock[]> {
    const url = getStockReceiveBySrcDocNoEndpointUrl(srcdocno, type);
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) =>
          StockHelper.getStocks(data.results, StockTypeField.STN)
        )
      );
  }

  /**
   * Service to Search invoices from the server by srcDocNumber and  type of transfer (L3)
   * @param srcdocno : srcdocno
   * @param type : type of transfer
   */
  searchInovices(srcdocno: number, type: string): Observable<Stock[]> {
    const url = getPurchaseInvoiceBySrcDocNoEndpointUrl(srcdocno, type);
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) =>
          StockHelper.getStocks(data.results, StockTypeField.INVOICE)
        )
      );
  }

  /**
   * Service to get Stock by srcdocno and Type (L1/L2)
   * @param srcdocno : srcdocno
   * @param type : type of transfer
   */
  getStock(id: string, type: string): Observable<Stock> {
    const url = getStockByIdEndpointUrl(id, type);
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) => StockAdaptor.fromJson(data, StockTypeField.STN))
      );
  }

  /**
   * Service to get Invoice by srcdocno and Type (L3)
   * @param srcdocno : srcdocno
   * @param type : type of transfer
   */
  getInvoice(id: string, type: string): Observable<Stock> {
    const url = getInvociePurchaseByIdEndpointUrl(id, type);
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) => StockAdaptor.fromJson(data, StockTypeField.INVOICE))
      );
  }

  getTolerance(): Observable<number> {
    // * in grams
    return of(tolerance);
  }

  getItemsCount(
    storeType: string,
    type: string,
    id: number
  ): Observable<LoadItemsTotalCountSuccessPayload> {
    const nonVerifiedCountUrl = getStockReceiveItemsEndpointUrl(
      storeType,
      type,
      StockItemStatusEnum.ISSUED,
      id,
      null,
      null,
      0,
      1,
      null,
      null,
      null
    );
    const verifiedCountUrl = getStockReceiveItemsEndpointUrl(
      storeType,
      type,
      StockItemStatusEnum.VERIFIED,
      id,
      null,
      null,
      0,
      1,
      null,
      null,
      null
    );
    return this.apiService
      .get(nonVerifiedCountUrl.path, nonVerifiedCountUrl.params)
      .pipe(map((data: any) => data.totalElements))
      .pipe(
        mergeMap(nonVerifiedItemsTotalCount =>
          this.apiService
            .get(verifiedCountUrl.path, verifiedCountUrl.params)
            .pipe(
              map((data: any) => ({
                nonVerifiedItemsTotalCount: nonVerifiedItemsTotalCount,
                verifiedItemsTotalCount: data.totalElements
              }))
            )
        )
      );
  }

  getItems(
    storeType: string,
    type: string,
    status: string,
    id: number,
    itemCode: string,
    lotNumber: string,
    pageIndex: number,
    pageSize: number,
    sortBy: string,
    sortOrder: string,
    filter: { key: string; value: any[] }[]
  ): Observable<{ items: Item[]; count: number }> {
    const url = getStockReceiveItemsEndpointUrl(
      storeType,
      type,
      status,
      id,
      itemCode,
      lotNumber,
      pageIndex,
      pageSize,
      sortBy,
      sortOrder,
      filter
    );
    return this.apiService
      .get(url.path, url.params)
      .pipe(map((data: any) => ItemHelper.getItems(data)));
  }

  verifyItem(
    storeType: string,
    type: string,
    id: number,
    itemId: string,
    itemUpdate: ItemUpdate
  ): Observable<Item> {
    const url = getStockReceiveVerifyItemEndpointUrl(
      storeType,
      type,
      id,
      itemId
    );
    return this.apiService
      .patch(url, itemUpdate)
      .pipe(map((data: any) => ItemAdaptor.fromJson(data)));
  }

  validateItem(
    productGroupCode: string,
    availableWeight: number,
    measuredWeight: number,
    measuredQuantity: number,
    availableQuantity: number
  ): Observable<boolean> {
    const url = getStockReceiveValidateItemEndpointUrl(
      productGroupCode,
      availableWeight,
      measuredWeight,
      measuredQuantity,
      availableQuantity,
      'WEIGHT_TOLERANCE'
    );
    return this.apiService.get(url.path, url.params);
  }

  updateAllItems(
    storeType: string,
    type: string,
    id: number,
    request: any
  ): Observable<any> {
    const url = getStockReceiveUpdateAllItemsEndpointUrl(storeType, type, id);
    return this.apiService.patch(url, request);
  }

  confirmStn(
    storeType: string,
    type: string,
    id: number,
    confirmReceive: ConfirmReceive
  ): Observable<any> {
    const url = getStockReceiveConfirmSTNEndpointUrl(storeType, type, id);
    return this.apiService.patch(url, confirmReceive).pipe(
      map((data: any) => {
        if (storeType === 'L1' || storeType === 'L2') {
          return StockAdaptor.fromJson(data, StockTypeField.STN);
        } else if (storeType === 'L3') {
          return StockAdaptor.fromJson(data, StockTypeField.INVOICE);
        }
      })
    );
  }
}
