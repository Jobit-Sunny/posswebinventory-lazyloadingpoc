import {
  OverlayNotificationService,
  OverlayNotificationEventRef,
  OverlayNotificationType,
  CardListComponent
} from '@poss-web/shared';
import {
  StockReceiveTypesEnum,
  StockReceiveAPITypesEnum,
  StockReceiveTabEnum,
  StockReceiveCarrierTypesEnum
} from '../../models/stock-receive.enum';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  AfterViewInit
} from '@angular/core';
import { Observable, Subject, fromEvent } from 'rxjs';
import { Stock } from '../../models/stock-receive.model';
import { debounceTime, takeUntil, filter } from 'rxjs/operators';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { StockReceiveFacade } from '../../+state/stock-receive.facade';
import { FormControl } from '@angular/forms';
import { DashboardFacade } from '../../../../dashboard/+state/dashboard.facade';

@Component({
  selector: 'poss-web-stock-receive-list',
  templateUrl: './stock-receive-list.component.html',
  styleUrls: ['./stock-receive-list.component.scss']
})
export class StockReceiveListComponent
  implements OnInit, OnDestroy, AfterViewInit {
  storeType: string;

  isLoadingCount$: Observable<boolean>;
  pendingFactorySTNCount$: Observable<number>;
  pendingBoutiqueSTNCount$: Observable<number>;
  pendingMerchandiseSTNcount$: Observable<number>;
  pendingFactorySTN$: Observable<Stock[]>;
  pendingBoutiqueSTN$: Observable<Stock[]>;
  searchStockResults$: Observable<Stock[]>;
  isLoadingPendingFactorySTN: boolean;
  isLoadingPendingBoutiqueSTN: boolean;
  isSearchingStocks$: Observable<boolean>;
  hasSearchStockResults$: Observable<boolean>;
  isPendingFactorySTNLoadedOnce = false;
  isPendingBoutiqueSTNLoadedOnce = false;

  pendingCFAInvoice$: Observable<Stock[]>;
  pendingCFAInvoiceCount$: Observable<number>;
  searchInvoiceResults$: Observable<Stock[]>;
  isLoadingPendingCFAInvoice: boolean;
  isSearchingInvoices$: Observable<boolean>;
  hasSearchInvoiceResults$: Observable<boolean>;
  isPendingCFAInvoiceLoadedOnce = false;

  error$: Observable<CustomErrors>;
  pageSize = 4;
  initalPageSize = 8;
  showNoResultsFoundNaN = false;
  stockReceiveType: StockReceiveTypesEnum;
  stockReceiveEnumRef = StockReceiveTypesEnum;
  stockReceiveCarrierTypesEnumRef = StockReceiveCarrierTypesEnum;

  @ViewChild(CardListComponent, { static: false })
  cardListComponentRef: CardListComponent;
  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;

  destroy$: Subject<null> = new Subject<null>();

  isL1L2Store: boolean;
  isL3Store: boolean;

  boutiqueTypes: {
    value: StockReceiveTypesEnum;
    count: Observable<number>;
    translateKey: string;
  }[] = [];

  otherBoutiqueFromControl = new FormControl();
  searchFromControl = new FormControl();

  constructor(
    private stockReceiveFacade: StockReceiveFacade,
    private dashboardFacade: DashboardFacade,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private appsettingFacade: AppsettingFacade,
    private overlayNotification: OverlayNotificationService
  ) {
    this.otherBoutiqueFromControl.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(type => {
        if (this.stockReceiveType !== type) {
          this.stockReceiveFacade.clearPendingBoutiqueSTN();
          this.stockReceiveType = type;
          let apiType;
          if (type === StockReceiveTypesEnum.BTQ_BTQ) {
            apiType = StockReceiveAPITypesEnum.BTQ_BTQ;
          } else {
            apiType = StockReceiveAPITypesEnum.MER_BTQ;
          }
          this.stockReceiveFacade.loadPendingBoutiqueSTN({
            type: apiType,
            pageIndex: 0,
            pageSize: this.initalPageSize
          });
          this.clearSearch();
          this.router.navigate(['..', type], {
            relativeTo: this.activatedRoute
          });
        }
      });
  }

  ngOnInit(): void {
    this.dashboardFacade.resetError();
    this.stockReceiveFacade.resetError();
    this.stockReceiveFacade.clearStocks();
    this.appsettingFacade
      .getStoreType()
      .pipe(takeUntil(this.destroy$))
      .subscribe((storeType: string) => {
        if (storeType) {
          this.storeType = storeType;
          this.isL1L2Store = this.storeType === 'L1' || this.storeType === 'L2';
          this.isL3Store = this.storeType === 'L3';
          this.componentInit();
        }
      });
    // TODO : change logic. Route state
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.navigateToTabs();
      });
  }

  ngAfterViewInit(): void {
    /**
     * Event Handler for search Box input with debounce
     */
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe((event: any) => {
        const searchValue = this.searchFromControl.value;
        this.showNoResultsFoundNaN = false;
        if (searchValue !== '' && !isNaN(searchValue)) {
          this.searchStocks(searchValue);
        } else if (searchValue === '' && !isNaN(searchValue)) {
          this.clearSearch();
        } else if (isNaN(searchValue)) {
          this.showNoResultsFoundNaN = true;
        }
      });
  }

  componentInit(): void {
    this.stockReceiveFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((stockReceiveError: CustomErrors) => {
        this.errorHandler(stockReceiveError);
      });

    this.dashboardFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((dashboardError: CustomErrors) => {
        this.errorHandler(dashboardError);
      });

    this.isLoadingCount$ = this.dashboardFacade.getIsLoadingCount();
    this.isSearchingStocks$ = this.stockReceiveFacade.getIsSearchingStocks();
    this.hasSearchStockResults$ = this.stockReceiveFacade.getHasSearchStockResults();
    this.isSearchingInvoices$ = this.stockReceiveFacade.getIsSearchingInvoices();
    this.hasSearchInvoiceResults$ = this.stockReceiveFacade.getHasSearchInvoiceResults();
    this.searchStockResults$ = this.stockReceiveFacade.getSearchStockResults();
    this.searchInvoiceResults$ = this.stockReceiveFacade.getSearchInvoiceResults();

    this.stockReceiveFacade
      .getIsLoadingPendingFactorySTN()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isLoading: boolean) => {
        this.isLoadingPendingFactorySTN = isLoading;
      });
    this.stockReceiveFacade
      .getIsLoadingPendingBoutiqueSTN()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isLoading: boolean) => {
        this.isLoadingPendingBoutiqueSTN = isLoading;
      });

    this.stockReceiveFacade
      .getIsLoadingPendingCFAInvoice()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isLoading: boolean) => {
        this.isLoadingPendingCFAInvoice = isLoading;
      });

    this.getStocks();
    this.navigateToTabs();
  }

  navigateToTabs() {
    const type = this.activatedRoute.snapshot.params['type'];
    if (
      this.isL1L2Store &&
      (!type ||
        !(
          type === StockReceiveTypesEnum.FAC_BTQ ||
          type === StockReceiveTypesEnum.BTQ_BTQ ||
          type === StockReceiveTypesEnum.MER_BTQ
        ))
    ) {
      this.router.navigate(['404']);
    } else if (
      this.isL3Store &&
      (!type || !(type === StockReceiveTypesEnum.CFA_BTQ))
    ) {
      this.router.navigate(['404']);
    } else {
      if (
        type === StockReceiveTypesEnum.BTQ_BTQ ||
        type === StockReceiveTypesEnum.MER_BTQ
      ) {
        this.otherBoutiqueFromControl.setValue(type);
      }
      this.changeStockReceiveType(type);
    }
  }

  /**
   * loads the pendingSTN's from respective source
   */
  getStocks(): void {
    if (this.isL1L2Store) {
      this.dashboardFacade.loadStockTransferNoteCount();
      this.pendingFactorySTNCount$ = this.dashboardFacade.getPendingFactorySTNCount();
      this.pendingBoutiqueSTNCount$ = this.dashboardFacade.getPendingBoutiqueSTNCount();
      this.pendingMerchandiseSTNcount$ = this.dashboardFacade.getPendingMerchandiseSTNcount();
      this.boutiqueTypes = [
        {
          value: StockReceiveTypesEnum.BTQ_BTQ,
          count: this.pendingBoutiqueSTNCount$,
          translateKey: 'pw.stockReceive.ibtBoutiqueTypeOption'
        },
        {
          value: StockReceiveTypesEnum.MER_BTQ,
          count: this.pendingMerchandiseSTNcount$,
          translateKey: 'pw.stockReceive.merchandiseBoutiqueTypeOption'
        }
      ];

      this.pendingFactorySTN$ = this.stockReceiveFacade.getPendingFactorySTN();
      this.pendingFactorySTN$
        .pipe(takeUntil(this.destroy$))
        .subscribe((stocks: Stock[]) => {
          if (
            stocks &&
            stocks.length !== 0 &&
            !this.isPendingFactorySTNLoadedOnce
          ) {
            this.isPendingFactorySTNLoadedOnce = true;
          }
        });
      this.pendingBoutiqueSTN$ = this.stockReceiveFacade.getPendingBoutiqueSTN();
      this.pendingBoutiqueSTN$
        .pipe(takeUntil(this.destroy$))
        .subscribe((stocks: Stock[]) => {
          if (
            stocks &&
            stocks.length !== 0 &&
            !this.isPendingBoutiqueSTNLoadedOnce
          ) {
            this.isPendingBoutiqueSTNLoadedOnce = true;
          }
        });
    } else if (this.isL3Store) {
      this.dashboardFacade.loadReceiveInvoiceCount();
      this.pendingCFAInvoiceCount$ = this.dashboardFacade.getPendingCFASTNCount();
      this.pendingCFAInvoice$ = this.stockReceiveFacade.getPendingCFAInvoice();
      this.pendingCFAInvoice$
        .pipe(takeUntil(this.destroy$))
        .subscribe((invoice: Stock[]) => {
          if (
            invoice &&
            invoice.length !== 0 &&
            !this.isPendingCFAInvoiceLoadedOnce
          ) {
            this.isPendingCFAInvoiceLoadedOnce = true;
          }
        });
    }
  }

  /**
   * Search Stocks based on srcDocNumber and Type of transfer
   * @param srcDocnumber : source doc number
   */
  searchStocks(srcDocnumber: number): void {
    let type: string;
    if (this.stockReceiveType === StockReceiveTypesEnum.FAC_BTQ) {
      type = StockReceiveAPITypesEnum.FAC_BTQ;
    } else if (this.stockReceiveType === StockReceiveTypesEnum.BTQ_BTQ) {
      type = StockReceiveAPITypesEnum.BTQ_BTQ;
    } else if (this.stockReceiveType === StockReceiveTypesEnum.MER_BTQ) {
      type = StockReceiveAPITypesEnum.MER_BTQ;
    } else if (this.stockReceiveType === StockReceiveTypesEnum.CFA_BTQ) {
      type = StockReceiveAPITypesEnum.CFA_BTQ;
    }

    if (this.cardListComponentRef) {
      this.cardListComponentRef.resetFocus();
    }

    if (this.isL1L2Store) {
      this.stockReceiveFacade.searchPendingStocks({
        srcDocnumber: srcDocnumber,
        type: type
      });
    } else if (this.isL3Store) {
      this.stockReceiveFacade.searchPendingInvoices({
        srcDocnumber: srcDocnumber,
        type: type
      });
    }
  }

  /**
   * Switch Between different types of transfer list
   * @param newType : new Stock Receive Type
   */
  changeStockReceiveType(newType: StockReceiveTypesEnum): void {
    this.showNoResultsFoundNaN = false;
    if (this.stockReceiveType !== newType) {
      this.clearSearch();
      this.router.navigate(['..', newType], {
        relativeTo: this.activatedRoute
      });
      this.stockReceiveType = newType;
      if (
        (this.stockReceiveType === StockReceiveTypesEnum.FAC_BTQ &&
          !this.isPendingFactorySTNLoadedOnce) ||
        ((this.stockReceiveType === StockReceiveTypesEnum.BTQ_BTQ ||
          this.stockReceiveType === StockReceiveTypesEnum.MER_BTQ) &&
          !this.isPendingBoutiqueSTNLoadedOnce) ||
        (this.stockReceiveType === StockReceiveTypesEnum.CFA_BTQ &&
          !this.isPendingCFAInvoiceLoadedOnce)
      ) {
        this.loadStocks(0);
      }
    }
  }

  /**
   * Loads the stocks based on the type of the transfer based on the pageIndex passed by poss-web-card-list component
   * @param pageIndex : next pageIndex to be loaded
   */
  loadStocks(pageIndex: number): void {
    if (
      this.isLoadingPendingFactorySTN === false &&
      this.stockReceiveType === StockReceiveTypesEnum.FAC_BTQ
    ) {
      this.stockReceiveFacade.loadPendingFactorySTN({
        type: StockReceiveAPITypesEnum.FAC_BTQ,
        pageIndex: pageIndex,
        pageSize: this.isPendingFactorySTNLoadedOnce
          ? this.pageSize
          : this.initalPageSize
      });
    } else if (
      this.isLoadingPendingBoutiqueSTN === false &&
      this.stockReceiveType === StockReceiveTypesEnum.BTQ_BTQ
    ) {
      this.stockReceiveFacade.loadPendingBoutiqueSTN({
        type: StockReceiveAPITypesEnum.BTQ_BTQ,
        pageIndex: pageIndex,
        pageSize: this.isPendingBoutiqueSTNLoadedOnce
          ? this.pageSize
          : this.initalPageSize
      });
    } else if (
      this.isLoadingPendingBoutiqueSTN === false &&
      this.stockReceiveType === StockReceiveTypesEnum.MER_BTQ
    ) {
      this.stockReceiveFacade.loadPendingBoutiqueSTN({
        type: StockReceiveAPITypesEnum.MER_BTQ,
        pageIndex: pageIndex,
        pageSize: this.isPendingBoutiqueSTNLoadedOnce
          ? this.pageSize
          : this.initalPageSize
      });
    } else if (
      this.isLoadingPendingCFAInvoice === false &&
      this.stockReceiveType === StockReceiveTypesEnum.CFA_BTQ
    ) {
      this.stockReceiveFacade.loadPendingCFAInvoice({
        type: StockReceiveAPITypesEnum.CFA_BTQ,
        pageIndex: pageIndex,
        pageSize: this.isPendingCFAInvoiceLoadedOnce
          ? this.pageSize
          : this.initalPageSize
      });
    }
  }

  /**
   * Function navigates to the seleted Stock's details page triggered by poss-web-card-list component
   * @param stock :Selected stock
   */
  onSelected(stock: any): void {
    this.router.navigate([stock.id, StockReceiveTabEnum.NON_VERIFIED], {
      relativeTo: this.activatedRoute
    });
  }

  /**
   * Clears the search result and search value
   */
  clearSearch(event = null): void {
    if (event) {
      event.stopPropagation();
    }
    this.searchFromControl.reset();
    if (this.cardListComponentRef) {
      this.cardListComponentRef.resetFocus();
    }
    this.stockReceiveFacade.searchClear();
    this.showNoResultsFoundNaN = false;
  }

  errorHandler(error: CustomErrors) {
    if (error) {
      this.overlayNotification
        .show({
          type: OverlayNotificationType.ERROR,
          hasClose: true,
          error: error
        })
        .events.pipe(takeUntil(this.destroy$))
        .subscribe((event: OverlayNotificationEventRef) => {});
    }
  }
  /**
   * NgOnDestroy function
   */
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
