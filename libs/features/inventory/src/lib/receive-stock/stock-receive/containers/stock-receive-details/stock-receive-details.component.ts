import { StockReceiveReasonForDelayPopupComponent } from './../../components/stock-receive-reason-for-delay-popup/stock-receive-reason-for-delay-popup.component';
import { LoadItemsTotalCountPayload } from './../../+state/stock-receive.actions';
import {
  StockItemBinGroupCodeEnum,
  StockReceiveCarrierTypesEnum
} from './../../models/stock-receive.enum';
import { FilterOption, ItemValidate } from './../../models/stock-receive.model';
import {
  StockItemStatusEnum,
  StockReceiveTabEnum,
  StockStatusEnum,
  StockReceiveTypesEnum,
  StockReceiveAPITypesEnum
} from '../../models/stock-receive.enum';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {
  AppsettingFacade,
  ShortcutService,
  Command,
  CustomErrors,
  Lov,
  BinCode
} from '@poss-web/core';
import {
  Stock,
  Item,
  ItemToUpdate,
  ConfirmReceive
} from '../../models/stock-receive.model';
import { Observable, Subject } from 'rxjs';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  TemplateRef
} from '@angular/core';
import { StockReceiveFacade } from '../../+state/stock-receive.facade';
import { takeUntil, filter, take } from 'rxjs/operators';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventType,
  OverlayNotificationEventRef,
  SearchResponse,
  SearchComponent,
  FilterService,
  Filter,
  SortDialogService,
  Column,
  ErrorEnums,
  SelectionDialogService,
  Option
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';

import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  PageEvent,
  MatDialog
} from '@angular/material';

import * as moment from 'moment';
import { LoadItemsPayload } from '../../+state/stock-receive.actions';

const CUSTOM_DATE_FORMATS = {
  parse: {
    dateInput: 'DD-MMM-YYYY'
  },
  display: {
    dateInput: 'DD-MMM-YYYY',
    monthYearLabel: 'MMM-YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM-YYYY'
  }
};

const SEARCH_SHORTCUT_KEY_F2 = 'StockReceiveDetailsComponent.F2';

@Component({
  selector: 'poss-web-stock-receive-details',
  templateUrl: './stock-receive-details.component.html',
  styleUrls: ['./stock-receive-details.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    { provide: MAT_DATE_FORMATS, useValue: CUSTOM_DATE_FORMATS }
  ]
})
export class StockReceiveDetailsComponent implements OnInit, OnDestroy {
  selectedStock: Stock;
  tab: StockReceiveTabEnum;
  stockReceiveTabEnumRef = StockReceiveTabEnum;

  initailPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };
  itemsPageEvent: PageEvent;

  nonVerifiedItemsTotalCount = 0;
  verifiedItemsTotalCount = 0;
  itemsTotalCountLoaded$: Observable<boolean>;
  itemsTotalCountLoading$: Observable<boolean>;
  items$: Observable<Item[]>;
  binCodes: BinCode[] = [];
  remarks$: Observable<Lov[]>;
  tolerance$: Observable<number>;
  type: string;
  stockId: string;
  storeType: string;
  currentDate = moment();
  error$: Observable<CustomErrors>;
  isLoadingSelectedStock$: Observable<boolean>;
  isLoadingBinGroups$: Observable<boolean>;
  isLoadingRemarks$: Observable<boolean>;
  isLoadingTolerance$: Observable<boolean>;
  isItemsLoading$: Observable<boolean>;
  isItemsLoaded$: Observable<boolean>;
  itemsCount$: Observable<number>;
  isAssigningBinToAllItems$: Observable<boolean>;
  hasNotification = false;
  isverifiedCountShownOnce = false;
  itemCode: string;
  lotNumber: string;
  binGroupCode: string;
  isL1L2Store: boolean;
  isL3Store: boolean;
  maxHoursOfDelay = 48;
  @ViewChild(SearchComponent, { static: true })
  searchRef: SearchComponent;

  stockForm: FormGroup;

  maxFilterLimit: number;
  maxSortLimit: number;
  filter: { key: string; value: any[] }[] = [];
  filterData: { [key: string]: Filter[] } = {};
  nonVerifiedFilterData: { [key: string]: Filter[] } = {};
  nonVerifiedFilter: { key: string; value: any[] }[] = [];
  verifiedFilterData: { [key: string]: Filter[] } = {};
  verifiedFilter: { key: string; value: any[] }[] = [];

  sortData: Column[] = [];
  sortBy: string;
  sortOrder: string;

  nonVerifiedSortData: Column[] = [];
  nonVerifiedSortBy: string;
  nonVerifiedSortOrder: string;
  verifiedSortData: Column[] = [];
  verifiedSortBy: string;
  verifiedSortOrder: string;

  nonVerifieditemCode: string;
  nonVerifiedlotNumber: string;
  verifieditemCode: string;
  verifiedlotNumber: string;
  isParentFormDirty = false;
  stockReceiveCarrierTypesEnumRef = StockReceiveCarrierTypesEnum;
  isShowingErrorNotifcation = false;
  destroy$: Subject<null> = new Subject<null>();
  @ViewChild('confirmSuccessNotificationTemplate', { static: true })
  confirmSuccessNotificationTemplate: TemplateRef<any>;

  destDocNo = 0;

  PRODUCT_CATEGORIES: { [key: string]: Filter[] } = {};
  PRODUCT_GROUP: { [key: string]: Filter[] } = {};
  productCategoryFilterLable: string;
  productGroupFilterLable: string;

  binsForSelection: Option[] = [];
  selectedBinCode: string;
  searchBinCodeLable: string;
  selectBinCodeLable: string;
  isItemLoadedOnce = false;
  stockReceiveEnumRef = StockReceiveTypesEnum;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private stockReceiveFacade: StockReceiveFacade,
    private appsettingFacade: AppsettingFacade,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService,
    private shortcutService: ShortcutService,
    private filterService: FilterService,
    private sortService: SortDialogService,
    private selectionDialog: SelectionDialogService,
    private dialog: MatDialog
  ) {
    this.stockForm = new FormGroup({
      courierReceivedDate: new FormControl(
        this.currentDate,
        Validators.required
      )
    });
  }

  ngOnInit() {
    this.translate
      .get([
        'pw.stockReceive.productCategoryFilterLable',
        'pw.stockReceive.productGroupFilterLable',
        'pw.stockReceive.itemWeightSortLable',
        'pw.stockReceive.itemQuantityLable',
        'pw.stockReceive.searchBinCodeLable',
        'pw.stockReceive.selectBinCodeLable'
      ])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: any) => {
        this.searchBinCodeLable =
          translatedMessages['pw.stockReceive.searchBinCodeLable'];
        this.selectBinCodeLable =
          translatedMessages['pw.stockReceive.selectBinCodeLable'];

        this.productCategoryFilterLable =
          translatedMessages['pw.stockReceive.productCategoryFilterLable'];
        this.productGroupFilterLable =
          translatedMessages['pw.stockReceive.productGroupFilterLable'];
        this.sortService.DataSource = [
          {
            id: 0,
            sortByColumnName:
              translatedMessages['pw.stockReceive.itemWeightSortLable'],
            sortAscOrder: false
          },
          {
            id: 1,
            sortByColumnName:
              translatedMessages['pw.stockReceive.itemQuantityLable'],
            sortAscOrder: false
          }
        ];
      });

    this.stockReceiveFacade.resetError();

    // TODO : change logic. Route state
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        const tab = this.activatedRoute.snapshot.params['tab'];
        this.changeTab(tab);
        this.showNotifications();
      });

    this.stockId = this.activatedRoute.snapshot.params['id'];
    this.type = this.activatedRoute.snapshot.params['type'];
    this.tab = this.activatedRoute.snapshot.params['tab'];

    if (
      !this.tab ||
      !(
        this.tab === StockReceiveTabEnum.NON_VERIFIED ||
        this.tab === StockReceiveTabEnum.VERIFIED
      )
    ) {
      this.router.navigate(['404']);
    }

    this.stockReceiveFacade.loadProductGroups();
    this.stockReceiveFacade.loadProductCategories();

    this.appsettingFacade
      .getMaxFilterLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxFilterLimit = data;
      });

    this.appsettingFacade
      .getMaxSortLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxSortLimit = data;
      });

    this.appsettingFacade
      .getStoreType()
      .pipe(takeUntil(this.destroy$))
      .subscribe((storeType: string) => {
        if (storeType) {
          this.storeType = storeType;
          this.isL1L2Store = this.storeType === 'L1' || this.storeType === 'L2';
          this.isL3Store = this.storeType === 'L3';
          this.binGroupCode = this.isL1L2Store
            ? StockItemBinGroupCodeEnum.STN
            : StockItemBinGroupCodeEnum.PURCFA;

          this.stockReceiveFacade
            .getProductGroups()
            .pipe(takeUntil(this.destroy$))
            .subscribe(productGroups => {
              this.PRODUCT_GROUP = this.mapToFilterOptions(
                this.productGroupFilterLable,
                productGroups.map(productGroup => ({
                  id: productGroup.productGroupCode,
                  description: productGroup.description
                }))
              );
            });

          this.stockReceiveFacade
            .getProductCategories()
            .pipe(takeUntil(this.destroy$))
            .subscribe(productCategories => {
              this.PRODUCT_CATEGORIES = this.mapToFilterOptions(
                this.productCategoryFilterLable,
                productCategories.map(productCategory => ({
                  id: productCategory.productCategoryCode,
                  description: productCategory.description
                }))
              );
            });

          this.appsettingFacade
            .getPageSize()
            .pipe(takeUntil(this.destroy$))
            .subscribe(data => {
              const pageSize = JSON.parse(data);
              this.initailPageEvent.pageSize = pageSize;
              this.itemsPageEvent = this.initailPageEvent;
              this.componentInit();
            });
        }
      });

    this.shortcutService.commands
      .pipe(takeUntil(this.destroy$))
      .subscribe(command => this.shortcutEventHandler(command));
  }

  mapToFilterOptions(
    key: string,
    options: FilterOption[]
  ): { [key: string]: Filter[] } {
    return {
      [key]: options.map(option => ({
        id: option.id,
        description: option.description,
        selected: false
      }))
    };
  }

  openBinSelectionPopup(event) {
    if (event) {
      event.stopPropagation();
    }

    this.selectionDialog
      .open({
        title: this.selectBinCodeLable,
        placeholder: this.searchBinCodeLable,
        options: this.binsForSelection
      })
      .pipe(takeUntil(this.destroy$))
      .subscribe((selectedOption: Option) => {
        if (selectedOption) {
          this.selectedBinCode = selectedOption.id;
          this.showProgressNotification();
          this.stockReceiveFacade.assignBinToAllItems({
            storeType: this.storeType,
            type: this.selectedStock.type,
            id: this.selectedStock.id,
            data: {
              binCode: selectedOption.id
            }
          });
        }
      });
  }

  /**
   * method to handle shortcut commands
   * @param command: shortcut command
   */
  shortcutEventHandler(command: Command) {
    if (command.name === SEARCH_SHORTCUT_KEY_F2) {
      if (this.searchRef) {
        this.searchRef.focus();
      }
    }
  }

  componentInit() {
    this.isLoadingSelectedStock$ = this.stockReceiveFacade.getIsLoadingSelectedStock();
    this.isLoadingBinGroups$ = this.stockReceiveFacade.getIsLoadingBinGroups();
    this.isLoadingRemarks$ = this.stockReceiveFacade.getIsLoadingRemarks();
    this.isLoadingTolerance$ = this.stockReceiveFacade.getIsLoadingTolerance();
    this.items$ = this.stockReceiveFacade.getItems();
    this.isItemsLoading$ = this.stockReceiveFacade.getIsItemsLoading();
    this.isItemsLoaded$ = this.stockReceiveFacade.getIsItemsLoaded();
    this.itemsCount$ = this.stockReceiveFacade.getItemsCount();
    this.itemsTotalCountLoaded$ = this.stockReceiveFacade.getItemsTotalCountLoaded();
    this.itemsTotalCountLoading$ = this.stockReceiveFacade.getIsItemsTotalCountLoading();
    this.remarks$ = this.stockReceiveFacade.getRemarks();
    this.tolerance$ = this.stockReceiveFacade.getTolerance();
    this.stockReceiveFacade.loadBinCodes(this.binGroupCode);
    this.stockReceiveFacade.loadRemarks();
    this.stockReceiveFacade.loadTolerance();

    this.isItemsLoaded$.pipe(takeUntil(this.destroy$)).subscribe(isLoaded => {
      if (!this.isItemLoadedOnce && isLoaded) {
        this.isItemLoadedOnce = true;
        this.showNotifications();
      }
    });

    this.stockReceiveFacade
      .getBinCodes()
      .pipe(takeUntil(this.destroy$))
      .subscribe((bincodes: BinCode[]) => {
        this.binCodes = bincodes;
        this.binsForSelection = bincodes.map(bincode => ({
          id: bincode.binCode,
          description: bincode.description
        }));
      });

    this.items$.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.showNotifications();
    });

    this.stockReceiveFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    if (this.isL1L2Store) {
      let stockType: string;
      if (this.type === StockReceiveTypesEnum.FAC_BTQ) {
        stockType = StockReceiveAPITypesEnum.FAC_BTQ;
      } else if (this.type === StockReceiveTypesEnum.BTQ_BTQ) {
        stockType = StockReceiveAPITypesEnum.BTQ_BTQ;
      } else if (this.type === StockReceiveTypesEnum.MER_BTQ) {
        stockType = StockReceiveAPITypesEnum.MER_BTQ;
      }
      if (stockType) {
        this.stockReceiveFacade.loadSelectedStock({
          id: this.stockId,
          type: stockType
        });
        this.stockReceiveFacade
          .getSelectedStock()
          .pipe(takeUntil(this.destroy$))
          .subscribe((stockTransferNote: Stock) => {
            if (stockTransferNote) {
              if (
                stockTransferNote.status.toLowerCase() !==
                StockStatusEnum.ISSUED.toLowerCase()
              ) {
                this.router.navigate(['404']);
              } else {
                this.selectedStock = stockTransferNote;
                this.initialLoad();
              }
            }
          });
      } else {
        this.router.navigate(['404']);
      }
    } else if (this.isL3Store) {
      let stockType: string;
      if (this.type === StockReceiveTypesEnum.CFA_BTQ) {
        stockType = StockReceiveAPITypesEnum.CFA_BTQ;
      }
      if (stockType) {
        this.stockReceiveFacade.loadSelectedInvocie({
          id: this.stockId,
          type: stockType
        });
        this.stockReceiveFacade
          .getSelectedInvoice()
          .pipe(takeUntil(this.destroy$))
          .subscribe((invoice: Stock) => {
            if (invoice) {
              if (invoice.status !== StockItemStatusEnum.ISSUED) {
                this.router.navigate(['404']);
              } else {
                this.selectedStock = invoice;
                this.initialLoad();
              }
            }
          });
      } else {
        this.router.navigate(['404']);
      }
    }

    this.stockReceiveFacade
      .getIsVerifyingAllItemSuccess()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isSuccess: boolean) => {
        if (isSuccess) {
          this.stockReceiveFacade.loadItemsTotalCount(
            this.createLoadCountPayLoad()
          );
          this.changeTab(StockReceiveTabEnum.VERIFIED);
        }
      });

    this.isAssigningBinToAllItems$ = this.stockReceiveFacade.getIsAssigningBinToAllItems();

    this.stockReceiveFacade
      .getIsAssigningBinToAllItemsSuccess()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isSuccess: boolean) => {
        if (isSuccess) {
          this.loadItems();
          this.selectedBinCode = null;
        } else if (isSuccess === false) {
          this.selectedBinCode = null;
        }
      });

    this.stockReceiveFacade
      .getConfirmedStock()
      .pipe(takeUntil(this.destroy$))
      .subscribe((confirmedStock: any) => {
        if (confirmedStock) {
          this.destDocNo = confirmedStock.destDocNo;
          this.showConfirmReceiveSuccessNotification(confirmedStock.destDocNo);
        }
      });

    this.stockReceiveFacade
      .getTotalCounts()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.nonVerifiedItemsTotalCount = data.nonVerifiedItemsTotalCount;
        this.verifiedItemsTotalCount = data.verifiedItemsTotalCount;
        if (data.isLoaded && data.nonVerifiedItemsTotalCount === 0) {
          this.changeTab(StockReceiveTabEnum.VERIFIED);
        }
        this.showNotifications();
      });
  }

  initialLoad() {
    this.stockReceiveFacade.loadItemsTotalCount(this.createLoadCountPayLoad());
    this.loadItems();
  }

  createLoadCountPayLoad(): LoadItemsTotalCountPayload {
    return {
      storeType: this.storeType,
      id: this.selectedStock.id,
      type: this.selectedStock.type
    };
  }

  changeTab(newTab: StockReceiveTabEnum) {
    if (this.tab !== newTab) {
      this.tab = newTab;

      if (this.tab === StockReceiveTabEnum.NON_VERIFIED) {
        this.nonVerifiedFilterData = this.filterData;
        this.nonVerifiedFilter = this.filter;
        this.filterData = this.verifiedFilterData;
        this.filter = this.verifiedFilter;

        this.nonVerifiedSortData = this.sortData;
        this.nonVerifiedSortBy = this.sortBy;
        this.nonVerifiedSortOrder = this.sortOrder;
        this.sortData = this.verifiedSortData;
        this.sortBy = this.verifiedSortBy;
        this.sortOrder = this.verifiedSortOrder;

        this.nonVerifieditemCode = this.itemCode;
        this.nonVerifiedlotNumber = this.lotNumber;
      } else if (this.tab === StockReceiveTabEnum.VERIFIED) {
        this.verifiedFilterData = this.filterData;
        this.verifiedFilter = this.filter;
        this.filterData = this.nonVerifiedFilterData;
        this.filter = this.nonVerifiedFilter;

        this.verifiedSortData = this.sortData;
        this.verifiedSortBy = this.sortBy;
        this.verifiedSortOrder = this.sortOrder;
        this.sortData = this.nonVerifiedSortData;
        this.sortBy = this.nonVerifiedSortBy;
        this.sortOrder = this.nonVerifiedSortOrder;

        this.verifieditemCode = this.itemCode;
        this.verifiedlotNumber = this.lotNumber;
      }

      this.stockReceiveFacade.clearItems();
      this.itemCode = null;
      this.lotNumber = null;

      if (this.searchRef) {
        this.searchRef.reset();
      }

      this.hasNotification = false;

      this.router.navigate(['..', this.tab], {
        relativeTo: this.activatedRoute
      });
      this.itemsPageEvent = this.initailPageEvent;
      this.loadItems();
    }
  }

  /**
   * Search searchItems based on varient code
   */
  searchItems(searchData: SearchResponse) {
    this.itemCode = searchData.searchValue;
    this.lotNumber = searchData.lotNumber;
    this.itemsPageEvent = this.initailPageEvent;
    this.loadItems();
  }

  /**
   * clears the search result
   */
  clearSearchItems() {
    this.itemCode = null;
    this.lotNumber = null;
    if (this.searchRef) {
      this.searchRef.reset();
    }
    this.itemsPageEvent = this.initailPageEvent;
    this.loadItems();
  }

  openFilter() {
    this.filterService.DataSource = {
      ...this.PRODUCT_CATEGORIES,
      ...this.PRODUCT_GROUP
    };
    this.filterService
      .openDialog(this.maxFilterLimit, this.filterData)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (filterResult: {
          data: { [key: string]: Filter[] };
          actionfrom: string;
        }) => {
          if (filterResult.actionfrom === 'apply') {
            const filterData = filterResult.data;
            if (filterData == null) {
              this.filterData = {};
            } else {
              this.filterData = filterData;
            }
            this.filter = [];
            if (filterData) {
              let filterValues = [];
              if (filterData[this.productGroupFilterLable]) {
                filterData[this.productGroupFilterLable].forEach(value => {
                  filterValues.push(value.id);
                });
                if (filterValues.length > 0) {
                  this.filter.push({
                    key: 'productGroup',
                    value: filterValues
                  });
                }
              }
              filterValues = [];
              if (filterData[this.productCategoryFilterLable]) {
                filterData[this.productCategoryFilterLable].forEach(value => {
                  filterValues.push(value.id);
                });
                if (filterValues.length > 0) {
                  this.filter.push({
                    key: 'productCategory',
                    value: filterValues
                  });
                }
              }
            }

            this.itemsPageEvent = this.initailPageEvent;
            this.loadItems();
          }
        }
      );
  }

  openSortDailog() {
    this.sortService
      .openDialog(this.maxSortLimit, this.sortData)
      .pipe(takeUntil(this.destroy$))
      .subscribe((sortResult: { data: Column[]; actionfrom: string }) => {
        if (sortResult.actionfrom === 'apply') {
          const sortData = sortResult.data;
          if (sortData == null || sortData.length === 0) {
            this.sortData = [];
            this.sortOrder = null;
            this.sortBy = null;
          } else {
            this.sortData = sortData;
            if (sortData.length > 0) {
              if (sortData[0].id === 0) {
                this.sortBy = 'issuedWeight';
              } else if (sortData[0].id === 1) {
                this.sortBy = 'issuedQuantity';
              }
              this.sortOrder = sortData[0].sortAscOrder ? 'asc' : 'DESC';
            }
          }
          this.itemsPageEvent = this.initailPageEvent;
          this.loadItems();
        }
      });
  }

  checkForDelay(): { hasDelay: boolean; delay: number } {
    const to = moment();
    const from =
      this.selectedStock && this.selectedStock.srcDocDate
        ? this.selectedStock.srcDocDate
        : null;
    const dateDiff = moment.duration(to.diff(from)).asHours();
    return { hasDelay: dateDiff > this.maxHoursOfDelay, delay: dateDiff };
  }

  openReasonForDelayPopup(remarks: string) {
    const dialogRef = this.dialog.open(
      StockReceiveReasonForDelayPopupComponent,
      {
        autoFocus: false,
        width: '400px',
        data: this.selectedStock.srcDocDate
      }
    );
    dialogRef
      .afterClosed()
      .pipe(take(1))
      .subscribe(response => {
        if (response && response.type === 'confirm') {
          this.confirmReceive(remarks, response.data);
        } else {
          this.showNotifications();
        }
      });
  }

  confirmReceive(remarks: string, reason?: string) {
    this.showProgressNotification();
    let confirmReceiveData: ConfirmReceive;
    if (this.isL1L2Store) {
      confirmReceiveData = {
        courierReceivedDate: (this.stockForm.value[
          'courierReceivedDate'
        ] as moment.Moment).format(),
        reasonForDelay: reason ? reason : '',
        remarks
      };
    } else if (this.isL3Store) {
      confirmReceiveData = {
        receivedDate: (this.stockForm.value[
          'courierReceivedDate'
        ] as moment.Moment).format(),
        remarks
      };
    }
    this.stockReceiveFacade.confirmStock({
      storeType: this.storeType,
      type: this.selectedStock.type,
      id: this.selectedStock.id,
      confirmReceive: confirmReceiveData
    });
  }

  loadItems() {
    if (this.storeType && this.selectedStock)
      this.stockReceiveFacade.loadItems(this.creatItemLoadPayload());
  }

  creatItemLoadPayload(): LoadItemsPayload {
    return {
      storeType: this.storeType,
      type: this.selectedStock.type,
      id: this.selectedStock.id,
      status:
        this.tab === StockReceiveTabEnum.NON_VERIFIED
          ? StockItemStatusEnum.ISSUED
          : StockItemStatusEnum.VERIFIED,
      itemCode: this.itemCode,
      lotNumber: this.lotNumber,
      pageIndex: this.itemsPageEvent.pageIndex,
      pageSize: this.itemsPageEvent.pageSize,
      sortBy: this.sortBy,
      sortOrder: this.sortOrder,
      filter: this.filter
    };
  }

  verifyItem(itemToUpdate: ItemToUpdate) {
    this.stockReceiveFacade.verifyItem({
      storeType: this.storeType,
      type: this.selectedStock.type,
      id: this.selectedStock.id,
      itemId: itemToUpdate.id,
      newUpdate: itemToUpdate.newUpdate,
      actualDetails: itemToUpdate.actualDetails,
      loadItemsPayload: this.creatItemLoadPayload(),
      loadTemsCountPayload: this.createLoadCountPayLoad()
    });
  }

  updateItem(itemToUpdate: ItemToUpdate) {
    this.stockReceiveFacade.updateItem({
      storeType: this.storeType,
      type: this.selectedStock.type,
      id: this.selectedStock.id,
      itemId: itemToUpdate.id,
      newUpdate: itemToUpdate.newUpdate,
      actualDetails: itemToUpdate.actualDetails
    });
  }

  validateItem(data: ItemValidate) {
    this.stockReceiveFacade.validateItem(data);
  }
  paginate(event: PageEvent) {
    this.itemsPageEvent = event;
    this.initailPageEvent.pageSize = event.pageSize;
    this.loadItems();
  }

  onParentFormDirty(isDirty) {
    this.isParentFormDirty = isDirty;
    this.showNotifications();
  }

  showNotifications() {
    if (!this.isShowingErrorNotifcation) {
      this.overlayNotification.close();

      if (
        this.isItemLoadedOnce &&
        (this.itemCode === null || this.itemCode === undefined) &&
        this.filter.length === 0
      ) {
        if (
          !this.isParentFormDirty &&
          this.nonVerifiedItemsTotalCount !== 0 &&
          this.tab === StockReceiveTabEnum.NON_VERIFIED
        ) {
          this.showVerifyAllNotification(this.nonVerifiedItemsTotalCount);
        }
        if (
          this.verifiedItemsTotalCount !== 0 &&
          this.tab === StockReceiveTabEnum.VERIFIED
        ) {
          if (this.nonVerifiedItemsTotalCount === 0) {
            this.showConfirmReceiveNotificationTranslator();
          } else if (!this.isverifiedCountShownOnce) {
            this.showVerifiedItemCountNotification(
              this.verifiedItemsTotalCount
            );
          }
        }
      }
    }
  }

  showVerifyAllNotification(count: number = 0) {
    const key =
      'pw.stockReceiveNotificationMessages.nonVerifiedProductsCountMessage';
    const buttonTextKey =
      'pw.stockReceiveNotificationMessages.verifyAllButtonText';
    this.translate
      .get([key, buttonTextKey])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            message: count + ' ' + translatedMessages[key],
            buttonText: translatedMessages[buttonTextKey]
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.showProgressNotification();
              this.stockReceiveFacade.verifyAllItems({
                storeType: this.storeType,
                type: this.selectedStock.type,
                id: this.selectedStock.id,
                data: {
                  status: StockItemStatusEnum.VERIFIED
                }
              });
            }
          });
      });
  }

  showConfirmReceiveNotificationTranslator(
    key: string = 'pw.stockReceiveNotificationMessages.confirmReceiveMessage'
  ) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.showConfirmReceiveNotification(translatedMessage);
      });
  }

  showConfirmReceiveNotification(message: string) {
    const key = 'pw.stockReceiveNotificationMessages.confirmReceiveButtonText';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            message: message,
            buttonText: translatedMessage,
            hasRemarks: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              if (this.stockForm.get('courierReceivedDate').valid) {
                if (!(this.isL1L2Store && this.checkForDelay().hasDelay)) {
                  this.confirmReceive(event.data);
                } else {
                  this.openReasonForDelayPopup(event.data);
                }
              } else {
                this.stockForm.get('courierReceivedDate').markAsTouched();
                this.showConfirmReceiveNotificationTranslator(
                  'pw.stockReceiveNotificationMessages.invalidReceivingDateMessage'
                );
              }
            }
          });
      });
  }

  showVerifiedItemCountNotification(count: number) {
    const key =
      'pw.stockReceiveNotificationMessages.verifiedProductsCountMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.SIMPLE,
            message: count + ' ' + translatedMessage,
            hasClose: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
            if (event.eventType === OverlayNotificationEventType.CLOSE) {
              this.isverifiedCountShownOnce = true;
            }
          });
      });
  }

  showAssignBinToAllSuccessNotification(binCode: string) {
    const key = 'pw.stockReceiveNotificationMessages.assignBinAllSuccessMessge';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.showConfirmReceiveNotification(
          translatedMessage.replace('{0}', binCode)
        );
      });
  }

  showProgressNotification() {
    const key = 'pw.stockReceiveNotificationMessages.progressMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.PROGRESS,
            message: translatedMsg,
            hasBackdrop: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe(() => {
            this.hasNotification = false;
          });
      });
  }

  showConfirmReceiveSuccessNotification(documentNumber: number) {
    const key =
      'pw.stockReceiveNotificationMessages.confirmReceiveSuccessMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.CUSTOM,
            message: translatedMsg + ' ' + documentNumber,
            hasBackdrop: true,
            hasClose: true,
            template: this.confirmSuccessNotificationTemplate
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
            if (event.eventType === OverlayNotificationEventType.CLOSE) {
              this.router.navigate(['inventory/stockreceive/' + this.type]);
            }
          });
      });
  }

  errorHandler(error: CustomErrors) {
    this.hasNotification = true;
    this.isShowingErrorNotifcation = true;
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error,
        hasBackdrop:
          error.code === ErrorEnums.ERR_INV_013 ||
          error.code === ErrorEnums.ERR_INV_029
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        this.hasNotification = false;
        this.isShowingErrorNotifcation = false;
        if (event.eventType === OverlayNotificationEventType.CLOSE) {
          if (
            error.code === ErrorEnums.ERR_INV_013 ||
            error.code === ErrorEnums.ERR_INV_029
          ) {
            this.router.navigate(['inventory/stockreceive/' + this.type]);
          } else {
            this.showNotifications();
          }
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
