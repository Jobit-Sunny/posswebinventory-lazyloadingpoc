import { takeUntil, debounceTime, delay } from 'rxjs/operators';
import { Subject, of } from 'rxjs';
import {
  Item,
  ItemToUpdate,
  ItemValidate
} from '../../models/stock-receive.model';
import {
  FormGroup,
  FormControl,
  Validators,
  ValidatorFn,
  AbstractControl,
  FormArray
} from '@angular/forms';
import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output,
  OnDestroy,
  SimpleChanges,
  OnChanges
} from '@angular/core';
import {
  StockItemBinGroupCodeEnum,
  StockItemBinCodeEnum
} from '../../models/stock-receive.enum';
import { Lov, BinCode } from '@poss-web/core';
import { SelectionDialogService, Option } from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'poss-web-stock-receive-item',
  templateUrl: './stock-receive-item.component.html',
  styleUrls: ['./stock-receive-item.component.scss']
})
export class StockReceiveItemComponent implements OnInit, OnDestroy, OnChanges {
  @Input() item: Item;
  @Input() isVerified: boolean; // verified/nonVerified
  @Input() binGroupCode: string;
  @Input() binCodes: BinCode[];
  @Input() remarks: Lov[];
  @Input() tolerance = 0;

  @Input() parentForm: FormArray;

  @Output() verify = new EventEmitter<ItemToUpdate>();
  @Output() update = new EventEmitter<ItemToUpdate>();
  @Output() validate = new EventEmitter<ItemValidate>();
  @Output() itemDelete = new EventEmitter();

  binGroupCodes: string[] = [];
  showUpdateStatus = true;
  stockItemBinGroupCodeEnumRef = StockItemBinGroupCodeEnum;
  itemForm: FormGroup;
  destroy$ = new Subject<null>();

  binsForSelection: Option[] = [];
  selectedBinCode: string;
  searchBinCodeLable: string;
  selectBinCodeLable: string;
  isWeightMismatch = false;
  prevMeasuredWeight: number;
  prevBinGroupCode: string;

  constructor(
    private selectionDialog: SelectionDialogService,
    private translate: TranslateService
  ) {
    this.translate
      .get([
        'pw.stockReceive.searchBinCodeLable',
        'pw.stockReceive.selectBinCodeLable'
      ])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: any) => {
        this.searchBinCodeLable =
          translatedMessages['pw.stockReceive.searchBinCodeLable'];
        this.selectBinCodeLable =
          translatedMessages['pw.stockReceive.selectBinCodeLable'];
      });
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (
      this.isVerified &&
      changes &&
      changes['item'] &&
      changes['item']['currentValue'] &&
      (changes['item']['currentValue']['isUpdatingSuccess'] === true ||
        changes['item']['currentValue']['isUpdatingSuccess'] === false)
    ) {
      of(true)
        .pipe(delay(2000))
        .pipe(takeUntil(this.destroy$))
        .subscribe(() => (this.showUpdateStatus = false));

      if (changes['item']['currentValue']['isUpdatingSuccess'] === false) {
        this.itemForm = this.createForm(this.item);
      }
    }
    if (
      changes &&
      changes['item'] &&
      changes['item']['currentValue'] &&
      (changes['item']['currentValue']['isValidatingSuccess'] === true ||
        changes['item']['currentValue']['isValidatingSuccess'] === false)
    ) {
      this.isWeightMismatch = !changes['item']['currentValue'][
        'isValidatingSuccess'
      ];
      this.prevMeasuredWeight = this.itemForm.get('measuredWeight').value;
      if (!this.isWeightMismatch) {
        if (
          this.itemForm.get('binGroupCode').value ===
          StockItemBinGroupCodeEnum.DISPUTE
        ) {
          this.prevBinGroupCode = this.binGroupCode;
          this.itemForm.patchValue({
            binGroupCode: this.binGroupCode,
            remarkGroupCode: null
          });
        }
      } else {
        this.prevBinGroupCode = StockItemBinGroupCodeEnum.DISPUTE;
        this.itemForm.patchValue({
          binGroupCode: StockItemBinGroupCodeEnum.DISPUTE,
          remarkGroupCode: null
        });
      }
      // TODO - Temp fix for the expression changed error
      setTimeout(() => {
        this.updateItem();
      }, 100);
    }
    if (
      changes &&
      changes['item'] &&
      changes['item']['currentValue'] &&
      changes['item']['currentValue']['isValidatingError']
    ) {
      this.itemForm.patchValue({
        measuredWeight: this.prevMeasuredWeight
      });
    }
  }

  ngOnInit() {
    if (this.item.binGroupCode === StockItemBinGroupCodeEnum.DISPUTE) {
      this.isWeightMismatch = true;
    }
    this.binsForSelection = this.binCodes.map(bincode => ({
      id: bincode.binCode,
      description: bincode.description
    }));
    this.binGroupCodes = [
      this.binGroupCode,
      StockItemBinGroupCodeEnum.DISPUTE,
      StockItemBinGroupCodeEnum.DEFECTIVE
    ];
    this.itemForm = this.createForm(this.item);
    this.prevMeasuredWeight = this.itemForm.get('measuredWeight').value;
    this.prevBinGroupCode = this.itemForm.get('binGroupCode').value;

    this.itemForm
      .get('binGroupCode')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (this.prevBinGroupCode !== value) {
          this.prevBinGroupCode = value;
          if (value === StockItemBinGroupCodeEnum.DEFECTIVE) {
            this.itemForm.patchValue({
              remarkGroupCode: this.remarks[0].code
            });
          }
          if (value === this.binGroupCode) {
            this.selectedBinCode = StockItemBinCodeEnum.ZERO_BIN;
          }
          this.itemForm.updateValueAndValidity();
          this.updateItem();
        }
      });

    if (!this.isVerified) {
      this.parentForm.push(this.itemForm);
      this.itemForm.valueChanges
        .pipe(takeUntil(this.destroy$))
        .subscribe(formValue => {
          if (
            formValue['binGroupCode'] === this.item.binGroupCode &&
            formValue['measuredWeight'] === this.item.measuredWeight
          ) {
            this.itemForm.markAsPristine();
          }
        });
    }
  }

  measuredWeightChange() {
    const value = this.itemForm.get('measuredWeight').value;
    if (this.prevMeasuredWeight !== value) {
      const newMessuredWeight =
        value && !isNaN(value) ? +Number.parseFloat(value).toFixed(3) : 0;
      this.itemForm.patchValue({
        measuredWeight: newMessuredWeight
      });
      this.validate.emit({
        itemId: this.item.id,
        productGroupCode: this.item.productGroup,
        availableWeight: this.item.availableWeight,
        measuredWeight: value,
        measuredQuantity: this.item.measuredQuantity,
        availableQuantity: this.item.availableQuantity
      });
    }
  }

  openBinSelectionPopup(event) {
    if (event) {
      event.stopPropagation();
    }
    this.selectionDialog
      .open({
        title: this.selectBinCodeLable,
        placeholder: this.searchBinCodeLable,
        options: this.binsForSelection
      })
      .pipe(takeUntil(this.destroy$))
      .subscribe((selectedOption: Option) => {
        if (selectedOption) {
          if (this.selectedBinCode !== selectedOption.id) {
            this.selectedBinCode = selectedOption.id;
            this.updateItem();
          }
        }
      });
  }

  createForm(item: Item): FormGroup {
    this.selectedBinCode =
      item.binCode &&
      item.binCode !== '' &&
      item.binGroupCode !== StockItemBinGroupCodeEnum.DISPUTE &&
      item.binGroupCode !== StockItemBinGroupCodeEnum.DEFECTIVE
        ? item.binCode
        : null;
    return new FormGroup(
      {
        id: new FormControl(item.id),
        measuredQuantity: new FormControl(item.measuredQuantity, [
          Validators.required,
          Validators.min(item.availableQuantity),
          Validators.max(item.availableQuantity)
        ]),
        measuredWeight: new FormControl(item.measuredWeight, [
          Validators.required,
          this.measuredWeightValidator()
        ]),
        binGroupCode: new FormControl(item.binGroupCode, Validators.required),
        remarkGroupCode: new FormControl(
          item.remarks && item.remarks !== '' ? item.remarks : null
        )
      },
      [this.remarksValidator()]
    );
  }

  measuredWeightValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      return control.value <= 0
        ? { minZero: 'pw.stockReceive.weightInvalidErrorMessage' }
        : null;
    };
  }

  remarksValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      return control.get('binGroupCode').value ===
        StockItemBinGroupCodeEnum.DEFECTIVE &&
        control.get('remarkGroupCode').value === null
        ? { remarkGroupCode: 'pw.stockReceive.remarksRequiredErrorMessage' }
        : null;
    };
  }

  verifyItem() {
    if (this.itemForm.valid) {
      this.verify.emit(this.createItemPayload());
    } else {
      this.makeFormDirty();
    }
  }

  updateItem() {
    if (this.isVerified) {
      if (this.itemForm.valid) {
        this.showUpdateStatus = true;
        this.update.emit(this.createItemPayload());
      } else {
        this.makeFormDirty();
      }
    }
  }

  createItemPayload(): ItemToUpdate {
    const itemFormValue = this.itemForm.value;
    return {
      id: this.item.id,
      newUpdate: {
        measuredWeight: itemFormValue.measuredWeight,
        binGroupCode: itemFormValue.binGroupCode,
        binCode:
          this.isVerified &&
          (itemFormValue.binGroupCode !== StockItemBinGroupCodeEnum.DISPUTE &&
            itemFormValue.binGroupCode !==
              StockItemBinGroupCodeEnum.DEFECTIVE) &&
          this.selectedBinCode
            ? this.selectedBinCode
            : itemFormValue.binGroupCode !==
                StockItemBinGroupCodeEnum.DISPUTE &&
              itemFormValue.binGroupCode !== StockItemBinGroupCodeEnum.DEFECTIVE
            ? StockItemBinCodeEnum.ZERO_BIN
            : itemFormValue.binGroupCode,
        remarks:
          itemFormValue.binGroupCode === StockItemBinGroupCodeEnum.DEFECTIVE
            ? itemFormValue.remarkGroupCode
            : '',
        // TODO : UIN Verification
        itemDetails: this.item.itemDetails
      },
      actualDetails: {
        binCode: this.item.binCode,
        binGroupCode: this.item.binGroupCode,
        measuredWeight: this.item.measuredWeight,
        remarks: this.item.remarks,
        // TODO : UIN Verification
        itemDetails: this.item.itemDetails
      }
    };
  }

  makeFormDirty() {
    this.itemForm.markAsDirty();
    this.itemForm.controls['measuredQuantity'].markAsDirty();
    this.itemForm.controls['measuredWeight'].markAsDirty();
    this.itemForm.controls['binGroupCode'].markAsDirty();
    this.itemForm.controls['remarkGroupCode'].markAsDirty();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (!this.isVerified) {
      this.itemForm.markAsPristine();
      this.parentForm.removeAt(
        this.parentForm.controls.findIndex(
          itemForm => itemForm.get('id').value === this.item.id
        )
      );
    }
  }
}
