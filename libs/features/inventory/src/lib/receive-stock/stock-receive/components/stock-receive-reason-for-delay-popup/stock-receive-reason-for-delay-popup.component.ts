import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'poss-web-stock-receive-reason-for-delay-popup',
  templateUrl: './stock-receive-reason-for-delay-popup.component.html',
  styleUrls: ['./stock-receive-reason-for-delay-popup.component.scss']
})
export class StockReceiveReasonForDelayPopupComponent implements OnInit {
  reasonForDelayForm: FormGroup;
  reasonMaxCharLimit = 250;

  constructor(
    public dialogRef: MatDialogRef<StockReceiveReasonForDelayPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.reasonForDelayForm = new FormGroup({
      reason: new FormControl(
        null,
        Validators.maxLength(this.reasonMaxCharLimit)
      )
    });
  }

  ngOnInit() {}

  close() {
    this.dialogRef.close({ type: 'close' });
  }

  confirm() {
    if (this.reasonForDelayForm.valid) {
      this.dialogRef.close({
        type: 'confirm',
        data: this.reasonForDelayForm.get('reason').value
      });
    }
  }
}
