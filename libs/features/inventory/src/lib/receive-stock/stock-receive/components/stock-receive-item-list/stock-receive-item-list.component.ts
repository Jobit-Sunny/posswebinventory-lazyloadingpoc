import { FormArray } from '@angular/forms';
import {
  Component,
  Input,
  EventEmitter,
  Output,
  OnDestroy,
  SimpleChanges,
  OnChanges,
  OnInit
} from '@angular/core';
import {
  Item,
  ItemToUpdate,
  ItemValidate
} from '../../models/stock-receive.model';
import { Subject } from 'rxjs';
import { PageEvent } from '@angular/material';
import { takeUntil } from 'rxjs/operators';
import { AppsettingFacade, Lov, BinCode } from '@poss-web/core';

@Component({
  selector: 'poss-web-stock-receive-item-list',
  templateUrl: './stock-receive-item-list.component.html',
  styleUrls: ['./stock-receive-item-list.component.scss']
})
export class StockReceiveItemListComponent
  implements OnInit, OnChanges, OnDestroy {
  @Input() itemList: Item[];
  @Input() isVerified: boolean; // verified/non
  @Input() binGroupCode: string;
  @Input() binCodes: BinCode[];
  @Input() remarks: Lov[] = [];
  @Input() tolerance: number;
  @Input() count = 0;
  @Input() pageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };

  @Output() verify = new EventEmitter<ItemToUpdate>();
  @Output() update = new EventEmitter<ItemToUpdate>();
  @Output() validate = new EventEmitter<ItemValidate>();
  @Output() paginator = new EventEmitter<PageEvent>();
  @Output() parentFormDirty = new EventEmitter<boolean>();

  destroy$ = new Subject<null>();
  parentForm: FormArray;
  pageSizeOptions: number[] = [];
  minPageSize = 0;

  ngOnInit(): void {
    this.appSettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.pageSizeOptions = data;
        this.minPageSize = this.pageSizeOptions.reduce((a: number, b: number) =>
          a < b ? a : b
        );
      });
  }

  constructor(private appSettingFacade: AppsettingFacade) {
    this.parentForm = new FormArray([]);
    if (!this.isVerified) {
      this.parentForm.valueChanges
        .pipe(takeUntil(this.destroy$))
        .subscribe(value => {
          this.sendFormStatus();
        });
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes &&
      changes['itemList'] &&
      changes['itemList'].currentValue &&
      changes['itemList'].previousValue &&
      (changes['itemList'].currentValue as []).length === 0 &&
      (changes['itemList'].previousValue as []).length > 0
    ) {
      const newPageEvent: PageEvent = {
        ...this.pageEvent,
        pageIndex: this.pageEvent.pageIndex - 1
      };

      if (newPageEvent.pageIndex >= 0) {
        this.paginate(newPageEvent);
      }
    }
  }
  sendFormStatus() {
    const isDirty = !this.parentForm.pristine;
    this.parentFormDirty.emit(isDirty);
  }

  verifyItem(itemToUpdate: ItemToUpdate) {
    this.verify.emit(itemToUpdate);
  }

  updateItem(itemToUpdate: ItemToUpdate) {
    this.update.emit(itemToUpdate);
  }

  validateItem(data: ItemValidate) {
    this.validate.emit(data);
  }

  paginate(event: PageEvent) {
    this.paginator.emit(event);
  }

  trackBy(index: number, item: Item) {
    return item.id;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
