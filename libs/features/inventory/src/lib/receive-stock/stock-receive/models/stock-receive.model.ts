import { Moment } from 'moment';
/***
 *The information about the count of the StockTransferNote
 */
export interface StockTransferNoteCount {
  count: number;
  type: string;
}

/***
 *The information about the count of the receive invoice
 */
export interface InvoiceCount {
  count: number;
  type: string;
}

export interface CourierDetails {
  type: string;
  data: {
    companyName: string;
    docketNumber: string;
    lockNumber: string;
    roadPermitNumber: string;
    employeeID: string;
    employeeMobileNumber: string;
    employeeName: string;
  };
}

export interface Stock {
  id: number;
  currencyCode: string;
  courierDetails: CourierDetails;
  courierReceivedDate: Moment;
  orderType: string;
  srcDocNo: number;
  srcDocDate: Moment;
  srcFiscalYear: number;
  srcLocationCode: string;
  status: string;
  destDocDate: Moment;
  destDocNo: number;
  destLocationCode: string;
  totalAvailableWeight: number;
  totalAvailableQuantity: number;
  totalAvailableValue: number;
  totalMeasuredQuantity: number;
  totalMeasuredValue: number;
  totalMeasuredWeight: number;
  type: string;
  weightUnit: string;
}

/**
 * information about item details
 */
// TODO : to be checked
export class ItemDetail {
  otherStoneWt: number;
  actualGoldWeight: number;
}
/**
 *The information about the StockTransferNote products
 */

export interface Item {
  id: string;
  currencyCode: string;
  imageURL: string;
  itemCode: string;
  itemDetails: ItemDetail;
  stdValue: number;
  lotNumber: string;
  mfgDate: Moment;
  orderType: string;
  productCategory: string;
  productGroup: string;
  status: string;
  availableQuantity: number;
  availableValue: number;
  availableWeight: number;
  weightUnit: string;
  measuredQuantity: number;
  measuredWeight: number;
  binCode: string;
  binGroupCode: string;
  remarks: string;
  isUpdating: boolean;
  isUpdatingSuccess: boolean;
  isValidating: boolean;
  isValidatingSuccess: boolean;
  isValidatingError: boolean;
}

export interface ItemUpdate {
  binCode: string;
  binGroupCode: string;
  measuredWeight: number;
  remarks: string;
  itemDetails: Object;
}

export interface ItemToUpdate {
  id: string;
  newUpdate: ItemUpdate;
  actualDetails: ItemUpdate;
}

export interface ItemValidate {
  itemId: string;
  productGroupCode: string;
  availableWeight: number;
  measuredWeight: number;
  measuredQuantity: number;
  availableQuantity: number;
}

/**
 * The details of confirmed StockTransferNote
 */
export interface ConfirmReceive {
  courierReceivedDate?: string;
  // * for l1/l2
  reasonForDelay?: string;
  // * for l3
  receivedDate?: string;
  remarks: string;
}

export interface FilterOption {
  id: string;
  description: string;
}
