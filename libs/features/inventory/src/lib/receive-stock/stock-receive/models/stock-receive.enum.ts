export enum StockReceiveTypesEnum {
  FAC_BTQ = 'factory',
  BTQ_BTQ = 'boutique',
  CFA_BTQ = 'cfa',
  MER_BTQ = 'merchandise'
}

export enum StockReceiveAPITypesEnum {
  FAC_BTQ = 'FAC_BTQ',
  BTQ_BTQ = 'BTQ_BTQ',
  CFA_BTQ = 'CFA_BTQ',
  MER_BTQ = 'MER_BTQ'
}

export enum StockReceiveTabEnum {
  NON_VERIFIED = 'nonverified',
  VERIFIED = 'verified'
}

export enum StockStatusEnum {
  ISSUED = 'issued'
}

export enum StockItemStatusEnum {
  VERIFIED = 'VERIFIED',
  ISSUED = 'ISSUED'
}

export enum StockTypeField {
  STN = 'transferType',
  INVOICE = 'invoiceType'
}

export enum StockItemBinGroupCodeEnum {
  STN = 'STN',
  PURCFA = 'PURCFA',
  DISPUTE = 'DISPUTE',
  DEFECTIVE = 'DEFECTIVE'
}
export enum StockItemBinCodeEnum {
  ZERO_BIN = 'ZEROBIN'
}

export enum StockReceiveCarrierTypesEnum {
  COURIER = 'courier',
  EMPLOYEE = 'employee'
}
