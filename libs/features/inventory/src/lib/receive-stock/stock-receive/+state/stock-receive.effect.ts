import {
  CustomErrorAdaptor,
  LovDataService,
  Lov,
  BinDataService,
  BinCode,
  ProductCategoryDataService,
  ProductGroupDataService,
  ProductGroup,
  ProductCategory
} from '@poss-web/core';
import { Item, Stock } from '../models/stock-receive.model';
import { Action } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';

import { map, mergeMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import {
  StockReceiveActionTypes,
  LoadItemsTotalCountSuccessPayload
} from './stock-receive.actions';
import * as StockReceiveActions from './stock-receive.actions';
import { DataPersistence } from '@nrwl/angular';
import { NotificationService, CustomErrors } from '@poss-web/core';
import { HttpErrorResponse } from '@angular/common/http';
import { StockReceiveService } from '../services/stock-receive.service';
import { ErrorEnums } from '@poss-web/shared';

/**
 * Stock Receive Effects
 */
@Injectable()
export class StockReceiveEffect {
  constructor(
    private stockReceiveService: StockReceiveService,
    private dataPersistence: DataPersistence<any>,
    private notificationService: NotificationService,
    private lovDataService: LovDataService,
    private binDataService: BinDataService,
    private productGroupDataService: ProductGroupDataService,
    private productCategoryDataService: ProductCategoryDataService
  ) {}
  /**
   *  The effect which handles the loadPendingFactorySTN Action
   */
  @Effect() loadPendingFactorySTN$: Observable<
    Action
  > = this.dataPersistence.fetch(
    StockReceiveActionTypes.LOAD_PENDING_FACTORY_STN,
    {
      run: (action: StockReceiveActions.LoadPendingFactorySTN) => {
        return this.stockReceiveService
          .getStocks(
            action.payload.type,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (stockTransferNotes: Stock[]) =>
                new StockReceiveActions.LoadPendingFactorySTNSuccess(
                  stockTransferNotes
                )
            )
          );
      },

      onError: (
        action: StockReceiveActions.LoadPendingFactorySTN,
        error: HttpErrorResponse
      ) => {
        return new StockReceiveActions.LoadPendingFactorySTNFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  /**
   * The effect which handles the loadPendingBoutiqueSTN Action
   */
  @Effect() loadPendingBoutiqueSTN$: Observable<
    Action
  > = this.dataPersistence.fetch(
    StockReceiveActionTypes.LOAD_PENDING_BOUTIQUE_STN,
    {
      run: (action: StockReceiveActions.LoadPendingBoutiqueSTN) => {
        return this.stockReceiveService
          .getStocks(
            action.payload.type,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (stockTransferNotes: Stock[]) =>
                new StockReceiveActions.LoadPendingBoutiqueSTNSuccess(
                  stockTransferNotes
                )
            )
          );
      },

      onError: (
        action: StockReceiveActions.LoadPendingBoutiqueSTN,
        error: HttpErrorResponse
      ) => {
        return new StockReceiveActions.LoadPendingBoutiqueSTNFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  /**
   * The effect which handles the loadPendingCFSInvoice Action
   */
  @Effect() loadPendingCFSInvoice$: Observable<
    Action
  > = this.dataPersistence.fetch(
    StockReceiveActionTypes.LOAD_PENDING_CFA_INVOICE,
    {
      run: (action: StockReceiveActions.LoadPendingCFAInvoice) => {
        return this.stockReceiveService
          .getInvoices(
            action.payload.type,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (invoices: Stock[]) =>
                new StockReceiveActions.LoadPendingCFAInvoiceSuccess(invoices)
            )
          );
      },

      onError: (
        action: StockReceiveActions.LoadPendingCFAInvoice,
        error: HttpErrorResponse
      ) => {
        return new StockReceiveActions.LoadPendingCFAInvoiceFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  /**
   * The effect which handles the  searchPendingStocks Action
   */
  @Effect() searchPendingStocks$ = this.dataPersistence.fetch(
    StockReceiveActionTypes.SEARCH_PENDING_STOCKS,
    {
      run: (action: StockReceiveActions.SearchPendingStocks) => {
        return this.stockReceiveService
          .searchStocks(action.payload.srcDocnumber, action.payload.type)
          .pipe(
            map(
              (searchresult: Stock[]) =>
                new StockReceiveActions.SearchPendingStocksSuccess(searchresult)
            )
          );
      },

      onError: (
        action: StockReceiveActions.SearchPendingStocks,
        error: HttpErrorResponse
      ) => {
        return new StockReceiveActions.SearchPendingStocksFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  /**
   * The effect which handles the  searchPendingInvoices Action
   */
  @Effect() searchPendingInvoices$ = this.dataPersistence.fetch(
    StockReceiveActionTypes.SEARCH_PENDING_INVOICES,
    {
      run: (action: StockReceiveActions.SearchPendingInvoices) => {
        return this.stockReceiveService
          .searchInovices(action.payload.srcDocnumber, action.payload.type)
          .pipe(
            map(
              (searchResult: Stock[]) =>
                new StockReceiveActions.SearchPendingInvoicesSuccess(
                  searchResult
                )
            )
          );
      },

      onError: (
        action: StockReceiveActions.SearchPendingInvoices,
        error: HttpErrorResponse
      ) => {
        return new StockReceiveActions.SearchPendingInvoicesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  /**
   * The effect which handles the  loadSeletedStock Action
   */
  @Effect() loadSeletedStock$ = this.dataPersistence.fetch(
    StockReceiveActionTypes.LOAD_SELECTED_STOCK,
    {
      run: (action: StockReceiveActions.LoadSelectedStock) => {
        return this.stockReceiveService
          .getStock(action.payload.id, action.payload.type)
          .pipe(
            map(
              (stockTransferNote: Stock) =>
                new StockReceiveActions.LoadSelectedStockSuccess(
                  stockTransferNote
                )
            )
          );
      },

      onError: (
        action: StockReceiveActions.LoadSelectedStock,
        error: HttpErrorResponse
      ) => {
        return new StockReceiveActions.LoadSelectedStockFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  /**
   * The effect which handles the  loadSeletedSTN Action
   */
  @Effect() loadSeletedInvoice$ = this.dataPersistence.fetch(
    StockReceiveActionTypes.LOAD_SELECTED_INVOICE,
    {
      run: (action: StockReceiveActions.LoadSelectedInvoice) => {
        return this.stockReceiveService
          .getInvoice(action.payload.id, action.payload.type)
          .pipe(
            map(
              (invoice: Stock) =>
                new StockReceiveActions.LoadSelectedInvoiceSuccess(invoice)
            )
          );
      },

      onError: (
        action: StockReceiveActions.LoadSelectedInvoice,
        error: HttpErrorResponse
      ) => {
        return new StockReceiveActions.LoadSelectedInvoiceFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  // For verify items

  @Effect()
  loadItemsTotalCount$ = this.dataPersistence.fetch(
    StockReceiveActionTypes.LOAD_ItEMS_COUNT,
    {
      run: (action: StockReceiveActions.LoadItemsTotalCount) => {
        return this.stockReceiveService
          .getItemsCount(
            action.payload.storeType,
            action.payload.type,
            action.payload.id
          )
          .pipe(
            map(
              (
                loadItemsTotalCountSuccessPayload: LoadItemsTotalCountSuccessPayload
              ) =>
                new StockReceiveActions.LoadItemsTotalCountSuccess(
                  loadItemsTotalCountSuccessPayload
                )
            )
          );
      },

      onError: (
        action: StockReceiveActions.LoadItemsTotalCount,
        error: HttpErrorResponse
      ) => {
        return new StockReceiveActions.LoadItemsTotalCountFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadItems$ = this.dataPersistence.fetch(StockReceiveActionTypes.LOAD_ITEMS, {
    run: (action: StockReceiveActions.LoadItems) => {
      return this.stockReceiveService
        .getItems(
          action.payload.storeType,
          action.payload.type,
          action.payload.status,
          action.payload.id,
          action.payload.itemCode,
          action.payload.lotNumber,
          action.payload.pageIndex,
          action.payload.pageSize,
          action.payload.sortBy,
          action.payload.sortOrder,
          action.payload.filter
        )
        .pipe(
          map(
            (data: { items: Item[]; count: number }) =>
              new StockReceiveActions.LoadItemsSuccess({
                ...data,
                status: action.payload.status
              })
          )
        );
    },
    onError: (
      action: StockReceiveActions.LoadItems,
      error: HttpErrorResponse
    ) => {
      return new StockReceiveActions.LoadItemsFailure(this.errorHandler(error));
    }
  });

  @Effect()
  loadBins$ = this.dataPersistence.fetch(
    StockReceiveActionTypes.LOAD_BIN_CODES,
    {
      run: (action: StockReceiveActions.LoadBinCodes) => {
        return this.binDataService
          .getBinDetails(action.payload, true, false)
          .pipe(
            map(
              (bincodes: BinCode[]) =>
                new StockReceiveActions.LoadBinCodesSuccess(bincodes)
            )
          );
      },

      onError: (
        action: StockReceiveActions.LoadBinCodes,
        error: HttpErrorResponse
      ) => {
        return new StockReceiveActions.LoadBinCodesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadRemarks$ = this.dataPersistence.fetch(
    StockReceiveActionTypes.LOAD_REMARKS,
    {
      run: () => {
        return this.lovDataService
          .getInventoryLovs('DEFECTTYPE')
          .pipe(
            map(
              (remarks: Lov[]) =>
                new StockReceiveActions.LoadRemarksSuccess(remarks)
            )
          );
      },

      onError: (
        action: StockReceiveActions.LoadRemarks,
        error: HttpErrorResponse
      ) => {
        return new StockReceiveActions.LoadRemarksFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadTolerance$ = this.dataPersistence.fetch(
    StockReceiveActionTypes.LOAD_TOLERANCE,
    {
      run: () => {
        return this.stockReceiveService
          .getTolerance()
          .pipe(
            map(
              (tolerance: number) =>
                new StockReceiveActions.LoadToleranceSuccess(tolerance)
            )
          );
      },

      onError: (
        action: StockReceiveActions.LoadTolerance,
        error: HttpErrorResponse
      ) => {
        return new StockReceiveActions.LoadToleranceFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  verifyItem$ = this.dataPersistence.pessimisticUpdate(
    StockReceiveActionTypes.VERIFY_ITEM,
    {
      run: (action: StockReceiveActions.VerifyItem) => {
        return this.stockReceiveService
          .verifyItem(
            action.payload.storeType,
            action.payload.type,
            action.payload.id,
            action.payload.itemId,
            action.payload.newUpdate
          )
          .pipe(
            mergeMap((item: Item) => [
              new StockReceiveActions.VerifyItemSuccess(item),
              new StockReceiveActions.LoadItems(
                action.payload.loadItemsPayload
              ),
              new StockReceiveActions.LoadItemsTotalCount(
                action.payload.loadTemsCountPayload
              )
            ])
          );
      },

      onError: (
        action: StockReceiveActions.VerifyItem,
        error: HttpErrorResponse
      ) => {
        return new StockReceiveActions.VerifyItemFailure({
          itemId: action.payload.itemId,
          actualDetails: action.payload.actualDetails,
          error: this.errorHandler(error)
        });
      }
    }
  );

  @Effect()
  updateItem$ = this.dataPersistence.pessimisticUpdate(
    StockReceiveActionTypes.UPADTE_ITEM,
    {
      run: (action: StockReceiveActions.UpdateItem) => {
        return this.stockReceiveService
          .verifyItem(
            action.payload.storeType,
            action.payload.type,
            action.payload.id,
            action.payload.itemId,
            action.payload.newUpdate
          )
          .pipe(
            map((item: Item) => new StockReceiveActions.UpdateItemSuccess(item))
          );
      },

      onError: (
        action: StockReceiveActions.UpdateItem,
        error: HttpErrorResponse
      ) => {
        return new StockReceiveActions.UpdateItemFailure({
          itemId: action.payload.itemId,
          actualDetails: action.payload.actualDetails,
          error: this.errorHandler(error)
        });
      }
    }
  );

  @Effect()
  validateItem$ = this.dataPersistence.fetch(
    StockReceiveActionTypes.VALIDATE_ITEM,
    {
      run: (action: StockReceiveActions.ValidateItem) => {
        return this.stockReceiveService
          .validateItem(
            action.payload.productGroupCode,
            action.payload.availableWeight,
            action.payload.measuredWeight,
            action.payload.measuredQuantity,
            action.payload.availableQuantity
          )
          .pipe(
            map(
              (isSuccess: boolean) =>
                new StockReceiveActions.ValidateItemSuccess({
                  itemId: action.payload.itemId,
                  isSuccess: true
                })
            )
          );
      },

      onError: (
        action: StockReceiveActions.ValidateItem,
        error: HttpErrorResponse
      ) => {
        const err = CustomErrorAdaptor.fromJson(error);
        if (err.code === ErrorEnums.ERR_INV_028) {
          return new StockReceiveActions.ValidateItemSuccess({
            itemId: action.payload.itemId,
            isSuccess: false
          });
        } else {
          return new StockReceiveActions.ValidateItemFailure({
            itemId: action.payload.itemId,
            error: this.errorHandler(error)
          });
        }
      }
    }
  );

  @Effect()
  verifyAllItems$ = this.dataPersistence.pessimisticUpdate(
    StockReceiveActionTypes.VERIFY_ALL_ITEMS,
    {
      run: (action: StockReceiveActions.VerifyAllItems) => {
        return this.stockReceiveService
          .updateAllItems(
            action.payload.storeType,
            action.payload.type,
            action.payload.id,
            action.payload.data
          )
          .pipe(
            map(
              (isSuccess: boolean) =>
                new StockReceiveActions.VerifyAllItemsSuccess(isSuccess)
            )
          );
      },

      onError: (
        action: StockReceiveActions.VerifyAllItems,
        error: HttpErrorResponse
      ) => {
        return new StockReceiveActions.VerifyAllItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  assignBinToAllItems$ = this.dataPersistence.pessimisticUpdate(
    StockReceiveActionTypes.ASSIGN_BIN_ALL_ITEMS,
    {
      run: (action: StockReceiveActions.AssignBinToAllItems) => {
        return this.stockReceiveService
          .updateAllItems(
            action.payload.storeType,
            action.payload.type,
            action.payload.id,
            action.payload.data
          )
          .pipe(
            map(
              (isSuccess: boolean) =>
                new StockReceiveActions.AssignBinToAllItemsSuccess(isSuccess)
            )
          );
      },

      onError: (
        action: StockReceiveActions.AssignBinToAllItems,
        error: HttpErrorResponse
      ) => {
        return new StockReceiveActions.AssignBinToAllItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  confirmStock$ = this.dataPersistence.pessimisticUpdate(
    StockReceiveActionTypes.CONFIRM_STOCK_RECEIVE,
    {
      run: (action: StockReceiveActions.ConfirmStockReceive) => {
        return this.stockReceiveService
          .confirmStn(
            action.payload.storeType,
            action.payload.type,
            action.payload.id,
            action.payload.confirmReceive
          )
          .pipe(
            map(
              (confirmedStock: any) =>
                new StockReceiveActions.ConfirmStockReceiveSuccess(
                  confirmedStock
                )
            )
          );
      },

      onError: (
        action: StockReceiveActions.ConfirmStockReceive,
        error: HttpErrorResponse
      ) => {
        return new StockReceiveActions.ConfirmStockReceiveFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadProductGroups$ = this.dataPersistence.fetch(
    StockReceiveActionTypes.LOAD_PRODUCT_GROUPS,
    {
      run: () => {
        return this.productGroupDataService
          .getProductGroups()
          .pipe(
            map(
              (data: ProductGroup[]) =>
                new StockReceiveActions.LoadProductGroupsSuccess(data)
            )
          );
      },

      onError: (
        action: StockReceiveActions.LoadProductGroups,
        error: HttpErrorResponse
      ) => {
        return new StockReceiveActions.LoadProductGroupsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadProductCategories$ = this.dataPersistence.fetch(
    StockReceiveActionTypes.LOAD_PRODUCT_CATEGORIES,
    {
      run: () => {
        return this.productCategoryDataService
          .getProductCategories()
          .pipe(
            map(
              (data: ProductCategory[]) =>
                new StockReceiveActions.LoadProductCategoriesSuccess(data)
            )
          );
      },

      onError: (
        action: StockReceiveActions.LoadProductCategories,
        error: HttpErrorResponse
      ) => {
        return new StockReceiveActions.LoadProductCategoriesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
}
