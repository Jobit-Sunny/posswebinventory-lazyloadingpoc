import { Action } from '@ngrx/store';
import {
  Item,
  ItemUpdate,
  ConfirmReceive,
  Stock,
  ItemValidate
} from '../models/stock-receive.model';

import {
  CustomErrors,
  Lov,
  BinCode,
  ProductGroup,
  ProductCategory
} from '@poss-web/core';

/**
 * The interface for Action payload
 */
export interface LoadPendingPayload {
  type: string;
  pageIndex: number;
  pageSize: number;
}

export interface SearchPendingPayload {
  srcDocnumber: number;
  type: string;
}

export interface LoadItemsPayload {
  storeType: string;
  type: string;
  id: number;
  status: string;
  itemCode: string;
  lotNumber: string;
  pageIndex: number;
  pageSize: number;
  sortBy: string;
  sortOrder: string;
  filter: { key: string; value: any[] }[];
}

export interface LoadItemsTotalCountPayload {
  storeType: string;
  type: string;
  id: number;
}
export interface LoadItemsTotalCountSuccessPayload {
  nonVerifiedItemsTotalCount: number;
  verifiedItemsTotalCount: number;
}

export interface UpdateItemPayload {
  type: string;
  storeType: string;
  id: number;
  itemId: string;
  newUpdate: ItemUpdate;
  actualDetails: ItemUpdate;
  loadItemsPayload?: LoadItemsPayload;
  loadTemsCountPayload?: LoadItemsTotalCountPayload;
}

export interface UpdateItemFailurePayload {
  itemId: string;
  actualDetails: ItemUpdate;
  error: CustomErrors;
}

export interface UpdateAllItemsPayload {
  type: string;
  storeType: string;
  id: number;
  data: {
    binCode?: string;
    id?: string[];
    status?: string;
  };
}

export interface ConfirmStockReceivePayload {
  type: string;
  storeType: string;
  id: number;
  confirmReceive: ConfirmReceive;
}

/**
 * The  enum defined for  list of actions of stock receive
 */
export enum StockReceiveActionTypes {
  LOAD_PENDING_FACTORY_STN = '[ Stock-Receive-List ] Load pending STN List from Factory',
  LOAD_PENDING_FACTORY_STN_SUCCESS = '[ Stock-Receive-List ] Load pending STN List from Factory Success',
  LOAD_PENDING_FACTORY_STN_FAILURE = '[ Stock-Receive-List ] Load pending STN List from Factory Failure',

  LOAD_PENDING_BOUTIQUE_STN = '[ Stock-Receive-List ] Load pending STN List from Boutique',
  LOAD_PENDING_BOUTIQUE_STN_SUCCESS = '[ Stock-Receive-List ] Load pending STN List from Boutique Success',
  LOAD_PENDING_BOUTIQUE_STN_FAILURE = '[ Stock-Receive-List ] Load pending STN List from Boutique Failure',
  CLEAR_PENDING_BOUTIQUE_STN = '[ Stock-Receive-List ] Clear pending STN List of Boutique',

  LOAD_PENDING_CFA_INVOICE = '[ Stock-Receive-List ] Load pending Invoice List from CFA',
  LOAD_PENDING_CFA_INVOICE_SUCCESS = '[ Stock-Receive-List ] Load pending Invoice List from CFA Success',
  LOAD_PENDING_CFA_INVOICE_FAILURE = '[ Stock-Receive-List ] Load pending Invoice List from CFA Failure',

  SEARCH_PENDING_STOCKS = '[ Stock-Receive-List ] Search Pending Stocks',
  SEARCH_PENDING_STOCKS_SUCCESS = '[ Stock-Receive-List ]Search Pending Stocks Success',
  SEARCH_PENDING_STOCKS_FAILURE = '[ Stock-Receive-List ] Search Pending Stocks Failure',

  SEARCH_PENDING_INVOICES = '[ Stock-Receive-List ] Search Pending Inovices ',
  SEARCH_PENDING_INVOICES_SUCCESS = '[ Stock-Receive-List ] Search Pending Inovices Success',
  SEARCH_PENDING_INVOICES_FAILURE = '[ Stock-Receive-List ] Search Pending Inovices Failure',

  LOAD_SELECTED_STOCK = '[ Stock-Receive-Details ] Load selected stock ',
  LOAD_SELECTED_STOCK_SUCCESS = '[ Stock-Receive-Details ] Load selected stock Success',
  LOAD_SELECTED_STOCK_FAILURE = '[ Stock-Receive-Details ] Load selected stock Failure',

  LOAD_SELECTED_INVOICE = '[ Stock-Receive-Details ] Load selected Invoice ',
  LOAD_SELECTED_INVOICE_SUCCESS = '[ Stock-Receive-Details  ] Load selected Invoice Success',
  LOAD_SELECTED_INVOICE_FAILURE = '[Stock-Receive-Details  ] Load selected Invoice Failure',

  SEARCH_CLEAR = '[ Stock-receive ] search-clear',

  LOAD_ItEMS_COUNT = '[ Stock-Receive-Details ] Load items Count ',
  LOAD_ItEMS_COUNT_SUCCESS = '[ Stock-Receive-Details ] Load items Count Success ',
  LOAD_ItEMS_COUNT_FAILURE = '[ Stock-Receive-Details ] Load items Count Failure ',

  LOAD_ITEMS = '[ Stock-Receive-Details ] Load Items ',
  LOAD_ITEMS_SUCCESS = '[ Stock-Receive-Details ] Load items Success ',
  LOAD_ITEMS_FAILURE = '[ Stock-Receive-Details ] Load items Failure ',

  LOAD_BIN_CODES = '[ Stock-Receive-Details ] Load all bins ',
  LOAD_BIN_CODES_SUCCESS = '[ Stock-Receive-Details ] Load all bins Success ',
  LOAD_BIN_CODES_FAILURE = '[ Stock-Receive-Details ]  Load all bins Failure ',

  LOAD_REMARKS = '[ Stock-Receive-Details ] Load all Remarks ',
  LOAD_REMARKS_SUCCESS = '[ Stock-Receive-Details ] Load all Remarks Success ',
  LOAD_REMARKS_FAILURE = '[ Stock-Receive-Details ]  Load all Remarks Failure ',

  LOAD_TOLERANCE = '[ Stock-Receive-Details ] Load tolerance ',
  LOAD_TOLERANCE_SUCCESS = '[ Stock-Receive-Details ] Load tolerance Success ',
  LOAD_TOLERANCE_FAILURE = '[ Stock-Receive-Details ]  Load tolerance Failure ',

  VERIFY_ITEM = '[ Stock-Receive-Details ] Verify item ',
  VERIFY_ITEM_SUCCESS = '[ Stock-Receive-Details ]  Verify item Success ',
  VERIFY_ITEM_FAILURE = '[ Stock-Receive-Details ]  Verify item Failure ',

  UPADTE_ITEM = '[ Stock-Receive-Details ] Update item ',
  UPADTE_ITEM_SUCCESS = '[ Stock-Receive-Details ]  Update item Success ',
  UPADTE_ITEM_FAILURE = '[ Stock-Receive-Details ]  Update item Failure ',

  VALIDATE_ITEM = '[ Stock-Receive-Details ] Validate item ',
  VALIDATE_ITEM_SUCCESS = '[ Stock-Receive-Details ]  Validate item Success ',
  VALIDATE_ITEM_FAILURE = '[ Stock-Receive-Details ]  Validate item Failure ',

  VERIFY_ALL_ITEMS = '[ Stock-Receive-Details ] Verify all items',
  VERIFY_ALL_ITEMS_SUCCESS = '[ Stock-Receive-Details ] Verify all items Success ',
  VERIFY_ALL_ITEMS_FAILURE = '[ Stock-Receive-Details ] Verify all items Failure ',

  ASSIGN_BIN_ALL_ITEMS = '[ Stock-Receive-Details ] Assign bin to all items ',
  ASSIGN_BIN_ALL_ITEMS_SUCCESS = '[ Stock-Receive-Details ]  Assign bin to all items Success ',
  ASSIGN_BIN_ALL_ITEMS_FAILURE = '[ Stock-Receive-Details ] Assign bin to all items Failure ',

  CONFIRM_STOCK_RECEIVE = '[stock-receive] Confirm Stock',
  CONFIRM_STOCK_RECEIVE_SUCCESS = '[stock-receive] Confirm STN Success',
  CONFIRM_STOCK_RECEIVE_FAILURE = '[stock-receive] Confirm STN Failure',

  LOAD_PRODUCT_GROUPS = '[stock-receive]  Load Product Groups ',
  LOAD_PRODUCT_GROUPS_SUCCESS = '[stock-receive]  Load Product Groups Success ',
  LOAD_PRODUCT_GROUPS_FAILURE = '[stock-receive]  Load Product Groups Failure ',

  LOAD_PRODUCT_CATEGORIES = '[stock-receive]  Load Product Categories ',
  LOAD_PRODUCT_CATEGORIES_SUCCESS = '[stock-receive]  Load Product Categories  Success ',
  LOAD_PRODUCT_CATEGORIES_FAILURE = '[stock-receive]  Load Product Categories  Failure ',

  CLEAR_STOCKS = '[stock-receive] Clear Loaded Stocks',
  CLEAR_ITEMS = '[stock-receive] Clear Items',
  RESET_ERROR = '[stock-receive] Reset Error'
}

/**
 * Stock Receive Actions
 */
export class LoadPendingFactorySTN implements Action {
  readonly type = StockReceiveActionTypes.LOAD_PENDING_FACTORY_STN;
  constructor(public payload: LoadPendingPayload) {}
}

export class LoadPendingFactorySTNSuccess implements Action {
  readonly type = StockReceiveActionTypes.LOAD_PENDING_FACTORY_STN_SUCCESS;
  constructor(public payload: Stock[]) {}
}

export class LoadPendingFactorySTNFailure implements Action {
  readonly type = StockReceiveActionTypes.LOAD_PENDING_FACTORY_STN_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadPendingBoutiqueSTN implements Action {
  readonly type = StockReceiveActionTypes.LOAD_PENDING_BOUTIQUE_STN;
  constructor(public payload: LoadPendingPayload) {}
}

export class LoadPendingBoutiqueSTNSuccess implements Action {
  readonly type = StockReceiveActionTypes.LOAD_PENDING_BOUTIQUE_STN_SUCCESS;
  constructor(public payload: Stock[]) {}
}

export class LoadPendingBoutiqueSTNFailure implements Action {
  readonly type = StockReceiveActionTypes.LOAD_PENDING_BOUTIQUE_STN_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class ClearPendingBoutiqueSTN implements Action {
  readonly type = StockReceiveActionTypes.CLEAR_PENDING_BOUTIQUE_STN;
}

export class LoadPendingCFAInvoice implements Action {
  readonly type = StockReceiveActionTypes.LOAD_PENDING_CFA_INVOICE;
  constructor(public payload: LoadPendingPayload) {}
}

export class LoadPendingCFAInvoiceSuccess implements Action {
  readonly type = StockReceiveActionTypes.LOAD_PENDING_CFA_INVOICE_SUCCESS;
  constructor(public payload: Stock[]) {}
}

export class LoadPendingCFAInvoiceFailure implements Action {
  readonly type = StockReceiveActionTypes.LOAD_PENDING_CFA_INVOICE_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class SearchPendingStocks implements Action {
  readonly type = StockReceiveActionTypes.SEARCH_PENDING_STOCKS;
  constructor(public payload: SearchPendingPayload) {}
}

export class SearchPendingStocksSuccess implements Action {
  readonly type = StockReceiveActionTypes.SEARCH_PENDING_STOCKS_SUCCESS;
  constructor(public payload: Stock[]) {}
}
export class SearchPendingStocksFailure implements Action {
  readonly type = StockReceiveActionTypes.SEARCH_PENDING_STOCKS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class SearchPendingInvoices implements Action {
  readonly type = StockReceiveActionTypes.SEARCH_PENDING_INVOICES;
  constructor(public payload: SearchPendingPayload) {}
}

export class SearchPendingInvoicesSuccess implements Action {
  readonly type = StockReceiveActionTypes.SEARCH_PENDING_INVOICES_SUCCESS;
  constructor(public payload: Stock[]) {}
}
export class SearchPendingInvoicesFailure implements Action {
  readonly type = StockReceiveActionTypes.SEARCH_PENDING_INVOICES_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class SearchClear implements Action {
  readonly type = StockReceiveActionTypes.SEARCH_CLEAR;
}

export class LoadSelectedStock implements Action {
  readonly type = StockReceiveActionTypes.LOAD_SELECTED_STOCK;
  constructor(public payload: { id: string; type: string }) {}
}

export class LoadSelectedStockSuccess implements Action {
  readonly type = StockReceiveActionTypes.LOAD_SELECTED_STOCK_SUCCESS;
  constructor(public payload: Stock) {}
}
export class LoadSelectedStockFailure implements Action {
  readonly type = StockReceiveActionTypes.LOAD_SELECTED_STOCK_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadSelectedInvoice implements Action {
  readonly type = StockReceiveActionTypes.LOAD_SELECTED_INVOICE;
  constructor(public payload: { id: string; type: string }) {}
}

export class LoadSelectedInvoiceSuccess implements Action {
  readonly type = StockReceiveActionTypes.LOAD_SELECTED_INVOICE_SUCCESS;
  constructor(public payload: Stock) {}
}
export class LoadSelectedInvoiceFailure implements Action {
  readonly type = StockReceiveActionTypes.LOAD_SELECTED_INVOICE_FAILURE;
  constructor(public payload: CustomErrors) {}
}

// For Stock-recevie verification

export class LoadItemsTotalCount implements Action {
  readonly type = StockReceiveActionTypes.LOAD_ItEMS_COUNT;
  constructor(public payload: LoadItemsTotalCountPayload) {}
}

export class LoadItemsTotalCountSuccess implements Action {
  readonly type = StockReceiveActionTypes.LOAD_ItEMS_COUNT_SUCCESS;
  constructor(public payload: LoadItemsTotalCountSuccessPayload) {}
}
export class LoadItemsTotalCountFailure implements Action {
  readonly type = StockReceiveActionTypes.LOAD_ItEMS_COUNT_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadItems implements Action {
  readonly type = StockReceiveActionTypes.LOAD_ITEMS;
  constructor(public payload: LoadItemsPayload) {}
}

export class LoadItemsSuccess implements Action {
  readonly type = StockReceiveActionTypes.LOAD_ITEMS_SUCCESS;
  constructor(
    public payload: { items: Item[]; count: number; status: string }
  ) {}
}
export class LoadItemsFailure implements Action {
  readonly type = StockReceiveActionTypes.LOAD_ITEMS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadBinCodes implements Action {
  readonly type = StockReceiveActionTypes.LOAD_BIN_CODES;
  constructor(public payload: string) {}
}

export class LoadBinCodesSuccess implements Action {
  readonly type = StockReceiveActionTypes.LOAD_BIN_CODES_SUCCESS;
  constructor(public payload: BinCode[]) {}
}
export class LoadBinCodesFailure implements Action {
  readonly type = StockReceiveActionTypes.LOAD_BIN_CODES_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadRemarks implements Action {
  readonly type = StockReceiveActionTypes.LOAD_REMARKS;
}

export class LoadRemarksSuccess implements Action {
  readonly type = StockReceiveActionTypes.LOAD_REMARKS_SUCCESS;
  constructor(public payload: Lov[]) {}
}
export class LoadRemarksFailure implements Action {
  readonly type = StockReceiveActionTypes.LOAD_REMARKS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadTolerance implements Action {
  readonly type = StockReceiveActionTypes.LOAD_TOLERANCE;
}

export class LoadToleranceSuccess implements Action {
  readonly type = StockReceiveActionTypes.LOAD_TOLERANCE_SUCCESS;
  constructor(public payload: number) {}
}
export class LoadToleranceFailure implements Action {
  readonly type = StockReceiveActionTypes.LOAD_TOLERANCE_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class VerifyItem implements Action {
  readonly type = StockReceiveActionTypes.VERIFY_ITEM;
  constructor(public payload: UpdateItemPayload) {}
}
export class VerifyItemSuccess implements Action {
  readonly type = StockReceiveActionTypes.VERIFY_ITEM_SUCCESS;
  constructor(public payload: Item) {}
}
export class VerifyItemFailure implements Action {
  readonly type = StockReceiveActionTypes.VERIFY_ITEM_FAILURE;
  constructor(public payload: UpdateItemFailurePayload) {}
}
export class UpdateItem implements Action {
  readonly type = StockReceiveActionTypes.UPADTE_ITEM;
  constructor(public payload: UpdateItemPayload) {}
}

export class UpdateItemSuccess implements Action {
  readonly type = StockReceiveActionTypes.UPADTE_ITEM_SUCCESS;
  constructor(public payload: Item) {}
}
export class UpdateItemFailure implements Action {
  readonly type = StockReceiveActionTypes.UPADTE_ITEM_FAILURE;
  constructor(public payload: UpdateItemFailurePayload) {}
}

export class ValidateItem implements Action {
  readonly type = StockReceiveActionTypes.VALIDATE_ITEM;
  constructor(public payload: ItemValidate) {}
}

export class ValidateItemSuccess implements Action {
  readonly type = StockReceiveActionTypes.VALIDATE_ITEM_SUCCESS;
  constructor(public payload: { itemId: string; isSuccess: boolean }) {}
}
export class ValidateItemFailure implements Action {
  readonly type = StockReceiveActionTypes.VALIDATE_ITEM_FAILURE;
  constructor(public payload: { itemId: string; error: CustomErrors }) {}
}

export class VerifyAllItems implements Action {
  readonly type = StockReceiveActionTypes.VERIFY_ALL_ITEMS;
  constructor(public payload: UpdateAllItemsPayload) {}
}
export class VerifyAllItemsSuccess implements Action {
  readonly type = StockReceiveActionTypes.VERIFY_ALL_ITEMS_SUCCESS;
  constructor(public payload: boolean) {}
}
export class VerifyAllItemsFailure implements Action {
  readonly type = StockReceiveActionTypes.VERIFY_ALL_ITEMS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class AssignBinToAllItems implements Action {
  readonly type = StockReceiveActionTypes.ASSIGN_BIN_ALL_ITEMS;
  constructor(public payload: UpdateAllItemsPayload) {}
}
export class AssignBinToAllItemsSuccess implements Action {
  readonly type = StockReceiveActionTypes.ASSIGN_BIN_ALL_ITEMS_SUCCESS;
  constructor(public payload: boolean) {}
}
export class AssignBinToAllItemsFailure implements Action {
  readonly type = StockReceiveActionTypes.ASSIGN_BIN_ALL_ITEMS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class ConfirmStockReceive implements Action {
  readonly type = StockReceiveActionTypes.CONFIRM_STOCK_RECEIVE;
  constructor(public payload: ConfirmStockReceivePayload) {}
}
export class ConfirmStockReceiveSuccess implements Action {
  readonly type = StockReceiveActionTypes.CONFIRM_STOCK_RECEIVE_SUCCESS;
  constructor(public payload: any) {}
}
export class ConfirmStockReceiveFailure implements Action {
  readonly type = StockReceiveActionTypes.CONFIRM_STOCK_RECEIVE_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadProductGroups implements Action {
  readonly type = StockReceiveActionTypes.LOAD_PRODUCT_GROUPS;
}
export class LoadProductGroupsSuccess implements Action {
  readonly type = StockReceiveActionTypes.LOAD_PRODUCT_GROUPS_SUCCESS;
  constructor(public payload: ProductGroup[]) {}
}
export class LoadProductGroupsFailure implements Action {
  readonly type = StockReceiveActionTypes.LOAD_PRODUCT_GROUPS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadProductCategories implements Action {
  readonly type = StockReceiveActionTypes.LOAD_PRODUCT_CATEGORIES;
}
export class LoadProductCategoriesSuccess implements Action {
  readonly type = StockReceiveActionTypes.LOAD_PRODUCT_CATEGORIES_SUCCESS;
  constructor(public payload: ProductCategory[]) {}
}
export class LoadProductCategoriesFailure implements Action {
  readonly type = StockReceiveActionTypes.LOAD_PRODUCT_CATEGORIES_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class ResetError implements Action {
  readonly type = StockReceiveActionTypes.RESET_ERROR;
}

export class ClearStocks implements Action {
  readonly type = StockReceiveActionTypes.CLEAR_STOCKS;
}

export class ClearItems implements Action {
  readonly type = StockReceiveActionTypes.CLEAR_ITEMS;
}

/**
 * Stock Receive Actions Type
 */
export type StockReceiveActions =
  | LoadPendingFactorySTN
  | LoadPendingFactorySTNSuccess
  | LoadPendingFactorySTNFailure
  | LoadPendingBoutiqueSTN
  | LoadPendingBoutiqueSTNSuccess
  | LoadPendingBoutiqueSTNFailure
  | ClearPendingBoutiqueSTN
  | LoadPendingCFAInvoice
  | LoadPendingCFAInvoiceSuccess
  | LoadPendingCFAInvoiceFailure
  | SearchPendingStocks
  | SearchPendingStocksSuccess
  | SearchPendingStocksFailure
  | SearchPendingInvoices
  | SearchPendingInvoicesSuccess
  | SearchPendingInvoicesFailure
  | SearchClear
  | LoadItemsTotalCount
  | LoadItemsTotalCountSuccess
  | LoadItemsTotalCountFailure
  | LoadSelectedStock
  | LoadSelectedStockSuccess
  | LoadSelectedStockFailure
  | LoadSelectedInvoice
  | LoadSelectedInvoiceSuccess
  | LoadSelectedInvoiceFailure
  | LoadItems
  | LoadItemsSuccess
  | LoadItemsFailure
  | LoadBinCodes
  | LoadBinCodesSuccess
  | LoadBinCodesFailure
  | VerifyItem
  | VerifyItemSuccess
  | VerifyItemFailure
  | UpdateItem
  | UpdateItemSuccess
  | UpdateItemFailure
  | ConfirmStockReceive
  | ConfirmStockReceiveSuccess
  | ConfirmStockReceiveFailure
  | LoadRemarks
  | LoadRemarksSuccess
  | LoadRemarksFailure
  | LoadTolerance
  | LoadToleranceSuccess
  | LoadToleranceFailure
  | VerifyAllItems
  | VerifyAllItemsSuccess
  | VerifyAllItemsFailure
  | AssignBinToAllItems
  | AssignBinToAllItemsSuccess
  | AssignBinToAllItemsFailure
  | ResetError
  | ClearStocks
  | LoadProductGroups
  | LoadProductGroupsSuccess
  | LoadProductGroupsFailure
  | LoadProductCategories
  | LoadProductCategoriesSuccess
  | LoadProductCategoriesFailure
  | ValidateItem
  | ValidateItemSuccess
  | ValidateItemFailure
  | ClearItems;
