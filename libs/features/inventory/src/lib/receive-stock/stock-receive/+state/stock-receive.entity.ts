import { Item, Stock } from '../models/stock-receive.model';
import { EntityState, createEntityAdapter } from '@ngrx/entity';

export interface ItemEntity extends EntityState<Item> {}
export const itemAdapter = createEntityAdapter<Item>({
  selectId: item => item.id
});
export const itemSelector = itemAdapter.getSelectors();

export interface StockEntity extends EntityState<Stock> {}
export const stockAdapter = createEntityAdapter<Stock>({
  selectId: stock => stock.id
});
export const stockSelector = stockAdapter.getSelectors();
