import { createSelector } from '@ngrx/store';
import { itemSelector, stockSelector } from './stock-receive.entity';
import { selectInventory } from '../../../inventory.state';

/**
 * The selectors for Stock-Receive store
 */

const pendingFactorySTN = createSelector(
  selectInventory,
  state => state.stockReceive.pendingFactorySTN
);

const selectPendingFactorySTN = createSelector(
  pendingFactorySTN,
  stockSelector.selectAll
);

const pendingBoutiqueSTN = createSelector(
  selectInventory,
  state => state.stockReceive.pendingBoutiqueSTN
);

const selectPendingBoutiqueSTN = createSelector(
  pendingBoutiqueSTN,
  stockSelector.selectAll
);

const pendingCFAInvoice = createSelector(
  selectInventory,
  state => state.stockReceive.pendingCFAInvoice
);

const selectPendingCFAInvoice = createSelector(
  pendingCFAInvoice,
  stockSelector.selectAll
);

const selectError = createSelector(
  selectInventory,
  state => state.stockReceive.error
);

const selectIsLoadingBinGroups = createSelector(
  selectInventory,
  state => state.stockReceive.isLoadingBinGroups
);

const selectIsLoadingRemarks = createSelector(
  selectInventory,
  state => state.stockReceive.isLoadingRemarks
);

const selectIsLoadingTolerance = createSelector(
  selectInventory,
  state => state.stockReceive.isLoadingTolerance
);

const searchStockResults = createSelector(
  selectInventory,
  state => state.stockReceive.searchStockResults
);
const selectSearchStockResults = createSelector(
  searchStockResults,
  stockSelector.selectAll
);

const searchInvoiceResults = createSelector(
  selectInventory,
  state => state.stockReceive.searchInvoiceResults
);

const selectSearchInvoiceResults = createSelector(
  searchInvoiceResults,
  stockSelector.selectAll
);

const selectIsLoadingPendingFactorySTN = createSelector(
  selectInventory,
  state => state.stockReceive.isLoadingPendingFactorySTN
);

const selectIsLoadingPendingBoutiqueSTN = createSelector(
  selectInventory,
  state => state.stockReceive.isLoadingPendingBoutiqueSTN
);

const selectIsLoadingPendingCFAInvoice = createSelector(
  selectInventory,
  state => state.stockReceive.isLoadingPendingCFAInvoice
);

const selectIsSearchingStocks = createSelector(
  selectInventory,
  state => state.stockReceive.isSearchingStocks
);

const selectHasSearchStockResults = createSelector(
  selectInventory,
  state => state.stockReceive.hasSearchStockResults
);

const selectIsSearchingInvoices = createSelector(
  selectInventory,
  state => state.stockReceive.isSearchingInvoices
);

const selectHasSearchInvoiceResults = createSelector(
  selectInventory,
  state => state.stockReceive.hasSearchInvoiceResults
);

const selectSelectedStock = createSelector(
  selectInventory,
  state => state.stockReceive.selectedStock
);

const selectSelectedInvoice = createSelector(
  selectInventory,
  state => state.stockReceive.selectedInvoice
);

const selectIsLoadingSelectedStock = createSelector(
  selectInventory,
  state => state.stockReceive.isLoadingSelectedStock
);

const selectItemsCount = createSelector(
  selectInventory,
  state => state.stockReceive.itemsCount
);

const items = createSelector(
  selectInventory,
  state => state.stockReceive.items
);

const selectIsItemsLoading = createSelector(
  selectInventory,
  state => state.stockReceive.isItemsLoading
);

const selectIsItemsLoaded = createSelector(
  selectInventory,
  state => state.stockReceive.isItemsLoaded
);

const selectItems = createSelector(
  items,
  itemSelector.selectAll
);

const selectTotalCounts = createSelector(
  selectInventory,
  state => state.stockReceive.totalCounts
);

const selectIsItemsTotalCountLoading = createSelector(
  selectInventory,
  state => state.stockReceive.isItemsTotalCountLoading
);

const selectisItemsTotalCountLoaded = createSelector(
  selectInventory,
  state => state.stockReceive.isItemsTotalCountLoaded
);

const selectConfirmedStock = createSelector(
  selectInventory,
  state => state.stockReceive.confirmedStock
);

const selectIsConfirmStockReceiveSuccess = createSelector(
  selectInventory,
  state => state.stockReceive.isConfirmStockReceiveSuccess
);

const selectIsConfirmingStockReceive = createSelector(
  selectInventory,
  state => state.stockReceive.isConfirmingStockReceive
);

const selectIsVerifyingAllItem = createSelector(
  selectInventory,
  state => state.stockReceive.isVerifyingAllItem
);

const selectIsVerifyingAllItemSuccess = createSelector(
  selectInventory,
  state => state.stockReceive.isVerifyingAllItemSuccess
);

const selectIsAssigningBinToAllItems = createSelector(
  selectInventory,
  state => state.stockReceive.isAssigningBinToAllItems
);

const selectIsAssigningBinToAllItemsSuccess = createSelector(
  selectInventory,
  state => state.stockReceive.isAssigningBinToAllItemsSuccess
);

const selectBinCodes = createSelector(
  selectInventory,
  state => state.stockReceive.binCodes
);

const selectRemarks = createSelector(
  selectInventory,
  state => state.stockReceive.remarks
);

const selectTolerance = createSelector(
  selectInventory,
  state => state.stockReceive.tolerance
);

const selectVerifyItemSuccess = createSelector(
  selectInventory,
  state => state.stockReceive.verifyItemSuccess
);

const selectUpdateItemSuccess = createSelector(
  selectInventory,
  state => state.stockReceive.updateItemSuccess
);

const selectProductGroups = createSelector(
  selectInventory,
  state => state.stockReceive.productGroups
);

const selectIsLoadingProductGroups = createSelector(
  selectInventory,
  state => state.stockReceive.isLoadingProductGroups
);

const selectProductCategories = createSelector(
  selectInventory,
  state => state.stockReceive.productCategories
);

const selectIsLoadingProductCategories = createSelector(
  selectInventory,
  state => state.stockReceive.isLoadingProductCategories
);

export const StockReceiveSelectors = {
  selectPendingFactorySTN,
  selectError,
  selectPendingBoutiqueSTN,
  selectPendingCFAInvoice,
  selectIsLoadingPendingFactorySTN,
  selectIsLoadingPendingBoutiqueSTN,
  selectIsLoadingPendingCFAInvoice,
  selectIsSearchingStocks,
  selectHasSearchStockResults,
  selectIsSearchingInvoices,
  selectHasSearchInvoiceResults,
  selectSelectedStock,
  selectIsLoadingSelectedStock,
  selectSearchStockResults,
  selectSearchInvoiceResults,
  selectSelectedInvoice,
  selectItems,
  selectItemsCount,
  selectIsItemsLoading,
  selectVerifyItemSuccess,
  selectUpdateItemSuccess,
  selectBinCodes,
  selectRemarks,
  selectTolerance,
  selectIsLoadingBinGroups,
  selectIsLoadingRemarks,
  selectIsLoadingTolerance,
  selectTotalCounts,
  selectIsItemsTotalCountLoading,
  selectisItemsTotalCountLoaded,
  selectIsVerifyingAllItem,
  selectIsVerifyingAllItemSuccess,
  selectIsAssigningBinToAllItems,
  selectIsAssigningBinToAllItemsSuccess,
  selectConfirmedStock,
  selectIsConfirmStockReceiveSuccess,
  selectIsConfirmingStockReceive,
  selectProductGroups,
  selectIsLoadingProductGroups,
  selectIsItemsLoaded,
  selectProductCategories,
  selectIsLoadingProductCategories
};
