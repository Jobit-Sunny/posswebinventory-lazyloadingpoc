import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { State } from '../../../inventory.state';

import * as StockReceiveActions from './stock-receive.actions';
import { StockReceiveSelectors } from './stock-receive.selectors';
import {
  SearchPendingPayload,
  LoadPendingPayload,
  ConfirmStockReceivePayload,
  UpdateItemPayload,
  LoadItemsPayload,
  LoadItemsTotalCountPayload,
  UpdateAllItemsPayload
} from './stock-receive.actions';
import { ItemValidate } from '../models/stock-receive.model';

/**
 * Stock Receive Facade for accesing Stock-Receive-State
 * */
@Injectable()
export class StockReceiveFacade {
  private pendingFactorySTN$ = this.store.select(
    StockReceiveSelectors.selectPendingFactorySTN
  );
  private pendingBoutiqueSTN$ = this.store.select(
    StockReceiveSelectors.selectPendingBoutiqueSTN
  );

  private pendingCFAInvoice$ = this.store.select(
    StockReceiveSelectors.selectPendingCFAInvoice
  );
  private selectedStock$ = this.store.select(
    StockReceiveSelectors.selectSelectedStock
  );
  private selectedInvoice$ = this.store.select(
    StockReceiveSelectors.selectSelectedInvoice
  );

  private error$ = this.store.select(StockReceiveSelectors.selectError);

  private isLoadingPendingFactorySTN$ = this.store.select(
    StockReceiveSelectors.selectIsLoadingPendingFactorySTN
  );
  private isLoadingPendingBoutiqueSTN$ = this.store.select(
    StockReceiveSelectors.selectIsLoadingPendingBoutiqueSTN
  );

  private isLoadingPendingCFAInvoice$ = this.store.select(
    StockReceiveSelectors.selectIsLoadingPendingCFAInvoice
  );

  private isSearchingStocks$ = this.store.select(
    StockReceiveSelectors.selectIsSearchingStocks
  );
  private hasSearchStockResults$ = this.store.select(
    StockReceiveSelectors.selectHasSearchStockResults
  );
  private isSearchingInvoices$ = this.store.select(
    StockReceiveSelectors.selectIsSearchingInvoices
  );
  private hasSearchInvoiceResults$ = this.store.select(
    StockReceiveSelectors.selectHasSearchInvoiceResults
  );

  private searchStockResults$ = this.store.select(
    StockReceiveSelectors.selectSearchStockResults
  );
  private searchInvoiceResults$ = this.store.select(
    StockReceiveSelectors.selectSearchInvoiceResults
  );
  private totalCounts$ = this.store.select(
    StockReceiveSelectors.selectTotalCounts
  );
  private isItemsTotalCountLoading$ = this.store.select(
    StockReceiveSelectors.selectIsItemsTotalCountLoading
  );
  private isLoadingSelectedStock$ = this.store.select(
    StockReceiveSelectors.selectIsLoadingSelectedStock
  );
  private itemsTotalCountLoaded$ = this.store.select(
    StockReceiveSelectors.selectisItemsTotalCountLoaded
  );
  private items$ = this.store.select(StockReceiveSelectors.selectItems);
  private itemsLoading$ = this.store.select(
    StockReceiveSelectors.selectIsItemsLoading
  );

  private confirmedStock$ = this.store.select(
    StockReceiveSelectors.selectConfirmedStock
  );

  private isConfirmStockReceiveSuccess$ = this.store.select(
    StockReceiveSelectors.selectIsConfirmStockReceiveSuccess
  );
  private isConfirmingStockReceive$ = this.store.select(
    StockReceiveSelectors.selectIsConfirmingStockReceive
  );

  private verifyItemSuccess$ = this.store.select(
    StockReceiveSelectors.selectVerifyItemSuccess
  );

  private updateItemSuccess$ = this.store.select(
    StockReceiveSelectors.selectUpdateItemSuccess
  );

  private isItemsLoaded$ = this.store.select(
    StockReceiveSelectors.selectIsItemsLoaded
  );

  private itemsCount$ = this.store.select(
    StockReceiveSelectors.selectItemsCount
  );

  private isVerifyingAllItem$ = this.store.select(
    StockReceiveSelectors.selectIsVerifyingAllItem
  );

  private isVerifyingAllItemSuccess$ = this.store.select(
    StockReceiveSelectors.selectIsVerifyingAllItemSuccess
  );

  private isAssigningBinToAllItems$ = this.store.select(
    StockReceiveSelectors.selectIsAssigningBinToAllItems
  );

  private isAssigningBinToAllItemsSuccess$ = this.store.select(
    StockReceiveSelectors.selectIsAssigningBinToAllItemsSuccess
  );

  private binCodes$ = this.store.select(StockReceiveSelectors.selectBinCodes);
  private remarks$ = this.store.select(StockReceiveSelectors.selectRemarks);
  private tolerance$ = this.store.select(StockReceiveSelectors.selectTolerance);

  private isLoadingBinGroups$ = this.store.select(
    StockReceiveSelectors.selectIsLoadingBinGroups
  );
  private isLoadingRemarks$ = this.store.select(
    StockReceiveSelectors.selectIsLoadingRemarks
  );
  private isLoadingTolerance$ = this.store.select(
    StockReceiveSelectors.selectIsLoadingTolerance
  );

  private productGroups$ = this.store.select(
    StockReceiveSelectors.selectProductGroups
  );

  private isLoadingProductGroups$ = this.store.select(
    StockReceiveSelectors.selectIsLoadingProductGroups
  );

  private productCategories$ = this.store.select(
    StockReceiveSelectors.selectProductCategories
  );

  private isLoadingProductCategories$ = this.store.select(
    StockReceiveSelectors.selectIsLoadingProductCategories
  );

  constructor(private store: Store<State>) {}

  /**
   * Access for the State selectors
   */
  getPendingFactorySTN() {
    return this.pendingFactorySTN$;
  }

  getPendingBoutiqueSTN() {
    return this.pendingBoutiqueSTN$;
  }

  getPendingCFAInvoice() {
    return this.pendingCFAInvoice$;
  }

  getSearchStockResults() {
    return this.searchStockResults$;
  }

  getSearchInvoiceResults() {
    return this.searchInvoiceResults$;
  }

  getIsLoadingPendingFactorySTN() {
    return this.isLoadingPendingFactorySTN$;
  }

  getIsLoadingPendingBoutiqueSTN() {
    return this.isLoadingPendingBoutiqueSTN$;
  }

  getIsLoadingPendingCFAInvoice() {
    return this.isLoadingPendingCFAInvoice$;
  }

  getIsSearchingStocks() {
    return this.isSearchingStocks$;
  }
  getHasSearchStockResults() {
    return this.hasSearchStockResults$;
  }
  getIsSearchingInvoices() {
    return this.isSearchingInvoices$;
  }
  getHasSearchInvoiceResults() {
    return this.hasSearchInvoiceResults$;
  }

  getError() {
    return this.error$;
  }

  getIsLoadingSelectedStock() {
    return this.isLoadingSelectedStock$;
  }

  getSelectedStock() {
    return this.selectedStock$;
  }

  getSelectedInvoice() {
    return this.selectedInvoice$;
  }

  getTotalCounts() {
    return this.totalCounts$;
  }

  getIsItemsTotalCountLoading() {
    return this.isItemsTotalCountLoading$;
  }

  getItemsTotalCountLoaded() {
    return this.itemsTotalCountLoaded$;
  }

  getItems() {
    return this.items$;
  }

  getIsItemsLoading() {
    return this.itemsLoading$;
  }

  getVerifyItemSuccess() {
    return this.verifyItemSuccess$;
  }
  getUpdateItemSuccess() {
    return this.updateItemSuccess$;
  }

  getItemsCount() {
    return this.itemsCount$;
  }

  getBinCodes() {
    return this.binCodes$;
  }

  getRemarks() {
    return this.remarks$;
  }

  getTolerance() {
    return this.tolerance$;
  }

  getIsItemsLoaded() {
    return this.isItemsLoaded$;
  }

  getIsLoadingBinGroups() {
    return this.isLoadingBinGroups$;
  }

  getIsLoadingRemarks() {
    return this.isLoadingRemarks$;
  }

  getIsLoadingTolerance() {
    return this.isLoadingTolerance$;
  }

  getProductGroups() {
    return this.productGroups$;
  }
  getIsLoadingProductGroups() {
    return this.isLoadingProductGroups$;
  }

  getProductCategories() {
    return this.productCategories$;
  }
  getIsLoadingProductCategories() {
    return this.isLoadingProductCategories$;
  }

  getConfirmedStock() {
    return this.confirmedStock$;
  }

  getIsConfirmStockReceiveSuccess() {
    return this.isConfirmStockReceiveSuccess$;
  }

  getIsConfirmingStockReceive() {
    return this.isConfirmingStockReceive$;
  }

  getIsVerifyingAllItem() {
    return this.isVerifyingAllItem$;
  }

  getIsVerifyingAllItemSuccess() {
    return this.isVerifyingAllItemSuccess$;
  }

  getIsAssigningBinToAllItems() {
    return this.isAssigningBinToAllItems$;
  }

  getIsAssigningBinToAllItemsSuccess() {
    return this.isAssigningBinToAllItemsSuccess$;
  }

  /**
   * Dispatch Action for loading STN from Factory (L1/L2)
   * @param loadPendingSTNPayload payload with transfer type , pageIndex and pageSize
   */
  loadPendingFactorySTN(loadPendingPayload: LoadPendingPayload) {
    this.store.dispatch(
      new StockReceiveActions.LoadPendingFactorySTN(loadPendingPayload)
    );
  }

  /**
   * Dispatch Action for loading STN from other Boutiques (L1/L2)
   * @param loadPendingSTNPayload payload with transfer type , pageIndex and pageSize
   */
  loadPendingBoutiqueSTN(loadPendingPayload: LoadPendingPayload) {
    this.store.dispatch(
      new StockReceiveActions.LoadPendingBoutiqueSTN(loadPendingPayload)
    );
  }

  clearPendingBoutiqueSTN() {
    this.store.dispatch(new StockReceiveActions.ClearPendingBoutiqueSTN());
  }

  /**
   * Dispatch Action for loading invoice from cfa (L3)
   * @param loadPendingSTNPayload payload with transfer type , pageIndex and pageSize
   */
  loadPendingCFAInvoice(loadPendingPayload: LoadPendingPayload) {
    this.store.dispatch(
      new StockReceiveActions.LoadPendingCFAInvoice(loadPendingPayload)
    );
  }

  /**
   * Dispatch Action for search based on srcDoc number and transfer type (L1/L2)
   * @param searchPendingpayload payload with srcDoc number and transfer type
   */
  searchPendingStocks(searchPendingpayload: SearchPendingPayload) {
    this.store.dispatch(
      new StockReceiveActions.SearchPendingStocks(searchPendingpayload)
    );
  }

  /**
   * Dispatch Action for search based on srcDoc number and transfer type (L3)
   * @param searchpendingpayload payload with srcDoc number and transfer type
   */
  searchPendingInvoices(searchPendingpayload: SearchPendingPayload) {
    this.store.dispatch(
      new StockReceiveActions.SearchPendingInvoices(searchPendingpayload)
    );
  }

  /**
   * Dispatch Action for Clearing search results
   */
  searchClear() {
    this.store.dispatch(new StockReceiveActions.SearchClear());
  }

  /**
   * Dispatch Action for getting selected Stocks  (L1/L2)
   */
  loadSelectedStock(data: { id: string; type: string }) {
    this.store.dispatch(new StockReceiveActions.LoadSelectedStock(data));
  }

  /**
   * Dispatch Action for getting selected Invoice (L3)
   */
  loadSelectedInvocie(data: { id: string; type: string }) {
    this.store.dispatch(new StockReceiveActions.LoadSelectedInvoice(data));
  }

  // for verify items

  loadItemsTotalCount(loadItemsTotalCountPayload: LoadItemsTotalCountPayload) {
    this.store.dispatch(
      new StockReceiveActions.LoadItemsTotalCount(loadItemsTotalCountPayload)
    );
  }

  loadItems(loadItemsPayload: LoadItemsPayload) {
    this.store.dispatch(new StockReceiveActions.LoadItems(loadItemsPayload));
  }

  loadBinCodes(binGroupCode: string) {
    this.store.dispatch(new StockReceiveActions.LoadBinCodes(binGroupCode));
  }

  loadRemarks() {
    this.store.dispatch(new StockReceiveActions.LoadRemarks());
  }

  verifyItem(updateItemPayload: UpdateItemPayload) {
    this.store.dispatch(new StockReceiveActions.VerifyItem(updateItemPayload));
  }

  updateItem(updateItemPayload: UpdateItemPayload) {
    this.store.dispatch(new StockReceiveActions.UpdateItem(updateItemPayload));
  }

  validateItem(itemValidate: ItemValidate) {
    this.store.dispatch(new StockReceiveActions.ValidateItem(itemValidate));
  }

  loadTolerance() {
    this.store.dispatch(new StockReceiveActions.LoadTolerance());
  }

  resetError() {
    this.store.dispatch(new StockReceiveActions.ResetError());
  }

  clearStocks() {
    this.store.dispatch(new StockReceiveActions.ClearStocks());
  }

  confirmStock(confirmStockReceivePayload: ConfirmStockReceivePayload) {
    this.store.dispatch(
      new StockReceiveActions.ConfirmStockReceive(confirmStockReceivePayload)
    );
  }

  verifyAllItems(updateAllItemsPayload: UpdateAllItemsPayload) {
    this.store.dispatch(
      new StockReceiveActions.VerifyAllItems(updateAllItemsPayload)
    );
  }

  assignBinToAllItems(updateAllItemsPayload: UpdateAllItemsPayload) {
    this.store.dispatch(
      new StockReceiveActions.AssignBinToAllItems(updateAllItemsPayload)
    );
  }

  loadProductGroups() {
    this.store.dispatch(new StockReceiveActions.LoadProductGroups());
  }

  loadProductCategories() {
    this.store.dispatch(new StockReceiveActions.LoadProductCategories());
  }

  clearItems() {
    this.store.dispatch(new StockReceiveActions.ClearItems());
  }
}
