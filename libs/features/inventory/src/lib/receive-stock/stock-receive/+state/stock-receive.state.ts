import { ItemEntity, StockEntity } from './stock-receive.entity';
import { Stock } from '../models/stock-receive.model';
import {
  CustomErrors,
  Lov,
  BinCode,
  ProductGroup,
  ProductCategory
} from '@poss-web/core';

/**
 * The interface for Stock Receive State
 */

export interface StockReceiveState {
  pendingFactorySTN: StockEntity;
  isLoadingPendingFactorySTN: boolean;
  pendingBoutiqueSTN: StockEntity;
  isLoadingPendingBoutiqueSTN: boolean;
  pendingCFAInvoice: StockEntity;
  isLoadingPendingCFAInvoice: boolean;
  searchStockResults: StockEntity;
  isSearchingStocks: boolean;
  hasSearchStockResults: boolean;
  searchInvoiceResults: StockEntity;
  isSearchingInvoices: boolean;
  hasSearchInvoiceResults: boolean;

  selectedStock: Stock;
  selectedInvoice: Stock;
  isLoadingSelectedStock: boolean;
  isItemsTotalCountLoading: boolean;
  isItemsTotalCountLoaded: boolean;
  items: ItemEntity;
  isItemsLoading: boolean;
  isItemsLoaded: boolean;
  itemsCount: number;
  totalCounts: {
    nonVerifiedItemsTotalCount: number;
    verifiedItemsTotalCount: number;
    isLoaded: boolean;
  };
  binCodes: BinCode[];
  isLoadingBinGroups: boolean;
  remarks: Lov[];
  isLoadingRemarks: boolean;
  tolerance: number;
  isLoadingTolerance: boolean;
  verifyItemSuccess: boolean;
  updateItemSuccess: boolean;
  isVerifyingAllItem: boolean;
  isVerifyingAllItemSuccess: boolean;
  isAssigningBinToAllItems: boolean;
  isAssigningBinToAllItemsSuccess: boolean;
  confirmedStock: any;
  isConfirmStockReceiveSuccess: boolean;
  isConfirmingStockReceive: boolean;

  productGroups: ProductGroup[];
  isLoadingProductGroups: boolean;

  productCategories: ProductCategory[];
  isLoadingProductCategories: boolean;
  error: CustomErrors;
}
