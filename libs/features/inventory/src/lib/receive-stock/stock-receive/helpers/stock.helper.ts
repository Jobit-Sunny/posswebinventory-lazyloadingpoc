import { Stock } from '../models/stock-receive.model';
import { StockAdaptor } from '../adaptors/stock.adaptor';

export class StockHelper {
  static getStocks(data: any, typeField: string): Stock[] {
    const stocks: Stock[] = [];
    for (const stock of data) {
      stocks.push(StockAdaptor.fromJson(stock, typeField));
    }
    return stocks;
  }
}
