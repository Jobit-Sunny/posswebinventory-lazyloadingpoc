import { Item } from '../models/stock-receive.model';
import { ItemAdaptor } from '../adaptors/item.adaptor';

export class ItemHelper {
  static getItems(data: any): { items: Item[]; count: number } {
    const items: Item[] = [];
    for (const item of data.results) {
      items.push(ItemAdaptor.fromJson(item));
    }
    return { items, count: data.totalElements };
  }
}
