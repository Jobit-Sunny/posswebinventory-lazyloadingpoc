import { Stock } from '../models/stock-receive.model';
import * as moment from 'moment';

export class StockAdaptor {
  static fromJson(data: any, typeField: string): Stock {
    if (!data) {
      return null;
    }
    const stock: Stock = {
      id: data.id,
      srcDocNo: data.srcDocNo,
      srcLocationCode: data.srcLocationCode,
      type: data[typeField],
      courierDetails: data.courierDetails
        ? {
            type: data.courierDetails['type'],
            data: data.courierDetails['data']
          }
        : {
            type: null,
            data: null
          },
      orderType: data.orderType,
      courierReceivedDate: moment(data.courierReceivedDate),
      totalAvailableValue: data.totalAvailableValue
        ? data.totalAvailableValue
        : 0,
      totalAvailableWeight: data.totalAvailableWeight
        ? data.totalAvailableWeight
        : 0,
      totalAvailableQuantity: data.totalAvailableQuantity
        ? data.totalAvailableQuantity
        : 0,
      totalMeasuredQuantity: data.totalMeasuredQuantity
        ? data.totalAvailableQuantity
        : 0,
      totalMeasuredValue: data.totalMeasuredValue
        ? data.totalAvailableQuantity
        : 0,
      totalMeasuredWeight: data.totalMeasuredWeight
        ? data.totalAvailableQuantity
        : 0,

      srcDocDate: moment(data.srcDocDate),
      currencyCode: data.currencyCode,
      weightUnit: data.weightUnit,
      status: data.status,
      srcFiscalYear: data.srcFiscalYear,
      destDocDate: moment(data.destDocDate),
      destDocNo: data.destDocNo,
      destLocationCode: data.destLocationCode
    };
    return stock;
  }
}
