import { FilterOption } from '../models/stock-receive.model';

export class FilterOptionAdaptor {
  static fromJson(
    data: any,
    idField: string,
    descriptionField: string = 'description'
  ): FilterOption {
    if (!data) {
      return null;
    }
    const filterOption: FilterOption = {
      id: data[idField],
      description: data[descriptionField]
    };
    return filterOption;
  }
}
