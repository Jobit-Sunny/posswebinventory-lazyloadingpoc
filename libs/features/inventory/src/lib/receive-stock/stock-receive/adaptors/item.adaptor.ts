import { Item } from '../models/stock-receive.model';
import * as moment from 'moment';

export class ItemAdaptor {
  static fromJson(item: any): Item {
    return {
      id: item.id,
      binCode: item.binCode,
      itemCode: item.itemCode,
      itemDetails: item.itemDetails,
      stdValue: item.stdValue ? item.stdValue : 0,
      lotNumber: item.lotNumber,
      mfgDate: item.mfgDate ? moment(item.mfgDate) : null,
      status: item.status,
      availableValue: item.availableValue ? item.availableValue : 0,
      availableWeight:
        item.availableWeight && !isNaN(item.availableWeight)
          ? +Number.parseFloat(item.availableWeight).toFixed(3)
          : 0,
      currencyCode: item.currencyCode,
      weightUnit: item.weightUnit,
      imageURL: item.imageURL,
      measuredQuantity: item.measuredQuantity ? item.measuredQuantity : 0,
      measuredWeight:
        item.measuredWeight && !isNaN(item.measuredWeight)
          ? +Number.parseFloat(item.measuredWeight).toFixed(3)
          : 0,
      binGroupCode: item.binGroupCode,
      availableQuantity: item.availableQuantity ? item.availableQuantity : 0,
      orderType: item.orderType,
      productCategory: item.productCategory,
      productGroup: item.productGroup,
      remarks: item.remarks,
      isUpdating: false,
      isUpdatingSuccess: null,
      isValidating: false,
      isValidatingSuccess: null,
      isValidatingError: false
    };
  }
}
