import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StockReceiveListComponent } from './stock-receive/containers/stock-receive-list/stock-receive-list.component';
import { AuthGuard } from '@poss-web/core';
import { StockReceiveDetailsComponent } from './stock-receive/containers/stock-receive-details/stock-receive-details.component';

const routes: Routes = [
  {
    path: ':type',
    component: StockReceiveListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: ':type/:id/:tab',
    component: StockReceiveDetailsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: ':type/:id',
    component: StockReceiveDetailsComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiveStockRoutingModule {}
