import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReceiveStockRoutingModule } from './receive-stock-routing.module';
import { MatSidenavModule } from '@angular/material';
import { UicomponentsModule, SharedModule } from '@poss-web/shared';
import { StockReceiveEffect } from './stock-receive/+state/stock-receive.effect';
import { EffectsModule } from '@ngrx/effects';
import { StockReceiveListComponent } from './stock-receive/containers/stock-receive-list/stock-receive-list.component';
import { StockReceiveItemComponent } from './stock-receive/components/stock-receive-item/stock-receive-item.component';
import { StockReceiveItemListComponent } from './stock-receive/components/stock-receive-item-list/stock-receive-item-list.component';
import { StockReceiveDetailsComponent } from './stock-receive/containers/stock-receive-details/stock-receive-details.component';
import { StockReceiveService } from './stock-receive/services/stock-receive.service';
import { StockReceiveFacade } from './stock-receive/+state/stock-receive.facade';
import { StockReceiveReasonForDelayPopupComponent } from './stock-receive/components/stock-receive-reason-for-delay-popup/stock-receive-reason-for-delay-popup.component';

@NgModule({
  declarations: [
    StockReceiveListComponent,
    StockReceiveItemListComponent,
    StockReceiveDetailsComponent,
    StockReceiveItemComponent,
    StockReceiveReasonForDelayPopupComponent
  ],
  imports: [
    CommonModule,
    ReceiveStockRoutingModule,
    SharedModule,
    UicomponentsModule,
    MatSidenavModule,
    EffectsModule.forFeature([StockReceiveEffect])
  ],
  providers: [StockReceiveService, StockReceiveFacade],
  entryComponents: [StockReceiveReasonForDelayPopupComponent]
})
export class ReceiveStockModule {}
