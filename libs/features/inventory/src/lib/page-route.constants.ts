export const getNotFoundRouteUrl = (): string => {
  return `404`;
};

/**
 * Inventory Routing Urls
 */

export const getInventoryRouteUrl = (): string => {
  return `inventory`;
};

export const getStockReceiveRouteUrl = (stockReceivePath: string): string => {
  return getInventoryRouteUrl() + `/stockreceive/${stockReceivePath}`;
};

export const getInStockRouteUrl = (): string => {
  return getInventoryRouteUrl() + `/instock`;
};

export const getStockIssueRouteUrl = (stockIssuePath: string): string => {
  return getInventoryRouteUrl() + `/stockissue/${stockIssuePath}`;
};

export const getBintoBinTransferRouteUrl = (transferType: string): string => {
  return getInStockRouteUrl() + `/bintobinTransfer/${transferType}`;
};

export const getInterBoutiqueTransferRouteUrl = (
  requestType: string
): string => {
  return getInStockRouteUrl() + `/inter-boutique-transfer/` + `${requestType}`;
};

export const getConversionRouteUrl = (conversionType: string): string => {
  return getInStockRouteUrl() + `/conversion/${conversionType}`;
};

export const getOtherReceiptsIssuesListRouteUrl = (
  type: string,
  receiptIssueType: string
): string => {
  return (
    getInStockRouteUrl() +
    `/other-receipts-issues-list/${type}/${receiptIssueType}`
  );
};

export const getOtherReceiptsRouteUrl = (receiptType: string): string => {
  return getInStockRouteUrl() + `/otherreceipts/${receiptType}`;
};

export const getOtherIssuesRouteUrl = (issueType: string): string => {
  return getInStockRouteUrl() + `/otherissues/${issueType}`;
};

export const getOtherIssuesCreateRouteUrl = (issueType: string): string => {
  return getInStockRouteUrl() + `/create/otherissues/${issueType}`;
};

export const getStockIssueTEPGEPRouteUrl = (
  type: string,
  transferType: string
): string => {
  return getStockIssueRouteUrl(type) + `/tep-gep/${transferType}`;
};

export const getIBTCreateRequestRouteUrl = (): string => {
  return getInStockRouteUrl() + `/inter-boutique-transfer/create-request`;
};

export const getApprovalsRouteUrl = (requestType: string): string => {
  return getInventoryRouteUrl() + `/approvals/${requestType}`;
};

export const getOtherReceiptsIssuesRouteUrl = (): string => {
  return getInStockRouteUrl() + `/other-receipts-issues-list/`;
};
