import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InStockManagementRoutingModule } from './in-stock-management-routing.module';
import { MatSidenavModule } from '@angular/material';
import { SharedModule, UicomponentsModule } from '@poss-web/shared';
import { EffectsModule } from '@ngrx/effects';
import { InStockEffects } from './in-stock/+state/in-stock.effect';
import { InStockFacade } from './in-stock/+state/in-stock.facade';
import { BinRequestPopupComponent } from './in-stock/bin-request-popup/bin-request-popup.component';
import { BinRequestSuccessPopupComponent } from './in-stock/bin-request-success-popup/bin-request-success-popup.component';
import { InStockComponent } from './in-stock/in-stock.component';

@NgModule({
  declarations: [
    InStockComponent,
    BinRequestPopupComponent,
    BinRequestSuccessPopupComponent
  ],
  imports: [
    CommonModule,
    InStockManagementRoutingModule,
    SharedModule,
    UicomponentsModule,
    MatSidenavModule,
    EffectsModule.forFeature([InStockEffects])
  ],
  providers: [InStockFacade],
  entryComponents: [BinRequestPopupComponent, BinRequestSuccessPopupComponent]
})
export class InStockManagementModule {}
