import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Subject } from 'rxjs';
import { InStockFacade } from '../+state/in-stock.facade';
import { AppsettingFacade } from '@poss-web/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { BinRequestResponse } from '../models/bin.model';

@Component({
  selector: 'poss-web-bin-request-success-popup',
  templateUrl: './bin-request-success-popup.component.html',
  styleUrls: ['./bin-request-success-popup.component.scss']
})
export class BinRequestSuccessPopupComponent implements OnInit, OnDestroy {
  destroy$: Subject<null> = new Subject<null>();


  ngOnInit() {

  }
  constructor(private inStockFacade: InStockFacade,
    private appsettingFacade: AppsettingFacade, public dialogRef: MatDialogRef<BinRequestSuccessPopupComponent>, @Inject(MAT_DIALOG_DATA) public data: BinRequestResponse, public dialog: MatDialog) {
    console.log(data.reqDocNo);
  }

  onClose(): void {
    this.inStockFacade.resetDocNo();
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }



}

