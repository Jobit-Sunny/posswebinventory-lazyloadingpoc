import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { InStockSelector } from './in-stock.selectors';
import { State } from '../../../inventory.state';
import * as inStockActions from './in-stock.action';
import { LoadBinCodesPayload } from './in-stock.action';
import { BinRequestResponse, BinRequestDto } from '../models/bin.model';

@Injectable()
export class InStockFacade {
  private boutiqueStatistics$ = this.store.select(
    InStockSelector.getBoutiqueStatistics
  );

  private binCodes$ = this.store.select(
    InStockSelector.selectBinCodes
  );

  private hasError$ = this.store.select(InStockSelector.selectHasError);
  private isLoaded$ = this.store.select(InStockSelector.selectIsLoaded);
  private isBinCodeReset$ = this.store.select(InStockSelector.selectIsBinCodeReset);
  private isDocNoReset$ = this.store.select(InStockSelector.selectIsDocNoReset)
  private isLoading$ = this.store.select(InStockSelector.selectIsLoading);
  private binCodesCount$ = this.store.select(
    InStockSelector.selectbinCodesCount
  );
  private isRequestingBin$ = this.store.select(
    InStockSelector.selectIsRequestingBin
  );
  private isRequestBinSuccess$ = this.store.select(
    InStockSelector.selectIsRequestedBinSuccess
  );

  private requestedBin$ = this.store.select(
    InStockSelector.selectRequestBinSuccess
  );

  constructor(private store: Store<State>) { }
  loadboutiqueStatistics() {
    this.store.dispatch(new inStockActions.LoadBoutiqueStatistics())
  }

  loadBinCodes() {
    this.store.dispatch(new inStockActions.LoadBinCodes());
  }

  resetBinCodes() {
    this.store.dispatch(new inStockActions.ResetBinCodes());
  }

  resetDocNo() {

    this.store.dispatch(new inStockActions.ResetDocNo());
  }

  loadbinCodesCount() {
    this.store.dispatch(new inStockActions.LoadCount());
  }

  requestedBin(requestBin: BinRequestDto) {
    this.store.dispatch(new inStockActions.RequestedBin(requestBin));
  }

  getboutiqueStatistics() {
    return this.boutiqueStatistics$;
  }

  getBinCodes() {
    return this.binCodes$;
  }
  getError() {
    return this.hasError$;
  }


  getIsBinCodeReset() {
    return this.isBinCodeReset$;
  }

  getIsDocNoReset() {


    return this.isDocNoReset$;
  }



  getIsLoading() {
    return this.isLoading$;
  }

  getIsLoaded() {
    return this.isLoaded$;
  }

  getbinCodesCount() {
    return this.binCodesCount$;
  }

  getRequestBin() {
    return this.requestedBin$;
  }

  getIsRequestBinSuccess() {
    return this.isRequestBinSuccess$;
  }

  getIsRequestingBin() {
    return this.isRequestingBin$;
  }


}
