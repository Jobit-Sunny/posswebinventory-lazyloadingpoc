
import { BoutiqueStatistics } from '../models/in-stock.model';

import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';

import { BinCodes, BinRequestDto, BinRequestResponse } from '../models/bin.model';
import { CustomErrors } from '@poss-web/core';


export interface LoadBinCodesPayload {

  pageIndex: number;
  pageSize: number;
}






export enum InStockActionTypes {


  LoadBoutiqueStatistics = '[BoutiqueStatistics] Load BoutiqueStatistics',
  LoadBoutiqueStatisticsSuccess = '[BoutiqueStatistics] Load BoutiqueStatistics Success',
  LoadBoutiqueStatisticsError = '[BoutiqueStatistics] Load BoutiqueStatistics Error',

  LOAD_BINCODES = '[BinCodes] Load BinCode',
  LOAD_BINCODES_SUCCESS = '[BinCodes] Load BinCode Success',
  LOAD_BINCODES_FAILURE = '[BinCodes] Load BinCode Failure',

  RESET_BINCODES = '[ BinCodes ] Reset BinCodes',
  RESET_DOCUMENT_NO = '[ BinCodes ] Reset Document Number',
  RESET_ERROR = '[BinCodes ] Reset Error',

  LOAD_COUNT = '[BinCodes ] Load Count',
  LOAD_COUNT_SUCCESS = '[BinCodes ] Load Count Success',
  LOAD_COUNT_FAILURE = '[BinCodes ] Load Count Failure',

  REQUESTED_BIN = '[ BinCodes ] Requested Bin',
  REQUESTED_BIN_SUCCESS = '[ BinCodes] Requested Bin success',
  REQUESTED_BIN_FAILURE = '[ BinCodes ] Requested Bin Failure',
}

export class LoadBoutiqueStatistics implements Action {
  readonly type = InStockActionTypes.LoadBoutiqueStatistics;
}

export class LoadBoutiqueStatisticsSuccess implements Action {
  readonly type = InStockActionTypes.LoadBoutiqueStatisticsSuccess;
  constructor(public payload: BoutiqueStatistics) { }
}

export class LoadBoutiqueStatisticsError implements Action {
  readonly type = InStockActionTypes.LoadBoutiqueStatisticsError;
  // Notes that `error` has to be like this, cannot be wrapped inside a payload
  constructor(public payload: CustomErrors) { }
}

export class LoadBinCodes implements Action {
  readonly type = InStockActionTypes.LOAD_BINCODES;
  constructor() { }
}

export class LoadBinCodesSuccess implements Action {
  readonly type = InStockActionTypes.LOAD_BINCODES_SUCCESS;
  constructor(public payload: BinCodes[]) { }
}

export class LoadBinCodesFailure implements Action {
  readonly type = InStockActionTypes.LOAD_BINCODES_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class ResetBinCodes implements Action {
  readonly type = InStockActionTypes.RESET_BINCODES;
}

export class ResetDocNo implements Action {
  readonly type = InStockActionTypes.RESET_DOCUMENT_NO;

}

export class LoadCount implements Action {
  readonly type = InStockActionTypes.LOAD_COUNT;
}

export class LoadCountSuccess implements Action {
  readonly type = InStockActionTypes.LOAD_COUNT_SUCCESS;
  constructor(public payload: any) { }
}


export class LoadCountFailure implements Action {
  readonly type = InStockActionTypes.LOAD_COUNT_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class RequestedBin implements Action {
  readonly type = InStockActionTypes.REQUESTED_BIN;
  constructor(public payload: BinRequestDto) { }
}
export class RequestedBinSuccess implements Action {
  readonly type = InStockActionTypes.REQUESTED_BIN_SUCCESS;
  constructor(public payload: BinRequestResponse) { }
}
export class RequestedBinFailure implements Action {
  readonly type = InStockActionTypes.REQUESTED_BIN_FAILURE;
  constructor(public payload: CustomErrors) { }
}

export class ResetError implements Action {
  readonly type = InStockActionTypes.RESET_ERROR;
}


export type InStockAction = LoadBoutiqueStatistics | LoadBoutiqueStatisticsSuccess | LoadBoutiqueStatisticsError | LoadBinCodes | LoadBinCodesSuccess | LoadBinCodesFailure | ResetBinCodes | ResetDocNo | LoadCount | LoadCountSuccess | LoadCountFailure | RequestedBin | RequestedBinSuccess | RequestedBinFailure;


