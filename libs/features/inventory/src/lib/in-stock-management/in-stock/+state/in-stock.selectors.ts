import { createSelector } from '@ngrx/store';
import { selectInventory } from '../../../inventory.state';

export const getBoutiqueStatistics = createSelector(
  selectInventory,
  state => state.instock.boutiqueStatistics
);

const selectBinCodes = createSelector(
  selectInventory,
  state => state.instock.binCodes
);



const selectHasError = createSelector(
  selectInventory,
  state => state.instock.error
);

const selectIsLoaded = createSelector(
  selectInventory,
  state => state.instock.loaded
);

const selectIsLoading = createSelector(
  selectInventory,
  state => state.instock.isLoading
);

const selectDocNo = createSelector(
  selectInventory,
  state => state.instock.docNo
);

const selectIsBinCodeReset = createSelector(
  selectInventory,
  state => state.instock.isBinCodeReset
);

const selectIsDocNoReset = createSelector(
  selectInventory,
  state => state.instock.isDocNoReset
);

const selectbinCodesCount = createSelector(
  selectInventory,
  state => state.instock.binCodeCount
);

const selectIsRequestingBin = createSelector(
  selectInventory,
  state => state.instock.isRequestingBin
);

const selectIsRequestedBinSuccess = createSelector(
  selectInventory,
  state => state.instock.isRequestedBinSuccess
);

const selectRequestBinSuccess = createSelector(
  selectInventory,
  state => state.instock.binRequestResponse
);



export const InStockSelector = {
  getBoutiqueStatistics,
  selectBinCodes,
  selectDocNo,
  selectHasError,
  selectIsBinCodeReset,
  selectIsDocNoReset, selectIsLoaded,
  selectIsLoading,
  selectbinCodesCount,
  selectIsRequestedBinSuccess,
  selectIsRequestingBin,
  selectRequestBinSuccess
};
