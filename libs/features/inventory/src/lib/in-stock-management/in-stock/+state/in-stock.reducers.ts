
import {
  InStockAction,
  InStockActionTypes
} from './in-stock.action';
import { BoutiqueStatisticsState } from './in-stock.state';
import { M } from '@angular/cdk/keycodes';

export const initialBoutiqueStatisticsState: BoutiqueStatisticsState = {
  boutiqueStatistics: {
    totalProducCount: 0,
    totalProductCategories: 0,
    totalBinGroups: 0,
    totalBinCodes: 0,
    // TODO : Check the type
    totalValueOfStock: '0'
  },
  binCodes: [],
  binRequestResponse: null,
  hasRequestedFailure: false,
  docNo: 0,
  error: null,
  loaded: false,
  isLoading: false,
  isBinCodeReset: false,
  isDocNoReset: false,
  binCodeCount: 0,
  isRequestedBinSuccess: false,
  isRequestingBin: false,

};

export function InStockReducer(
  state: BoutiqueStatisticsState = initialBoutiqueStatisticsState,
  action: InStockAction
): BoutiqueStatisticsState {
  switch (action.type) {
    case InStockActionTypes.LoadBoutiqueStatisticsSuccess: {
      return {
        ...state,
        boutiqueStatistics: action.payload,
        loaded: true,
        error: null
      };
    }
    case InStockActionTypes.LoadBoutiqueStatisticsError: {
      return {
        ...state,
        loaded: false,
        error: action.payload
      };
    }
    case InStockActionTypes.LOAD_BINCODES:
      return {
        ...state,
        isLoading: true,
        error: null,
        hasRequestedFailure: null
      };

    case InStockActionTypes.LOAD_BINCODES_SUCCESS:
      return {
        ...state,
        binCodes: action.payload,
        loaded: true,
        isLoading: false,
        error: null,
        hasRequestedFailure: null
      };

    case InStockActionTypes.LOAD_BINCODES_FAILURE:
      return {
        ...state,
        loaded: false,
        error: action.payload,
        isLoading: false
      };
    case InStockActionTypes.RESET_BINCODES:
      return {
        ...state,
        binCodes: [],
        isBinCodeReset: true,
        error: null,
        hasRequestedFailure: null,
        isLoading: false
      };

    case InStockActionTypes.RESET_DOCUMENT_NO:

      return {
        ...state,
        docNo: null,
        binRequestResponse: null,
        isDocNoReset: true,
        error: null,
        hasRequestedFailure: null,
        isLoading: false
      };

    case InStockActionTypes.LOAD_COUNT:
      return {
        ...state,
        error: null,
        hasRequestedFailure: null
      }


    case InStockActionTypes.LOAD_COUNT_SUCCESS:
      return {
        ...state,
        binCodeCount: action.payload,
        error: null,
        hasRequestedFailure: null
      };

    case InStockActionTypes.LOAD_COUNT_FAILURE:
      return {
        ...state,
        error: action.payload,

      };

    case InStockActionTypes.REQUESTED_BIN:
      return {
        ...state,
        isLoading: true,
        error: null,
        hasRequestedFailure: null,
        docNo: null,
        isRequestedBinSuccess: null,
        isRequestingBin: true,
      };

    case InStockActionTypes.REQUESTED_BIN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isRequestedBinSuccess: true,
        binRequestResponse: action.payload,
        docNo: null,
        isRequestingBin: false,
        error: null,
        hasRequestedFailure: null

      };

    case InStockActionTypes.REQUESTED_BIN_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
        hasRequestedFailure: true,
        isRequestedBinSuccess: false,
        isRequestingBin: false,
      };




    default:
      return state;
  }
}
