import { BoutiqueStatistics } from '../models/in-stock.model';
import { ActionReducerMap } from '@ngrx/store';
import { InStockReducer } from './in-stock.reducers';
import { BinCodes, BinRequestResponse } from '../models/bin.model';
import { CustomErrors } from '@poss-web/core';
import { InterBoutiqueTransferState } from '../../inter-boutique-transfer/+state/inter-boutique-transfer.state';
import { InterBoutiqueTransferReducer } from '../../inter-boutique-transfer/+state/inter-boutique-transfer.reducer';

export interface BoutiqueStatisticsState {
  boutiqueStatistics: BoutiqueStatistics;
  binCodes: BinCodes[];
  loaded: boolean;
  error: CustomErrors;
  isLoading: boolean;
  docNo: number;
  isBinCodeReset: boolean;
  isDocNoReset: boolean;
  binCodeCount: number;
  hasRequestedFailure: boolean;
  isRequestedBinSuccess: boolean;
  isRequestingBin: boolean;
  binRequestResponse: BinRequestResponse;
}

export interface InStockState {
  boutiqueStatistics: BoutiqueStatisticsState;
  interBoutiqueTransfer: InterBoutiqueTransferState;
}

export const inStockReducers: ActionReducerMap<InStockState> = {
  boutiqueStatistics: InStockReducer,
  interBoutiqueTransfer: InterBoutiqueTransferReducer
};
