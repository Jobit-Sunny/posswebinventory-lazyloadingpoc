import { InventoryState } from '../../../inventory.state';
import { CustomErrorAdaptor } from '@poss-web/core';
import { Action } from '@ngrx/store';
import { Injectable } from '@angular/core';

import { Effect } from '@ngrx/effects';



import { BoutiqueStatistics } from '../models/in-stock.model';


import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { DataPersistence } from '@nrwl/angular';
import { InStockService } from '../in-stock.service';
import { NotificationService, CustomErrors } from '@poss-web/core';
import { HttpErrorResponse } from '@angular/common/http';
import { InStockActionTypes, InStockAction, LoadBoutiqueStatistics, LoadBoutiqueStatisticsSuccess, LoadBoutiqueStatisticsError, LoadBinCodes, LoadBinCodesSuccess, LoadBinCodesFailure, LoadCount, LoadCountSuccess, LoadCountFailure, RequestedBin, RequestedBinSuccess, RequestedBinFailure } from './in-stock.action';
import { BoutiqueStatisticsState } from './in-stock.state';

import { BinCodes } from '../models/bin.model';

@Injectable()
export class InStockEffects {
  @Effect()
  GetBoutiqueStatistics$ = this.dataPersistence.fetch(
    InStockActionTypes.LoadBoutiqueStatistics,
    {
      run: () => {
        return this.service
          .getBoutiqueStatistics()
          .pipe(
            map(
              (boutiqueStatistics: BoutiqueStatistics) =>
                new LoadBoutiqueStatisticsSuccess(boutiqueStatistics)
            )
          );
      },

      onError: (action: LoadBoutiqueStatistics, error) => {
        return new LoadBoutiqueStatisticsError(this.errorHandler(error));
      }
    }
  );


  @Effect() loadBinCodes$: Observable<Action> = this.dataPersistence.fetch(
    InStockActionTypes.LOAD_BINCODES,
    {
      run: (action: LoadBinCodes) => {
        return this.service
          .getBinCodes()
          .pipe(
            map(
              (binCodes: BinCodes[]) =>
                new LoadBinCodesSuccess(binCodes)
            )
          );
      },

      onError: (action: LoadBinCodes, error: HttpErrorResponse) => {

        return new LoadBinCodesFailure(this.errorHandler(error));
      }
    }
  );

  @Effect() binCodeCount$ = this.dataPersistence.fetch(InStockActionTypes.LOAD_COUNT, {
    // provides an action and the current state of the store
    run: (action: LoadCount) => {
      return this.service
        .getTotalBinCodes()
        .pipe(map((data: any) => new LoadCountSuccess(data)));
    },

    onError: (action: LoadCount, error: HttpErrorResponse) => {

      return new LoadCountFailure(this.errorHandler(error));
    }
  });

  @Effect()
  requestBin$ = this.dataPersistence.pessimisticUpdate(
    InStockActionTypes.REQUESTED_BIN,
    {
      run: (action: RequestedBin) => {
        return this.service
          .requestBin(action.payload)
          .pipe(map((data: any) => new RequestedBinSuccess(data)));
      },

      onError: (action: RequestedBin, error: HttpErrorResponse) => {

        return new RequestedBinFailure(this.errorHandler(error));
      }
    }
  );

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }

  constructor(
    private service: InStockService,
    private dataPersistence: DataPersistence<BoutiqueStatisticsState>,
    private notificationService: NotificationService
  ) { }
}
