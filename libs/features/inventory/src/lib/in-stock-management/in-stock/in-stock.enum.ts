export enum OtherReceiptsIssuesEnum {
  OTHER_RECEIPTS = 'otherreceipts',
  OTHER_ISSUES = 'otherissues',
  LOAN = 'LOAN',
  EXHIBITION = 'Exhibition',
  EXHIBITION_TYPE = 'EXH',
  NON_VERIFIED = 'nonverified',
  VERIFIED = 'verified',
  OPEN = 'OPEN',
  SELECTED = 'SELECTED',
  ALL = 'allProducts',
  SELECTED_PRODUCTS = 'Selected',
  APVL_PENDING_STATUS = 'APVL_PENDING',
  APPROVED_STATUS = 'APPROVED',
  REQUESTED_STATUS = 'REQUESTED',
  LOAN_TYPE = 'Loan',
  ADJUSTMENT = 'Adjustment',
  ADJUSTMENT_TYPE = 'ADJ',
  LOSS = 'Loss',
  LOSS_TYPE = 'LOSS',
  PSV = 'PSV',
  FOC = 'FOC'
}
export enum ProductGroupEnum {
  PLAIN = '71',
  PLAIN_GOLD = 'PLAINGOLD',
  STUDDED = '72'
}
export enum ItemStatusEnum {
  VERIFIED = 'VERIFIED',
  ISSUED = 'ISSUED'
}
export enum StockStatusEnum {
  ISSUED = 'issued'
}
