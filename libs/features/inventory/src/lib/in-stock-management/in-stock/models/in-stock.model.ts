export interface BoutiqueStatistics {
  totalProducCount: number;
  totalProductCategories: number;
  totalBinCodes: number;
  totalValueOfStock: string;
  totalBinGroups: number;
}

export interface TransferType {
  type: string;
  count: number;
}
