import { Component, OnInit, OnDestroy } from '@angular/core';

import { BoutiqueStatistics } from './models/in-stock.model';
import { Observable, Subject } from 'rxjs';
import { tap, map, takeUntil } from 'rxjs/operators';
import { InStockFacade } from './+state/in-stock.facade';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import {
  getInterBoutiqueTransferRouteUrl,
  getInventoryRouteUrl,
  getBintoBinTransferRouteUrl,
  getOtherReceiptsIssuesListRouteUrl,
  getConversionRouteUrl
} from '../../page-route.constants';
import { OtherReceiptsIssuesEnum } from './in-stock.enum';
import { BinCodes, BinRequestDto } from './models/bin.model';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatDialog } from '@angular/material/dialog';
import {
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationService
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { BinRequestPopupComponent } from './bin-request-popup/bin-request-popup.component';
import { BinRequestSuccessPopupComponent } from './bin-request-success-popup/bin-request-success-popup.component';
import { Router } from '@angular/router';

@Component({
  selector: 'poss-web-in-stock',
  templateUrl: './in-stock.component.html',
  styleUrls: ['./in-stock.component.scss']
})
export class InStockComponent implements OnInit, OnDestroy {
  data$: Observable<any>;

  boutiqueStatistics: BoutiqueStatistics;
  storeType$: Observable<boolean>;
  // otherReceiptIssuesListUrL: string;
  defaultInStockPath = '';
  defaultIBTPath = 'SENT';
  defaultBintoBinPath = 'variantCode';
  defaultOtherReceiptIssuePath = 'Exhibition';
  defaultConversionPath = 'search';
  defaultInStockConversionPath;
  // defaultPage = '';
  show = false;
  isLoading: boolean;
  binCodesFirstLoad = true;
  mobileQuery: MediaQueryList;
  pageSize: number;
  binCodes$: Observable<BinCodes[]>;
  user$: Observable<boolean>;
  hasNotification: boolean;
  destroy$: Subject<null> = new Subject<null>();
  constructor(
    private inStockFacade: InStockFacade,
    private appsettingFacade: AppsettingFacade,
    media: MediaMatcher,
    public dialog: MatDialog,
    private translate: TranslateService,
    private overlayNotification: OverlayNotificationService,
    private router: Router
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 991px)');
  }

  ngOnInit() {
    // this.boutiqueStatisticFacade.loadboutiqueStatistics();
    this.data$ = this.inStockFacade.getboutiqueStatistics();
    this.loadBinCodes();

    this.inStockFacade
      .getRequestBin()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          this.binRequestSuccessPopup(data);
        }
      });
    this.binCodes$ = this.inStockFacade.getBinCodes();
    // this.inStockFacade.loadbinCodesCount();
    // this.inStockFacade.getbinCodesCount().pipe(takeUntil(this.destroy$)).subscribe(data => {
    //   this.loadBinCodes(0, data);
    // });
    this.inStockFacade
      .getIsLoading()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isLoading: boolean) => {
        this.isLoading = isLoading;
      });
    this.storeType$ = this.appsettingFacade
      .getStoreType()
      .pipe(map(storeType => storeType === 'L1' || storeType === 'L2'));

    this.user$ = this.appsettingFacade
      .getStoreType()
      .pipe(
        map(
          storeType =>
            storeType === 'L1' || storeType === 'L2' || storeType === 'L3'
        )
      );

    // this.defaultPage = OtherReceiptsIssuesEnum.OTHER_RECEIPTS + '/Exhibition';
    // this.otherReceiptIssuesListUrL = getOtherReceiptsIssuesListRouteUrl(
    //   OtherReceiptsIssuesEnum.OTHER_RECEIPTS,
    //   this.defaultOtherReceiptIssuePath
    // );

    this.inStockFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe(error => {
        if (error) {
          this.errorHandler(error);
        }
      });
  }

  inventoryUrl() {
    this.router.navigate([getInventoryRouteUrl()]);
  }

  if(error) {
    this.errorHandler(error);
  }
  interBoutiqueTransferUrl() {
    this.router.navigate([
      getInterBoutiqueTransferRouteUrl(this.defaultIBTPath)
    ]);
  }

  bintobinTransferUrl() {
    this.router.navigate([
      getBintoBinTransferRouteUrl(this.defaultBintoBinPath)
    ]);
  }

  conversionUrl() {
    this.router.navigate([getConversionRouteUrl(this.defaultConversionPath)]);
  }

  otherReceiptIssueUrl() {
    this.router.navigate([
      getOtherReceiptsIssuesListRouteUrl(
        OtherReceiptsIssuesEnum.OTHER_RECEIPTS,
        this.defaultOtherReceiptIssuePath
      )
    ]);
  }

  loadBinCodes() {
    if (!this.isLoading) {
      this.inStockFacade.loadBinCodes();
      this.binCodesFirstLoad = false;
    }
  }

  binRequestPopup(): void {
    const dialogRef = this.dialog.open(BinRequestPopupComponent, {
      width: '500px'
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(result => {
        if (result) {
          this.postNewBinRequest(result);
        }
      });
  }

  postNewBinRequest(binRequest: BinRequestDto) {
    this.inStockFacade.requestedBin(binRequest);
  }

  binRequestSuccessPopup(data): void {
    const dialogRef = this.dialog.open(BinRequestSuccessPopupComponent, {
      width: '500px',

      data: {
        reqDocNo: data.reqDocNo
      }
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(result => {});
  }

  errorHandler(error: CustomErrors) {
    this.hasNotification = true;
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        this.hasNotification = false;
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
