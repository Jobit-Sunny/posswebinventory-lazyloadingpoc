import { Injectable } from '@angular/core';
import { ApiService } from '@poss-web/core';
import { InStockAdaptor } from '../../adaptors/in-stock.adaptor';

import { map } from 'rxjs/operators';
import {
  getBoutiqueStatisticsEndpointUrl,
  getAllBinCodesUrl,
  getAllBinCodesCountUrl
} from '../../endpoints.constants';
import { requestBinUrl } from '../../endpoints.constants';
import { Observable } from 'rxjs';

import { BinCodes, BinRequestDto } from './models/bin.model';

@Injectable()
export class InStockService {
  getBoutiqueStatistics() {
    const boutiqueStatisticsUrl = getBoutiqueStatisticsEndpointUrl();
    return this.apiService
      .get(boutiqueStatisticsUrl)
      .pipe(map(data => InStockAdaptor.boutiqueStatisticsfromJson(data)));
  }

  getBinCodes(): Observable<BinCodes[]> {
    const url = getAllBinCodesUrl();

    return this.apiService
      .get(url)
      .pipe(map((data: any) => InStockAdaptor.BinCodesJson(data)));
  }

  getTotalBinCodes(): Observable<any> {
    const url = getAllBinCodesCountUrl();

    return this.apiService
      .get(url)
      .pipe(map((data: any) => data.totalElements));
  }

  requestBin(requestBin: BinRequestDto): Observable<any> {
    const url = requestBinUrl();

    return this.apiService.post(url, requestBin).pipe(map((data: any) => data));
  }
  constructor(private apiService: ApiService) {}
}
