import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { BinRequestDto, BinRequestResponse } from '../models/bin.model';
import { Subject } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';


@Component({
  selector: 'poss-web-bin-request-popup',
  templateUrl: './bin-request-popup.component.html',
  styleUrls: ['./bin-request-popup.component.scss']
})
export class BinRequestPopupComponent implements OnInit, OnDestroy {

  newBinRequestForm = new FormGroup({
    bin: new FormControl(''),
    remarks: new FormControl(''),
  });
  value: string;
  binRequest: BinRequestDto;

  requestedBinResponse: BinRequestResponse;
  destroy$: Subject<null> = new Subject<null>();

  constructor(public dialogRef: MatDialogRef<BinRequestPopupComponent>, @Inject(MAT_DIALOG_DATA) public data: BinRequestDto, public dialog: MatDialog, form: FormBuilder) {
    this.newBinRequestForm = form.group({
      bin: ['',[Validators.required,Validators.maxLength(20),Validators.pattern('^[a-zA-Z0-9 ]+')]],
      remarks: ['', [Validators.required,  Validators.maxLength(250)]],
    })
    console.log(this.newBinRequestForm.errors);
  }


  ngOnInit() { }

  onClose(): void {
    this.dialogRef.close();
  }

  requestBin() {
    this.binRequest = {
      bin: this.newBinRequestForm.get('bin').value,
      remarks: this.newBinRequestForm.get('remarks').value

    }


    if (this.newBinRequestForm.valid === true) {
      this.dialogRef.close(this.binRequest);
    }
    else {
      this.newBinRequestForm.markAllAsTouched();
    }

  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
