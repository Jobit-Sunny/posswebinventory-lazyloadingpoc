import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InterBoutiqueTransferRoutingModule } from './inter-boutique-transfer-routing.module';
import { MatSidenavModule } from '@angular/material';
import { UicomponentsModule, SharedModule } from '@poss-web/shared';
import { EffectsModule } from '@ngrx/effects';
import { InterBoutiqueTransferEffects } from './+state/inter-boutique-transfer.effect';
import { InterBoutiqueTransferComponent } from './containers/inter-boutique-transfer/inter-boutique-transfer.component';
import { InterBoutiqueTransferDetailsComponent } from './containers/inter-boutique-transfer-details/inter-boutique-transfer-details.component';
import { InterBoutiqueTransferCreateRequestComponent } from './containers/inter-boutique-transfer-create-request/inter-boutique-transfer-create-request.component';
import { InterBoutiqueTransferItemListComponent } from './components/inter-boutique-transfer-item-list/inter-boutique-transfer-item-list.component';
import { InterBoutiqueTransferItemComponent } from './components/inter-boutique-transfer-item/inter-boutique-transfer-item.component';
import { InterBoutiqueTransferFacade } from './+state/inter-boutique-transfer.facade';
import { InterBoutiqueTransferService } from './services/inter-boutique-transfer.service';

@NgModule({
  declarations: [
    InterBoutiqueTransferComponent,
    InterBoutiqueTransferDetailsComponent,
    InterBoutiqueTransferCreateRequestComponent,
    InterBoutiqueTransferItemListComponent,
    InterBoutiqueTransferItemComponent
  ],
  imports: [
    CommonModule,
    InterBoutiqueTransferRoutingModule,
    SharedModule,
    UicomponentsModule,
    MatSidenavModule,
    EffectsModule.forFeature([InterBoutiqueTransferEffects])
  ],
  providers: [InterBoutiqueTransferFacade, InterBoutiqueTransferService]
})
export class InterBoutiqueTransferModule {}
