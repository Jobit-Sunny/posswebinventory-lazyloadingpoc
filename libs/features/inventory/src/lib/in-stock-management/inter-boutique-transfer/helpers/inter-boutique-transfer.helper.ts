import { InterBoutiqueTransferAdaptor } from '../adaptors/inter-boutique-transfer.adaptor';
import {
  RequestList,
  ItemList,
  BoutiqueList,
  BoutiqueListResponse
} from '../models/inter-boutique-transfer.model';

export class InterBoutiqueTransferHelper {
  static getRequestList(data: any): RequestList[] {
    const requestList: RequestList[] = [];
    for (const request of data.results) {
      requestList.push(InterBoutiqueTransferAdaptor.requestFromJson(request));
    }
    return requestList;
  }

  static getBoutiqueList(data: any): BoutiqueListResponse {
    const boutiqueList: BoutiqueList[] = [];
    for (const boutique of data.results) {
      boutiqueList.push(
        InterBoutiqueTransferAdaptor.boutiqueFromJson(boutique)
      );
    }
    return { boutiqueList: boutiqueList, totalElements: data.totalElements };
  }

  static getItemList(data: any): ItemList[] {
    const itemList: ItemList[] = [];
    for (const item of data.results) {
      itemList.push(InterBoutiqueTransferAdaptor.itemFromJson(item));
    }
    return itemList;
  }
}
