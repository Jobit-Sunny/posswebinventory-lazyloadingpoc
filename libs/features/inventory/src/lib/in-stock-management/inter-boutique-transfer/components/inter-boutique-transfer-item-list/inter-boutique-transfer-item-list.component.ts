import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {
  InterBoutiqueTransferRequestTypesEnum,
  InterBoutiqueTransferStatusTypesEnum
} from '../../models/inter-boutique-transfer.enum';
import {
  ItemList,
  IsSelectedData,
  IsSelectedItemCode,
  IsSelectedItem
} from '../../models/inter-boutique-transfer.model';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'poss-web-inter-boutique-transfer-item-list',
  templateUrl: './inter-boutique-transfer-item-list.component.html',
  styleUrls: ['./inter-boutique-transfer-item-list.component.scss']
})
export class InterBoutiqueTransferItemListComponent
  implements OnInit, OnDestroy {
  @Input() itemList$: Observable<ItemList[]>;
  @Input() requestType: string;
  @Input() selectedRequestStatus: string;
  @Input() selectedItemCodeEvents: Observable<IsSelectedItemCode>;

  @Output() isSelected = new EventEmitter<IsSelectedData>();
  @Output() isSelectedItem = new EventEmitter<IsSelectedItem>();

  interBoutiqueTransferRequestTypesEnumRef = InterBoutiqueTransferRequestTypesEnum;
  interBoutiqueTransferStatusTypesEnumRef = InterBoutiqueTransferStatusTypesEnum;
  status: string;
  statusColor: string;

  selectedItemCode$: Subject<IsSelectedItemCode> = new Subject<
    IsSelectedItemCode
  >();
  destroy$: Subject<null> = new Subject<null>();

  constructor() {}

  ngOnInit() {
    this.selectedItemCodeEvents
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => this.selectedItemCode$.next(data));
  }

  /**
   * gets triggered when any change happens in check box field
   * @param event
   */
  isSelectedChange(event: IsSelectedData) {
    this.isSelected.emit(event);
  }

  isSelectedItemChange(event: IsSelectedItem) {
    this.isSelectedItem.emit(event);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
