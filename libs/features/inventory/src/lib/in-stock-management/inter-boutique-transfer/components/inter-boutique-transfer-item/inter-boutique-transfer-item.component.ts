import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output,
  OnDestroy
} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { commonTranslateKeyMap } from '../../../../common.map';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil, debounceTime } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import {
  InterBoutiqueTransferRequestTypesEnum,
  InterBoutiqueTransferStatusTypesEnum
} from '../../models/inter-boutique-transfer.enum';
import {
  ItemList,
  IsSelectedData,
  IsSelectedItemCode,
  IsSelectedItem
} from '../../models/inter-boutique-transfer.model';
import { AppsettingFacade } from '@poss-web/core';

@Component({
  selector: 'poss-web-inter-boutique-transfer-item',
  templateUrl: './inter-boutique-transfer-item.component.html',
  styleUrls: ['./inter-boutique-transfer-item.component.scss']
})
export class InterBoutiqueTransferItemComponent implements OnInit, OnDestroy {
  @Input() item: ItemList;
  @Input() selectedItemCodeEvents: Observable<IsSelectedItemCode>;
  @Input() selectedRequestStatus: string;

  @Output() isSelected = new EventEmitter<IsSelectedData>();
  @Output() isSelectedItem = new EventEmitter<IsSelectedItem>();

  interBoutiqueTransferRequestTypesEnumRef = InterBoutiqueTransferRequestTypesEnum;
  interBoutiqueTransferStatusTypesEnumRef = InterBoutiqueTransferStatusTypesEnum;

  itemForm: FormGroup;
  weightToDisplay: string;
  quantityToDisplay: number;
  requestId: number;
  requestType: string;
  disabled: boolean;
  status: string;
  statusColor: string;

  dateFormat$: Observable<string>;
  destroy$: Subject<null> = new Subject<null>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private translate: TranslateService,
    private appsettingFacade: AppsettingFacade
  ) {}

  ngOnInit() {
    this.requestId = this.activatedRoute.snapshot.params['requestId'];
    this.requestType = this.activatedRoute.snapshot.params['requestType'];
    this.dateFormat$ = this.appsettingFacade.getDateFormat();

    if (this.item.requestedQuantity > this.item.availableQuantity) {
      this.quantityToDisplay = this.item.availableQuantity;
    } else {
      this.quantityToDisplay = this.item.requestedQuantity;
    }
    this.weightToDisplay = this.item.requestedWeight.toFixed(3);

    this.itemForm = this.createForm();

    if (
      this.requestType === InterBoutiqueTransferRequestTypesEnum.RECEIVED &&
      this.item.availableQuantity === 0
    ) {
      this.itemForm.disable();
      this.disabled = true;
    }
    if (
      (this.item.status === InterBoutiqueTransferStatusTypesEnum.ACCEPTED ||
        this.item.status === InterBoutiqueTransferStatusTypesEnum.APPROVED) &&
      this.selectedRequestStatus ===
        InterBoutiqueTransferStatusTypesEnum.REQUESTED &&
      this.requestType === InterBoutiqueTransferRequestTypesEnum.RECEIVED
    ) {
      this.itemForm.patchValue({ isSelected: true });
      this.isSelectedItem.emit({
        itemId: this.item.id,
        itemCode: this.item.itemCode
      });
    }

    this.selectedItemCodeEvents
      .pipe(
        debounceTime(10),
        takeUntil(this.destroy$)
      )
      .subscribe(data => {
        if (data.itemId === this.item.id) {
          this.itemForm.patchValue({ isSelected: data.isSelected });
        }
      });
  }

  /**
   * gets triggered when any change happens in select item check box
   */
  isSelectedChange(): void {
    const isSelectedData = {
      itemId: this.item.id,
      isSelected: this.itemForm.value.isSelected,
      itemCode: this.item.itemCode,
      quantity: this.itemForm.value.requestedQuantity
    };
    this.isSelected.emit(isSelectedData);
  }

  /**
   * form for quantity and check box
   * @param item: item received
   */
  createForm(): FormGroup {
    return new FormGroup({
      requestedQuantity: new FormControl(this.quantityToDisplay),
      isSelected: new FormControl(false)
    });
  }

  /**
   * gets the status of the request and determines the color code and status description
   * @param status : status of the request
   */
  getStatus(status: string) {
    let key = {
      status: '',
      statusColor: ''
    };
    if (commonTranslateKeyMap.has(status)) {
      key = commonTranslateKeyMap.get(status);
    }
    this.translate
      .get([key.status, key.statusColor])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: string) => {
        this.status = translatedMessages[key.status];
        this.statusColor = translatedMessages[key.statusColor];
      });
    return this.statusColor;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
