import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@poss-web/core';
import { InterBoutiqueTransferCreateRequestComponent } from './containers/inter-boutique-transfer-create-request/inter-boutique-transfer-create-request.component';
import { InterBoutiqueTransferComponent } from './containers/inter-boutique-transfer/inter-boutique-transfer.component';
import { InterBoutiqueTransferDetailsComponent } from './containers/inter-boutique-transfer-details/inter-boutique-transfer-details.component';

const routes: Routes = [
  {
    path: 'create-request',
    component: InterBoutiqueTransferCreateRequestComponent,
    canActivate: [AuthGuard]
  },
  {
    path: ':requestType',
    component: InterBoutiqueTransferComponent,
    canActivate: [AuthGuard]
  },
  {
    path: ':requestType/:requestId',
    component: InterBoutiqueTransferDetailsComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InterBoutiqueTransferRoutingModule {}
