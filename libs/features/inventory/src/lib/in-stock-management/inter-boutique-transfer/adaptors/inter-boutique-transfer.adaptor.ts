import * as moment from 'moment';
import {
  RequestList,
  BoutiqueList,
  ItemList
} from '../models/inter-boutique-transfer.model';

export class InterBoutiqueTransferAdaptor {
  /**
   * The function maps the json data to respective model type
   */

  static requestFromJson(data: any): RequestList {
    return {
      id: data.id,
      reqDocNo: data.reqDocNo,
      srcLocationCode: data.srcLocationCode,
      destLocationCode: data.destLocationCode,
      totalRequestedQuantity: data.totalRequestedQuantity,
      acceptedQuantity: data.acceptedQuantity,
      approvedQuantity: data.approvedQuantity,
      status: data.status,
      reqDocDate: moment(data.reqDocDate),
      requestType: data.requestType
    };
  }

  static boutiqueFromJson(data: any): BoutiqueList {
    return {
      locationCode: data.locationCode,
      address: data.address,
      contactNumber: data.contactNumber
    };
  }

  static itemFromJson(data: any): ItemList {
    return {
      id: data.id,
      itemCode: data.itemCode,
      lotNumber: data.lotNumber,
      mfgDate: moment(data.reqDocDate),
      productCategory: data.productCategory,
      productGroup: data.productGroup,
      binCode: data.binCode,
      binGroupCode: data.binGroupCode,
      stdValue: data.stdValue,
      stdWeight: data.stdWeight,
      currencyCode: data.currencyCode,
      weightUnit: data.weightUnit,
      status: data.status,
      imageURL: data.imageURL,
      itemDetails: data.itemDetails,
      requestedQuantity: data.requestedQuantity,
      requestedWeight: data.requestedWeight,
      acceptedQuantity: data.acceptedQuantity,
      approvedQuantity: data.approvedQuantity,
      availableQuantity: data.availableQuantity,
      inventoryId: data.inventoryId,
      totalAcceptedQuantity: data.totalAcceptedQuantity
    };
  }
}
