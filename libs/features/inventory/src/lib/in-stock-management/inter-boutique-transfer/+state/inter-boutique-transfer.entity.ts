import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { RequestList, ItemList } from '../models/inter-boutique-transfer.model';

export interface RequestListEntity extends EntityState<RequestList> {}
export const requestListAdapter = createEntityAdapter<RequestList>({
  selectId: requestList => requestList.id
});
export const requestListSelector = requestListAdapter.getSelectors();

export interface ItemListEntity extends EntityState<ItemList> {}
export const itemListAdapter = createEntityAdapter<ItemList>({
  selectId: itemList => itemList.id
});
export const itemListSelector = itemListAdapter.getSelectors();
