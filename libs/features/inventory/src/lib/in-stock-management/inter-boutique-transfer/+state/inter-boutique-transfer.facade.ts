import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { InterBoutiqueTransferSelectors } from './inter-boutique-transfer.selectors';
import { State } from '../../../inventory.state';
import * as InterBoutiqueTransferActions from './inter-boutique-transfer.actions';
import { Request } from '../models/inter-boutique-transfer.model';

@Injectable()
export class InterBoutiqueTransferFacade {
  private requestSentList$ = this.store.select(
    InterBoutiqueTransferSelectors.selectRequestSentList
  );

  private requestReceivedList$ = this.store.select(
    InterBoutiqueTransferSelectors.selectRequestReceivedList
  );

  private requestSentListCount$ = this.store.select(
    InterBoutiqueTransferSelectors.selectRequestSentListCount
  );

  private requestReceivedListCount$ = this.store.select(
    InterBoutiqueTransferSelectors.selectRequestReceivedListCount
  );

  private boutiqueList$ = this.store.select(
    InterBoutiqueTransferSelectors.selectBoutiqueList
  );

  private createRequestResponse$ = this.store.select(
    InterBoutiqueTransferSelectors.selectCreateRequestResponse
  );

  private request$ = this.store.select(
    InterBoutiqueTransferSelectors.selectRequest
  );

  private itemList$ = this.store.select(
    InterBoutiqueTransferSelectors.selectItemList
  );

  private updateItemListResponse$ = this.store.select(
    InterBoutiqueTransferSelectors.selectUpdateItemListResponse
  );

  private updateItemListStatusResponse$ = this.store.select(
    InterBoutiqueTransferSelectors.selectUpdateItemListStatusResponse
  );

  private searchItemResponse$ = this.store.select(
    InterBoutiqueTransferSelectors.selectSearchItemResponse
  );

  private hasError$ = this.store.select(
    InterBoutiqueTransferSelectors.selectHasError
  );

  private isLoading$ = this.store.select(
    InterBoutiqueTransferSelectors.selectIsLoading
  );

  constructor(private store: Store<State>) {}

  loadRequestSentListCount(type: string) {
    this.store.dispatch(
      new InterBoutiqueTransferActions.LoadRequestSentListCount(type)
    );
  }

  loadRequestReceivedListCount(type: string) {
    this.store.dispatch(
      new InterBoutiqueTransferActions.LoadRequestReceivedListCount(type)
    );
  }

  getRequestSentListCount() {
    return this.requestSentListCount$;
  }

  getRequestReceivedListCount() {
    return this.requestReceivedListCount$;
  }

  loadRequestSentList(
    loadRequestListPayload: InterBoutiqueTransferActions.LoadRequestListPayload
  ) {
    this.store.dispatch(
      new InterBoutiqueTransferActions.LoadRequestSentList(
        loadRequestListPayload
      )
    );
  }

  getRequestSentList() {
    return this.requestSentList$;
  }

  loadRequestReceivedList(
    loadRequestListPayload: InterBoutiqueTransferActions.LoadRequestListPayload
  ) {
    this.store.dispatch(
      new InterBoutiqueTransferActions.LoadRequestReceivedList(
        loadRequestListPayload
      )
    );
  }

  getRequestReceivedList() {
    return this.requestReceivedList$;
  }

  loadBoutiqueList(
    loadBoutiqueListPayload: InterBoutiqueTransferActions.LoadBoutiqueListPayload
  ) {
    this.store.dispatch(
      new InterBoutiqueTransferActions.LoadBoutiqueList(loadBoutiqueListPayload)
    );
  }

  getBoutiqueList() {
    return this.boutiqueList$;
  }

  createRequest(request: Request) {
    this.store.dispatch(
      new InterBoutiqueTransferActions.CreateRequest(request)
    );
  }

  getcreateRequestResponse() {
    return this.createRequestResponse$;
  }

  loadRequest(
    loadRequestPayload: InterBoutiqueTransferActions.LoadRequestPayload
  ) {
    this.store.dispatch(
      new InterBoutiqueTransferActions.LoadRequest(loadRequestPayload)
    );
  }

  getRequest() {
    return this.request$;
  }

  loadItemList(
    loadItemListPayload: InterBoutiqueTransferActions.LoadItemListPayload
  ) {
    this.store.dispatch(
      new InterBoutiqueTransferActions.LoadItemList(loadItemListPayload)
    );
  }

  getItemList() {
    return this.itemList$;
  }

  updateItemList(
    updateItemListPayload: InterBoutiqueTransferActions.UpdateItemListPayload
  ) {
    this.store.dispatch(
      new InterBoutiqueTransferActions.UpdateItemList(updateItemListPayload)
    );
  }

  updateItemListResponse() {
    return this.updateItemListResponse$;
  }

  updateItemListStatus(
    updateItemListStatusPayload: InterBoutiqueTransferActions.UpdateItemListStatusPayload
  ) {
    this.store.dispatch(
      new InterBoutiqueTransferActions.UpdateItemListStatus(
        updateItemListStatusPayload
      )
    );
  }

  updateSelectedRequestProductListStatusResponse() {
    return this.updateItemListStatusResponse$;
  }

  getHasError() {
    return this.hasError$;
  }

  getIsLoading() {
    return this.isLoading$;
  }

  loadSearchItem(itemCode: string) {
    this.store.dispatch(new InterBoutiqueTransferActions.SearchItem(itemCode));
  }

  getSearchItemResponse() {
    return this.searchItemResponse$;
  }

  /**
   * Dispatch Action for Clearing list results
   */
  clearRequestSentList() {
    this.store.dispatch(
      new InterBoutiqueTransferActions.ClearRequestSentList()
    );
  }

  clearRequestReceivedList() {
    this.store.dispatch(
      new InterBoutiqueTransferActions.ClearRequestReceivedList()
    );
  }

  clearItemList() {
    this.store.dispatch(new InterBoutiqueTransferActions.ClearItemList());
  }

  clearBoutiqueList() {
    this.store.dispatch(new InterBoutiqueTransferActions.ClearBoutiqueList());
  }

  clearSearchItemResponse() {
    this.store.dispatch(
      new InterBoutiqueTransferActions.ClearSearchItemResponse()
    );
  }

  resetRequestList() {
    this.store.dispatch(new InterBoutiqueTransferActions.ResetRequestList());
  }
}
