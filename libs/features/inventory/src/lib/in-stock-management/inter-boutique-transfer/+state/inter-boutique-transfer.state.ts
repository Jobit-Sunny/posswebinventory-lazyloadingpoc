import {
  RequestListEntity,
  ItemListEntity
} from './inter-boutique-transfer.entity';

import {
  RequestList,
  ItemList,
  BoutiqueListResponse
} from '../models/inter-boutique-transfer.model';
import { CustomErrors } from '@poss-web/core';

export class InterBoutiqueTransferState {
  requestSentList: RequestListEntity;
  requestReceivedList: RequestListEntity;
  requestSentListCount: number;
  requestReceivedListCount: number;
  boutiqueList: BoutiqueListResponse;
  createRequestResponse: RequestList;
  request: RequestList;
  itemList: ItemListEntity;
  updateItemListResponse: ItemList;
  updateItemListStatusResponse: RequestList;
  searchItemResponse: string;
  hasError?: CustomErrors;
  isLoading?: boolean;
}
