import {
  InterBoutiqueTransferActions,
  InterBoutiqueTransferActionTypes
} from './inter-boutique-transfer.actions';
import { InterBoutiqueTransferState } from './inter-boutique-transfer.state';
import {
  requestListAdapter,
  itemListAdapter
} from './inter-boutique-transfer.entity';
import {
  RequestList,
  ItemList,
  BoutiqueListResponse
} from '../models/inter-boutique-transfer.model';

export const initialState: InterBoutiqueTransferState = {
  requestSentList: requestListAdapter.getInitialState(),
  requestReceivedList: requestListAdapter.getInitialState(),
  requestSentListCount: 0,
  requestReceivedListCount: 0,
  boutiqueList: {} as BoutiqueListResponse,
  createRequestResponse: {} as RequestList,
  request: {} as RequestList,
  itemList: itemListAdapter.getInitialState(),
  updateItemListResponse: {} as ItemList,
  updateItemListStatusResponse: {} as RequestList,
  searchItemResponse: null,
  hasError: null,
  isLoading: false
};

export function InterBoutiqueTransferReducer(
  state: InterBoutiqueTransferState = initialState,
  action: InterBoutiqueTransferActions
): InterBoutiqueTransferState {
  switch (action.type) {
    case InterBoutiqueTransferActionTypes.LOAD_REQUEST_SENT_LIST:
    case InterBoutiqueTransferActionTypes.LOAD_REQUEST_RECEIVED_LIST:
    case InterBoutiqueTransferActionTypes.LOAD_REQUEST_SENT_LIST_COUNT:
    case InterBoutiqueTransferActionTypes.LOAD_REQUEST_RECEIVED_LIST_COUNT:
    case InterBoutiqueTransferActionTypes.LOAD_BOUTIQUE_LIST:
    case InterBoutiqueTransferActionTypes.CREATE_REQUEST:
    case InterBoutiqueTransferActionTypes.LOAD_REQUEST:
    case InterBoutiqueTransferActionTypes.LOAD_ITEM_LIST:
    case InterBoutiqueTransferActionTypes.UPDATE_ITEM_LIST:
    case InterBoutiqueTransferActionTypes.UPDATE_ITEM_LIST_STATUS:
    case InterBoutiqueTransferActionTypes.SEARCH_ITEM:
      return { ...state, isLoading: true, hasError: null };

    case InterBoutiqueTransferActionTypes.LOAD_REQUEST_SENT_LIST_FAILURE:
    case InterBoutiqueTransferActionTypes.LOAD_REQUEST_RECEIVED_LIST_FAILURE:
    case InterBoutiqueTransferActionTypes.LOAD_REQUEST_SENT_LIST_COUNT_FAILURE:
    case InterBoutiqueTransferActionTypes.LOAD_REQUEST_RECEIVED_LIST_COUNT_FAILURE:
    case InterBoutiqueTransferActionTypes.LOAD_BOUTIQUE_LIST_FAILURE:
    case InterBoutiqueTransferActionTypes.CREATE_REQUEST_FAILURE:
    case InterBoutiqueTransferActionTypes.LOAD_REQUEST_FAILURE:
    case InterBoutiqueTransferActionTypes.LOAD_ITEM_LIST_FAILURE:
    case InterBoutiqueTransferActionTypes.UPDATE_ITEM_LIST_FAILURE:
    case InterBoutiqueTransferActionTypes.UPDATE_ITEM_LIST_STATUS_FAILURE:
    case InterBoutiqueTransferActionTypes.SEARCH_ITEM_FAILURE:
      return {
        ...state,
        hasError: action.payload,
        isLoading: false
      };

    case InterBoutiqueTransferActionTypes.LOAD_REQUEST_SENT_LIST_SUCCESS:
      return {
        ...state,
        requestSentList: requestListAdapter.addMany(
          action.payload,
          state.requestSentList
        ),
        isLoading: false,
        hasError: null
      };

    case InterBoutiqueTransferActionTypes.LOAD_REQUEST_RECEIVED_LIST_SUCCESS:
      return {
        ...state,
        requestReceivedList: requestListAdapter.addMany(
          action.payload,
          state.requestReceivedList
        ),
        isLoading: false,
        hasError: null
      };

    case InterBoutiqueTransferActionTypes.LOAD_REQUEST_SENT_LIST_COUNT_SUCCESS:
      return {
        ...state,
        isLoading: false,
        hasError: null,
        requestSentListCount: action.payload
      };

    case InterBoutiqueTransferActionTypes.LOAD_REQUEST_RECEIVED_LIST_COUNT_SUCCESS:
      return {
        ...state,
        isLoading: false,
        hasError: null,
        requestReceivedListCount: action.payload
      };

    case InterBoutiqueTransferActionTypes.LOAD_BOUTIQUE_LIST_SUCCESS:
      return {
        ...state,
        boutiqueList: action.payload,
        isLoading: false,
        hasError: null
      };

    case InterBoutiqueTransferActionTypes.CREATE_REQUEST_SUCCESS:
      return {
        ...state,
        createRequestResponse: action.payload,
        isLoading: false,
        hasError: null
      };

    case InterBoutiqueTransferActionTypes.LOAD_REQUEST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        hasError: null,
        request: action.payload
      };

    case InterBoutiqueTransferActionTypes.LOAD_ITEM_LIST_SUCCESS:
      return {
        ...state,
        itemList: itemListAdapter.addAll(action.payload, state.itemList),
        isLoading: false,
        hasError: null
      };

    case InterBoutiqueTransferActionTypes.UPDATE_ITEM_LIST_SUCCESS:
      return {
        ...state,
        updateItemListResponse: action.payload,
        isLoading: false,
        hasError: null
      };

    case InterBoutiqueTransferActionTypes.UPDATE_ITEM_LIST_STATUS_SUCCESS:
      return {
        ...state,
        updateItemListStatusResponse: action.payload,
        requestReceivedList: requestListAdapter.updateOne(
          {
            id: action.payload.id,
            changes: {
              status: action.payload.status
            }
          },
          state.requestReceivedList
        ),
        isLoading: false,
        hasError: null
      };

    case InterBoutiqueTransferActionTypes.SEARCH_ITEM_SUCCESS:
      return {
        ...state,
        searchItemResponse: action.payload,
        isLoading: false,
        hasError: null
      };

    case InterBoutiqueTransferActionTypes.CLEAR_REQUEST_SENT_LIST:
      return {
        ...state,
        requestSentList: requestListAdapter.removeAll(state.requestSentList)
      };

    case InterBoutiqueTransferActionTypes.CLEAR_REQUEST_RECEIVED_LIST:
      return {
        ...state,
        requestReceivedList: requestListAdapter.removeAll(
          state.requestReceivedList
        )
      };

    case InterBoutiqueTransferActionTypes.CLEAR_ITEM_LIST:
      return {
        ...state,
        itemList: itemListAdapter.removeAll(state.itemList)
      };

    case InterBoutiqueTransferActionTypes.CLEAR_BOUTIQUE_LIST:
      return {
        ...state,
        boutiqueList: null
      };

    case InterBoutiqueTransferActionTypes.CLEAR_SEARCH_ITEM_RESPONSE:
      return {
        ...state,
        searchItemResponse: null
      };

    case InterBoutiqueTransferActionTypes.RESET_REQUEST_LIST:
      return {
        ...state,
        requestSentList: requestListAdapter.removeAll(state.requestSentList),
        requestReceivedList: requestListAdapter.removeAll(
          state.requestReceivedList
        ),
        requestSentListCount: 0,
        requestReceivedListCount: 0,
        boutiqueList: null,
        createRequestResponse: null,
        itemList: itemListAdapter.removeAll(state.itemList),
        request: null,
        updateItemListResponse: null,
        updateItemListStatusResponse: null,
        searchItemResponse: null,
        hasError: null,
        isLoading: false
      };

    default:
      return state;
  }
}
