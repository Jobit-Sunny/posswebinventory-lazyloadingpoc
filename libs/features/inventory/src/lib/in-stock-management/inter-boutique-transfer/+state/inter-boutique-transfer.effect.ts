import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import { DataPersistence } from '@nrwl/angular';
import { InterBoutiqueTransferActionTypes } from './inter-boutique-transfer.actions';
import * as InterBoutiqueTransferActions from './inter-boutique-transfer.actions';
import { InterBoutiqueTransferState } from './inter-boutique-transfer.state';
import {
  NotificationService,
  CustomErrors,
  ItemDataService
} from '@poss-web/core';
import { CustomErrorAdaptor, Item } from '@poss-web/core';
import { HttpErrorResponse } from '@angular/common/http';
import { InterBoutiqueTransferService } from '../services/inter-boutique-transfer.service';
import {
  RequestList,
  ItemList,
  BoutiqueListResponse
} from '../models/inter-boutique-transfer.model';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';

@Injectable()
export class InterBoutiqueTransferEffects {
  constructor(
    private dataPersistence: DataPersistence<InterBoutiqueTransferState>,
    private interBoutiqueTransferService: InterBoutiqueTransferService,
    private notificationService: NotificationService,
    private itemDataService: ItemDataService
  ) {}
  @Effect() loadRequestSentList$: Observable<
    Action
  > = this.dataPersistence.fetch(
    InterBoutiqueTransferActionTypes.LOAD_REQUEST_SENT_LIST,
    {
      run: (
        action: InterBoutiqueTransferActions.LoadRequestSentList,
        state: InterBoutiqueTransferState
      ) => {
        return this.interBoutiqueTransferService
          .getRequestList(
            action.payload.requestGroup,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (data: RequestList[]) =>
                new InterBoutiqueTransferActions.LoadRequestSentListSuccess(
                  data
                )
            )
          );
      },

      onError: (
        action: InterBoutiqueTransferActions.LoadRequestSentList,
        error: HttpErrorResponse
      ) => {
        return new InterBoutiqueTransferActions.LoadRequestSentListFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadRequestReceivedList$: Observable<
    Action
  > = this.dataPersistence.fetch(
    InterBoutiqueTransferActionTypes.LOAD_REQUEST_RECEIVED_LIST,
    {
      run: (
        action: InterBoutiqueTransferActions.LoadRequestReceivedList,
        state: InterBoutiqueTransferState
      ) => {
        return this.interBoutiqueTransferService
          .getRequestList(
            action.payload.requestGroup,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (data: RequestList[]) =>
                new InterBoutiqueTransferActions.LoadRequestReceivedListSuccess(
                  data
                )
            )
          );
      },

      onError: (
        action: InterBoutiqueTransferActions.LoadRequestReceivedList,
        error: HttpErrorResponse
      ) => {
        return new InterBoutiqueTransferActions.LoadRequestReceivedListFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadRequestSentListCount$: Observable<
    Action
  > = this.dataPersistence.fetch(
    InterBoutiqueTransferActionTypes.LOAD_REQUEST_SENT_LIST_COUNT,
    {
      // provides an action and the current state of the store
      run: (
        action: InterBoutiqueTransferActions.LoadRequestSentListCount,
        state: InterBoutiqueTransferState
      ) => {
        return this.interBoutiqueTransferService
          .getRequestCount(action.payload)
          .pipe(
            map(
              (data: number) =>
                new InterBoutiqueTransferActions.LoadRequestSentListCountSuccess(
                  data
                )
            )
          );
      },

      onError: (
        action: InterBoutiqueTransferActions.LoadRequestSentListCount,
        error: HttpErrorResponse
      ) => {
        return new InterBoutiqueTransferActions.LoadRequestSentListCountFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadRequestReceivedListCount$: Observable<
    Action
  > = this.dataPersistence.fetch(
    InterBoutiqueTransferActionTypes.LOAD_REQUEST_RECEIVED_LIST_COUNT,
    {
      // provides an action and the current state of the store
      run: (
        action: InterBoutiqueTransferActions.LoadRequestReceivedListCount,
        state: InterBoutiqueTransferState
      ) => {
        return this.interBoutiqueTransferService
          .getRequestCount(action.payload)
          .pipe(
            map(
              (data: number) =>
                new InterBoutiqueTransferActions.LoadRequestReceivedListCountSuccess(
                  data
                )
            )
          );
      },

      onError: (
        action: InterBoutiqueTransferActions.LoadRequestReceivedListCount,
        error: HttpErrorResponse
      ) => {
        return new InterBoutiqueTransferActions.LoadRequestReceivedListCountFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadBoutiqueList$: Observable<Action> = this.dataPersistence.fetch(
    InterBoutiqueTransferActionTypes.LOAD_BOUTIQUE_LIST,
    {
      run: (
        action: InterBoutiqueTransferActions.LoadBoutiqueList,
        state: InterBoutiqueTransferState
      ) => {
        return this.interBoutiqueTransferService
          .getBoutiqueList(
            action.payload.item,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (data: BoutiqueListResponse) =>
                new InterBoutiqueTransferActions.LoadBoutiqueListSuccess(data)
            )
          );
      },

      onError: (
        action: InterBoutiqueTransferActions.LoadBoutiqueList,
        error: HttpErrorResponse
      ) => {
        return new InterBoutiqueTransferActions.LoadBoutiqueListFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() createRequest$: Observable<Action> = this.dataPersistence.fetch(
    InterBoutiqueTransferActionTypes.CREATE_REQUEST,
    {
      run: (
        action: InterBoutiqueTransferActions.CreateRequest,
        state: InterBoutiqueTransferState
      ) => {
        return this.interBoutiqueTransferService
          .createRequest(
            action.payload.items,
            action.payload.remarks,
            action.payload.srcLocationCode
          )
          .pipe(
            map(
              (data: RequestList) =>
                new InterBoutiqueTransferActions.CreateRequestSuccess(data)
            )
          );
      },

      onError: (
        action: InterBoutiqueTransferActions.CreateRequest,
        error: HttpErrorResponse
      ) => {
        return new InterBoutiqueTransferActions.CreateRequestFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadRequest$: Observable<Action> = this.dataPersistence.fetch(
    InterBoutiqueTransferActionTypes.LOAD_REQUEST,
    {
      // provides an action and the current state of the store
      run: (
        action: InterBoutiqueTransferActions.LoadRequest,
        state: InterBoutiqueTransferState
      ) => {
        return this.interBoutiqueTransferService
          .getRequest(action.payload.id, action.payload.requestGroup)
          .pipe(
            map(
              (data: RequestList) =>
                new InterBoutiqueTransferActions.LoadRequestSuccess(data)
            )
          );
      },

      onError: (
        action: InterBoutiqueTransferActions.LoadRequest,
        error: HttpErrorResponse
      ) => {
        return new InterBoutiqueTransferActions.LoadRequestFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadItemList$: Observable<Action> = this.dataPersistence.fetch(
    InterBoutiqueTransferActionTypes.LOAD_ITEM_LIST,
    {
      run: (
        action: InterBoutiqueTransferActions.LoadItemList,
        state: InterBoutiqueTransferState
      ) => {
        return this.interBoutiqueTransferService
          .getItemList(action.payload.id, action.payload.requestGroup)
          .pipe(
            map(
              (data: ItemList[]) =>
                new InterBoutiqueTransferActions.LoadItemListSuccess(data)
            )
          );
      },

      onError: (
        action: InterBoutiqueTransferActions.LoadItemList,
        error: HttpErrorResponse
      ) => {
        return new InterBoutiqueTransferActions.LoadItemListFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() updateItemList$: Observable<Action> = this.dataPersistence.fetch(
    InterBoutiqueTransferActionTypes.UPDATE_ITEM_LIST,
    {
      run: (
        action: InterBoutiqueTransferActions.UpdateItemList,
        state: InterBoutiqueTransferState
      ) => {
        return this.interBoutiqueTransferService
          .updateItemList(
            action.payload.id,
            action.payload.itemId,
            action.payload.requestGroup,
            action.payload.data
          )
          .pipe(
            map(
              (data: ItemList) =>
                new InterBoutiqueTransferActions.UpdateItemListSuccess(data)
            )
          );
      },

      onError: (
        action: InterBoutiqueTransferActions.UpdateItemList,
        error: HttpErrorResponse
      ) => {
        return new InterBoutiqueTransferActions.UpdateItemListFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() updateItemListStatus$: Observable<
    Action
  > = this.dataPersistence.fetch(
    InterBoutiqueTransferActionTypes.UPDATE_ITEM_LIST_STATUS,
    {
      run: (
        action: InterBoutiqueTransferActions.UpdateItemListStatus,
        state: InterBoutiqueTransferState
      ) => {
        return this.interBoutiqueTransferService
          .updateItemListStatus(
            action.payload.type,
            action.payload.id,
            action.payload.itemIds,
            action.payload.requestGroup,
            action.payload.remarks
          )
          .pipe(
            map(
              (data: RequestList) =>
                new InterBoutiqueTransferActions.UpdateItemListStatusSuccess(
                  data
                )
            )
          );
      },

      onError: (
        action: InterBoutiqueTransferActions.UpdateItemListStatus,
        error: HttpErrorResponse
      ) => {
        return new InterBoutiqueTransferActions.UpdateItemListStatusFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() searchItem$: Observable<Action> = this.dataPersistence.fetch(
    InterBoutiqueTransferActionTypes.SEARCH_ITEM,
    {
      run: (
        action: InterBoutiqueTransferActions.SearchItem,
        state: InterBoutiqueTransferState
      ) => {
        return this.itemDataService
          .getItemByCode(action.payload)
          .pipe(
            map(
              (data: Item) =>
                new InterBoutiqueTransferActions.SearchItemSuccess(
                  data.itemCode
                )
            )
          );
      },

      onError: (
        action: InterBoutiqueTransferActions.SearchItem,
        error: HttpErrorResponse
      ) => {
        return new InterBoutiqueTransferActions.SearchItemFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
}
