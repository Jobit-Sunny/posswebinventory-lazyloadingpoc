import { Action } from '@ngrx/store';
import {
  RequestList,
  ItemList,
  Request,
  BoutiqueListResponse,
  Item
} from '../models/inter-boutique-transfer.model';
import { CustomErrors } from '@poss-web/core';

export interface LoadRequestListPayload {
  requestGroup: string;
  pageIndex: number;
  pageSize: number;
}

export interface LoadBoutiqueListPayload {
  item: Item[];
  pageIndex: number;
  pageSize: number;
}
export interface LoadRequestPayload {
  id: number;
  requestGroup: string;
}
export interface LoadItemListPayload {
  id: number;
  requestGroup: string;
}
export interface UpdateItemListPayload {
  id: number;
  itemId: string;
  requestGroup: string;
  data: {
    quantity: number;
    status: string;
  };
}
export interface UpdateItemListStatusPayload {
  type: string;
  id: number;
  requestGroup: string;
  itemIds: string[];
  remarks: string;
}

export enum InterBoutiqueTransferActionTypes {
  LOAD_REQUEST_SENT_LIST = '[Inter-Boutique-Transfer] Load RequestSent List',
  LOAD_REQUEST_SENT_LIST_SUCCESS = '[Inter-Boutique-Transfer] Load RequestSent List Success',
  LOAD_REQUEST_SENT_LIST_FAILURE = '[Inter-Boutique-Transfer] Load RequestSent List Failure',

  LOAD_REQUEST_RECEIVED_LIST = '[Inter-Boutique-Transfer] Load RequestReceived List',
  LOAD_REQUEST_RECEIVED_LIST_SUCCESS = '[Inter-Boutique-Transfer] Load RequestReceived List Success',
  LOAD_REQUEST_RECEIVED_LIST_FAILURE = '[Inter-Boutique-Transfer] Load RequestReceived List Failure',

  LOAD_REQUEST_SENT_LIST_COUNT = '[Inter-Boutique-Transfer] Load RequestSent List Count',
  LOAD_REQUEST_SENT_LIST_COUNT_SUCCESS = '[Inter-Boutique-Transfer] Load RequestSent List Count Success',
  LOAD_REQUEST_SENT_LIST_COUNT_FAILURE = '[Inter-Boutique-Transfer] Load RequestSent List Count Failure',

  LOAD_REQUEST_RECEIVED_LIST_COUNT = '[Inter-Boutique-Transfer] Load RequestReceived List Count',
  LOAD_REQUEST_RECEIVED_LIST_COUNT_SUCCESS = '[Inter-Boutique-Transfer] Load RequestReceived List Count Success',
  LOAD_REQUEST_RECEIVED_LIST_COUNT_FAILURE = '[Inter-Boutique-Transfer] Load RequestReceived List Count Failure',

  LOAD_BOUTIQUE_LIST = '[Inter-Boutique-Transfer] Load Boutique List',
  LOAD_BOUTIQUE_LIST_SUCCESS = '[Inter-Boutique-Transfer] Load Boutique List Success',
  LOAD_BOUTIQUE_LIST_FAILURE = '[Inter-Boutique-Transfer] Load Boutique List Failure',

  CREATE_REQUEST = '[Inter-Boutique-Transfer] Create Request',
  CREATE_REQUEST_SUCCESS = '[Inter-Boutique-Transfer] Create Request Success',
  CREATE_REQUEST_FAILURE = '[Inter-Boutique-Transfer] Create Request Failure',

  LOAD_REQUEST = '[Inter-Boutique-Transfer] Load Request',
  LOAD_REQUEST_SUCCESS = '[Inter-Boutique-Transfer] Load Request Success',
  LOAD_REQUEST_FAILURE = '[Inter-Boutique-Transfer] Load Request Failure',

  LOAD_ITEM_LIST = '[Inter-Boutique-Transfer] Load Item List',
  LOAD_ITEM_LIST_SUCCESS = '[Inter-Boutique-Transfer] Load Item List Success',
  LOAD_ITEM_LIST_FAILURE = '[Inter-Boutique-Transfer] Load Item List Failure',

  UPDATE_ITEM_LIST = '[Inter-Boutique-Transfer] Update Item List',
  UPDATE_ITEM_LIST_SUCCESS = '[Inter-Boutique-Transfer] Update Item List Success',
  UPDATE_ITEM_LIST_FAILURE = '[Inter-Boutique-Transfer] Update Item List Failure',

  UPDATE_ITEM_LIST_STATUS = '[Inter-Boutique-Transfer] Update Item List Status',
  UPDATE_ITEM_LIST_STATUS_SUCCESS = '[Inter-Boutique-Transfer] Update Item List Status Success',
  UPDATE_ITEM_LIST_STATUS_FAILURE = '[Inter-Boutique-Transfer] Update Item List Status Failure',

  SEARCH_ITEM = '[Inter-Boutique-Transfer] Search Item',
  SEARCH_ITEM_SUCCESS = '[Inter-Boutique-Transfer] Search Item Success',
  SEARCH_ITEM_FAILURE = '[Inter-Boutique-Transfer] Search Item Failure',

  CLEAR_REQUEST_SENT_LIST = '[Inter-Boutique-Transfer] Clear RequestSent List',
  CLEAR_REQUEST_RECEIVED_LIST = '[Inter-Boutique-Transfer] Clear RequestReceived List',
  CLEAR_ITEM_LIST = '[Inter-Boutique-Transfer] Clear Item List',
  CLEAR_BOUTIQUE_LIST = '[Inter-Boutique-Transfer] Clear Boutique List',
  CLEAR_SEARCH_ITEM_RESPONSE = '[Inter-Boutique-Transfer] Clear Search Item Response',
  RESET_REQUEST_LIST = '[Inter-Boutique-Transfer] Reset Request List'
}

export class LoadRequestSentList implements Action {
  readonly type = InterBoutiqueTransferActionTypes.LOAD_REQUEST_SENT_LIST;
  constructor(public payload: LoadRequestListPayload) {}
}

export class LoadRequestSentListSuccess implements Action {
  readonly type =
    InterBoutiqueTransferActionTypes.LOAD_REQUEST_SENT_LIST_SUCCESS;
  constructor(public payload: RequestList[]) {}
}

export class LoadRequestSentListFailure implements Action {
  readonly type =
    InterBoutiqueTransferActionTypes.LOAD_REQUEST_SENT_LIST_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadRequestReceivedList implements Action {
  readonly type = InterBoutiqueTransferActionTypes.LOAD_REQUEST_RECEIVED_LIST;
  constructor(public payload: LoadRequestListPayload) {}
}

export class LoadRequestReceivedListSuccess implements Action {
  readonly type =
    InterBoutiqueTransferActionTypes.LOAD_REQUEST_RECEIVED_LIST_SUCCESS;
  constructor(public payload: RequestList[]) {}
}

export class LoadRequestReceivedListFailure implements Action {
  readonly type =
    InterBoutiqueTransferActionTypes.LOAD_REQUEST_RECEIVED_LIST_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadRequestSentListCount implements Action {
  readonly type = InterBoutiqueTransferActionTypes.LOAD_REQUEST_SENT_LIST_COUNT;
  constructor(public payload: string) {}
}

export class LoadRequestSentListCountSuccess implements Action {
  readonly type =
    InterBoutiqueTransferActionTypes.LOAD_REQUEST_SENT_LIST_COUNT_SUCCESS;
  constructor(public payload: number) {}
}

export class LoadRequestSentListCountFailure implements Action {
  readonly type =
    InterBoutiqueTransferActionTypes.LOAD_REQUEST_SENT_LIST_COUNT_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadRequestReceivedListCount implements Action {
  readonly type =
    InterBoutiqueTransferActionTypes.LOAD_REQUEST_RECEIVED_LIST_COUNT;
  constructor(public payload: string) {}
}

export class LoadRequestReceivedListCountSuccess implements Action {
  readonly type =
    InterBoutiqueTransferActionTypes.LOAD_REQUEST_RECEIVED_LIST_COUNT_SUCCESS;
  constructor(public payload: number) {}
}

export class LoadRequestReceivedListCountFailure implements Action {
  readonly type =
    InterBoutiqueTransferActionTypes.LOAD_REQUEST_RECEIVED_LIST_COUNT_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadBoutiqueList implements Action {
  readonly type = InterBoutiqueTransferActionTypes.LOAD_BOUTIQUE_LIST;
  constructor(public payload: LoadBoutiqueListPayload) {}
}

export class LoadBoutiqueListSuccess implements Action {
  readonly type = InterBoutiqueTransferActionTypes.LOAD_BOUTIQUE_LIST_SUCCESS;
  constructor(public payload: BoutiqueListResponse) {}
}

export class LoadBoutiqueListFailure implements Action {
  readonly type = InterBoutiqueTransferActionTypes.LOAD_BOUTIQUE_LIST_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class CreateRequest implements Action {
  readonly type = InterBoutiqueTransferActionTypes.CREATE_REQUEST;
  constructor(public payload: Request) {}
}

export class CreateRequestSuccess implements Action {
  readonly type = InterBoutiqueTransferActionTypes.CREATE_REQUEST_SUCCESS;
  constructor(public payload: RequestList) {}
}

export class CreateRequestFailure implements Action {
  readonly type = InterBoutiqueTransferActionTypes.CREATE_REQUEST_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadRequest implements Action {
  readonly type = InterBoutiqueTransferActionTypes.LOAD_REQUEST;
  constructor(public payload: LoadRequestPayload) {}
}

export class LoadRequestSuccess implements Action {
  readonly type = InterBoutiqueTransferActionTypes.LOAD_REQUEST_SUCCESS;
  constructor(public payload: RequestList) {}
}

export class LoadRequestFailure implements Action {
  readonly type = InterBoutiqueTransferActionTypes.LOAD_REQUEST_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadItemList implements Action {
  readonly type = InterBoutiqueTransferActionTypes.LOAD_ITEM_LIST;
  constructor(public payload: LoadItemListPayload) {}
}

export class LoadItemListSuccess implements Action {
  readonly type = InterBoutiqueTransferActionTypes.LOAD_ITEM_LIST_SUCCESS;
  constructor(public payload: ItemList[]) {}
}

export class LoadItemListFailure implements Action {
  readonly type = InterBoutiqueTransferActionTypes.LOAD_ITEM_LIST_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class UpdateItemList implements Action {
  readonly type = InterBoutiqueTransferActionTypes.UPDATE_ITEM_LIST;
  constructor(public payload: UpdateItemListPayload) {}
}

export class UpdateItemListSuccess implements Action {
  readonly type = InterBoutiqueTransferActionTypes.UPDATE_ITEM_LIST_SUCCESS;
  constructor(public payload: ItemList) {}
}

export class UpdateItemListFailure implements Action {
  readonly type = InterBoutiqueTransferActionTypes.UPDATE_ITEM_LIST_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class UpdateItemListStatus implements Action {
  readonly type = InterBoutiqueTransferActionTypes.UPDATE_ITEM_LIST_STATUS;
  constructor(public payload: UpdateItemListStatusPayload) {}
}

export class UpdateItemListStatusSuccess implements Action {
  readonly type =
    InterBoutiqueTransferActionTypes.UPDATE_ITEM_LIST_STATUS_SUCCESS;
  constructor(public payload: RequestList) {}
}

export class UpdateItemListStatusFailure implements Action {
  readonly type =
    InterBoutiqueTransferActionTypes.UPDATE_ITEM_LIST_STATUS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class SearchItem implements Action {
  readonly type = InterBoutiqueTransferActionTypes.SEARCH_ITEM;
  constructor(public payload: string) {}
}

export class SearchItemSuccess implements Action {
  readonly type = InterBoutiqueTransferActionTypes.SEARCH_ITEM_SUCCESS;
  constructor(public payload: string) {}
}

export class SearchItemFailure implements Action {
  readonly type = InterBoutiqueTransferActionTypes.SEARCH_ITEM_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class ClearRequestSentList implements Action {
  readonly type = InterBoutiqueTransferActionTypes.CLEAR_REQUEST_SENT_LIST;
}

export class ClearRequestReceivedList implements Action {
  readonly type = InterBoutiqueTransferActionTypes.CLEAR_REQUEST_RECEIVED_LIST;
}

export class ClearItemList implements Action {
  readonly type = InterBoutiqueTransferActionTypes.CLEAR_ITEM_LIST;
}

export class ClearBoutiqueList implements Action {
  readonly type = InterBoutiqueTransferActionTypes.CLEAR_BOUTIQUE_LIST;
}

export class ClearSearchItemResponse implements Action {
  readonly type = InterBoutiqueTransferActionTypes.CLEAR_SEARCH_ITEM_RESPONSE;
}

export class ResetRequestList implements Action {
  readonly type = InterBoutiqueTransferActionTypes.RESET_REQUEST_LIST;
}

export type InterBoutiqueTransferActions =
  | LoadRequestSentList
  | LoadRequestSentListSuccess
  | LoadRequestSentListFailure
  | LoadRequestReceivedList
  | LoadRequestReceivedListSuccess
  | LoadRequestReceivedListFailure
  | LoadRequestSentListCount
  | LoadRequestSentListCountSuccess
  | LoadRequestSentListCountFailure
  | LoadRequestReceivedListCount
  | LoadRequestReceivedListCountSuccess
  | LoadRequestReceivedListCountFailure
  | LoadBoutiqueList
  | LoadBoutiqueListSuccess
  | LoadBoutiqueListFailure
  | CreateRequest
  | CreateRequestSuccess
  | CreateRequestFailure
  | LoadRequest
  | LoadRequestSuccess
  | LoadRequestFailure
  | LoadItemList
  | LoadItemListSuccess
  | LoadItemListFailure
  | UpdateItemList
  | UpdateItemListSuccess
  | UpdateItemListFailure
  | UpdateItemListStatus
  | UpdateItemListStatusSuccess
  | UpdateItemListStatusFailure
  | SearchItem
  | SearchItemSuccess
  | SearchItemFailure
  | ClearRequestSentList
  | ClearRequestReceivedList
  | ClearItemList
  | ClearBoutiqueList
  | ClearSearchItemResponse
  | ResetRequestList;
