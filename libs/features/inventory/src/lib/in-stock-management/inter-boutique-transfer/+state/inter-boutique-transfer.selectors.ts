import { createSelector } from '@ngrx/store';
import { selectInventory } from '../../../inventory.state';
import {
  requestListSelector,
  itemListSelector
} from './inter-boutique-transfer.entity';

const requestSentList = createSelector(
  selectInventory,
  state => state.interBoutiqueTransfer.requestSentList
);

const selectRequestSentList = createSelector(
  requestSentList,
  requestListSelector.selectAll
);

const requestReceivedList = createSelector(
  selectInventory,
  state => state.interBoutiqueTransfer.requestReceivedList
);

const selectRequestReceivedList = createSelector(
  requestReceivedList,
  requestListSelector.selectAll
);

const selectRequestSentListCount = createSelector(
  selectInventory,
  state => state.interBoutiqueTransfer.requestSentListCount
);

const selectRequestReceivedListCount = createSelector(
  selectInventory,
  state => state.interBoutiqueTransfer.requestReceivedListCount
);

const selectBoutiqueList = createSelector(
  selectInventory,
  state => state.interBoutiqueTransfer.boutiqueList
);

const selectCreateRequestResponse = createSelector(
  selectInventory,
  state => state.interBoutiqueTransfer.createRequestResponse
);

const selectRequest = createSelector(
  selectInventory,
  state => state.interBoutiqueTransfer.request
);

const itemList = createSelector(
  selectInventory,
  state => state.interBoutiqueTransfer.itemList
);

const selectItemList = createSelector(
  itemList,
  itemListSelector.selectAll
);

const selectUpdateItemListResponse = createSelector(
  selectInventory,
  state => state.interBoutiqueTransfer.updateItemListResponse
);

const selectUpdateItemListStatusResponse = createSelector(
  selectInventory,
  state => state.interBoutiqueTransfer.updateItemListStatusResponse
);

const selectSearchItemResponse = createSelector(
  selectInventory,
  state => state.interBoutiqueTransfer.searchItemResponse
);

const selectHasError = createSelector(
  selectInventory,
  state => state.interBoutiqueTransfer.hasError
);

const selectIsLoading = createSelector(
  selectInventory,
  state => state.interBoutiqueTransfer.isLoading
);

export const InterBoutiqueTransferSelectors = {
  selectRequestSentList,
  selectRequestReceivedList,
  selectRequestSentListCount,
  selectRequestReceivedListCount,
  selectBoutiqueList,
  selectCreateRequestResponse,
  selectRequest,
  selectItemList,
  selectUpdateItemListResponse,
  selectUpdateItemListStatusResponse,
  selectSearchItemResponse,
  selectHasError,
  selectIsLoading
};
