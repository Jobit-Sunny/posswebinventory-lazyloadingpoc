import {
  getRequestListByPaginationEndpointUrl,
  updateItemListStatusEndpointUrl,
  updateItemListEndpointUrl,
  getItemListEndpointUrl,
  getRequestEndpointUrl,
  getCreateRequestListEndpointUrl,
  getBoutiqueListByPaginationEndpointUrl
} from '../../../endpoints.constants';
import { ApiService } from '@poss-web/core';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { InterBoutiqueTransferHelper } from '../helpers/inter-boutique-transfer.helper';
import { Observable } from 'rxjs';
import {
  RequestList,
  ItemList,
  Item,
  BoutiqueListResponse
} from '../models/inter-boutique-transfer.model';
import { InterBoutiqueTransferAdaptor } from '../adaptors/inter-boutique-transfer.adaptor';

@Injectable()
export class InterBoutiqueTransferService {
  constructor(private apiService: ApiService) {}
  getRequestList(
    type: string,
    pageIndex: number,
    pageSize: number
  ): Observable<RequestList[]> {
    const requestListUrl = getRequestListByPaginationEndpointUrl(
      type,
      pageIndex,
      pageSize
    );
    return this.apiService
      .get(requestListUrl)
      .pipe(
        map((data: any) => InterBoutiqueTransferHelper.getRequestList(data))
      );
  }

  getRequestCount(type: string): Observable<number> {
    const requestCountUrl = getRequestListByPaginationEndpointUrl(type, 0, 1);
    return this.apiService
      .get(requestCountUrl)
      .pipe(map((data: any) => data.totalElements));
  }

  getBoutiqueList(
    items: Array<Item>,
    pageIndex: number,
    pageSize: number
  ): Observable<BoutiqueListResponse> {
    const boutiqueListUrl = getBoutiqueListByPaginationEndpointUrl(
      pageIndex,
      pageSize
    );
    return this.apiService
      .post(boutiqueListUrl, { reqItems: items })
      .pipe(
        map((data: any) => InterBoutiqueTransferHelper.getBoutiqueList(data))
      );
  }

  getBoutiqueListCount(itemCode: string, quantity: number): Observable<number> {
    const boutiqueListUrl = getBoutiqueListByPaginationEndpointUrl(0, 1);
    return this.apiService
      .post(boutiqueListUrl, {
        reqItems: [{ itemCode: itemCode, quantity: quantity }]
      })
      .pipe(map((data: any) => data.totalElements));
  }

  createRequest(
    items: Array<Item>,
    remarks: string,
    srcLocationCode: string
  ): Observable<RequestList> {
    const createRequestUrl = getCreateRequestListEndpointUrl();
    return this.apiService
      .post(createRequestUrl, { items, remarks, srcLocationCode })
      .pipe(
        map((data: any) => InterBoutiqueTransferAdaptor.requestFromJson(data))
      );
  }

  getRequest(id: number, requestGroup: string): Observable<RequestList> {
    const selectedRequestDetailsUrl = getRequestEndpointUrl(id, requestGroup);
    return this.apiService
      .get(selectedRequestDetailsUrl)
      .pipe(
        map((data: any) => InterBoutiqueTransferAdaptor.requestFromJson(data))
      );
  }

  getItemList(id: number, requestGroup: string): Observable<ItemList[]> {
    const selectedRequestProductListUrl = getItemListEndpointUrl(
      id,
      requestGroup
    );

    return this.apiService
      .get(selectedRequestProductListUrl)
      .pipe(map((data: any) => InterBoutiqueTransferHelper.getItemList(data)));
  }

  updateItemList(
    id: number,
    itemId: string,
    requestGroup: string,
    itemDetail: any
  ): Observable<ItemList> {
    const updateSelectedRequestProductListUrl = updateItemListEndpointUrl(
      id,
      itemId,
      requestGroup
    );
    return this.apiService
      .patch(updateSelectedRequestProductListUrl, itemDetail)
      .pipe(
        map((data: any) => InterBoutiqueTransferAdaptor.itemFromJson(data))
      );
  }

  updateItemListStatus(
    status: string,
    id: number,
    itemIds: Array<string>,
    requestGroup: string,
    remarks: string
  ): Observable<RequestList> {
    const updateSelectedRequestProductListStatusUrl = updateItemListStatusEndpointUrl(
      id,
      requestGroup
    );

    return this.apiService
      .patch(updateSelectedRequestProductListStatusUrl, {
        itemIds,
        requestGroup,
        remarks,
        status
      })
      .pipe(
        map((data: any) => InterBoutiqueTransferAdaptor.requestFromJson(data))
      );
  }
}
