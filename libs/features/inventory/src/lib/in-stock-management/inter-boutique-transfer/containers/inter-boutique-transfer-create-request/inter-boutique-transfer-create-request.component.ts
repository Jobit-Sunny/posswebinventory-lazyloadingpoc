import {
  Component,
  OnInit,
  AfterViewInit,
  ElementRef,
  ViewChild,
  OnDestroy,
  QueryList,
  ViewChildren
} from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { fromEvent, Subject, Observable } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { InterBoutiqueTransferFacade } from '../../+state/inter-boutique-transfer.facade';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationEventType
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { CustomErrors, ShortcutService, Command } from '@poss-web/core';
import {
  RequestList,
  BoutiqueList,
  Request,
  BoutiqueListResponse
} from '../../models/inter-boutique-transfer.model';
import { MatExpansionPanel } from '@angular/material';
import { Router } from '@angular/router';
import { getInterBoutiqueTransferRouteUrl } from '../../../../page-route.constants';

const SEARCH_SHORTCUT_KEY_F2 = 'InterBoutiqueTransferCreateRequestComponent.F2';
@Component({
  selector: 'poss-web-inter-boutique-transfer-create-request',
  templateUrl: './inter-boutique-transfer-create-request.component.html',
  styleUrls: ['./inter-boutique-transfer-create-request.component.scss']
})
export class InterBoutiqueTransferCreateRequestComponent
  implements OnInit, AfterViewInit, OnDestroy {
  boutiqueList$: Observable<BoutiqueListResponse>;
  createRequestResponse$: Observable<RequestList>;
  searchItemResponse$: Observable<string>;

  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;
  @ViewChildren(MatExpansionPanel) viewPanels: QueryList<MatExpansionPanel>;

  searchForm: FormGroup;
  itemForm: FormGroup;
  itemList: FormArray;
  request: Request;
  boutiqueList: BoutiqueList[];
  boutiqueListCount: number;

  searchedItems = [''];
  hasNotification = false;
  isLoading = false;
  pageSize = 4;
  destroy$: Subject<null> = new Subject<null>();
  IBTDefaultPath = 'SENT';

  panelOpenState = false;
  _allExpandState = false;

  validationMessages = {
    searchValue: [{ type: 'pattern', message: 'Invalid VariantCode' }],
    quantity: [
      { type: 'required', message: 'Required' },
      { type: 'pattern', message: 'Invalid Quantity' }
    ]
  };

  constructor(
    private formBuilder: FormBuilder,
    private interBoutiqueTransferFacade: InterBoutiqueTransferFacade,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService,
    private router: Router,
    private shortcutService: ShortcutService
  ) {
    this.searchForm = this.formBuilder.group({
      searchValue: [
        null,
        Validators.compose([Validators.pattern('[a-zA-Z0-9]{1,14}')])
      ]
    });
    /**
     * creates a form group with form array
     * form array contains array of form groups
     */
    this.itemForm = this.formBuilder.group({
      searchItem: this.formBuilder.array([])
    });
    /**
     * set searchList to the form control containing search
     */
    this.itemList = this.itemForm.get('searchItem') as FormArray;
    this.itemList.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.resetBoutiqueList();
    });
  }

  ngOnInit() {
    this.interBoutiqueTransferFacade.resetRequestList();
    this.componentInit();
    this.shortcutService.commands
      .pipe(takeUntil(this.destroy$))
      .subscribe(command => this.shortcutEventHandler(command));
  }

  ngAfterViewInit() {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe((event: any) => {
        this.overlayNotification.close();
        const searchValue = this.searchForm.get('searchValue').value;
        if (searchValue !== '') {
          this.searchItems(searchValue);
        } else if (searchValue === '') {
          this.clearSearch();
        }
      });
  }

  /**
   * method to handle shortcut commands
   * @param command: shortcut command
   */
  shortcutEventHandler(command: Command) {
    if (command.name === SEARCH_SHORTCUT_KEY_F2) {
      if (this.searchBox) {
        this.searchBox.nativeElement.focus();
      }
    }
  }

  componentInit() {
    this.searchItemResponse$ = this.interBoutiqueTransferFacade.getSearchItemResponse();
    this.boutiqueList$ = this.interBoutiqueTransferFacade.getBoutiqueList();
    this.createRequestResponse$ = this.interBoutiqueTransferFacade.getcreateRequestResponse();

    this.interBoutiqueTransferFacade
      .getHasError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.interBoutiqueTransferFacade
      .getIsLoading()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isLoading: boolean) => {
        this.isLoading = isLoading;
      });

    this.searchItemResponse$.pipe(takeUntil(this.destroy$)).subscribe(data => {
      if (data !== null) {
        if (!this.searchedItems.includes(data)) {
          this.itemList.push(this.createItemList(data));
          this.allExpandState = true;
          this.searchedItems.push(data);
          this.clearSearch();
        }
      }
    });

    this.boutiqueList$.pipe(takeUntil(this.destroy$)).subscribe(data => {
      this.boutiqueList = [];
      this.boutiqueListCount = 0;
      if (data !== null) {
        this.boutiqueList = data.boutiqueList;
        this.boutiqueListCount = data.totalElements;
        if (this.boutiqueListCount === 0) {
          this.InterBoutiqueTransferBoutiqueListNotifications();
        }
      }
    });

    this.createRequestResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data !== null) {
          if (data.reqDocNo) {
            this.InterBoutiqueTransferSendRequestSuccessNotifications(
              data.reqDocNo
            );
          }
        }
      });
  }

  createItemList(searchValue: string): FormGroup {
    return this.formBuilder.group({
      itemCode: [searchValue],
      quantity: [
        1,
        Validators.compose([
          Validators.required,
          Validators.pattern('^[1-9][0-9]*$')
        ])
      ]
    });
  }

  /**
   * to get into exact field
   * @param index index of particular searchForm field
   */
  getSearchItemFormGroup(index: number): FormGroup {
    this.itemList = this.itemForm.get('searchItem') as FormArray;
    const formGroup = this.itemList.controls[index] as FormGroup;
    return formGroup;
  }

  /**
   *remove Search from group
   */
  removeSearchedItem(index: number) {
    this.itemList.removeAt(index);
    this.searchedItems.splice(index + 1, 1);
    if (this.itemList.length === 0) {
      this.allExpandState = false;
      this.clearSearch();
    }
    this.resetBoutiqueList();
  }

  /**
   * Method to check quantity input field
   */
  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  increaseQuantity(i: number) {
    let quantity = this.itemList.controls[i].value.quantity;
    quantity = quantity + 1;
    this.itemList.controls[i].patchValue({ quantity: quantity });
  }

  decreaseQuantity(i: number) {
    let quantity = this.itemList.controls[i].value.quantity;
    if (quantity > 1) {
      quantity = quantity - 1;
    }
    this.itemList.controls[i].patchValue({ quantity: quantity });
  }

  searchItems(searchValue: string) {
    if (this.searchForm.valid) {
      this.interBoutiqueTransferFacade.loadSearchItem(searchValue);
    }
  }

  checkAvailability() {
    this.loadBoutiques(0);
  }

  /**
   * To send request to the selected boutique
   *  @param boutique: selected boutique by user to send request
   */
  onSelectedBoutique(boutique: BoutiqueList) {
    this.request = {
      items: this.itemList.value,
      remarks: '',
      srcLocationCode: boutique.locationCode
    };
    this.InterBoutiqueTransferSendRequestNotifications();
  }

  /**
   * Notification overlay for sending request
   */
  InterBoutiqueTransferSendRequestNotifications() {
    const key = 'pw.interBoutiqueTransferNotifications.sendRequestMsg';
    const buttonKey = 'pw.instock.sendRequestButtonText';
    this.translate
      .get([key, buttonKey])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            message: translatedMessages[key],
            buttonText: translatedMessages[buttonKey],
            hasBackdrop: true,
            hasRemarks: true,
            isRemarksMandatory: true,
            hasClose: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.request.remarks = event.data;
              this.interBoutiqueTransferFacade.createRequest(this.request);
            }
          });
      });
  }

  /**
   * Notification overlay for sending request success message
   */
  InterBoutiqueTransferSendRequestSuccessNotifications(docNum: number) {
    const key = 'pw.interBoutiqueTransferNotifications.requestSendSection';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.SUCCESS,
            message: translatedMessage + ' ' + docNum,
            hasBackdrop: true,
            hasClose: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.CLOSE) {
              this.resetValue();
            }
          });
      });
  }

  /**
   * empty boutique list notifications
   */
  InterBoutiqueTransferBoutiqueListNotifications() {
    const key =
      'pw.interBoutiqueTransferNotifications.emptyBoutiqueListNotifications';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification.show({
          type: OverlayNotificationType.SIMPLE,
          message: translatedMessage,
          hasClose: true,
          hasBackdrop: true
        });
      });
  }

  /**
   * Load Requests data based on the type of the request
   * @param pageIndex: page number index
   */
  loadBoutiques(pageIndex: number): void {
    if (!this.isLoading) {
      this.interBoutiqueTransferFacade.loadBoutiqueList({
        item: this.itemForm.value.searchItem,
        pageIndex: pageIndex,
        pageSize: this.pageSize
      });
    }
  }

  clearSearch() {
    this.searchForm.reset();
    this.interBoutiqueTransferFacade.clearSearchItemResponse();
  }

  resetValue() {
    this.itemForm.reset();
    this.itemList.clear();
    this.clearSearch();
    this.searchedItems = [];
    this.resetBoutiqueList();
    this.allExpandState = false;
  }

  resetBoutiqueList() {
    this.boutiqueList = [];
    this.boutiqueListCount = 0;
    this.interBoutiqueTransferFacade.clearBoutiqueList();
  }

  back() {
    this.router.navigate([
      getInterBoutiqueTransferRouteUrl(this.IBTDefaultPath)
    ]);
  }

  /**
   * to display error message
   * @param error : error from api
   */
  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true, // optional
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        // Action based event
      });
  }

  public set allExpandState(value: boolean) {
    this._allExpandState = value;
    this.togglePanels(value);
  }

  public get allExpandState(): boolean {
    return this._allExpandState;
  }

  public togglePanels(value: boolean) {
    this.viewPanels.forEach(p => (value ? p.open() : p.close()));
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
