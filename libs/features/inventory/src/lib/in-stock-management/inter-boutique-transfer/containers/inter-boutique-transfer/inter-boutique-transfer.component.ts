import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { InterBoutiqueTransferFacade } from '../../+state/inter-boutique-transfer.facade';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { CustomErrors } from '@poss-web/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  getInStockRouteUrl,
  getIBTCreateRequestRouteUrl
} from '../../../../page-route.constants';
import { commonTranslateKeyMap } from '../../../../common.map';
import { InterBoutiqueTransferRequestTypesEnum } from '../../models/inter-boutique-transfer.enum';
import { RequestList } from '../../models/inter-boutique-transfer.model';

@Component({
  selector: 'poss-web-inter-boutique-transfer',
  templateUrl: './inter-boutique-transfer.component.html',
  styleUrls: ['./inter-boutique-transfer.component.scss']
})
export class InterBoutiqueTransferComponent implements OnInit, OnDestroy {
  requestSentListCount$: Observable<number>;
  requestReceivedListCount$: Observable<number>;
  requestSentList$: Observable<RequestList[]>;
  requestReceivedList$: Observable<RequestList[]>;

  interBoutiqueTransferRequestType: InterBoutiqueTransferRequestTypesEnum;
  requestType: InterBoutiqueTransferRequestTypesEnum;
  interBoutiqueTransferRequestTypesEnumRef = InterBoutiqueTransferRequestTypesEnum;

  requestId: number;
  status: string;
  statusColor: string;

  pageSize = 4;
  initalPageSize = 8;
  requestSentListLoadedOnce = true;
  requestReceivedListLoadedOnce = true;
  isLoading = false;
  hasNotification = false;

  destroy$: Subject<null> = new Subject<null>();

  constructor(
    private interBoutiqueTransferFacade: InterBoutiqueTransferFacade,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.interBoutiqueTransferFacade.resetRequestList();
    this.requestType = this.activatedRoute.snapshot.params['requestType'];
    this.changeInterBoutiqueTransferRequestType(this.requestType);
    this.componentInit();
  }

  /**
   * to load and get data
   */
  componentInit(): void {
    this.requestSentListCount$ = this.interBoutiqueTransferFacade.getRequestSentListCount();
    this.requestReceivedListCount$ = this.interBoutiqueTransferFacade.getRequestReceivedListCount();
    this.requestSentList$ = this.interBoutiqueTransferFacade.getRequestSentList();
    this.requestReceivedList$ = this.interBoutiqueTransferFacade.getRequestReceivedList();

    this.interBoutiqueTransferFacade.loadRequestSentListCount(
      InterBoutiqueTransferRequestTypesEnum.SENT
    );
    this.interBoutiqueTransferFacade.loadRequestReceivedListCount(
      InterBoutiqueTransferRequestTypesEnum.RECEIVED
    );
    this.interBoutiqueTransferFacade
      .getIsLoading()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isLoading: boolean) => {
        this.isLoading = isLoading;
      });
    this.interBoutiqueTransferFacade
      .getHasError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });
  }

  inStockUrl() {
    this.router.navigate([getInStockRouteUrl()]);
  }
  /**
   * Switch Between different types of request list
   * @param newInterBoutiqueTransferRequestType : new Request Type
   */
  changeInterBoutiqueTransferRequestType(
    newInterBoutiqueTransferRequestType: InterBoutiqueTransferRequestTypesEnum
  ): void {
    if (
      this.interBoutiqueTransferRequestType !==
      newInterBoutiqueTransferRequestType
    ) {
      this.interBoutiqueTransferRequestType = newInterBoutiqueTransferRequestType;
      this.router.navigate(['..', newInterBoutiqueTransferRequestType], {
        relativeTo: this.activatedRoute
      });
      if (
        (this.interBoutiqueTransferRequestType ===
          InterBoutiqueTransferRequestTypesEnum.SENT &&
          this.requestSentListLoadedOnce) ||
        (this.interBoutiqueTransferRequestType ===
          InterBoutiqueTransferRequestTypesEnum.RECEIVED &&
          this.requestReceivedListLoadedOnce)
      ) {
        this.loadRequests(0);
      }
    }
  }

  /**
   * Load Requests data based on the type of the request
   * @param pageIndex: page number index
   */
  loadRequests(pageIndex: number): void {
    if (!this.isLoading) {
      if (
        this.interBoutiqueTransferRequestType ===
        InterBoutiqueTransferRequestTypesEnum.SENT
      ) {
        this.interBoutiqueTransferFacade.loadRequestSentList({
          requestGroup: this.interBoutiqueTransferRequestTypesEnumRef.SENT,
          pageIndex: pageIndex,
          pageSize: this.requestSentListLoadedOnce
            ? this.initalPageSize
            : this.pageSize
        });
        this.requestSentListLoadedOnce = false;
      } else if (
        this.interBoutiqueTransferRequestType ===
        InterBoutiqueTransferRequestTypesEnum.RECEIVED
      ) {
        this.interBoutiqueTransferFacade.loadRequestReceivedList({
          requestGroup: this.interBoutiqueTransferRequestTypesEnumRef.RECEIVED,
          pageIndex: pageIndex,
          pageSize: this.requestReceivedListLoadedOnce
            ? this.initalPageSize
            : this.pageSize
        });
        this.requestReceivedListLoadedOnce = false;
      }
    }
  }

  /**
   * On click of request,request id is captured to load list of products in that request by routing
   * @param request: contains details of particular request
   */
  onRequestSelected(request: RequestList): void {
    this.requestId = request.id;
    this.router.navigate([this.requestId], {
      relativeTo: this.activatedRoute
    });
  }

  /**
   * to create a new request
   */
  createRequest(): void {
    this.router.navigate([getIBTCreateRequestRouteUrl()]);
  }

  /**
   * gets the status of the request and determines the color code and status description
   * @param status : status of the request
   */
  getStatus(status: string) {
    let key;
    if (commonTranslateKeyMap.has(status)) {
      key = commonTranslateKeyMap.get(status);
    }
    this.translate
      .get([key.status, key.statusColor])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: string) => {
        this.status = translatedMessages[key.status];
        this.statusColor = translatedMessages[key.statusColor];
      });
    return this.statusColor;
  }

  /**
   * to display error message
   * @param error : error from api
   */
  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true, // optional
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        // Action based event
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
