import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { InterBoutiqueTransferFacade } from '../../+state/inter-boutique-transfer.facade';
import {
  OverlayNotificationService,
  OverlayNotificationEventType,
  OverlayNotificationType,
  OverlayNotificationEventRef
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { commonTranslateKeyMap } from '../../../../common.map';
import {
  InterBoutiqueTransferRequestTypesEnum,
  InterBoutiqueTransferStatusTypesEnum
} from '../../models/inter-boutique-transfer.enum';
import {
  RequestList,
  ItemList,
  IsSelectedData,
  IsSelectedItemCode,
  IsSelectedItem
} from '../../models/inter-boutique-transfer.model';
import { CustomErrors } from '@poss-web/core';
import { getInterBoutiqueTransferRouteUrl } from '../../../../page-route.constants';

@Component({
  selector: 'poss-web-inter-boutique-transfer-details',
  templateUrl: './inter-boutique-transfer-details.component.html',
  styleUrls: ['./inter-boutique-transfer-details.component.scss']
})
export class InterBoutiqueTransferDetailsComponent
  implements OnInit, OnDestroy {
  interBoutiqueTransferRequestTypesEnumRef = InterBoutiqueTransferRequestTypesEnum;

  request$: Observable<RequestList>;
  itemList$: Observable<ItemList[]>;
  updateItemListStatusResponse$: Observable<RequestList>;

  isSelectedArray: string[] = [];
  isSelectedItemCodeArray: string[] = [];
  selectedRequest: RequestList;
  requestId: number;
  requestType: string;
  status: string;
  statusColor: string;
  selectedRequestStatus: string;

  hasNotification = false;
  isLoading = false;

  selectedItemCode$: Subject<IsSelectedItemCode> = new Subject<
    IsSelectedItemCode
  >();
  destroy$: Subject<null> = new Subject<null>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private interBoutiqueTransferFacade: InterBoutiqueTransferFacade,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService,
    private router: Router
  ) {}

  ngOnInit() {
    this.interBoutiqueTransferFacade.resetRequestList();
    this.requestId = this.activatedRoute.snapshot.params['requestId'];
    this.requestType = this.activatedRoute.snapshot.params['requestType'];
    this.interBoutiqueTransferFacade.loadRequest({
      id: this.requestId,
      requestGroup: this.requestType
    });
    this.componentInit();
  }

  componentInit() {
    this.request$ = this.interBoutiqueTransferFacade.getRequest();
    this.itemList$ = this.interBoutiqueTransferFacade.getItemList();
    this.updateItemListStatusResponse$ = this.interBoutiqueTransferFacade.updateSelectedRequestProductListStatusResponse();

    this.interBoutiqueTransferFacade
      .getIsLoading()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isLoading: boolean) => {
        this.isLoading = isLoading;
      });

    this.interBoutiqueTransferFacade
      .getHasError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.request$.pipe(takeUntil(this.destroy$)).subscribe(data => {
      if (data) {
        this.selectedRequest = data;
        this.selectedRequestStatus = data.status;
        this.interBoutiqueTransferFacade.loadItemList({
          id: this.requestId,
          requestGroup: this.requestType
        });
      }
    });

    this.itemList$.pipe(takeUntil(this.destroy$)).subscribe(data => {
      if (data.length !== 0) {
        if (
          this.requestType === InterBoutiqueTransferRequestTypesEnum.SENT &&
          this.getCancelStatus(this.selectedRequestStatus)
        ) {
          this.InterBoutiqueTransferCancelNotifications();
        } else if (
          this.requestType === InterBoutiqueTransferRequestTypesEnum.RECEIVED &&
          this.selectedRequestStatus ===
            InterBoutiqueTransferStatusTypesEnum.REQUESTED
        ) {
          this.InterBoutiqueTransferAcceptRejectNotifications();
        }
      }
    });

    this.updateItemListStatusResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data !== null) {
          if (data.status) {
            this.InterBoutiqueTransferStatusNotifications(data.status);
          }
        }
      });
  }

  isSelectedChange(event: IsSelectedData) {
    if (event.isSelected) {
      if (this.isSelectedItemCodeArray.includes(event.itemCode)) {
        this.selectedItemCode$.next({
          isSelected: false,
          itemId: event.itemId
        });
      } else {
        this.isSelectedArray.push(event.itemId);
        this.isSelectedItemCodeArray.push(event.itemCode);
        this.interBoutiqueTransferFacade.updateItemList({
          id: this.requestId,
          itemId: event.itemId,
          requestGroup: this.requestType,
          data: {
            quantity: event.quantity,
            status: InterBoutiqueTransferStatusTypesEnum.ACCEPTED
          }
        });
      }
    } else {
      this.isSelectedArray.splice(
        this.isSelectedArray.indexOf(event.itemId),
        1
      );
      this.isSelectedItemCodeArray.splice(
        this.isSelectedItemCodeArray.indexOf(event.itemCode),
        1
      );
      this.interBoutiqueTransferFacade.updateItemList({
        id: this.requestId,
        itemId: event.itemId,
        requestGroup: this.requestType,
        data: {
          quantity: event.quantity,
          status: InterBoutiqueTransferStatusTypesEnum.ACPT_REJECTED
        }
      });
    }
  }

  /**
   * Notification overlay for accept and reject
   */
  InterBoutiqueTransferAcceptRejectNotifications() {
    const key = 'pw.interBoutiqueTransferNotifications.acceptRejectMsg';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.REQUEST_ACTION,
            message: translatedMessage,
            hasRemarks: true,
            isRemarksMandatory: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              if (this.isSelectedArray.length !== 0) {
                const acceptRequest = {
                  type: InterBoutiqueTransferStatusTypesEnum.ACCEPTED,
                  id: this.requestId,
                  requestGroup: this.requestType,
                  itemIds: this.isSelectedArray,
                  remarks: event.data
                };

                this.interBoutiqueTransferFacade.updateItemListStatus(
                  acceptRequest
                );
                this.resetValue();
              } else {
                this.InterBoutiqueTransferErrorNotifications();
              }
            }
            if (event.eventType === OverlayNotificationEventType.FALSE) {
              const rejectRequest = {
                type: InterBoutiqueTransferStatusTypesEnum.ACPT_REJECTED,
                id: this.requestId,
                requestGroup: this.requestType,
                itemIds: ['0'],
                remarks: event.data
              };
              this.interBoutiqueTransferFacade.updateItemListStatus(
                rejectRequest
              );
            }
          });
      });
  }

  /**
   * Notification overlay for cancel request
   */
  InterBoutiqueTransferCancelNotifications() {
    this.hasNotification = true;
    // const key = 'pw.interBoutiqueTransferNotifications.cancelNotificationsMsg';
    const buttonKey = 'pw.interboutiqueTransfer.cancelRequestButtonText';
    this.translate
      .get([buttonKey])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            message: '',
            buttonText: translatedMessages[buttonKey],
            hasRemarks: true,
            isRemarksMandatory: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === 1) {
              const cancelRequest = {
                type: InterBoutiqueTransferStatusTypesEnum.CANCELLED,
                id: this.requestId,
                requestGroup: this.requestType,
                itemIds: ['0'],
                remarks: event.data
              };

              this.interBoutiqueTransferFacade.updateItemListStatus(
                cancelRequest
              );
            }
          });
      });
  }

  /**
   * Notification overlay for status
   * @param status: status of request
   */
  InterBoutiqueTransferStatusNotifications(status: string) {
    this.hasNotification = true;
    const key =
      'pw.interBoutiqueTransferNotifications.statusResponseNotifications';
    let statusKey = {
      status: '',
      statusColor: ''
    };
    if (commonTranslateKeyMap.has(status)) {
      statusKey = commonTranslateKeyMap.get(status);
    }
    this.translate
      .get([key, statusKey.status])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            message:
              translatedMessages[key] +
              ' ' +
              translatedMessages[statusKey.status],
            hasBackdrop: true,
            hasClose: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.CLOSE) {
              this.back();
            }
          });
      });
  }

  InterBoutiqueTransferErrorNotifications() {
    const key =
      'pw.interBoutiqueTransferNotifications.selectItemsNotifications';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.SIMPLE,
            message: translatedMessage,
            hasBackdrop: true,
            hasClose: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.CLOSE) {
              this.InterBoutiqueTransferAcceptRejectNotifications();
            }
          });
      });
  }

  /**
   * gets the status of the request and determines the color code and status description
   * @param status : status of the request
   */
  getStatus(status?: string) {
    let key = {
      status: '',
      statusColor: ''
    };
    if (commonTranslateKeyMap.has(status)) {
      key = commonTranslateKeyMap.get(status);
    }
    this.translate
      .get([key.status, key.statusColor])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: string) => {
        this.status = translatedMessages[key.status];
        this.statusColor = translatedMessages[key.statusColor];
      });
    return this.statusColor;
  }

  getCancelStatus(status: string): boolean {
    switch (status) {
      case InterBoutiqueTransferStatusTypesEnum.REQUESTED:
      case InterBoutiqueTransferStatusTypesEnum.ACCEPTED:
      case InterBoutiqueTransferStatusTypesEnum.APPROVED:
      case InterBoutiqueTransferStatusTypesEnum.APVL_PENDING:
      case InterBoutiqueTransferStatusTypesEnum.SELECTED:
        return true;
      case InterBoutiqueTransferStatusTypesEnum.ACPT_REJECTED:
      case InterBoutiqueTransferStatusTypesEnum.APVL_REJECTED:
      case InterBoutiqueTransferStatusTypesEnum.CANCELLED:
      case InterBoutiqueTransferStatusTypesEnum.EXPIRED:
      case InterBoutiqueTransferStatusTypesEnum.ISSUED:
        return false;
    }
  }

  /**
   * to display error message
   * @param error : error from api
   */
  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true, // optional
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        // Action based event
      });
  }

  isSelectedItemChange(event: IsSelectedItem) {
    this.isSelectedItemCodeArray.push(event.itemCode);
    this.isSelectedArray.push(event.itemId);
  }

  /**
   * to go back to previous page
   */
  back() {
    this.router.navigate([getInterBoutiqueTransferRouteUrl(this.requestType)]);
  }

  /**
   * to reset values when component loads
   */
  resetValue() {
    this.isSelectedArray = [];
    this.isSelectedItemCodeArray = [];
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
