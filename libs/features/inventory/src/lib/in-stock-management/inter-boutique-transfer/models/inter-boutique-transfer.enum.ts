/**
 * Inter-boutique-transfer enum types
 */
export enum InterBoutiqueTransferRequestTypesEnum {
  SENT = 'SENT',
  RECEIVED = 'RECEIVED'
}

export enum InterBoutiqueTransferStatusTypesEnum {
  ACCEPTED = 'ACCEPTED',
  ACPT_REJECTED = 'ACPT_REJECTED',
  APPROVED = 'APPROVED',
  APVL_REJECTED = 'APVL_REJECTED',
  REQUESTED = 'REQUESTED',
  APVL_PENDING = 'APVL_PENDING',
  CANCELLED = 'CANCELLED',
  EXPIRED = 'EXPIRED',
  SELECTED = 'SELECTED',
  ISSUED = 'ISSUED'
}

export enum InterBoutiqueTransferStatusColorsEnum {
  REQUESTED = 'pw-seventh-color',
  ACCEPTED = 'pw-success-color',
  ACPT_REJECTED = 'pw-error-color',
  APPROVED = 'pw-success-color',
  APVL_PENDING = 'pw-warning-color',
  APVL_REJECTED = 'pw-error-color',
  CANCELLED = 'pw-error-color',
  EXPIRED = 'pw-error-color',
  SELECTED = 'pw-success-color',
  ISSUED = 'pw-success-color'
}
