import { Moment } from 'moment';

export interface RequestList {
  id: number;
  reqDocNo: number;
  srcLocationCode: string;
  destLocationCode: string;
  totalRequestedQuantity: number;
  acceptedQuantity: number;
  approvedQuantity: number;
  status: string;
  reqDocDate: Moment;
  requestType: string;
}

export interface Request {
  items: Item[];
  remarks: string;
  srcLocationCode: string;
}

export interface Item {
  itemCode: string;
  quantity: number;
}

export interface BoutiqueList {
  locationCode: string;
  address: string;
  contactNumber: string;
}

export interface BoutiqueListResponse {
  boutiqueList: BoutiqueList[];
  totalElements: number;
}

export interface ItemList {
  id: string;
  itemCode: string;
  lotNumber: string;
  mfgDate: Moment;
  productCategory: string;
  productGroup: string;
  binCode: string;
  binGroupCode: string;
  stdValue: number;
  stdWeight: number;
  currencyCode: string;
  weightUnit: string;
  status: string;
  imageURL: string;
  itemDetails: {};
  requestedQuantity: number;
  requestedWeight: number;
  acceptedQuantity: number;
  approvedQuantity: number;
  availableQuantity: number;
  inventoryId: string;
  totalAcceptedQuantity: number;
}

export interface IsSelectedData {
  itemId: string;
  isSelected: boolean;
  itemCode: string;
  quantity: number;
}

export interface IsSelectedItemCode {
  isSelected: boolean;
  itemId: string;
}

export interface IsSelectedItem {
  itemId: string;
  itemCode: string;
}
