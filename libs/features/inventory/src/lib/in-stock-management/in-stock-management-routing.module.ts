import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InStockComponent } from './in-stock/in-stock.component';
import { AuthGuard } from '@poss-web/core';

const routes: Routes = [
  {
    path: '',
    component: InStockComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'bintobinTransfer',
    loadChildren: () =>
      import('./bin-to-bin-transfer/bin-to-bin-transfer.module').then(
        m => m.BinToBinTransferModule
      ),
    canActivate: [AuthGuard]
  },
  {
    path: 'instock/inter-boutique-transfer',
    loadChildren: () =>
      import('./inter-boutique-transfer/inter-boutique-transfer.module').then(
        m => m.InterBoutiqueTransferModule
      ),
    canActivate: [AuthGuard]
  },
  {
    path: 'instock/conversion',
    loadChildren: () =>
      import('./conversion/conversion.module').then(m => m.ConversionModule),
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InStockManagementRoutingModule {}
