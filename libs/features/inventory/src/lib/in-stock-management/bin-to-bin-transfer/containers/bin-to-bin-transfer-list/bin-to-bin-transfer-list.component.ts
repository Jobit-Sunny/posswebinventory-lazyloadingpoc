import {
  SELECT_BY_KEY_MAP,
  SELECT_BY_OPTIONS,
  SORT_DATA,
  BIN_CODE_AND_GROUP_SEPARATOR
} from '../../models/bin-data';

import {
  ConfirmTransferItemsRequest,
  ConfirmTransferResponse,
  ItemListGroup,
  Item,
  FilterOption
} from '../../models/bin-to-bin-transfer.model';
import { BinToBinTransferTypeEnum } from '../../models/bin-to-bin-transfer.enum';
import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  OnDestroy
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationEventType,
  SearchResponse,
  SearchListComponent,
  SortDialogService,
  FilterService,
  Filter,
  SearchComponent,
  CardListComponent,
  Column,
  SelectionDialogService,
  Option
} from '@poss-web/shared';
import { takeUntil, filter } from 'rxjs/operators';
import { BinToBinTransferFacade } from '../../+state/bin-to-bin-transfer.facade';
import {
  CustomErrors,
  ShortcutService,
  Command,
  AppsettingFacade,
  StoreBin
} from '@poss-web/core';

const SEARCH_LIST_SHORTCUT_KEY = 'BinToBinTransferComponent.SEARCH_LIST';
// const SEARCH_SHORTCUT_KEY = 'BinToBinTransferComponent.SEARCH';
const CARD_LIST_SHORTCUT_KEY = 'BinToBinTransferComponent.CARD_LIST';
const DROP_DOWN_SHORTCUT_KEY = 'BinToBinTransferComponent.DROP_DOWN';

@Component({
  selector: 'poss-web-bin-to-bin-transfer-list',
  templateUrl: './bin-to-bin-transfer-list.component.html',
  styleUrls: ['./bin-to-bin-transfer-list.component.scss']
})
export class BinToBinTransferListComponent implements OnInit, OnDestroy {
  pageSize = 4;
  initalPageSize = 8;
  selectByKeyMap = SELECT_BY_KEY_MAP;
  selectByOptions = SELECT_BY_OPTIONS;
  itemList: Item[] = [];
  allItemList: Item[] = [];
  searchedItemList: Item[] = [];
  isSearchingItems$: Observable<boolean>;
  hasSearchedItems$: Observable<boolean>;
  selectedItems: Item[] = [];
  sendSelectAllEvent = true;
  hasNotification = false;
  isLoadingBins$: Observable<boolean>;
  bins: StoreBin[] = [];
  binsForSelection: Option[] = [];
  selectedBin: { binCode: string; binGroupCode: string };
  hasResults = null;
  sourceBins$: Observable<ItemListGroup[]>;
  sourceBinsTotalCount$: Observable<number>;
  productGroups$: Observable<ItemListGroup[]>;
  productGroupsTotalCount$: Observable<number>;
  productCategory$: Observable<ItemListGroup[]>;
  productCategoryTotalCount$: Observable<number>;
  searchedItemListGroups$: Observable<ItemListGroup[]>;
  searchedItemListGroupsTotalCount$: Observable<number>;
  itemGroups$: Observable<ItemListGroup[]>;
  itemGroupsTotalCount$: Observable<number>;
  isLoadingItemListGroup$: Observable<boolean>;
  confirmTransferResponse: ConfirmTransferResponse;
  isSourceBinsLoadedOnce = false;
  isProducCategoryLoadedOnce = false;
  isProductGroupsLoadedOnce = false;
  hasReachedMaxLimit = false;
  isAddedToCart = false;
  itemCode: string;
  lotNumber: string;
  sortBy: string;
  sortOrder: string;
  filter: { key: string; value: any[] }[] = [];
  itemListId: string[] = [];
  allItemsId: string[] = [];
  selectedItemsId: string[] = [];
  binToBinTransferForm: FormGroup;
  destroy$: Subject<null> = new Subject<null>();
  filterData: { [key: string]: Filter[] } = {};
  sortData: Column[] = [];
  selectDestinationBinLableText: string;
  searchDestinationBinLableText: string;

  maxFilterLimit: number;
  maxSortLimit: number;
  maxProductInList: number;

  @ViewChild(SearchListComponent, { static: true })
  searchListRef: SearchListComponent;
  @ViewChild(SearchComponent, { static: false })
  searchRef: SearchComponent;
  @ViewChild(CardListComponent, { static: false })
  cardListComponentRef: CardListComponent;
  @ViewChild(CardListComponent, { static: false, read: ElementRef })
  cardListElementRef: ElementRef;
  @ViewChild('transferByDropdown', { static: true, read: ElementRef })
  transferByDropdown: ElementRef;

  PRODUCT_CATEGORIES: { [key: string]: Filter[] } = {};
  PRODUCT_GROUP: { [key: string]: Filter[] } = {};
  SOURCE_BIN: { [key: string]: Filter[] } = {};

  constructor(
    private router: Router,
    private translate: TranslateService,
    private overlayNotification: OverlayNotificationService,
    private binToBinTransferFacade: BinToBinTransferFacade,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private sortService: SortDialogService,
    private filterService: FilterService,
    private selectionDialog: SelectionDialogService,
    private shortcutService: ShortcutService,
    private appsettingFacade: AppsettingFacade
  ) {
    this.shortcutService.commands
      .pipe(takeUntil(this.destroy$))
      .subscribe(command => {
        this.shortcutEventHandler(command);
      });

    this.sortService.DataSource = SORT_DATA;

    this.binToBinTransferForm = this.formBuilder.group({
      transferBy: [''],
      selectItems: ['']
    });

    this.binToBinTransferFacade.clearItems();
    this.binToBinTransferFacade.clearSearchedItems();
    this.binToBinTransferFacade.clearConfirmTransferResponse();
    this.binToBinTransferFacade.clearItemsGroups();
    this.binToBinTransferFacade.loadProductGroupOptions();
    this.binToBinTransferFacade.loadProductCategoryOptions();
    this.binToBinTransferFacade.loadSourceBinOptions();

    this.binToBinTransferFacade
      .getProductGroupOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(productGroups => {
        this.PRODUCT_GROUP = this.mapToFilterOptions(
          'Product Group',
          productGroups.map(productGroup => ({
            id: productGroup.productGroupCode,
            description: productGroup.description
          }))
        );
      });

    this.binToBinTransferFacade
      .getProductCategoryOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(productCategories => {
        this.PRODUCT_CATEGORIES = this.mapToFilterOptions(
          'Product Category',
          productCategories.map(productCategory => ({
            id: productCategory.productCategoryCode,
            description: productCategory.description
          }))
        );
      });

    this.binToBinTransferFacade
      .getSoruceBinOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(sourceBins => {
        this.SOURCE_BIN = this.mapToFilterOptions('Source Bin', sourceBins);
      });

    this.appsettingFacade
      .getMaxFilterLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxFilterLimit = data;
      });

    this.appsettingFacade
      .getMaxSortLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxSortLimit = data;
      });

    this.appsettingFacade
      .getMaxProductInList()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxProductInList = data;
      });

    this.translate
      .get([
        'pw.binToBinTransfer.selectDestinationBinLableText',
        'pw.binToBinTransfer.searchDestinationBinLableText'
      ])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: any) => {
        this.selectDestinationBinLableText =
          translatedMessages[
            'pw.binToBinTransfer.selectDestinationBinLableText'
          ];
        this.searchDestinationBinLableText =
          translatedMessages[
            'pw.binToBinTransfer.searchDestinationBinLableText'
          ];
      });

    this.binToBinTransferFacade
      .getBins()
      .pipe(takeUntil(this.destroy$))
      .subscribe((bins: StoreBin[]) => {
        if (bins) {
          this.bins = bins;
          this.binsForSelection = bins.map(bin => ({
            id: bin.binCode + BIN_CODE_AND_GROUP_SEPARATOR + bin.binGroupCode,
            description: bin.binCode
          }));
        }
      });

    this.isLoadingBins$ = this.binToBinTransferFacade.getIsLoadingBins();

    this.binToBinTransferFacade.loadBins();

    this.binToBinTransferFacade
      .getSearchedItemList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((items: Item[]) => {
        this.isAddedToCart = false;
        if (items.length > 0) {
          if (items.length === 1) {
            if (this.searchListRef) {
              this.searchListRef.reset();
            }
            this.addToItemList(items);

            this.isAddedToCart = true;
          } else {
            this.searchedItemList = items;
          }
        } else {
          this.searchedItemList = [];
        }
      });

    this.binToBinTransferFacade
      .getItemList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((itemList: Item[]) => {
        if (itemList) {
          this.allItemList = itemList;

          this.allItemsId = this.allItemList.map(item => item.id);
          this.selectedItems = this.allItemList.filter(item => item.isSelected);

          this.selectedItemsId = this.selectedItems.map(item => item.id);

          this.loadItemCart();
          this.checkAndSetSelectAll();
          this.showNotification();
        }
      });

    this.isSearchingItems$ = this.binToBinTransferFacade.getIsSearchingItems();

    this.hasSearchedItems$ = this.binToBinTransferFacade.getHasSearchedItems();

    this.isLoadingItemListGroup$ = this.binToBinTransferFacade.getIsLoadingItemListGroup();

    this.searchedItemListGroups$ = this.binToBinTransferFacade.getSearchedItemListGroups();

    this.searchedItemListGroupsTotalCount$ = this.binToBinTransferFacade.getSearchedItemListGroupsTotalCount();

    this.sourceBins$ = this.binToBinTransferFacade.getSourceBins();
    this.sourceBins$
      .pipe(takeUntil(this.destroy$))
      .subscribe((itemGroup: ItemListGroup[]) => {
        if (
          itemGroup &&
          itemGroup.length !== 0 &&
          !this.isSourceBinsLoadedOnce
        ) {
          this.isSourceBinsLoadedOnce = true;
        }
      });
    this.sourceBinsTotalCount$ = this.binToBinTransferFacade.getSourceBinsTotalCount();

    this.productGroups$ = this.binToBinTransferFacade.getProductGroups();
    this.productGroups$
      .pipe(takeUntil(this.destroy$))
      .subscribe((itemGroup: ItemListGroup[]) => {
        if (
          itemGroup &&
          itemGroup.length !== 0 &&
          !this.isProductGroupsLoadedOnce
        ) {
          this.isProductGroupsLoadedOnce = true;
        }
      });
    this.productGroupsTotalCount$ = this.binToBinTransferFacade.getProductGroupsTotalCount();

    this.productCategory$ = this.binToBinTransferFacade.getProductCategory();
    this.productCategory$
      .pipe(takeUntil(this.destroy$))
      .subscribe((itemGroup: ItemListGroup[]) => {
        if (
          itemGroup &&
          itemGroup.length !== 0 &&
          !this.isProducCategoryLoadedOnce
        ) {
          this.isProducCategoryLoadedOnce = true;
        }
      });
    this.productCategoryTotalCount$ = this.binToBinTransferFacade.getProductCategoryTotalCount();

    this.binToBinTransferForm
      .get('transferBy')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(mode => {
        if (this.cardListComponentRef) {
          this.cardListComponentRef.resetFocus();
        }
        this.router.navigate(['..', mode], { relativeTo: this.activatedRoute });
        if (this.searchListRef) {
          this.searchListRef.reset();
        }
      });

    this.binToBinTransferForm
      .get('selectItems')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(type => {
        if (type === 'selectAll' && this.sendSelectAllEvent) {
          this.selectedBin = null;
          this.binToBinTransferFacade.changeSelectionOfAllItems({
            idList: this.itemListId,
            select: true,
            disable: false
          });
        }
        this.sendSelectAllEvent = true;
      });

    this.binToBinTransferFacade
      .getConfirmTransferResponse()
      .pipe(takeUntil(this.destroy$))
      .subscribe((confirmTransferResponse: ConfirmTransferResponse) => {
        this.confirmTransferResponse = confirmTransferResponse;
        this.showNotification();
        if (this.confirmTransferResponse) {
          this.binToBinTransferFacade.clearItemsGroups();
          this.isSourceBinsLoadedOnce = false;
          this.isProducCategoryLoadedOnce = false;
          this.isProductGroupsLoadedOnce = false;
        }
      });

    this.binToBinTransferFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    // TODO : change logic. Route state
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        const transferBy = this.activatedRoute.snapshot.params['type'];
        if (
          transferBy !== BinToBinTransferTypeEnum.VARIANT_CODE &&
          transferBy !== BinToBinTransferTypeEnum.BIN_CODE &&
          transferBy !== BinToBinTransferTypeEnum.PRODUCT_GROUP &&
          transferBy !== BinToBinTransferTypeEnum.PRODUCT_CATEGORY
        ) {
          this.router.navigate(['404']);
          return;
        }
        if (this.binToBinTransferForm.get('transferBy').value !== transferBy) {
          this.binToBinTransferForm.patchValue({ transferBy: transferBy });
        }
        this.binToBinTransferFacade.clearSearchedItems();
        this.intialize();
      });
  }

  mapToFilterOptions(
    key: string,
    options: FilterOption[]
  ): { [key: string]: Filter[] } {
    return {
      [key]: options.map(option => ({
        id: option.id,
        description: option.description,
        selected: false
      }))
    };
  }

  addToItemList(items: Item[]) {
    let clearSelection = false;

    if (this.itemCode != null || this.filter.length > 0) {
      clearSelection = true;
    }

    this.binToBinTransferForm.get('selectItems').reset();

    this.clearAll();

    this.binToBinTransferFacade.addToItemList(items);

    if (clearSelection) {
      this.resetSelection();
    }
  }
  /**
   * Method to handle shortcut commands
   * @param command: shortcut command
   */
  shortcutEventHandler(command: Command): void {
    switch (command.name) {
      case SEARCH_LIST_SHORTCUT_KEY: {
        if (this.searchListRef) {
          this.searchListRef.focus();
        }
        break;
      }
      case CARD_LIST_SHORTCUT_KEY: {
        if (
          this.cardListElementRef &&
          this.cardListElementRef.nativeElement &&
          this.cardListElementRef.nativeElement.focus
        ) {
          this.cardListElementRef.nativeElement.focus();
        }
        break;
      }
      case DROP_DOWN_SHORTCUT_KEY: {
        if (
          this.transferByDropdown &&
          this.transferByDropdown.nativeElement &&
          this.transferByDropdown.nativeElement.focus
        ) {
          this.transferByDropdown.nativeElement.focus();
        }
        break;
      }
    }
  }

  intialize() {
    const mode = this.binToBinTransferForm.get('transferBy').value;
    if (mode === BinToBinTransferTypeEnum.VARIANT_CODE) {
      this.resetSelection();
      if (this.searchListRef) {
        this.searchListRef.focus();
      }
    }

    if (mode === BinToBinTransferTypeEnum.BIN_CODE) {
      this.itemGroups$ = this.sourceBins$;
      this.itemGroupsTotalCount$ = this.sourceBinsTotalCount$;
    }

    if (mode === BinToBinTransferTypeEnum.PRODUCT_GROUP) {
      this.itemGroups$ = this.productGroups$;
      this.itemGroupsTotalCount$ = this.productGroupsTotalCount$;
    }

    if (mode === BinToBinTransferTypeEnum.PRODUCT_CATEGORY) {
      this.itemGroups$ = this.productCategory$;
      this.itemGroupsTotalCount$ = this.productCategoryTotalCount$;
    }

    if (
      (mode === BinToBinTransferTypeEnum.BIN_CODE &&
        !this.isSourceBinsLoadedOnce) ||
      (mode === BinToBinTransferTypeEnum.PRODUCT_CATEGORY &&
        !this.isProducCategoryLoadedOnce) ||
      (mode === BinToBinTransferTypeEnum.PRODUCT_GROUP &&
        !this.isProductGroupsLoadedOnce)
    ) {
      this.loadList(0);
    }
  }

  ngOnInit(): void {}

  openBinSelectionPopup() {
    this.selectionDialog
      .open({
        title: this.selectDestinationBinLableText,
        placeholder: this.searchDestinationBinLableText,
        options: this.binsForSelection
      })
      .pipe(takeUntil(this.destroy$))
      .subscribe((selectedOption: Option) => {
        if (selectedOption) {
          this.selectedBin = this.binSeparator(selectedOption.id);
          this.binToBinTransferFacade.updateDestinationBinForSelectedItems({
            idList: this.selectedItemsId,
            destinationBinCode: this.selectedBin.binCode,
            destinationBinGroupCode: this.selectedBin.binGroupCode
          });
        }
      });
  }

  checkAndSetSelectAll() {
    if (
      this.selectedItems.length !== 0 &&
      this.selectedItems.length === this.itemList.length
    ) {
      this.sendSelectAllEvent = false;
      this.binToBinTransferForm.patchValue({
        selectItems: 'selectAll'
      });
    } else {
      this.binToBinTransferForm.get('selectItems').reset();
    }
  }

  isSearchByVariantCode(): boolean {
    return (
      this.binToBinTransferForm.get('transferBy').value ===
      BinToBinTransferTypeEnum.VARIANT_CODE
    );
  }

  search(searchData: SearchResponse) {
    if (
      this.binToBinTransferForm.get('transferBy').value ===
      BinToBinTransferTypeEnum.VARIANT_CODE
    ) {
      if (this.allItemList.length < this.maxProductInList) {
        this.binToBinTransferFacade.searchItems({
          itemCode: searchData.searchValue,
          lotNumber: searchData.lotNumber
        });
      } else {
        if (this.searchListRef) {
          this.searchListRef.reset();
        }
        this.hasReachedMaxLimit = true;
        this.showNotification();
      }
    } else {
      if (this.cardListComponentRef) {
        this.cardListComponentRef.resetFocus();
      }
      this.binToBinTransferFacade.searchItemGroups({
        type: this.binToBinTransferForm.get('transferBy').value,
        pageIndex: 0,
        pageSize: this.pageSize,
        value: searchData.searchValue
      });
      this.itemGroups$ = this.searchedItemListGroups$;
      this.itemGroupsTotalCount$ = this.searchedItemListGroupsTotalCount$;
    }
  }

  loadItemCart() {
    this.itemList = [...this.allItemList];
    if (this.itemCode) {
      this.itemList = this.itemList.filter(
        (item: Item) =>
          item.itemCode.toLowerCase() === this.itemCode.toLowerCase() &&
          (this.lotNumber
            ? this.lotNumber.toLowerCase() === item.lotNumber.toLowerCase()
            : true)
      );
    }
    if (this.filter.length > 0) {
      for (let i = 0; i < this.filter.length; i++) {
        this.itemList = this.itemList.filter((item: Item) => {
          return this.filter[i].value.includes(item[this.filter[i].key]);
        });
      }
    }
    if (this.sortBy) {
      const binItemsSort  = [...this.itemList].sort(
        (item1: Item, item2: Item) => {
        if (item1[this.sortBy] === item2[this.sortBy]) {
          return 0;
        }
        return item1[this.sortBy] < item2[this.sortBy]
          ? this.sortOrder === 'ASC'
            ? -1
            : 1
          : this.sortOrder === 'ASC'
          ? 1
          : -1;
      });
      this.itemList = binItemsSort;
    }

    this.itemListId = this.itemList.map(item => item.id);

    if (this.itemListId.length === 1 && this.selectedItemsId.length === 0) {
      this.binToBinTransferFacade.changeSelectionOfAllItems({
        idList: this.itemListId,
        select: true,
        disable: false
      });
    }
  }

  searchInCart(searchData: SearchResponse) {
    this.itemCode = searchData.searchValue;
    this.lotNumber = searchData.lotNumber;
    this.resetSelection();
    this.loadItemCart();
  }

  clearSearchofCart(): void {
    this.itemCode = null;
    this.lotNumber = null;
    this.resetSelection();
    this.loadItemCart();
  }

  openSortDailog() {
    this.sortService
      .openDialog(this.maxSortLimit, this.sortData)
      .pipe(takeUntil(this.destroy$))
      .subscribe((sortResult: { data: Column[]; actionfrom: string }) => {
        if (sortResult.actionfrom === 'apply') {
          const sortData = sortResult.data;
          if (sortData == null || sortData.length === 0) {
            this.sortData = [];
            this.sortOrder = null;
            this.sortBy = null;
          } else {
            this.sortData = sortData;
            if (sortData.length > 0) {
              if (sortData[0].id === 0) {
                this.sortBy = 'availableWeight';
              } else if (sortData[0].id === 1) {
                this.sortBy = 'availableQuantity';
              }
              this.sortOrder = sortData[0].sortAscOrder ? 'ASC' : 'DESC';
            }
          }
          this.loadItemCart();
        }
      });
  }

  openFilter() {
    this.filterService.DataSource = {
      ...this.PRODUCT_CATEGORIES,
      ...this.SOURCE_BIN,
      ...this.PRODUCT_GROUP
    };
    this.filterService
      .openDialog(this.maxFilterLimit, this.filterData)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (filterResult: {
          data: { [key: string]: Filter[] };
          actionfrom: string;
        }) => {
          // TODO : Enum
          if (filterResult.actionfrom === 'apply') {
            const filterData = filterResult.data;
            if (filterData == null) {
              this.filterData = {};
            } else {
              this.filterData = filterData;
            }
            this.filter = [];
            if (filterData) {
              let filterValues = [];
              if (filterData['Source Bin']) {
                filterData['Source Bin'].forEach(value => {
                  filterValues.push(value.id);
                });
                if (filterValues.length > 0) {
                  this.filter.push({
                    key: 'binCode',
                    value: filterValues
                  });
                }
              }
              filterValues = [];
              if (filterData['Product Group']) {
                filterData['Product Group'].forEach(value => {
                  filterValues.push(value.id);
                });
                if (filterValues.length > 0) {
                  this.filter.push({
                    key: 'productGroup',
                    value: filterValues
                  });
                }
              }
              filterValues = [];
              if (filterData['Product Category']) {
                filterData['Product Category'].forEach(value => {
                  filterValues.push(value.id);
                });
                if (filterValues.length > 0) {
                  this.filter.push({
                    key: 'productCategory',
                    value: filterValues
                  });
                }
              }
            }

            this.resetSelection();
            this.loadItemCart();
          }
        }
      );
  }

  onSearchClear() {
    if (!this.isSearchByVariantCode()) {
      if (this.cardListComponentRef) {
        this.cardListComponentRef.resetFocus();
      }
      this.intialize();
    } else {
      this.binToBinTransferFacade.clearSearchedItems();
    }
  }

  clearAll() {
    this.itemCode = null;
    this.lotNumber = null;
    this.sortBy = null;
    this.sortOrder = null;
    this.sortData = [];
    this.filter = [];
    this.filterData = {};
    this.selectedBin = null;
    if (this.searchRef) {
      this.searchRef.reset();
    }
    this.loadItemCart();
  }

  loadList(pageIndex: number): void {
    if (
      this.binToBinTransferForm.get('transferBy').value ===
      BinToBinTransferTypeEnum.BIN_CODE
    ) {
      this.binToBinTransferFacade.loadSourceBins({
        type: BinToBinTransferTypeEnum.BIN_CODE,
        pageIndex: pageIndex,
        pageSize: this.isSourceBinsLoadedOnce
          ? this.pageSize
          : this.initalPageSize
      });
    }

    if (
      this.binToBinTransferForm.get('transferBy').value ===
      BinToBinTransferTypeEnum.PRODUCT_CATEGORY
    ) {
      this.binToBinTransferFacade.loadProductsCategory({
        type: BinToBinTransferTypeEnum.PRODUCT_CATEGORY,

        pageIndex: pageIndex,
        pageSize: this.isProducCategoryLoadedOnce
          ? this.pageSize
          : this.initalPageSize
      });
    }

    if (
      this.binToBinTransferForm.get('transferBy').value ===
      BinToBinTransferTypeEnum.PRODUCT_GROUP
    ) {
      this.binToBinTransferFacade.loadProductsGroups({
        type: BinToBinTransferTypeEnum.PRODUCT_GROUP,

        pageIndex: pageIndex,
        pageSize: this.isProductGroupsLoadedOnce
          ? this.pageSize
          : this.initalPageSize
      });
    }
  }

  onSearchItemsSelected(items: Item[]) {
    if (this.allItemList.length + items.length <= this.maxProductInList) {
      this.addToItemList(items);
    } else {
      if (this.searchListRef) {
        this.searchListRef.reset();
      }
      this.hasReachedMaxLimit = true;
      this.showNotification();
    }
  }

  onSelected(data: ItemListGroup): void {
    this.router.navigate([
      '/',
      'inventory',
      'instock',
      'bintobinTransfer',
      this.binToBinTransferForm.get('transferBy').value,
      data.name
    ]);
  }

  onDelete(itemId: string) {
    this.selectedBin = null;
    this.binToBinTransferFacade.deleteFromItemList(itemId);
  }

  onUpdateItem(item: Item) {
    this.selectedBin = null;
    this.binToBinTransferFacade.updateItemList(item);
  }

  deleteSelectedItems() {
    this.selectedBin = null;
    this.binToBinTransferFacade.deleteSelectedItems(this.selectedItemsId);
  }

  allAssignedWithBins(): boolean {
    return (
      this.selectedItems.filter(
        item => item.isSelected && item.destinationBinCode == null
      ).length === 0
    );
  }

  resetSelection() {
    this.selectedBin = null;
    this.binToBinTransferForm.get('selectItems').reset();
    this.binToBinTransferFacade.changeSelectionOfAllItems({
      idList: this.itemListId,
      select: false,
      disable: false
    });
  }

  binSeparator(
    binCodeAndGroup: string
  ): { binCode: string; binGroupCode: string } {
    const data = binCodeAndGroup.split(BIN_CODE_AND_GROUP_SEPARATOR);
    return {
      binCode: data[0],
      binGroupCode: data[1]
    };
  }

  showNotification() {
    if (this.confirmTransferResponse) {
      this.showConfirmReceiveSuccessNotification(
        this.confirmTransferResponse.transferId
      );
    } else if (this.hasReachedMaxLimit) {
      this.showMaximumProductsPerRequestLimitNotification();
    } else {
      if (this.selectedItems.length > 0) {
        if (this.allAssignedWithBins()) {
          this.showConfirmTransferNotification(this.selectedItems.length);
        } else {
          this.showAssignDestinationBinNotification(this.selectedItems.length);
        }
      } else {
        this.overlayNotification.close();
        this.hasNotification = false;
      }
    }
  }

  showConfirmTransferNotification(count: number = 0) {
    const key =
      'pw.binToBinTransferNotificationMessages.confirmTransferMessage';
    const buttonKey1 =
      'pw.binToBinTransferNotificationMessages.confirmTransferButtonText';
    const buttonKey2 =
      'pw.binToBinTransferNotificationMessages.removeButtonText';
    this.translate
      .get([key, buttonKey1, buttonKey2])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: any) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.CONFIRM_TRANSFER,
            message: count + ' ' + translatedMessages[key]
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.showProgressNotification();
              const confirmTransferRequest: ConfirmTransferItemsRequest = {
                request: {
                  binItems: this.selectedItems.map(item => ({
                    inventoryId: item.id,
                    binGroupCode: item.destinationBinGroupCode,
                    binCode: item.destinationBinCode
                  }))
                },
                remove: true
              };
              this.binToBinTransferFacade.confirmTransferItems(
                confirmTransferRequest
              );
            } else if (event.eventType === OverlayNotificationEventType.FALSE) {
              this.deleteSelectedItems();
            }
          });
      });
  }

  showConfirmReceiveSuccessNotification(transferNumber: number) {
    const key =
      'pw.binToBinTransferNotificationMessages.confirmTransferSuccessMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.SIMPLE,
            message: translatedMsg + ' ' + transferNumber,
            hasBackdrop: true,
            hasClose: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe(() => {
            this.hasNotification = false;
            this.binToBinTransferFacade.clearConfirmTransferResponse();
          });
      });
  }

  showMaximumProductsPerRequestLimitNotification() {
    const key =
      'pw.binToBinTransferNotificationMessages.maximumProductsLimitMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: translatedMessage.replace(
              '{0}',
              '' + this.maxProductInList
            )
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe(() => {
            this.hasNotification = false;
            this.hasReachedMaxLimit = false;
          });
      });
  }
  showAssignDestinationBinNotification(count: number = 0) {
    const key =
      'pw.binToBinTransferNotificationMessages.assignDestinationBinMessage';
    const buttonKey =
      'pw.binToBinTransferNotificationMessages.removeButtonText';
    this.translate
      .get([key, buttonKey])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            message: count + ' ' + translatedMessages[key],
            buttonText: translatedMessages[buttonKey]
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.deleteSelectedItems();
            }
          });
      });
  }

  showProgressNotification() {
    const key = 'pw.binToBinTransferNotificationMessages.progressMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.PROGRESS,
            message: translatedMsg,
            hasBackdrop: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe(() => {
            this.hasNotification = false;
          });
      });
  }

  errorHandler(error: CustomErrors) {
    this.hasNotification = true;
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        this.hasNotification = false;
        this.showNotification();
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
