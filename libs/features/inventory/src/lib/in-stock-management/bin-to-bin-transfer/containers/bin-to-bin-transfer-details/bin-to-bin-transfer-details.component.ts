import {
  SELECT_BY_KEY_MAP,
  SORT_DATA,
  BIN_CODE_AND_GROUP_SEPARATOR
} from '../../models/bin-data';
import { Subject, Observable } from 'rxjs';
import {
  ConfirmTransferResponse,
  ConfirmTransferItemsRequest,
  ItemListGroup,
  Item,
  FilterOption
} from '../../models/bin-to-bin-transfer.model';
import { MatDialog, PageEvent } from '@angular/material';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import {
  OverlayNotificationType,
  OverlayNotificationService,
  OverlayNotificationEventRef,
  OverlayNotificationEventType,
  SearchResponse,
  FilterService,
  SortDialogService,
  Filter,
  SearchComponent,
  Column,
  SelectionDialogService,
  Option
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { BinToBinTransferFacade } from '../../+state/bin-to-bin-transfer.facade';
import { ActivatedRoute, Router } from '@angular/router';
import { BinToBinTransferTypeEnum } from '../../models/bin-to-bin-transfer.enum';
import {
  AppsettingFacade,
  CustomErrors,
  ShortcutService,
  Command,
  StoreBin
} from '@poss-web/core';

const SEARCH_SHORTCUT_KEY = 'BinToBinTransferComponent.SEARCH';

@Component({
  selector: 'poss-web-bin-to-bin-transfer-details',
  templateUrl: './bin-to-bin-transfer-details.component.html',
  styleUrls: ['./bin-to-bin-transfer-details.component.scss']
})
export class BinToBinTransferDetailsComponent implements OnInit, OnDestroy {
  selectByKeyMap = SELECT_BY_KEY_MAP;
  binToBinTransferForm: FormGroup;
  destroy$ = new Subject();
  binsForSelection: Option[] = [];
  selectedBin: { binCode: string; binGroupCode: string };
  isLoadingBins$: Observable<boolean>;
  isLoadingItems$: Observable<boolean>;
  isLoadingItemsSuccess$: Observable<boolean>;
  itemsTotalCount: number;
  type: string;
  value: string;
  itemCode: string;
  lotNumber: string;
  sortBy: string;
  sortOrder: string;
  filter: { key: string; value: any[] }[] = [];
  initailPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };
  itemListPageEvent: PageEvent = this.initailPageEvent;
  confirmTransferResponse: ConfirmTransferResponse;
  hasNotification = false;
  isSelectAll = false;
  sendSelectCurrentEvent = true;
  itemGroup: ItemListGroup;
  showSourceBin = true;
  itemList: Item[] = [];
  selectedItems: Item[] = [];
  selectedQuantity = 0;
  previousSelectedQuantity = 0;
  previousItemGroupQuantity = 0;
  selectedValue = 0;
  allItemsId: string[] = [];
  filterData: { [key: string]: Filter[] } = {};
  sortData: Column[] = [];
  selectDestinationBinLableText: string;
  searchDestinationBinLableText: string;
  isItemsLoadedOnce = false;

  maxFilterLimit: number;
  maxSortLimit: number;

  @ViewChild(SearchComponent, { static: true })
  searchRef: SearchComponent;

  PRODUCT_CATEGORIES: { [key: string]: Filter[] } = {};
  PRODUCT_GROUP: { [key: string]: Filter[] } = {};
  SOURCE_BIN: { [key: string]: Filter[] } = {};

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private translate: TranslateService,
    private formBuilder: FormBuilder,
    private overlayNotification: OverlayNotificationService,
    private binToBinTransferFacade: BinToBinTransferFacade,
    private activatedRoute: ActivatedRoute,
    private filterService: FilterService,
    private sortService: SortDialogService,
    private appsettingFacade: AppsettingFacade,
    private selectionDialog: SelectionDialogService,
    private shortcutService: ShortcutService
  ) {
    this.shortcutService.commands
      .pipe(takeUntil(this.destroy$))
      .subscribe(command => {
        this.shortcutEventHandler(command);
      });

    this.type = this.activatedRoute.snapshot.params['type'];
    this.value = this.activatedRoute.snapshot.params['value'];

    if (this.type === BinToBinTransferTypeEnum.BIN_CODE) {
      this.showSourceBin = false;
    }
    if (
      this.type !== BinToBinTransferTypeEnum.VARIANT_CODE &&
      this.type !== BinToBinTransferTypeEnum.BIN_CODE &&
      this.type !== BinToBinTransferTypeEnum.PRODUCT_GROUP &&
      this.type !== BinToBinTransferTypeEnum.PRODUCT_CATEGORY
    ) {
      this.router.navigate(['404']);
      return;
    }

    this.translate
      .get([
        'pw.binToBinTransfer.selectDestinationBinLableText',
        'pw.binToBinTransfer.searchDestinationBinLableText'
      ])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: any) => {
        this.selectDestinationBinLableText =
          translatedMessages[
            'pw.binToBinTransfer.selectDestinationBinLableText'
          ];
        this.searchDestinationBinLableText =
          translatedMessages[
            'pw.binToBinTransfer.searchDestinationBinLableText'
          ];
      });

    this.binToBinTransferForm = this.formBuilder.group({
      selectItems: ['']
    });

    this.binToBinTransferForm
      .get('selectItems')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(type => {
        if (type === 'selectAll') {
          this.isSelectAll = true;
          this.binToBinTransferFacade.changeSelectionOfAllItems({
            idList: this.allItemsId,
            select: true,
            disable: true
          });
        } else if (
          type === 'selectCurrentPage' &&
          this.sendSelectCurrentEvent
        ) {
          this.isSelectAll = false;
          this.binToBinTransferFacade.changeSelectionOfAllItems({
            idList: this.allItemsId,
            select: true,
            disable: false
          });
        }
        this.sendSelectCurrentEvent = true;
      });

    this.sortService.DataSource = SORT_DATA;
  }

  ngOnInit() {
    this.binToBinTransferFacade.loadProductGroupOptions();
    this.binToBinTransferFacade.loadProductCategoryOptions();
    this.binToBinTransferFacade.loadSourceBinOptions();

    this.binToBinTransferFacade
      .getProductGroupOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(productGroups => {
        this.PRODUCT_GROUP = this.mapToFilterOptions(
          'Product Group',
          productGroups.map(productGroup => ({
            id: productGroup.productGroupCode,
            description: productGroup.description
          }))
        );
      });

    this.binToBinTransferFacade
      .getProductCategoryOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(productCategories => {
        this.PRODUCT_CATEGORIES = this.mapToFilterOptions(
          'Product Category',
          productCategories.map(productCategory => ({
            id: productCategory.productCategoryCode,
            description: productCategory.description
          }))
        );
      });

    this.binToBinTransferFacade
      .getSoruceBinOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(sourceBins => {
        this.SOURCE_BIN = this.mapToFilterOptions('Source Bin', sourceBins);
      });

    this.appsettingFacade
      .getMaxFilterLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxFilterLimit = data;
      });

    this.appsettingFacade
      .getMaxSortLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxSortLimit = data;
      });

    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        const pageSize = JSON.parse(data);
        this.initailPageEvent.pageSize = pageSize;
        this.componentInit();
      });
  }

  shortcutEventHandler(command: Command): void {
    if(command.name === SEARCH_SHORTCUT_KEY) {
      if (this.searchRef) {
        this.searchRef.focus();
      }
    }
  }

  mapToFilterOptions(
    key: string,
    options: FilterOption[]
  ): { [key: string]: Filter[] } {
    return {
      [key]: options.map((option, index) => ({
        id: option.id,
        description: option.description,
        selected: false
      }))
    };
  }

  componentInit() {
    this.binToBinTransferFacade
      .getBins()
      .pipe(takeUntil(this.destroy$))
      .subscribe((bins: StoreBin[]) => {
        if (bins) {
          if (this.type === BinToBinTransferTypeEnum.BIN_CODE) {
            bins = bins.filter(
              bin => bin.binCode.toLowerCase() !== this.value.toLowerCase()
            );
          }
          this.binsForSelection = bins.map(bin => ({
            id: bin.binCode + BIN_CODE_AND_GROUP_SEPARATOR + bin.binGroupCode,
            description: bin.binCode
          }));
        }
      });

    this.isLoadingBins$ = this.binToBinTransferFacade.getIsLoadingBins();
    this.binToBinTransferFacade.loadBins();
    this.binToBinTransferFacade.clearItems();
    this.binToBinTransferFacade.clearConfirmTransferResponse();
    this.binToBinTransferFacade.clearSelectedItemGroup();

    this.binToBinTransferFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.binToBinTransferFacade.loadItemGroup({
      type: this.type,
      value: this.value
    });

    this.binToBinTransferFacade
      .getSelectedItemListGroup()
      .pipe(takeUntil(this.destroy$))
      .subscribe((itemGroup: ItemListGroup) => {
        if (itemGroup) {
          this.itemGroup = itemGroup;
        }
      });

    this.binToBinTransferFacade
      .getIsLoadingSelectedItemListGroupSuccess()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isLoaded: boolean) => {
        if (isLoaded === false && !this.isSelectAll) {
          this.back();
        }
        if (isLoaded) {
          this.resetSelection();
          this.loadItems();
        }
      });

    this.binToBinTransferFacade
      .getItemList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((itemList: Item[]) => {
        if (itemList) {
          if (!this.isItemsLoadedOnce && itemList.length > 0) {
            this.isItemsLoadedOnce = true;
          }
          this.itemList = itemList;
          this.allItemsId = this.itemList.map(item => item.id);
          this.selectedItems = this.itemList.filter(item => item.isSelected);

          if (this.allItemsId.length === 1 && this.selectedItems.length === 0) {
            this.binToBinTransferFacade.changeSelectionOfAllItems({
              idList: this.allItemsId,
              select: true,
              disable: false
            });
          }

          this.checkAndSetSelectAll();
          this.calculateSelectedItemsData();
          this.showNotification();
        }
      });

    this.isLoadingItems$ = this.binToBinTransferFacade.getIsLoadingItems();

    this.binToBinTransferFacade
      .getItemsTotalCount()
      .pipe(takeUntil(this.destroy$))
      .subscribe((itemsTotalCount: number) => {
        this.itemsTotalCount = itemsTotalCount;
      });

    this.binToBinTransferFacade
      .getConfirmTransferResponse()
      .pipe(takeUntil(this.destroy$))
      .subscribe((confirmTransferResponse: ConfirmTransferResponse) => {
        this.confirmTransferResponse = confirmTransferResponse;

        if (this.confirmTransferResponse) {
          this.previousSelectedQuantity = this.selectedQuantity;
          this.previousItemGroupQuantity = this.itemGroup.products;
          if (
            this.type !== BinToBinTransferTypeEnum.BIN_CODE ||
            this.previousItemGroupQuantity > this.previousSelectedQuantity
          ) {
            this.binToBinTransferFacade.loadItemGroup({
              type: this.type,
              value: this.value
            });
          }
        }
        this.showNotification();
      });

    this.isLoadingItemsSuccess$ = this.binToBinTransferFacade.getIsLoadingItemsSuccess();

    this.isLoadingItemsSuccess$
      .pipe(takeUntil(this.destroy$))
      .subscribe((isSuccess: boolean) => {
        if (isSuccess) {
          if (this.isSelectAll) {
            this.binToBinTransferFacade.changeSelectionOfAllItems({
              idList: this.allItemsId,
              select: true,
              disable: true
            });
          } else {
            this.resetSelection();
          }
        }
      });
  }

  openBinSelectionPopup() {
    this.selectionDialog
      .open({
        title: this.selectDestinationBinLableText,
        placeholder: this.searchDestinationBinLableText,
        options: this.binsForSelection
      })
      .pipe(takeUntil(this.destroy$))
      .subscribe((selectedOption: Option) => {
        if (selectedOption) {
          this.selectedBin = this.binSeparator(selectedOption.id);
          this.showNotification();
        }
      });
  }

  checkAndSetSelectAll() {
    if (
      !this.isSelectAll &&
      this.selectedItems.length !== 0 &&
      this.selectedItems.length === this.itemList.length
    ) {
      this.sendSelectCurrentEvent = false;
      this.binToBinTransferForm.patchValue({
        selectItems: 'selectCurrentPage'
      });
    } else if (!this.isSelectAll) {
      this.binToBinTransferForm.get('selectItems').reset();
    }
  }

  calculateSelectedItemsData() {
    if (this.itemGroup) {
      if (this.isSelectAll) {
        this.selectedQuantity = this.itemGroup.products;
        this.selectedValue = this.itemGroup.totalValue;
      } else {
        const caluculatedData = this.selectedItems
          .map(item => ({
            availableQuantity: item.availableQuantity,
            availableValue: item.availableValue
          }))
          .reduce(
            (item1, item2) => ({
              availableQuantity:
                item1.availableQuantity + item2.availableQuantity,
              availableValue: item1.availableValue + item2.availableValue
            }),
            {
              availableQuantity: 0,
              availableValue: 0
            }
          );

        this.selectedQuantity = caluculatedData.availableQuantity;
        this.selectedValue = caluculatedData.availableValue;
      }
    }
  }

  search(searchData: SearchResponse) {
    this.itemCode = searchData.searchValue;
    this.lotNumber = searchData.lotNumber;
    this.isSelectAll = false;
    this.itemListPageEvent = this.initailPageEvent;
    this.loadItems();
  }

  onSearchClear(): void {
    this.itemCode = null;
    this.lotNumber = null;
    this.isSelectAll = false;
    this.itemListPageEvent = this.initailPageEvent;
    this.loadItems();
  }

  openFilter() {
    this.filterService.DataSource = {
      ...(this.type !== BinToBinTransferTypeEnum.PRODUCT_CATEGORY
        ? this.PRODUCT_CATEGORIES
        : null),
      ...(this.type !== BinToBinTransferTypeEnum.BIN_CODE
        ? this.SOURCE_BIN
        : null),
      ...(this.type !== BinToBinTransferTypeEnum.PRODUCT_GROUP
        ? this.PRODUCT_GROUP
        : null)
    };

    this.filterService
      .openDialog(this.maxFilterLimit, this.filterData)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (filterResult: {
          data: { [key: string]: Filter[] };
          actionfrom: string;
        }) => {
          // TODO : Enum
          if (filterResult.actionfrom === 'apply') {
            const filterData = filterResult.data;
            if (filterData == null) {
              this.filterData = {};
            } else {
              this.filterData = filterData;
            }
            this.filter = [];
            if (filterData) {
              let filterValues = [];
              if (filterData['Source Bin']) {
                filterData['Source Bin'].forEach(value => {
                  filterValues.push(value.id);
                });
                if (filterValues.length > 0) {
                  this.filter.push({
                    key: 'binCode',
                    value: filterValues
                  });
                }
              }
              filterValues = [];
              if (filterData['Product Group']) {
                filterData['Product Group'].forEach(value => {
                  filterValues.push(value.id);
                });
                if (filterValues.length > 0) {
                  this.filter.push({
                    key: 'productGroup',
                    value: filterValues
                  });
                }
              }
              filterValues = [];
              if (filterData['Product Category']) {
                filterData['Product Category'].forEach(value => {
                  filterValues.push(value.id);
                });
                if (filterValues.length > 0) {
                  this.filter.push({
                    key: 'productCategory',
                    value: filterValues
                  });
                }
              }
            }

            this.isSelectAll = false;
            this.itemListPageEvent = this.initailPageEvent;
            this.loadItems();
          }
        }
      );
  }

  openSortDailog() {
    this.sortService
      .openDialog(this.maxSortLimit, this.sortData)
      .pipe(takeUntil(this.destroy$))
      .subscribe((sortResult: { data: Column[]; actionfrom: string }) => {
        if (sortResult.actionfrom === 'apply') {
          const sortData = sortResult.data;
          if (sortData == null || sortData.length === 0) {
            this.sortData = [];
            this.sortOrder = null;
            this.sortBy = null;
          } else {
            this.sortData = sortData;
            if (sortData.length > 0) {
              if (sortData[0].id === 0) {
                this.sortBy = 'totalWeight';
              } else if (sortData[0].id === 1) {
                this.sortBy = 'totalQuantity';
              }
              this.sortOrder = sortData[0].sortAscOrder ? 'ASC' : 'DESC';
            }
          }
          this.isSelectAll = false;
          this.itemListPageEvent = this.initailPageEvent;
          this.loadItems();
        }
      });
  }

  paginate(pageEvent: PageEvent) {
    this.itemListPageEvent = pageEvent;
    this.loadItems();
  }

  loadItems() {
    this.binToBinTransferFacade.loadItems({
      type: this.type,
      value: this.value,
      itemCode: this.itemCode,
      lotNumber: this.lotNumber,
      pageIndex: this.itemListPageEvent.pageIndex,
      pageSize: this.itemListPageEvent.pageSize,
      sortBy: this.sortBy,
      sortOrder: this.sortOrder,
      filter: this.filter
    });
  }

  clearAll() {
    this.itemCode = null;
    this.lotNumber = null;
    this.sortBy = null;
    this.sortOrder = null;
    this.sortData = [];
    this.filter = [];
    this.filterData = {};
    if (this.searchRef) {
      this.searchRef.reset();
    }
  }

  onUpdateItem(item: Item) {
    this.binToBinTransferFacade.updateItemList(item);
  }

  resetSelection() {
    this.isSelectAll = false;
    this.binToBinTransferForm.get('selectItems').reset();
    this.binToBinTransferFacade.changeSelectionOfAllItems({
      idList: this.allItemsId,
      select: false,
      disable: false
    });
  }

  back() {
    this.router.navigate([
      '/',
      'inventory',
      'instock',
      'bintobinTransfer',
      this.type
    ]);
  }

  binSeparator(
    binCodeAndGroup: string
  ): { binCode: string; binGroupCode: string } {
    const data = binCodeAndGroup.split(BIN_CODE_AND_GROUP_SEPARATOR);
    return {
      binCode: data[0],
      binGroupCode: data[1]
    };
  }

  showNotification() {
    if (this.confirmTransferResponse) {
      this.showConfirmReceiveSuccessNotification(
        this.confirmTransferResponse.transferId
      );
    } else {
      if (this.selectedItems.length > 0 || this.isSelectAll) {
        if (this.selectedBin && this.selectedBin.binCode) {
          this.showConfirmTransferNotification(
            this.isSelectAll ? this.itemsTotalCount : this.selectedItems.length
          );
        } else {
          this.showAsaignDestinationBinNotification(
            this.isSelectAll ? this.itemsTotalCount : this.selectedItems.length
          );
        }
      } else {
        this.overlayNotification.close();
        this.hasNotification = false;
      }
    }
  }

  showConfirmTransferNotification(count: number = 0) {
    const key =
      'pw.binToBinTransferNotificationMessages.confirmTransferMessage';
    const buttonKey =
      'pw.binToBinTransferNotificationMessages.confirmTransferButtonText';
    this.translate
      .get([key, buttonKey])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            message: count + ' ' + translatedMessages[key],
            buttonText: translatedMessages[buttonKey]
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.showProgressNotification();
              if (this.isSelectAll) {
                this.binToBinTransferFacade.confirmTransferAllItems({
                  type: this.type,
                  value: this.value,
                  destinationBinCode: this.selectedBin.binCode,
                  destinationBinGroupCode: this.selectedBin.binGroupCode
                });
              } else {
                const confirmTransferRequest: ConfirmTransferItemsRequest = {
                  request: {
                    binItems: this.selectedItems.map(item => ({
                      inventoryId: item.id,
                      binGroupCode: this.selectedBin.binGroupCode,
                      binCode: this.selectedBin.binCode
                    }))
                  },
                  remove: false
                };
                this.binToBinTransferFacade.confirmTransferItems(
                  confirmTransferRequest
                );
              }
            }
          });
      });
  }

  showConfirmReceiveSuccessNotification(transferNumber: number) {
    const key =
      'pw.binToBinTransferNotificationMessages.confirmTransferSuccessMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.SIMPLE,
            message: translatedMsg + ' ' + transferNumber,
            hasBackdrop: true,
            hasClose: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
            this.binToBinTransferFacade.clearConfirmTransferResponse();
            if (
              this.type === BinToBinTransferTypeEnum.BIN_CODE &&
              (this.isSelectAll ||
                !(
                  this.previousItemGroupQuantity > this.previousSelectedQuantity
                ))
            ) {
              this.back();
            }
          });
      });
  }

  showAsaignDestinationBinNotification(count: number = 0) {
    const key =
      'pw.binToBinTransferNotificationMessages.assignDestinationBulkBinMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.SIMPLE,
            message: count + ' ' + translatedMessage
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
          });
      });
  }

  showProgressNotification() {
    const key = 'pw.binToBinTransferNotificationMessages.progressMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.PROGRESS,
            message: translatedMsg,
            hasBackdrop: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
          });
      });
  }

  errorHandler(error: CustomErrors) {
    this.hasNotification = true;
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        this.hasNotification = false;
        this.showNotification();
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
