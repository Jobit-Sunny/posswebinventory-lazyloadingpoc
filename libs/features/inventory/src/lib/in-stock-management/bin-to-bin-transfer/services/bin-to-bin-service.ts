import {
  LoadItemListGroupResponse,
  LoadItemsResponse,
  ConfirmTransferAllItemsRequest,
  ConfirmTransferItemsRequest,
  ConfirmTransferResponse
} from '../models/bin-to-bin-transfer.model';

import {
  getBinToBinTransferConfirmTransferItemsUrl,
  getBinToBinTransferGetItemsUrl,
  getBinToBinTransferGetItemListGroupsUrl,
  getBinToBinTransferConfirmTransferAllItemsUrl,
  getBinToBinTransferSearchItemsUrl
} from '../../../endpoints.constants';
import { Injectable } from '@angular/core';
import { ApiService } from '@poss-web/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BinToBinTransferItemHelper } from '../helpers/Item.helper';
import { ItemListGroupHelper } from '../helpers/item-list-group.helper';
import { BIN_TYPE } from '../models/bin-data';

@Injectable()
export class BinToBinTransferService {
  constructor(private apiService: ApiService) {}

  getItemListGroups(
    type: string,
    pageIndex: number,
    pageSize: number,
    value?: string
  ): Observable<LoadItemListGroupResponse> {
    const url = getBinToBinTransferGetItemListGroupsUrl(
      type,
      pageIndex,
      pageSize,
      value,
      BIN_TYPE
    );
    return this.apiService
      .get(url.path, url.params)
      .pipe(map((data: any) => ItemListGroupHelper.getInfo(data)));
  }

  searchItems(
    itemCode: string,
    lotNumber: string
  ): Observable<LoadItemsResponse> {
    const url = getBinToBinTransferSearchItemsUrl(
      itemCode,
      lotNumber,
      BIN_TYPE
    );

    return this.apiService
      .get(url.path, url.params)
      .pipe(map((data: any) => BinToBinTransferItemHelper.getItems(data)));
  }
  getItems(
    itemCode: string,
    lotNumber: string,
    type: string,
    value: string,
    pageIndex: number,
    pageSize: number,
    sortBy: string,
    sortOrder: string,
    filter: { key: string; value: any[] }[]
  ): Observable<LoadItemsResponse> {
    const url = getBinToBinTransferGetItemsUrl(
      itemCode,
      lotNumber,
      type,
      value,
      pageIndex,
      pageSize,
      sortBy,
      sortOrder,
      filter,
      BIN_TYPE
    );
    return this.apiService
      .get(url.path, url.params)
      .pipe(map((data: any) => BinToBinTransferItemHelper.getItems(data)));
  }

  confirmTransferItems(
    confirmTransferItemsRequest: ConfirmTransferItemsRequest
  ): Observable<ConfirmTransferResponse> {
    {
      const url = getBinToBinTransferConfirmTransferItemsUrl();
      return this.apiService
        .patch(url, confirmTransferItemsRequest.request)
        .pipe(map((data: any) => ({ transferId: data['docNo'] })));
    }
  }

  confirmTransferAllItems(
    confirmTransferAllItemsRequest: ConfirmTransferAllItemsRequest
  ): Observable<ConfirmTransferResponse> {
    {
      const url = getBinToBinTransferConfirmTransferAllItemsUrl(
        confirmTransferAllItemsRequest
      );
      return this.apiService
        .patch(url.path, null, url.params)
        .pipe(map((data: any) => ({ transferId: data['docNo'] })));
    }
  }
}
