import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BinToBinTransferRoutingModule } from './bin-to-bin-transfer-routing.module';
import { SharedModule, UicomponentsModule } from '@poss-web/shared';
import { MatSidenavModule } from '@angular/material';
import { EffectsModule } from '@ngrx/effects';
import { BinToBinTransferEffect } from './+state/bin-to-bin-transfer.effect';
import { BinToBinTransferListComponent } from './containers/bin-to-bin-transfer-list/bin-to-bin-transfer-list.component';
import { BinToBinTransferItemComponent } from './components/bin-to-bin-transfer-item/bin-to-bin-transfer-item.component';
import { BinToBinTransferItemListComponent } from './components/bin-to-bin-transfer-item-list/bin-to-bin-transfer-item-list.component';
import { BinToBinTransferDetailsComponent } from './containers/bin-to-bin-transfer-details/bin-to-bin-transfer-details.component';
import { BinToBinTransferService } from './services/bin-to-bin-service';
import { BinToBinTransferFacade } from './+state/bin-to-bin-transfer.facade';

@NgModule({
  declarations: [
    BinToBinTransferItemComponent,
    BinToBinTransferItemListComponent,
    BinToBinTransferDetailsComponent,
    BinToBinTransferListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    UicomponentsModule,
    MatSidenavModule,
    BinToBinTransferRoutingModule,
    EffectsModule.forFeature([BinToBinTransferEffect])
  ],
  providers: [BinToBinTransferFacade, BinToBinTransferService]
})
export class BinToBinTransferModule {}
