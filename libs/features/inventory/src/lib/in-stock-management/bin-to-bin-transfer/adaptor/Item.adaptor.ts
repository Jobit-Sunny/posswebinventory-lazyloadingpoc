import * as moment from 'moment';
import { Item } from '../models/bin-to-bin-transfer.model';

export class ItemAdaptor {
  static fromJson(item: any): Item {
    return {
      id: item.id,
      binCode: item.binCode,
      itemCode: item.itemCode,
      itemDetails: item.itemDetails,
      stdValue: item.stdValue ? item.stdValue : 0,
      stdWeight:
        item.stdWeight && !isNaN(item.stdWeight)
          ? +Number.parseFloat(item.stdWeight).toString(3)
          : 0,
      lotNumber: item.lotNumber,
      mfgDate: item.mfgDate ? moment(item.mfgDate) : null,
      availableValue: item.availableValue ? item.availableValue : 0,
      availableWeight:
        item.availableWeight && !isNaN(item.availableWeight)
          ? +Number.parseFloat(item.availableWeight).toFixed(3)
          : 0,
      currencyCode: item.currencyCode,
      weightUnit: item.weightUnit,
      imageURL: item.imageURL,
      binGroupCode: item.binGroupCode,
      availableQuantity: item.availableQuantity,
      productCategory: item.productCategory,
      productGroup: item.productGroup,
      isSelected: false,
      isDisabled: false,
      destinationBinGroupCode: null,
      destinationBinCode: null
    };
  }
}
