import { ItemListGroup } from '../models/bin-to-bin-transfer.model';

export class ItemListGroupAdaptor {
  static fromJson(group: any): ItemListGroup {
    return {
      name: group.name,
      products: group.products ? group.products : 0,
      totalValue: group.totalValue ? group.totalValue : 0,
      totalWeight: group.totalWeight ? group.totalWeight : 0,
      currencyCode: group.currencyCode,
      weightUnit: group.weightUnit
    };
  }
}
