import {
  ItemListGroup,
  ConfirmTransferResponse,
  FilterOption
} from '../models/bin-to-bin-transfer.model';
import { ItemEntity, ItemListGroupEntity } from './bin-to-bin-transfer.entity';
import {
  CustomErrors,
  StoreBin,
  ProductGroup,
  ProductCategory
} from '@poss-web/core';

export interface BinToBinTransferState {
  confirmTransferResponse: ConfirmTransferResponse;
  searchedItemList: ItemEntity;
  isSearchingItems: boolean;
  hasSearchedItems: boolean;
  itemList: ItemEntity;
  isLoadingItems: boolean;
  isLoadingItemsSuccess: boolean;
  itemsTotalCount: number;
  sourceBins: ItemListGroupEntity;
  sourceBinsTotalCount: number;
  productGroups: ItemListGroupEntity;
  productGroupsTotalCount: number;
  productCategory: ItemListGroupEntity;
  productCategoryTotalCount: number;
  searchedItemListGroups: ItemListGroupEntity;
  searchedItemListGroupsTotalCount: number;
  selectedItemListGroup: ItemListGroup;
  isLoadingSelectedItemListGroupSuccess: boolean;
  isLoadingItemListGroup: boolean;
  bins: StoreBin[];
  isLoadingBins: boolean;
  error: CustomErrors;

  productGroupOptions: ProductGroup[];
  isLoadingProductGroupOptions: boolean;

  productCategoryOptions: ProductCategory[];
  isLoadingProductCategoryOptions: boolean;

  soruceBinOptions: FilterOption[];
  isLoadingSoruceBinOptionsOptions: boolean;
}
