import {
  ConfirmTransferItemsRequest,
  ConfirmTransferAllItemsRequest,
  Item,
  ChangeSelectionPayload,
  UpdateDestinationBinPayload,
  LoadItemsPayload,
  LoadItemGroupsPayload
} from '../models/bin-to-bin-transfer.model';
import { State } from '../../../inventory.state';
import { Injectable } from '@angular/core';
import { BinToBinTransferSelectors } from './bin-to-bin-transfer.selectors';
import { Store } from '@ngrx/store';
import * as BinToBinTransferActions from './bin-to-bin-transfer-actions';

@Injectable()
export class BinToBinTransferFacade {
  private error$ = this.store.select(BinToBinTransferSelectors.selectError);

  private sourceBins$ = this.store.select(
    BinToBinTransferSelectors.selectSourceBins
  );

  private sourceBinsTotalCount$ = this.store.select(
    BinToBinTransferSelectors.selectSourceBinsTotalCount
  );

  private productCategory$ = this.store.select(
    BinToBinTransferSelectors.selectProductCategory
  );

  private productCategoryTotalCount$ = this.store.select(
    BinToBinTransferSelectors.selectProductCategoryTotalCount
  );

  private productGroups$ = this.store.select(
    BinToBinTransferSelectors.selectProductGroups
  );

  private productGroupsTotalCount$ = this.store.select(
    BinToBinTransferSelectors.selectProductGroupsTotalCount
  );

  private searchedItemListGroups$ = this.store.select(
    BinToBinTransferSelectors.selectSearchedItemListGroups
  );

  private searchedItemListGroupsTotalCount$ = this.store.select(
    BinToBinTransferSelectors.selectSearchedItemListGroupsTotalCount
  );

  private isLoadingItemListGroup$ = this.store.select(
    BinToBinTransferSelectors.selectIsLoadingItemListGroup
  );

  private selectedItemListGroup$ = this.store.select(
    BinToBinTransferSelectors.selectSelectedItemListGroup
  );
  private isLoadingSelectedItemListGroupSuccess$ = this.store.select(
    BinToBinTransferSelectors.selectIsLoadingSelectedItemListGroupSuccess
  );

  private searchedItemList$ = this.store.select(
    BinToBinTransferSelectors.selectSearchedItemList
  );

  private isSearchingItems$ = this.store.select(
    BinToBinTransferSelectors.selectIsSearchingItems
  );

  private hasSearchedItems$ = this.store.select(
    BinToBinTransferSelectors.selectHasSearchedItems
  );

  private itemList$ = this.store.select(
    BinToBinTransferSelectors.selectItemList
  );

  private isLoadingItems$ = this.store.select(
    BinToBinTransferSelectors.selectIsLoadingItems
  );

  private itemsTotalCount$ = this.store.select(
    BinToBinTransferSelectors.selectItemsTotalCount
  );

  private isLoadingItemsSuccess$ = this.store.select(
    BinToBinTransferSelectors.selectIsLoadingItemsSuccess
  );

  private confirmTransferResponse$ = this.store.select(
    BinToBinTransferSelectors.selectConfirmTransferResponse
  );

  private bins$ = this.store.select(BinToBinTransferSelectors.selectBins);

  private isLoadingBins$ = this.store.select(
    BinToBinTransferSelectors.selectIsLoadingBins
  );

  private productGroupOptions$ = this.store.select(
    BinToBinTransferSelectors.selectProductGroupOptions
  );

  private isLoadingProductGroupOptions$ = this.store.select(
    BinToBinTransferSelectors.selectIsLoadingProductGroupOptions
  );

  private productCategoryOptions$ = this.store.select(
    BinToBinTransferSelectors.selectProductCategoryOptions
  );

  private isLoadingProductCategoryOptions$ = this.store.select(
    BinToBinTransferSelectors.selectIsLoadingProductCategoryOptions
  );
  private soruceBinOptions$ = this.store.select(
    BinToBinTransferSelectors.selectSoruceBinOptions
  );

  private isLoadingSoruceBinOptionsOptions$ = this.store.select(
    BinToBinTransferSelectors.selectIsLoadingSoruceBinOptionsOptions
  );

  constructor(private store: Store<State>) {}

  getBins() {
    return this.bins$;
  }
  getIsLoadingBins() {
    return this.isLoadingBins$;
  }

  getError() {
    return this.error$;
  }

  getSourceBins() {
    return this.sourceBins$;
  }

  getSourceBinsTotalCount() {
    return this.sourceBinsTotalCount$;
  }

  getProductCategory() {
    return this.productCategory$;
  }

  getProductCategoryTotalCount() {
    return this.productCategoryTotalCount$;
  }

  getProductGroups() {
    return this.productGroups$;
  }

  getProductGroupsTotalCount() {
    return this.productGroupsTotalCount$;
  }

  getSelectedItemListGroup() {
    return this.selectedItemListGroup$;
  }

  getIsLoadingSelectedItemListGroupSuccess() {
    return this.isLoadingSelectedItemListGroupSuccess$;
  }

  getSearchedItemListGroups() {
    return this.searchedItemListGroups$;
  }

  getSearchedItemListGroupsTotalCount() {
    return this.searchedItemListGroupsTotalCount$;
  }

  getIsLoadingItemListGroup() {
    return this.isLoadingItemListGroup$;
  }

  getItemList() {
    return this.itemList$;
  }

  getIsLoadingItemsSuccess() {
    return this.isLoadingItemsSuccess$;
  }

  getSearchedItemList() {
    return this.searchedItemList$;
  }
  getIsSearchingItems() {
    return this.isSearchingItems$;
  }

  getHasSearchedItems() {
    return this.hasSearchedItems$;
  }

  getIsLoadingItems() {
    return this.isLoadingItems$;
  }

  getItemsTotalCount() {
    return this.itemsTotalCount$;
  }

  getConfirmTransferResponse() {
    return this.confirmTransferResponse$;
  }
  getProductGroupOptions() {
    return this.productGroupOptions$;
  }
  getIsLoadingProductGroupOptions() {
    return this.isLoadingProductGroupOptions$;
  }
  getProductCategoryOptions() {
    return this.productCategoryOptions$;
  }
  getIsLoadingProductCategoryOptions() {
    return this.isLoadingProductCategoryOptions$;
  }

  getSoruceBinOptions() {
    return this.soruceBinOptions$;
  }
  getIsLoadingSoruceBinOptionsOptions() {
    return this.isLoadingSoruceBinOptionsOptions$;
  }

  loadBins() {
    this.store.dispatch(new BinToBinTransferActions.LoadBins());
  }

  addToItemList(items: Item[]) {
    this.store.dispatch(new BinToBinTransferActions.AddToItemList(items));
  }

  updateItemList(item: Item) {
    this.store.dispatch(new BinToBinTransferActions.UpdateItemList(item));
  }

  deleteFromItemList(itemId: string) {
    this.store.dispatch(new BinToBinTransferActions.DeleteFromItemList(itemId));
  }

  changeSelectionOfAllItems(changeSelectionPayload: ChangeSelectionPayload) {
    this.store.dispatch(
      new BinToBinTransferActions.ChangeSelectionOfAllItems(
        changeSelectionPayload
      )
    );
  }
  updateDestinationBinForSelectedItems(
    updateDestinationBinPayload: UpdateDestinationBinPayload
  ) {
    this.store.dispatch(
      new BinToBinTransferActions.UpdateDestinationBinForSelectedItems(
        updateDestinationBinPayload
      )
    );
  }
  deleteSelectedItems(idList: string[]) {
    this.store.dispatch(
      new BinToBinTransferActions.DeleteSelectedItems(idList)
    );
  }

  loadSourceBins(loadItemGroupsPayload: LoadItemGroupsPayload) {
    this.store.dispatch(
      new BinToBinTransferActions.LoadSourceBins(loadItemGroupsPayload)
    );
  }

  loadProductsCategory(loadItemGroupsPayload: LoadItemGroupsPayload) {
    this.store.dispatch(
      new BinToBinTransferActions.LoadProductsCategory(loadItemGroupsPayload)
    );
  }

  loadProductsGroups(loadItemGroupsPayload: LoadItemGroupsPayload) {
    this.store.dispatch(
      new BinToBinTransferActions.LoadProductsGroups(loadItemGroupsPayload)
    );
  }

  clearItemsGroups() {
    this.store.dispatch(new BinToBinTransferActions.ClearItemsGroups());
  }

  loadItemGroup(loadItemGroupsPayload: LoadItemGroupsPayload) {
    this.store.dispatch(
      new BinToBinTransferActions.LoadItemGroup(loadItemGroupsPayload)
    );
  }

  clearSelectedItemGroup() {
    this.store.dispatch(new BinToBinTransferActions.ClearSelectedItemGroup());
  }

  searchItemGroups(loadItemGroupsPayload: LoadItemGroupsPayload) {
    this.store.dispatch(
      new BinToBinTransferActions.SearchItemGroups(loadItemGroupsPayload)
    );
  }

  loadItems(loadItemsPayload: LoadItemsPayload) {
    this.store.dispatch(
      new BinToBinTransferActions.LoadItems(loadItemsPayload)
    );
  }

  searchItems(loadItemsPayload: LoadItemsPayload) {
    this.store.dispatch(
      new BinToBinTransferActions.SearchItems(loadItemsPayload)
    );
  }

  loadProductGroupOptions() {
    this.store.dispatch(new BinToBinTransferActions.LoadProductGroupOptions());
  }

  loadProductCategoryOptions() {
    this.store.dispatch(
      new BinToBinTransferActions.LoadProductCategoryOptions()
    );
  }

  loadSourceBinOptions() {
    this.store.dispatch(new BinToBinTransferActions.LoadSourceBinOptions());
  }

  confirmTransferItems(
    confirmTransferItemsRequest: ConfirmTransferItemsRequest
  ) {
    this.store.dispatch(
      new BinToBinTransferActions.ConfirmTransferItems(
        confirmTransferItemsRequest
      )
    );
  }

  confirmTransferAllItems(
    confirmTransferAllItemsRequest: ConfirmTransferAllItemsRequest
  ) {
    this.store.dispatch(
      new BinToBinTransferActions.ConfirmTransferAllItems(
        confirmTransferAllItemsRequest
      )
    );
  }

  clearConfirmTransferResponse() {
    this.store.dispatch(
      new BinToBinTransferActions.ClearConfirmTransferResponse()
    );
  }

  clearSearchedItems() {
    this.store.dispatch(new BinToBinTransferActions.ClearSearchedItems());
  }

  clearItems() {
    this.store.dispatch(new BinToBinTransferActions.ClearItems());
  }
}
