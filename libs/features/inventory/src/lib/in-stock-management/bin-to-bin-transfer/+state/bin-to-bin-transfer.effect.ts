import { map, tap } from 'rxjs/operators';
import { BinToBinTransferActionTypes } from './bin-to-bin-transfer-actions';
import { Effect } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { DataPersistence } from '@nrwl/angular';
import {
  NotificationService,
  CustomErrors,
  CustomErrorAdaptor,
  StoreConfigDataService,
  StoreBin,
  ProductGroupDataService,
  ProductCategoryDataService,
  ProductGroup,
  ProductCategory
} from '@poss-web/core';
import * as BinToBinTransferActions from './bin-to-bin-transfer-actions';
import { HttpErrorResponse } from '@angular/common/http';
import { BinToBinTransferService } from '../services/bin-to-bin-service';
import {
  ConfirmTransferResponse,
  LoadItemListGroupResponse,
  LoadItemsResponse
} from '../models/bin-to-bin-transfer.model';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { BIN_TYPE } from '../models/bin-data';
import { BinToBinTransferTypeEnum } from '../models/bin-to-bin-transfer.enum';

const MAX_PAGE_SIZE = 10000;
@Injectable()
export class BinToBinTransferEffect {
  constructor(
    private service: BinToBinTransferService,
    private dataPersistence: DataPersistence<any>,
    private notificationService: NotificationService,
    private storeConfigDataService: StoreConfigDataService,
    private productGroupDataService: ProductGroupDataService,
    private productCategoryDataService: ProductCategoryDataService
  ) {}

  @Effect() loadSourceBins$: Observable<Action> = this.dataPersistence.fetch(
    BinToBinTransferActionTypes.LOAD_SOURCE_BINS,
    {
      run: (action: BinToBinTransferActions.LoadSourceBins) => {
        return this.service
          .getItemListGroups(
            action.payload.type,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (loadItemListGroupResponse: LoadItemListGroupResponse) =>
                new BinToBinTransferActions.LoadSourceBinsSuccess(
                  loadItemListGroupResponse
                )
            )
          );
      },

      onError: (
        action: BinToBinTransferActions.LoadSourceBins,
        error: HttpErrorResponse
      ) => {
        return new BinToBinTransferActions.LoadSourceBinsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadProductsGroups$: Observable<
    Action
  > = this.dataPersistence.fetch(
    BinToBinTransferActionTypes.LOAD_PRODUCT_GROUPS,
    {
      run: (action: BinToBinTransferActions.LoadProductsGroups) => {
        return this.service
          .getItemListGroups(
            action.payload.type,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (loadItemListGroupResponse: LoadItemListGroupResponse) =>
                new BinToBinTransferActions.LoadProductsGroupsSuccess(
                  loadItemListGroupResponse
                )
            )
          );
      },

      onError: (
        action: BinToBinTransferActions.LoadProductsGroups,
        error: HttpErrorResponse
      ) => {
        return new BinToBinTransferActions.LoadProductsGroupsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadProductsCategory$: Observable<
    Action
  > = this.dataPersistence.fetch(
    BinToBinTransferActionTypes.LOAD_PRODUCT_CATEGORY,
    {
      run: (action: BinToBinTransferActions.LoadProductsCategory) => {
        return this.service
          .getItemListGroups(
            action.payload.type,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (loadItemListGroupResponse: LoadItemListGroupResponse) =>
                new BinToBinTransferActions.LoadProductsCategorySuccess(
                  loadItemListGroupResponse
                )
            )
          );
      },

      onError: (
        action: BinToBinTransferActions.LoadProductsCategory,
        error: HttpErrorResponse
      ) => {
        return new BinToBinTransferActions.LoadProductsCategoryFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadItemGroup$: Observable<Action> = this.dataPersistence.fetch(
    BinToBinTransferActionTypes.LOAD_ITEM_GROUP,
    {
      run: (action: BinToBinTransferActions.LoadItemGroup) => {
        return this.service
          .getItemListGroups(
            action.payload.type,
            null,
            null,
            action.payload.value
          )
          .pipe(
            map(
              (loadItemListGroupResponse: LoadItemListGroupResponse) =>
                new BinToBinTransferActions.LoadItemGroupSuccess(
                  loadItemListGroupResponse.itemListGroups[0]
                )
            )
          );
      },

      onError: (
        action: BinToBinTransferActions.LoadItemGroup,
        error: HttpErrorResponse
      ) => {
        return new BinToBinTransferActions.LoadItemGroupFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() searchItemGroups$: Observable<Action> = this.dataPersistence.fetch(
    BinToBinTransferActionTypes.SEARCH_ITEM_GROUPS,
    {
      run: (action: BinToBinTransferActions.SearchItemGroups) => {
        return this.service
          .getItemListGroups(
            action.payload.type,
            action.payload.pageIndex,
            action.payload.pageSize,
            action.payload.value
          )
          .pipe(
            map(
              (loadItemListGroupResponse: LoadItemListGroupResponse) =>
                new BinToBinTransferActions.SearchItemGroupsSuccess(
                  loadItemListGroupResponse
                )
            )
          );
      },

      onError: (
        action: BinToBinTransferActions.SearchItemGroups,
        error: HttpErrorResponse
      ) => {
        return new BinToBinTransferActions.SearchItemGroupsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadItems$ = this.dataPersistence.fetch(
    BinToBinTransferActionTypes.LOAD_ITEMS,
    {
      run: (action: BinToBinTransferActions.LoadItems) => {
        return this.service
          .getItems(
            action.payload.itemCode,
            action.payload.lotNumber,
            action.payload.type,
            action.payload.value,
            action.payload.pageIndex,
            action.payload.pageSize,
            action.payload.sortBy,
            action.payload.sortOrder,
            action.payload.filter
          )
          .pipe(
            map(
              (loadItemsResponse: LoadItemsResponse) =>
                new BinToBinTransferActions.LoadItemsSuccess(loadItemsResponse)
            )
          );
      },

      onError: (
        action: BinToBinTransferActions.LoadItems,
        error: HttpErrorResponse
      ) => {
        return new BinToBinTransferActions.LoadItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() searchItems$ = this.dataPersistence.fetch(
    BinToBinTransferActionTypes.SEARCH_ITEMS,
    {
      run: (action: BinToBinTransferActions.SearchItems) => {
        return this.service
          .searchItems(action.payload.itemCode, action.payload.lotNumber)
          .pipe(
            map(
              (loadItemsResponse: LoadItemsResponse) =>
                new BinToBinTransferActions.SearchItemsSuccess(
                  loadItemsResponse
                )
            )
          );
      },

      onError: (
        action: BinToBinTransferActions.SearchItems,
        error: HttpErrorResponse
      ) => {
        return new BinToBinTransferActions.SearchItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadBins$ = this.dataPersistence.fetch(
    BinToBinTransferActionTypes.LOAD_BINS,
    {
      run: (action: BinToBinTransferActions.LoadBins) => {
        return this.storeConfigDataService
          .getStoreBins('RECEIVE_BIN')
          .pipe(
            map(
              (bins: StoreBin[]) =>
                new BinToBinTransferActions.LoadBinsSuccess(bins)
            )
          );
      },

      onError: (
        action: BinToBinTransferActions.LoadBins,
        error: HttpErrorResponse
      ) => {
        return new BinToBinTransferActions.LoadBinsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadProductGroupsOptions$ = this.dataPersistence.fetch(
    BinToBinTransferActionTypes.LOAD_PRODUCT_GROUP_OPTIONS,
    {
      run: () => {
        return this.productGroupDataService
          .getProductGroups()
          .pipe(
            map(
              (data: ProductGroup[]) =>
                new BinToBinTransferActions.LoadProductGroupOptionsSuccess(data)
            )
          );
      },

      onError: (
        action: BinToBinTransferActions.LoadProductGroupOptions,
        error: HttpErrorResponse
      ) => {
        return new BinToBinTransferActions.LoadProductGroupOptionsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadProductCategoriesOptions$ = this.dataPersistence.fetch(
    BinToBinTransferActionTypes.LOAD_PRODUCT_CATEGORY_OPTIONS,
    {
      run: () => {
        return this.productCategoryDataService
          .getProductCategories()
          .pipe(
            map(
              (data: ProductCategory[]) =>
                new BinToBinTransferActions.LoadProductCategoryOptionsSuccess(
                  data
                )
            )
          );
      },

      onError: (
        action: BinToBinTransferActions.LoadProductCategoryOptions,
        error: HttpErrorResponse
      ) => {
        return new BinToBinTransferActions.LoadProductCategoryOptionsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadSourceBinOptions$ = this.dataPersistence.fetch(
    BinToBinTransferActionTypes.LOAD_SOURCE_BIN_OPTIONS,
    {
      run: () => {
        return this.service
          .getItemListGroups(
            BinToBinTransferTypeEnum.BIN_CODE,
            0,
            MAX_PAGE_SIZE
          )
          .pipe(
            map(
              data =>
                new BinToBinTransferActions.LoadSourceBinOptionsSuccess(
                  data.itemListGroups.map(ele => ({
                    id: ele.name,
                    description: ele.name
                  }))
                )
            )
          );
      },

      onError: (
        action: BinToBinTransferActions.LoadSourceBinOptions,
        error: HttpErrorResponse
      ) => {
        return new BinToBinTransferActions.LoadSourceBinOptionsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() confirmTransferItems$ = this.dataPersistence.pessimisticUpdate(
    BinToBinTransferActionTypes.CONFIRM_TRANSFER_ITEMS,
    {
      run: (action: BinToBinTransferActions.ConfirmTransferItems) => {
        return this.service.confirmTransferItems(action.payload).pipe(
          map(
            (confirmTransferResponse: ConfirmTransferResponse) =>
              new BinToBinTransferActions.ConfirmTransferItemsSuccess({
                confirmTransferResponse,
                itemId: action.payload.request.binItems.map(
                  request => request.inventoryId
                ),
                remove: action.payload.remove
              })
          )
        );
      },

      onError: (
        action: BinToBinTransferActions.ConfirmTransferItems,
        error: HttpErrorResponse
      ) => {
        return new BinToBinTransferActions.ConfirmTransferItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() confirmTransferAllItems$ = this.dataPersistence.pessimisticUpdate(
    BinToBinTransferActionTypes.CONFIRM_TRANSFER_ALL_ITEMS,
    {
      run: (action: BinToBinTransferActions.ConfirmTransferAllItems) => {
        return this.service
          .confirmTransferAllItems(action.payload)
          .pipe(
            map(
              (confirmTransferResponse: ConfirmTransferResponse) =>
                new BinToBinTransferActions.ConfirmTransferAllItemsSuccess(
                  confirmTransferResponse
                )
            )
          );
      },

      onError: (
        action: BinToBinTransferActions.ConfirmTransferAllItems,
        error: HttpErrorResponse
      ) => {
        return new BinToBinTransferActions.ConfirmTransferAllItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
}
