import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { ItemListGroup, Item } from '../models/bin-to-bin-transfer.model';

export interface ItemEntity extends EntityState<Item> {}
export const itemAdapter = createEntityAdapter<Item>({
  selectId: item => item.id
});
export const itemSelector = itemAdapter.getSelectors();

export interface ItemListGroupEntity extends EntityState<ItemListGroup> {}
export const itemListGroupAdapter = createEntityAdapter<ItemListGroup>({
  selectId: itemListGroup => itemListGroup.name
});
export const itemListGroupSelector = itemListGroupAdapter.getSelectors();
