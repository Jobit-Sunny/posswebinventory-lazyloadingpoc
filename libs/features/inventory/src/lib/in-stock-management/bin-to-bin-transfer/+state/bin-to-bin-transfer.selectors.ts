import {
  itemSelector,
  itemListGroupSelector
} from './bin-to-bin-transfer.entity';
import { selectInventory } from '../../../inventory.state';
import { createSelector } from '@ngrx/store';

const selectError = createSelector(
  selectInventory,
  state => state.binToBinTransfer.error
);

const sourceBins = createSelector(
  selectInventory,
  state => state.binToBinTransfer.sourceBins
);

const selectSourceBins = createSelector(
  sourceBins,
  itemListGroupSelector.selectAll
);

const selectSourceBinsTotalCount = createSelector(
  selectInventory,
  state => state.binToBinTransfer.sourceBinsTotalCount
);

const productCategory = createSelector(
  selectInventory,
  state => state.binToBinTransfer.productCategory
);

const selectProductCategory = createSelector(
  productCategory,
  itemListGroupSelector.selectAll
);

const selectProductCategoryTotalCount = createSelector(
  selectInventory,
  state => state.binToBinTransfer.productCategoryTotalCount
);

const productGroups = createSelector(
  selectInventory,
  state => state.binToBinTransfer.productGroups
);

const selectProductGroups = createSelector(
  productGroups,
  itemListGroupSelector.selectAll
);

const selectProductGroupsTotalCount = createSelector(
  selectInventory,
  state => state.binToBinTransfer.productGroupsTotalCount
);

const selectProductGroupOptions = createSelector(
  selectInventory,
  state => state.binToBinTransfer.productGroupOptions
);

const selectIsLoadingProductGroupOptions = createSelector(
  selectInventory,
  state => state.binToBinTransfer.isLoadingProductGroupOptions
);

const selectProductCategoryOptions = createSelector(
  selectInventory,
  state => state.binToBinTransfer.productCategoryOptions
);

const selectIsLoadingProductCategoryOptions = createSelector(
  selectInventory,
  state => state.binToBinTransfer.isLoadingProductCategoryOptions
);

const selectSoruceBinOptions = createSelector(
  selectInventory,
  state => state.binToBinTransfer.soruceBinOptions
);

const selectIsLoadingSoruceBinOptionsOptions = createSelector(
  selectInventory,
  state => state.binToBinTransfer.isLoadingSoruceBinOptionsOptions
);

const searchedItemListGroups = createSelector(
  selectInventory,
  state => state.binToBinTransfer.searchedItemListGroups
);

const selectSearchedItemListGroups = createSelector(
  searchedItemListGroups,
  itemListGroupSelector.selectAll
);

const selectSearchedItemListGroupsTotalCount = createSelector(
  selectInventory,
  state => state.binToBinTransfer.searchedItemListGroupsTotalCount
);

const selectSelectedItemListGroup = createSelector(
  selectInventory,
  state => state.binToBinTransfer.selectedItemListGroup
);
const selectIsLoadingSelectedItemListGroupSuccess = createSelector(
  selectInventory,
  state => state.binToBinTransfer.isLoadingSelectedItemListGroupSuccess
);

const selectIsLoadingItemListGroup = createSelector(
  selectInventory,
  state => state.binToBinTransfer.isLoadingItemListGroup
);

const itemList = createSelector(
  selectInventory,
  state => state.binToBinTransfer.itemList
);

const selectItemList = createSelector(
  itemList,
  itemSelector.selectAll
);
const selectIsLoadingItemsSuccess = createSelector(
  selectInventory,
  state => state.binToBinTransfer.isLoadingItemsSuccess
);

const selectIsLoadingItems = createSelector(
  selectInventory,
  state => state.binToBinTransfer.isLoadingItems
);

const searchedItemList = createSelector(
  selectInventory,
  state => state.binToBinTransfer.searchedItemList
);

const selectSearchedItemList = createSelector(
  searchedItemList,
  itemSelector.selectAll
);

const selectIsSearchingItems = createSelector(
  selectInventory,
  state => state.binToBinTransfer.isSearchingItems
);

const selectHasSearchedItems = createSelector(
  selectInventory,
  state => state.binToBinTransfer.hasSearchedItems
);

const selectItemsTotalCount = createSelector(
  selectInventory,
  state => state.binToBinTransfer.itemsTotalCount
);

const selectConfirmTransferResponse = createSelector(
  selectInventory,
  state => state.binToBinTransfer.confirmTransferResponse
);

const selectIsLoadingBins = createSelector(
  selectInventory,
  state => state.binToBinTransfer.isLoadingBins
);

const selectBins = createSelector(
  selectInventory,
  state => state.binToBinTransfer.bins
);

export const BinToBinTransferSelectors = {
  selectError,
  selectSearchedItemList,
  selectIsSearchingItems,
  selectHasSearchedItems,
  selectSourceBinsTotalCount,
  selectSourceBins,
  selectProductCategory,
  selectProductCategoryTotalCount,
  selectProductGroups,
  selectProductGroupsTotalCount,
  selectSearchedItemListGroups,
  selectSearchedItemListGroupsTotalCount,
  selectIsLoadingItemListGroup,
  selectItemList,
  selectIsLoadingItemsSuccess,
  selectIsLoadingItems,
  selectItemsTotalCount,
  selectConfirmTransferResponse,
  selectSelectedItemListGroup,
  selectIsLoadingSelectedItemListGroupSuccess,
  selectBins,
  selectIsLoadingBins,
  selectProductGroupOptions,
  selectIsLoadingProductGroupOptions,
  selectProductCategoryOptions,
  selectIsLoadingProductCategoryOptions,
  selectSoruceBinOptions,
  selectIsLoadingSoruceBinOptionsOptions
};
