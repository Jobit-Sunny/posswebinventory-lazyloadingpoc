import {
  BinToBinTransferActions,
  BinToBinTransferActionTypes
} from './bin-to-bin-transfer-actions';
import { BinToBinTransferState } from './bin-to-bin-transfer.state';
import {
  itemAdapter,
  itemListGroupAdapter
} from './bin-to-bin-transfer.entity';
import { ItemListGroup, Item } from '../models/bin-to-bin-transfer.model';
import { Update } from '@ngrx/entity';

const initialState: BinToBinTransferState = {
  searchedItemList: itemAdapter.getInitialState(),
  isSearchingItems: false,
  hasSearchedItems: null,

  itemList: itemAdapter.getInitialState(),
  isLoadingItems: false,
  isLoadingItemsSuccess: null,
  itemsTotalCount: 0,

  confirmTransferResponse: null,

  sourceBins: itemListGroupAdapter.getInitialState(),
  sourceBinsTotalCount: 0,

  productGroups: itemListGroupAdapter.getInitialState(),
  productGroupsTotalCount: 0,

  productCategory: itemListGroupAdapter.getInitialState(),
  productCategoryTotalCount: 0,

  searchedItemListGroups: itemListGroupAdapter.getInitialState(),
  searchedItemListGroupsTotalCount: 0,

  selectedItemListGroup: null,
  isLoadingSelectedItemListGroupSuccess: null,

  isLoadingItemListGroup: false,

  bins: [],
  isLoadingBins: false,

  productGroupOptions: [],
  isLoadingProductGroupOptions: false,

  productCategoryOptions: [],
  isLoadingProductCategoryOptions: false,

  soruceBinOptions: [],
  isLoadingSoruceBinOptionsOptions: false,

  error: null
};

export function BinToBinTransferReducer(
  state: BinToBinTransferState = initialState,
  action: BinToBinTransferActions
): BinToBinTransferState {
  switch (action.type) {
    case BinToBinTransferActionTypes.LOAD_SOURCE_BINS:
    case BinToBinTransferActionTypes.LOAD_PRODUCT_GROUPS:
    case BinToBinTransferActionTypes.LOAD_PRODUCT_CATEGORY:
      return {
        ...state,
        isLoadingItemListGroup: true,
        error: null
      };

    case BinToBinTransferActionTypes.LOAD_SOURCE_BINS_SUCCESS:
      return {
        ...state,
        sourceBins: itemListGroupAdapter.addMany(
          action.payload.itemListGroups,
          state.sourceBins
        ),
        sourceBinsTotalCount: action.payload.count,
        isLoadingItemListGroup: false
      };

    case BinToBinTransferActionTypes.LOAD_SOURCE_BINS_FAILURE:
    case BinToBinTransferActionTypes.LOAD_PRODUCT_GROUPS_FAILURE:
    case BinToBinTransferActionTypes.LOAD_PRODUCT_CATEGORY_FAILURE:
    case BinToBinTransferActionTypes.LOAD_ITEM_GROUP_FAILURE:
    case BinToBinTransferActionTypes.SERACH_ITEM_GROUPS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingItemListGroup: false
      };

    case BinToBinTransferActionTypes.LOAD_PRODUCT_GROUPS_SUCCESS:
      return {
        ...state,
        productGroups: itemListGroupAdapter.addMany(
          action.payload.itemListGroups,
          state.productGroups
        ),
        productGroupsTotalCount: action.payload.count,
        isLoadingItemListGroup: false
      };

    case BinToBinTransferActionTypes.LOAD_PRODUCT_CATEGORY_SUCCESS:
      return {
        ...state,
        productCategory: itemListGroupAdapter.addMany(
          action.payload.itemListGroups,
          state.productCategory
        ),
        productCategoryTotalCount: action.payload.count,
        isLoadingItemListGroup: false
      };

    case BinToBinTransferActionTypes.LOAD_ITEM_GROUP:
      return {
        ...state,
        isLoadingItemListGroup: true,
        error: null,
        selectedItemListGroup: null,
        isLoadingSelectedItemListGroupSuccess: null
      };

    case BinToBinTransferActionTypes.LOAD_ITEM_GROUP_SUCCESS:
      let isLoaded = false;
      if (action.payload) {
        isLoaded = true;
      }
      return {
        ...state,
        selectedItemListGroup: action.payload,
        isLoadingSelectedItemListGroupSuccess: isLoaded,
        isLoadingItemListGroup: false
      };

    case BinToBinTransferActionTypes.SEARCH_ITEM_GROUPS:
      return {
        ...state,
        isLoadingItemListGroup: true,
        error: null,
        searchedItemListGroups: itemListGroupAdapter.removeAll(
          state.searchedItemListGroups
        ),
        searchedItemListGroupsTotalCount: 0
      };

    case BinToBinTransferActionTypes.SEARCH_ITEM_GROUPS_SUCCESS:
      return {
        ...state,
        searchedItemListGroups: itemListGroupAdapter.addAll(
          action.payload.itemListGroups,
          state.searchedItemListGroups
        ),
        searchedItemListGroupsTotalCount: action.payload.count,
        isLoadingItemListGroup: false
      };

    case BinToBinTransferActionTypes.SEARCH_ITEMS:
      return {
        ...state,
        hasSearchedItems: null,
        isSearchingItems: true,
        searchedItemList: itemAdapter.removeAll(state.searchedItemList),
        error: null
      };

    case BinToBinTransferActionTypes.SEARCH_ITEMS_SUCCESS: {
      return {
        ...state,
        searchedItemList: itemAdapter.addAll(
          action.payload.items,
          state.searchedItemList
        ),
        hasSearchedItems: true,
        isSearchingItems: false
      };
    }

    case BinToBinTransferActionTypes.SEARCH_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        hasSearchedItems: false,
        isSearchingItems: false
      };

    case BinToBinTransferActionTypes.LOAD_ITEMS:
      return {
        ...state,
        isLoadingItems: true,
        isLoadingItemsSuccess: null,
        itemList: itemAdapter.removeAll(state.itemList),
        error: null
      };

    case BinToBinTransferActionTypes.LOAD_ITEMS_SUCCESS: {
      return {
        ...state,
        itemList: itemAdapter.addAll(action.payload.items, state.itemList),
        itemsTotalCount: action.payload.count,
        isLoadingItemsSuccess: true,
        isLoadingItems: false
      };
    }

    case BinToBinTransferActionTypes.LOAD_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingItems: false,
        isLoadingItemsSuccess: false
      };

    case BinToBinTransferActionTypes.ADD_TO_ITEM_LIST:
      return {
        ...state,
        itemList: itemAdapter.addMany(action.payload, state.itemList)
      };

    case BinToBinTransferActionTypes.UPDATE_LIST_ITEM:
      return {
        ...state,
        itemList: itemAdapter.updateOne(
          {
            id: action.payload.id,
            changes: {
              isSelected: action.payload.isSelected,
              destinationBinCode: action.payload.destinationBinCode,
              destinationBinGroupCode: action.payload.destinationBinGroupCode
            }
          },
          state.itemList
        )
      };

    case BinToBinTransferActionTypes.DELETE_FROM_ITEM_LIST:
      return {
        ...state,
        itemList: itemAdapter.removeOne(action.payload, state.itemList)
      };

    case BinToBinTransferActionTypes.CHANGE_SELECTION_OF_ALL_ITEMS:
      return {
        ...state,
        itemList: itemAdapter.updateMany(
          action.payload.idList.map(
            (id): Update<Item> => ({
              id: id,
              changes: {
                isDisabled: action.payload.disable,
                isSelected: action.payload.select
              }
            })
          ),
          state.itemList
        )
      };

    case BinToBinTransferActionTypes.UPDATE_DESTINATION_BIN_FOR_SELECTED_ITEMS:
      return {
        ...state,
        itemList: itemAdapter.updateMany(
          action.payload.idList.map(
            (id): Update<Item> => ({
              id: id,
              changes: {
                destinationBinCode: action.payload.destinationBinCode,
                destinationBinGroupCode: action.payload.destinationBinGroupCode
              }
            })
          ),
          state.itemList
        )
      };

    case BinToBinTransferActionTypes.DELETE_SELECTED_ITEMS:
      return {
        ...state,
        itemList: itemAdapter.removeMany(action.payload, state.itemList)
      };

    case BinToBinTransferActionTypes.CONFIRM_TRANSFER_ITEMS:
    case BinToBinTransferActionTypes.CONFIRM_TRANSFER_ALL_ITEMS:
      return {
        ...state,
        confirmTransferResponse: null,
        error: null
      };

    case BinToBinTransferActionTypes.CONFIRM_TRANSFER_ITEMS_SUCCESS:
      return {
        ...state,
        confirmTransferResponse: action.payload.confirmTransferResponse,
        itemList: itemAdapter.removeMany(
          action.payload.remove ? action.payload.itemId : [],
          state.itemList
        )
      };

    case BinToBinTransferActionTypes.CONFIRM_TRANSFER_ITEMS_FAILURE:
    case BinToBinTransferActionTypes.CONFIRM_TRANSFER_ALL_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload
      };

    case BinToBinTransferActionTypes.CONFIRM_TRANSFER_ALL_ITEMS_SUCCESS:
      return {
        ...state,
        confirmTransferResponse: action.payload
      };

    case BinToBinTransferActionTypes.CLEAR_CONFIRM_TRANSFER_RESPONSE:
      return {
        ...state,
        confirmTransferResponse: null
      };

    case BinToBinTransferActionTypes.CLEAR_SEARCHED_ITEMS:
      return {
        ...state,
        searchedItemList: itemAdapter.removeAll(state.searchedItemList),
        hasSearchedItems: false
      };

    case BinToBinTransferActionTypes.CLEAR_ITEMS:
      return {
        ...state,
        itemList: itemAdapter.removeAll(state.itemList)
      };

    case BinToBinTransferActionTypes.CLEAR_ITEMS_GROUPS:
      return {
        ...state,
        productGroups: itemListGroupAdapter.removeAll(state.productGroups),
        sourceBins: itemListGroupAdapter.removeAll(state.sourceBins),
        productCategory: itemListGroupAdapter.removeAll(state.productCategory),
        searchedItemListGroups: itemListGroupAdapter.removeAll(
          state.searchedItemListGroups
        )
      };

    case BinToBinTransferActionTypes.CLEAR_SELECTED_ITEM_GROUP:
      return {
        ...state,
        selectedItemListGroup: null
      };

    case BinToBinTransferActionTypes.LOAD_BINS:
      return {
        ...state,
        isLoadingBins: true,
        bins: [],
        error: null
      };

    case BinToBinTransferActionTypes.LOAD_BINS_SUCCESS:
      return {
        ...state,
        bins: action.payload,
        isLoadingBins: false
      };

    case BinToBinTransferActionTypes.LOAD_BINS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingBins: false
      };

    case BinToBinTransferActionTypes.LOAD_PRODUCT_GROUP_OPTIONS:
      return {
        ...state,
        isLoadingProductGroupOptions: true,
        error: null
      };

    case BinToBinTransferActionTypes.LOAD_PRODUCT_GROUP_OPTIONS_SUCCESS:
      return {
        ...state,
        productGroupOptions: action.payload,
        isLoadingProductGroupOptions: false
      };

    case BinToBinTransferActionTypes.LOAD_PRODUCT_GROUP_OPTIONS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingProductGroupOptions: false
      };

    case BinToBinTransferActionTypes.LOAD_PRODUCT_CATEGORY_OPTIONS:
      return {
        ...state,
        isLoadingProductCategoryOptions: true,
        error: null
      };

    case BinToBinTransferActionTypes.LOAD_PRODUCT_CATEGORY_OPTIONS_SUCCESS:
      return {
        ...state,
        productCategoryOptions: action.payload,
        isLoadingProductCategoryOptions: false
      };

    case BinToBinTransferActionTypes.LOAD_PRODUCT_CATEGORY_OPTIONS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingProductCategoryOptions: false
      };

    case BinToBinTransferActionTypes.LOAD_SOURCE_BIN_OPTIONS:
      return {
        ...state,
        isLoadingSoruceBinOptionsOptions: true,
        error: null
      };

    case BinToBinTransferActionTypes.LOAD_SOURCE_BIN_OPTIONS_SUCCESS:
      return {
        ...state,
        soruceBinOptions: action.payload,
        isLoadingSoruceBinOptionsOptions: false
      };

    case BinToBinTransferActionTypes.LOAD_SOURCE_BIN_OPTIONS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingSoruceBinOptionsOptions: false
      };

    default:
      return state;
  }
}
