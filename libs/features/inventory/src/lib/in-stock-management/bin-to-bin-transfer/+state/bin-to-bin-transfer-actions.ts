import {
  ConfirmTransferItemsRequest,
  ConfirmTransferResponse,
  ItemListGroup,
  ConfirmTransferAllItemsRequest,
  Item,
  LoadItemGroupsPayload,
  LoadItemListGroupResponse,
  LoadItemsPayload,
  LoadItemsResponse,
  ChangeSelectionPayload,
  UpdateDestinationBinPayload,
  FilterOption
} from '../models/bin-to-bin-transfer.model';
import { Action } from '@ngrx/store';
import {
  CustomErrors,
  StoreBin,
  ProductGroup,
  ProductCategory
} from '@poss-web/core';

export enum BinToBinTransferActionTypes {
  ADD_TO_ITEM_LIST = '[ BinToBinTransfer ] Add to item list Items',
  DELETE_FROM_ITEM_LIST = '[ BinToBinTransfer ] Remove from item list',
  UPDATE_LIST_ITEM = '[ BinToBinTransfer ] Update list item',
  CHANGE_SELECTION_OF_ALL_ITEMS = '[ BinToBinTransfer ] Change selection of all the items',
  UPDATE_DESTINATION_BIN_FOR_SELECTED_ITEMS = '[ BinToBinTransfer ] Update destination bin for selected items',
  DELETE_SELECTED_ITEMS = '[ BinToBinTransfer ] Delete the selected items',

  SEARCH_ITEM_GROUPS = '[ BinToBinTransfer ] Search Item Groups',
  SEARCH_ITEM_GROUPS_SUCCESS = '[ BinToBinTransfer ]  Search Item Groups Success',
  SERACH_ITEM_GROUPS_FAILURE = '[ BinToBinTransfer ]  Search Item Groups Failure',

  LOAD_ITEM_GROUP = '[ BinToBinTransfer ] Load Items Group',
  LOAD_ITEM_GROUP_SUCCESS = '[ BinToBinTransfer ]  Load Item Groups Success',
  LOAD_ITEM_GROUP_FAILURE = '[ BinToBinTransfer ]  Load Item Groups Failure',

  LOAD_SOURCE_BINS = '[ BinToBinTransfer ] Load source bin list',
  LOAD_SOURCE_BINS_SUCCESS = '[ BinToBinTransfer ] Load source bin list Success',
  LOAD_SOURCE_BINS_FAILURE = '[ BinToBinTransfer ] Load source bin list Failure',

  LOAD_PRODUCT_GROUPS = '[ BinToBinTransfer ] Load product groups',
  LOAD_PRODUCT_GROUPS_SUCCESS = '[ BinToBinTransfer ] Load product groups Success',
  LOAD_PRODUCT_GROUPS_FAILURE = '[ BinToBinTransfer ] Load product groups Failure',

  LOAD_PRODUCT_CATEGORY = '[ BinToBinTransfer ] Load product category ',
  LOAD_PRODUCT_CATEGORY_SUCCESS = '[ BinToBinTransfer ] Load product category  Success',
  LOAD_PRODUCT_CATEGORY_FAILURE = '[ BinToBinTransfer ] Load product category  Failure',

  LOAD_ITEMS = '[ BinToBinTransfer ] Load Items ',
  LOAD_ITEMS_SUCCESS = '[ BinToBinTransfer] Load Items Success',
  LOAD_ITEMS_FAILURE = '[ BinToBinTransfer ] Load Items Failure',

  SEARCH_ITEMS = '[ BinToBinTransfer ] Search Items ',
  SEARCH_ITEMS_SUCCESS = '[ BinToBinTransfer] Search Items Success',
  SEARCH_ITEMS_FAILURE = '[ BinToBinTransfer ] Search Items Failure',

  CONFIRM_TRANSFER_ALL_ITEMS = '[ BinToBinTransfer ] Confirm transfer all Items ',
  CONFIRM_TRANSFER_ALL_ITEMS_SUCCESS = '[ BinToBinTransfer] Confirm transfer all Success',
  CONFIRM_TRANSFER_ALL_ITEMS_FAILURE = '[ BinToBinTransfer ] Confirm transfer all Failure',

  CONFIRM_TRANSFER_ITEMS = '[ BinToBinTransfer ] Confirm transfer Items ',
  CONFIRM_TRANSFER_ITEMS_SUCCESS = '[ BinToBinTransfer] Confirm transfer Success',
  CONFIRM_TRANSFER_ITEMS_FAILURE = '[ BinToBinTransfer ] Confirm transfer Failure',

  LOAD_BINS = '[ BinToBinTransfer ] Load all bins ',
  LOAD_BINS_SUCCESS = '[ BinToBinTransfer ] Load all bins Success ',
  LOAD_BINS_FAILURE = '[ BinToBinTransfer ]  Load all bins Failure ',

  LOAD_PRODUCT_GROUP_OPTIONS = '[ BinToBinTransfer ]   Load Product Group options ',
  LOAD_PRODUCT_GROUP_OPTIONS_SUCCESS = '[ BinToBinTransfer ]   Load Product Group options Success ',
  LOAD_PRODUCT_GROUP_OPTIONS_FAILURE = '[ BinToBinTransfer ]   Load Product Group  options Failure ',

  LOAD_PRODUCT_CATEGORY_OPTIONS = '[ BinToBinTransfer ]   Load Product Category options ',
  LOAD_PRODUCT_CATEGORY_OPTIONS_SUCCESS = '[ BinToBinTransfer ]   Load Product Category options Success ',
  LOAD_PRODUCT_CATEGORY_OPTIONS_FAILURE = '[ BinToBinTransfer ]   Load Product Category options Failure ',

  LOAD_SOURCE_BIN_OPTIONS = '[ BinToBinTransfer ]   Load Source Bin options ',
  LOAD_SOURCE_BIN_OPTIONS_SUCCESS = '[ BinToBinTransfer ]   Load Source Bin options Success ',
  LOAD_SOURCE_BIN_OPTIONS_FAILURE = '[ BinToBinTransfer ]   Load Source Bin options Failure ',

  CLEAR_CONFIRM_TRANSFER_RESPONSE = '[ BinToBinTransfer ] Clear Confirm transfer Response',
  CLEAR_SEARCHED_ITEMS = '[ BinToBinTransfer ] Clear Searched Items',
  CLEAR_ITEMS = '[ BinToBinTransfer ] Clear Items',
  CLEAR_ITEMS_GROUPS = '[ BinToBinTransfer ] Clear Items Groups',
  CLEAR_SELECTED_ITEM_GROUP = '[ BinToBinTransfer ] Clear Selected Item Group'
}
export class AddToItemList implements Action {
  readonly type = BinToBinTransferActionTypes.ADD_TO_ITEM_LIST;
  constructor(public payload: Item[]) {}
}

export class UpdateItemList implements Action {
  readonly type = BinToBinTransferActionTypes.UPDATE_LIST_ITEM;
  constructor(public payload: Item) {}
}

export class DeleteFromItemList implements Action {
  readonly type = BinToBinTransferActionTypes.DELETE_FROM_ITEM_LIST;
  constructor(public payload: string) {}
}

export class ChangeSelectionOfAllItems implements Action {
  readonly type = BinToBinTransferActionTypes.CHANGE_SELECTION_OF_ALL_ITEMS;
  constructor(public payload: ChangeSelectionPayload) {}
}

export class UpdateDestinationBinForSelectedItems implements Action {
  readonly type =
    BinToBinTransferActionTypes.UPDATE_DESTINATION_BIN_FOR_SELECTED_ITEMS;
  constructor(public payload: UpdateDestinationBinPayload) {}
}

export class DeleteSelectedItems implements Action {
  readonly type = BinToBinTransferActionTypes.DELETE_SELECTED_ITEMS;
  constructor(public payload: string[]) {}
}
export class LoadSourceBins implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_SOURCE_BINS;
  constructor(public payload: LoadItemGroupsPayload) {}
}

export class LoadSourceBinsSuccess implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_SOURCE_BINS_SUCCESS;
  constructor(public payload: LoadItemListGroupResponse) {}
}

export class LoadSourceBinsFailure implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_SOURCE_BINS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadProductsGroups implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_PRODUCT_GROUPS;
  constructor(public payload: LoadItemGroupsPayload) {}
}

export class LoadProductsGroupsSuccess implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_PRODUCT_GROUPS_SUCCESS;
  constructor(public payload: LoadItemListGroupResponse) {}
}

export class LoadProductsGroupsFailure implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_PRODUCT_GROUPS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadProductsCategory implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_PRODUCT_CATEGORY;
  constructor(public payload: LoadItemGroupsPayload) {}
}

export class LoadProductsCategorySuccess implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_PRODUCT_CATEGORY_SUCCESS;
  constructor(public payload: LoadItemListGroupResponse) {}
}

export class LoadProductsCategoryFailure implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_PRODUCT_CATEGORY_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class SearchItemGroups implements Action {
  readonly type = BinToBinTransferActionTypes.SEARCH_ITEM_GROUPS;
  constructor(public payload: LoadItemGroupsPayload) {}
}

export class SearchItemGroupsSuccess implements Action {
  readonly type = BinToBinTransferActionTypes.SEARCH_ITEM_GROUPS_SUCCESS;
  constructor(public payload: LoadItemListGroupResponse) {}
}

export class SearchItemGroupsFailure implements Action {
  readonly type = BinToBinTransferActionTypes.SERACH_ITEM_GROUPS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadItemGroup implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_ITEM_GROUP;
  constructor(public payload: LoadItemGroupsPayload) {}
}

export class LoadItemGroupSuccess implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_ITEM_GROUP_SUCCESS;
  constructor(public payload: ItemListGroup) {}
}

export class LoadItemGroupFailure implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_ITEM_GROUP_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadItems implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_ITEMS;
  constructor(public payload: LoadItemsPayload) {}
}

export class LoadItemsSuccess implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_ITEMS_SUCCESS;
  constructor(public payload: LoadItemsResponse) {}
}

export class LoadItemsFailure implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_ITEMS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class SearchItems implements Action {
  readonly type = BinToBinTransferActionTypes.SEARCH_ITEMS;
  constructor(public payload: LoadItemsPayload) {}
}

export class SearchItemsSuccess implements Action {
  readonly type = BinToBinTransferActionTypes.SEARCH_ITEMS_SUCCESS;
  constructor(public payload: LoadItemsResponse) {}
}

export class SearchItemsFailure implements Action {
  readonly type = BinToBinTransferActionTypes.SEARCH_ITEMS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class ConfirmTransferAllItems implements Action {
  readonly type = BinToBinTransferActionTypes.CONFIRM_TRANSFER_ALL_ITEMS;
  constructor(public payload: ConfirmTransferAllItemsRequest) {}
}

export class ConfirmTransferAllItemsSuccess implements Action {
  readonly type =
    BinToBinTransferActionTypes.CONFIRM_TRANSFER_ALL_ITEMS_SUCCESS;
  constructor(public payload: ConfirmTransferResponse) {}
}

export class ConfirmTransferAllItemsFailure implements Action {
  readonly type =
    BinToBinTransferActionTypes.CONFIRM_TRANSFER_ALL_ITEMS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class ConfirmTransferItems implements Action {
  readonly type = BinToBinTransferActionTypes.CONFIRM_TRANSFER_ITEMS;
  constructor(public payload: ConfirmTransferItemsRequest) {}
}

export class ConfirmTransferItemsSuccess implements Action {
  readonly type = BinToBinTransferActionTypes.CONFIRM_TRANSFER_ITEMS_SUCCESS;
  constructor(
    public payload: {
      confirmTransferResponse: ConfirmTransferResponse;
      itemId: string[];
      remove: boolean;
    }
  ) {}
}

export class ConfirmTransferItemsFailure implements Action {
  readonly type = BinToBinTransferActionTypes.CONFIRM_TRANSFER_ITEMS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class ClearConfirmTransferResponse implements Action {
  readonly type = BinToBinTransferActionTypes.CLEAR_CONFIRM_TRANSFER_RESPONSE;
}

export class ClearSearchedItems implements Action {
  readonly type = BinToBinTransferActionTypes.CLEAR_SEARCHED_ITEMS;
}

export class ClearItems implements Action {
  readonly type = BinToBinTransferActionTypes.CLEAR_ITEMS;
}

export class ClearItemsGroups implements Action {
  readonly type = BinToBinTransferActionTypes.CLEAR_ITEMS_GROUPS;
}

export class ClearSelectedItemGroup implements Action {
  readonly type = BinToBinTransferActionTypes.CLEAR_SELECTED_ITEM_GROUP;
}

export class LoadBins implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_BINS;
}

export class LoadBinsSuccess implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_BINS_SUCCESS;
  constructor(public payload: StoreBin[]) {}
}
export class LoadBinsFailure implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_BINS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadProductGroupOptions implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_PRODUCT_GROUP_OPTIONS;
}
export class LoadProductGroupOptionsSuccess implements Action {
  readonly type =
    BinToBinTransferActionTypes.LOAD_PRODUCT_GROUP_OPTIONS_SUCCESS;
  constructor(public payload: ProductGroup[]) {}
}
export class LoadProductGroupOptionsFailure implements Action {
  readonly type =
    BinToBinTransferActionTypes.LOAD_PRODUCT_GROUP_OPTIONS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadProductCategoryOptions implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_PRODUCT_CATEGORY_OPTIONS;
}
export class LoadProductCategoryOptionsSuccess implements Action {
  readonly type =
    BinToBinTransferActionTypes.LOAD_PRODUCT_CATEGORY_OPTIONS_SUCCESS;
  constructor(public payload: ProductCategory[]) {}
}
export class LoadProductCategoryOptionsFailure implements Action {
  readonly type =
    BinToBinTransferActionTypes.LOAD_PRODUCT_CATEGORY_OPTIONS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadSourceBinOptions implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_SOURCE_BIN_OPTIONS;
}
export class LoadSourceBinOptionsSuccess implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_SOURCE_BIN_OPTIONS_SUCCESS;
  constructor(public payload: FilterOption[]) {}
}
export class LoadSourceBinOptionsFailure implements Action {
  readonly type = BinToBinTransferActionTypes.LOAD_SOURCE_BIN_OPTIONS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export type BinToBinTransferActions =
  | AddToItemList
  | UpdateItemList
  | DeleteFromItemList
  | ChangeSelectionOfAllItems
  | UpdateDestinationBinForSelectedItems
  | DeleteSelectedItems
  | SearchItems
  | SearchItemsSuccess
  | SearchItemsFailure
  | LoadItems
  | LoadItemsSuccess
  | LoadItemsFailure
  | ConfirmTransferAllItems
  | ConfirmTransferAllItemsSuccess
  | ConfirmTransferAllItemsFailure
  | ConfirmTransferItems
  | ConfirmTransferItemsSuccess
  | ConfirmTransferItemsFailure
  | ClearConfirmTransferResponse
  | LoadItemGroup
  | LoadItemGroupFailure
  | SearchItemGroups
  | SearchItemGroupsSuccess
  | SearchItemGroupsFailure
  | LoadItemGroupSuccess
  | LoadSourceBins
  | LoadSourceBinsSuccess
  | LoadSourceBinsFailure
  | LoadProductsGroups
  | LoadProductsGroupsSuccess
  | LoadProductsGroupsFailure
  | LoadProductsCategory
  | LoadProductsCategorySuccess
  | LoadProductsCategoryFailure
  | ClearSearchedItems
  | ClearItems
  | ClearItemsGroups
  | ClearSelectedItemGroup
  | LoadBins
  | LoadBinsSuccess
  | LoadBinsFailure
  | LoadProductGroupOptions
  | LoadProductGroupOptionsSuccess
  | LoadProductGroupOptionsFailure
  | LoadProductCategoryOptions
  | LoadProductCategoryOptionsSuccess
  | LoadProductCategoryOptionsFailure
  | LoadSourceBinOptions
  | LoadSourceBinOptionsSuccess
  | LoadSourceBinOptionsFailure;
