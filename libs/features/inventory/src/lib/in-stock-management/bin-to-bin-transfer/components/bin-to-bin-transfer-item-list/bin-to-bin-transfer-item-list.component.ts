import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { PageEvent } from '@angular/material';
import { Item } from '../../models/bin-to-bin-transfer.model';
import { Subject } from 'rxjs';
import { AppsettingFacade, StoreBin } from '@poss-web/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'poss-web-bin-to-bin-transfer-item-list',
  templateUrl: './bin-to-bin-transfer-item-list.component.html',
  styleUrls: ['./bin-to-bin-transfer-item-list.component.scss']
})
export class BinToBinTransferItemListComponent implements OnInit, OnDestroy {
  @Input() itemList: Item[] = [];
  @Input() canSelect = false;
  @Input() canDelete = false;
  @Input() showSourceBin = false;
  @Input() showDestinationBin = false;
  @Input() count = 0;
  @Input() pageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };

  @Input() bins: StoreBin[] = [];

  @Output() paginator = new EventEmitter<PageEvent>();
  @Output() delete = new EventEmitter<string>();
  @Output() updateItem = new EventEmitter<Item>();

  pageSizeOptions: number[] = [];
  minPageSize = 0;
  destroy$ = new Subject<null>();

  constructor(private appSettingFacade: AppsettingFacade) {
    this.appSettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.pageSizeOptions = data;
        this.minPageSize = this.pageSizeOptions.reduce((a: number, b: number) =>
          a < b ? a : b
        );
      });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
