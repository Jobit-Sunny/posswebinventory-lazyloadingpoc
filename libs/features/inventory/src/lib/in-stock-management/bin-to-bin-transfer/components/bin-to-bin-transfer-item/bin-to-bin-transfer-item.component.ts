import { Item } from '../../models/bin-to-bin-transfer.model';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { Subject } from 'rxjs';
import { SelectionDialogService, Option } from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { BIN_CODE_AND_GROUP_SEPARATOR } from '../../models/bin-data';
import { StoreBin } from '@poss-web/core';

@Component({
  selector: 'poss-web-bin-to-bin-transfer-item',
  templateUrl: './bin-to-bin-transfer-item.component.html',
  styleUrls: ['./bin-to-bin-transfer-item.component.scss']
})
export class BinToBinTransferItemComponent implements OnInit, OnDestroy {
  @Input() item: Item;
  @Input() canSelect = false;
  @Input() canDelete = false;
  @Input() showSourceBin = false;
  @Input() showDestinationBin = false;
  @Input() bins: StoreBin[] = [];

  selectDestinationBinLableText: string;
  searchDestinationBinLableText: string;
  binsForSelection: Option[] = [];

  @Output() updateItem = new EventEmitter<Item>();
  @Output() delete = new EventEmitter<string>();

  destroy$ = new Subject();

  constructor(
    private selectionDialog: SelectionDialogService,
    private translate: TranslateService
  ) {
    this.translate
      .get([
        'pw.binToBinTransfer.selectDestinationBinLableText',
        'pw.binToBinTransfer.searchDestinationBinLableText'
      ])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: any) => {
        this.selectDestinationBinLableText =
          translatedMessages[
            'pw.binToBinTransfer.selectDestinationBinLableText'
          ];
        this.searchDestinationBinLableText =
          translatedMessages[
            'pw.binToBinTransfer.searchDestinationBinLableText'
          ];
      });
  }

  ngOnInit() {
    this.binsForSelection = this.bins
      .filter(
        bin => bin.binCode.toLowerCase() !== this.item.binCode.toLowerCase()
      )
      .map(bin => ({
        id: bin.binCode + BIN_CODE_AND_GROUP_SEPARATOR + bin.binGroupCode,
        description: bin.binCode
      }));
  }

  sendSelectionChangeEvent(isSelected: boolean): void {
    this.updateItem.emit({ ...this.item, isSelected: isSelected });
  }

  deleteItem(): void {
    this.delete.emit(this.item.id);
  }

  openBinSelectionPopup() {
    this.selectionDialog
      .open({
        title: this.selectDestinationBinLableText,
        placeholder: this.searchDestinationBinLableText,
        options: this.binsForSelection
      })
      .pipe(takeUntil(this.destroy$))
      .subscribe((selectedOption: Option) => {
        if (selectedOption) {
          const selectedBin = this.binSeparator(selectedOption.id);
          this.updateItem.emit({
            ...this.item,
            destinationBinCode: selectedBin.binCode,
            destinationBinGroupCode: selectedBin.binGroupCode
          });
        }
      });
  }

  binSeparator(
    binCodeAndGroup: string
  ): { binCode: string; binGroupCode: string } {
    const data = binCodeAndGroup.split(BIN_CODE_AND_GROUP_SEPARATOR);
    return {
      binCode: data[0],
      binGroupCode: data[1]
    };
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
