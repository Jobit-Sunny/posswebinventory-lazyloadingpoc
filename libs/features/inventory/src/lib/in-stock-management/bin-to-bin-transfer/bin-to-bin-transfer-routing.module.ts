import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BinToBinTransferListComponent } from './containers/bin-to-bin-transfer-list/bin-to-bin-transfer-list.component';
import { BinToBinTransferDetailsComponent } from './containers/bin-to-bin-transfer-details/bin-to-bin-transfer-details.component';
import { AuthGuard } from '@poss-web/core';

const routes: Routes = [
  {
    path: ':type',
    component: BinToBinTransferListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: ':type/:value',
    component: BinToBinTransferDetailsComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BinToBinTransferRoutingModule {}
