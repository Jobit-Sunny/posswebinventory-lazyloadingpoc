import { BinToBinTransferTypeEnum } from './bin-to-bin-transfer.enum';
import { Filter } from '@poss-web/shared';

export const BIN_CODE_AND_GROUP_SEPARATOR = '==++==';

export const SELECT_BY_OPTIONS: { code: string; translationKey: string }[] = [
  {
    code: BinToBinTransferTypeEnum.VARIANT_CODE,
    translationKey: 'pw.binToBinTransfer.variantCodeLable'
  },
  {
    code: BinToBinTransferTypeEnum.BIN_CODE,
    translationKey: 'pw.binToBinTransfer.sourceBinLable'
  },
  {
    code: BinToBinTransferTypeEnum.PRODUCT_GROUP,
    translationKey: 'pw.binToBinTransfer.productGroupLable'
  },
  {
    code: BinToBinTransferTypeEnum.PRODUCT_CATEGORY,
    translationKey: 'pw.binToBinTransfer.productCategoryLable'
  }
];

export const SELECT_BY_KEY_MAP: Map<string, string> = new Map();
SELECT_BY_OPTIONS.forEach(option =>
  SELECT_BY_KEY_MAP.set(option.code, option.translationKey)
);

export const SORT_DATA = [
  {
    id: 0,
    sortByColumnName: 'Item Weight',
    sortAscOrder: false
  },
  {
    id: 1,
    sortByColumnName: 'Item Quantity',
    sortAscOrder: false
  }
];

export const BIN_TYPE = 'BIN_BIN';
