export enum BinToBinTransferTypeEnum {
  VARIANT_CODE = 'variantCode',
  BIN_CODE = 'binCode',
  PRODUCT_GROUP = 'productGroup',
  PRODUCT_CATEGORY = 'productCategory'
}
