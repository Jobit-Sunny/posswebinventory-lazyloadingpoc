import { Moment } from 'moment';

export interface Item {
  id: string;
  currencyCode: string;
  imageURL: string;
  itemCode: string;
  itemDetails: any;
  stdValue: number;
  stdWeight: number;
  lotNumber: string;
  mfgDate: Moment;
  productCategory: string;
  productGroup: string;
  availableQuantity: number;
  availableValue: number;
  availableWeight: number;
  weightUnit: string;
  binCode: string;
  binGroupCode: string;
  isSelected: boolean;
  isDisabled: boolean;
  destinationBinGroupCode: string;
  destinationBinCode: string;
}

export interface FilterOption {
  id: string;
  description: string;
}

export interface ConfirmTransferItemsRequest {
  request: {
    binItems: {
      binCode: string;
      inventoryId: string;
      binGroupCode: string;
    }[];
  };
  remove: boolean;
}

export interface ConfirmTransferAllItemsRequest {
  type: string;
  value: string;
  destinationBinCode: string;
  destinationBinGroupCode: string;
}
export interface ConfirmTransferResponse {
  transferId: number;
}

export interface ItemListGroup {
  name: string;
  products: number;
  totalValue: number;
  totalWeight: number;
  currencyCode: string;
  weightUnit: string;
}

export interface LoadItemGroupsPayload {
  type: string;
  pageIndex?: number;
  pageSize?: number;
  value?: string;
}

export interface LoadItemListGroupResponse {
  count: number;
  itemListGroups: ItemListGroup[];
}

export interface LoadItemsPayload {
  itemCode?: string;
  lotNumber?: string;
  type?: string;
  value?: string;
  pageIndex?: number;
  pageSize?: number;
  sortBy?: string;
  sortOrder?: string;
  filter?: { key: string; value: any[] }[];
}

export interface LoadItemsResponse {
  count: number;
  items: Item[];
}

export interface ChangeSelectionPayload {
  select: boolean;
  disable: boolean;
  idList: string[];
}

export interface UpdateDestinationBinPayload {
  destinationBinGroupCode: string;
  destinationBinCode: string;
  idList: string[];
}
