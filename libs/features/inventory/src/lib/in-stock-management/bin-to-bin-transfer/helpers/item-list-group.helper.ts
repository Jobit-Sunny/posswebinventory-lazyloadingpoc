import {
  ItemListGroup,
  LoadItemListGroupResponse
} from '../models/bin-to-bin-transfer.model';
import { ItemListGroupAdaptor } from '../adaptor/item-list-group.adaptor';

export class ItemListGroupHelper {
  static getInfo(data: any): LoadItemListGroupResponse {
    const itemListGroups: ItemListGroup[] = [];
    for (const group of data.results) {
      itemListGroups.push(ItemListGroupAdaptor.fromJson(group));
    }
    return {
      itemListGroups: itemListGroups,
      count: data.totalElements ? data.totalElements : 0
    };
  }
}
