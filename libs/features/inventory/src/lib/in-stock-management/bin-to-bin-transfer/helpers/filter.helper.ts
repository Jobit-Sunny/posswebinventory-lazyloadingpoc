import { FilterOptionAdaptor } from '../adaptor/filter-option.adaptor';
import { FilterOption } from '../models/bin-to-bin-transfer.model';

export class FilterOptionHelper {
  static getOptions(
    data: any,
    idField: string,
    descriptionField: string = 'description'
  ): FilterOption[] {
    const filters: FilterOption[] = [];
    for (const filter of data) {
      filters.push(
        FilterOptionAdaptor.fromJson(filter, idField, descriptionField)
      );
    }
    return filters;
  }
}
