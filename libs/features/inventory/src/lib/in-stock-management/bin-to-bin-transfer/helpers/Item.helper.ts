import { ItemAdaptor } from '../adaptor/Item.adaptor';
import { Item, LoadItemsResponse } from '../models/bin-to-bin-transfer.model';

export class BinToBinTransferItemHelper {
  static getItems(data: any): LoadItemsResponse {
    const items: Item[] = [];
    for (const item of data.results) {
      items.push(ItemAdaptor.fromJson(item));
    }
    return {
      items: items,
      count: data.totalElements ? data.totalElements : 0
    };
  }
}
