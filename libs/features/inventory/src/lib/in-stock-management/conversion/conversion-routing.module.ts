import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@poss-web/core';
import { ConversionDetailsComponent } from './container/conversion-details/conversion-details.component';
import { ConversionComponent } from './container/conversion/conversion.component';

const routes: Routes = [
  {
    path: ':type',
    component: ConversionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'requests/:srcDocNo',
    component: ConversionDetailsComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConversionRoutingModule {}
