import { Moment } from 'moment';
// ConvertableItem

export interface ConversionItem {
  autoApproved: boolean;
  binCode: string;
  childItems: ConversionItem[];
  complexityCode: string;
  description: string;
  imageURL: string;
  inventoryId: string;
  itemCode: string;
  lotNumber: string;
  productCategory: string;
  productType: string;
  stdValue: number;
  stdWeight: number;
  stoneValue: number;
}
export interface InventoryItem {
  availableQuantity: number;
  availableValue: number;
  availableWeight: number;
  binCode: string;
  binGroupCode: string;
  currencyCode: string;
  id: string;
  imageURL: string;
  itemCode: string;
  itemDetails: any;
  lotNumber: string;
  mfgDate: Moment;
  productCategory: string;
  productGroup: string;
  status: string;
  stdValue: number;
  stdWeight: number;
  weightUnit: string;
}
export interface ConversionRequests {
  id: number;
  srcDocNo: number;
  status: string;
  createdDate: Moment;
  totalQuantity: number;
  totalWeight: number;
  totalValue: number;
}
export interface ConversionRequestItems {
  binCode: string;
  imageURL: string;
  inventoryId: string;
  itemCode: string;
  itemDetails: {
    complexityCode: string;
    itemCode: string;
    itemType: string;
    netWeight: string;
    remarks: string;
    sold: string;
    stonePrice: string;
  };
  lotNumber: string;
  mfgDate: Moment;
  productCategory: string;
  productGroup: string;
  stdValue: number;
  stdWeight: number;
}

export interface ConversionRequestResponse {
  currencyCode: string;
  destDocDate: Moment;
  destDocNo: number;
  destLocationCode: string;
  id: number;
  orderType: string;
  reqDocDate: Moment;
  reqDocNo: number;
  requestType: string;
  srcDocDate: Moment;
  srcDocNo: number;
  srcFiscalYear: number;
  srcLocationCode: string;
  status: string;
  totalAvailableQuantity: number;
  totalAvailableValue: number;
  totalAvailableWeight: number;
  totalMeasuredQuantity: number;
  totalMeasuredValue: number;
  totalMeasuredWeight: number;
  weightUnit: string;
}

export interface ConversionResponse {
  // courierDetails: {};
  currencyCode: string;
  destDocDate: Moment;
  destDocNo: number;
  destLocationCode: string;
  id: number;
  orderType: string;
  srcDocDate: Moment;
  srcDocNo: number;
  srcFiscalYear: number;
  srcLocationCode: string;
  status: string;
  totalQuantity: number;
  totalValue: number;
  totalWeight: number;
  weightUnit: string;
}
