export interface RsoDetails {
  empName: string;
  employeeCode: string;
  locationCode: string;
  orgCode: string;
  primaryRole: string;
  regionCode: string;
  userType: string;
}
