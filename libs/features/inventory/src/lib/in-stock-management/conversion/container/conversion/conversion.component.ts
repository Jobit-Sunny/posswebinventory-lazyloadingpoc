import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  AfterViewInit,
  ElementRef,
  TemplateRef
} from '@angular/core';
import { Subject, Observable, of, fromEvent } from 'rxjs';
import { ConversionFacade } from '../../+state/conversion.facade';
import { takeUntil, subscribeOn, debounceTime } from 'rxjs/operators';
import { CustomErrors, AppsettingFacade } from '@poss-web/core';
import { TranslateService } from '@ngx-translate/core';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  SearchResponse,
  SearchListComponent,
  OverlayNotificationEventType,
  ErrorEnums
} from '@poss-web/shared';
import { Router, ActivatedRoute } from '@angular/router';
import {
  ConversionRequests,
  InventoryItem
} from '../../models/conversion.model';
import { MatDialog } from '@angular/material/dialog';
import { ConversionRequestPopupComponent } from '../../components/conversion-request-popup/conversion-request-popup.component';
import {
  SplitItemPayload,
  ItemPayload,
  RsoDetailsPayload
} from '../../+state/conversion.actions';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { commonTranslateKeyMap } from '../../../../common.map';

export enum InStockConversionTypesEnum {
  SEARCH = 'search',
  REQUEST = 'requests'
}
@Component({
  selector: 'poss-web-conversion',
  templateUrl: './conversion.component.html',
  styleUrls: ['./conversion.component.scss']
})
export class ConversionComponent implements OnInit, OnDestroy, AfterViewInit {
  storeType: string;
  tab: InStockConversionTypesEnum;
  id: number;
  pageSize = 4;

  searchedItemsList: InventoryItem[] = [];
  isSearchingItems$: Observable<boolean>;
  hasSearchedItems$: Observable<boolean>;

  itemList$: Observable<InventoryItem[]>;
  hasItem$: Observable<boolean>;

  selectedItem: any;
  showChildItems = false;
  parentItem: InventoryItem;

  isLoadingCount$: Observable<boolean>;

  conversionRequestsCount = 0;
  conversionRequests$: Observable<ConversionRequests[]>;
  isLoadingConversionRequests$: Observable<boolean>;
  isConversionReqLoadedOnce = false;

  isSearchingRequests$: Observable<boolean>;
  hasSearchedRequests$: Observable<boolean>;
  searchedRequestResult$: Observable<ConversionRequests[]>;

  isLoadingConversionItems$: Observable<boolean>;
  rsoDetails$: Observable<RsoDetailsPayload>;
  isLoadingRsoDetails$: Observable<boolean>;

  childItemsWeight = 0;
  hasConversionItemsLoaded = false;

  status: string;
  statusColor: string;
  conversionDocNo = 0;

  @ViewChild(SearchListComponent, { static: true })
  searchListRef: SearchListComponent;

  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;
  searchForm: FormGroup;

  destroy$: Subject<null> = new Subject<null>();

  parentForm: FormArray;
  rsoForm: FormGroup;
  rsoNames: string[] = [];

  sendReqDocNo = 0;

  @ViewChild('confirmSuccessNotificationTemplate', { static: true })
  confirmSuccessNotificationTemplate: TemplateRef<any>;
  @ViewChild('confirmRequestSuccessNotificationTemplate', { static: true })
  confirmRequestSuccessNotificationTemplate: TemplateRef<any>;

  constructor(
    private conversionFacade: ConversionFacade,
    private translate: TranslateService,
    private overlayNotification: OverlayNotificationService,
    private appsettingFacade: AppsettingFacade,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    private formBuilder: FormBuilder
  ) {
    this.parentForm = new FormArray([]);

    this.searchForm = this.formBuilder.group({
      searchValue: []
    });

    this.rsoForm = this.formBuilder.group({
      rsoName: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.tab = InStockConversionTypesEnum.SEARCH;
    this.conversionFacade.resetError();
    this.id = this.activatedRoute.snapshot.params['id'];
    this.appsettingFacade
      .getStoreType()
      .pipe(takeUntil(this.destroy$))
      .subscribe((storeType: string) => {
        if (storeType) {
          this.storeType = storeType;
          this.componentInit();
        }
      });
    // this.conversionFacade.searchConversionRequests(10064);
    this.parentForm.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        this.showNotification();
      });
  }
  showNotification() {
    this.overlayNotification.close();
    if (this.hasConversionItemsLoaded) {
      this.showChildItems = true;
      if (this.selectedItem.autoApproved && this.parentForm.valid) {
        this.showChildItems = true;
        this.convertNowOverlay();
      } else if (!this.selectedItem.autoApproved) {
        // this.overlayNotification.close();
        this.showChildItems = false;
        this.sendRequestOverlay();
      } else {
        // this.showNotification();
      }
    }
  }
  ngAfterViewInit() {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe((event: any) => {
        const searchValue = this.searchForm.get('searchValue').value;
        if (searchValue !== '' && !isNaN(searchValue)) {
          this.searchRequests(searchValue);
        } else if (searchValue === '') {
          this.clearSearch();
        } else if (isNaN(searchValue)) {
          this.searchForm.get('searchValue').setErrors({
            nan: 'Only Numbers'
          });
          this.searchForm.get('searchValue').markAsTouched();
        }
      });
  }

  isL1L2Store(): boolean {
    return this.storeType === 'L1' || this.storeType === 'L2';
  }

  isL3Store(): boolean {
    return this.storeType === 'L3';
  }

  componentInit() {
    this.conversionFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });
    const type = this.activatedRoute.snapshot.params['type'];

    this.isLoadingCount$ = this.conversionFacade.getIsLoadingConversionRequestCount();
    this.isSearchingItems$ = this.conversionFacade.getIsSearchingItems();
    this.hasSearchedItems$ = this.conversionFacade.getHasSearchedItems();
    this.isLoadingConversionItems$ = this.conversionFacade.getisLoadingConversionItems();
    this.isLoadingConversionRequests$ = this.conversionFacade.getIsLoadingConversionRequests();
    this.hasItem$ = this.conversionFacade.getHasSelectedVarient();

    this.isSearchingRequests$ = this.conversionFacade.getIsSearchingRequests();
    this.hasSearchedRequests$ = this.conversionFacade.getHasSearchedRequests();
    this.searchedRequestResult$ = this.conversionFacade.getSearchedRequests();
    // this.rsoDetails$ = this.conversionFacade.getRsoDetails();
    this.conversionFacade.loadRsoDetails();
    this.isLoadingRsoDetails$ = this.conversionFacade.getIsLoadingRsoDetails();
    this.conversionFacade
      .getItemSplitResponse()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        if (data) {
          this.conversionDocNo = data.srcDocNo;
        }
      });
    // this.searchedRequestResult$.subscribe(data => {});
    this.conversionFacade
      .getSearchedItemsList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        this.searchedItemsList = data;
      });
    this.conversionFacade
      .getSelectedVarient()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        this.parentItem = data[0];
      });

    this.conversionFacade
      .getConversionItems()
      .pipe(takeUntil(this.destroy$))
      .subscribe((response: any) => {
        if (response != null) {
          this.selectedItem = response;
        }
      });
    this.conversionFacade
      .getConversionRequestResponse()
      .pipe(takeUntil(this.destroy$))
      .subscribe((response: any) => {
        if (response) {
          this.sendReqDocNo = response.srcDocNo;
          this.showConversionRequestSuccessNotification(response.srcDocNo);
        }
      });
    this.conversionFacade
      .getConversionRequestCount()
      .pipe(takeUntil(this.destroy$))
      .subscribe((count: any) => {
        this.conversionRequestsCount = count;
      });
    this.conversionFacade
      .getHasRsoDetails()
      .pipe(takeUntil(this.destroy$))
      .subscribe((hasData: any) => {
        if (hasData) {
          this.conversionFacade
            .getRsoDetails()
            .pipe(takeUntil(this.destroy$))
            .subscribe((details: any) => {
              this.rsoNames = details.empName;
            });
        }
      });
    this.clearAll();
    this.getRequests();
    this.changeType(type);
  }

  getRequests() {
    this.conversionFacade.loadConversionReqCount();

    this.conversionRequests$ = this.conversionFacade.getConversionRequests();
    this.conversionRequests$
      .pipe(takeUntil(this.destroy$))
      .subscribe((requests: ConversionRequests[]) => {
        if (
          requests &&
          requests.length !== 0 &&
          !this.isConversionReqLoadedOnce
        ) {
          this.isConversionReqLoadedOnce = true;
        }
      });
  }

  changeType(newType: any) {
    if (this.tab !== newType) {
      this.overlayNotification.close();
      this.router.navigate(['..', newType], {
        relativeTo: this.activatedRoute
      });
      this.tab = newType;
      if (this.tab === InStockConversionTypesEnum.REQUEST) {
        this.loadRequests(0);
      } else {
        this.clearAll();
      }
    }
  }
  search(searchData: SearchResponse) {
    this.showChildItems = false;
    this.clearAll();
    this.conversionFacade.loadSearchedItemsList(searchData.searchValue);
  }
  onSearchItemsSelected(item: InventoryItem) {
    this.conversionFacade.removeFromSelectedVarient();
    this.conversionFacade.addToSelectedVarient(item);
  }
  isSplitAvailable(item: any) {
    this.conversionFacade.loadConversionItems({
      lotNumber: this.parentItem.lotNumber,
      itemCode: this.parentItem.itemCode,
      itemWeight: this.parentItem.stdWeight,
      binCode: this.parentItem.binCode
    });
    this.conversionFacade
      .getHasConverionItemsLoaded()
      .pipe(takeUntil(this.destroy$))
      .subscribe((hasItemsLoaded: boolean) => {
        this.hasConversionItemsLoaded = hasItemsLoaded;
        if (this.hasConversionItemsLoaded) {
          this.showChildItems = true;
        } else {
          this.showChildItems = false;
        }
      });
  }
  loadRequests(pageIndex) {
    if (this.tab === InStockConversionTypesEnum.REQUEST) {
      this.conversionFacade.loadConversionRequests({
        pageIndex: pageIndex,
        pageSize: this.pageSize
      });
    } else {
      //
    }
  }
  onSelected(request: any) {
    // if (request.status === 'APPROVED') {
    this.router.navigate([request.srcDocNo], {
      relativeTo: this.activatedRoute
    });
    // }
  }
  showRequestPopup() {
    this.clearAll();
    const dialogRef = this.dialog.open(ConversionRequestPopupComponent, {
      width: '100vw',
      data: this.selectedItem
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => {
        if (res) {
          this.sendSplitRequest(res);
        }
      });
  }
  sendSplitRequest(payload) {
    this.conversionFacade.sendConversionRequest(payload);
  }
  searchRequests(srcDocNo: number) {
    this.conversionFacade.searchConversionRequests(srcDocNo);
  }

  clearSearch() {
    this.searchForm.reset();
    this.conversionFacade.clearSearchedRequests();
  }
  clearAll() {
    while (this.parentForm.length !== 0) {
      this.parentForm.removeAt(0);
    }
    this.conversionFacade.clearLoadedConversionItems();
    this.conversionFacade.removeFromSelectedVarient();
    this.conversionFacade.clearSearchedItemsList();
    this.rsoForm.patchValue({ rsoName: '' }, { emitEvent: false });
    this.overlayNotification.close();
  }
  createSplitItemPayload(): SplitItemPayload {
    const issueItem: ItemPayload[] = [
      {
        inventoryId: this.selectedItem.inventoryId,
        itemCode: this.selectedItem.itemCode,
        lotNumber: this.selectedItem.lotNumber,
        measuredWeight: this.selectedItem.stdWeight
      }
    ];
    const receiveItems: ItemPayload[] = [];
    for (let i = 0; i < this.parentForm.controls.length; i++) {
      const receiveItem: ItemPayload = {
        inventoryId: this.parentForm.controls[i].value.inventoryId,
        itemCode: this.parentForm.controls[i].value.itemCode,
        lotNumber: this.parentForm.controls[i].value.lotNumber,
        measuredWeight: this.parentForm.controls[i].value.weight
      };
      receiveItems.push(receiveItem);
    }
    const splitItemPayload: SplitItemPayload = {
      issueItems: issueItem,
      receiveItems: receiveItems,
      rsoName: this.rsoForm.get('rsoName').value
    };
    return splitItemPayload;
  }
  /**
   * gets the status of the request and determines the color code and status description
   * @param status : status of the request
   */
  getStatus(status?: string) {
    let key = {
      status: '',
      statusColor: ''
    };
    if (commonTranslateKeyMap.has(status)) {
      key = commonTranslateKeyMap.get(status);
    }
    this.translate
      .get([key.status, key.statusColor])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: string) => {
        this.status = translatedMessages[key.status];
        this.statusColor = translatedMessages[key.statusColor];
      });
    return this.statusColor;
  }
  convertNowOverlay() {
    this.translate
      .get('pw.stockIssueNotificationMessages.confirmOverlayMessage')
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            buttonText: 'CONVERT NOW',
            message:
              'Split option is available for this parent product. Select RSO name and Click on "Convert Now"'
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.rsoForm.get('rsoName').markAsTouched();
              if (this.rsoForm.valid) {
                this.showProgressNotification();
                this.conversionFacade.splitItem(this.createSplitItemPayload());
                this.conversionFacade
                  .getItemSplitResponse()
                  .pipe(takeUntil(this.destroy$))
                  .subscribe((response: any) => {
                    this.overlayNotification.close();
                    if (response) {
                      this.conversionSuccessMsgOverlay();
                    }
                    // else {
                    // }
                  });
              } else {
                this.showNotification();
              }
            }
          });
      });
  }
  showConversionRequestSuccessNotification(documentNumber: number) {
    // this.hasNotification = true;
    this.overlayNotification
      .show({
        type: OverlayNotificationType.CUSTOM,
        message: '',
        hasBackdrop: true,
        hasClose: true,
        template: this.confirmRequestSuccessNotificationTemplate
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        // this.hasNotification = false;
        this.reloadComponent(event);
      });
  }

  showProgressNotification() {
    const key = 'pw.stockReceiveNotificationMessages.progressMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        // this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.PROGRESS,
            message: translatedMsg,
            hasBackdrop: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            // this.hasNotification = false;
          });
      });
  }
  conversionSuccessMsgOverlay() {
    this.clearAll();
    // this.hasNotification = true;
    this.overlayNotification
      .show({
        type: OverlayNotificationType.CUSTOM,
        hasClose: true,
        hasBackdrop: true,
        message: '',
        template: this.confirmSuccessNotificationTemplate
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        // this.hasNotification = false;
        this.reloadComponent(event);
      });
  }

  reloadComponent(event: any) {
    if (event.eventType === OverlayNotificationEventType.CLOSE) {
      this.componentInit();
    }
  }
  // conversionErrorMsgOverlay() {
  //   // this.hasNotification = true;
  //   this.overlayNotification
  //     .show({
  //       type: OverlayNotificationType.SIMPLE,
  //       hasClose: true,
  //       hasBackdrop: true,
  //       message: 'Your products are not converted'
  //     })
  //     .events.pipe(takeUntil(this.destroy$))
  //     .subscribe((event: OverlayNotificationEventRef) => {
  //       // this.hasNotification = false;
  //       if (event.eventType === OverlayNotificationEventType.CLOSE) {
  //         this.componentInit();
  //       }
  //     });
  // }
  sendRequestOverlay() {
    this.translate
      .get('pw.conversion.sendRequestOverlayMessage')
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            buttonText: 'Send Request',
            message: translatedMsg
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.showRequestPopup();
            }
          });
      });
  }
  errorHandler(error: CustomErrors) {
    this.overlayNotification.close();
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error,
        hasBackdrop:
          error.code === ErrorEnums.ERR_INV_011 ||
          error.code === ErrorEnums.ERR_INV_026
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {});
  }
  ngOnDestroy(): void {
    while (this.parentForm.length !== 0) {
      this.parentForm.removeAt(0);
    }
    this.destroy$.next();
    this.destroy$.complete();
  }
}
