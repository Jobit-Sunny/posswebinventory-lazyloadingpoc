import {
  Component,
  OnInit,
  OnDestroy,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import { takeUntil } from 'rxjs/operators';
import { ConversionFacade } from '../../+state/conversion.facade';
import { ActivatedRoute, Router } from '@angular/router';
import {
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationService,
  OverlayNotificationEventType,
  ErrorEnums
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import {
  ConversionRequests,
  ConversionRequestItems
} from '../../models/conversion.model';
import {
  ItemPayload,
  SplitItemPayload,
  RsoDetailsPayload,
  ConfirmConversionPayload
} from '../../+state/conversion.actions';
import { FormArray, FormGroup, FormBuilder } from '@angular/forms';
import { commonTranslateKeyMap } from '../../../../common.map';

@Component({
  selector: 'poss-web-conversion-details',
  templateUrl: './conversion-details.component.html',
  styleUrls: ['./conversion-details.component.scss']
})
export class ConversionDetailsComponent implements OnInit, OnDestroy {
  storeType: string;
  srcDocNo: number;
  lotNumber: any;
  itemCode: any;
  selectedRequest: ConversionRequests;
  selectedRequestData: ConversionRequestItems[];
  parentItem: ConversionRequestItems = null;
  childItems: ConversionRequestItems[] = [];
  childItemsWeight = 0;
  isLoadingSelectedRequest$: Observable<boolean>;
  isLoadingSelectedRequestData$: Observable<boolean>;
  isLoadingCount$: Observable<boolean>;
  destroy$: Subject<null> = new Subject<null>();
  status: any;
  statusColor: any;
  hasRsoDetails$: Observable<boolean>;
  rsoDetails$: Observable<RsoDetailsPayload>;
  isLoadingRsoDetails$: Observable<boolean>;
  rsoNames: string[] = [];

  parentForm: FormArray;
  rsoForm: FormGroup;

  conversionDocNo = 0;
  @ViewChild('confirmSuccessNotificationTemplate', { static: true })
  confirmSuccessNotificationTemplate: TemplateRef<any>;

  constructor(
    private appSettingFacade: AppsettingFacade,
    private conversionFacade: ConversionFacade,
    private activatedRoute: ActivatedRoute,
    private translate: TranslateService,
    private router: Router,
    private overlayNotification: OverlayNotificationService,
    private formBuilder: FormBuilder
  ) {
    this.parentForm = new FormArray([]);

    this.rsoForm = this.formBuilder.group({
      rsoName: []
    });
  }

  ngOnInit() {
    this.conversionFacade.resetError();
    this.overlayNotification.close();
    this.srcDocNo = this.activatedRoute.snapshot.params['srcDocNo'];
    this.appSettingFacade
      .getStoreType()
      .pipe(takeUntil(this.destroy$))
      .subscribe((storeType: string) => {
        if (storeType) {
          this.storeType = storeType;
          this.componentInit();
        }
      });

    this.parentForm.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        this.showNotification();
      });
  }
  isL1L2Store(): boolean {
    return this.storeType === 'L1' || this.storeType === 'L2';
  }

  isL3Store(): boolean {
    return this.storeType === 'L3';
  }
  componentInit() {
    this.conversionFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });
    this.isLoadingSelectedRequestData$ = this.conversionFacade.getIsLoadingSelectedRequestData();
    this.isLoadingCount$ = this.conversionFacade.getIsLoadingConversionRequestCount();
    this.isLoadingSelectedRequest$ = this.conversionFacade.getIsLoadingSelectedRequest();
    this.conversionFacade.loadSelectedRequest(this.srcDocNo);
    this.isLoadingRsoDetails$ = this.conversionFacade.getIsLoadingRsoDetails();
    this.conversionFacade
      .getSelectedRequest()
      .pipe(takeUntil(this.destroy$))
      .subscribe((response: ConversionRequests[]) => {
        if (response) {
          this.selectedRequest = response[0];
          this.initialLoad();
        } else {
          //todo
        }
      });
    this.conversionFacade
      .getHasRsoDetails()
      .pipe(takeUntil(this.destroy$))
      .subscribe((hasData: any) => {
        if (hasData) {
          this.conversionFacade
            .getRsoDetails()
            .pipe(takeUntil(this.destroy$))
            .subscribe((details: any) => {
              this.rsoNames = details.empName;
            });
        }
      });
    this.conversionFacade
      .getItemSplitResponse()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        if (data) {
          this.conversionDocNo = data.srcDocNo;
        }
      });
  }
  initialLoad() {
    this.conversionFacade.loadSelectedRequestData(this.selectedRequest.id);
    this.conversionFacade.loadRsoDetails();

    this.conversionFacade
      .getSelectedRequestData()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: ConversionRequestItems[]) => {
        if (data) {
          this.childItems = [];
          for (const item of data) {
            if (item.itemDetails.itemType.toUpperCase() === 'PARENT') {
              this.parentItem = item;
            } else {
              this.childItems.push(item);
            }
          }
        }
      });
  }
  showNotification() {
    if (
      this.selectedRequest.status.toUpperCase() === 'APPROVED' &&
      this.parentForm.valid
    ) {
      this.convertNowOverlay();
    } else {
      this.overlayNotification.close();
    }
  }

  convertNowOverlay() {
    this.translate
      .get('pw.stockIssueNotificationMessages.confirmOverlayMessage')
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            buttonText: 'CONVERT NOW',
            message:
              'Split option is available for this parent product. Select RSO name and Click on "Convert Now"'
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.rsoForm.get('rsoName').markAsTouched();
              if (this.rsoForm.valid) {
                this.showProgressNotification();
                this.conversionFacade.confirmConversion(
                  this.createSplitPayload()
                );
                this.conversionFacade
                  .getItemSplitResponse()
                  .pipe(takeUntil(this.destroy$))
                  .subscribe((response: any) => {
                    this.overlayNotification.close();
                    if (response) {
                      this.conversionSuccessMsgOverlay();
                    }
                    // else {
                    // TODO
                    // this.conversionErrorMsgOverlay();
                    // }
                  });
              } else {
                this.showNotification();
              }
            }
          });
      });
  }
  createSplitPayload(): ConfirmConversionPayload {
    const splitItemPayload: ConfirmConversionPayload = {
      id: this.selectedRequest.id,
      rsoName: this.rsoForm.get('rsoName').value
    };
    return splitItemPayload;
  }
  /**
   * gets the status of the request and determines the color code and status description
   * @param status : status of the request
   */
  getStatus(status?: string) {
    let key = {
      status: '',
      statusColor: ''
    };
    if (commonTranslateKeyMap.has(status)) {
      key = commonTranslateKeyMap.get(status);
    }
    this.translate
      .get([key.status, key.statusColor])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: string) => {
        this.status = translatedMessages[key.status];
        this.statusColor = translatedMessages[key.statusColor];
      });
    return this.statusColor;
  }
  weightMismatchOverlay() {
    this.translate
      .get('pw.stockIssueNotificationMessages.confirmOverlayMessage')
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.SIMPLE,
            hasClose: true,
            message:
              'SORRY, this product is not available for conversion due to weight Mismatch'
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.closeButtonNvaigation(event);
          });
      });
  }
  showProgressNotification() {
    const key = 'pw.stockReceiveNotificationMessages.progressMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        // this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.PROGRESS,
            message: translatedMsg,
            hasBackdrop: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            // this.hasNotification = false;
          });
      });
  }
  conversionSuccessMsgOverlay() {
    // this.hasNotification = true;
    this.overlayNotification
      .show({
        type: OverlayNotificationType.CUSTOM,
        hasClose: true,
        hasBackdrop: true,
        message: '',
        template: this.confirmSuccessNotificationTemplate
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        // this.hasNotification = false;
        this.closeButtonNvaigation(event);
      });
  }
  // conversionErrorMsgOverlay() {
  //   // this.hasNotification = true;
  //   this.overlayNotification
  //     .show({
  //       type: OverlayNotificationType.SIMPLE,
  //       hasClose: true,
  //       hasBackdrop: true,
  //       message: 'Your products are not converted'
  //     })
  //     .events.pipe(takeUntil(this.destroy$))
  //     .subscribe((event: OverlayNotificationEventRef) => {
  //       // this.hasNotification = false;
  //       this.closeButtonNvaigation(event);
  //     });
  // }
  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error,
        hasBackdrop: error.code === ErrorEnums.ERR_INV_029
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        // this.hasNotification = false;
        this.closeButtonNvaigation(event);
      });

    this.showNotification();
  }
  closeButtonNvaigation(event: any) {
    if (event.eventType === OverlayNotificationEventType.CLOSE) {
      this.router.navigate(['..'], {
        relativeTo: this.activatedRoute
      });
    }
  }

  ngOnDestroy() {
    // this.overlayNotification.close();
    while (this.parentForm.length !== 0) {
      this.parentForm.removeAt(0);
    }
    this.destroy$.next();
    this.destroy$.complete();
  }
}
