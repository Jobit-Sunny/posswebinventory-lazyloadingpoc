import {
  ConversionRequests,
  ConversionRequestItems,
  ConversionItem,
  InventoryItem,
  ConversionRequestResponse,
  ConversionResponse
} from '../models/conversion.model';
import * as moment from 'moment';
import { RsoDetailsPayload } from '../+state/conversion.actions';

export class ConversionAdaptor {
  static inventoryItemFromJson(data: any): InventoryItem[] {
    const searchedItems: InventoryItem[] = [];
    for (const searchedItem of data.results) {
      searchedItems.push({
        availableQuantity: searchedItem.availableQuantity,
        availableValue: searchedItem.availableValue,
        availableWeight: searchedItem.availableWeight,
        binCode: searchedItem.binCode,
        binGroupCode: searchedItem.binGroupCode,
        currencyCode: searchedItem.currencyCode,
        id: searchedItem.id,
        imageURL: searchedItem.imageURL,
        itemCode: searchedItem.itemCode,
        itemDetails: searchedItem.itemDetails,
        lotNumber: searchedItem.lotNumber,
        mfgDate: moment(searchedItem.mfgDate),
        productCategory: searchedItem.productCategory,
        productGroup: searchedItem.productGroup,
        status: searchedItem.status,
        stdValue: searchedItem.stdValue,
        stdWeight: searchedItem.stdWeight,
        weightUnit: searchedItem.weightUnit
      });
    }
    return searchedItems;
  }
  static requestsFromJson(data: any): ConversionRequests[] {
    const conversionRequests: ConversionRequests[] = [];
    for (const conversionRequest of data.results) {
      conversionRequests.push({
        id: conversionRequest.id,
        srcDocNo: conversionRequest.srcDocNo,
        status: conversionRequest.status,
        createdDate: moment(conversionRequest.createdDate),
        totalQuantity: conversionRequest.totalQuantity,
        totalWeight: conversionRequest.totalWeight,
        totalValue: conversionRequest.totalValue
      });
    }
    return conversionRequests;
  }

  static SelectedRequestFromJson(data: any): ConversionRequests {
    if (!data) {
      return null;
    }
    const conversionRequest: ConversionRequests = {
      id: data.id,
      srcDocNo: data.srcDocNo,
      status: data.status,
      createdDate: moment(data.createdDate),
      totalQuantity: data.totalQuantity,
      totalWeight: data.totalWeight,
      totalValue: data.totalValue
    };
    return conversionRequest;
  }

  static SelectedRequestDataFromJson(
    conversionRequestItem: any
  ): ConversionRequestItems[] {
    const conversionRequestItems: ConversionRequestItems[] = [];
    for (const conversionRequestData of conversionRequestItem) {
      conversionRequestItems.push({
        binCode: conversionRequestData.binCode,
        inventoryId: conversionRequestData.inventoryId,
        imageURL: conversionRequestData.imageUrl,
        itemCode: conversionRequestData.itemCode,
        itemDetails: {
          complexityCode: conversionRequestData.itemDetails.complexityCode,
          itemCode: conversionRequestData.itemDetails.itemCode,
          itemType: conversionRequestData.itemDetails.itemType,
          netWeight: conversionRequestData.itemDetails.netWeight,
          remarks: conversionRequestData.itemDetails.remarks,
          sold: conversionRequestData.itemDetails.sold,
          stonePrice: conversionRequestData.itemDetails.stonePrice
        },
        lotNumber: conversionRequestData.lotNumber,
        mfgDate: moment(conversionRequestData.mfgDate),
        productCategory: conversionRequestData.productCategory,
        productGroup: conversionRequestData.productGroup,
        stdValue: conversionRequestData.stdValue,
        stdWeight: conversionRequestData.stdWeight
      });
    }
    return conversionRequestItems;
  }

  static ItemsFromJson(data: any): ConversionItem {
    const conversionItems: ConversionItem = {
      autoApproved: data[0].autoApproved,
      binCode: data[0].binCode,
      childItems: data[0].childItems,
      complexityCode: data[0].complexityCode,
      description: data[0].description,
      imageURL: data[0].imageUrl,
      itemCode: data[0].itemCode,
      inventoryId: data[0].inventoryId,
      lotNumber: data[0].lotNumber,
      productCategory: data[0].productCategory,
      productType: data[0].productType,
      stdValue: data[0].stdValue,
      stdWeight: data[0].stdWeight,
      stoneValue: data[0].stoneValue
    };
    return conversionItems;
  }
  static conversionRequestResponseFromJson(data: any) {
    const conversionRequestResponse: ConversionRequestResponse = {
      currencyCode: data.currencyCode,
      destDocDate: moment(data.destDocDate),
      destDocNo: data.destDocNo,
      destLocationCode: data.destLocationCode,
      id: data.id,
      orderType: data.orderType,
      reqDocDate: moment(data.reqDocDate),
      reqDocNo: data.reqDocNo,
      requestType: data.requestType,
      srcDocDate: moment(data.srcDocDate),
      srcDocNo: data.srcDocNo,
      srcFiscalYear: data.srcFiscalYear,
      srcLocationCode: data.srcLocationCode,
      status: data.status,
      totalAvailableQuantity: data.totalAvailableQuantity,
      totalAvailableValue: data.totalAvailableValue,
      totalAvailableWeight: data.totalAvailableWeight,
      totalMeasuredQuantity: data.totalMeasuredQuantity,
      totalMeasuredValue: data.totalMeasuredValue,
      totalMeasuredWeight: data.totalMeasuredWeight,
      weightUnit: data.weightUnit
    };
    return conversionRequestResponse;
  }
  static conversionResponseFromJson(data: any): ConversionResponse {
    const conversionResponse: ConversionResponse = {
      // courierDetails: {},
      currencyCode: data[0].currencyCode,
      destDocDate: moment(data.currencyCode),
      destDocNo: data[0].destDocNo,
      destLocationCode: data[0].destLocationCode,
      id: data.id,
      orderType: data[0].orderType,
      srcDocDate: moment(data.srcDocDate),
      srcDocNo: data[0].srcDocNo,
      srcFiscalYear: data[0].srcFiscalYear,
      srcLocationCode: data[0].srcLocationCode,
      status: data[0].status,
      totalQuantity: data[0].totalQuantity,
      totalValue: data[0].totalValue,
      totalWeight: data[0].totalWeight,
      weightUnit: data[0].weightUnit
    };
    return conversionResponse;
  }
  static rsoDetailsFromJson(data: any): RsoDetailsPayload {
    const empName: string[] = [];
    const employeeCode: string[] = [];
    let rsoDetails: RsoDetailsPayload;
    for (const details of data.results) {
      empName.push(details.empName);
      employeeCode.push(details.employeeCode);
    }
    rsoDetails = {
      empName: empName,
      employeeCode: employeeCode
    };
    return rsoDetails;
  }
}
