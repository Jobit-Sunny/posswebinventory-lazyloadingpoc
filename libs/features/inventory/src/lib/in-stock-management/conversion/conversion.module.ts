import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConversionRoutingModule } from './conversion-routing.module';
import { MatSidenavModule } from '@angular/material';
import { UicomponentsModule, SharedModule } from '@poss-web/shared';
import { EffectsModule } from '@ngrx/effects';
import { ConversionComponent } from './container/conversion/conversion.component';
import { ConversionDetailsComponent } from './container/conversion-details/conversion-details.component';
import { ConversionItemComponent } from './components/conversion-item/conversion-item.component';
import { ConversionRequestPopupComponent } from './components/conversion-request-popup/conversion-request-popup.component';
import { ConversionEffect } from './+state/conversion.effects';
import { ConversionService } from './services/conversion.service';
import { ConversionFacade } from './+state/conversion.facade';

@NgModule({
  declarations: [
    ConversionComponent,
    ConversionDetailsComponent,
    ConversionItemComponent,
    ConversionRequestPopupComponent
  ],
  imports: [
    CommonModule,
    ConversionRoutingModule,
    SharedModule,
    UicomponentsModule,
    MatSidenavModule,
    EffectsModule.forFeature([ConversionEffect])
  ],
  providers: [ConversionFacade, ConversionService],
  entryComponents: [ConversionRequestPopupComponent]
})
export class ConversionModule {}
