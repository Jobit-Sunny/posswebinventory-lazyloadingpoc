import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  FormArray,
  Validators
} from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Subject } from 'rxjs';
import {
  SplitReqPayload,
  SplitReqItemPayload
} from '../../+state/conversion.actions';

@Component({
  selector: 'poss-web-conversion-request-popup',
  templateUrl: './conversion-request-popup.component.html',
  styleUrls: ['./conversion-request-popup.component.scss']
})
export class ConversionRequestPopupComponent implements OnInit, OnDestroy {
  destroy$: Subject<null> = new Subject<null>();

  form: FormGroup;
  childListForm: FormArray;
  items: any[] = [];
  splitReqPayload: SplitReqPayload;

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA)
    public data: any,
    public dialog: MatDialog,
    public fb: FormBuilder
  ) {
    this.form = this.fb.group({
      childItems: this.fb.array([]),
      product: this.data.productCategory,
      inventoryId: this.data.inventoryId,
      itemCode: this.data.itemCode,
      lotNumber: this.data.lotNumber,
      netWeight: this.data.stdWeight,
      stonePrice: this.data.stoneValue,
      complexityCode: this.data.complexityCode,
      sold: 'Y',
      itemType: 'parent',
      remarks: ''
    });
    this.childListForm = this.form.get('childItems') as FormArray;
  }
  ngOnInit() {
    for (const child of this.data.childItems) {
      this.childListForm.push(
        this.fb.group({
          childItems: new FormControl(child.childItems),
          product: new FormControl(child.productCategory),
          inventoryId: new FormControl(child.inventoryId),
          itemCode: new FormControl(child.itemCode),
          lotNumber: new FormControl(child.lotNumber),
          netWeight: new FormControl(
            child.stdWeight,
            Validators.compose([
              Validators.required,
              Validators.min(0.001),
              Validators.pattern('^[0-9]{1,11}(?:.[0-9]{1,3})?$')
            ])
          ),
          stonePrice: new FormControl(child.stoneValue),
          complexityCode: new FormControl(child.complexityCode),
          sold: new FormControl('N'),
          itemType: new FormControl('child')
        })
      );
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }
  sendRequestNow() {
    console.log('form', this.form);

    this.dialogRef.close(this.createPayload());
  }
  createPayload() {
    this.splitReqPayload = null;
    this.items = [];
    const data = this.form.value;
    const itemData: SplitReqItemPayload = {
      inventoryId: data.inventoryId,
      itemCode: data.itemCode,
      itemDetails: {
        data: {
          remarks: data.product,
          itemCode: data.itemCode,
          netWeight: data.netWeight,
          stonePrice: data.stonePrice,
          complexityCode: data.complexityCode,
          sold: data.sold,
          itemType: data.itemType
        },
        type: 'conversion'
      },
      lotNumber: data.lotNumber,
      quantity: 1
    };
    this.items.push(itemData);

    for (const formValue of this.form.value.childItems) {
      const childItemData: SplitReqItemPayload = {
        inventoryId: formValue.inventoryId,
        itemCode: formValue.itemCode,
        itemDetails: {
          data: {
            remarks: formValue.product,
            itemCode: formValue.itemCode,
            netWeight: formValue.netWeight,
            stonePrice: formValue.stonePrice,
            complexityCode: formValue.complexityCode,
            sold: formValue.sold,
            itemType: formValue.itemType
          },
          type: 'conversion'
        },
        lotNumber: data.lotNumber,
        quantity: 1
      };
      this.items.push(childItemData);
    }
    this.splitReqPayload = {
      approvalDetails: {
        data: {},
        type: ''
      },
      items: this.items,
      remarks: this.form.get('remarks').value
    };
    return this.splitReqPayload;
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
