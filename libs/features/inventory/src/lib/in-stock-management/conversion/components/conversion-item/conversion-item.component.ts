import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators } from '@angular/forms';
import { of, Subject } from 'rxjs';
import { takeUntil, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'poss-web-conversion-item',
  templateUrl: './conversion-item.component.html',
  styleUrls: ['./conversion-item.component.scss']
})
export class ConversionItemComponent implements OnInit, OnDestroy {
  @Input() item: any;
  @Input() tab: any;
  @Input() showButton: any = 'false';
  @Input() isSearching = false;

  @Input() parentForm: FormArray;
  @Input() hasForm: any;

  @Output() isSplitAvailable = new EventEmitter<any>();

  itemForm: FormGroup;
  destroy$: Subject<null> = new Subject<null>();
  constructor() {}

  ngOnInit() {
    if (this.hasForm !== 0) {
      this.itemForm = this.createForm(this.item);
      this.parentForm.push(this.itemForm);
    }
  }
  checkIsSplitAvailable() {
    this.isSplitAvailable.emit(this.item);
  }
  createForm(item: any): FormGroup {
    return new FormGroup({
      inventoryId: new FormControl(item.inventoryId),
      itemCode: new FormControl(item.itemCode),
      lotNumber: new FormControl(item.lotNumber),
      quantity: new FormControl('1'),
      weight: new FormControl(
        item.stdWeight,
        Validators.compose([Validators.required, Validators.min(0.001)])
      )
      // bin: new FormControl(item.binGroupCode)
    });
  }

  ngOnDestroy() {
    if (this.hasForm === 1) {
      while (this.parentForm.length !== 0) {
        this.parentForm.removeAt(0);
      }
    }
    this.destroy$.next();
    this.destroy$.complete();
  }
}
