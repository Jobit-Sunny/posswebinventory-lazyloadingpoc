import { ConversionState } from './conversion.state';
import { conversionRequestAdaptor, itemAdapter } from './conversion.entity';
import { ConversionActions, ConversionActionTypes } from './conversion.actions';

export const initialState: ConversionState = {
  searchedItemsList: [],
  isSearchingItems: false,
  hasSearchedItems: null,

  selectedVarient: itemAdapter.getInitialState(),
  hasselectedVarient: false,

  conversionItems: null,
  isLoadingConversionItems: false,
  hasConversionItems: null,

  ItemSplitResponse: null,
  isSplitting: null,
  isSplitted: null,

  conversionRequestResponse: null,
  isSendingRequest: false,
  hasRequestResponse: null,

  conversionRequests: conversionRequestAdaptor.getInitialState(),
  isLoadingRequests: false,

  conversionRequestsCount: 0,
  isLoadingCount: false,

  searchedConversionRequests: conversionRequestAdaptor.getInitialState(),
  isSearchingRequests: false,
  hasSearchedConversionRequests: null,

  selectedRequest: null,
  isLoadingSelectedRequest: false,

  selectedRequestData: [],
  isLoadingSelectedRequestData: false,

  rsoDetails: null,
  isLoadingRsoDetails: false,
  hasRsoDetails: false,
  error: null
};
export function ConversionReducer(
  state: ConversionState = initialState,
  action: ConversionActions
): ConversionState {
  switch (action.type) {
    case ConversionActionTypes.LOAD_SEARCH_VARIENT:
      return {
        ...state,
        isSearchingItems: true,
        hasSearchedItems: null,
        conversionItems: null,
        error: null
      };
    case ConversionActionTypes.LOAD_SEARCH_VARIENT_SUCCESS:
      return {
        ...state,
        searchedItemsList: action.payload,
        isSearchingItems: false,
        hasSearchedItems: true
      };
    case ConversionActionTypes.LOAD_SEARCH_VARIENT_FAILURE:
      return {
        ...state,
        isSearchingItems: false,
        hasSearchedItems: false,
        error: action.payload
      };
    case ConversionActionTypes.CLEAR_VARIENT_SEARCH_LIST:
      return {
        ...state,
        hasSearchedItems: false,
        searchedItemsList: []
      };
    case ConversionActionTypes.ADD_TO_SELECTED_VARIENT:
      return {
        ...state,
        selectedVarient: itemAdapter.addOne(
          action.payload,
          state.selectedVarient
        ),
        hasselectedVarient: true
      };
    case ConversionActionTypes.REMOVE_FROM_SELECTED_VARIENT:
      return {
        ...state,
        selectedVarient: itemAdapter.removeAll(state.selectedVarient),
        hasselectedVarient: false
      };
    case ConversionActionTypes.LOAD_CONVERSION_ITEMS:
      return {
        ...state,
        isLoadingConversionItems: true,
        conversionItems: null,
        error: null
      };
    case ConversionActionTypes.LOAD_CONVERSION_ITEMS_SUCCESS:
      return {
        ...state,
        isLoadingConversionItems: false,
        hasConversionItems: true,
        conversionItems: action.payload
      };
    case ConversionActionTypes.LOAD_CONVERSION_ITEMS_FAILURE:
      return {
        ...state,
        isLoadingConversionItems: false,
        hasConversionItems: false,
        error: action.payload
      };
    case ConversionActionTypes.CLEAR_LOADED_CONVERSION_ITEMS:
      return {
        ...state,
        hasConversionItems: false,
        conversionRequests: conversionRequestAdaptor.getInitialState()
      };
    case ConversionActionTypes.SPLIT_ITEM:
    case ConversionActionTypes.CONFIRM_CONVERSION:
      return {
        ...state,
        ItemSplitResponse: null,
        isSplitting: true,
        isSplitted: null,
        error: null
      };
    case ConversionActionTypes.SPLIT_ITEM_SUCCESS:
    case ConversionActionTypes.CONFIRM_CONVERSION_SUCCESS:
      return {
        ...state,
        ItemSplitResponse: action.payload,
        isSplitting: false,
        isSplitted: true
      };
    case ConversionActionTypes.SPLIT_ITEM_FAILURE:
    case ConversionActionTypes.CONFIRM_CONVERSION_FAILURE:
      return {
        ...state,
        isSplitting: false,
        isSplitted: false,
        error: action.payload
      };
    case ConversionActionTypes.SEND_CONVERSION_REQUEST:
      return {
        ...state,
        error: null,
        isSendingRequest: true
      };
    case ConversionActionTypes.SEND_CONVERSION_REQUEST_SUCCESS:
      return {
        ...state,
        isSendingRequest: false,
        conversionRequestResponse: action.payload,
        hasRequestResponse: true
      };
    case ConversionActionTypes.SEND_CONVERSION_REQUEST_FAILURE:
      return {
        ...state,
        error: action.payload,
        isSendingRequest: false,
        hasRequestResponse: false
      };

    case ConversionActionTypes.LOAD_REQUESTS_COUNT:
      return {
        ...state,
        isLoadingCount: true,
        error: null
      };
    case ConversionActionTypes.LOAD_REQUESTS_COUNT_SUCCESS:
      return {
        ...state,
        conversionRequestsCount: action.payload,
        isLoadingCount: false
      };
    case ConversionActionTypes.LOAD_REQUESTS_COUNT_FAILURE:
      return {
        ...state,
        error: action.payload
      };
    case ConversionActionTypes.LOAD_CONVERSION_REQUESTS:
      return {
        ...state,
        isLoadingRequests: true,
        error: null
      };
    case ConversionActionTypes.LOAD_CONVERSION_REQUESTS_SUCCESS:
      return {
        ...state,
        isLoadingRequests: false,
        conversionRequests: conversionRequestAdaptor.addMany(
          action.payload,
          state.conversionRequests
        )
      };
    case ConversionActionTypes.LOAD_CONVERSION_REQUESTS_FAILURE:
      return {
        ...state,
        isLoadingRequests: false,
        error: action.payload
      };
    case ConversionActionTypes.SEARCH_CONVERSION_REQUESTS:
      return {
        ...state,
        isSearchingRequests: true,
        error: null
      };
    case ConversionActionTypes.SEARCH_CONVERSION_REQUESTS_SUCCESS:
      return {
        ...state,
        isSearchingRequests: false,
        hasSearchedConversionRequests: true,
        searchedConversionRequests: conversionRequestAdaptor.addAll(
          action.payload,
          state.searchedConversionRequests
        )
      };
    case ConversionActionTypes.SEARCH_CONVERSION_REQUESTS_FAILURE:
      return {
        ...state,
        isSearchingRequests: false,
        hasSearchedConversionRequests: false,
        error: action.payload
      };
    case ConversionActionTypes.SEARCH_CLEAR:
      return {
        ...state,
        isSearchingRequests: false,
        hasSearchedConversionRequests: null,
        searchedConversionRequests: conversionRequestAdaptor.getInitialState()
      };
    case ConversionActionTypes.LOAD_SELECTED_REQUEST:
      return {
        ...state,
        isLoadingSelectedRequest: true,
        selectedRequest: null,
        error: null
      };
    case ConversionActionTypes.LOAD_SELECTED_REQUEST_SUCCESS:
      return {
        ...state,
        selectedRequest: action.payload,
        isLoadingSelectedRequest: false
      };
    case ConversionActionTypes.LOAD_SELECTED_REQUEST_FAILURE:
      return {
        ...state,
        isLoadingSelectedRequest: false,
        error: action.payload
      };
    case ConversionActionTypes.LOAD_SELECTED_REQUEST_DATA:
      return {
        ...state,
        isLoadingSelectedRequestData: true,
        selectedRequestData: null,
        error: null
      };
    case ConversionActionTypes.LOAD_SELECTED_REQUEST_DATA_SUCCESS:
      return {
        ...state,
        isLoadingSelectedRequestData: false,
        selectedRequestData: action.payload
      };
    case ConversionActionTypes.LOAD_SELECTED_REQUEST_DATA_FAILURE:
      return {
        ...state,
        isLoadingSelectedRequestData: false,
        error: action.payload
      };
    case ConversionActionTypes.LOAD_RSO_DETAILS:
      return {
        ...state,
        isLoadingRsoDetails: true,
        error: null
      };
    case ConversionActionTypes.LOAD_RSO_DETAILS_SUCCESS:
      return {
        ...state,
        rsoDetails: action.payload,
        isLoadingRsoDetails: false,
        hasRsoDetails: true
      };
    case ConversionActionTypes.LOAD_RSO_DETAILS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingRsoDetails: false
      };

    // case ConversionActionTypes.CONFIRM_CONVERSION:
    //   return {
    //     ...state,
    //     ItemSplitResponse: null,
    //     isSplitting: true,
    //     isSplitted: null,
    //     error: null
    //   };
    // case ConversionActionTypes.CONFIRM_CONVERSION_SUCCESS:
    //   return {
    //     ...state,
    //     ItemSplitResponse: action.payload,
    //     isSplitting: false,
    //     isSplitted: true
    //   };
    // case ConversionActionTypes.CONFIRM_CONVERSION_FAILURE:
    //   return {
    //     ...state,
    //     isSplitting: false,
    //     isSplitted: false,
    //     error: action.payload
    //   };

    case ConversionActionTypes.RESET_ERROR:
      return {
        ...state,
        error: null
      };
    default:
      return state;
  }
}
