import { createSelector, select } from '@ngrx/store';
import { selectInventory } from '../../../inventory.state';
import { conversionRequestSelector, itemSelector } from './conversion.entity';

const selectSearchedItemsList = createSelector(
  selectInventory,
  state => state.conversion.searchedItemsList
);
const selectIsSearchingItems = createSelector(
  selectInventory,
  state => state.conversion.isSearchingItems
);
const selectHasSearchedItems = createSelector(
  selectInventory,
  state => state.conversion.hasSearchedItems
);

const selectedVarient = createSelector(
  selectInventory,
  state => state.conversion.selectedVarient
);
const selectSelectedVarient = createSelector(
  selectedVarient,
  itemSelector.selectAll
);
const selectHasSelectedVarient = createSelector(
  selectInventory,
  state => state.conversion.hasselectedVarient
);

const selectisLoadingConversionItems = createSelector(
  selectInventory,
  state => state.conversion.isLoadingConversionItems
);
const selectHasConversionItems = createSelector(
  selectInventory,
  state => state.conversion.hasConversionItems
);
const selectConversionItems = createSelector(
  selectInventory,
  state => state.conversion.conversionItems
);

const selectItemSplitResponse = createSelector(
  selectInventory,
  state => state.conversion.ItemSplitResponse
);
const selectIsSplitting = createSelector(
  selectInventory,
  state => state.conversion.isSplitting
);
const selectIsSplitted = createSelector(
  selectInventory,
  state => state.conversion.isSplitted
);

const selectConversionRequestResponse = createSelector(
  selectInventory,
  state => state.conversion.conversionRequestResponse
);
const selectIsSendingRequest = createSelector(
  selectInventory,
  state => state.conversion.isSendingRequest
);
const selectHasRequestResponse = createSelector(
  selectInventory,
  state => state.conversion.hasRequestResponse
);

const selectConversionRequestCount = createSelector(
  selectInventory,
  state => state.conversion.conversionRequestsCount
);
const selectIsLoadingCount = createSelector(
  selectInventory,
  state => state.conversion.isLoadingCount
);

const conversionRequests = createSelector(
  selectInventory,
  state => state.conversion.conversionRequests
);
const selectConversionRequests = createSelector(
  conversionRequests,
  conversionRequestSelector.selectAll
);
const selectIsLoadingRequests = createSelector(
  selectInventory,
  state => state.conversion.isLoadingRequests
);

const searchedConversionRequests = createSelector(
  selectInventory,
  state => state.conversion.searchedConversionRequests
);
const selectSearchedConversionRequests = createSelector(
  searchedConversionRequests,
  conversionRequestSelector.selectAll
);
const selectIsSearchingRequests = createSelector(
  selectInventory,
  state => state.conversion.isSearchingRequests
);
const selectHasSearchedRequests = createSelector(
  selectInventory,
  state => state.conversion.hasSearchedConversionRequests
);

const selectIsLoadingSelectedRequest = createSelector(
  selectInventory,
  state => state.conversion.isLoadingSelectedRequest
);
const selectSelectedRequest = createSelector(
  selectInventory,
  state => state.conversion.selectedRequest
);

const selectIsLoadingSelectedRequestData = createSelector(
  selectInventory,
  state => state.conversion.isLoadingSelectedRequestData
);
const selectSelectedRequestData = createSelector(
  selectInventory,
  state => state.conversion.selectedRequestData
);

const selectRsoDetails = createSelector(
  selectInventory,
  state => state.conversion.rsoDetails
);
const selectIsLoadingRsoDetails = createSelector(
  selectInventory,
  state => state.conversion.isLoadingRsoDetails
);
const selectHasRsoDetails = createSelector(
  selectInventory,
  state => state.conversion.hasRsoDetails
);
const selectError = createSelector(
  selectInventory,
  state => state.conversion.error
);

export const conversionSelectors = {
  selectSearchedItemsList,
  selectIsSearchingItems,
  selectHasSearchedItems,

  selectSelectedVarient,
  selectHasSelectedVarient,

  selectConversionItems,
  selectHasConversionItems,
  selectisLoadingConversionItems,

  selectItemSplitResponse,
  selectIsSplitting,
  selectIsSplitted,

  selectConversionRequestResponse,
  selectIsSendingRequest,
  selectHasRequestResponse,

  selectIsLoadingCount,
  selectConversionRequestCount,

  selectConversionRequests,
  selectIsLoadingRequests,

  selectSearchedConversionRequests,
  selectIsSearchingRequests,
  selectHasSearchedRequests,

  selectIsLoadingSelectedRequest,
  selectSelectedRequest,

  selectIsLoadingSelectedRequestData,
  selectSelectedRequestData,

  selectRsoDetails,
  selectIsLoadingRsoDetails,
  selectHasRsoDetails,

  selectError
};
