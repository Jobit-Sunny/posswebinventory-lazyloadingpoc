import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { ConversionRequests, InventoryItem } from '../models/conversion.model';

export interface ConverionRequestEntity extends EntityState<ConversionRequests> { }
export const conversionRequestAdaptor = createEntityAdapter<ConversionRequests>({
    selectId: conversionRequest => conversionRequest.id
});
export const conversionRequestSelector = conversionRequestAdaptor.getSelectors();

export interface ItemEntity extends EntityState<InventoryItem> { }
export const itemAdapter = createEntityAdapter<InventoryItem>({
    selectId: item => item.id
});
export const itemSelector = itemAdapter.getSelectors();