import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../../inventory.state';
import { conversionSelectors } from './conversion.selector';
import * as ConversionActions from './conversion.actions';
import { InventoryItem } from '../models/conversion.model';
@Injectable()
export class ConversionFacade {
  private searchedItemsList$ = this.store.select(
    conversionSelectors.selectSearchedItemsList
  );
  private isSearchingItems$ = this.store.select(
    conversionSelectors.selectIsSearchingItems
  );
  private hasSearchedItems$ = this.store.select(
    conversionSelectors.selectHasSearchedItems
  );

  private selectedVarient$ = this.store.select(
    conversionSelectors.selectSelectedVarient
  );
  private hasSelectedVarient$ = this.store.select(
    conversionSelectors.selectHasSelectedVarient
  );

  private isLoadingConversionItems$ = this.store.select(
    conversionSelectors.selectisLoadingConversionItems
  );
  private hasConversionItems$ = this.store.select(
    conversionSelectors.selectHasConversionItems
  );
  private conversionItems$ = this.store.select(
    conversionSelectors.selectConversionItems
  );
  private itemSplitResponse$ = this.store.select(
    conversionSelectors.selectItemSplitResponse
  );
  private isSplitting$ = this.store.select(
    conversionSelectors.selectIsSplitting
  );
  private isSplitted$ = this.store.select(conversionSelectors.selectIsSplitted);
  private hasRequestResponse$ = this.store.select(
    conversionSelectors.selectHasRequestResponse
  );

  private isSendingRequest$ = this.store.select(
    conversionSelectors.selectIsSendingRequest
  );
  private conversionRequestResponse$ = this.store.select(
    conversionSelectors.selectConversionRequestResponse
  );
  private isConversionCountLoading$ = this.store.select(
    conversionSelectors.selectIsLoadingCount
  );
  private conversionRequestCount$ = this.store.select(
    conversionSelectors.selectConversionRequestCount
  );
  private conversionRequests$ = this.store.select(
    conversionSelectors.selectConversionRequests
  );

  private isConversionRequestsLoading$ = this.store.select(
    conversionSelectors.selectIsLoadingRequests
  );

  private isSearchingRequests$ = this.store.select(
    conversionSelectors.selectIsSearchingRequests
  );
  private hasSearchedRequests$ = this.store.select(
    conversionSelectors.selectHasSearchedRequests
  );
  private searchedRequests$ = this.store.select(
    conversionSelectors.selectSearchedConversionRequests
  );

  private isSelectedRequestLoading$ = this.store.select(
    conversionSelectors.selectIsLoadingSelectedRequest
  );
  private selectedRequest$ = this.store.select(
    conversionSelectors.selectSelectedRequest
  );
  private isSelectedRequestDataLoading$ = this.store.select(
    conversionSelectors.selectIsLoadingSelectedRequestData
  );
  private selectedRequestData$ = this.store.select(
    conversionSelectors.selectSelectedRequestData
  );

  private selectRsoDetails$ = this.store.select(
    conversionSelectors.selectRsoDetails
  );
  private selectIsLoadingRsoDetails$ = this.store.select(
    conversionSelectors.selectIsLoadingRsoDetails
  );
  private selectHasRsoDetails$ = this.store.select(
    conversionSelectors.selectHasRsoDetails
  );
  private error$ = this.store.select(conversionSelectors.selectError);

  constructor(private store: Store<State>) {}

  getSearchedItemsList() {
    return this.searchedItemsList$;
  }
  getIsSearchingItems() {
    return this.isSearchingItems$;
  }
  getHasSearchedItems() {
    return this.hasSearchedItems$;
  }

  getSelectedVarient() {
    return this.selectedVarient$;
  }
  getHasSelectedVarient() {
    return this.hasSelectedVarient$;
  }

  getisLoadingConversionItems() {
    return this.isLoadingConversionItems$;
  }
  getHasConverionItemsLoaded() {
    return this.hasConversionItems$;
  }
  getConversionItems() {
    return this.conversionItems$;
  }

  getItemSplitResponse() {
    return this.itemSplitResponse$;
  }
  getIsSplitting() {
    return this.isSplitting$;
  }
  getIsSplitted() {
    return this.isSplitted$;
  }
  getIsSendingRequest() {
    return this.isSendingRequest$;
  }
  getConversionRequestResponse() {
    return this.conversionRequestResponse$;
  }
  getHasRequestResponse() {
    return this.hasRequestResponse$;
  }
  getConversionRequestCount() {
    return this.conversionRequestCount$;
  }
  getIsLoadingConversionRequestCount() {
    return this.isConversionCountLoading$;
  }
  getConversionRequests() {
    return this.conversionRequests$;
  }

  getIsLoadingConversionRequests() {
    return this.isConversionRequestsLoading$;
  }

  getIsSearchingRequests() {
    return this.isSearchingRequests$;
  }
  getHasSearchedRequests() {
    return this.hasSearchedRequests$;
  }
  getSearchedRequests() {
    return this.searchedRequests$;
  }

  getIsLoadingSelectedRequest() {
    return this.isSelectedRequestLoading$;
  }
  getSelectedRequest() {
    return this.selectedRequest$;
  }
  getIsLoadingSelectedRequestData() {
    return this.isSelectedRequestDataLoading$;
  }
  getSelectedRequestData() {
    return this.selectedRequestData$;
  }
  getRsoDetails() {
    return this.selectRsoDetails$;
  }
  getIsLoadingRsoDetails() {
    return this.selectIsLoadingRsoDetails$;
  }
  getHasRsoDetails() {
    return this.selectHasRsoDetails$;
  }

  loadSearchedItemsList(SearchItemsPayload: string) {
    this.store.dispatch(
      new ConversionActions.LoadSearchVarient(SearchItemsPayload)
    );
  }
  clearSearchedItemsList() {
    this.store.dispatch(new ConversionActions.ClearVarientSearchList());
  }
  loadConversionItems(loadItemsPayload: ConversionActions.LoadItemsPayload) {
    this.store.dispatch(
      new ConversionActions.LoadConversionItems(loadItemsPayload)
    );
  }
  clearLoadedConversionItems() {
    this.store.dispatch(new ConversionActions.ClearLoadedConversionItem());
  }
  addToSelectedVarient(item: InventoryItem) {
    this.store.dispatch(new ConversionActions.AddToItemList(item));
  }
  removeFromSelectedVarient() {
    this.store.dispatch(new ConversionActions.RemoveFromItemList());
  }
  splitItem(splitItemPayload: ConversionActions.SplitItemPayload) {
    this.store.dispatch(new ConversionActions.SplitItems(splitItemPayload));
  }
  sendConversionRequest(
    conversionRequestPayload: ConversionActions.SplitReqPayload
  ) {
    this.store.dispatch(
      new ConversionActions.SendConversionRequest(conversionRequestPayload)
    );
  }
  loadConversionReqCount() {
    this.store.dispatch(new ConversionActions.LoadRequestsCount());
  }
  loadConversionRequests(
    loadConversionRequestsPayload: ConversionActions.LoadConversionRequestsPayload
  ) {
    this.store.dispatch(
      new ConversionActions.LoadConversionRequests(
        loadConversionRequestsPayload
      )
    );
  }
  searchConversionRequests(srcDocNo: number) {
    this.store.dispatch(
      new ConversionActions.SearchConversionRequests(srcDocNo)
    );
  }
  clearSearchedRequests() {
    this.store.dispatch(new ConversionActions.ClearSearchRequests());
  }
  loadSelectedRequest(srcDocNo: number) {
    this.store.dispatch(new ConversionActions.LoadSelectedRequest(srcDocNo));
  }
  loadSelectedRequestData(id: number) {
    this.store.dispatch(new ConversionActions.LoadSelectedRequestData(id));
  }
  loadRsoDetails() {
    this.store.dispatch(new ConversionActions.LoadRsoDetails());
  }
  resetError() {
    this.store.dispatch(new ConversionActions.ResetError());
  }
  confirmConversion(
    confirmConversionPayload: ConversionActions.ConfirmConversionPayload
  ) {
    this.store.dispatch(
      new ConversionActions.ConfirmConversion(confirmConversionPayload)
    );
  }
  getError() {
    return this.error$;
  }
}
