import { ConverionRequestEntity, ItemEntity } from './conversion.entity';
import { CustomErrors } from '@poss-web/core';
import {
  ConversionRequests,
  ConversionRequestItems,
  ConversionItem,
  InventoryItem,
  ConversionResponse,
  ConversionRequestResponse
} from '../models/conversion.model';
import { RsoDetails } from '../models/rsoDetails.model';
import { RsoDetailsPayload } from './conversion.actions';

export interface ConversionState {
  searchedItemsList: InventoryItem[];
  isSearchingItems: boolean;
  hasSearchedItems: boolean;

  selectedVarient: ItemEntity;
  hasselectedVarient: boolean;

  conversionItems: ConversionItem;
  isLoadingConversionItems: boolean;
  hasConversionItems: boolean;

  ItemSplitResponse: ConversionResponse;
  isSplitting: boolean;
  isSplitted: boolean;

  conversionRequestResponse: ConversionRequestResponse;
  isSendingRequest: boolean;
  hasRequestResponse: boolean;

  conversionRequests: ConverionRequestEntity;
  isLoadingRequests: boolean;
  conversionRequestsCount: number;
  isLoadingCount: boolean;

  searchedConversionRequests: ConverionRequestEntity;
  isSearchingRequests: boolean;
  hasSearchedConversionRequests: boolean;

  selectedRequest: ConversionRequests[];
  isLoadingSelectedRequest: boolean;

  selectedRequestData: ConversionRequestItems[];
  isLoadingSelectedRequestData: boolean;

  rsoDetails: RsoDetailsPayload;
  isLoadingRsoDetails: boolean;
  hasRsoDetails: boolean;

  error: CustomErrors;
}
