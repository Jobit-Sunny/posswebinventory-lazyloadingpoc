import { Injectable } from '@angular/core';
import {
  NotificationService,
  CustomErrors,
  CustomErrorAdaptor
} from '@poss-web/core';
import { DataPersistence } from '@nrwl/angular';
import { ConversionService } from '../services/conversion.service';
import { Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { ConversionActionTypes } from './conversion.actions';
import {
  ConversionRequests,
  ConversionRequestItems,
  ConversionItem,
  InventoryItem,
  ConversionResponse,
  ConversionRequestResponse
} from '../models/conversion.model';
import { map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import * as ConversionActions from './conversion.actions';
@Injectable()
export class ConversionEffect {
  constructor(
    private conversionService: ConversionService,
    private dataPersistence: DataPersistence<any>,
    private notificationService: NotificationService
  ) {}

  @Effect()
  searchedItems$: Observable<Action> = this.dataPersistence.fetch(
    ConversionActionTypes.LOAD_SEARCH_VARIENT,
    {
      run: (action: ConversionActions.LoadSearchVarient) => {
        return this.conversionService
          .getSearchedItems(action.payload)
          .pipe(
            map(
              (searchedItems: InventoryItem[]) =>
                new ConversionActions.LoadSearchVarientSuccess(searchedItems)
            )
          );
      },
      onError: (
        action: ConversionActions.LoadSearchVarient,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new ConversionActions.LoadSearchVarientFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadConversionRequests$: Observable<Action> = this.dataPersistence.fetch(
    ConversionActionTypes.LOAD_CONVERSION_REQUESTS,
    {
      run: (action: ConversionActions.LoadConversionRequests) => {
        return this.conversionService
          .getRequests(action.payload.pageIndex, action.payload.pageSize)
          .pipe(
            map(
              (conversionRequests: ConversionRequests[]) =>
                new ConversionActions.LoadConversionRequestsSuccess(
                  conversionRequests
                )
            )
          );
      },
      onError: (
        action: ConversionActions.LoadConversionRequests,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new ConversionActions.LoadConversionRequestsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  LoadConversionRequestsCount$ = this.dataPersistence.fetch(
    ConversionActionTypes.LOAD_REQUESTS_COUNT,
    {
      run: (action: ConversionActions.LoadRequestsCount) => {
        return this.conversionService
          .getCount()
          .pipe(
            map(
              (data: number) =>
                new ConversionActions.LoadRequestsCountSuccess(data)
            )
          );
      },
      onError: (
        action: ConversionActions.LoadRequestsCount,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new ConversionActions.LoadRequestsCountFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  searchConversionRequests$ = this.dataPersistence.fetch(
    ConversionActionTypes.SEARCH_CONVERSION_REQUESTS,
    {
      run: (action: ConversionActions.SearchConversionRequests) => {
        return this.conversionService
          .getRequest(action.payload)
          .pipe(
            map(
              (searchResult: ConversionRequests[]) =>
                new ConversionActions.SearchConversionRequestsSuccess(
                  searchResult
                )
            )
          );
      },
      onError: (
        action: ConversionActions.SearchConversionRequests,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new ConversionActions.SearchConversionRequestsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadSelectedRequest$ = this.dataPersistence.fetch(
    ConversionActionTypes.LOAD_SELECTED_REQUEST,
    {
      run: (action: ConversionActions.LoadSelectedRequest) => {
        return this.conversionService
          .getRequest(action.payload)
          .pipe(
            map(
              (conversionRequestData: ConversionRequests[]) =>
                new ConversionActions.LoadSelectedRequestSuccess(
                  conversionRequestData
                )
            )
          );
      },
      onError: (
        action: ConversionActions.LoadSelectedRequest,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new ConversionActions.LoadSelectedRequestFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect()
  loadSelectedRequestData$ = this.dataPersistence.fetch(
    ConversionActionTypes.LOAD_SELECTED_REQUEST_DATA,
    {
      run: (action: ConversionActions.LoadSelectedRequestData) => {
        return this.conversionService
          .getSelectedRequestData(action.payload)
          .pipe(
            map(
              (conversionRequestItemData: ConversionRequestItems[]) =>
                new ConversionActions.LoadSelectedRequestDataSuccess(
                  conversionRequestItemData
                )
            )
          );
      },
      onError: (
        action: ConversionActions.LoadSelectedRequestData,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new ConversionActions.LoadSelectedRequestDataFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect()
  loadSelectedRequestItems$ = this.dataPersistence.fetch(
    ConversionActionTypes.LOAD_CONVERSION_ITEMS,
    {
      run: (action: ConversionActions.LoadConversionItems) => {
        return this.conversionService
          .getConversionItems(action.payload)
          .pipe(
            map(
              (conversionItemData: ConversionItem) =>
                new ConversionActions.LoadConversionItemsSuccess(
                  conversionItemData
                )
            )
          );
      },
      onError: (
        action: ConversionActions.LoadConversionItems,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new ConversionActions.LoadConversionItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  sendConversionRequest$ = this.dataPersistence.fetch(
    ConversionActionTypes.SEND_CONVERSION_REQUEST,
    {
      run: (action: ConversionActions.SendConversionRequest) => {
        return this.conversionService
          .sendConversionsRequest(action.payload)
          .pipe(
            map(
              (conversionReqResponse: ConversionRequestResponse) =>
                new ConversionActions.SendConversionRequestSuccess(
                  conversionReqResponse
                )
            )
          );
      },
      onError: (
        action: ConversionActions.SendConversionRequest,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new ConversionActions.SendConversionRequestFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  splitItems$ = this.dataPersistence.fetch(ConversionActionTypes.SPLIT_ITEM, {
    run: (action: ConversionActions.SplitItems) => {
      return this.conversionService
        .splitItems(action.payload)
        .pipe(
          map(
            (conversionResponse: ConversionResponse) =>
              new ConversionActions.SplitItemsSuccess(conversionResponse)
          )
        );
    },
    onError: (
      action: ConversionActions.SplitItems,
      error: HttpErrorResponse
    ) => {
      const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
      this.notificationService.error(customError);
      return new ConversionActions.SplitItemsFailure(this.errorHandler(error));
    }
  });

  @Effect()
  Conversion$ = this.dataPersistence.fetch(
    ConversionActionTypes.CONFIRM_CONVERSION,
    {
      run: (action: ConversionActions.ConfirmConversion) => {
        return this.conversionService
          .confirmConversion(action.payload)
          .pipe(
            map(
              (conversionResponse: ConversionResponse) =>
                new ConversionActions.ConfirmConversionSuccess(
                  conversionResponse
                )
            )
          );
      },
      onError: (
        action: ConversionActions.ConfirmConversion,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new ConversionActions.ConfirmConversionFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  rsoDetails$ = this.dataPersistence.fetch(
    ConversionActionTypes.LOAD_RSO_DETAILS,
    {
      run: (action: ConversionActions.LoadRsoDetails) => {
        return this.conversionService
          .getRsoDetails()
          .pipe(
            map(
              (rsoDetails: ConversionActions.RsoDetailsPayload) =>
                new ConversionActions.LoadRsoDetailsSuccess(rsoDetails)
            )
          );
      },
      onError: (
        action: ConversionActions.LoadRsoDetails,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new ConversionActions.LoadRsoDetailsFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
}
