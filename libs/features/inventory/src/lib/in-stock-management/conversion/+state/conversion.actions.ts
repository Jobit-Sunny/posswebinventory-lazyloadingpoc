import { Action } from '@ngrx/store';
import {
  ConversionRequests,
  ConversionRequestItems,
  ConversionItem,
  InventoryItem,
  ConversionResponse,
  ConversionRequestResponse
} from '../models/conversion.model';
import { CustomErrors } from '@poss-web/core';
export interface LoadConversionRequestsPayload {
  pageIndex: number;
  pageSize: number;
}
export interface LoadItemsPayload {
  itemCode: string;
  lotNumber: string;
  itemWeight: any;
  binCode: string;
}
export interface ItemPayload {
  inventoryId: string;
  itemCode: string;
  lotNumber: string;
  measuredWeight: number;
}
export interface SplitItemPayload {
  issueItems: ItemPayload[];
  receiveItems: ItemPayload[];
  rsoName: string;
}

//
export interface SplitReqPayload {
  approvalDetails: ApprovalDetailsPayload;
  items: SplitReqItemPayload[];
  remarks: string;
}
export interface ApprovalDetailsPayload {
  data: any;
  type: string;
}
export interface SplitReqItemPayload {
  inventoryId: string;
  itemCode: string;
  itemDetails: SplitItemDetailsPayload;
  lotNumber: string;
  quantity: number;
}
export interface SplitItemDetailsPayload {
  data: SplitItemDetailsDataPayload;
  type: string;
}
export interface SplitItemDetailsDataPayload {
  remarks: string;
  itemCode: string;
  netWeight: number;
  stonePrice: number;
  complexityCode: number;
  sold: string;
  itemType: string;
}
export interface RsoDetailsPayload {
  empName: string[];
  employeeCode: string[];
}
export interface ConfirmConversionPayload {
  id: number;
  rsoName: string;
}

export enum ConversionActionTypes {
  LOAD_SEARCH_VARIENT = '[Conversion] Load Search Varient',
  LOAD_SEARCH_VARIENT_SUCCESS = '[Conversion] Load Search Varient Success',
  LOAD_SEARCH_VARIENT_FAILURE = '[Conversion] Load Search Varient Failure',

  CLEAR_VARIENT_SEARCH_LIST = '[Conversion] Clear Load Search Varient',

  ADD_TO_SELECTED_VARIENT = '[Conversion] Add Selected Varient',
  REMOVE_FROM_SELECTED_VARIENT = '[Conversion] Remove Selected Varient',

  LOAD_CONVERSION_ITEMS = '[Conversion-Details] Load Conversion Items',
  LOAD_CONVERSION_ITEMS_SUCCESS = '[Conversion-Details] Load Conversion Items Success',
  LOAD_CONVERSION_ITEMS_FAILURE = '[Conversion-Details] Load Conversion Items Failure',

  CLEAR_LOADED_CONVERSION_ITEMS = '[Conversion-Details] Clear Load Conversion Items',

  SPLIT_ITEM = '[Conversion] Split Item',
  SPLIT_ITEM_SUCCESS = '[Conversion ] Split Item Success ',
  SPLIT_ITEM_FAILURE = '[COnversion] Split Item Failure',

  SEND_CONVERSION_REQUEST = '[Conversion] Send Conversion Request',
  SEND_CONVERSION_REQUEST_SUCCESS = '[Conversion] Send Conversion Request Success',
  SEND_CONVERSION_REQUEST_FAILURE = '[Conversion] Send Conversion Request Failure',

  LOAD_CONVERSION_REQUESTS = '[Conversion]  Load Conversion Requests',
  LOAD_CONVERSION_REQUESTS_SUCCESS = '[Conversion]  Load Conversion Requests Success',
  LOAD_CONVERSION_REQUESTS_FAILURE = '[Conversion]  Load Conversion Requests Failure',

  LOAD_REQUESTS_COUNT = '[Conversion] Load Requests Count',
  LOAD_REQUESTS_COUNT_SUCCESS = '[Conversion] Load Requests Count Success',
  LOAD_REQUESTS_COUNT_FAILURE = '[Conversion] Load Requests Count Failure',

  SEARCH_CONVERSION_REQUESTS = '[Conversion] Search Conversion Requests',
  SEARCH_CONVERSION_REQUESTS_SUCCESS = '[Conversion] Search Conversion Requests Success',
  SEARCH_CONVERSION_REQUESTS_FAILURE = '[Conversion] Search Conversion Requests Failure',

  SEARCH_CLEAR = '[Conversion] search-clear',

  LOAD_SELECTED_REQUEST = '[Conversion-Details] Load Selected Requests',
  LOAD_SELECTED_REQUEST_SUCCESS = '[Conversion-Details] Load Selected Requests Success',
  LOAD_SELECTED_REQUEST_FAILURE = '[Conversion-Details] Load Selected Requests Failure',

  LOAD_SELECTED_REQUEST_DATA = '[Conversion-Details] Load Selected Requests Data',
  LOAD_SELECTED_REQUEST_DATA_SUCCESS = '[Conversion-Details] Load Selected Requests Data Success',
  LOAD_SELECTED_REQUEST_DATA_FAILURE = '[Conversion-Details] Load Selected Requests Data Failure',

  LOAD_RSO_DETAILS = '[Conversion] Load RSO Details',
  LOAD_RSO_DETAILS_SUCCESS = '[Conversion] Load RSO Details Success',
  LOAD_RSO_DETAILS_FAILURE = '[Conversion] Load RSO Details Failure',

  CONFIRM_CONVERSION = '[Conversion] Confirm conversion after Approval',
  CONFIRM_CONVERSION_SUCCESS = '[Conversion] Confirm conversion after Approval Success',
  CONFIRM_CONVERSION_FAILURE = '[Conversion] Confirm conversion after Approval Failure',

  RESET_ERROR = '[Conversion] Reset Error'
}

export class LoadSearchVarient implements Action {
  readonly type = ConversionActionTypes.LOAD_SEARCH_VARIENT;
  constructor(public payload: string) {}
}
export class LoadSearchVarientSuccess implements Action {
  readonly type = ConversionActionTypes.LOAD_SEARCH_VARIENT_SUCCESS;
  constructor(public payload: InventoryItem[]) {}
}
export class LoadSearchVarientFailure implements Action {
  readonly type = ConversionActionTypes.LOAD_SEARCH_VARIENT_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export class ClearVarientSearchList implements Action {
  readonly type = ConversionActionTypes.CLEAR_VARIENT_SEARCH_LIST;
}

export class AddToItemList implements Action {
  readonly type = ConversionActionTypes.ADD_TO_SELECTED_VARIENT;
  constructor(public payload: InventoryItem) {}
}
export class RemoveFromItemList implements Action {
  readonly type = ConversionActionTypes.REMOVE_FROM_SELECTED_VARIENT;
}

export class LoadConversionItems implements Action {
  readonly type = ConversionActionTypes.LOAD_CONVERSION_ITEMS;
  constructor(public payload: LoadItemsPayload) {}
}
export class LoadConversionItemsSuccess implements Action {
  readonly type = ConversionActionTypes.LOAD_CONVERSION_ITEMS_SUCCESS;
  constructor(public payload: ConversionItem) {}
}
export class LoadConversionItemsFailure implements Action {
  readonly type = ConversionActionTypes.LOAD_CONVERSION_ITEMS_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export class ClearLoadedConversionItem implements Action {
  readonly type = ConversionActionTypes.CLEAR_LOADED_CONVERSION_ITEMS;
}

export class SplitItems implements Action {
  readonly type = ConversionActionTypes.SPLIT_ITEM;
  constructor(public payload: SplitItemPayload) {}
}
export class SplitItemsSuccess implements Action {
  readonly type = ConversionActionTypes.SPLIT_ITEM_SUCCESS;
  constructor(public payload: ConversionResponse) {}
}
export class SplitItemsFailure implements Action {
  readonly type = ConversionActionTypes.SPLIT_ITEM_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class SendConversionRequest implements Action {
  readonly type = ConversionActionTypes.SEND_CONVERSION_REQUEST;
  constructor(public payload: SplitReqPayload) {}
}
export class SendConversionRequestSuccess implements Action {
  readonly type = ConversionActionTypes.SEND_CONVERSION_REQUEST_SUCCESS;
  constructor(public payload: ConversionRequestResponse) {}
}
export class SendConversionRequestFailure implements Action {
  readonly type = ConversionActionTypes.SEND_CONVERSION_REQUEST_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export class LoadRequestsCount implements Action {
  readonly type = ConversionActionTypes.LOAD_REQUESTS_COUNT;
}
export class LoadRequestsCountSuccess implements Action {
  readonly type = ConversionActionTypes.LOAD_REQUESTS_COUNT_SUCCESS;
  constructor(public payload: number) {}
}
export class LoadRequestsCountFailure implements Action {
  readonly type = ConversionActionTypes.LOAD_REQUESTS_COUNT_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export class LoadConversionRequests implements Action {
  readonly type = ConversionActionTypes.LOAD_CONVERSION_REQUESTS;
  constructor(public payload: LoadConversionRequestsPayload) {}
}
export class LoadConversionRequestsSuccess implements Action {
  readonly type = ConversionActionTypes.LOAD_CONVERSION_REQUESTS_SUCCESS;
  constructor(public payload: ConversionRequests[]) {}
}
export class LoadConversionRequestsFailure implements Action {
  readonly type = ConversionActionTypes.LOAD_CONVERSION_REQUESTS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class SearchConversionRequests implements Action {
  readonly type = ConversionActionTypes.SEARCH_CONVERSION_REQUESTS;
  constructor(public payload: number) {}
}
export class SearchConversionRequestsSuccess implements Action {
  readonly type = ConversionActionTypes.SEARCH_CONVERSION_REQUESTS_SUCCESS;
  constructor(public payload: ConversionRequests[]) {}
}
export class SearchConversionRequestsFailure implements Action {
  readonly type = ConversionActionTypes.SEARCH_CONVERSION_REQUESTS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class ClearSearchRequests implements Action {
  readonly type = ConversionActionTypes.SEARCH_CLEAR;
}
export class LoadSelectedRequest implements Action {
  readonly type = ConversionActionTypes.LOAD_SELECTED_REQUEST;
  constructor(public payload: number) {}
}
export class LoadSelectedRequestSuccess implements Action {
  readonly type = ConversionActionTypes.LOAD_SELECTED_REQUEST_SUCCESS;
  constructor(public payload: ConversionRequests[]) {}
}
export class LoadSelectedRequestFailure implements Action {
  readonly type = ConversionActionTypes.LOAD_SELECTED_REQUEST_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadSelectedRequestData implements Action {
  readonly type = ConversionActionTypes.LOAD_SELECTED_REQUEST_DATA;
  constructor(public payload: number) {}
}
export class LoadSelectedRequestDataSuccess implements Action {
  readonly type = ConversionActionTypes.LOAD_SELECTED_REQUEST_DATA_SUCCESS;
  constructor(public payload: ConversionRequestItems[]) {}
}
export class LoadSelectedRequestDataFailure implements Action {
  readonly type = ConversionActionTypes.LOAD_SELECTED_REQUEST_DATA_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadRsoDetails implements Action {
  readonly type = ConversionActionTypes.LOAD_RSO_DETAILS;
}
export class LoadRsoDetailsSuccess implements Action {
  readonly type = ConversionActionTypes.LOAD_RSO_DETAILS_SUCCESS;
  constructor(public payload: RsoDetailsPayload) {}
}
export class LoadRsoDetailsFailure implements Action {
  readonly type = ConversionActionTypes.LOAD_RSO_DETAILS_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export class ConfirmConversion implements Action {
  readonly type = ConversionActionTypes.CONFIRM_CONVERSION;
  constructor(public payload: ConfirmConversionPayload) {}
}
export class ConfirmConversionSuccess implements Action {
  readonly type = ConversionActionTypes.CONFIRM_CONVERSION_SUCCESS;
  constructor(public payload: ConversionResponse) {}
}
export class ConfirmConversionFailure implements Action {
  readonly type = ConversionActionTypes.CONFIRM_CONVERSION_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export class ResetError implements Action {
  readonly type = ConversionActionTypes.RESET_ERROR;
}

export type ConversionActions =
  | LoadSearchVarient
  | LoadSearchVarientSuccess
  | LoadSearchVarientFailure
  | ClearVarientSearchList
  | AddToItemList
  | RemoveFromItemList
  | LoadConversionItems
  | LoadConversionItemsSuccess
  | LoadConversionItemsFailure
  | ClearLoadedConversionItem
  | SplitItems
  | SplitItemsSuccess
  | SplitItemsFailure
  | SendConversionRequest
  | SendConversionRequestSuccess
  | SendConversionRequestFailure
  | LoadRequestsCount
  | LoadRequestsCountSuccess
  | LoadRequestsCountFailure
  | LoadConversionRequests
  | LoadConversionRequestsSuccess
  | LoadConversionRequestsFailure
  | SearchConversionRequests
  | SearchConversionRequestsSuccess
  | SearchConversionRequestsFailure
  | ClearSearchRequests
  | LoadSelectedRequest
  | LoadSelectedRequestSuccess
  | LoadSelectedRequestFailure
  | LoadSelectedRequestData
  | LoadSelectedRequestDataSuccess
  | LoadSelectedRequestDataFailure
  | LoadRsoDetails
  | LoadRsoDetailsSuccess
  | LoadRsoDetailsFailure
  | ConfirmConversion
  | ConfirmConversionSuccess
  | ConfirmConversionFailure
  | ResetError;
