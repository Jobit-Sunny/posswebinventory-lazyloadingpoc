import { Injectable } from '@angular/core';
import { ApiService } from '@poss-web/core';
import {
  ConversionRequests,
  ConversionRequestItems,
  ConversionItem,
  InventoryItem,
  ConversionRequestResponse,
  ConversionResponse
} from '../models/conversion.model';
import { map } from 'rxjs/operators';
import {
  getConversionReqCountEndpointUrl,
  getConversionRequestDataByIdEnpointUrl,
  getConversionItemsEndpointUrl,
  getConversionSearchItemsEndpointUrl,
  getConversionSplitItemEndpointUrl,
  getConversionRequestEndpointUrl,
  getConversionRequestsEndpointUrl,
  getConversionRequestsBySrcDocNoUrl,
  getRsoDetailsEndpointUrl,
  getConfirmConversionEndpointUrl
} from '../../../endpoints.constants';
import { ConversionAdaptor } from '../adaptor/conversion.adaptor';
import { Observable } from 'rxjs';
import {
  LoadItemsPayload,
  SplitItemPayload,
  SplitReqPayload,
  ConfirmConversionPayload
} from '../+state/conversion.actions';

@Injectable()
export class ConversionService {
  constructor(private apiService: ApiService) {}

  getSearchedItems(itemCode: string): Observable<InventoryItem[]> {
    const url = getConversionSearchItemsEndpointUrl(itemCode);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => ConversionAdaptor.inventoryItemFromJson(data)));
  }
  getRequests(
    pageIndex: number,
    pageSize: number
  ): Observable<ConversionRequests[]> {
    const url = getConversionRequestsEndpointUrl(pageIndex, pageSize);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => ConversionAdaptor.requestsFromJson(data)));
  }

  getCount(): Observable<number> {
    const url = getConversionReqCountEndpointUrl();
    return this.apiService
      .get(url)
      .pipe(map((data: any) => data.totalElements));
  }

  getRequest(srcDocNo: any): Observable<ConversionRequests[]> {
    const url = getConversionRequestsBySrcDocNoUrl(srcDocNo);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => ConversionAdaptor.requestsFromJson(data)));
  }
  getSelectedRequestData(id: any): Observable<ConversionRequestItems[]> {
    const url = getConversionRequestDataByIdEnpointUrl(id);
    return this.apiService.get(url).pipe(
      map((data: any) =>
        // ConversionHelper.getSelectedRequestData(data.results))
        ConversionAdaptor.SelectedRequestDataFromJson(data.results)
      )
    );
  }
  getConversionItems(
    loadItemsPayload: LoadItemsPayload
  ): Observable<ConversionItem> {
    const url = getConversionItemsEndpointUrl(loadItemsPayload);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => ConversionAdaptor.ItemsFromJson(data)));
  }

  sendConversionsRequest(
    conversionRequest: SplitReqPayload
  ): Observable<ConversionRequestResponse> {
    const url = getConversionRequestEndpointUrl();
    return this.apiService
      .post(url, conversionRequest)
      .pipe(
        map((data: any) =>
          ConversionAdaptor.conversionRequestResponseFromJson(data)
        )
      );
  }

  splitItems(
    spiltItemPayload: SplitItemPayload
  ): Observable<ConversionResponse> {
    const url = getConversionSplitItemEndpointUrl();
    return this.apiService
      .post(url, spiltItemPayload)
      .pipe(
        map((data: any) =>
          ConversionAdaptor.conversionResponseFromJson(
            data.stockTransactionDetails
          )
        )
      );
  }
  confirmConversion(
    payload: ConfirmConversionPayload
  ): Observable<ConversionResponse> {
    console.log('11 service');

    const url = getConfirmConversionEndpointUrl(payload.id);
    return this.apiService
      .patch(url, { rsoName: payload.rsoName })
      .pipe(
        map((data: any) =>
          ConversionAdaptor.conversionResponseFromJson(
            data.stockTransactionDetails
          )
        )
      );
  }

  getRsoDetails() {
    const url = getRsoDetailsEndpointUrl();
    return this.apiService
      .get(url)
      .pipe(map((data: any) => ConversionAdaptor.rsoDetailsFromJson(data)));
  }
}
