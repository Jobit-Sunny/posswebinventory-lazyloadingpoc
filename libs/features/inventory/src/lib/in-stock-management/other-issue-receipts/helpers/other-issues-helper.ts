import { OtherIssuedataModel, OtherIssueModel } from '../models/other-issues.model';
import { OtherIssuesAdaptor } from '../adaptors/other-issues.adaptor';

export class OtherIssuesDataHelper {
  static getOtherisssuesData(data: any): OtherIssuedataModel {
    const otherIssueData: OtherIssuedataModel = new OtherIssuedataModel();
    otherIssueData.totalElements = data.totalElements;
    for (const OtherIssue of data.results) {
      otherIssueData.issueData.push(
        OtherIssuesAdaptor.OtherIssueDatafromJson(OtherIssue)
      );
    }
    return otherIssueData;
  }
  static getOtherisssuesSearchData(data: any): OtherIssueModel[] {
    const otherIssueSearchData: OtherIssueModel[] = [];
    for (const OtherIssue of data) {
      otherIssueSearchData.push(
        OtherIssuesAdaptor.OtherIssueDatafromJson(OtherIssue)
      );
    }
    return otherIssueSearchData;
  }

}
