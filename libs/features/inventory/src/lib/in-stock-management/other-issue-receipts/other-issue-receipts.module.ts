import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OtherIssueReceiptsRoutingModule } from './other-issue-receipts-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    OtherIssueReceiptsRoutingModule
  ]
})
export class OtherIssueReceiptsModule { }
