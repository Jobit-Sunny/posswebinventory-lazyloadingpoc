import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { OtherIssueModel, OtherIssuesItem } from '../models/other-issues.model';


export interface OtherIssueEntity extends EntityState<OtherIssueModel> { }
export const OtherIssueAdapter = createEntityAdapter<OtherIssueModel>({
selectId: otherIssueData => otherIssueData.id
});
export const otheIssueSelector = OtherIssueAdapter.getSelectors();

export interface OtherIssueItemEntity extends EntityState<OtherIssuesItem> { }
export const otherIssuesItemAdapter = createEntityAdapter<OtherIssuesItem>({
  selectId: item => item.id
});
export const otherIssueItemSelector = otherIssuesItemAdapter.getSelectors();


export interface OtherIssueCreateItemEntity extends EntityState<OtherIssuesItem> { }
export const otherIssuesCreateItemAdapter = createEntityAdapter<OtherIssuesItem>({
  selectId: item => item.inventoryId
});
export const otherIssueCreateItemSelector = otherIssuesCreateItemAdapter.getSelectors();

