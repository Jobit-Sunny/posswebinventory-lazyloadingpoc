import { selectInventory } from '../../../inventory.state';
import { createSelector } from '@ngrx/store';
import {
  otheIssueSelector,
  otherIssueItemSelector,
  otherIssueCreateItemSelector
} from './other-issues.entity';

const selectHasError = createSelector(
  selectInventory,
  state => state.otherIssues.hasError
);

const selectIsLoading = createSelector(
  selectInventory,
  state => state.otherIssues.isLoading
);

const selectOtherIssuesSTNCount = createSelector(
  selectInventory,
  state => state.otherIssues.pendingOtherIssuesSTNCount
);

const PendingIssuesList = createSelector(
  selectInventory,
  state => state.otherIssues.otherIssuesList
);

const selectPendingIssuesList = createSelector(
  PendingIssuesList,
  otheIssueSelector.selectAll
);

const PendingIssuesLoanList = createSelector(
  selectInventory,
  state => state.otherIssues.otherIssueLoanList
);

const selectPendingIssuesLoanList = createSelector(
  PendingIssuesLoanList,
  otheIssueSelector.selectAll
);

const selectIsLoadingOtherIssuesSTN = createSelector(
  selectInventory,
  state => state.otherIssues.isLoadingOtherIssuesList
);

const selectIsSearchingStocks = createSelector(
  selectInventory,
  state => state.otherIssues.isSearchingStocks
);

const selectHasSearchStockResults = createSelector(
  selectInventory,
  state => state.otherIssues.hasSearchStockResults
);

const searchOtherIssueStockResults = createSelector(
  selectInventory,
  state => state.otherIssues.searchIssueStockResults
);

const selectOtherIssueStockResults = createSelector(
  searchOtherIssueStockResults,
  otheIssueSelector.selectAll
);

const selectIsVerifyingAllItemSuccess = createSelector(
  selectInventory,
  state => state.stockReceive.isVerifyingAllItemSuccess
);

const selectIsAssigningBinToAllItemsSuccess = createSelector(
  selectInventory,
  state => state.stockReceive.isAssigningBinToAllItemsSuccess
);

const selectOtherIssuesDropDown = createSelector(
  selectInventory,
  state => state.otherIssues.OtherIssuesDropdownValues
);
const selectOtherissuesSelectedDropDown = createSelector(
  selectInventory,
  state => state.otherIssues.selectedDropDownForIssues
);
const selectSelectedIssue = createSelector(
  selectInventory,
  state => state.otherIssues.selectedIssue
);
const selectIssueItemsTotalCount = createSelector(
  selectInventory,
  state => state.stockIssue.issueItemsTotalCount
);
const selectTotalIssueCount = createSelector(
  selectInventory,
  state => state.otherIssues.totalElementsOtherIssues
);
const nonVerifiedOtherIssuesItems = createSelector(
  selectInventory,
  state => state.otherIssues.nonVerifiedOtherIssuesItems
);
const selectNonVerifiedOtherIssueItems = createSelector(
  nonVerifiedOtherIssuesItems,
  otherIssueItemSelector.selectAll
);
const selectIsLoadingSelectedIssueStock = createSelector(
  selectInventory,
  state => state.otherIssues.isLoadingOtherIssueSelectedStock
);

const selectIsSearchingOtherIssueItems = createSelector(
  selectInventory,
  state => state.otherIssues.isSearchingOtherIssueItems
);
const selectHasSearchedOtherIssueItems = createSelector(
  selectInventory,
  state => state.otherIssues.hasSearchedOtherssueIItems
);
const createOtherIssueStockRequestResponse = createSelector(
  selectInventory,
  state => state.otherIssues.createOtherIssueStockRequestResponse
);

const AllOtherIssuesCreateItems = createSelector(
  selectInventory,
  state => state.otherIssues.allOtherIssueCreateItems
);
const selectAllOtherIssuesCreateItems = createSelector(
  AllOtherIssuesCreateItems,
  otherIssueCreateItemSelector.selectAll
);
const SelectedOtherIssuesCreateItems = createSelector(
  selectInventory,
  state => state.otherIssues.selectedOtherIssueCreateItems
);
const selectSelectedOtherIssuesCreateItems = createSelector(
  SelectedOtherIssuesCreateItems,
  otherIssueCreateItemSelector.selectAll
);
const selectAllOtherIssueCreateItemsTotalCount = createSelector(
  selectInventory,
  state => state.otherIssues.AllOtherIssueCreateItemsTotalCount
);

const selectSelectedOtherIssueCreateTotalCount = createSelector(
  selectInventory,
  state => state.otherIssues.SelectedOtherIssueCreateItemsTotalCount
);

const selectIsAllOtherIssueCreateItemsLoading = createSelector(
  selectInventory,
  state => state.otherIssues.isallOtherIssueCreateItemsLoading
);
const selectIsSelectedOtherIssueItemsLoading = createSelector(
  selectInventory,
  state => state.otherIssues.isselectedOtherIssueCreateItemsLoading
);
const selectIsOtherIssueCreateTotalCountLoaded = createSelector(
  selectInventory,
  state => state.otherIssues.isOtherIssueCreateItemTotalCountLoaded
);
const selectIsOtherIssueCreateTotalCountLoading = createSelector(
  selectInventory,
  state => state.otherIssues.isOtherIssueItemTotalCountLoading
);

const selectIsSearchingOtherIssueCreateItems = createSelector(
  selectInventory,
  state => state.otherIssues.isSearchingOtherIssueCreateItems
);
const selectHasSearchedOtherIssueCreateItems = createSelector(
  selectInventory,
  state => state.otherIssues.hasSearchedOtherssueCreateItems
);
const selectCreateStockRequestItemsResponse = createSelector(
  selectInventory,
  state => state.otherIssues.createOtherIssueStockRequestItemsResponse
);
const selectRemoveOtherIssueStockRequestItemsResponse = createSelector(
  selectInventory,
  state => state.otherIssues.RemoveOtherIssueStockRequestItemsResponse
);
const selectupdateStockRequestResponse = createSelector(
  selectInventory,
  state => state.otherIssues.updateStockRequestResponse
);
const selectCreateOtherStockIssueItemsResponse = createSelector(
  selectInventory,
  state => state.otherIssues.createOtherStockIssueItemsResponse
);
const selectConfirmOtherStockIssueResponse = createSelector(
  selectInventory,
  state => state.otherIssues.confirmOtherStockIssueResponse
);
const selectisLoadingOtherIssueDetails = createSelector(
  selectInventory,
  state => state.otherIssues.isLoadingOtherIssueDetails
);

// loan & PSV
const searchedAdjustmentItems = createSelector(
  selectInventory,
  state => state.otherIssues.searchedAdjustmentItems
);

const selectSearchedAdjustmentItems = createSelector(
  searchedAdjustmentItems,
  otherIssueCreateItemSelector.selectAll
);
const AdjustmentItemsInCarts = createSelector(
  selectInventory,
  state => state.otherIssues.AdjustmentItemsInCarts
);
const selectAdjustmentItemsInCart = createSelector(
  AdjustmentItemsInCarts,
  otherIssueCreateItemSelector.selectAll
);
const PendingIssuesADJList = createSelector(
  selectInventory,
  state => state.otherIssues.otherIssueADJList
);

const selectPendingIssuesADJList = createSelector(
  PendingIssuesADJList,
  otheIssueSelector.selectAll
);
const PendingIssuesLossList = createSelector(
  selectInventory,
  state => state.otherIssues.otherIssueLossList
);

const selectPendingIssuesLossList = createSelector(
  PendingIssuesLossList,
  otheIssueSelector.selectAll
);
const PendingIssuesPSVList = createSelector(
  selectInventory,
  state => state.otherIssues.otherIssuePSVList
);

const selectPendingIssuesPSVList = createSelector(
  PendingIssuesPSVList,
  otheIssueSelector.selectAll
);
const selectcreateStockRequestAdjustmentResponse = createSelector(
  selectInventory,
  state => state.otherIssues.createStockRequestAdjustmentResponse
);

const AdjustmentItemsInCartsSearch = createSelector(
  selectInventory,
  state => state.otherIssues.AdjustmentItemsInCartsSearch
);

const selectAdjustmentItemsInCartsSearch = createSelector(
  AdjustmentItemsInCartsSearch,
  otherIssueCreateItemSelector.selectAll
);

const selectIsSearchingAdjustment = createSelector(
  selectInventory,
  state => state.otherIssues.isSearchingAdjustment
);
const selectHasSearchedItemAdjustment = createSelector(
  selectInventory,
  state => state.otherIssues.hasSearchedItemAdjustment
);
const selectIsLoadingAdjustment = createSelector(
  selectInventory,
  state => state.otherIssues.IsLoadingAdjustment
);
const selectHasSearchItemInCartSearch = createSelector(
  selectInventory,
  state => state.otherIssues.hasSearchItemInCartSearch
);
//psv
const searchedPSVtItems = createSelector(
  selectInventory,
  state => state.otherIssues.searchedPSVItems
);

const selectSearchedPSVItems = createSelector(
  searchedPSVtItems,
  otherIssueCreateItemSelector.selectAll
);
const PSVItemsInCarts = createSelector(
  selectInventory,
  state => state.otherIssues.PSVItemsInCarts
);
const selectPSVItemsInCart = createSelector(
  PSVItemsInCarts,
  otherIssueCreateItemSelector.selectAll
);

const selectcreateStockRequestPSVResponse = createSelector(
  selectInventory,
  state => state.otherIssues.createStockRequestPSVResponse
);

const PSVItemsInCartsSearch = createSelector(
  selectInventory,
  state => state.otherIssues.PSVItemsInCartsSearch
);

const selectPSVItemsInCartsSearch = createSelector(
  PSVItemsInCartsSearch,
  otherIssueCreateItemSelector.selectAll
);
const selectError = createSelector(
  selectInventory,
  state => state.otherIssues.error
);

const selectIsSearchingPSV = createSelector(
  selectInventory,
  state => state.otherIssues.isSearchingPSV
);
const selectHasSearchedItemPSV = createSelector(
  selectInventory,
  state => state.otherIssues.hasSearchedItemPSV
);
const selectIsLoadingPSV = createSelector(
  selectInventory,
  state => state.otherIssues.IsLoadingPSV
);
const selectHasSearchItemInCartSearchPSV = createSelector(
  selectInventory,
  state => state.otherIssues.hasSearchItemInCartSearchPSV
);
//FOC
const searchedFOCItems = createSelector(
  selectInventory,
  state => state.otherIssues.searchedFOCItems
);

const selectSearchedFOCItems = createSelector(
  searchedFOCItems,
  otherIssueCreateItemSelector.selectAll
);
const FOCItemsInCarts = createSelector(
  selectInventory,
  state => state.otherIssues.FOCItemsInCarts
);
const selectFOCItemsInCart = createSelector(
  FOCItemsInCarts,
  otherIssueCreateItemSelector.selectAll
);

const selectcreateStockRequestFOCResponse = createSelector(
  selectInventory,
  state => state.otherIssues.createStockRequestFOCResponse
);

const FOCItemsInCartsSearch = createSelector(
  selectInventory,
  state => state.otherIssues.FOCItemsInCartsSearch
);

const selectFOCItemsInCartsSearch = createSelector(
  FOCItemsInCartsSearch,
  otherIssueCreateItemSelector.selectAll
);

const PendingIssuesFOCList = createSelector(
  selectInventory,
  state => state.otherIssues.otherIssueFOCList
);

const selectPendingIssuesFOCList = createSelector(
  PendingIssuesFOCList,
  otheIssueSelector.selectAll
);
const selectIsSearchingFOC = createSelector(
  selectInventory,
  state => state.otherIssues.isSearchingFOC
);
const selectHasSearchedItemFOC = createSelector(
  selectInventory,
  state => state.otherIssues.hasSearchedItemFOC
);
const selectIsLoadingFOC = createSelector(
  selectInventory,
  state => state.otherIssues.IsLoadingFOC
);
const selectHasSearchItemInCartSearchFOC = createSelector(
  selectInventory,
  state => state.otherIssues.hasSearchItemInCartSearchFOC
);
const selectIsLoadingCancelStockRequestResponse = createSelector(
  selectInventory,
  state => state.otherIssues.IsLoadingCancelStockRequestResponse
);
const selectCancelOtherStockRequestResponse = createSelector(
  selectInventory,
  state => state.otherIssues.cancelStockRequestResponse
);
const selectprintDataResponse = createSelector(
  selectInventory,
  state => {
    return state.otherIssues.printDataResponse;
  }
);
const selectProductCategories = createSelector(
  selectInventory,
  state => state.otherIssues.productCategories
);

const selectProductGroups = createSelector(
  selectInventory,
  state => state.otherIssues.productGroups
);
const selectfilterDataAllProducts = createSelector(
  selectInventory,
  state => state.otherIssues.filterDataAllProducts
);

const selectfilterDataSelectedProducts = createSelector(
  selectInventory,
  state => state.otherIssues.filterDataSelectedProducts
);
const selectSortDataAllProducts = createSelector(
  selectInventory,
  state => state.otherIssues.sortDataAllProducts
);

const selectSortDataSelectedProducts = createSelector(
  selectInventory,
  state => state.otherIssues.sortDataSelectedProducts
);

const selectfilterDataOtherIssue = createSelector(
  selectInventory,
  state => state.otherIssues.filterDataOtherIssue
);
const selectSortDataOtherIssue = createSelector(
  selectInventory,
  state => state.otherIssues.sortDataotherIssue
);
export const OtherIssuesSelector = {
  selectHasError,
  selectIsLoading,
  selectOtherIssuesSTNCount,
  selectPendingIssuesList,
  selectIsLoadingOtherIssuesSTN,
  selectIsSearchingStocks,
  selectHasSearchStockResults,
  selectIsVerifyingAllItemSuccess,
  selectIsAssigningBinToAllItemsSuccess,
  selectOtherIssueStockResults,
  selectOtherIssuesDropDown,
  selectPendingIssuesLoanList,
  selectOtherissuesSelectedDropDown,
  selectSelectedIssue,
  selectIssueItemsTotalCount,
  selectTotalIssueCount,
  selectNonVerifiedOtherIssueItems,
  selectIsLoadingSelectedIssueStock,
  selectIsSearchingOtherIssueItems,
  selectHasSearchedOtherIssueItems,
  createOtherIssueStockRequestResponse,
  selectAllOtherIssuesCreateItems,
  selectSelectedOtherIssuesCreateItems,
  selectAllOtherIssueCreateItemsTotalCount,
  selectSelectedOtherIssueCreateTotalCount,
  selectIsAllOtherIssueCreateItemsLoading,
  selectIsSelectedOtherIssueItemsLoading,
  selectIsOtherIssueCreateTotalCountLoaded,
  selectIsOtherIssueCreateTotalCountLoading,
  selectIsSearchingOtherIssueCreateItems,
  selectHasSearchedOtherIssueCreateItems,
  selectCreateStockRequestItemsResponse,
  selectRemoveOtherIssueStockRequestItemsResponse,
  selectupdateStockRequestResponse,
  selectCreateOtherStockIssueItemsResponse,
  selectConfirmOtherStockIssueResponse,
  selectisLoadingOtherIssueDetails,
  selectSearchedAdjustmentItems,
  selectAdjustmentItemsInCart,
  selectPendingIssuesADJList,
  selectcreateStockRequestAdjustmentResponse,
  selectIsSearchingAdjustment,
  selectPendingIssuesLossList,
  selectPendingIssuesPSVList,
  selectAdjustmentItemsInCartsSearch,
  selectHasSearchedItemAdjustment,
  selectIsLoadingAdjustment,
  selectHasSearchItemInCartSearch,
  //psv
  selectSearchedPSVItems,
  selectPSVItemsInCart,
  selectcreateStockRequestPSVResponse,
  selectPSVItemsInCartsSearch,
  selectIsSearchingPSV,
  selectHasSearchedItemPSV,
  selectIsLoadingPSV,
  selectHasSearchItemInCartSearchPSV,
  selectError,
  //FOC
  selectSearchedFOCItems,
  selectFOCItemsInCart,
  selectcreateStockRequestFOCResponse,
  selectFOCItemsInCartsSearch,
  selectPendingIssuesFOCList,
  selectIsSearchingFOC,
  selectHasSearchedItemFOC,
  selectIsLoadingFOC,
  selectHasSearchItemInCartSearchFOC,
  selectIsLoadingCancelStockRequestResponse,
  selectCancelOtherStockRequestResponse,
  selectprintDataResponse,
  selectProductCategories,
  selectProductGroups,
  selectfilterDataAllProducts,
  selectfilterDataSelectedProducts,
  selectSortDataAllProducts,
  selectSortDataSelectedProducts,
  selectfilterDataOtherIssue,
  selectSortDataOtherIssue
};
