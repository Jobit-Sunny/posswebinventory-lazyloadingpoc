import {
  OtherIssueEntity,
  OtherIssueItemEntity,
  OtherIssueCreateItemEntity
} from './other-issues.entity';

import { TransferType } from '../../../in-stock-management/in-stock/models/in-stock.model';

import { CustomErrors, ProductCategory, ProductGroup } from '@poss-web/core';

import {
  RequestOtherIssueStockTransferNote,
  OtherIssuesCreateStockResponse
} from '../models/other-issues.model';
import { Filter, Column } from '@poss-web/shared';

export interface OtherIssuesState {
  pendingOtherIssuesSTNCount: number;

  hasError?: string;
  isLoading: boolean;

  isSearchingStocks: boolean;
  hasSearchStockResults: boolean;
  searchIssueStockResults: OtherIssueEntity;
  otherIssuesList: OtherIssueEntity;
  otherIssueLoanList: OtherIssueEntity;
  isLoadingOtherIssuesList: boolean;
  isLoadingOtherIssuesLoanList: boolean;

  OtherIssuesDropdownValues: TransferType[];

  error: CustomErrors;
  selectedDropDownForIssues: string;
  selectedIssue: RequestOtherIssueStockTransferNote;
  isItemIssued: boolean;
  issueItemsTotalCount: number;
  totalElementsOtherIssues: number;
  nonVerifiedOtherIssuesItems: OtherIssueItemEntity;
  isLoadingOtherIssueSelectedStock: boolean;
  isSearchingOtherIssueItems: boolean;
  hasSearchedOtherssueIItems: boolean;
  createOtherStockIssueItemsResponse: any;
  isLoadingOtherIssueDetails: boolean;

  confirmOtherStockIssueResponse: any;
  //create  page
  createOtherIssueStockRequestResponse: OtherIssuesCreateStockResponse;
  isCreateOtherIssueStockRequestPending: boolean;

  allOtherIssueCreateItems: OtherIssueCreateItemEntity;
  isallOtherIssueCreateItemsLoading: boolean;

  selectedOtherIssueCreateItems: OtherIssueCreateItemEntity;
  isselectedOtherIssueCreateItemsLoading: boolean;

  AllOtherIssueCreateItemsTotalCount: number;
  SelectedOtherIssueCreateItemsTotalCount: number;
  isOtherIssueCreateItemTotalCountLoaded: boolean;
  isOtherIssueItemTotalCountLoading: boolean;

  isSearchingOtherIssueCreateItems: boolean;
  hasSearchedOtherssueCreateItems: boolean;

  createOtherIssueStockRequestItemsResponse: any;
  IsLoadingOtherIssueRequestItemsResponse: boolean;

  RemoveOtherIssueStockRequestItemsResponse: any;
  IsLoadingOtherIssueStockRequestItemsResponse: boolean;

  updateStockRequestCreateItemResponse: any;
  IsLoadingUpdateStockRequestCreateItemResponse: boolean;

  updateStockRequestResponse: any;
  IsLoadingUpdateStockRequestResponse: boolean;

  //adjustment
  searchedAdjustmentItems: OtherIssueItemEntity;
  isSearchingAdjustment?: boolean;
  hasSearchedItemAdjustment?: boolean;
  searchCountAdjustment: number;
  AdjustmentItemsInCarts: OtherIssueItemEntity;
  AdjustmentItemsInCartsSearch: OtherIssueItemEntity;
  hasSearchItemInCartSearch: boolean;
  createStockRequestAdjustmentResponse: any;
  IsLoadingAdjustment: boolean;

  otherIssueADJList: OtherIssueEntity;
  isLoadingOtherIssuesADJList: boolean;

  otherIssueLossList: OtherIssueEntity;
  isLoadingOtherIssuesLossList: boolean;

  otherIssuePSVList: OtherIssueEntity;
  isLoadingOtherIssuesPSVList: boolean;

  //psv
  searchedPSVItems: OtherIssueItemEntity;
  isSearchingPSV?: boolean;
  hasSearchedItemPSV?: boolean;
  searchCountPSV: number;
  PSVItemsInCarts: OtherIssueItemEntity;
  PSVItemsInCartsSearch: OtherIssueItemEntity;
  hasSearchItemInCartSearchPSV: boolean;
  createStockRequestPSVResponse: any;
  IsLoadingPSV: boolean;

  //FOC
  searchedFOCItems: OtherIssueItemEntity;
  isSearchingFOC?: boolean;
  hasSearchedItemFOC?: boolean;
  searchCountFOC: number;
  FOCItemsInCarts: OtherIssueItemEntity;
  FOCItemsInCartsSearch: OtherIssueItemEntity;
  hasSearchItemInCartSearchFOC: boolean;
  createStockRequestFOCResponse: any;
  IsLoadingFOC: boolean;

  otherIssueFOCList: OtherIssueEntity;
  isLoadingOtherIssuesFOCList: boolean;

  IsLoadingCancelStockRequestResponse: boolean;
  cancelStockRequestResponse: any;
  printDataResponse: any;

  productCategories: ProductCategory[];
  productGroups: ProductGroup[];
  filterDataAllProducts: { [key: string]: Filter[] };
  filterDataSelectedProducts: { [key: string]: Filter[] };

  sortDataAllProducts: Column[];
  sortDataSelectedProducts: Column[];

  filterDataOtherIssue: { [key: string]: Filter[] };
  sortDataotherIssue: Column[];
}
