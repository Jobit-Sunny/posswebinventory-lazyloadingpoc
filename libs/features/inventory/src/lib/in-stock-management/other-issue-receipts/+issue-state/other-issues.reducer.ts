import {
  OtherIssuesActionTypes,
  OtherIssuesActions
} from './other-issues.actions';
import {
  OtherIssueAdapter,
  otherIssuesItemAdapter,
  otherIssuesCreateItemAdapter
} from './other-issues.entity';
import { OtherReceiptsIssuesEnum } from '../../../in-stock-management/in-stock/in-stock.enum';
import { OtherIssuesState } from './other-issues.state';

export const initialState: OtherIssuesState = {
  isLoading: false,
  pendingOtherIssuesSTNCount: 0,
  otherIssuesList: OtherIssueAdapter.getInitialState(),
  otherIssueLoanList: OtherIssueAdapter.getInitialState(),
  isLoadingOtherIssuesList: false,
  isLoadingOtherIssuesLoanList: false,
  isSearchingStocks: false,
  hasSearchStockResults: null,
  searchIssueStockResults: OtherIssueAdapter.getInitialState(),
  OtherIssuesDropdownValues: null,
  selectedDropDownForIssues: OtherReceiptsIssuesEnum.EXHIBITION_TYPE,
  error: null,
  selectedIssue: null,
  isItemIssued: false,
  issueItemsTotalCount: 0,
  totalElementsOtherIssues: 0,
  nonVerifiedOtherIssuesItems: otherIssuesItemAdapter.getInitialState(),
  isLoadingOtherIssueSelectedStock: false,
  isSearchingOtherIssueItems: false,
  hasSearchedOtherssueIItems: false,
  createOtherStockIssueItemsResponse: {},
  isLoadingOtherIssueDetails: false,
  confirmOtherStockIssueResponse: {},
  //create page
  createOtherIssueStockRequestResponse: null,
  isCreateOtherIssueStockRequestPending: false,
  allOtherIssueCreateItems: otherIssuesCreateItemAdapter.getInitialState(),
  selectedOtherIssueCreateItems: otherIssuesCreateItemAdapter.getInitialState(),
  isallOtherIssueCreateItemsLoading: false,
  isselectedOtherIssueCreateItemsLoading: false,

  AllOtherIssueCreateItemsTotalCount: 0,
  SelectedOtherIssueCreateItemsTotalCount: 0,

  isOtherIssueCreateItemTotalCountLoaded: null,
  isOtherIssueItemTotalCountLoading: false,
  isSearchingOtherIssueCreateItems: false,
  hasSearchedOtherssueCreateItems: false,
  createOtherIssueStockRequestItemsResponse: {},
  IsLoadingOtherIssueRequestItemsResponse: false,

  RemoveOtherIssueStockRequestItemsResponse: {},
  IsLoadingOtherIssueStockRequestItemsResponse: false,
  updateStockRequestCreateItemResponse: {},
  IsLoadingUpdateStockRequestCreateItemResponse: false,

  updateStockRequestResponse: {},
  IsLoadingUpdateStockRequestResponse: false,

  //adjustment
  isSearchingAdjustment: false,
  hasSearchedItemAdjustment: false,
  searchedAdjustmentItems: otherIssuesItemAdapter.getInitialState(),
  searchCountAdjustment: null,
  AdjustmentItemsInCarts: otherIssuesItemAdapter.getInitialState(),
  createStockRequestAdjustmentResponse: {},
  IsLoadingAdjustment: false,
  AdjustmentItemsInCartsSearch: otherIssuesItemAdapter.getInitialState(),
  hasSearchItemInCartSearch: false,
  otherIssueADJList: OtherIssueAdapter.getInitialState(),
  isLoadingOtherIssuesADJList: false,

  otherIssueLossList: OtherIssueAdapter.getInitialState(),
  isLoadingOtherIssuesLossList: false,

  otherIssuePSVList: OtherIssueAdapter.getInitialState(),
  isLoadingOtherIssuesPSVList: false,
  // psv
  isSearchingPSV: false,
  hasSearchedItemPSV: false,
  searchedPSVItems: otherIssuesItemAdapter.getInitialState(),
  searchCountPSV: null,
  PSVItemsInCarts: otherIssuesItemAdapter.getInitialState(),
  createStockRequestPSVResponse: {},
  IsLoadingPSV: false,
  PSVItemsInCartsSearch: otherIssuesItemAdapter.getInitialState(),
  hasSearchItemInCartSearchPSV: false,
  //FOC
  isSearchingFOC: false,
  hasSearchedItemFOC: false,
  searchedFOCItems: otherIssuesItemAdapter.getInitialState(),
  searchCountFOC: null,
  FOCItemsInCarts: otherIssuesItemAdapter.getInitialState(),
  createStockRequestFOCResponse: {},
  IsLoadingFOC: false,
  FOCItemsInCartsSearch: otherIssuesItemAdapter.getInitialState(),
  hasSearchItemInCartSearchFOC: false,

  otherIssueFOCList: OtherIssueAdapter.getInitialState(),
  isLoadingOtherIssuesFOCList: false,

  IsLoadingCancelStockRequestResponse: false,
  cancelStockRequestResponse: null,
  printDataResponse: null,

  productCategories: null,
  productGroups: null,
  filterDataAllProducts: {},
  filterDataSelectedProducts: {},
  sortDataAllProducts: [],
  sortDataSelectedProducts: [],

  filterDataOtherIssue: {},
  sortDataotherIssue: []
};

export function OtherIssuesReducer(
  state: OtherIssuesState = initialState,
  action: OtherIssuesActions
): OtherIssuesState {
  switch (action.type) {
    case OtherIssuesActionTypes.RESET_ISSUE_LIST_DATA:
      return {
        ...state,
        pendingOtherIssuesSTNCount: 0,
        OtherIssuesDropdownValues: null,
        otherIssuesList: OtherIssueAdapter.getInitialState(),
        otherIssueLoanList: OtherIssueAdapter.getInitialState(),
        otherIssueADJList: OtherIssueAdapter.getInitialState(),
        otherIssuePSVList: OtherIssueAdapter.getInitialState(),
        otherIssueLossList: OtherIssueAdapter.getInitialState(),
        otherIssueFOCList: OtherIssueAdapter.getInitialState(),
        totalElementsOtherIssues: 0,
        selectedDropDownForIssues: OtherReceiptsIssuesEnum.EXHIBITION_TYPE,
        error: null
      };
    case OtherIssuesActionTypes.LOAD_ISSUES_STN_COUNT:
      return {
        ...state,
        error: null
      };

    case OtherIssuesActionTypes.LOAD_ISSUES_STN_COUNT_SUCCESS:
      return {
        ...state,
        pendingOtherIssuesSTNCount: action.payload.pendingOtherIssuesSTNCount,
        OtherIssuesDropdownValues: action.payload.countData
      };

    case OtherIssuesActionTypes.LOAD_ISSUE_LIST:
      return {
        ...state,
        isLoadingOtherIssuesList: true,
        error: null
      };

    case OtherIssuesActionTypes.LOAD_ISSUE_LIST_SUCCESS:
      return {
        ...state,
        otherIssuesList: OtherIssueAdapter.addMany(
          action.payload.issueData,
          state.otherIssuesList
        ),
        isLoadingOtherIssuesList: false,
        error: null,
        totalElementsOtherIssues: action.payload.totalElements
      };

    case OtherIssuesActionTypes.LOAD_ISSUE_LIST_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingOtherIssuesList: false
      };
    case OtherIssuesActionTypes.LOAD_ISSUE_LOAN_LIST:
      return {
        ...state,
        isLoadingOtherIssuesLoanList: true,
        error: null
      };

    case OtherIssuesActionTypes.LOAD_ISSUE_LOAN_LIST_SUCCESS:
      return {
        ...state,
        otherIssueLoanList: OtherIssueAdapter.addMany(
          action.payload.issueData,
          state.otherIssueLoanList
        ),
        isLoadingOtherIssuesLoanList: false,
        totalElementsOtherIssues: action.payload.totalElements,
        error: null
      };

    case OtherIssuesActionTypes.LOAD_ISSUE_LOAN_LIST_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingOtherIssuesLoanList: false
      };

    case OtherIssuesActionTypes.SEARCH_PENDING_ISSUE:
      return {
        ...state,
        isSearchingStocks: true,
        hasSearchStockResults: null,
        searchIssueStockResults: OtherIssueAdapter.removeAll(
          state.searchIssueStockResults
        ),
        error: null
      };

    case OtherIssuesActionTypes.SEARCH_PENDING_ISSUE_SUCCESS:
      return {
        ...state,
        searchIssueStockResults: OtherIssueAdapter.addAll(
          action.payload,
          state.searchIssueStockResults
        ),
        isSearchingStocks: false,
        hasSearchStockResults: true
      };

    case OtherIssuesActionTypes.SEARCH_PENDING_ISSUE_FAILURE:
      return {
        ...state,
        error: action.payload,
        isSearchingStocks: false,
        hasSearchStockResults: false
      };

    case OtherIssuesActionTypes.DROPDOWN_SELECTED_FOR_ISSUES:
      return {
        ...state,
        selectedDropDownForIssues: action.payload
      };
    case OtherIssuesActionTypes.LOAD_SELECTED_ISSUE:
      return {
        ...state,
        isLoadingOtherIssueSelectedStock: true,
        selectedIssue: null,
        hasError: null,
        isItemIssued: null
      };
    case OtherIssuesActionTypes.LOAD_SELECTED_ISSUE_SUCCESS:
      return {
        ...state,
        selectedIssue: action.payload,
        isLoadingOtherIssueSelectedStock: false
      };
    case OtherIssuesActionTypes.LOAD_SELECTED_ISSUE_FAILURE:
      return {
        ...state,
        isLoadingOtherIssueSelectedStock: false,
        hasError: action.payload
      };
    //same 2
    /* case OtherIssuesActionTypes.LOAD_NON_VERIFIED_OTHER_ISSUE_ITEMS:
      return {
        ...state,
        isLoadingOtherIssueDetails: true,
        error: null
      };
 */
    case OtherIssuesActionTypes.LOAD_NON_VERIFIED_OTHER_ISSUE_ITEMS_SUCCESS:
      return {
        ...state,
        nonVerifiedOtherIssuesItems: otherIssuesItemAdapter.addAll(
          action.payload,
          state.nonVerifiedOtherIssuesItems
        ),
        isLoadingOtherIssueDetails: false
      };

    //same1
    /* case OtherIssuesActionTypes.LOAD_NON_VERIFIED_OTHER_ISSUE_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingOtherIssueDetails: false
      }; */

    case OtherIssuesActionTypes.SEARCH_CLEAR_ISSUE:
      return {
        ...state,
        searchIssueStockResults: OtherIssueAdapter.removeAll(
          state.searchIssueStockResults
        ),
        error: null,
        isSearchingStocks: false,
        hasSearchStockResults: null
      };
    case OtherIssuesActionTypes.CREATE_OTHER_STOCK_ISSUE_ITEMS:
      return {
        ...state,
        isLoadingOtherIssueDetails: true,
        error: null,
        createOtherStockIssueItemsResponse: {}
      };
    case OtherIssuesActionTypes.CREATE_OTHER_STOCK_ISSUE_ITEMS_SUCCESS:
      return {
        ...state,
        createOtherStockIssueItemsResponse: action.payload,
        isLoadingOtherIssueDetails: false,
        error: null
      };
    //same1
    /* case OtherIssuesActionTypes.CREATE_OTHER_STOCK_ISSUE_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingOtherIssueDetails: false
      }; */
    case OtherIssuesActionTypes.REMOVE_INITIAL_LOAD_OTHER_ISSUE:
      return {
        ...state,
        nonVerifiedOtherIssuesItems: otherIssuesItemAdapter.getInitialState()
      };
    //same 2
    case OtherIssuesActionTypes.CONFIRM_OTHER_STOCK_ISSUE:
    case OtherIssuesActionTypes.LOAD_NON_VERIFIED_OTHER_ISSUE_ITEMS:
      return {
        ...state,
        isLoadingOtherIssueDetails: true,
        error: null
      };
    case OtherIssuesActionTypes.CONFIRM_OTHER_STOCK_ISSUE_SUCCESS:
      return {
        ...state,
        confirmOtherStockIssueResponse: action.payload,
        isLoadingOtherIssueDetails: false,
        error: null
      };
    //same1
    case OtherIssuesActionTypes.CONFIRM_OTHER_STOCK_ISSUE_FAILURE:
    case OtherIssuesActionTypes.LOAD_NON_VERIFIED_OTHER_ISSUE_ITEMS_FAILURE:
    case OtherIssuesActionTypes.CREATE_OTHER_STOCK_ISSUE_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingOtherIssueDetails: false
      };
    case OtherIssuesActionTypes.RESET_CONFIRM_OTHER_STOCK_ISSUE_RESPONSE:
      return {
        ...state,
        confirmOtherStockIssueResponse: {},
        printDataResponse: null,
        filterDataOtherIssue: {},
        sortDataotherIssue: [],
        error: null
      };
    //create page
    case OtherIssuesActionTypes.CREATE_OTHER_ISSUE_STOCK_REQUEST:
      return {
        ...state,
        createOtherIssueStockRequestResponse: null,
        isCreateOtherIssueStockRequestPending: true
      };

    case OtherIssuesActionTypes.CREATE_OTHER_ISSUE_STOCK_REQUEST_SUCCESS:
      return {
        ...state,
        createOtherIssueStockRequestResponse: action.payload,
        isCreateOtherIssueStockRequestPending: false
      };

    case OtherIssuesActionTypes.CREATE_OTHER_ISSUE_STOCK_REQUEST_FAILURE:
      return {
        ...state,
        error: action.payload,
        isCreateOtherIssueStockRequestPending: false
      };

    case OtherIssuesActionTypes.LOAD_ALL_OTHER_ISSUE_CREATE_ITEMS:
      return {
        ...state,
        isallOtherIssueCreateItemsLoading: true,
        error: null
      };

    case OtherIssuesActionTypes.LOAD_ALL_OTHER_ISSUE_CREATE_ITEMS_SUCCESS:
      return {
        ...state,
        allOtherIssueCreateItems: otherIssuesCreateItemAdapter.addAll(
          action.payload,
          state.allOtherIssueCreateItems
        ),
        isallOtherIssueCreateItemsLoading: false
      };

    case OtherIssuesActionTypes.LOAD_ALL_OTHER_ISSUE_CREATE_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isallOtherIssueCreateItemsLoading: false
      };

    case OtherIssuesActionTypes.LOAD_SELECTED_OTHER_ISSUE_ITEMS:
      return {
        ...state,
        isselectedOtherIssueCreateItemsLoading: true,
        error: null
      };

    case OtherIssuesActionTypes.LOAD_SELECTED_OTHER_ISSUE_ITEMS_SUCCESS:
      return {
        ...state,
        selectedOtherIssueCreateItems: otherIssuesCreateItemAdapter.addAll(
          action.payload,
          state.selectedOtherIssueCreateItems
        ),
        isselectedOtherIssueCreateItemsLoading: false
      };

    case OtherIssuesActionTypes.LOAD_SELECTED_OTHER_ISSUE_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isselectedOtherIssueCreateItemsLoading: false
      };

    case OtherIssuesActionTypes.LOAD_ISSUE_ITEMS_CREATE_COUNT:
      return {
        ...state,
        isOtherIssueCreateItemTotalCountLoaded: null,
        isOtherIssueItemTotalCountLoading: true,
        AllOtherIssueCreateItemsTotalCount: 0,
        SelectedOtherIssueCreateItemsTotalCount: 0,
        hasError: null
      };
    case OtherIssuesActionTypes.LOAD_ISSUE_ITEMS_CREATE_COUNT_SUCCESS:
      return {
        ...state,
        AllOtherIssueCreateItemsTotalCount:
          action.payload.AllOtherIssueCreateItemsTotalCount,

        isOtherIssueCreateItemTotalCountLoaded: true,

        SelectedOtherIssueCreateItemsTotalCount:
          action.payload.SelectedOtherIssueCreateItemsTotalCount,

        isOtherIssueItemTotalCountLoading: false
      };

    case OtherIssuesActionTypes.LOAD_ISSUE_ITEMS_CREATE_COUNT_FAILURE:
      return {
        ...state,
        error: action.payload,
        isOtherIssueCreateItemTotalCountLoaded: false,
        isOtherIssueItemTotalCountLoading: false
      };

    case OtherIssuesActionTypes.CREATE_OTHER_ISSUE_STOCK_REQUEST_ITEMS:
      return {
        ...state,
        IsLoadingOtherIssueRequestItemsResponse: true,
        error: null,
        createOtherIssueStockRequestItemsResponse: {}
      };
    case OtherIssuesActionTypes.CREATE_OTHER_ISSUE_STOCK_REQUEST_ITEMS_SUCCESS:
      return {
        ...state,
        createOtherIssueStockRequestItemsResponse: action.payload,
        IsLoadingOtherIssueRequestItemsResponse: false,
        error: null
      };
    case OtherIssuesActionTypes.CREATE_OTHER_ISSUE_STOCK_REQUEST_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        IsLoadingOtherIssueRequestItemsResponse: false
      };

    case OtherIssuesActionTypes.REMOVE_OTHER_ISSUE_STOCK_REQUEST_ITEMS:
      return {
        ...state,
        IsLoadingOtherIssueStockRequestItemsResponse: true,
        error: null,
        RemoveOtherIssueStockRequestItemsResponse: {}
      };
    case OtherIssuesActionTypes.REMOVE_OTHER_ISSUE_STOCK_REQUEST_ITEMS_SUCCESS:
      return {
        ...state,
        RemoveOtherIssueStockRequestItemsResponse: action.payload,
        IsLoadingOtherIssueStockRequestItemsResponse: false,
        error: null
      };
    case OtherIssuesActionTypes.REMOVE_OTHER_ISSUE_STOCK_REQUEST_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        IsLoadingOtherIssueStockRequestItemsResponse: false
      };

    case OtherIssuesActionTypes.UPDATE_STOCK_REQUEST_CREATE_ITEM:
      return {
        ...state,
        IsLoadingUpdateStockRequestCreateItemResponse: true,
        hasError: null
      };
    case OtherIssuesActionTypes.UPDATE_STOCK_REQUEST_CREATE_ITEM_SUCCESS:
      return {
        ...state,
        updateStockRequestCreateItemResponse: action.payload,
        IsLoadingUpdateStockRequestCreateItemResponse: false,
        hasError: null
      };
    case OtherIssuesActionTypes.UPDATE_STOCK_REQUEST_CREATE_ITEM_FAILURE:
      return {
        ...state,
        error: action.payload,
        IsLoadingUpdateStockRequestCreateItemResponse: false
      };

    case OtherIssuesActionTypes.RESET_OTHER_ISSUE_CREATE_LIST_ITEMS:
      return {
        ...state,
        allOtherIssueCreateItems: otherIssuesCreateItemAdapter.removeAll(
          state.allOtherIssueCreateItems
        ),
        selectedOtherIssueCreateItems: otherIssuesCreateItemAdapter.removeAll(
          state.selectedOtherIssueCreateItems
        ),
        error: null
      };
    case OtherIssuesActionTypes.RESET_OTHER_ISSUE_CREATE_RESPONSE:
      return {
        ...state,
        createOtherIssueStockRequestItemsResponse: {},
        RemoveOtherIssueStockRequestItemsResponse: {},
        updateStockRequestResponse: {},
        AllOtherIssueCreateItemsTotalCount: 0,
        SelectedOtherIssueCreateItemsTotalCount: 0,
        filterDataAllProducts: {},
        filterDataSelectedProducts: {},
        sortDataAllProducts: [],
        sortDataSelectedProducts: [],
        error: null
      };

    case OtherIssuesActionTypes.UPDATE_STOCK_REQUEST:
      return {
        ...state,
        IsLoadingUpdateStockRequestResponse: true,
        error: null
      };
    case OtherIssuesActionTypes.UPDATE_STOCK_REQUEST_SUCCESS:
      return {
        ...state,
        updateStockRequestResponse: action.payload,
        IsLoadingUpdateStockRequestResponse: false,
        error: null
      };
    case OtherIssuesActionTypes.UPDATE_STOCK_REQUEST_FAILURE:
      return {
        ...state,
        error: action.payload,
        IsLoadingUpdateStockRequestResponse: false
      };
    case OtherIssuesActionTypes.ADJUSTMENT_SEARCH:
      return {
        ...state,
        isSearchingAdjustment: true,
        hasSearchedItemAdjustment: false,
        hasError: null
      };
    case OtherIssuesActionTypes.ADJUSTMENT_SEARCH_SUCCESS:
      return {
        ...state,
        isSearchingAdjustment: false,
        hasSearchedItemAdjustment: true,
        searchedAdjustmentItems: otherIssuesItemAdapter.addAll(
          action.payload.items,
          state.searchedAdjustmentItems
        ),
        searchCountAdjustment: action.payload.count
      };

    case OtherIssuesActionTypes.ADJUSTMENT_SEARCH_FAILURE:
      return {
        ...state,
        isSearchingAdjustment: false,
        hasSearchedItemAdjustment: false,
        error: action.payload
      };
    case OtherIssuesActionTypes.ADD_ADJUSTMENT_ITEMS_TO_CART:
      return {
        ...state,
        searchedAdjustmentItems: otherIssuesItemAdapter.removeAll(
          state.searchedAdjustmentItems
        ),
        AdjustmentItemsInCarts: otherIssuesItemAdapter.addMany(
          action.payload,
          state.AdjustmentItemsInCarts
        )
      };
    case OtherIssuesActionTypes.CREATE_STOCK_REQUEST_ADJUSTMENT:
      return {
        ...state,
        IsLoadingAdjustment: true,
        error: null
      };
    case OtherIssuesActionTypes.CREATE_STOCK_REQUEST_ADJUSTMENT_SUCCESS:
      return {
        ...state,
        createStockRequestAdjustmentResponse: action.payload,
        IsLoadingAdjustment: false,
        error: null
      };
    case OtherIssuesActionTypes.CREATE_STOCK_REQUEST_ADJUSTMENT_FAILURE:
      return {
        ...state,
        error: action.payload,
        IsLoadingAdjustment: false
      };
    case OtherIssuesActionTypes.UPDATE_CART_ITEM_ADJUSTMENT:
      return {
        ...state,
        AdjustmentItemsInCarts: otherIssuesItemAdapter.updateOne(
          {
            id: action.payload.id,
            changes: {
              measuredQuantity: action.payload.quantity,
              measuredWeight: action.payload.weight
            }
          },
          state.AdjustmentItemsInCarts
        )
      };
    case OtherIssuesActionTypes.REMOVE_CART_ITEM_ADJUSTMENT:
      return {
        ...state,
        AdjustmentItemsInCarts: otherIssuesItemAdapter.removeMany(
          action.payload.ids,
          state.AdjustmentItemsInCarts
        ),
        AdjustmentItemsInCartsSearch: otherIssuesItemAdapter.removeMany(
          action.payload.ids,
          state.AdjustmentItemsInCartsSearch
        ),
        hasSearchItemInCartSearch: false
      };
    case OtherIssuesActionTypes.SEARCH_CART_ITEM_ADJUSTMENT:
      return {
        ...state,
        AdjustmentItemsInCartsSearch: state.AdjustmentItemsInCarts,
        hasSearchItemInCartSearch: true
      };
    case OtherIssuesActionTypes.LOAD_ISSUE_ADJ_LIST:
      return {
        ...state,
        isLoadingOtherIssuesADJList: true,
        error: null
      };

    case OtherIssuesActionTypes.LOAD_ISSUE_ADJ_LIST_SUCCESS:
      return {
        ...state,
        otherIssueADJList: OtherIssueAdapter.addMany(
          action.payload.issueData,
          state.otherIssueADJList
        ),
        isLoadingOtherIssuesADJList: false,
        totalElementsOtherIssues: action.payload.totalElements,
        error: null
      };

    case OtherIssuesActionTypes.LOAD_ISSUE_ADJ_LIST_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingOtherIssuesADJList: false
      };
    case OtherIssuesActionTypes.LOAD_ISSUE_LOSS_LIST:
      return {
        ...state,
        isLoadingOtherIssuesLossList: true,
        error: null
      };

    case OtherIssuesActionTypes.LOAD_ISSUE_LOSS_LIST_SUCCESS:
      return {
        ...state,
        otherIssueLossList: OtherIssueAdapter.addMany(
          action.payload.issueData,
          state.otherIssueLossList
        ),
        isLoadingOtherIssuesLossList: false,
        totalElementsOtherIssues: action.payload.totalElements,
        error: null
      };

    case OtherIssuesActionTypes.LOAD_ISSUE_LOSS_LIST_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingOtherIssuesLossList: false
      };

    case OtherIssuesActionTypes.LOAD_ISSUE_PSV_LIST:
      return {
        ...state,
        isLoadingOtherIssuesPSVList: true,
        error: null
      };

    case OtherIssuesActionTypes.LOAD_ISSUE_PSV_LIST_SUCCESS:
      return {
        ...state,
        otherIssuePSVList: OtherIssueAdapter.addMany(
          action.payload.issueData,
          state.otherIssuePSVList
        ),
        isLoadingOtherIssuesPSVList: false,
        totalElementsOtherIssues: action.payload.totalElements,
        error: null
      };

    case OtherIssuesActionTypes.LOAD_ISSUE_PSV_LIST_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingOtherIssuesPSVList: false
      };
    case OtherIssuesActionTypes.RESET_ADJUSTMENT_ISSUE_DATA:
      return {
        ...state,
        AdjustmentItemsInCarts: otherIssuesCreateItemAdapter.removeAll(
          state.AdjustmentItemsInCarts
        ),
        AdjustmentItemsInCartsSearch: otherIssuesCreateItemAdapter.getInitialState(),
        createStockRequestAdjustmentResponse: {},
        error: null,
        isSearchingAdjustment: false,
        hasSearchedItemAdjustment: false,
        hasSearchItemInCartSearch: false,
        searchedAdjustmentItems: otherIssuesCreateItemAdapter.getInitialState()
      };
    case OtherIssuesActionTypes.CLEAR_SEARCH_CART_ITEM_ADJUSTMENT:
      return {
        ...state,
        AdjustmentItemsInCartsSearch: otherIssuesCreateItemAdapter.getInitialState(),
        hasSearchItemInCartSearch: false
      };
    case OtherIssuesActionTypes.CLEAR_SEARCH_INVENTORY_ADJUSTMENT:
      return {
        ...state,
        isSearchingAdjustment: false,
        hasSearchedItemAdjustment: false,
        searchedAdjustmentItems: otherIssuesCreateItemAdapter.getInitialState(),
        searchCountAdjustment: null
      };
    //psv
    case OtherIssuesActionTypes.PSV_SEARCH:
      return {
        ...state,
        isSearchingPSV: true,
        hasSearchedItemPSV: false,
        hasError: null
      };
    case OtherIssuesActionTypes.PSV_SEARCH_SUCCESS:
      return {
        ...state,
        isSearchingPSV: false,
        hasSearchedItemPSV: true,
        searchedPSVItems: otherIssuesItemAdapter.addAll(
          action.payload.items,
          state.searchedPSVItems
        ),
        searchCountPSV: action.payload.count
      };

    case OtherIssuesActionTypes.PSV_SEARCH_FAILURE:
      return {
        ...state,
        isSearchingPSV: false,
        hasSearchedItemPSV: false,
        error: action.payload
      };
    case OtherIssuesActionTypes.ADD_PSV_ITEMS_TO_CART:
      return {
        ...state,
        searchedPSVItems: otherIssuesItemAdapter.removeAll(
          state.searchedPSVItems
        ),
        PSVItemsInCarts: otherIssuesItemAdapter.addMany(
          action.payload,
          state.PSVItemsInCarts
        )
      };
    case OtherIssuesActionTypes.CREATE_STOCK_REQUEST_PSV:
      return {
        ...state,
        IsLoadingPSV: true,
        error: null
      };
    case OtherIssuesActionTypes.CREATE_STOCK_REQUEST_PSV_SUCCESS:
      return {
        ...state,
        createStockRequestPSVResponse: action.payload,
        IsLoadingPSV: false,
        error: null
      };
    case OtherIssuesActionTypes.CREATE_STOCK_REQUEST_PSV_FAILURE:
      return {
        ...state,
        error: action.payload,
        IsLoadingPSV: false
      };

    case OtherIssuesActionTypes.UPDATE_CART_ITEM_PSV:
      return {
        ...state,
        PSVItemsInCarts: otherIssuesItemAdapter.updateOne(
          {
            id: action.payload.id,
            changes: {
              measuredQuantity: action.payload.quantity,
              measuredWeight: action.payload.weight
            }
          },
          state.PSVItemsInCarts
        )
      };
    case OtherIssuesActionTypes.REMOVE_CART_ITEM_PSV:
      return {
        ...state,
        PSVItemsInCarts: otherIssuesItemAdapter.removeMany(
          action.payload.ids,
          state.PSVItemsInCarts
        ),
        PSVItemsInCartsSearch: otherIssuesItemAdapter.removeMany(
          action.payload.ids,
          state.PSVItemsInCartsSearch
        )
      };
    case OtherIssuesActionTypes.SEARCH_CART_ITEM_PSV:
      return {
        ...state,
        PSVItemsInCartsSearch: state.PSVItemsInCarts,
        hasSearchItemInCartSearchPSV: true
      };

    case OtherIssuesActionTypes.RESET_PSV_ISSUE_DATA:
      return {
        ...state,
        PSVItemsInCarts: otherIssuesCreateItemAdapter.removeAll(
          state.PSVItemsInCarts
        ),
        PSVItemsInCartsSearch: otherIssuesCreateItemAdapter.getInitialState(),
        createStockRequestPSVResponse: {},
        error: null,
        hasSearchItemInCartSearchPSV: false,
        isSearchingPSV: false,
        hasSearchedItemPSV: false,
        searchedPSVItems: otherIssuesItemAdapter.getInitialState()
      };
    case OtherIssuesActionTypes.CLEAR_SEARCH_CART_ITEM_PSV:
      return {
        ...state,
        PSVItemsInCartsSearch: otherIssuesCreateItemAdapter.getInitialState(),
        hasSearchItemInCartSearchPSV: false
      };
    case OtherIssuesActionTypes.CLEAR_SEARCH_INVENTORY_PSV:
      return {
        ...state,
        isSearchingPSV: false,
        hasSearchedItemPSV: false,
        searchedPSVItems: otherIssuesCreateItemAdapter.getInitialState(),
        searchCountPSV: null
      };
    //FOC
    case OtherIssuesActionTypes.FOC_SEARCH:
      return {
        ...state,
        isSearchingFOC: true,
        hasSearchedItemFOC: false,
        hasError: null
      };
    case OtherIssuesActionTypes.FOC_SEARCH_SUCCESS:
      return {
        ...state,
        isSearchingFOC: false,
        hasSearchedItemFOC: true,
        searchedFOCItems: otherIssuesItemAdapter.addAll(
          action.payload.items,
          state.searchedFOCItems
        ),
        searchCountFOC: action.payload.count
      };

    case OtherIssuesActionTypes.FOC_SEARCH_FAILURE:
      return {
        ...state,
        isSearchingFOC: false,
        hasSearchedItemFOC: false,
        error: action.payload
      };
    case OtherIssuesActionTypes.ADD_FOC_ITEMS_TO_CART:
      return {
        ...state,
        searchedFOCItems: otherIssuesItemAdapter.removeAll(
          state.searchedFOCItems
        ),
        FOCItemsInCarts: otherIssuesItemAdapter.addMany(
          action.payload,
          state.FOCItemsInCarts
        )
      };
    case OtherIssuesActionTypes.CREATE_STOCK_REQUEST_FOC:
      return {
        ...state,
        IsLoadingFOC: true,
        error: null
      };
    case OtherIssuesActionTypes.CREATE_STOCK_REQUEST_FOC_SUCCESS:
      return {
        ...state,
        createStockRequestFOCResponse: action.payload,
        IsLoadingFOC: false,
        error: null
      };
    case OtherIssuesActionTypes.CREATE_STOCK_REQUEST_FOC_FAILURE:
      return {
        ...state,
        error: action.payload,
        IsLoadingFOC: false
      };

    case OtherIssuesActionTypes.UPDATE_CART_ITEM_FOC:
      return {
        ...state,
        FOCItemsInCarts: otherIssuesItemAdapter.updateOne(
          {
            id: action.payload.id,
            changes: {
              measuredQuantity: action.payload.quantity,
              measuredWeight: action.payload.weight
            }
          },
          state.FOCItemsInCarts
        )
      };
    case OtherIssuesActionTypes.REMOVE_CART_ITEM_FOC:
      return {
        ...state,
        FOCItemsInCarts: otherIssuesItemAdapter.removeMany(
          action.payload.ids,
          state.FOCItemsInCarts
        ),
        FOCItemsInCartsSearch: otherIssuesItemAdapter.removeMany(
          action.payload.ids,
          state.FOCItemsInCartsSearch
        ),
        hasSearchItemInCartSearchFOC: false
      };
    case OtherIssuesActionTypes.SEARCH_CART_ITEM_FOC:
      return {
        ...state,
        FOCItemsInCartsSearch: state.FOCItemsInCarts,
        hasSearchItemInCartSearchFOC: true
      };

    case OtherIssuesActionTypes.RESET_FOC_ISSUE_DATA:
      return {
        ...state,
        FOCItemsInCarts: otherIssuesCreateItemAdapter.removeAll(
          state.FOCItemsInCarts
        ),
        FOCItemsInCartsSearch: otherIssuesCreateItemAdapter.getInitialState(),
        createStockRequestFOCResponse: {},
        error: null,
        hasSearchItemInCartSearchFOC: false,
        searchedFOCItems: otherIssuesCreateItemAdapter.getInitialState(),
        isSearchingFOC: false,
        hasSearchedItemFOC: false
      };
    case OtherIssuesActionTypes.CLEAR_SEARCH_CART_ITEM_FOC:
      return {
        ...state,
        FOCItemsInCartsSearch: otherIssuesCreateItemAdapter.getInitialState(),
        hasSearchItemInCartSearchFOC: false
      };
    case OtherIssuesActionTypes.LOAD_ISSUE_FOC_LIST:
      return {
        ...state,
        isLoadingOtherIssuesFOCList: true,
        error: null
      };

    case OtherIssuesActionTypes.LOAD_ISSUE_FOC_LIST_SUCCESS:
      return {
        ...state,
        otherIssueFOCList: OtherIssueAdapter.addMany(
          action.payload.issueData,
          state.otherIssueFOCList
        ),
        isLoadingOtherIssuesFOCList: false,
        totalElementsOtherIssues: action.payload.totalElements,
        error: null
      };

    case OtherIssuesActionTypes.LOAD_ISSUE_FOC_LIST_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingOtherIssuesFOCList: false
      };
    case OtherIssuesActionTypes.CLEAR_SEARCH_INVENTORY_FOC:
      return {
        ...state,
        isSearchingFOC: false,
        hasSearchedItemFOC: false,
        searchedFOCItems: otherIssuesCreateItemAdapter.getInitialState(),
        searchCountFOC: null
      };

    case OtherIssuesActionTypes.CANCEL_STOCK_REQUEST:
      return {
        ...state,
        IsLoadingCancelStockRequestResponse: true,
        hasError: null
      };
    case OtherIssuesActionTypes.CANCEL_STOCK_REQUEST_SUCCESS:
      return {
        ...state,
        cancelStockRequestResponse: action.payload,
        IsLoadingCancelStockRequestResponse: false,
        hasError: null
      };
    case OtherIssuesActionTypes.CANCEL_STOCK_REQUEST_FAILURE:
      return {
        ...state,
        error: action.payload,
        IsLoadingCancelStockRequestResponse: false
      };

    case OtherIssuesActionTypes.PRINT_OTHER_ISSUES:
      return {
        ...state,
        printDataResponse: null,
        error: null
      };
    case OtherIssuesActionTypes.PRINT_OTHER_ISSUES_SUCCESS:
      return {
        ...state,
        printDataResponse: action.payload,
        error: null
      };

    case OtherIssuesActionTypes.PRINT_OTHER_ISSUES_FAILURE:
    case OtherIssuesActionTypes.LOAD_ISSUES_STN_COUNT_FAILURE:
      return {
        ...state,
        error: action.payload
      };

    case OtherIssuesActionTypes.LOAD_PRODUCT_CATEGORIES_SUCCESS:
      return {
        ...state,
        productCategories: action.payload,
        isLoading: false,
        error: null
      };
    case OtherIssuesActionTypes.LOAD_PROUDCT_GROUPS:
    case OtherIssuesActionTypes.LOAD_PRODUCT_CATEGORIES:
      return {
        ...state,
        isLoading: true,
        error: null
      };
    case OtherIssuesActionTypes.LOAD_PROUDCT_GROUPS_SUCCESS:
      return {
        ...state,
        productGroups: action.payload,
        isLoading: false,
        error: null
      };
    //same4
    case OtherIssuesActionTypes.LOAD_PROUDCT_GROUPS_FAILURE:
    case OtherIssuesActionTypes.LOAD_PRODUCT_CATEGORIES_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoading: false
      };
    case OtherIssuesActionTypes.SET_FILTER_DATA_ALL_PRODUCTS:
      return {
        ...state,
        filterDataAllProducts: action.payload
      };
    case OtherIssuesActionTypes.SET_FILTER_DATA_SELECTED_PRODUCTS:
      return {
        ...state,
        filterDataSelectedProducts: action.payload
      };
    case OtherIssuesActionTypes.SET_SORT_DATA_ALL_PRODUCTS:
      return {
        ...state,
        sortDataAllProducts: action.payload
      };
    case OtherIssuesActionTypes.SET_SORT_DATA_SELECTED_PRODUCTS:
      return {
        ...state,
        sortDataSelectedProducts: action.payload
      };
    case OtherIssuesActionTypes.SET_FILTER_DATA_OTHER_ISSUE:
      return {
        ...state,
        filterDataOtherIssue: action.payload
      };
    case OtherIssuesActionTypes.SET_SORT_DATA_OTHER_ISSUE:
      return {
        ...state,
        sortDataotherIssue: action.payload
      };
  }

  return state;
}
