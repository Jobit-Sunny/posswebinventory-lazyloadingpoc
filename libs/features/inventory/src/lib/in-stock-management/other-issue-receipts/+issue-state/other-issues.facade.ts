import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../../inventory.state';
import { OtherIssuesSelector } from './other-issues.selector';
import * as OtherIssuesActions from './other-issues.actions';
import {
  LoadListItemsPayload,
  SearchPendingPayload,
  LoadOtherIssuesItemPayload,
  SearchOthreIssueItemsPayload,
  OtherIssuesCreateStockResponsePayload,
  LoadAllOtherIssuePayload,
  LoadOtherIssueCreateItemsTotalCountPayload
} from './other-issues.actions';
import { OtherIssuesItem } from '../models/other-issues.model';
import { Filter, Column } from '@poss-web/shared';

@Injectable()
export class OtherIssuesFacade {
  private hasError$ = this.store.select(OtherIssuesSelector.selectHasError);

  private isLoading$ = this.store.select(OtherIssuesSelector.selectIsLoading);

  private otherIssuesSTNCount$ = this.store.select(
    OtherIssuesSelector.selectOtherIssuesSTNCount
  );

  private issuesList$ = this.store.select(
    OtherIssuesSelector.selectPendingIssuesList
  );
  private issuesLoanList$ = this.store.select(
    OtherIssuesSelector.selectPendingIssuesLoanList
  );

  private isLoadingOtherIssuesSTN$ = this.store.select(
    OtherIssuesSelector.selectIsLoadingOtherIssuesSTN
  );

  private searchOtherIssueStockResults$ = this.store.select(
    OtherIssuesSelector.selectOtherIssueStockResults
  );

  private otherIssuesDropDown$ = this.store.select(
    OtherIssuesSelector.selectOtherIssuesDropDown
  );
  private getSelectedIssueDropDownvalue$ = this.store.select(
    OtherIssuesSelector.selectOtherissuesSelectedDropDown
  );
  private selectedIssue$ = this.store.select(
    OtherIssuesSelector.selectSelectedIssue
  );
  private selectTotalIssueCount$ = this.store.select(
    OtherIssuesSelector.selectTotalIssueCount
  );
  private isSearchingStocks$ = this.store.select(
    OtherIssuesSelector.selectIsSearchingStocks
  );
  private hasSearchStockResults$ = this.store.select(
    OtherIssuesSelector.selectHasSearchStockResults
  );
  private nonVerifiedOtherIssueItems$ = this.store.select(
    OtherIssuesSelector.selectNonVerifiedOtherIssueItems
  );
  private isLoadingSelectedIssueStock$ = this.store.select(
    OtherIssuesSelector.selectIsLoadingSelectedIssueStock
  );
  private hasSearchedOtherIssueItems$ = this.store.select(
    OtherIssuesSelector.selectHasSearchedOtherIssueItems
  );
  private isSearchingOtherIssueItems$ = this.store.select(
    OtherIssuesSelector.selectIsSearchingOtherIssueItems
  );
  private createOtherIssueStockRequestResponse$ = this.store.select(
    OtherIssuesSelector.createOtherIssueStockRequestResponse
  );
  private allOtherIssueCreateItems$ = this.store.select(
    OtherIssuesSelector.selectAllOtherIssuesCreateItems
  );
  private selectedOtherIssueCreateItems$ = this.store.select(
    OtherIssuesSelector.selectSelectedOtherIssuesCreateItems
  );
  private allOtherIssueCreateItemsTotalCount$ = this.store.select(
    OtherIssuesSelector.selectAllOtherIssueCreateItemsTotalCount
  );
  private SelectedOtherIssueCreateTotalCount$ = this.store.select(
    OtherIssuesSelector.selectSelectedOtherIssueCreateTotalCount
  );

  private selectIsAllOtherIssueCreateItemsLoading$ = this.store.select(
    OtherIssuesSelector.selectIsAllOtherIssueCreateItemsLoading
  );
  private selectIsSelectedOtherIssueItemsLoading$ = this.store.select(
    OtherIssuesSelector.selectIsSelectedOtherIssueItemsLoading
  );

  private selectIsOtherIssueCreateTotalCountLoaded$ = this.store.select(
    OtherIssuesSelector.selectIsOtherIssueCreateTotalCountLoaded
  );
  private selectIsOtherIssueCreateTotalCountLoading$ = this.store.select(
    OtherIssuesSelector.selectIsOtherIssueCreateTotalCountLoading
  );

  private hasSearchedOtherIssueCreateItems$ = this.store.select(
    OtherIssuesSelector.selectHasSearchedOtherIssueCreateItems
  );
  private isSearchingOtherIssueCreateItems$ = this.store.select(
    OtherIssuesSelector.selectIsSearchingOtherIssueCreateItems
  );
  private createStockRequestItemsResponse$ = this.store.select(
    OtherIssuesSelector.selectCreateStockRequestItemsResponse
  );
  private removeOtherIssueStockRequestItemsResponse$ = this.store.select(
    OtherIssuesSelector.selectRemoveOtherIssueStockRequestItemsResponse
  );
  private updateStockRequestResponse$ = this.store.select(
    OtherIssuesSelector.selectupdateStockRequestResponse
  );
  private createOtherStockIssueItemsResponse$ = this.store.select(
    OtherIssuesSelector.selectCreateOtherStockIssueItemsResponse
  );
  private confirmOtherStockIssueResponse$ = this.store.select(
    OtherIssuesSelector.selectConfirmOtherStockIssueResponse
  );
  private isLoadingOtherIssueDetails$ = this.store.select(
    OtherIssuesSelector.selectisLoadingOtherIssueDetails
  );
  private searchedAdjustmentItems$ = this.store.select(
    OtherIssuesSelector.selectSearchedAdjustmentItems
  );
  private adjustmentItemsInCarts$ = this.store.select(
    OtherIssuesSelector.selectAdjustmentItemsInCart
  );
  private issuesADJList$ = this.store.select(
    OtherIssuesSelector.selectPendingIssuesADJList
  );
  private issuesLossList$ = this.store.select(
    OtherIssuesSelector.selectPendingIssuesLossList
  );
  private issuesPSVList$ = this.store.select(
    OtherIssuesSelector.selectPendingIssuesPSVList
  );
  private createStockRequestAdjustmentResponse$ = this.store.select(
    OtherIssuesSelector.selectcreateStockRequestAdjustmentResponse
  );
  private selectAdjustmentItemsInCartsSearch$ = this.store.select(
    OtherIssuesSelector.selectAdjustmentItemsInCartsSearch
  );
  private isSearchingAdjustment$ = this.store.select(
    OtherIssuesSelector.selectIsSearchingAdjustment
  );
  private hasSearchedItemAdjustment$ = this.store.select(
    OtherIssuesSelector.selectHasSearchedItemAdjustment
  );
  private IsLoadingAdjustment$ = this.store.select(
    OtherIssuesSelector.selectIsLoadingAdjustment
  );
  private hasSearchItemInCartSearch$ = this.store.select(
    OtherIssuesSelector.selectHasSearchItemInCartSearch
  );
  //psv
  private searchedPSVItems$ = this.store.select(
    OtherIssuesSelector.selectSearchedPSVItems
  );
  private PSVItemsInCarts$ = this.store.select(
    OtherIssuesSelector.selectPSVItemsInCart
  );
  private createStockRequestPSVResponse$ = this.store.select(
    OtherIssuesSelector.selectcreateStockRequestPSVResponse
  );
  private selectPSVItemsInCartsSearch$ = this.store.select(
    OtherIssuesSelector.selectPSVItemsInCartsSearch
  );
  private isSearchingPSV$ = this.store.select(
    OtherIssuesSelector.selectIsSearchingPSV
  );
  private hasSearchedItemPSV$ = this.store.select(
    OtherIssuesSelector.selectHasSearchedItemPSV
  );
  private IsLoadingPSV$ = this.store.select(
    OtherIssuesSelector.selectIsLoadingPSV
  );
  private hasSearchItemInCartSearchPSV$ = this.store.select(
    OtherIssuesSelector.selectHasSearchItemInCartSearchPSV
  );
  private error$ = this.store.select(OtherIssuesSelector.selectError);

  //FOC
  private searchedFOCItems$ = this.store.select(
    OtherIssuesSelector.selectSearchedFOCItems
  );
  private FOCItemsInCarts$ = this.store.select(
    OtherIssuesSelector.selectFOCItemsInCart
  );
  private createStockRequestFOCResponse$ = this.store.select(
    OtherIssuesSelector.selectcreateStockRequestFOCResponse
  );
  private selectFOCItemsInCartsSearch$ = this.store.select(
    OtherIssuesSelector.selectFOCItemsInCartsSearch
  );

  private issuesFOCList$ = this.store.select(
    OtherIssuesSelector.selectPendingIssuesFOCList
  );
  private isSearchingFOC$ = this.store.select(
    OtherIssuesSelector.selectIsSearchingFOC
  );
  private hasSearchedItemFOC$ = this.store.select(
    OtherIssuesSelector.selectHasSearchedItemFOC
  );
  private IsLoadingFOC$ = this.store.select(
    OtherIssuesSelector.selectIsLoadingFOC
  );
  private hasSearchItemInCartSearchFOC$ = this.store.select(
    OtherIssuesSelector.selectHasSearchItemInCartSearchFOC
  );

  private isLoadingCancelStockRequestResponse$ = this.store.select(
    OtherIssuesSelector.selectIsLoadingCancelStockRequestResponse
  );
  private cancelOtherStockRequestResponse$ = this.store.select(
    OtherIssuesSelector.selectCancelOtherStockRequestResponse
  );
  private printDataResponse$ = this.store.select(
    OtherIssuesSelector.selectprintDataResponse
  );

  private productCategories$ = this.store.select(
    OtherIssuesSelector.selectProductCategories
  );

  private productGroups$ = this.store.select(
    OtherIssuesSelector.selectProductGroups
  );
  private filterDataAllProducts$ = this.store.select(
    OtherIssuesSelector.selectfilterDataAllProducts
  );

  private filterDataSelectedProducts$ = this.store.select(
    OtherIssuesSelector.selectfilterDataSelectedProducts
  );
  private sortDataAllProducts$ = this.store.select(
    OtherIssuesSelector.selectSortDataAllProducts
  );

  private sortDataSelectedProducts$ = this.store.select(
    OtherIssuesSelector.selectSortDataSelectedProducts
  );

  private filterDataOtherIssue$ = this.store.select(
    OtherIssuesSelector.selectfilterDataOtherIssue
  );
  private sortDataOtherIssue$ = this.store.select(
    OtherIssuesSelector.selectSortDataOtherIssue
  );
  resetOtherIssueListData() {
    this.store.dispatch(new OtherIssuesActions.ResetIssueListData());
  }
  searchPendingIssuesStocks(searchPendingpayload: SearchPendingPayload) {
    this.store.dispatch(
      new OtherIssuesActions.SearchPendingIssue(searchPendingpayload)
    );
  }

  loadOtherIssuesCount() {
    this.store.dispatch(new OtherIssuesActions.LoadIssuesSTNCount());
  }
  loadIssueList(loadIssueListPayload: LoadListItemsPayload) {
    this.store.dispatch(
      new OtherIssuesActions.LoadIssueList(loadIssueListPayload)
    );
  }
  loadIssueLoanList(loadIssueListPayload: LoadListItemsPayload) {
    this.store.dispatch(
      new OtherIssuesActions.LoadIssueLoanList(loadIssueListPayload)
    );
  }
  setSelectedDropDownForIssues(selectedDropdownValue: string) {
    this.store.dispatch(
      new OtherIssuesActions.DropDownvalueForIssues(selectedDropdownValue)
    );
  }
  searchIssueClear() {
    this.store.dispatch(new OtherIssuesActions.SearchClearIssue());
  }
  getHasError() {
    return this.hasError$;
  }

  getIsLoading() {
    return this.isLoading$;
  }

  getOtherIssuesSTNCount() {
    return this.otherIssuesSTNCount$;
  }

  getOtherIssueList() {
    return this.issuesList$;
  }
  getOtherIssueLoanList() {
    return this.issuesLoanList$;
  }

  getIsLoadingOtherIssuesSTN() {
    return this.isLoadingOtherIssuesSTN$;
  }
  getOtherIssueSearchStockResults() {
    return this.searchOtherIssueStockResults$;
  }
  getOtherIssuesDropdown() {
    return this.otherIssuesDropDown$;
  }
  getSelectedIssueDropDownValue() {
    return this.getSelectedIssueDropDownvalue$;
  }
  getSelectedIssue() {
    return this.selectedIssue$;
  }

  loadSelectedIssue(
    loadSelectedIssuePayload: OtherIssuesActions.LoadSelectedPayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.LoadSelectedIssue(loadSelectedIssuePayload)
    );
  }
  getTotalIssueElementCount() {
    return this.selectTotalIssueCount$;
  }
  getIsSearchingStocks() {
    return this.isSearchingStocks$;
  }

  getHasSearchStockResults() {
    return this.hasSearchStockResults$;
  }
  getNonVerifiedOtherissueItems() {
    return this.nonVerifiedOtherIssueItems$;
  }
  loadNonVerifiedItems(loadItemsPayload: LoadOtherIssuesItemPayload) {
    this.store.dispatch(
      new OtherIssuesActions.LoadNonVerifiedOtherIssueItems(loadItemsPayload)
    );
  }
  getisLoadingSelectedIssueStock() {
    return this.isLoadingSelectedIssueStock$;
  }

  getHasSearchedOtherIssueItems() {
    return this.hasSearchedOtherIssueItems$;
  }

  getIsSearchingOtherIssueItems() {
    return this.isSearchingOtherIssueItems$;
  }

  createOtherStockIssueItems(
    createOtherStockIssueItemsPayload: OtherIssuesActions.CreateOtherStockIssueItemsPayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.CreateOtherStockIssueItems(
        createOtherStockIssueItemsPayload
      )
    );
  }
  getCreateOtherStockIssueItemsResponse() {
    return this.createOtherStockIssueItemsResponse$;
  }
  removeInitialLoadOtherIssue() {
    this.store.dispatch(new OtherIssuesActions.RemoveInitialLoadOtherIssue());
  }

  confirmOtherStockIssue(
    confirmOtherStockIssuePayload: OtherIssuesActions.ConfirmOtherStockIssuePayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.ConfirmOtherStockIssue(
        confirmOtherStockIssuePayload
      )
    );
  }
  getConfirmOtherStockIssueResponse() {
    return this.confirmOtherStockIssueResponse$;
  }
  getisLoadingOtherIssueDetails() {
    return this.isLoadingOtherIssueDetails$;
  }
  resetConfirmOtherIssueResponse() {
    this.store.dispatch(
      new OtherIssuesActions.ResetConfirmOtherIssueResponse()
    );
  }
  // create page
  createOtherIssuesStockRequest(
    createresponsePayload: OtherIssuesCreateStockResponsePayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.CreateOtherIssueStockRequest(createresponsePayload)
    );
  }
  getOtherIssuesStockRequest() {
    return this.createOtherIssueStockRequestResponse$;
  }

  loadAllOtherIssueCreateItems(loadItemsPayload: LoadAllOtherIssuePayload) {
    this.store.dispatch(
      new OtherIssuesActions.LoadAllOtherIssueCreateItems(loadItemsPayload)
    );
  }

  loadSelectedOtherIssueCreateItems(
    loadItemsPayload: LoadAllOtherIssuePayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.LoadSelectedOtherIssueCreateItems(loadItemsPayload)
    );
  }
  getAllOtherIssuesCreateItems() {
    return this.allOtherIssueCreateItems$;
  }
  getSelecetedIssuesCreateItems() {
    return this.selectedOtherIssueCreateItems$;
  }

  loadOtherIssueItemsCreateTotalCount(
    loadIssueItemsTotalCountPayload: LoadOtherIssueCreateItemsTotalCountPayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.LoadIssueItemsCreateTotalCount(
        loadIssueItemsTotalCountPayload
      )
    );
  }
  getallOtherIssueCreateItemsTotalCount() {
    return this.allOtherIssueCreateItemsTotalCount$;
  }
  getSelectedOtherIssueCreateTotalCount() {
    return this.SelectedOtherIssueCreateTotalCount$;
  }

  getIsAllOtherIssueCreateItemsLoading() {
    return this.selectIsAllOtherIssueCreateItemsLoading$;
  }
  getselectIsSelectedOtherIssueItemsLoading() {
    return this.selectIsSelectedOtherIssueItemsLoading$;
  }
  getselectIsOtherIssueCreateTotalCountLoaded() {
    return this.selectIsOtherIssueCreateTotalCountLoaded$;
  }
  getselectIsOtherIssueCreateTotalCountLoading() {
    return this.selectIsOtherIssueCreateTotalCountLoading$;
  }

  gethasSearchedOtherIssueCreateItems() {
    return this.hasSearchedOtherIssueCreateItems$;
  }
  getisSearchingOtherIssueCreateItems() {
    return this.isSearchingOtherIssueCreateItems$;
  }

  createOtherIssueStockRequestItems(
    createStockIssueItemsPayload: OtherIssuesActions.CreateOtherIssueStockRequestItemsPayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.CreateOtherIssueStockRequestItems(
        createStockIssueItemsPayload
      )
    );
  }
  removeOtherIssueStockRequestItems(
    createStockIssueItemsPayload: OtherIssuesActions.RemoveOtherIssueStockRequestItemsPayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.RemoveOtherIssueStockRequestItems(
        createStockIssueItemsPayload
      )
    );
  }
  updateStockRequestCreateItem(
    updateStockIssueItemPayload: OtherIssuesActions.UpdateStockRequestItemPayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.UpdateStockRequestCreateItem(
        updateStockIssueItemPayload
      )
    );
  }
  getCreateStockRequestItemsResponse() {
    return this.createStockRequestItemsResponse$;
  }
  resetOtherIssueCreateListItems() {
    this.store.dispatch(
      new OtherIssuesActions.ResetOtherIssueCreateListItems()
    );
  }
  resetOtherIssueCreateResponse() {
    this.store.dispatch(new OtherIssuesActions.ResetOtherIssueCreateResponse());
  }
  getRemoveOtherIssueStockRequestItemsResponse() {
    return this.removeOtherIssueStockRequestItemsResponse$;
  }
  getupdateStockRequestResponse() {
    return this.updateStockRequestResponse$;
  }
  updateStockRequest(
    updateStockRequestPayload: OtherIssuesActions.UpdateStockRequestPayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.UpdateStockRequest(updateStockRequestPayload)
    );
  }
  /**
   * Dispatch an Action for searching Items
   * @param searchItemPayload:Payload for serach item
   */
  SearchAdjustmentItems(
    searchItemPayload: OtherIssuesActions.AdjustmentSearchItemPayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.SearchAdjustment(searchItemPayload)
    );
  }
  getSearchedAdjustmentItems() {
    return this.searchedAdjustmentItems$;
  }
  getpAdjustmentItemsInCarts() {
    return this.adjustmentItemsInCarts$;
  }
  /**
   * Dispatch an Action to additems to cart
   * @param items :Payload of items
   */
  addItemsToCart(items: OtherIssuesItem[]) {
    this.store.dispatch(new OtherIssuesActions.AddItemsToCart(items));
  }
  createStockRequestAdjustment(
    createStockRequestAdjustmentPayload: OtherIssuesActions.CreateStockRequestAdjustmentPayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.CreateStockRequestAdjustment(
        createStockRequestAdjustmentPayload
      )
    );
  }
  /**
   * Dispatch an Action for update cart Item
   * @param updateItemPayload : payload to update
   */
  updateCartItem(
    updateCartItemPayload: OtherIssuesActions.UpdateCartItemAdjustmentPayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.UpdateCartItemsAdjustment(updateCartItemPayload)
    );
  }
  removeSelectedItems(
    removeItems: OtherIssuesActions.RemoveCartItemAdjustmentPayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.RemoveCartItemsAdjustment(removeItems)
    );
  }
  SearchAdjustmentCartItems(
    searchItems: OtherIssuesActions.SearchCartItemAdjustmentPayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.SearchCartItemsAdjustment(searchItems)
    );
  }
  loadIssueADJList(loadIssueListPayload: LoadListItemsPayload) {
    this.store.dispatch(
      new OtherIssuesActions.LoadIssueADJList(loadIssueListPayload)
    );
  }
  loadIssueLossList(loadIssueListPayload: LoadListItemsPayload) {
    this.store.dispatch(
      new OtherIssuesActions.LoadIssueLossList(loadIssueListPayload)
    );
  }
  loadIssuePSVList(loadIssueListPayload: LoadListItemsPayload) {
    this.store.dispatch(
      new OtherIssuesActions.LoadIssuePSVList(loadIssueListPayload)
    );
  }
  clearSearchCartItemAdjustment() {
    this.store.dispatch(new OtherIssuesActions.ClearSearchCartItemAdjustment());
  }
  clearSearchInventoryItemAdjustment() {
    this.store.dispatch(
      new OtherIssuesActions.ClearSearchInventoryItemAdjustment()
    );
  }
  getOtherIssueADJList() {
    return this.issuesADJList$;
  }
  getOtherIssueLossList() {
    return this.issuesLossList$;
  }
  getOtherIssuePSVList() {
    return this.issuesPSVList$;
  }
  getCreateStockRequestAdjustmentResponse() {
    return this.createStockRequestAdjustmentResponse$;
  }
  resetAdjustmentIssueData() {
    this.store.dispatch(new OtherIssuesActions.ResetAdjustmentIssueData());
  }
  getAdjustmentItemsInCartsSearch() {
    return this.selectAdjustmentItemsInCartsSearch$;
  }
  getIsSearchingAdjustment() {
    return this.isSearchingAdjustment$;
  }
  gethasSearchedItemAdjustment() {
    return this.hasSearchedItemAdjustment$;
  }
  getIsLoadingAdjustment() {
    return this.IsLoadingAdjustment$;
  }
  getHasSearchItemInCartSearch() {
    return this.hasSearchItemInCartSearch$;
  }
  //psv
  /**
   * Dispatch an Action for searching Items
   * @param searchItemPayload:Payload for serach item
   */
  SearchPSVItems(searchItemPayload: OtherIssuesActions.PSVSearchItemPayload) {
    this.store.dispatch(new OtherIssuesActions.SearchPSV(searchItemPayload));
  }
  getSearchedPSVItems() {
    return this.searchedPSVItems$;
  }
  getPSVItemsInCarts() {
    return this.PSVItemsInCarts$;
  }
  /**
   * Dispatch an Action to additems to cart
   * @param items :Payload of items
   */
  addPSVItemsToCart(items: OtherIssuesItem[]) {
    this.store.dispatch(new OtherIssuesActions.AddPSVItemsToCart(items));
  }
  createStockRequestPSV(
    createStockRequestPSVPayload: OtherIssuesActions.CreateStockRequestPSVPayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.CreateStockRequestPSV(createStockRequestPSVPayload)
    );
  }
  /**
   * Dispatch an Action for update cart Item
   * @param updateItemPayload : payload to update
   */
  updatePSVCartItem(
    updateCartItemPayload: OtherIssuesActions.UpdateCartItemPSVPayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.UpdateCartItemsPSV(updateCartItemPayload)
    );
  }
  removeSelectedPSVItems(
    removeItems: OtherIssuesActions.RemoveCartItemPSVPayload
  ) {
    this.store.dispatch(new OtherIssuesActions.RemoveCartItemsPSV(removeItems));
  }
  SearchPSVCartItems(searchItems: OtherIssuesActions.SearchCartItemPSVPayload) {
    this.store.dispatch(new OtherIssuesActions.SearchCartItemsPSV(searchItems));
  }
  clearSearchCartItemPSV() {
    this.store.dispatch(new OtherIssuesActions.ClearSearchCartItemPSV());
  }
  getCreateStockRequestPSVResponse() {
    return this.createStockRequestPSVResponse$;
  }
  resetPSVIssueData() {
    this.store.dispatch(new OtherIssuesActions.ResetPSVIssueData());
  }
  getPSVItemsInCartsSearch() {
    return this.selectPSVItemsInCartsSearch$;
  }
  getError() {
    return this.error$;
  }
  getIsSearchingPSV() {
    return this.isSearchingPSV$;
  }
  gethasSearchedItemPSV() {
    return this.hasSearchedItemPSV$;
  }
  getIsLoadingPSV() {
    return this.IsLoadingPSV$;
  }
  getHasSearchItemInCartSearchPSV() {
    return this.hasSearchItemInCartSearchPSV$;
  }
  clearSearchInventoryItemPSV() {
    this.store.dispatch(new OtherIssuesActions.ClearSearchInventoryItemPSV());
  }
  //FOC
  /**
   * Dispatch an Action for searching Items
   * @param searchItemPayload:Payload for serach item
   */
  SearchFOCItems(searchItemPayload: OtherIssuesActions.PSVSearchItemPayload) {
    this.store.dispatch(new OtherIssuesActions.SearchFOC(searchItemPayload));
  }
  getSearchedFOCItems() {
    return this.searchedFOCItems$;
  }
  getFOCItemsInCarts() {
    return this.FOCItemsInCarts$;
  }
  /**
   * Dispatch an Action to additems to cart
   * @param items :Payload of items
   */
  addFOCItemsToCart(items: OtherIssuesItem[]) {
    this.store.dispatch(new OtherIssuesActions.AddFOCItemsToCart(items));
  }
  createStockRequestFOC(
    createStockRequestFOCPayload: OtherIssuesActions.CreateStockRequestPSVPayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.CreateStockRequestFOC(createStockRequestFOCPayload)
    );
  }
  /**
   * Dispatch an Action for update cart Item
   * @param updateItemPayload : payload to update
   */
  updateFOCCartItem(
    updateCartItemPayload: OtherIssuesActions.UpdateCartItemPSVPayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.UpdateCartItemsFOC(updateCartItemPayload)
    );
  }
  removeSelectedFOCItems(
    removeItems: OtherIssuesActions.RemoveCartItemPSVPayload
  ) {
    this.store.dispatch(new OtherIssuesActions.RemoveCartItemsFOC(removeItems));
  }
  SearchFOCCartItems(searchItems: OtherIssuesActions.SearchCartItemPSVPayload) {
    this.store.dispatch(new OtherIssuesActions.SearchCartItemsFOC(searchItems));
  }
  clearSearchCartItemFOC() {
    this.store.dispatch(new OtherIssuesActions.ClearSearchCartItemFOC());
  }
  getCreateStockRequestFOCResponse() {
    return this.createStockRequestFOCResponse$;
  }
  resetFOCIssueData() {
    this.store.dispatch(new OtherIssuesActions.ResetFOCIssueData());
  }
  getFOCItemsInCartsSearch() {
    return this.selectFOCItemsInCartsSearch$;
  }
  getOtherIssueFOCList() {
    return this.issuesFOCList$;
  }
  loadIssueFOCList(loadIssueListPayload: LoadListItemsPayload) {
    this.store.dispatch(
      new OtherIssuesActions.LoadIssueFOCList(loadIssueListPayload)
    );
  }
  getIsSearchingFOC() {
    return this.isSearchingFOC$;
  }
  gethasSearchedItemFOC() {
    return this.hasSearchedItemFOC$;
  }
  getIsLoadingFOC() {
    return this.IsLoadingFOC$;
  }
  getHasSearchItemInCartSearchFOC() {
    return this.hasSearchItemInCartSearchFOC$;
  }
  clearSearchInventoryItemFOC() {
    this.store.dispatch(new OtherIssuesActions.ClearSearchInventoryItemFOC());
  }
  getIsLoadingCancelStockRequestResponse() {
    return this.isLoadingCancelStockRequestResponse$;
  }
  getCancelOtherStockRequestResponse() {
    return this.cancelOtherStockRequestResponse$;
  }
  cancelOtherStockRequestResponse(
    cancelOtherRequestPayload: OtherIssuesActions.CancelOtherRequestPayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.CancelStockRequest(cancelOtherRequestPayload)
    );
  }
  printOtherIssue(
    printOtherIssuePayload: OtherIssuesActions.PrintOtherIssuePayload
  ) {
    this.store.dispatch(
      new OtherIssuesActions.PrintOtherIssues(printOtherIssuePayload)
    );
  }
  getPrintDataResponse() {
    return this.printDataResponse$;
  }
  loadProductCategories() {
    this.store.dispatch(new OtherIssuesActions.LoadProductCategories());
  }

  getProductCategories() {
    return this.productCategories$;
  }

  loadProductGroups() {
    this.store.dispatch(new OtherIssuesActions.LoadProductGroups());
  }

  getProductGroups() {
    return this.productGroups$;
  }
  setOtherIssueAllProductsFilter(filterData: { [key: string]: Filter[] }) {
    this.store.dispatch(
      new OtherIssuesActions.SetFilterDataAllProducts(filterData)
    );
  }
  setOtherIssueSelectedProductsFilter(filterData: { [key: string]: Filter[] }) {
    this.store.dispatch(
      new OtherIssuesActions.SetFilterDataSelectedProducts(filterData)
    );
  }
  getfilterDataAllProducts() {
    return this.filterDataAllProducts$;
  }
  getfilterDataSelectedProducts() {
    return this.filterDataSelectedProducts$;
  }
  setOtherIssueAllProductsSort(sortData: Column[]) {
    this.store.dispatch(
      new OtherIssuesActions.SetSortDataAllProducts(sortData)
    );
  }
  setOtherIssueSelectedProductsSort(sortData: Column[]) {
    this.store.dispatch(
      new OtherIssuesActions.SetSortDataSelectedProducts(sortData)
    );
  }

  getSortDataAllProducts() {
    return this.sortDataAllProducts$;
  }
  getSortDataSelectedProducts() {
    return this.sortDataSelectedProducts$;
  }

  setOtherIssueFilter(filterData: { [key: string]: Filter[] }) {
    this.store.dispatch(
      new OtherIssuesActions.SetFilterDataOtherIssue(filterData)
    );
  }
  setOtherIssueSort(sortData: Column[]) {
    this.store.dispatch(new OtherIssuesActions.SetSortDataOtherIssue(sortData));
  }
  getFilterDataOtherIssue() {
    return this.filterDataOtherIssue$;
  }
  getSortDataOtherIssue() {
    return this.sortDataOtherIssue$;
  }
  constructor(private store: Store<State>) {}
}
