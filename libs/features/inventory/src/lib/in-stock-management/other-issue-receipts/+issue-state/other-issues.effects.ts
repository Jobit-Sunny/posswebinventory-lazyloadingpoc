import { Effect } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { DataPersistence } from '@nrwl/angular';
import {
  OtherIssuesActionTypes,
  LoadIssuesSTNCount,
  LoadIssuesSTNCountSuccess,
  LoadIssuesSTNCountFailure,
  LoadOtherIssueCreateItemsTotalCountSuccessPayload
} from './other-issues.actions';
import * as otherRecieptissuesActions from './other-issues.actions';
import { map } from 'rxjs/operators';
import {
  NotificationService,
  Errors,
  CustomErrors,
  ProductGroupDataService,
  ProductCategoryDataService,
  ProductCategory,
  ProductGroup
} from '@poss-web/core';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { CustomErrorAdaptor } from '@poss-web/core';
import {
  OtherReceiptsIssuesEnum,
  ProductGroupEnum
} from '../../../in-stock-management/in-stock/in-stock.enum';
import {
  OtherIssuedataModel,
  OtherIssueModel,
  OtherIssuesItem,
  OtherIssuesCreateStockResponse,
  RequestOtherIssueStockTransferNote
} from '../models/other-issues.model';
import { OtherReceiptIssueService } from '../services/other-receipts-issue.service';
import { OtherIssuesState } from './other-issues.state';
import { Filter } from '@poss-web/shared';

@Injectable()
export class OtherIssuesEffect {
  //v2
  @Effect() loadOtherIssuesCount$ = this.dataPersistence.fetch(
    OtherIssuesActionTypes.LOAD_ISSUES_STN_COUNT,
    {
      // provides an action and the current state of the store
      run: (action: LoadIssuesSTNCount, state: OtherIssuesState) => {
        return this.otherReceiptIssueService
          .getOtherIssuesSTNCount()
          .pipe(
            map(
              (
                data: otherRecieptissuesActions.LoadOtherIssuesSTNCountPayload
              ) => new LoadIssuesSTNCountSuccess(data)
            )
          );
      },

      onError: (action: LoadIssuesSTNCount, error: HttpErrorResponse) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new LoadIssuesSTNCountFailure(this.errorHandler(error));
      }
    }
  );

  @Effect() loadIssueList$: Observable<Action> = this.dataPersistence.fetch(
    OtherIssuesActionTypes.LOAD_ISSUE_LIST,
    {
      run: (action: otherRecieptissuesActions.LoadIssueList) => {
        return this.otherReceiptIssueService
          .getIssueList(
            action.payload.type,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (stockTransferNotes: OtherIssuedataModel) =>
                new otherRecieptissuesActions.LoadIssueListSuccess(
                  stockTransferNotes
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadIssueList,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.LoadIssueListFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadIssueLoanList$: Observable<Action> = this.dataPersistence.fetch(
    OtherIssuesActionTypes.LOAD_ISSUE_LOAN_LIST,
    {
      run: (action: otherRecieptissuesActions.LoadIssueLoanList) => {
        return this.otherReceiptIssueService
          .getIssueList(
            action.payload.type,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (stockTransferNotes: OtherIssuedataModel) =>
                new otherRecieptissuesActions.LoadIssueLoanListSuccess(
                  stockTransferNotes
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadIssueLoanList,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.LoadIssueLoanListFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() searchPendingIssuesStocks$ = this.dataPersistence.fetch(
    OtherIssuesActionTypes.SEARCH_PENDING_ISSUE,
    {
      run: (action: otherRecieptissuesActions.SearchPendingIssue) => {
        return this.otherReceiptIssueService
          .searchIssueStocks(action.payload.srcDocnumber, action.payload.type)
          .pipe(
            map(
              (searchresult: OtherIssueModel[]) =>
                new otherRecieptissuesActions.SearchPendingIssueSuccess(
                  searchresult
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.SearchPendingIssue,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.SearchPendingIssueFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadSelectedIssue$ = this.dataPersistence.fetch(
    OtherIssuesActionTypes.LOAD_SELECTED_ISSUE,
    {
      run: (action: otherRecieptissuesActions.LoadSelectedIssue) => {
        return this.otherReceiptIssueService
          .getOtherStockIssue(action.payload.reqDocNo, action.payload.type)
          .pipe(
            map(
              (
                requestStockTransferNoteData: RequestOtherIssueStockTransferNote
              ) =>
                new otherRecieptissuesActions.LoadSelectedIssueSuccess(
                  requestStockTransferNoteData
                )
            )
          );
      },
      onError: (
        action: otherRecieptissuesActions.LoadSelectedIssue,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new otherRecieptissuesActions.LoadSelectedIssueFailure(
          error.message
        );
      }
    }
  );
  @Effect()
  loadNonVerifiedOtherIssueItems$ = this.dataPersistence.fetch(
    OtherIssuesActionTypes.LOAD_NON_VERIFIED_OTHER_ISSUE_ITEMS,
    {
      run: (
        action: otherRecieptissuesActions.LoadNonVerifiedOtherIssueItems
      ) => {
        return this.otherReceiptIssueService
          .getOtherIssuesItems(
            action.payload.id,
            action.payload.status,
            action.payload.pageIndex,
            action.payload.pageSize,
            action.payload.type,
            action.payload.itemCode,
            action.payload.lotNumber,
            action.payload.sort,
            action.payload.filter
          )
          .pipe(
            map(
              (items: OtherIssuesItem[]) =>
                new otherRecieptissuesActions.LoadNonVerifiedOtherIssueItemsSuccess(
                  items
                )
            )
          );
      },
      onError: (
        action: otherRecieptissuesActions.LoadNonVerifiedOtherIssueItems,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.LoadNonVerifiedOtherIssueItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect() createOtherStockIssueItems = this.dataPersistence.fetch(
    OtherIssuesActionTypes.CREATE_OTHER_STOCK_ISSUE_ITEMS,
    {
      run: (
        action: otherRecieptissuesActions.CreateOtherStockIssueItems,
        state: OtherIssuesState
      ) => {
        return this.otherReceiptIssueService
          .CreateOtherStockIssueItems(
            action.payload.id,
            action.payload.data,
            action.payload.transferType
          )
          .pipe(
            map(
              (data: any) =>
                new otherRecieptissuesActions.CreateOtherStockIssueItemsItemsSuccess(
                  data
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.CreateOtherStockIssueItems,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.CreateOtherStockIssueItemsItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect() confirmOtherStockIssue = this.dataPersistence.fetch(
    OtherIssuesActionTypes.CONFIRM_OTHER_STOCK_ISSUE,
    {
      run: (action: otherRecieptissuesActions.ConfirmOtherStockIssue) => {
        return this.otherReceiptIssueService
          .confirmOtherStockIssue(
            action.payload.id,
            action.payload.transferType,
            action.payload.carrierDetails,
            action.payload.remarks,
            action.payload.destinationLocationCode
          )
          .pipe(
            map(
              (data: any) =>
                new otherRecieptissuesActions.ConfirmOtherStockIssueSuccess(
                  data
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.ConfirmOtherStockIssue,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.ConfirmOtherStockIssueFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  //create page
  @Effect()
  createOtherIssuesStockRequest$ = this.dataPersistence.fetch(
    OtherIssuesActionTypes.CREATE_OTHER_ISSUE_STOCK_REQUEST,
    {
      run: (action: otherRecieptissuesActions.CreateOtherIssueStockRequest) => {
        return this.otherReceiptIssueService
          .createOtherIssuesStockRequest(action.payload.reqtype)
          .pipe(
            map(
              (data: OtherIssuesCreateStockResponse) =>
                new otherRecieptissuesActions.CreateOtherIssueStockRequestSuccess(
                  data
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.CreateOtherIssueStockRequest,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.CreateOtherIssueStockRequestFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadAllOtherIssueCreateItems$ = this.dataPersistence.fetch(
    OtherIssuesActionTypes.LOAD_ALL_OTHER_ISSUE_CREATE_ITEMS,
    {
      run: (action: otherRecieptissuesActions.LoadAllOtherIssueCreateItems) => {
        return this.otherReceiptIssueService
          .getOtherIssueCreateItems(
            action.payload.id,
            OtherReceiptsIssuesEnum.OPEN,
            action.payload.pageIndex,
            action.payload.pageSize,
            action.payload.reqtype,
            action.payload.itemCode,
            action.payload.lotNumber,
            action.payload.sort,
            action.payload.filter
          )
          .pipe(
            map(
              (items: OtherIssuesItem[]) =>
                new otherRecieptissuesActions.LoadAllOtherIssueCreateItemsSuccess(
                  items
                )
            )
          );
      },
      onError: (
        action: otherRecieptissuesActions.LoadAllOtherIssueCreateItems,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.LoadAllOtherIssueCreateItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect()
  loadSelectedCreateItems$ = this.dataPersistence.fetch(
    OtherIssuesActionTypes.LOAD_SELECTED_OTHER_ISSUE_ITEMS,
    {
      run: (
        action: otherRecieptissuesActions.LoadSelectedOtherIssueCreateItems
      ) => {
        return this.otherReceiptIssueService
          .getOtherIssueCreateItems(
            action.payload.id,
            OtherReceiptsIssuesEnum.SELECTED,
            action.payload.pageIndex,
            action.payload.pageSize,
            action.payload.reqtype,
            action.payload.itemCode,
            action.payload.lotNumber,
            action.payload.sort,
            action.payload.filter
          )
          .pipe(
            map(
              (items: OtherIssuesItem[]) =>
                new otherRecieptissuesActions.LoadSelectedOtherIssueCreateItemsSuccess(
                  items
                )
            )
          );
      },
      onError: (
        action: otherRecieptissuesActions.LoadSelectedOtherIssueCreateItems,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.LoadSelectedOtherIssueCreateItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect()
  loadOtherIssueItemsCreateTotalCount$ = this.dataPersistence.fetch(
    OtherIssuesActionTypes.LOAD_ISSUE_ITEMS_CREATE_COUNT,
    {
      run: (
        action: otherRecieptissuesActions.LoadIssueItemsCreateTotalCount
      ) => {
        return this.otherReceiptIssueService
          .getOtherIssuesCreateItemsCount(
            action.payload.reqtype,
            action.payload.id
          )
          .pipe(
            map(
              (
                loadItemsTotalCountSuccessPayload: LoadOtherIssueCreateItemsTotalCountSuccessPayload
              ) =>
                new otherRecieptissuesActions.LoadIssueItemsCreateTotalCountSuccess(
                  loadItemsTotalCountSuccessPayload
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadIssueItemsCreateTotalCount,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.LoadIssueItemsCreateTotalCountFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() createStockIssueItems = this.dataPersistence.fetch(
    OtherIssuesActionTypes.CREATE_OTHER_ISSUE_STOCK_REQUEST_ITEMS,
    {
      run: (
        action: otherRecieptissuesActions.CreateOtherIssueStockRequestItems,
        state: OtherIssuesState
      ) => {
        return this.otherReceiptIssueService
          .CreateOtherIssueStockRequestItems(
            action.payload.id,
            action.payload.data,
            action.payload.requestType
          )
          .pipe(
            map(
              (data: any) =>
                new otherRecieptissuesActions.CreateOtherIssueStockRequestItemsSuccess(
                  data
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.CreateOtherIssueStockRequestItems,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.CreateOtherIssueStockRequestItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect() removeOtherStockIssueItems = this.dataPersistence.fetch(
    OtherIssuesActionTypes.REMOVE_OTHER_ISSUE_STOCK_REQUEST_ITEMS,
    {
      run: (
        action: otherRecieptissuesActions.RemoveOtherIssueStockRequestItems,
        state: OtherIssuesState
      ) => {
        return this.otherReceiptIssueService
          .removeOtherIssueStockRequestItems(
            action.payload.id,
            action.payload.data,
            action.payload.requestType
          )
          .pipe(
            map(
              (data: any) =>
                new otherRecieptissuesActions.RemoveOtherIssueStockRequestItemsSuccess(
                  data
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.RemoveOtherIssueStockRequestItems,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.RemoveOtherIssueStockRequestItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect() updateStockIssueItem = this.dataPersistence.fetch(
    OtherIssuesActionTypes.UPDATE_STOCK_REQUEST_CREATE_ITEM,
    {
      run: (action: otherRecieptissuesActions.UpdateStockRequestCreateItem) => {
        return this.otherReceiptIssueService
          .updateStockRequestCreateItem(
            action.payload.id,
            action.payload.itemid,
            action.payload.reqType,
            action.payload.value
          )
          .pipe(
            map(
              (data: any) =>
                new otherRecieptissuesActions.UpdateStockRequestCreateItemSuccess(
                  data
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.UpdateStockRequestCreateItem,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.UpdateStockRequestCreateItemFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect() updateStockIssue = this.dataPersistence.fetch(
    OtherIssuesActionTypes.UPDATE_STOCK_REQUEST,
    {
      run: (action: otherRecieptissuesActions.UpdateStockRequest) => {
        return this.otherReceiptIssueService
          .updateStockRequest(
            action.payload.id,
            action.payload.reqType,
            action.payload.carrierDetails,
            action.payload.approvalDetails,
            action.payload.remarks,
            action.payload.status
          )
          .pipe(
            map(
              (data: any) =>
                new otherRecieptissuesActions.UpdateStockRequestSuccess(data)
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.UpdateStockRequest,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.UpdateStockRequestFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  /**
   * The effect which handles the  searchItem Action
   */
  @Effect() searchItemAdjustment$: Observable<
    Action
  > = this.dataPersistence.fetch(OtherIssuesActionTypes.ADJUSTMENT_SEARCH, {
    run: (action: otherRecieptissuesActions.SearchAdjustment) => {
      return this.otherReceiptIssueService
        .searchAdjustmentItem(
          action.payload.variantCode,
          action.payload.lotNumber
        )
        .pipe(
          map(
            (
              searchItemPayloadSuccess: otherRecieptissuesActions.AdjustmentSearchItemPayloadSuccess
            ) =>
              new otherRecieptissuesActions.SearchAdjustmentSuccess({
                items: searchItemPayloadSuccess.items.filter(data => {
                  return data.productGroupId === ProductGroupEnum.PLAIN;
                }),
                count: searchItemPayloadSuccess.count
              })
          )
        );
    },
    onError: (
      action: otherRecieptissuesActions.SearchAdjustment,
      error: HttpErrorResponse
    ) => {
      return new otherRecieptissuesActions.SearchAdjustmentFailure(
        this.errorHandler(error)
      );
    }
  });
  @Effect() CreateStockRequestAdjustment = this.dataPersistence.fetch(
    OtherIssuesActionTypes.CREATE_STOCK_REQUEST_ADJUSTMENT,
    {
      run: (action: otherRecieptissuesActions.CreateStockRequestAdjustment) => {
        return this.otherReceiptIssueService
          .createStockRequestAdjustment(
            action.payload.reqType,
            action.payload.approvalDetails,
            action.payload.items,
            action.payload.remarks
          )
          .pipe(
            map(
              (data: any) =>
                new otherRecieptissuesActions.CreateStockRequestAdjustmentSuccess(
                  data
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.CreateStockRequestAdjustment,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.CreateStockRequestAdjustmentFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect() loadIssueADJList$: Observable<Action> = this.dataPersistence.fetch(
    OtherIssuesActionTypes.LOAD_ISSUE_ADJ_LIST,
    {
      run: (action: otherRecieptissuesActions.LoadIssueADJList) => {
        return this.otherReceiptIssueService
          .getIssueList(
            action.payload.type,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (stockTransferNotes: OtherIssuedataModel) =>
                new otherRecieptissuesActions.LoadIssueADJListSuccess(
                  stockTransferNotes
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadIssueADJList,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.LoadIssueADJListFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadIssueLossList$: Observable<Action> = this.dataPersistence.fetch(
    OtherIssuesActionTypes.LOAD_ISSUE_LOSS_LIST,
    {
      run: (action: otherRecieptissuesActions.LoadIssueLossList) => {
        return this.otherReceiptIssueService
          .getIssueList(
            action.payload.type,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (stockTransferNotes: OtherIssuedataModel) =>
                new otherRecieptissuesActions.LoadIssueLossListSuccess(
                  stockTransferNotes
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadIssueLossList,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.LoadIssueLossListFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect() loadIssuePSVList$: Observable<Action> = this.dataPersistence.fetch(
    OtherIssuesActionTypes.LOAD_ISSUE_PSV_LIST,
    {
      run: (action: otherRecieptissuesActions.LoadIssuePSVList) => {
        return this.otherReceiptIssueService
          .getIssueList(
            action.payload.type,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (stockTransferNotes: OtherIssuedataModel) =>
                new otherRecieptissuesActions.LoadIssuePSVListSuccess(
                  stockTransferNotes
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadIssuePSVList,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.LoadIssuePSVListFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect() searchItemPSV$: Observable<Action> = this.dataPersistence.fetch(
    OtherIssuesActionTypes.PSV_SEARCH,
    {
      run: (action: otherRecieptissuesActions.SearchPSV) => {
        return this.otherReceiptIssueService
          .searchAdjustmentItem(
            action.payload.variantCode,
            action.payload.lotNumber
          )
          .pipe(
            map(
              (
                searchItemPayloadSuccess: otherRecieptissuesActions.PSVSearchItemPayloadSuccess
              ) =>
                new otherRecieptissuesActions.SearchPSVSuccess({
                  items: searchItemPayloadSuccess.items.filter(data => {
                    return data.productGroupId === ProductGroupEnum.STUDDED;
                  }),
                  count: searchItemPayloadSuccess.count
                })
            )
          );
      },
      onError: (
        action: otherRecieptissuesActions.SearchPSV,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.SearchPSVFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect() CreateStockRequestPSV$ = this.dataPersistence.fetch(
    OtherIssuesActionTypes.CREATE_STOCK_REQUEST_PSV,
    {
      run: (action: otherRecieptissuesActions.CreateStockRequestPSV) => {
        return this.otherReceiptIssueService
          .createStockRequestAdjustment(
            action.payload.reqType,
            action.payload.approvalDetails,
            action.payload.items,
            action.payload.remarks
          )
          .pipe(
            map(
              (data: any) =>
                new otherRecieptissuesActions.CreateStockRequestPSVSuccess(data)
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.CreateStockRequestPSV,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.CreateStockRequestPSVFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect() searchItemFOC$: Observable<Action> = this.dataPersistence.fetch(
    OtherIssuesActionTypes.FOC_SEARCH,
    {
      run: (action: otherRecieptissuesActions.SearchFOC) => {
        return this.otherReceiptIssueService
          .searchAdjustmentItem(
            action.payload.variantCode,
            action.payload.lotNumber
          )
          .pipe(
            map(
              (
                searchItemPayloadSuccess: otherRecieptissuesActions.PSVSearchItemPayloadSuccess
              ) =>
                new otherRecieptissuesActions.SearchFOCSuccess({
                  items: searchItemPayloadSuccess.items,
                  count: searchItemPayloadSuccess.count
                })
            )
          );
      },
      onError: (
        action: otherRecieptissuesActions.SearchFOC,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.SearchFOCFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect() CreateStockRequestFOC$ = this.dataPersistence.fetch(
    OtherIssuesActionTypes.CREATE_STOCK_REQUEST_FOC,
    {
      run: (action: otherRecieptissuesActions.CreateStockRequestFOC) => {
        return this.otherReceiptIssueService
          .createStockRequestAdjustment(
            action.payload.reqType,
            action.payload.approvalDetails,
            action.payload.items,
            action.payload.remarks
          )
          .pipe(
            map(
              (data: any) =>
                new otherRecieptissuesActions.CreateStockRequestFOCSuccess(data)
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.CreateStockRequestFOC,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.CreateStockRequestFOCFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect() loadIssueFOCList$: Observable<Action> = this.dataPersistence.fetch(
    OtherIssuesActionTypes.LOAD_ISSUE_FOC_LIST,
    {
      run: (action: otherRecieptissuesActions.LoadIssueFOCList) => {
        return this.otherReceiptIssueService
          .getIssueList(
            action.payload.type,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (stockTransferNotes: OtherIssuedataModel) =>
                new otherRecieptissuesActions.LoadIssueFOCListSuccess(
                  stockTransferNotes
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadIssueFOCList,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.LoadIssueFOCListFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  //todo change
  @Effect() cancelStockRequest = this.dataPersistence.fetch(
    OtherIssuesActionTypes.CANCEL_STOCK_REQUEST,
    {
      run: (action: otherRecieptissuesActions.CancelStockRequest) => {
        return this.otherReceiptIssueService
          .cancelStockRequest(action.payload.id, action.payload.requestType)
          .pipe(
            map(
              (data: any) =>
                new otherRecieptissuesActions.CancelStockRequestSuccess(data)
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.CancelStockRequest,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.CancelStockRequestFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect() printOtherIssues = this.dataPersistence.fetch(
    OtherIssuesActionTypes.PRINT_OTHER_ISSUES,
    {
      run: (action: otherRecieptissuesActions.PrintOtherIssues) => {
        return this.otherReceiptIssueService
          .printOtherIssue(action.payload.id, action.payload.requestType)
          .pipe(
            map(
              (data: any) =>
                new otherRecieptissuesActions.PrintOtherIssuesSuccess(data)
            )
          );
      },
      onError: (
        action: otherRecieptissuesActions.PrintOtherIssues,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.PrintOtherIssuesFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect() loadProductCategories = this.dataPersistence.fetch(
    OtherIssuesActionTypes.LOAD_PRODUCT_CATEGORIES,
    {
      run: (action: otherRecieptissuesActions.LoadProductCategories) => {
        return this.productCategoryDataService
          .getProductCategories()
          .pipe(
            map(
              (data: ProductCategory[]) =>
                new otherRecieptissuesActions.LoadProductCategoriesSuccess(data)
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadProductCategories,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.LoadProductCategoriesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadProductGroups = this.dataPersistence.fetch(
    OtherIssuesActionTypes.LOAD_PROUDCT_GROUPS,
    {
      run: (action: otherRecieptissuesActions.LoadProductGroups) => {
        return this.productGroupDataService
          .getProductGroups()
          .pipe(
            map(
              (data: ProductGroup[]) =>
                new otherRecieptissuesActions.LoadProductGroupsSuccess(data)
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadProductGroups,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.LoadProductGroupsFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }

  constructor(
    private dataPersistence: DataPersistence<OtherIssuesState>,
    private notificationService: NotificationService,
    private otherReceiptIssueService: OtherReceiptIssueService,
    private productGroupDataService: ProductGroupDataService,
    private productCategoryDataService: ProductCategoryDataService
  ) {}
}
