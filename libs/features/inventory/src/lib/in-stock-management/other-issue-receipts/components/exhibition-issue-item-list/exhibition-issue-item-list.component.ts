import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { PageEvent } from '@angular/material';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { AppsettingFacade } from '@poss-web/core';
import { OtherIssuesItem } from '../../models/other-issues.model';

@Component({
  selector: 'poss-web-exhibition-issue-item-list',
  templateUrl: './exhibition-issue-item-list.component.html',
  styleUrls: ['./exhibition-issue-item-list.component.scss']
})
export class ExhibitionIssueItemListComponent implements OnInit, OnDestroy {
  @Input() itemList: OtherIssuesItem[];
  @Input() count: number;
  @Input() pageSize = 0;
  @Input() pageEvent: PageEvent;
  @Output() paginator = new EventEmitter<PageEvent>();
  destroy$ = new Subject<null>();
  pageSizeOptions: number[] = [];

  constructor(private appsettingFacade: AppsettingFacade) {}

  ngOnInit() {
    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(resp => (this.pageSize = JSON.parse(resp)));

    this.pageEvent.pageSize = this.pageSize;
    this.appsettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(resp => (this.pageSizeOptions = resp));
  }
  paginate(event: PageEvent) {
    this.paginator.emit(event);
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
