import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
  OnDestroy
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subject, of, Observable } from 'rxjs';
import { OtherReceiptsIssuesEnum } from '../../../../in-stock-management/in-stock/in-stock.enum';
import { OtherIssuesItem } from '../../models/other-issues.model';

@Component({
  selector: 'poss-web-exhibition-issue-item',
  templateUrl: './exhibition-issue-item.component.html',
  styleUrls: ['./exhibition-issue-item.component.scss']
})
export class ExhibitionIssueItemComponent implements OnInit, OnDestroy {
  @Input() item: OtherIssuesItem;

  itemForm: FormGroup;
  destroy$ = new Subject<null>();
  measuredWeight: number;
  itemWeight: number;
  showUpdateStatus = true;
  weight = 0;
  status: string;
  weightToDisplay: string;
  qty: number;
  disabled: boolean;
  constructor() {}

  ngOnInit() {
    this.itemForm = this.createForm(this.item);
    if (this.item.availableQuantity === 0) {
      this.itemForm.disable();
      this.disabled = true;
    }
    this.itemWeight = this.item.itemWeight / this.item.totalQuantity;
    if (this.item.status === OtherReceiptsIssuesEnum.APVL_PENDING_STATUS) {
      this.itemForm.controls.isItemSelected.disable();
    }
  }

  createForm(item: OtherIssuesItem): FormGroup {
    this.weight = this.item.availableQuantity * this.item.stdWeight;
    this.weightToDisplay = Number(this.weight).toFixed(3);
    this.qty = this.item.availableQuantity;
    return new FormGroup({
      isItemSelected: new FormControl(false),
      issueQuantity: new FormControl(
        this.qty,
        Validators.compose([
          Validators.required,
          Validators.min(1),
          Validators.pattern('^[1-9][0-9]*$'),
          Validators.max(this.qty)
        ])
      ),
      weight: new FormControl(this.weightToDisplay)
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
