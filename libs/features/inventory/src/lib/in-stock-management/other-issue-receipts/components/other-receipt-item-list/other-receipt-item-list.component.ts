import {
  Component,
  Input,
  EventEmitter,
  Output,
  OnDestroy,
  OnInit,
  SimpleChanges,
  OnChanges
} from '@angular/core';

import { Subject } from 'rxjs';
import { PageEvent } from '@angular/material';
import { FormArray } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { AppsettingFacade, BinCode, Lov } from '@poss-web/core';
import { OtherReceiptItem, OtherReceiptItemToUpdate} from '../../models/other-receipt.model';

@Component({
  selector: 'poss-web-other-receipt-item-list',
  templateUrl: './other-receipt-item-list.component.html',
  styleUrls: ['./other-receipt-item-list.component.scss']
})
export class OtherReceiptItemListComponent
  implements OnInit, OnChanges, OnDestroy {
  @Input() itemList: OtherReceiptItem[];
  @Input() isVerified: boolean; // verified/non
  @Input() binGroupCode: string;
  @Input() binCodes: BinCode[];
  @Input() remarks: Lov[] = [];
  @Input() tolerance: number;
  @Input() count = 0;
  @Input() pageSize = 0;
  @Input() pageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };
  minPageSize = 0;
  @Output() verify = new EventEmitter<OtherReceiptItemToUpdate>();
  @Output() update = new EventEmitter<OtherReceiptItemToUpdate>();
  @Output() paginator = new EventEmitter<PageEvent>();
  @Output() parentFormDirty = new EventEmitter<boolean>();
  pageSizeOptions: number[]=[];
  destroy$ = new Subject<null>();
  parentForm: FormArray;
  isFormDirty = false;

  constructor(private appSettingFacade: AppsettingFacade) {
    this.parentForm = new FormArray([]);
    if (!this.isVerified) {
      this.parentForm.valueChanges
        .pipe(takeUntil(this.destroy$))
        .subscribe(value => {
          /* let isDirty = false;
          for (
            let i = 0;
            i < this.parentForm.controls.length && !isDirty;
            i++
          ) {
            if (!this.parentForm.controls[i].pristine) {
              isDirty = true;
            }
          }
          if (this.isFormDirty !== isDirty) {
            this.isFormDirty = isDirty;
            this.parentFormDirty.emit(isDirty);
          } */
          this.sendFormStatus();
        });
    }
  }

  ngOnInit() {
    this.appSettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.pageSizeOptions = data;
        this.minPageSize = this.pageSizeOptions.reduce((a: number, b: number) => (a < b ? a : b));
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes &&
      changes['itemList'] &&
      changes['itemList'].currentValue &&
      changes['itemList'].previousValue &&
      (changes['itemList'].currentValue as []).length === 0 &&
      (changes['itemList'].previousValue as []).length > 0
    ) {
      const newPageEvent: PageEvent = {
        ...this.pageEvent,
        pageIndex: this.pageEvent.pageIndex - 1
      };
      if (newPageEvent.pageIndex >= 0) {
        this.paginate(newPageEvent);
      }
    }
  }
  sendFormStatus() {
    const isDirty = !this.parentForm.pristine;
    this.parentFormDirty.emit(isDirty);
  }
  verifyItem(itemToUpdate: OtherReceiptItemToUpdate) {
    this.verify.emit(itemToUpdate);
  }

  updateItem(itemToUpdate: OtherReceiptItemToUpdate) {
    this.update.emit(itemToUpdate);
  }

  paginate(event: PageEvent) {
    this.paginator.emit(event);
  }

  trackBy(index: number, item: OtherReceiptItem) {
    return item.id;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
