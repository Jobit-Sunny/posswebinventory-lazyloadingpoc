import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  Output,
  EventEmitter
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil, debounceTime } from 'rxjs/operators';
import { UpdateAdjustementItemPayload } from '../../+receive-state/other-receipts.actions';
import { AdjustmentItem } from '../../models/other-receipt.model';
import { BinCode } from '@poss-web/core';

@Component({
  selector: 'poss-web-adjustment-receipts-item-list',
  templateUrl: './adjustment-receipts-item-list.component.html',
  styleUrls: ['./adjustment-receipts-item-list.component.scss']
})
export class AdjustmentReceiptsItemListComponent implements OnInit, OnDestroy {
  @Input() itemList: AdjustmentItem[];
  @Input() selectionEvents: Observable<boolean>;
  @Input() hasRemove;
  @Output() removeItem = new EventEmitter<AdjustmentItem>();
  @Output() selection: EventEmitter<{
    selected: boolean;
    id: number;
  }> = new EventEmitter();
  selectionAllSubscription: any;
  selectionAllSubject: Subject<any> = new Subject<any>();
  destroy$: Subject<null> = new Subject<null>();
  @Output() updateItem = new EventEmitter<UpdateAdjustementItemPayload>();
  @Input() binGroupCode: string;
  @Input() binCodes: BinCode[];
  constructor() {}

  ngOnInit() {
    this.selectionEvents
      .pipe(
        takeUntil(this.destroy$),
        debounceTime(10)
      )
      .subscribe(data => {
        this.selectionAllSubject.next(data);
      });
  }
  selectionEmit(selection: { selected: boolean; id: number }) {
    this.selection.emit(selection);
  }
  updateItems(item: UpdateAdjustementItemPayload) {
    this.updateItem.emit(item);
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
  remove(item: AdjustmentItem) {
    this.removeItem.emit(item);
  }
}
