import {
  Component,
  OnInit,
  SimpleChanges,
  EventEmitter,
  Output,
  Input,
  OnDestroy
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { delay, debounceTime, takeUntil } from 'rxjs/operators';
import { of, Subject, Observable } from 'rxjs';
import { OtherReceiptsIssuesEnum } from '../../../../in-stock-management/in-stock/in-stock.enum';
import { OtherIssuesItem } from '../../models/other-issues.model';

@Component({
  selector: 'poss-web-exhibition-issue-create-item',
  templateUrl: './exhibition-issue-create-item.component.html',
  styleUrls: ['./exhibition-issue-create-item.component.scss']
})
export class ExhibitionIssueCreateItemComponent implements OnInit, OnDestroy {
  @Input() item: OtherIssuesItem;
  @Input() selectionEvents: Observable<any>;
  @Output() quantityChange = new EventEmitter<any>();
  @Output() selection: EventEmitter<{
    selected: boolean;
    item: any;
  }> = new EventEmitter();

  itemForm: FormGroup;
  destroy$ = new Subject<null>();
  measuredWeight: number;
  itemWeight: number;
  showUpdateStatus = true;
  weight = 0;
  status: string;
  isChecked: boolean;
  selectionAllSubscription: any;
  @Input() SelectedTab: any;
  isReadOnly: boolean;
  @Output() quantity = new EventEmitter<any>();
  qty: number;
  weightToDisplay: any;
  otherIssuesTabEnumRef = OtherReceiptsIssuesEnum;
  constructor() {}

  ngOnInit() {
    this.itemForm = this.createForm(this.item);
    this.itemWeight = this.item.itemWeight / this.item.totalQuantity;
    if (this.SelectedTab === OtherReceiptsIssuesEnum.ALL) {
      this.isReadOnly = true;
    } else if (
      this.SelectedTab === OtherReceiptsIssuesEnum.SELECTED_PRODUCTS &&
      this.item.requestedQuantity > 1
    ) {
      this.isReadOnly = false;
    }
    this.itemForm.valueChanges.pipe(
      debounceTime(1000),
      takeUntil(this.destroy$)
    );
    this.selectionAllSubscription = this.selectionEvents
      .pipe(
        debounceTime(10),
        takeUntil(this.destroy$)
      )
      .subscribe(data => {
        if (data.selectCheckbox === true) {
          this.itemForm.patchValue({ isItemSelected: true });
        } else {
          this.itemForm.patchValue({ isItemSelected: false });
        }
        if (data.enableCheckbox === true) {
          this.itemForm.controls.isItemSelected.enable();
        } else {
          this.itemForm.controls.isItemSelected.disable();
        }
      });
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  createForm(item: OtherIssuesItem): FormGroup {
    if (this.SelectedTab === OtherReceiptsIssuesEnum.ALL) {
      this.weight = this.item.availableWeight;
      this.weightToDisplay = Number(this.weight).toFixed(3);
      this.qty = this.item.availableQuantity;
    } else if (this.SelectedTab === OtherReceiptsIssuesEnum.SELECTED_PRODUCTS) {
      this.weight = this.item.measuredWeight;
      this.weightToDisplay = Number(this.weight).toFixed(3);
      this.qty = this.item.measuredQuantity;
    }
    return new FormGroup({
      isItemSelected: new FormControl(false),
      issueQuantity: new FormControl(
        this.qty,
        Validators.compose([
          Validators.required,
          Validators.min(1),
          Validators.pattern('^[1-9][0-9]*$'),
          Validators.max(this.item.availableQuantity)
        ])
      ),
      weight: new FormControl(this.weightToDisplay)
    });
  }
  onRequestedQuantityChange() {
    if (
      this.itemForm.value.issueQuantity !== 0 &&
      this.itemForm.value.issueQuantity &&
      this.itemForm.value.issueQuantity <= this.item.availableQuantity
    ) {
      this.measuredWeight =
        this.item.stdWeight * this.itemForm.value.issueQuantity;
      this.itemForm.patchValue({
        weight: this.measuredWeight.toFixed(3)
      });
      this.quantity.emit({
        itemId: this.item.id,
        inventoryId: this.item.inventoryId,
        measuredWeight: this.measuredWeight,
        quantity: this.itemForm.value.issueQuantity,
        status: 'SELECTED'
      });
    } else {
      let ItemQuantity = 0;
      this.measuredWeight = this.item.measuredWeight;
      ItemQuantity = this.item.measuredQuantity;
      this.itemForm.patchValue({
        weight: this.measuredWeight.toFixed(3),
        issueQuantity: ItemQuantity
      });
    }
  }
  selectionEmit() {
    this.selection.emit({
      selected: this.itemForm.value.isItemSelected,
      item: this.item
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
