import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  OnDestroy,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { PageEvent, MatPaginator } from '@angular/material';
import { takeUntil, debounceTime } from 'rxjs/operators';
import {
  OtherIssuesItem,
  AdjustmentItemToUpdate
} from '../../models/other-issues.model';

@Component({
  selector: 'poss-web-adjsutment-issue-item-list',
  templateUrl: './adjsutment-issue-item-list.component.html',
  styleUrls: ['./adjsutment-issue-item-list.component.scss']
})
export class AdjsutmentIssueItemListComponent implements OnInit, OnDestroy {
  @Input() itemList: OtherIssuesItem[] = [];
  @Input() tab: string;

  //@Input() loadedItems: Item[] = [];
  @Input() count = 0;
  pageSizeOptions: number[] = [];
  @Input() selectionEvents: Observable<boolean>;
  @ViewChild(MatPaginator, { static: false }) paginationRef: MatPaginator;
  @Input() haspaginator = false;
  @Output() updateItem = new EventEmitter<AdjustmentItemToUpdate>();
  @Output() removeItem = new EventEmitter<OtherIssuesItem>();
  @Output() paginator = new EventEmitter<PageEvent>();
  @Output() selection: EventEmitter<{
    selected: boolean;
    id: number;
  }> = new EventEmitter();
  selectionAllSubscription: any;

  selectionAllSubject: Subject<any> = new Subject<any>();
  destroy$: Subject<null> = new Subject<null>();
  @Input()
  resetPagination: Observable<null>;

  constructor() {}

  ngOnInit() {
    this.selectionEvents
      .pipe(
        takeUntil(this.destroy$),
        debounceTime(10)
      )
      .subscribe(data => {
        this.selectionAllSubject.next(data);
      });
  }
  /**
   * emits selected items
   * @param item
   */

  paginate(event: PageEvent) {
    this.paginator.emit(event);
  }
  /**
   * emits an event  to remove item from cart
   * @param item
   */
  remove(item: OtherIssuesItem) {
    this.removeItem.emit(item);
  }

  /**
   * emits event to upadte the item
   * @param item
   */
  updateItems(item: AdjustmentItemToUpdate) {
    this.updateItem.emit(item);
  }
  selectionEmit(selection: { selected: boolean; id: number }) {
    this.selection.emit(selection);
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
