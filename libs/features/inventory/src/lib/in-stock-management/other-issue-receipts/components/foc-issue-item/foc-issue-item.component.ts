import {
  Component,
  OnInit,
  SimpleChanges,
  EventEmitter,
  Input,
  Output,
  OnDestroy,
  OnChanges
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { delay, takeUntil, debounceTime } from 'rxjs/operators';
import { of, Observable, Subject } from 'rxjs';
import {
  OtherIssuesItem,
  AdjustmentItemToUpdate
} from '../../models/other-issues.model';

@Component({
  selector: 'poss-web-foc-issue-item',
  templateUrl: './foc-issue-item.component.html',
  styleUrls: ['./foc-issue-item.component.scss']
})
export class FocIssueItemComponent implements OnInit, OnDestroy {
  @Input() item: OtherIssuesItem;

  @Input() cart = true;
  @Input() tab: string;

  @Output() selectedItem = new EventEmitter<OtherIssuesItem>();
  @Output() checkBox = new EventEmitter<boolean>();
  @Output() updateItem = new EventEmitter<AdjustmentItemToUpdate>();
  @Output() remove = new EventEmitter<OtherIssuesItem>();
  readonlyqty = true;
  checkBoxValue = false;
  itemForm: FormGroup;
  destroy$ = new Subject();
  @Input() selectionEvents: Observable<any>;
  @Output() selection: EventEmitter<{
    selected: boolean;
    id: number;
  }> = new EventEmitter();
  selectedAll: boolean;
  weight: number;
  weightToDisplay: string;
  qty: number;
  tolerance = 0.03;
  weightMismatchError: string;
  quantityError: string;
  readonlyData: boolean;
  productGroupError: string;
  constructor() {}

  ngOnInit() {
    this.itemForm = this.createForm(this.item);
    this.selectionEvents.pipe(takeUntil(this.destroy$)).subscribe(data => {
      if (data.selectCheckbox === true) {
        this.itemForm.patchValue({ isItemSelected: true });
      } else {
        this.itemForm.patchValue({ isItemSelected: false });
      }
      if (data.enableCheckbox === true) {
        this.itemForm.controls.isItemSelected.enable();
      } else {
        this.itemForm.controls.isItemSelected.disable();
      }
    });

    this.itemForm
      .get('measuredQuantity')
      .valueChanges.pipe(debounceTime(1000))
      .subscribe(() => {
        if (
          this.itemForm.get('measuredQuantity').value >
            this.item.availableQuantity ||
          this.itemForm.get('measuredQuantity').value <= 0
        ) {
          this.quantityError = 'Invalid Quantity';
          of(true)
            .pipe(delay(1000))
            .subscribe(() => {
              this.itemForm.patchValue({
                measuredQuantity: this.item.availableQuantity
              });
            });
        } else if (
          this.item.availableQuantity >=
            this.itemForm.get('measuredQuantity').value &&
          this.itemForm.valid
        ) {
          this.itemForm.patchValue({
            measuredWeight: (
              this.item.stdWeight * this.itemForm.get('measuredQuantity').value
            ).toFixed(3)
          });
          this.updateItemValue(
            this.itemForm.value.measuredQuantity,
            this.itemForm.value.measuredWeight
          );
          this.quantityError = '';
        }
      });

    if (
      this.tab === 'selectedProducts' &&
      this.item.availableQuantity > 1 &&
      this.cart === true
    ) {
      this.readonlyqty = false;
    }

    this.itemForm
      .get('measuredWeight')
      .valueChanges.pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe(val => {
        if (
          Math.abs(
            val -
              this.item.stdWeight *
                this.itemForm.get('measuredQuantity').value.toFixed(3)
          ) > this.tolerance
        ) {
          this.weightMismatchError = 'Weight Mismatch';
          of(true)
            .pipe(delay(1000))
            .subscribe(() => {
              this.itemForm.patchValue({
                measuredWeight: (
                  this.item.stdWeight *
                  this.itemForm.get('measuredQuantity').value
                ).toFixed(3)
              });
            });
        } else {
          this.weightMismatchError = '';
          this.updateItemValue(
            this.itemForm.value.measuredQuantity,
            this.itemForm.value.measuredWeight
          );
        }
      });
    if (!this.cart) {
      this.readonlyData = true;
    }
  }

  createForm(item: OtherIssuesItem): FormGroup {
    this.weight = this.item.measuredWeight;
    this.weightToDisplay = Number(this.weight).toFixed(3);
    this.qty = this.item.measuredQuantity;
    return new FormGroup({
      measuredQuantity: new FormControl(this.qty, [
        Validators.required,
        Validators.min(1)
      ]),
      isItemSelected: new FormControl(''),
      measuredWeight: new FormControl(this.weight),
      binGroupCode: new FormControl()
    });
  }

  /**
   * emits event for remove item from cart
   */
  removeItem() {
    this.remove.emit(this.item);
  }

  /**
   * emits the selected items
   */
  emitSelectedItem() {
    this.selectedItem.emit(this.item);
    this.checkBox.emit(this.checkBoxValue);
  }
  /**
   * creates item payload with updated quantity
   * @param quantity :updated quantity
   */
  createUpdateItemPayload(
    quantity: number,
    weight: number
  ): AdjustmentItemToUpdate {
    return {
      id: this.item.id,
      newUpdate: { quantity: quantity, weight: weight },
      actualDetails: {
        quantity: this.item.totalQuantity,
        weight: this.item.totalWeight
      }
    };
  }
  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
  selectItem() {
    this.selection.emit({
      id: this.item.id,
      selected: this.itemForm.get('isItemSelected').value
    });
  }
  updateItemValue(quantity: number, weight: number) {
    if (this.itemForm.valid) {
      this.updateItem.emit({
        id: this.item.id,
        actualDetails: {
          quantity: this.item.totalQuantity,
          weight: this.item.totalWeight
        },
        newUpdate: {
          quantity: quantity,
          weight: weight
        }
      });
    }
  }
}
