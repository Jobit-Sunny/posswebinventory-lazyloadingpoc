import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnDestroy
} from '@angular/core';
import { PageEvent } from '@angular/material';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { AppsettingFacade } from '@poss-web/core';
import { OtherIssuesItem } from '../../models/other-issues.model';

@Component({
  selector: 'poss-web-exhibition-issue-create-item-list',
  templateUrl: './exhibition-issue-create-item-list.component.html',
  styleUrls: ['./exhibition-issue-create-item-list.component.scss']
})
export class ExhibitionIssueCreateItemListComponent
  implements OnInit, OnDestroy {
  @Input() itemList: OtherIssuesItem[];
  @Input() count: number;
  @Input() pageSize = 0;
  @Input() pageEvent: PageEvent;
  @Input() selectionEvents: Observable<any>;
  @Output() paginator = new EventEmitter<PageEvent>();
  @Output() selection: EventEmitter<{
    selected: boolean;
    item: any;
  }> = new EventEmitter();
  isReadOnly: boolean;
  @Input() SelectedTab: any;
  selectionAllSubject: Subject<boolean> = new Subject<boolean>();
  destroy$ = new Subject<null>();
  selectionAllSubscription: any;
  @Output() quantity = new EventEmitter<any>();
  pageSizeOptions: number[] = [];
  constructor(private appsettingFacade: AppsettingFacade) {}

  ngOnInit() {
    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(resp => (this.pageSize = JSON.parse(resp)));
    this.appsettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(resp => (this.pageSizeOptions = resp));
    this.selectionAllSubscription = this.selectionEvents
      .pipe(
        debounceTime(10),
        takeUntil(this.destroy$)
      )
      .subscribe(data => {
        this.selectionAllSubject.next(data);
      });
    this.appsettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(resp => (this.pageSizeOptions = resp));
  }
  paginate(event: PageEvent) {
    this.paginator.emit(event);
  }

  selectionEmit(selection: { selected: boolean; item: any }) {
    this.selection.emit(selection);
  }
  quantityChange(event) {
    this.quantity.emit(event);
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
