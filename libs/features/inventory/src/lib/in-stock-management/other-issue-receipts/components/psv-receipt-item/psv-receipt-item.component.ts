import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable, Subject, of } from 'rxjs';
import { debounceTime, takeUntil, delay } from 'rxjs/operators';
import { UpdateAdjustementItemPayload } from '../../+receive-state/other-receipts.actions';
import { AdjustmentItem } from '../../models/other-receipt.model';
import { BinCode } from '@poss-web/core';
import { SelectionDialogService, Option } from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'poss-web-psv-receipt-item',
  templateUrl: './psv-receipt-item.component.html',
  styleUrls: ['./psv-receipt-item.component.scss']
})
export class PsvReceiptItemComponent implements OnInit, OnDestroy {
  @Input() hasRemove = true;
  @Output() selectedItem = new EventEmitter<AdjustmentItem>();
  @Output() checkBox = new EventEmitter<boolean>();
  @Output() updateItem = new EventEmitter<UpdateAdjustementItemPayload>();
  @Input() item: AdjustmentItem;
  @Input() selectionEvents: Observable<any>;
  @Output() remove = new EventEmitter<AdjustmentItem>();
  @Output() selection: EventEmitter<{
    selected: boolean;
    id: number;
  }> = new EventEmitter();
  selectedAll: boolean;
  itemForm: FormGroup;
  selectForm: FormGroup;
  readonlyqty = true;
  checkBoxValue = false;
  binGroups = ['Sales Bin', 'Dispute Bin', 'Defect Bin'];
  assignBinCode: string;
  destroy$ = new Subject();
  readonlyData: boolean;
  productGroupError: string;
  weightMismatchError: string;
  tolerance = 0.03;
  quantityError: string;
  @Input() binGroupCode: string;
  @Input() binCodes: BinCode[] = [];
  selectedBinCode: any;
  searchBinCodeLable: any;
  selectBinCodeLable: any;
  binsForSelection: Option[] = [];
  constructor(
    private selectionDialog: SelectionDialogService,
    private translate: TranslateService
  ) {
    this.translate
      .get([
        'pw.otherReceiptsIssues.searchBinCodeLable',
        'pw.otherReceiptsIssues.selectBinCodeLable'
      ])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: any) => {
        this.searchBinCodeLable =
          translatedMessages['pw.otherReceiptsIssues.searchBinCodeLable'];
        this.selectBinCodeLable =
          translatedMessages['pw.otherReceiptsIssues.selectBinCodeLable'];
      });
  }

  ngOnInit() {
    this.readonlyqty = true;
    this.selectForm = new FormGroup({
      isItemSelected: new FormControl('')
    });
    this.selectionEvents.pipe(takeUntil(this.destroy$)).subscribe(data => {
      if (data.selectCheckbox === true) {
        this.selectForm.patchValue({ isItemSelected: true });
      } else {
        this.selectForm.patchValue({ isItemSelected: false });
      }
      if (data.enableCheckbox === true) {
        this.selectForm.controls.isItemSelected.enable();
      } else {
        this.selectForm.controls.isItemSelected.disable();
      }
    });
    this.itemForm = this.createForm(this.item);
    this.itemForm
      .get('measuredQuantity')
      .valueChanges.pipe(debounceTime(1000))
      .subscribe(() => {
        if (
          this.itemForm.get('measuredQuantity').value >
            this.item.availableQuantity ||
          this.itemForm.get('measuredQuantity').value <= 0
        ) {
          this.quantityError = 'Invalid Quantity';
          of(true)
            .pipe(delay(1000))
            .subscribe(() => {
              this.itemForm.patchValue({
                measuredQuantity: this.item.availableQuantity
              });
            });
        } else if (
          this.item.availableQuantity >=
            this.itemForm.get('measuredQuantity').value &&
          this.itemForm.valid
        ) {
          this.itemForm.patchValue({
            measuredWeight: (
              this.item.stdWeight * this.itemForm.get('measuredQuantity').value
            ).toFixed(3)
          });
          this.updateItemValue(
            this.itemForm.value.measuredQuantity,
            this.itemForm.value.measuredWeight,
            this.itemForm.value.assignedBin,
            this.selectedBinCode
          );
          this.quantityError = '';
        }
      });
    this.itemForm
      .get('measuredWeight')
      .valueChanges.pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe(val => {
        if (
          Math.abs(
            val -
              this.item.stdWeight *
                this.itemForm.get('measuredQuantity').value.toFixed(3)
          ) > this.tolerance
        ) {
          this.weightMismatchError = 'Weight Mismatch';
          of(true)
            .pipe(delay(1000))
            .subscribe(() => {
              this.itemForm.patchValue({
                measuredWeight: (
                  this.item.stdWeight *
                  this.itemForm.get('measuredQuantity').value
                ).toFixed(3)
              });
            });
        } else {
          this.weightMismatchError = '';
          this.updateItemValue(
            this.itemForm.value.measuredQuantity,
            this.itemForm.value.measuredWeight,
            this.itemForm.value.assignedBin,
            this.selectedBinCode
          );
        }
      });
    /* this.itemForm
      .get('assignedBinCode')
      .valueChanges.pipe(debounceTime(1000))
      .subscribe(() => {
        if (this.itemForm.valid) {
          this.updateItemValue(
            this.itemForm.value.measuredQuantity,
            this.itemForm.value.measuredWeight,
            this.itemForm.value.assignedBin,
            this.itemForm.value.assignedBinCode
          );
        }
      }); */
    if (!this.hasRemove) {
      this.readonlyData = true;
    }
    if (this.binCodes.length > 0) {
      this.binsForSelection = this.binCodes.map(bincode => ({
        id: bincode.binCode,
        description: bincode.description
      }));
    }
  }
  change(event) {
    this.assignBinCode = event.value;
  }

  createForm(item: AdjustmentItem): FormGroup {
    this.selectedBinCode = item.binCode;
    return new FormGroup({
      measuredQuantity: new FormControl(item.measuredQuantity, [
        Validators.required,
        Validators.min(1)
      ]),
      measuredWeight: new FormControl(item.measuredWeight, [
        Validators.required,
        Validators.min(1)
      ]),
      assignedBin: new FormControl(this.hasRemove ? this.binGroupCode : null),
      assignedBinCode: new FormControl(
        this.hasRemove ? this.binGroups[0] : null
      )
    });
  }
  emitSelectedItem() {
    this.selectedItem.emit(this.item);
    this.checkBox.emit(this.checkBoxValue);
  }
  selectItem() {
    this.selection.emit({
      id: this.item.id,
      selected: this.selectForm.get('isItemSelected').value
    });
  }
  removeItem() {
    this.remove.emit(this.item);
  }
  updateItemValue(
    quantity: number,
    weight: number,
    assignedBin: string,
    assignedBinCode: string
  ) {
    if (this.itemForm.valid) {
      this.updateItem.emit({
        itemId: this.item.id,
        items: {
          itemCode: this.item.itemCode,
          measuredWeight: weight,
          binCode: assignedBinCode,
          binGroupCode: assignedBin,
          value: this.item.stdValue,
          quantity: quantity
        }
      });
    }
  }
  openBinSelectionPopup(event) {
    if (event) {
      event.stopPropagation();
    }
    this.selectionDialog
      .open({
        title: this.selectBinCodeLable,
        placeholder: this.searchBinCodeLable,
        options: this.binsForSelection
      })
      .pipe(takeUntil(this.destroy$))
      .subscribe((selectedOption: Option) => {
        if (selectedOption) {
          this.selectedBinCode = selectedOption.id;
          this.updateItemValue(
            this.itemForm.value.measuredQuantity,
            this.itemForm.value.measuredWeight,
            this.itemForm.value.assignedBin,
            this.selectedBinCode
          );
        }
      });
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
