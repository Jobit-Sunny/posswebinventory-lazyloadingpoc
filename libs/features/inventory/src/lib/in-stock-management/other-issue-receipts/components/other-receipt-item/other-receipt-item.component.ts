import { takeUntil, debounceTime, delay } from 'rxjs/operators';
import { Subject, of } from 'rxjs';
import {
  FormGroup,
  FormControl,
  Validators,
  ValidatorFn,
  AbstractControl,
  FormArray
} from '@angular/forms';
import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output,
  OnDestroy,
  SimpleChanges,
  OnChanges
} from '@angular/core';
import {
  OtherReceiptItem,
  OtherReceiptItemToUpdate
} from '../../models/other-receipt.model';
import {
  StockItemBinGroupCodeEnum,
  StockItemBinCodeEnum
} from '../../models/other-receipt.enum';
import { Lov, BinCode } from '@poss-web/core';
@Component({
  selector: 'poss-web-other-receipt-item',
  templateUrl: './other-receipt-item.component.html',
  styleUrls: ['./other-receipt-item.component.scss']
})
export class OtherReceiptItemComponent implements OnInit, OnDestroy, OnChanges {
  @Input() item: OtherReceiptItem;
  @Input() isVerified: boolean; // verified/nonVerified
  @Input() binGroupCode: string;
  @Input() binCodes: BinCode[];
  @Input() remarks: Lov[];
  @Input() tolerance = 0;

  @Input() parentForm: FormArray;

  @Output() verify = new EventEmitter<OtherReceiptItemToUpdate>();
  @Output() update = new EventEmitter<OtherReceiptItemToUpdate>();

  showUpdateStatus = true;

  itemForm: FormGroup;
  destroy$ = new Subject<null>();
  binGroupCodes: string[] = [];
  otherReceiptsStockItemBinGroupCodeEnumRef = StockItemBinGroupCodeEnum;
  constructor() {}
  ngOnChanges(changes: SimpleChanges): void {
    if (
      this.isVerified &&
      changes &&
      changes['item'] &&
      changes['item']['currentValue'] &&
      (changes['item']['currentValue']['isUpdatingSuccess'] === true ||
        changes['item']['currentValue']['isUpdatingSuccess'] === false)
    ) {
      of(true)
        .pipe(delay(2000))
        .pipe(takeUntil(this.destroy$))
        .subscribe(() => (this.showUpdateStatus = false));
    }
  }

  ngOnInit() {
    this.binGroupCodes = [
      this.binGroupCode,
      StockItemBinGroupCodeEnum.DISPUTE,
      StockItemBinGroupCodeEnum.DEFECTIVE
    ];
    this.itemForm = this.createForm(this.item);
    this.itemForm
      .get('measuredWeight')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (this.isWeightMismatch()) {
          this.itemForm.patchValue({
            binGroupCode: StockItemBinGroupCodeEnum.DISPUTE,
            remarkGroupCode: null
          });
        } else if (
          this.itemForm.get('binGroupCode').value ===
          StockItemBinGroupCodeEnum.DISPUTE
        ) {
          this.itemForm.patchValue({
            binGroupCode: this.binGroupCode,
            remarkGroupCode: null
          });
        }
      });

    this.itemForm
      .get('binGroupCode')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (value === StockItemBinGroupCodeEnum.DEFECTIVE) {
          this.itemForm.patchValue({
            remarkGroupCode: this.remarks[0].code
          });
        }
        if (value === this.binGroupCode) {
          this.itemForm.patchValue({
            binCode: StockItemBinCodeEnum.ZERO_BIN
          });
        }
      });

    if (this.isVerified) {
      this.itemForm.valueChanges
        .pipe(
          debounceTime(1000),
          takeUntil(this.destroy$)
        )
        .subscribe(() => {
          this.updateItem();
        });
    }

    if (!this.isVerified) {
      this.parentForm.push(this.itemForm);
      this.itemForm.valueChanges
        .pipe(takeUntil(this.destroy$))
        .subscribe(formValue => {
          if (
            formValue['binGroupCode'] === this.item.binGroupCode &&
            formValue['measuredWeight'] === this.item.measuredWeight
          ) {
            this.itemForm.markAsPristine();
          }
        });
    }
  }

  createForm(item: OtherReceiptItem): FormGroup {
    return new FormGroup(
      {
        id: new FormControl(item.id),
        measuredQuantity: new FormControl(item.measuredQuantity, [
          Validators.required,
          Validators.min(item.availableQuantity),
          Validators.max(item.availableQuantity)
        ]),
        measuredWeight: new FormControl(item.measuredWeight, [
          Validators.required,
          this.measuredWeightValidator()
        ]),
        binGroupCode: new FormControl(item.binGroupCode, Validators.required),
        remarkGroupCode: new FormControl(
          item.remarks && item.remarks !== '' ? item.remarks : null
        ),
        binCode: new FormControl(
          item.binCode &&
          item.binCode !== '' &&
          item.binGroupCode !== StockItemBinGroupCodeEnum.DISPUTE &&
          item.binGroupCode !== StockItemBinGroupCodeEnum.DEFECTIVE
            ? item.binCode
            : null
        )
      },
      [this.remarksValidator()]
    );
  }

  measuredWeightValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      return control.value <= 0
        ? { minZero: 'pw.otherReceiptsIssues.weightInvalidErrorMessage' }
        : null;
    };
  }

  remarksValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      return control.get('binGroupCode').value ===
        StockItemBinGroupCodeEnum.DEFECTIVE &&
        control.get('remarkGroupCode').value === null
        ? {
            remarkGroupCode:
              'pw.otherReceiptsIssues.remarksRequiredErrorMessage'
          }
        : null;
    };
  }

  isWeightMismatch = (): boolean => {
    const value = this.itemForm.get('measuredWeight').value;
    return !(
      value == null ||
      (this.item.availableWeight - this.tolerance <= value &&
        value <= this.item.availableWeight + this.tolerance)
    );
  };

  verifyItem() {
    if (this.itemForm.valid) {
      this.verify.emit(this.createItemPayload());
    } else {
      this.makeFormDirty();
    }
  }

  updateItem() {
    if (this.isVerified) {
      if (this.itemForm.valid) {
        this.update.emit(this.createItemPayload());
      } else {
        this.makeFormDirty();
      }
    }
  }

  createItemPayload(): OtherReceiptItemToUpdate {
    const itemFormValue = this.itemForm.value;
    return {
      id: this.item.id,
      newUpdate: {
        measuredWeight: itemFormValue.measuredWeight,
        binGroupCode: itemFormValue.binGroupCode,
        binCode:
          this.isVerified &&
          (itemFormValue.binGroupCode !== StockItemBinGroupCodeEnum.DISPUTE &&
            itemFormValue.binGroupCode !==
              StockItemBinGroupCodeEnum.DEFECTIVE) &&
          itemFormValue.binCode
            ? itemFormValue.binCode
            : itemFormValue.binGroupCode !==
                StockItemBinGroupCodeEnum.DISPUTE &&
              itemFormValue.binGroupCode !== StockItemBinGroupCodeEnum.DEFECTIVE
            ? StockItemBinCodeEnum.ZERO_BIN
            : itemFormValue.binGroupCode,
        remarks:
          itemFormValue.binGroupCode === StockItemBinGroupCodeEnum.DEFECTIVE
            ? itemFormValue.remarkGroupCode
            : '',
        // TODO : UIN Verification
        itemDetails: this.item.itemDetails
      },
      actualDetails: {
        binCode: this.item.binCode,
        binGroupCode: this.item.binGroupCode,
        measuredWeight: this.item.measuredWeight,
        remarks: this.item.remarks,
        // TODO : UIN Verification
        itemDetails: this.item.itemDetails
      }
    };
  }

  makeFormDirty() {
    this.itemForm.markAsDirty();
    this.itemForm.controls['measuredQuantity'].markAsDirty();
    this.itemForm.controls['measuredWeight'].markAsDirty();
    this.itemForm.controls['binGroupCode'].markAsDirty();
    this.itemForm.controls['remarkGroupCode'].markAsDirty();
    this.itemForm.controls['binCode'].markAsDirty();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (!this.isVerified) {
      this.itemForm.markAsPristine();
      this.parentForm.removeAt(
        this.parentForm.controls.findIndex(
          itemForm => itemForm.get('id').value === this.item.id
        )
      );
    }
  }
}
