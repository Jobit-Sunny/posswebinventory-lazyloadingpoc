import {
  Component,
  OnInit,
  OnDestroy,
  ElementRef,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  SearchResponse,
  SearchListComponent,
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationEventType,
  Filter,
  SortDialogService,
  FilterService,
  FilterActions,
  Column,
  SearchComponent,
  Option
} from '@poss-web/shared';
import { OtherReceiptsFacade } from '../../+receive-state/other-receipts.facade';
import { OtherReceiptsIssuesEnum } from '../../../../in-stock-management/in-stock/in-stock.enum';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { UpdateAdjustementItemPayload } from '../../+receive-state/other-receipts.actions';
import { TranslateService } from '@ngx-translate/core';
import {
  CustomErrors,
  AppsettingFacade,
  ProductCategory,
  ProductGroup,
  BinCode
} from '@poss-web/core';
import {
  ConfirmAdjustementItem,
  AdjustmentItem,
  FilterOption
} from '../../models/other-receipt.model';
import { SORT_DATA } from '../../models/other-issues.model';
import { StockItemBinGroupCodeEnum } from '../../models/other-receipt.enum';

@Component({
  selector: 'poss-web-adjustment-receipts-details',
  templateUrl: './adjustment-receipts-details.component.html',
  styleUrls: ['./adjustment-receipts-details.component.scss']
})
export class AdjustmentReceiptsDetailsComponent implements OnInit, OnDestroy {
  destroy$: Subject<null> = new Subject<null>();
  hasNotification = false;
  adjustmentSearchedItems$: Observable<AdjustmentItem[]>;
  itemsInCart$: Observable<AdjustmentItem[]>;
  adjustmentSearchedItemsCount$: Observable<number>;
  confirmAdjustementItemsResponse$: Observable<AdjustmentItem>;
  cartItemsCount = 0;
  itemIds = [];
  @ViewChild(SearchListComponent, { static: true })
  searchListRef: SearchListComponent;
  selectForm = this.fb.group({
    selectRadioButton: ['']
  });
  selectionAllSubscription: any;
  selectionAllSubject: Subject<any> = new Subject<any>();
  confirmItems: ConfirmAdjustementItem[] = [];
  selectAll = false;
  destDocNumber = null;
  totalItemCount: number;
  allItemIds: any[];
  searchForm: FormGroup;
  @ViewChild('searchBox', { static: false }) searchBox: ElementRef;
  searchedAdjustmentItemsInCart$: Observable<AdjustmentItem[]>;
  searchedAdjustmentItemsInCartResults: AdjustmentItem[];
  otherIssuesTabEnumRef = OtherReceiptsIssuesEnum;
  isSearchingAdjustmentItem$: Observable<boolean>;
  hasSearchedAdjustmentItem$: Observable<boolean>;
  isloadingAdjsutment$: Observable<boolean>;
  isItemsInCart: boolean;
  hasSearchItemInCartSearch$: Observable<boolean>;
  hideSearchResults = false;
  allItemsInCart: AdjustmentItem[];
  productCategory: { [key: string]: Filter[] };
  productGroup: { [key: string]: Filter[] };
  maxProductInList: any;
  maxSortLimit: any;
  maxFilterLimit: any;
  filterData: { [key: string]: Filter[] };
  filter: { key: string; value: any[] }[] = [];
  sortData: Column[] = [];
  sortOrder: any;
  sortBy: any;
  itemList: AdjustmentItem[];
  itemCode: string;
  lotNumber: string;
  @ViewChild(SearchComponent, { static: true })
  searchRef: SearchComponent;
  storeType: string;
  isL1L2Store: boolean;
  isL3Store: boolean;
  binGroupCode: string;
  binCodes: BinCode[] = [];
  constructor(
    private router: Router,
    private otherReceiptsFacade: OtherReceiptsFacade,
    private overlayNotification: OverlayNotificationService,
    private fb: FormBuilder,
    private translate: TranslateService,
    private sortService: SortDialogService,
    private filterService: FilterService,
    private appsettingFacade: AppsettingFacade
  ) {
    this.sortService.DataSource = SORT_DATA;
    this.searchForm = new FormGroup({
      searchValue: new FormControl('')
    });
  }

  ngOnInit() {
    this.appsettingFacade
      .getStoreType()
      .pipe(takeUntil(this.destroy$))
      .subscribe((storeType: string) => {
        if (storeType) {
          this.storeType = storeType;
          this.isL1L2Store = this.storeType === 'L1' || this.storeType === 'L2';
          this.isL3Store = this.storeType === 'L3';
          this.binGroupCode = this.isL1L2Store
            ? StockItemBinGroupCodeEnum.STN
            : StockItemBinGroupCodeEnum.PURCFA;
          this.componentInit();
        }
      });
  }
  componentInit() {
    this.searchedAdjustmentItemsInCartResults = [];
    this.resetData();
    this.otherReceiptsFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });
    this.isSearchingAdjustmentItem$ = this.otherReceiptsFacade.getIsSearchingAdjustment();
    this.hasSearchedAdjustmentItem$ = this.otherReceiptsFacade.gethasSearchedItemAdjustment();
    this.isloadingAdjsutment$ = this.otherReceiptsFacade.getIsLoadingAdjustment();
    this.hasSearchItemInCartSearch$ = this.otherReceiptsFacade.getHasSearchItemInCartSearchAdjustment();

    this.hasNotification = true;
    this.adjustmentSearchedItems$ = this.otherReceiptsFacade.getAdjustmentSearchedItems();
    this.itemsInCart$ = this.otherReceiptsFacade.getItemsInCart();
    this.otherReceiptsFacade
      .getConfirmAdjustementItemsResponse()
      .subscribe((confirmAdjustementItemsResponse: AdjustmentItem) => {
        if (confirmAdjustementItemsResponse) {
          this.destDocNumber = confirmAdjustementItemsResponse.destDocNo;
          this.showConfirmAdjustementItemsSuccessNotification();
        }
      });
    this.otherReceiptsFacade
      .getAdjustmentItemsSearchCount()
      .pipe(takeUntil(this.destroy$))
      .subscribe(count => {});
    this.otherReceiptsFacade
      .getItemsInCart()
      .pipe(takeUntil(this.destroy$))
      .subscribe(items => {
        this.allItemsInCart = items;
        this.allItemIds = [];
        this.confirmItems = [];
        this.totalItemCount = items.length;
        items.forEach(item => {
          this.confirmItems.push({
            binCode: item.binCode,
            binGroupCode: item.binGroupCode,
            itemCode: item.itemCode,
            measuredWeight: item.measuredWeight,
            value: item.stdValue,
            quantity: item.measuredQuantity
          });
          this.allItemIds.push(item.id);
        });
        this.loadItemCart();
        this.showNotifications();
      });
    this.otherReceiptsFacade
      .getAdjustmentSearchedItems()
      .pipe(takeUntil(this.destroy$))
      .subscribe(searchedItems => {
        if (searchedItems) {
          this.isItemsInCart = false;
          if (searchedItems.length === 1) {
            this.isItemsInCart = true;
            this.addToCart(searchedItems);
          }
        }
      });
    //todo remove this
    this.searchedAdjustmentItemsInCart$ = this.otherReceiptsFacade.getAdjustmentItemsInCartsSearch();
    this.searchedAdjustmentItemsInCart$
      .pipe(takeUntil(this.destroy$))
      .subscribe(items => {
        if (items) {
          this.searchedAdjustmentItemsInCartResults = items.filter(x => {
            return x.itemCode === this.searchForm.get('searchValue').value;
          });
        }
      });
    this.otherReceiptsFacade.loadProductCategories();
    this.otherReceiptsFacade.loadProductGroups();
    this.otherReceiptsFacade
      .getProductCategories()
      .subscribe((productCategories: ProductCategory[]) => {
        if (productCategories !== null) {
          this.productCategory = this.mapToFilterOptions(
            'Product Category',
            productCategories.map(productCategory => ({
              id: productCategory.productCategoryCode,
              description: productCategory.description
            }))
          );
        }
      });

    this.otherReceiptsFacade
      .getProductGroups()
      .subscribe((productGroups: ProductGroup[]) => {
        if (productGroups !== null) {
          this.productGroup = this.mapToFilterOptions(
            'Product Group',
            productGroups.map(productGroup => ({
              id: productGroup.productGroupCode,
              description: productGroup.description
            }))
          );
        }
      });
    this.appsettingFacade
      .getMaxFilterLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxFilterLimit = data;
      });

    this.appsettingFacade
      .getMaxSortLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxSortLimit = data;
      });

    this.appsettingFacade
      .getMaxProductInList()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxProductInList = data;
      });

    this.otherReceiptsFacade.loadBinCodes(this.binGroupCode);
    this.otherReceiptsFacade
      .getBinCodes()
      .pipe(takeUntil(this.destroy$))
      .subscribe((bincodes: BinCode[]) => {
        this.binCodes = bincodes;
      });
  }

  onSearch(searchResponse: SearchResponse) {
    if (this.allItemsInCart.length < this.maxProductInList) {
      this.otherReceiptsFacade.adjustmentItemSearch({
        variantCode: searchResponse.searchValue,
        lotNumber: searchResponse.lotNumber
      });
    }
  }
  addToCart(items: AdjustmentItem[]) {
    this.clearInventorySearch();
    if (this.allItemsInCart.length + items.length < this.maxProductInList) {
      this.searchListRef.reset();
      this.otherReceiptsFacade.addItemsToCart(items);
    }
    this.showNotifications();
  }
  selectChange() {
    if (this.selectForm.value.selectRadioButton === '1') {
      this.selectAllItems();
    }

    this.selectionAllSubject.next({
      selectCheckbox: true,
      enableCheckbox: true
    });
    this.selectAll = true;
    this.showNotifications();
  }
  selectAllItems() {
    this.itemIds = [];
    this.selectionAllSubject.next({
      selectCheckbox: true,
      enableCheckbox: false
    });
    this.showNotifications();
  }
  selectionEmit(selection: { selected: boolean; id: number }) {
    this.selectForm.patchValue({
      selectRadioButton: null
    });
    if (selection.selected === false) {
      this.selectForm.patchValue({
        selectRadioButton: null
      });
      const itemToRemove = selection.id;
      this.itemIds.splice(this.itemIds.indexOf(itemToRemove), 1);
    } else if (selection.selected === true) {
      this.itemIds.push(selection.id);
      if (this.itemIds.length === this.allItemIds.length) {
        this.selectForm.patchValue({
          selectRadioButton: '1'
        });
      }
    }
    this.selectAll = false;
    this.overlayNotification.close();
    this.showNotifications();
  }
  showNotifications() {
    if (
      this.totalItemCount > 0 &&
      !(this.selectForm.value.selectRadioButton === '1') &&
      this.filter.length === 0 &&
      !this.itemCode
    ) {
      this.showConfirmNotification(
        'pw.otherReceiptsIssues.confirmReceiptsNotificationMessage'
      );
    } else if (
      this.totalItemCount === 50 &&
      !(this.selectForm.value.selectRadioButton === '1')
    ) {
      this.showConfirmNotification(
        'pw.otherReceiptsIssues.confirmReceiptsNotificationCartFullMessage'
      );
    }

    if (this.selectForm.value.selectRadioButton === '1') {
      this.RemoveProductsOverlay(
        this.allItemIds.length,
        'pw.otherReceiptsIssues.removeProductNotifictionMessage'
      );
    }
    if (this.itemIds.length > 0) {
      this.RemoveProductsOverlay(
        this.itemIds.length,
        'pw.otherReceiptsIssues.removeProductNotifictionMessage'
      );
    }
    if (this.totalItemCount === 0) {
      this.overlayNotification.close();
    }
  }
  showConfirmAdjustementItemsSuccessNotification() {
    this.hasNotification = true;
    this.overlayNotification
      .show({
        type: OverlayNotificationType.SIMPLE,
        hasBackdrop: true,
        hasClose: true,
        message: 'Document Number is' + ' ' + this.destDocNumber
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        this.hasNotification = false;
        if (event.eventType === OverlayNotificationEventType.CLOSE) {
          this.router.navigate([
            'inventory/instock/other-receipts-issues-list/otherreceipts/Exhibition'
          ]);
        }
      });
  }
  RemoveProductsOverlay(count: number, key: any) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            buttonText: 'Remove',
            message: translatedMessage.replace('{0}', count.toString())
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.otherReceiptsFacade.removeMultipleAdjustementItems(
                this.selectForm.value.selectRadioButton === '1'
                  ? this.allItemIds
                  : this.itemIds
              );
              this.selectAll = false;
              this.itemIds = [];
              this.overlayNotification.close();
              this.showNotifications();
              this.selectForm.patchValue({
                selectRadioButton: null
              });
              this.clearSearchItems();
            }
          });
      });
  }
  showConfirmNotification(key: string) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            message:
              this.totalItemCount === 50
                ? translatedMessage.replace(
                    '{0}',
                    this.totalItemCount.toString()
                  )
                : translatedMessage,
            buttonText: 'CONFIRM',
            hasRemarks: true,
            isRemarksMandatory: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.otherReceiptsFacade.confirmAdjustementItems({
                items: this.confirmItems,
                remarks: event.data,
                type: OtherReceiptsIssuesEnum.ADJUSTMENT_TYPE
              });
            }
          });
      });
  }
  clearAll() {
    this.itemIds = [];
    this.selectForm.patchValue({
      selectRadioButton: null
    });
    this.selectionAllSubject.next({
      selectCheckbox: false,
      enableCheckbox: true
    });
    this.overlayNotification.close();
    this.showNotifications();
  }

  back() {
    this.router.navigate([
      'inventory/instock/other-receipts-issues-list/otherreceipts/Exhibition'
    ]);
  }
  updateItems(updateAdjustementItem: UpdateAdjustementItemPayload) {
    this.otherReceiptsFacade.updateAdjustementItem({
      itemId: updateAdjustementItem.itemId,
      items: updateAdjustementItem.items
    });
  }
  removeItem(item: AdjustmentItem) {
    this.otherReceiptsFacade.removeAdjustementItem(item);
    this.selectAll = false;
    //this.cartCount();
    //this.showNotifications();
  }
  clearSearchItems() {
    this.itemCode = null;
    this.lotNumber = null;
    this.loadItemCart();
    /* this.searchForm.get('searchValue').reset();
    this.hideSearchResults = false;
    this.otherReceiptsFacade.clearSearchCartItemAdjustment(); */
    this.overlayNotification.close();
    this.showNotifications();
    this.clearAll();
  }
  resetData() {
    this.otherReceiptsFacade.resetAdjustmentReceiptData();
  }
  clearInventorySearch() {
    this.itemCode = null;
    this.lotNumber = null;
    this.sortBy = null;
    this.sortOrder = null;
    this.sortData = [];
    this.filter = [];
    this.filterData = {};
    if (this.searchListRef) {
      this.searchListRef.reset();
    }
    if (this.searchRef) {
      this.searchRef.reset();
    }
    this.loadItemCart();
    this.otherReceiptsFacade.clearSearchInventoryItemAdjustment();
  }
  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {});
  }
  openFilter() {
    this.filterService.DataSource = {
      ...this.productCategory,
      ...this.productGroup
    };
    this.filterService
      .openDialog(this.maxFilterLimit, this.filterData)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (filterResult: {
          data: { [key: string]: Filter[] };
          actionfrom: string;
        }) => {
          if (filterResult.actionfrom === FilterActions.APPLY) {
            const filterData = filterResult.data;
            if (filterData == null) {
              this.filterData = {};
            } else {
              this.filterData = filterData;
            }
            this.filter = [];
            if (filterData) {
              let filterValues = [];
              if (filterData['Product Group']) {
                filterData['Product Group'].forEach(value => {
                  filterValues.push(value.description.toLowerCase());
                });
                if (filterValues.length > 0) {
                  this.filter.push({
                    key: 'productGroup',
                    value: filterValues
                  });
                }
              }
              filterValues = [];
              if (filterData['Product Category']) {
                filterData['Product Category'].forEach(value => {
                  filterValues.push(value.description.toLowerCase());
                });
                if (filterValues.length > 0) {
                  this.filter.push({
                    key: 'productCategory',
                    value: filterValues
                  });
                }
              }
            }

            // this.resetSelection();
            this.loadItemCart();
          }
        }
      );
  }
  openSort() {
    this.sortService
      .openDialog(this.maxSortLimit, this.sortData)
      .pipe(takeUntil(this.destroy$))
      .subscribe((sortResult: { data: Column[]; actionfrom: string }) => {
        if (sortResult.actionfrom === FilterActions.APPLY) {
          const sortData = sortResult.data;
          if (sortData == null || sortData.length === 0) {
            this.sortData = [];
            this.sortOrder = null;
            this.sortBy = null;
          } else {
            this.sortData = sortData;
            if (sortData.length > 0) {
              if (sortData[0].id === 0) {
                this.sortBy = 'availableWeight';
              } else if (sortData[0].id === 1) {
                this.sortBy = 'availableQuantity';
              }
              this.sortOrder = sortData[0].sortAscOrder ? 'ASC' : 'DESC';
            }
          }
          this.loadItemCart();
        }
      });
  }
  loadItemCart() {
    this.itemList = this.allItemsInCart;
    if (this.itemCode) {
      this.itemList = this.itemList.filter(
        (item: AdjustmentItem) =>
          item.itemCode.toLowerCase() === this.itemCode.toLowerCase() &&
          (this.lotNumber
            ? this.lotNumber.toLowerCase() === item.lotNumber.toLowerCase()
            : true)
      );
    }
    if (this.filter.length > 0) {
      for (let i = 0; i < this.filter.length; i++) {
        this.itemList = this.itemList.filter((item: AdjustmentItem) => {
          return this.filter[i].value.includes(
            typeof item[this.filter[i].key] === 'string'
              ? item[this.filter[i].key].toLowerCase()
              : item[this.filter[i].key]
          );
        });
      }
    }
    if (this.sortBy) {
      const issueItemsSort = [...this.itemList].sort(
        (item1: AdjustmentItem, item2: AdjustmentItem) => {
          if (item1[this.sortBy] === item2[this.sortBy]) {
            return 0;
          }
          return item1[this.sortBy] < item2[this.sortBy]
            ? this.sortOrder === 'ASC'
              ? -1
              : 1
            : this.sortOrder === 'ASC'
            ? 1
            : -1;
        }
      );
      this.itemList = issueItemsSort;
    }

    this.clearAll();
    this.overlayNotification.close();
    this.showNotifications();
  }
  searchInCart(searchData: SearchResponse) {
    this.itemCode = searchData.searchValue;
    this.lotNumber = searchData.lotNumber;
    this.loadItemCart();
  }
  mapToFilterOptions(
    key: string,
    options: FilterOption[]
  ): { [key: string]: Filter[] } {
    return {
      [key]: options.map((option, index) => ({
        id: option.id,
        description: option.description,
        selected: false
      }))
    };
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
