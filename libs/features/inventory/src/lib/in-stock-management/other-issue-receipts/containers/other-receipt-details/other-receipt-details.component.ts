import {
  FormGroup,
  AbstractControl,
  FormControl,
  ValidatorFn
} from '@angular/forms';
import {
  AppsettingFacade,
  ShortcutService,
  Command,
  CustomErrors,
  BinCode,
  Lov,
  ProductCategory,
  ProductGroup
} from '@poss-web/core';

import { Observable, Subject, Subscription } from 'rxjs';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';

import { takeUntil, debounceTime, filter } from 'rxjs/operators';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventType,
  OverlayNotificationEventRef,
  SearchResponse,
  SearchComponent,
  Filter,
  FilterService,
  SortDialogService,
  FilterActions,
  Column,
  ErrorEnums,
  ErrorTranslateKeyMap
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';

import * as moment from 'moment';

import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  PageEvent
} from '@angular/material';
import { OtherReceiptsFacade } from '../../+receive-state/other-receipts.facade';
import {
  OtherReceiptsIssuesEnum,
  StockStatusEnum,
  ItemStatusEnum
} from '../../../../in-stock-management/in-stock/in-stock.enum';

import {
  OtherReceiptsModel,
  OtherReceiptItem,
  OtherReceiptItemToUpdate,
  ConfirmOtherReceive,
  FilterOption
} from '../../models/other-receipt.model';
import { StockItemBinGroupCodeEnum } from '../../models/other-receipt.enum';

export const DATE_FORMATS = {
  parse: {
    dateInput: 'DD-MMM-YYYY'
  },
  display: {
    dateInput: 'DD-MMM-YYYY',
    monthYearLabel: 'MMM-YYYY',
    dateA11yLabel: 'DD-MMM-YYYY',
    monthYearA11yLabel: 'DD-MMM-YYYY'
  }
};

export enum ITEM_STATUS {
  VERIFIED = 'VERIFIED',
  ISSUED = 'ISSUED'
}
const SORT_DATA = [
  {
    id: 0,
    sortByColumnName: 'Item Weight',
    sortAscOrder: false
  },
  {
    id: 1,
    sortByColumnName: 'Item Quantity',
    sortAscOrder: false
  }
];
const SEARCH_SHORTCUT_KEY_F2 = 'OtherReceiptDetailsComponent.F2';

@Component({
  selector: 'poss-web-other-receipt-details',
  templateUrl: './other-receipt-details.component.html',
  styleUrls: ['./other-receipt-details.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    { provide: MAT_DATE_FORMATS, useValue: DATE_FORMATS }
  ]
})
export class OtherReceiptDetailsComponent implements OnInit, OnDestroy {
  selectedStock: any;
  tab: string;
  pageSize = 5;
  pageIndex = 0;

  nonVerifiedItemsPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };
  verifiedItemsPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };
  initailPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };
  searchedItems$: Observable<OtherReceiptItem[]>;
  isSearchingItems$: Observable<boolean>;
  hasSearchedItems$: Observable<boolean>;

  nonVerifiedItemsTotalCount = 0;
  verifiedItemsTotalCount = 0;

  itemsTotalCountLoaded$: Observable<boolean>;
  itemsTotalCountLoading$: Observable<boolean>;

  nonVerifiedItems$: Observable<OtherReceiptItem[]>;
  verifiedItems$: Observable<OtherReceiptItem[]>;
  remarks$: Observable<Lov[]>;
  tolerance$: Observable<number>;

  type: string;
  stockId: string;
  storeType: string;

  maxDate = moment();

  hasError$: Observable<string>;
  isLoading$: Observable<boolean>;
  isNonVerifiedItemsLoading$: Observable<boolean>;
  isVerifiedItemsLoading$: Observable<boolean>;

  isAssigningBinToAllItems$: Observable<boolean>;
  isverifiedCountShownOnce = false;
  hasNotification = false;

  isLoadingSelectedStock$: Observable<boolean>;
  isLoadingBinGroups$: Observable<boolean>;
  isLoadingRemarks$: Observable<boolean>;
  isLoadingTolerance$: Observable<boolean>;
  selectedSortBy: string = null;
  selectedProperty: string = null;
  currenTab: string;
  stockType: string;
  @ViewChild(SearchComponent, { static: true })
  searchRef: SearchComponent;
  stockForm: FormGroup;

  destroy$: Subject<null> = new Subject<null>();
  binGroupCode: string;
  binCodes$: Observable<BinCode[]>;
  itemCode: string;
  lotNumber: string;
  maxFilterLimit: any;
  maxSortLimit: any;
  productCategory: { [key: string]: Filter[] };
  productGroup: { [key: string]: Filter[] };
  filterDataNonVerifiedProducts: Observable<{ [key: string]: Filter[] }>;
  filterTabDataNonVerifiedProducts: { [key: string]: Filter[] };
  filterDataVerifiedProducts: Observable<{ [key: string]: Filter[] }>;
  filterTabDataVerifiedProducts: { [key: string]: Filter[] };
  sortDataNonVerifiedProducts: Observable<Column[]>;
  sortDataVerifiedProducts: Observable<Column[]>;
  sortTabDataNonVerifiedProducts: Column[];
  sortTabDataVerifiedProducts: Column[];
  filterData: {};
  filterNonVerified: { key: string; value: any[] }[] = [];
  filterVerified: { key: string; value: any[] }[] = [];
  sortData: any[];
  sortOrder: any;
  sortBy: any;
  sortMapNonverified = new Map();
  sortMapVerified = new Map();
  isParentFormDirty = false;
  itemsCountNonVerified$: Observable<number>;
  itemsCountVerified$: Observable<number>;
  isNonVerifiedItemsLoaded$: Observable<boolean>;
  isVerifiedItemsLoaded$: Observable<boolean>;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private otherReceiptsFacade: OtherReceiptsFacade,
    private appsettingFacade: AppsettingFacade,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService,
    private shortcutService: ShortcutService,
    private filterService: FilterService,
    private sortDialogService: SortDialogService
  ) {
    this.sortDialogService.DataSource = SORT_DATA;
    this.stockForm = new FormGroup(
      {
        searchValue: new FormControl(''),
        binCode: new FormControl(''),
        courierReceivedDate: new FormControl(moment()),
        reasonForDelay: new FormControl(null)
      },
      [this.reasonForDelayValidator()]
    );

    this.stockForm
      .get('binCode')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe((value: string) => {
        if (value && value !== '') {
          this.overlayNotification.close();
          this.hasNotification = false;
          this.showProgressNotification();
          this.otherReceiptsFacade.assignBinToAllItems({
            id: this.selectedStock.id,
            data: {
              binCode: value
            },
            transactionType: this.stockType
          });
        }
      });
  }

  ngOnInit() {
    // this.stockReceiveFacade.resetError();
    // TODO : change logic. Route state
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        const tab = this.activatedRoute.snapshot.params['tab'];
        this.changeTab(tab);
        this.showNotifications();
      });

    this.stockId = this.activatedRoute.snapshot.params['id'];
    this.type = this.activatedRoute.snapshot.params['type'];
    this.tab = this.activatedRoute.snapshot.params['tab'];

    // TODO : show notfound page
    if (!this.tab || !(this.tab === 'nonverified' || this.tab === 'verified')) {
      this.router.navigate(['404']);
    }

    this.appsettingFacade
      .getStoreType()
      .pipe(takeUntil(this.destroy$))
      .subscribe((storeType: string) => {
        if (storeType) {
          this.storeType = storeType;
          this.binGroupCode = this.isL1L2Store()
            ? StockItemBinGroupCodeEnum.STN
            : StockItemBinGroupCodeEnum.PURCFA;

          this.appsettingFacade
            .getPageSize()
            .pipe(takeUntil(this.destroy$))
            .subscribe(data => {
              const pageSize = JSON.parse(data);
              this.initailPageEvent.pageSize = pageSize;
              this.nonVerifiedItemsPageEvent = this.initailPageEvent;
              this.verifiedItemsPageEvent = this.initailPageEvent;
              this.componentInit();
            });
        }
        this.appsettingFacade
          .getMaxFilterLimit()
          .pipe(takeUntil(this.destroy$))
          .subscribe(data => {
            this.maxFilterLimit = data;
          });

        this.appsettingFacade
          .getMaxSortLimit()
          .pipe(takeUntil(this.destroy$))
          .subscribe(data => {
            this.maxSortLimit = data;
          });
      });

    this.shortcutService.commands
      .pipe(takeUntil(this.destroy$))
      .subscribe(command => this.shortcutEventHandler(command));
  }

  reasonForDelayValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const to = moment();
      const from = control.get('courierReceivedDate').value;
      const dateDiff = moment.duration(to.diff(from)).asHours();
      if (dateDiff > 48) {
        if (
          control.get('reasonForDelay').value === null ||
          control.get('reasonForDelay').value === ''
        ) {
          control.get('reasonForDelay').markAsTouched();
          control
            .get('reasonForDelay')
            .setErrors({ reasonForDelay: 'Reason For Delay is Required' });
          return {
            hasRasonForDelay: 'hasRasonForDelay',
            reasonForDelay: 'pw.stockReceive.reasonForDelayRequiredErrorMessage'
          };
        } else if (
          control.get('reasonForDelay').value.length < 30 ||
          control.get('reasonForDelay').value.length > 250
        ) {
          control.get('reasonForDelay').markAsTouched();
          control.get('reasonForDelay').setErrors({
            reasonForDelay:
              'pw.otherReceiptsIssues.reasonForDelaylimitErrorMessage'
          });

          return {
            hasRasonForDelay: 'hasRasonForDelay',
            reasonForDelay:
              'pw.otherReceiptsIssues.reasonForDelaylimitErrorMessage'
          };
        } else {
          return {
            hasRasonForDelay: 'hasRasonForDelay'
          };
        }
      } else {
        return null;
      }
    };
  }

  /**
   * method to handle shortcut commands
   * @param command: shortcut command
   */
  shortcutEventHandler(command: Command) {
    /* switch (command.name) {
      case SEARCH_SHORTCUT_KEY_F2: {
        if (this.searchRef) {
          this.searchRef.focus();
        }
        break;
      }
    } */
    if (command.name === SEARCH_SHORTCUT_KEY_F2) {
      if (this.searchRef) {
        this.searchRef.focus();
      }
    }
  }

  componentInit() {
    this.otherReceiptsFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          // TODO : check all errors after implemention
          this.errorHandler(error);
        }
      });

    this.isLoadingSelectedStock$ = this.otherReceiptsFacade.getIsLoadingSelectedStock();
    this.isLoadingBinGroups$ = this.otherReceiptsFacade.getIsLoadingBinGroups();
    this.isLoadingRemarks$ = this.otherReceiptsFacade.getIsLoadingRemarks();
    this.isLoadingTolerance$ = this.otherReceiptsFacade.getIsLoadingTolerance();

    this.hasError$ = this.otherReceiptsFacade.getHasError();
    this.isLoading$ = this.otherReceiptsFacade.getIsLoading();

    this.nonVerifiedItems$ = this.otherReceiptsFacade.getNonVerifiedItems();
    this.verifiedItems$ = this.otherReceiptsFacade.getVerifiedItems();
    this.itemsCountNonVerified$ = this.otherReceiptsFacade.getItemsCountNonVerified();
    this.itemsCountVerified$ = this.otherReceiptsFacade.getItemsCountVerified();
    this.isNonVerifiedItemsLoading$ = this.otherReceiptsFacade.getIsNonVerifiedItemsLoading();
    this.isVerifiedItemsLoading$ = this.otherReceiptsFacade.getIsVerifiedItemsLoading();
    this.itemsTotalCountLoaded$ = this.otherReceiptsFacade.getItemsTotalCountLoaded();
    this.itemsTotalCountLoading$ = this.otherReceiptsFacade.getIsItemsTotalCountLoading();
    this.isSearchingItems$ = this.otherReceiptsFacade.getIsSearchingItems();
    this.hasSearchedItems$ = this.otherReceiptsFacade.getHasSearchedItems();
    this.isNonVerifiedItemsLoaded$ = this.otherReceiptsFacade.getIsNonVerifiedItemsLoaded();
    this.isVerifiedItemsLoaded$ = this.otherReceiptsFacade.getIsVerifiedItemsLoaded();
    this.binCodes$ = this.otherReceiptsFacade.getBinCodes();
    this.remarks$ = this.otherReceiptsFacade.getRemarks();
    this.tolerance$ = this.otherReceiptsFacade.getTolerance();

    this.otherReceiptsFacade.loadBinCodes(this.binGroupCode);
    this.otherReceiptsFacade.loadRemarks();
    this.otherReceiptsFacade.loadTolerance();

    if (this.type === OtherReceiptsIssuesEnum.LOAN) {
      this.stockType = OtherReceiptsIssuesEnum.LOAN;
    } else if (this.type === OtherReceiptsIssuesEnum.EXHIBITION) {
      this.stockType = OtherReceiptsIssuesEnum.EXHIBITION_TYPE;
    }
    //done
    this.otherReceiptsFacade.loadSelectedStock({
      id: this.stockId,
      transactionType: this.stockType
    });

    this.otherReceiptsFacade
      .getSelectedStock()
      .pipe(takeUntil(this.destroy$))
      .subscribe((stockTransferNote: OtherReceiptsModel) => {
        if (stockTransferNote) {
          if (
            stockTransferNote.status.toLowerCase() !==
            StockStatusEnum.ISSUED.toLowerCase()
          ) {
            this.router.navigate(['404']);
          } else {
            this.selectedStock = stockTransferNote;
            this.initialLoad();
          }
        }
      });

    this.otherReceiptsFacade
      .getNonVerifiedItemsTotalCount()
      .pipe(takeUntil(this.destroy$))
      .subscribe((count: number) => {
        this.nonVerifiedItemsTotalCount = count;
        //this.loadNonVerifiedItems();
        this.showNotifications();
      });

    this.otherReceiptsFacade
      .getVerifiedItemsTotalCount()
      .pipe(takeUntil(this.destroy$))
      .subscribe((count: number) => {
        this.verifiedItemsTotalCount = count;
        this.showNotifications();
      });

    this.itemsTotalCountLoaded$
      .pipe(takeUntil(this.destroy$))
      .subscribe((isLoaded: boolean) => {
        if (isLoaded) {
          this.showNotifications();
        }
      });

    this.otherReceiptsFacade
      .getUpdateItemSuccess()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isSuccess: boolean) => {
        if (isSuccess) {
          this.showNotifications();
        }
      });
    this.otherReceiptsFacade
      .getVerifyItemSuccess()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isVerified: boolean) => {
        if (isVerified) {
          this.loadNonVerifiedItems();
        }
      });
    this.otherReceiptsFacade
      .getIsVerifyingAllItemSuccess()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isSuccess: boolean) => {
        if (isSuccess) {
          this.changeTab('verified');
        } else if (isSuccess === false) {
          //this.showVerifyAllFailureNotification();
        }
      });

    this.isAssigningBinToAllItems$ = this.otherReceiptsFacade.getIsAssigningBinToAllItems();

    this.otherReceiptsFacade
      .getIsAssigningBinToAllItemsSuccess()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isSuccess: boolean) => {
        const binCode = this.stockForm.get('binCode').value;
        if (isSuccess) {
          this.loadVerifiedItems();
          this.showAssignBinToAllSuccessNotification(binCode);
          this.stockForm.get('binCode').reset();
        } else if (isSuccess === false) {
          //this.showAssignBinToAllFailureNotification(binCode);
          this.stockForm.get('binCode').reset();
        }
      });

    this.otherReceiptsFacade
      .getConfirmedStock()
      .pipe(takeUntil(this.destroy$))
      .subscribe((confirmedStock: any) => {
        if (confirmedStock) {
          this.showConfirmReceiveSuccessNotification(confirmedStock.destDocNo);
        }
      });

    this.otherReceiptsFacade.loadProductCategories();
    this.otherReceiptsFacade.loadProductGroups();
    this.otherReceiptsFacade
      .getProductCategories()
      .subscribe((productCategories: ProductCategory[]) => {
        if (productCategories !== null) {
          this.productCategory = this.mapToFilterOptions(
            'Product Category',
            productCategories.map(productCategory => ({
              id: productCategory.productCategoryCode,
              description: productCategory.description
            }))
          );
        }
      });

    this.otherReceiptsFacade
      .getProductGroups()
      .subscribe((productGroups: ProductGroup[]) => {
        if (productGroups !== null) {
          this.productGroup = this.mapToFilterOptions(
            'Product Group',
            productGroups.map(productGroup => ({
              id: productGroup.productGroupCode,
              description: productGroup.description
            }))
          );
        }
      });

    this.filterDataNonVerifiedProducts = this.otherReceiptsFacade.getfilterDataNonVerifiedProducts();
    this.filterDataNonVerifiedProducts.subscribe(filterValue => {
      this.filterTabDataNonVerifiedProducts = filterValue;
    });
    this.filterDataVerifiedProducts = this.otherReceiptsFacade.getfilterDataVerifiedProducts();
    this.filterDataVerifiedProducts.subscribe(filterValue => {
      this.filterTabDataVerifiedProducts = filterValue;
    });

    this.sortDataNonVerifiedProducts = this.otherReceiptsFacade.getSortDataNonVerifiedProducts();
    this.sortDataNonVerifiedProducts.subscribe(sortValue => {
      this.sortTabDataNonVerifiedProducts = sortValue;
    });
    this.sortDataVerifiedProducts = this.otherReceiptsFacade.getSortDataVerifiedProducts();
    this.sortDataVerifiedProducts.subscribe(sortValue => {
      this.sortTabDataVerifiedProducts = sortValue;
    });
  }

  initialLoad() {
    this.otherReceiptsFacade.loadItemsTotalCount({
      transactionType: this.stockType,
      id: this.selectedStock.id
    });

    if (this.tab === 'nonverified') {
      this.loadNonVerifiedItems();
    } else if (this.tab === 'verified') {
      this.loadVerifiedItems();
    } else {
      // TODO : Throw an error
    }
  }

  isL1L2Store(): boolean {
    return this.storeType === 'L1' || this.storeType === 'L2';
  }

  isL3Store(): boolean {
    return this.storeType === 'L3';
  }

  changeTab(newTab: string) {
    if (this.tab !== newTab) {
      this.tab = newTab;

      this.clearSearchItems();
      this.hasNotification = false;

      this.router
        .navigate(['..', this.tab], {
          relativeTo: this.activatedRoute
        })
        .then(isTabChangeSuccess => {
          if (isTabChangeSuccess) {
            this.showNotifications();
          }
        });

      if (this.tab === 'nonverified') {
        this.nonVerifiedItemsPageEvent = this.initailPageEvent;
        this.loadNonVerifiedItems();
      } else if (this.tab === 'verified') {
        this.verifiedItemsPageEvent = this.initailPageEvent;
        this.loadVerifiedItems();
      } else {
        // TODO : Throw an error
      }
    }
  }

  /**
   * Search searchItems based on varient code
   */
  searchItems(searchData: SearchResponse) {
    this.itemCode = searchData.searchValue;
    this.lotNumber = searchData.lotNumber;
    this.initialLoad();
  }

  /**
   * clears the search result
   */
  clearSearchItems() {
    this.itemCode = null;
    this.lotNumber = null;
    if (this.searchRef) {
      this.searchRef.reset();
    }
    this.initialLoad();
    //this.otherReceiptsFacade.clearSearchItems();
    this.showNotifications();
  }

  loadNonVerifiedItems() {
    if (this.selectedStock)
      this.otherReceiptsFacade.loadNonVerifiedItems({
        id: this.selectedStock.id,
        pageIndex: this.nonVerifiedItemsPageEvent.pageIndex,
        pageSize: this.nonVerifiedItemsPageEvent.pageSize,
        sortBy: this.selectedSortBy,
        property: this.selectedProperty,
        transactionType: this.stockType,
        itemCode: this.itemCode,
        lotNumber: this.lotNumber,
        sort: this.sortMapNonverified,
        filter: this.filterNonVerified
      });
  }

  loadVerifiedItems() {
    if (this.selectedStock)
      this.otherReceiptsFacade.loadVerifiedItems({
        id: this.selectedStock.id,
        pageIndex: this.verifiedItemsPageEvent.pageIndex,
        pageSize: this.verifiedItemsPageEvent.pageSize,
        sortBy: this.selectedSortBy,
        property: this.selectedProperty,
        transactionType: this.stockType,
        itemCode: this.itemCode,
        lotNumber: this.lotNumber,
        sort: this.sortMapVerified,
        filter: this.filterVerified
      });
  }

  verifyItem(itemToUpdate: OtherReceiptItemToUpdate) {
    this.otherReceiptsFacade.verifyItem({
      id: this.selectedStock.id,
      itemId: itemToUpdate.id,
      newUpdate: itemToUpdate.newUpdate,
      actualDetails: itemToUpdate.actualDetails,
      transactionType: this.stockType
    });
  }

  updateItem(itemToUpdate: OtherReceiptItemToUpdate) {
    this.otherReceiptsFacade.updateItem({
      id: this.selectedStock.id,
      itemId: itemToUpdate.id,
      newUpdate: itemToUpdate.newUpdate,
      actualDetails: itemToUpdate.actualDetails,
      transactionType: this.stockType
    });
  }

  paginateNonVerifedItems(event: PageEvent) {
    this.nonVerifiedItemsPageEvent = event;
    this.loadNonVerifiedItems();
  }

  paginateVerifedItems(event: PageEvent) {
    this.verifiedItemsPageEvent = event;
    this.loadVerifiedItems();
  }

  onParentFormDirty(isDirty) {
    this.isParentFormDirty = isDirty;
    this.showNotifications();
  }

  showNotifications() {
    this.overlayNotification.close();
    /* if (
      this.stockForm.get('searchValue').value == null ||
      this.stockForm.get('searchValue').value === ''
    ) {
      if (this.nonVerifiedItemsTotalCount !== 0 && this.tab === 'nonverified') {
        this.showVerifyAllNotification(this.nonVerifiedItemsTotalCount);
      }
      if (this.verifiedItemsTotalCount !== 0 && this.tab === 'verified') {
        if (this.nonVerifiedItemsTotalCount === 0) {
          this.showConfirmReceiveNotificationTranslator();
        } else if (!this.isverifiedCountShownOnce) {
          this.showVerifiedItemCountNotification(this.verifiedItemsTotalCount);
        }
      }
    } */
    if (
      !this.isParentFormDirty &&
      this.nonVerifiedItemsTotalCount !== 0 &&
      this.tab === OtherReceiptsIssuesEnum.NON_VERIFIED &&
      (this.itemCode === null || this.itemCode === undefined) &&
      this.filterNonVerified.length === 0
    ) {
      this.showVerifyAllNotification(this.nonVerifiedItemsTotalCount);
    }
    if (
      this.verifiedItemsTotalCount !== 0 &&
      this.tab === OtherReceiptsIssuesEnum.VERIFIED
    ) {
      if (
        this.nonVerifiedItemsTotalCount === 0 &&
        (this.itemCode === null || this.itemCode === undefined) &&
        this.filterVerified.length === 0
      ) {
        this.showConfirmReceiveNotificationTranslator();
      } else if (!this.isverifiedCountShownOnce) {
        this.showVerifiedItemCountNotification(this.verifiedItemsTotalCount);
      }
    }
  }

  showVerifyAllNotification(count: number = 0) {
    const key =
      'pw.stockReceiveNotificationMessages.nonVerifiedProductsCountMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            message: count + ' ' + translatedMessage,
            buttonText: 'VERIFY ALL'
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.showProgressNotification();
              this.otherReceiptsFacade.verifyAllItems({
                id: this.selectedStock.id,
                // TODO : no need to send everything
                data: {
                  binCode: '',
                  status: ItemStatusEnum.VERIFIED
                },
                transactionType: this.stockType
              });
            }
          });
      });
  }

  showConfirmReceiveNotificationTranslator(
    key: string = 'pw.stockReceiveNotificationMessages.confirmReceiveMessage'
  ) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.showConfirmReceiveNotification(translatedMessage);
      });
  }

  showConfirmReceiveNotification(message: string) {
    this.hasNotification = true;
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ACTION,
        message: message,
        buttonText: 'CONFIRM RECEIVE',
        hasRemarks: true
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        this.hasNotification = false;

        if (event.eventType === OverlayNotificationEventType.TRUE) {
          if (
            !(this.isL1L2Store() && this.stockForm.hasError('reasonForDelay'))
          ) {
            this.showProgressNotification();
            let confirmReceiveData: ConfirmOtherReceive;
            if (this.isL1L2Store()) {
              confirmReceiveData = {
                courierReceivedDate: (this.stockForm.value[
                  'courierReceivedDate'
                ] as moment.Moment).format(),
                reasonForDelay: this.stockForm.value['reasonForDelay']
                  ? this.stockForm.value['reasonForDelay']
                  : '',
                remarks: event.data
              };
            } else if (this.isL3Store()) {
              confirmReceiveData = {
                receivedDate: (this.stockForm.value[
                  'courierReceivedDate'
                ] as moment.Moment).format(),
                remarks: event.data
              };
            }
            this.otherReceiptsFacade.confirmStock({
              id: this.selectedStock.id,
              confirmReceive: confirmReceiveData,
              transactionType: this.stockType
            });
          } else {
            this.showConfirmReceiveNotificationTranslator(
              'pw.stockReceiveNotificationMessages.reasonForDelayMessage'
            );
          }
        }
      });
  }

  showVerifiedItemCountNotification(count: number) {
    const key =
      'pw.stockReceiveNotificationMessages.verifiedProductsCountMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.TIMER,
            message: count + ' ' + translatedMessage
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
            if (event.eventType === OverlayNotificationEventType.CLOSE) {
              this.isverifiedCountShownOnce = true;
            }
          });
      });
  }

  showVerifyAllFailureNotification() {
    const key = 'pw.stockReceiveNotificationMessages.verifyAllFailedMessge';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.SIMPLE,
            message: translatedMessage,
            hasClose: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.showFailureNotification(event);
          });
      });
  }

  showAssignBinToAllSuccessNotification(binCode: string) {
    const key = 'pw.stockReceiveNotificationMessages.assignBinAllSuccessMessge';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.showConfirmReceiveNotification(
          translatedMessage.replace('{0}', binCode)
        );
      });
  }

  showAssignBinToAllFailureNotification(binCode: string) {
    const key = 'pw.stockReceiveNotificationMessages.assignBinAllFailedMessge';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.SIMPLE,
            message: translatedMessage.replace('{0}', binCode),
            hasClose: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.showFailureNotification(event);
          });
      });
  }

  showProgressNotification() {
    const key = 'pw.stockReceiveNotificationMessages.progressMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.PROGRESS,
            message: translatedMsg,
            hasBackdrop: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
          });
      });
  }

  showConfirmReceiveSuccessNotification(documentNumber: number) {
    const key =
      'pw.stockReceiveNotificationMessages.confirmReceiveSuccessMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.SIMPLE,
            message: translatedMsg + ' ' + documentNumber,
            hasBackdrop: true,
            hasClose: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
            if (event.eventType === OverlayNotificationEventType.CLOSE) {
              this.router.navigate([
                'inventory/instock/other-receipts-issues-list/otherreceipts/' +
                  this.type
              ]);
            }
          });
      });
  }

  errorHandler(error: CustomErrors) {
    if (
      error.code === ErrorEnums.ERR_INV_005 ||
      error.code === ErrorEnums.ERR_INV_009
    ) {
      this.showConfirmReceiveNotificationTranslator(
        ErrorTranslateKeyMap.get(error.code)
      );
    } else {
      this.overlayNotification
        .show({
          type: OverlayNotificationType.ERROR,
          hasClose: true,
          error: error,
          hasBackdrop:
            error.code === ErrorEnums.ERR_INV_013 ||
            error.code === ErrorEnums.ERR_INV_029
        })
        .events.pipe(takeUntil(this.destroy$))
        .subscribe((event: OverlayNotificationEventRef) => {
          this.hasNotification = false;
          if (event.eventType === OverlayNotificationEventType.CLOSE) {
            if (
              error.code === ErrorEnums.ERR_INV_013 ||
              error.code === ErrorEnums.ERR_INV_029
            ) {
              this.router.navigate([
                'inventory/instock/other-receipts-issues-list/otherreceipts/' +
                  this.type
              ]);
            } else {
              this.showNotifications();
            }
          }
        });
    }
  }
  selectedSort(sortSelection: string) {
    if (this.tab === 'nonverified') {
      this.otherReceiptsFacade.loadNonVerifiedItems({
        id: this.selectedStock.id,
        pageIndex: this.nonVerifiedItemsPageEvent.pageIndex,
        pageSize: this.nonVerifiedItemsPageEvent.pageSize,
        sortBy: 'itemWeight',
        property: sortSelection,
        transactionType: this.stockType
      });
    } else {
      this.otherReceiptsFacade.loadVerifiedItems({
        id: this.selectedStock.id,
        pageIndex: this.verifiedItemsPageEvent.pageIndex,
        pageSize: this.verifiedItemsPageEvent.pageSize,
        sortBy: 'itemWeight',
        property: sortSelection,
        transactionType: this.stockType
      });
    }
  }
  openFilter(): void {
    this.filterService.DataSource = {
      ...this.productCategory,
      ...this.productGroup
    };
    let filterTabData: { [key: string]: Filter[] } = {};
    if (this.tab === OtherReceiptsIssuesEnum.NON_VERIFIED) {
      filterTabData = this.filterTabDataNonVerifiedProducts;
    } else if (this.tab === OtherReceiptsIssuesEnum.VERIFIED) {
      filterTabData = this.filterTabDataVerifiedProducts;
    }
    this.filterService
      .openDialog(this.maxFilterLimit, filterTabData)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (filterResult: {
          data: { [key: string]: Filter[] };
          actionfrom: string;
        }) => {
          if (filterResult.actionfrom === FilterActions.APPLY) {
            if (this.tab === OtherReceiptsIssuesEnum.NON_VERIFIED) {
              this.otherReceiptsFacade.setOtherReciptNonVerifiedFilter(
                filterResult.data
              );
              const filterData = filterResult.data;
              if (filterData == null) {
                this.filterData = {};
              } else {
                this.filterData = filterData;
              }
              this.filterNonVerified = [];
              if (filterData) {
                let filterValues = [];
                if (filterData['Product Group']) {
                  filterData['Product Group'].forEach(value => {
                    filterValues.push(value.id);
                  });
                  if (filterValues.length > 0) {
                    this.filterNonVerified.push({
                      key: 'productGroup',
                      value: filterValues
                    });
                  }
                }
                filterValues = [];
                if (filterData['Product Category']) {
                  filterData['Product Category'].forEach(value => {
                    filterValues.push(value.id);
                  });
                  if (filterValues.length > 0) {
                    this.filterNonVerified.push({
                      key: 'productCategory',
                      value: filterValues
                    });
                  }
                }
              }
              this.nonVerifiedItemsPageEvent = this.initailPageEvent;
            } else if (this.tab === OtherReceiptsIssuesEnum.VERIFIED) {
              this.otherReceiptsFacade.setOtherReciptVerifiedFilter(
                filterResult.data
              );
              const filterData = filterResult.data;
              if (filterData == null) {
                this.filterData = {};
              } else {
                this.filterData = filterData;
              }
              this.filterVerified = [];
              if (filterData) {
                let filterValues = [];
                if (filterData['Product Group']) {
                  filterData['Product Group'].forEach(value => {
                    filterValues.push(value.id);
                  });
                  if (filterValues.length > 0) {
                    this.filterVerified.push({
                      key: 'productGroup',
                      value: filterValues
                    });
                  }
                }
                filterValues = [];
                if (filterData['Product Category']) {
                  filterData['Product Category'].forEach(value => {
                    filterValues.push(value.id);
                  });
                  if (filterValues.length > 0) {
                    this.filterVerified.push({
                      key: 'productCategory',
                      value: filterValues
                    });
                  }
                }
              }
              this.verifiedItemsPageEvent = this.initailPageEvent;
            }
            this.initialLoad();
          }
        }
      );
  }
  openSort(): void {
    let sortTabData: Column[] = [];
    if (this.tab === OtherReceiptsIssuesEnum.NON_VERIFIED) {
      sortTabData = this.sortTabDataNonVerifiedProducts;
    } else if (this.tab === OtherReceiptsIssuesEnum.VERIFIED) {
      sortTabData = this.sortTabDataVerifiedProducts;
    }
    this.sortDialogService
      .openDialog(this.maxSortLimit, sortTabData)
      .pipe(takeUntil(this.destroy$))
      .subscribe((sortResult: { data: Column[]; actionfrom: string }) => {
        if (sortResult.actionfrom === FilterActions.APPLY) {
          if (this.tab === OtherReceiptsIssuesEnum.NON_VERIFIED) {
            this.otherReceiptsFacade.setOtherReceiptNonVerifiedProductsSort(
              sortResult.data
            );
            this.sortMapNonverified.clear();
            const sortData = sortResult.data;
            if (sortData == null || sortData.length === 0) {
              this.sortData = [];
              this.sortOrder = null;
              this.sortBy = null;
            } else {
              this.sortData = sortData;
              if (sortData.length > 0) {
                if (sortData[0].id === 0) {
                  this.sortBy = 'issuedWeight';
                } else if (sortData[0].id === 1) {
                  this.sortBy = 'issuedQuantity';
                }
                this.sortOrder = sortData[0].sortAscOrder ? 'ASC' : 'DESC';
              }
            }
            if (this.sortBy !== null && this.sortOrder !== null) {
              this.sortMapNonverified.set(
                'sort',
                this.sortBy + ',' + this.sortOrder
              );
            }
            this.nonVerifiedItemsPageEvent = this.initailPageEvent;
          } else if (this.tab === OtherReceiptsIssuesEnum.VERIFIED) {
            this.otherReceiptsFacade.setOtherReceiptVerifiedProductsSort(
              sortResult.data
            );
            this.sortMapVerified.clear();
            const sortData = sortResult.data;
            if (sortData == null || sortData.length === 0) {
              this.sortData = [];
              this.sortOrder = null;
              this.sortBy = null;
            } else {
              this.sortData = sortData;
              if (sortData.length > 0) {
                if (sortData[0].id === 0) {
                  this.sortBy = 'issuedWeight';
                } else if (sortData[0].id === 1) {
                  this.sortBy = 'issuedQuantity';
                }
                this.sortOrder = sortData[0].sortAscOrder ? 'ASC' : 'DESC';
              }
            }
            if (this.sortBy !== null && this.sortOrder !== null) {
              this.sortMapVerified.set(
                'sort',
                this.sortBy + ',' + this.sortOrder
              );
            }
            this.verifiedItemsPageEvent = this.initailPageEvent;
          }
          this.initialLoad();
        }
      });
  }
  mapToFilterOptions(
    key: string,
    options: FilterOption[]
  ): { [key: string]: Filter[] } {
    return {
      [key]: options.map((option, index) => ({
        id: option.id,
        description: option.description,
        selected: false
      }))
    };
  }
  showFailureNotification(event: OverlayNotificationEventRef) {
    this.hasNotification = false;
    if (event.eventType === OverlayNotificationEventType.CLOSE) {
      this.showNotifications();
    }
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
