import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  TemplateRef
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntil, take, filter } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { OtherReceiptsIssuesEnum } from '../../../../in-stock-management/in-stock/in-stock.enum';
import { PageEvent } from '@angular/material';
import { OtherIssuesFacade } from '../../+issue-state/other-issues.facade';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import {
  OverlayNotificationEventRef,
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventType,
  SortDialogService,
  FilterService,
  SearchComponent,
  SearchResponse,
  Filter,
  FilterActions,
  Column
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import {
  CustomErrors,
  ShortcutService,
  Command,
  AppsettingFacade,
  ProductCategory,
  ProductGroup
} from '@poss-web/core';
import {
  OtherIssuesItem,
  RequestOtherIssueStockTransferNote,
  SORT_DATA,
  FilterOption
} from '../../models/other-issues.model';

const SEARCH_SHORTCUT_KEY_F2 = 'ExhibitionIssueDetailsComponent.F2';
@Component({
  selector: 'poss-web-exhibition-issue-details',
  templateUrl: './exhibition-issue-details.component.html',
  styleUrls: ['./exhibition-issue-details.component.scss']
})
export class ExhibitionIssueDetailsComponent implements OnInit, OnDestroy {
  type: string;
  reqDocNumber: number;
  destroy$: Subject<null> = new Subject<null>();
  storeType: string;
  selectedIssue: RequestOtherIssueStockTransferNote;
  pageSize = 5;
  pageIndex = 0;
  initialPageEvent: PageEvent = {
    pageIndex: this.pageIndex,
    pageSize: this.pageSize,
    length: 0
  };
  nonVerifiedOtherIssueItemsPageEvent: PageEvent = this.initialPageEvent;
  isLoadingSelectedStock$: Observable<boolean>;
  nonVerifiedOtherIssueItems$: Observable<OtherIssuesItem[]>;
  isNonVerifiedOtherIssueItemsLoading$: Observable<boolean>;
  isVerifiedOtherIssueItemsLoading$: Observable<boolean>;
  isOtherIssueItemsTotalCountLoaded$: Observable<boolean>;
  isOtherIssueItemsTotalCountLoading$: Observable<boolean>;
  hasNotification = false;
  nonVerifiedOtherIssueItemsTotalCount = 0;
  verifiedOtherIssueItemsTotalCount = 0;
  otherIssuesTabEnumRef = OtherReceiptsIssuesEnum;

  @ViewChild(SearchComponent, { static: true })
  searchRef: SearchComponent;

  searchForm: FormGroup;
  isSearchingOtherIssueItems$: Observable<boolean>;
  hasSearchedOtherIssueItems$: Observable<boolean>;
  transferType: string;
  CreateOtherStockIssueItemsResponse$: Observable<any>;
  confirmOtherStockIssueResponse$: Observable<any>;
  isLoadingOtherIssueDetails$: Observable<boolean>;
  sortByData = '';
  IsLoadingCancelStockRequestResponse$: any;
  CancelOtherStockRequestResponse$: Observable<any>;
  itemCode: string;
  lotNumber: string;
  result: any;
  printdata: Observable<any>;
  @ViewChild('confirmSuccessNotificationTemplate', { static: true })
  confirmSuccessNotificationTemplate: TemplateRef<any>;
  requestDocNumber: any;
  productCategory: { [key: string]: Filter[] };
  productGroup: { [key: string]: Filter[] };
  maxFilterLimit: any;
  maxSortLimit: any;
  /* productCategory: { [key: string]: Filter[] };
  productGroup: { [key: string]: Filter[] }; */
  filterData: { [key: string]: Filter[] } = {};
  filterMap = new Map();
  filterDataOtherIssue: any;
  sortDataOtherIssue: Observable<Column[]>;
  sortData: Column[];
  sortMap = new Map();
  sortOrder: any;
  sortBy: any;
  filter: { key: string; value: any[] }[] = [];
  stockTransactionNumber: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private otherIssueFacade: OtherIssuesFacade,
    private router: Router,
    private fb: FormBuilder,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService,
    private filterService: FilterService,
    private shortcutService: ShortcutService,
    private appsettingFacade: AppsettingFacade,
    private sortDialogService: SortDialogService
  ) {
    this.sortDialogService.DataSource = SORT_DATA;
    this.searchForm = this.fb.group({
      searchValue: []
    });
  }

  ngOnInit() {
    this.reset();
    this.reqDocNumber = this.activatedRoute.snapshot.params['reqDocNo'];
    this.type = this.activatedRoute.snapshot.params['type'];
    if (this.type === OtherReceiptsIssuesEnum.EXHIBITION) {
      this.transferType = OtherReceiptsIssuesEnum.EXHIBITION_TYPE;
    } else if (this.type === OtherReceiptsIssuesEnum.LOAN) {
      this.transferType = OtherReceiptsIssuesEnum.LOAN;
    } else if (this.type === OtherReceiptsIssuesEnum.ADJUSTMENT) {
      this.transferType = OtherReceiptsIssuesEnum.ADJUSTMENT_TYPE;
    } else if (this.type === OtherReceiptsIssuesEnum.LOSS) {
      this.transferType = OtherReceiptsIssuesEnum.LOSS_TYPE;
    } else if (this.type === OtherReceiptsIssuesEnum.PSV) {
      this.transferType = OtherReceiptsIssuesEnum.PSV;
    } else if (this.type === OtherReceiptsIssuesEnum.FOC) {
      this.transferType = OtherReceiptsIssuesEnum.FOC;
    }
    this.appsettingFacade
      .getMaxFilterLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxFilterLimit = data;
      });

    this.appsettingFacade
      .getMaxSortLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxSortLimit = data;
      });
    this.otherIssueFacade.loadSelectedIssue({
      reqDocNo: this.reqDocNumber,
      type: this.transferType
    });
    this.otherIssueFacade
      .getSelectedIssue()
      .pipe(takeUntil(this.destroy$))
      .subscribe(requestStockTransferNote => {
        if (requestStockTransferNote) {
          this.selectedIssue = requestStockTransferNote;
          if (
            this.selectedIssue &&
            this.selectedIssue.status ===
              OtherReceiptsIssuesEnum.APPROVED_STATUS
          ) {
            this.otherIssueFacade.createOtherStockIssueItems({
              id: this.selectedIssue.id,
              data: [],
              transferType: this.transferType
            });
          }
          this.componentInit();
        }
      });
    this.shortcutService.commands
      .pipe(takeUntil(this.destroy$))
      .subscribe(command => this.shortcutEventHandler(command));
  }
  componentInit() {
    this.otherIssueFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });
    this.isLoadingSelectedStock$ = this.otherIssueFacade.getisLoadingSelectedIssueStock();
    this.nonVerifiedOtherIssueItems$ = this.otherIssueFacade.getNonVerifiedOtherissueItems();
    this.nonVerifiedOtherIssueItems$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.nonVerifiedOtherIssueItemsTotalCount = 0;
        if (data.length !== 0) {
          this.nonVerifiedOtherIssueItemsTotalCount = data[0].totalElements;
        }
        this.showNotifications();
      });
    this.isLoadingOtherIssueDetails$ = this.otherIssueFacade.getisLoadingOtherIssueDetails();
    this.confirmOtherStockIssueResponse$ = this.otherIssueFacade.getConfirmOtherStockIssueResponse();
    this.confirmOtherStockIssueResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data.srcDocNo) {
          this.requestDocNumber = data.srcDocNo;
          this.stockTransactionNumber = data.id;
          this.showSuccessMessageNotification();
        }
      });
    this.isSearchingOtherIssueItems$ = this.otherIssueFacade.getIsSearchingOtherIssueItems();
    this.hasSearchedOtherIssueItems$ = this.otherIssueFacade.getHasSearchedOtherIssueItems();

    if (
      this.selectedIssue &&
      this.selectedIssue.status === OtherReceiptsIssuesEnum.APPROVED_STATUS
    ) {
      this.CreateOtherStockIssueItemsResponse$ = this.otherIssueFacade.getCreateOtherStockIssueItemsResponse();
      this.CreateOtherStockIssueItemsResponse$.pipe(
        takeUntil(this.destroy$)
      ).subscribe(data => {
        if (data === null) {
          this.initialLoad();
        }
      });
    }
    this.IsLoadingCancelStockRequestResponse$ = this.otherIssueFacade.getIsLoadingCancelStockRequestResponse();
    this.CancelOtherStockRequestResponse$ = this.otherIssueFacade.getCancelOtherStockRequestResponse();
    this.CancelOtherStockRequestResponse$.pipe(
      takeUntil(this.destroy$)
    ).subscribe(data => {
      if (data) {
        const key =
          'pw.otherReceiptsIssues.OtherStockIssueCancellationSuccessMessage';
        this.translate
          .get(key)
          .pipe(takeUntil(this.destroy$))
          .subscribe((translatedMessage: string) => {
            this.showCancelSuccessMessageNotification(translatedMessage);
          });
      }
    });

    if (
      this.selectedIssue &&
      this.selectedIssue.status === OtherReceiptsIssuesEnum.APVL_PENDING_STATUS
    ) {
      this.initialLoad();
    }
    this.printdata = this.otherIssueFacade.getPrintDataResponse();
    this.printdata
      .pipe(
        filter(data => !!data),
        takeUntil(this.destroy$)
      )
      .subscribe(data => {
        const blob = new Blob([data], { type: 'text/html' });
        const url = window.URL.createObjectURL(blob);
        window.open(url);
      });

    this.otherIssueFacade.loadProductCategories();
    this.otherIssueFacade.loadProductGroups();
    this.otherIssueFacade
      .getProductCategories()
      .subscribe((productCategories: ProductCategory[]) => {
        if (productCategories !== null) {
          this.productCategory = this.mapToFilterOptions(
            'Product Category',
            productCategories.map(productCategory => ({
              id: productCategory.productCategoryCode,
              description: productCategory.description
            }))
          );
        }
      });

    this.otherIssueFacade
      .getProductGroups()
      .subscribe((productGroups: ProductGroup[]) => {
        if (productGroups !== null) {
          this.productGroup = this.mapToFilterOptions(
            'Product Group',
            productGroups.map(productGroup => ({
              id: productGroup.productGroupCode,
              description: productGroup.description
            }))
          );
        }
      });
    this.filterDataOtherIssue = this.otherIssueFacade.getFilterDataOtherIssue();
    this.filterDataOtherIssue.subscribe(filterValue => {
      this.filterData = filterValue;
    });
    this.sortDataOtherIssue = this.otherIssueFacade.getSortDataOtherIssue();
    this.sortDataOtherIssue.subscribe(sortValue => {
      this.sortData = sortValue;
    });
  }

  initialLoad() {
    this.loadPendingItems();
  }
  back(event) {
    event.stopPropagation();
    this.otherIssueFacade.removeInitialLoadOtherIssue();
    const url =
      '/inventory/instock/other-receipts-issues-list/otherissues/' + this.type;
    this.router.navigateByUrl(url);
  }
  loadPendingItems() {
    if (this.selectedIssue) {
      this.otherIssueFacade.loadNonVerifiedItems({
        id: this.selectedIssue.id,
        pageIndex: this.nonVerifiedOtherIssueItemsPageEvent.pageIndex,
        pageSize: this.nonVerifiedOtherIssueItemsPageEvent.pageSize,
        type: this.transferType,
        status:
          this.selectedIssue.status ===
          OtherReceiptsIssuesEnum.APVL_PENDING_STATUS
            ? OtherReceiptsIssuesEnum.APVL_PENDING_STATUS
            : OtherReceiptsIssuesEnum.SELECTED,
        itemCode: this.itemCode,
        lotNumber: this.lotNumber,
        sort: this.sortMap,
        filter: this.filter
      });
    }
  }
  paginateNonVerifedItems(event: PageEvent) {
    this.nonVerifiedOtherIssueItemsPageEvent = event;
    this.loadPendingItems();
  }

  searchItems(searchData: SearchResponse) {
    this.itemCode = searchData.searchValue;
    this.lotNumber = searchData.lotNumber;
    if (this.nonVerifiedOtherIssueItemsTotalCount !== 0) {
      this.loadPendingItems();
    }
  }

  showConfirmIssueNotification(key: any) {
    this.hasNotification = true;
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            buttonText: 'CONFIRM ISSUE',
            hasRemarks: true,
            message: translatedMsg,
            isRemarksMandatory: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.hasNotification = true;
              this.showProgressNotification();
              if (
                this.transferType === OtherReceiptsIssuesEnum.EXHIBITION_TYPE ||
                this.transferType === OtherReceiptsIssuesEnum.LOAN ||
                this.transferType === OtherReceiptsIssuesEnum.LOSS_TYPE
              ) {
                this.otherIssueFacade.confirmOtherStockIssue({
                  id: this.selectedIssue.id,
                  transferType: this.transferType,
                  carrierDetails: {
                    type: null,
                    data: null
                  },
                  remarks: event.data,
                  destinationLocationCode: ''
                });
              }
              if (
                this.transferType === OtherReceiptsIssuesEnum.ADJUSTMENT_TYPE ||
                this.transferType === OtherReceiptsIssuesEnum.PSV ||
                this.transferType === OtherReceiptsIssuesEnum.FOC
              ) {
                this.otherIssueFacade.confirmOtherStockIssue({
                  id: this.selectedIssue.id,
                  transferType: this.transferType,
                  carrierDetails: {
                    type: '',
                    data: ''
                  },
                  remarks: event.data,
                  destinationLocationCode: ''
                });
              }
            }
          });
      });
  }

  clearSearchItems() {
    this.itemCode = null;
    this.lotNumber = null;
    if (this.searchRef) {
      this.searchRef.reset();
    }
    this.loadPendingItems();
  }

  showNotifications() {
    this.overlayNotification.close();
    if (
      this.selectedIssue.status === OtherReceiptsIssuesEnum.APPROVED_STATUS &&
      this.filter.length === 0 &&
      this.itemCode == null &&
      this.lotNumber == null
    ) {
      this.showConfirmIssueNotification(
        'pw.otherReceiptsIssues.confirmIssueNotificationMessage'
      );
    }
    if (
      this.selectedIssue.status ===
        OtherReceiptsIssuesEnum.APVL_PENDING_STATUS &&
      this.filter.length === 0 &&
      this.itemCode == null &&
      this.lotNumber == null
    ) {
      this.OtherIssueCancelNotifications();
    }
  }
  showSuccessMessageNotification() {
    this.hasNotification = true;
    this.overlayNotification
      .show({
        type: OverlayNotificationType.CUSTOM,
        message: '',
        hasBackdrop: true,
        hasClose: true,
        template: this.confirmSuccessNotificationTemplate
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        this.successCancelCloseOperation(event);
      });
  }
  showCancelSuccessMessageNotification(msg: any) {
    this.hasNotification = true;
    this.overlayNotification
      .show({
        type: OverlayNotificationType.SIMPLE,
        message: msg,
        hasBackdrop: true,
        hasClose: true
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        this.successCancelCloseOperation(event);
      });
  }
  showProgressNotification() {
    const key = 'pw.stockReceiveNotificationMessages.progressMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification.show({
          type: OverlayNotificationType.PROGRESS,
          message: translatedMsg,
          hasBackdrop: true
        });
      });
  }
  openFilter() {
    this.filterService.DataSource = {
      ...this.productCategory,
      ...this.productGroup
    };
    this.filterService
      .openDialog(this.maxFilterLimit, this.filterData)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (filterResult: {
          data: { [key: string]: Filter[] };
          actionfrom: string;
        }) => {
          if (filterResult.actionfrom === FilterActions.APPLY) {
            this.otherIssueFacade.setOtherIssueFilter(filterResult.data);
            const filterData = filterResult.data;
            if (filterData == null) {
              this.filterData = {};
            } else {
              this.filterData = filterData;
            }
            this.filter = [];
            if (filterData) {
              let filterValues = [];
              if (filterData['Product Group']) {
                filterData['Product Group'].forEach(value => {
                  filterValues.push(value.id);
                });
                if (filterValues.length > 0) {
                  this.filter.push({
                    key: 'productGroup',
                    value: filterValues
                  });
                }
              }
              filterValues = [];
              if (filterData['Product Category']) {
                filterData['Product Category'].forEach(value => {
                  filterValues.push(value.id);
                });
                if (filterValues.length > 0) {
                  this.filter.push({
                    key: 'productCategory',
                    value: filterValues
                  });
                }
              }
            }
            this.nonVerifiedOtherIssueItemsPageEvent = this.initialPageEvent;
            this.loadPendingItems();
            //this.clearAll();
          }
        }
      );
  }
  openSort() {
    this.sortDialogService
      .openDialog(this.maxSortLimit, this.sortData)
      .pipe(takeUntil(this.destroy$))
      .subscribe((sortResult: { data: Column[]; actionfrom: string }) => {
        if (sortResult.actionfrom === FilterActions.APPLY) {
          this.otherIssueFacade.setOtherIssueAllProductsSort(sortResult.data);
          this.sortMap.clear();
          const sortData = sortResult.data;
          if (sortData == null || sortData.length === 0) {
            this.sortData = [];
            this.sortOrder = null;
            this.sortBy = null;
          } else {
            this.sortData = sortData;
            if (sortData.length > 0) {
              if (sortData[0].id === 0) {
                this.sortBy = 'requestedWeight';
              } else if (sortData[0].id === 1) {
                this.sortBy = 'approvedQuantity';
              }
              this.sortOrder = sortData[0].sortAscOrder ? 'ASC' : 'DESC';
            }
          }
          if (this.sortBy !== null && this.sortOrder !== null) {
            this.sortMap.set('sort', this.sortBy + ',' + this.sortOrder);
          }
          this.nonVerifiedOtherIssueItemsPageEvent = this.initialPageEvent;
          this.loadPendingItems();
          //this.clearAll();
        }
      });
  }
  getStatusColor(status: string) {
    switch (status) {
      case 'REQUESTED':
      case 'ACPT_REJECTED':
      case 'APRVL_PENDING':
      case 'APRVL_REJECTED':
      case 'CANCELLED':
      case 'EXPIRED':
      case 'REJECTED':
        return false;

      case 'ACCEPTED':
      case 'APPROVED':
        return true;
    }
  }
  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {});
  }
  reset() {
    this.otherIssueFacade.resetConfirmOtherIssueResponse();
  }
  /**
   * Notification overlay for cancel request
   */
  OtherIssueCancelNotifications() {
    this.hasNotification = true;
    const key = 'pw.otherReceiptsIssues.cancelNotificationsMsg';
    const buttonKey = 'pw.otherReceiptsIssues.cancelRequestButtonText';
    this.translate
      .get([key, buttonKey])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            message: translatedMessages[key],
            buttonText: translatedMessages[buttonKey],
            hasClose: false,
            hasRemarks: true,
            isRemarksMandatory: true
          })
          .events.subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.otherIssueFacade.cancelOtherStockRequestResponse({
                id: this.selectedIssue.id,
                requestType: this.transferType
              });
            }
          });
      });
  }
  /**
   * method to handle shortcut commands
   * @param command: shortcut command
   */
  shortcutEventHandler(command: Command) {
    if (command.name === SEARCH_SHORTCUT_KEY_F2) {
      if (this.searchRef) {
        this.searchRef.focus();
      }
    }
  }
  print() {
    this.otherIssueFacade.printOtherIssue({
      id: this.stockTransactionNumber,
      requestType: this.transferType
    });
  }
  mapToFilterOptions(
    key: string,
    options: FilterOption[]
  ): { [key: string]: Filter[] } {
    return {
      [key]: options.map((option, index) => ({
        id: option.id,
        description: option.description,
        selected: false
      }))
    };
  }
  successCancelCloseOperation(event: OverlayNotificationEventRef) {
    if (event.eventType === OverlayNotificationEventType.CLOSE) {
      const url =
        '/inventory/instock/other-receipts-issues-list/otherissues/' +
        this.type;
      this.router.navigateByUrl(url);
    }
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
