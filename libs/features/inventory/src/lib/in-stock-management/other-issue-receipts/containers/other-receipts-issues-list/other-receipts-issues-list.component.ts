import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy
} from '@angular/core';

import { Observable, fromEvent, Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { debounceTime, takeUntil } from 'rxjs/operators';
import {
  CardListComponent,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationEventType,
  OverlayNotificationService
} from '@poss-web/shared';
import { CustomErrors } from '@poss-web/core';
import { TranslateService } from '@ngx-translate/core';
import { TransferType } from '../../../../in-stock-management/in-stock/models/in-stock.model';
import { OtherIssuesFacade } from '../../+issue-state/other-issues.facade';
import { OtherReceiptsFacade } from '../../+receive-state/other-receipts.facade';
import { OtherReceiptsIssuesEnum } from '../../../../in-stock-management/in-stock/in-stock.enum';
import { getOtherReceiptsIssuesRouteUrl } from '../../../../page-route.constants';
import { OtherIssueModel } from '../../models/other-issues.model';
import { OtherReceiptsModel } from '../../models/other-receipt.model';

@Component({
  selector: 'poss-web-other-receipts-issues-list',
  templateUrl: './other-receipts-issues-list.component.html',
  styleUrls: ['./other-receipts-issues-list.component.scss']
})
export class OtherReceiptsIssuesListComponent
  implements OnInit, AfterViewInit, OnDestroy {
  type: OtherReceiptsIssuesEnum;
  transferType: OtherReceiptsIssuesEnum;
  pageSize = 4;
  getReceiptsIssueUrl: string;

  otherIssuesTabEnumRef = OtherReceiptsIssuesEnum;

  hasError$: Observable<string>;
  isLoading$: Observable<boolean>;

  otherReceiptsCount$: Observable<number>;
  otherIssuesCount$: Observable<number>;

  otherReceiptsDropDown$: Observable<TransferType[]>;
  otherIssueDropDown$: Observable<TransferType[]>;
  otherRecieptsCardCount: number;
  otherIssueCardCountNumber: number;
  dropDownValue: TransferType[] = [];

  otherReceiptsList$: Observable<OtherReceiptsModel[]>;
  otherIssuesList$: Observable<OtherIssueModel[]>;
  isOtherReceiptListLoadedOnce = false;
  isOtherIssueListLoadedOnce = false;
  isOtherReceiptLoanListLoadedOnce = false;
  isOtherIssueLoanListLoadedOnce = false;

  destroy$: Subject<null> = new Subject<null>();

  isLoadingOtherReceiptsSTN$: Observable<boolean>;
  isLoadingOtherIssuesSTN$: Observable<boolean>;

  searchStockResults$: Observable<OtherReceiptsModel[]>;
  searchOtherissueStockResults$: Observable<OtherIssueModel[]>;
  isSearchingStocks$: Observable<boolean>;
  hasSearchStockResults$: Observable<boolean>;

  transferTypeDropDown = new FormControl();
  transferTypeForm: FormGroup;
  totalIssueElementCount$: Observable<number>;
  totalReceiptsElementCount$: Observable<number>;
  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;

  @ViewChild('otherReceiptsCardList', { static: false })
  otherReceiptsCardList: CardListComponent;

  @ViewChild('otherIssuesCardList', { static: false })
  otherIssuesCardList: CardListComponent;

  searchForm: FormGroup;

  otherReceiptsSelectedDropDown: string;
  otherIssuesSelectedDropDown: string;
  hasNotification = false;
  reqType: any;
  constructor(
    private otherIssueFacade: OtherIssuesFacade,
    private otherReceiptsFacade: OtherReceiptsFacade,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private overlayNotification: OverlayNotificationService
  ) {
    this.searchForm = this.formBuilder.group({
      searchValue: []
    });
  }

  ngOnInit() {
    this.componentInIt();
  }

  ngAfterViewInit() {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe((event: any) => {
        const searchValue = this.searchForm.get('searchValue').value;

        if (searchValue !== '' && !isNaN(searchValue)) {
          this.searchStocks(searchValue);
        } else if (searchValue === '') {
          this.clearSearch();
        } else if (isNaN(searchValue)) {
          this.searchForm.get('searchValue').setErrors({ nan: 'Only numbers' });
          this.searchForm.get('searchValue').markAsTouched();
        }
      });
  }

  componentInIt() {
    this.otherIssueFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });
    let type = this.activatedRoute.snapshot.params['tabType'];
    this.reqType = this.activatedRoute.snapshot.params['type'];
    this.resetListData();
    // todo change required here in dropdown setting
    if (this.reqType === OtherReceiptsIssuesEnum.EXHIBITION) {
      if (type === OtherReceiptsIssuesEnum.OTHER_ISSUES) {
        this.otherIssueFacade.setSelectedDropDownForIssues(
          OtherReceiptsIssuesEnum.EXHIBITION_TYPE
        );
      } else if (type === OtherReceiptsIssuesEnum.OTHER_RECEIPTS) {
        this.otherReceiptsFacade.setSelectedDropDownForReceipts(
          OtherReceiptsIssuesEnum.EXHIBITION_TYPE
        );
      }
    }
    if (this.reqType === OtherReceiptsIssuesEnum.LOAN) {
      if (type === OtherReceiptsIssuesEnum.OTHER_ISSUES) {
        this.otherIssueFacade.setSelectedDropDownForIssues(
          OtherReceiptsIssuesEnum.LOAN
        );
      } else if (type === OtherReceiptsIssuesEnum.OTHER_RECEIPTS) {
        this.otherReceiptsFacade.setSelectedDropDownForReceipts(
          OtherReceiptsIssuesEnum.LOAN
        );
      }
    }
    if (this.reqType === OtherReceiptsIssuesEnum.ADJUSTMENT) {
      this.otherIssueFacade.setSelectedDropDownForIssues(
        OtherReceiptsIssuesEnum.ADJUSTMENT_TYPE
      );
    }
    if (this.reqType === OtherReceiptsIssuesEnum.PSV) {
      this.otherIssueFacade.setSelectedDropDownForIssues(
        OtherReceiptsIssuesEnum.PSV
      );
    }
    if (this.reqType === OtherReceiptsIssuesEnum.LOSS) {
      if (type === OtherReceiptsIssuesEnum.OTHER_ISSUES) {
        this.otherIssueFacade.setSelectedDropDownForIssues(
          OtherReceiptsIssuesEnum.LOSS_TYPE
        );
      } else if (type === OtherReceiptsIssuesEnum.OTHER_RECEIPTS) {
        this.otherReceiptsFacade.setSelectedDropDownForReceipts(
          OtherReceiptsIssuesEnum.LOSS_TYPE
        );
      }
    }
    if (this.reqType === OtherReceiptsIssuesEnum.FOC) {
      this.otherIssueFacade.setSelectedDropDownForIssues(
        OtherReceiptsIssuesEnum.FOC
      );
    }
    this.totalReceiptsElementCount$ = this.otherReceiptsFacade.getTotalReceiptsElementCount();
    if (type === OtherReceiptsIssuesEnum.OTHER_RECEIPTS) {
      this.hasError$ = this.otherReceiptsFacade.getHasError();
      this.isSearchingStocks$ = this.otherReceiptsFacade.getIsSearchingStocks();
      this.hasSearchStockResults$ = this.otherReceiptsFacade.getHasSearchStockResults();
    }
    if (type === OtherReceiptsIssuesEnum.OTHER_ISSUES) {
      this.hasError$ = this.otherIssueFacade.getHasError();
      this.isSearchingStocks$ = this.otherIssueFacade.getIsSearchingStocks();
      this.hasSearchStockResults$ = this.otherIssueFacade.getHasSearchStockResults();
    }

    this.isLoadingOtherReceiptsSTN$ = this.otherReceiptsFacade.getIsLoadingOtherReceiptSTN();
    this.isLoadingOtherIssuesSTN$ = this.otherIssueFacade.getIsLoadingOtherIssuesSTN();

    this.searchStockResults$ = this.otherReceiptsFacade.getSearchStockResults();
    this.searchOtherissueStockResults$ = this.otherIssueFacade.getOtherIssueSearchStockResults();

    this.otherReceiptsFacade.getSelectedDropDownvalue().subscribe(result => {
      this.otherReceiptsSelectedDropDown = result;
    });
    if (
      this.otherReceiptsSelectedDropDown ===
      OtherReceiptsIssuesEnum.EXHIBITION_TYPE
    ) {
      this.transferType = OtherReceiptsIssuesEnum.EXHIBITION;
    }
    if (this.otherReceiptsSelectedDropDown === OtherReceiptsIssuesEnum.LOAN) {
      this.transferType = OtherReceiptsIssuesEnum.LOAN;
    }
    if (
      this.otherReceiptsSelectedDropDown ===
      OtherReceiptsIssuesEnum.ADJUSTMENT_TYPE
    ) {
      this.transferType = OtherReceiptsIssuesEnum.ADJUSTMENT;
    }
    if (
      this.otherReceiptsSelectedDropDown === OtherReceiptsIssuesEnum.LOSS_TYPE
    ) {
      this.transferType = OtherReceiptsIssuesEnum.LOSS;
    }
    if (this.otherReceiptsSelectedDropDown === OtherReceiptsIssuesEnum.PSV) {
      this.transferType = OtherReceiptsIssuesEnum.PSV;
    }
    if (this.otherReceiptsSelectedDropDown === OtherReceiptsIssuesEnum.FOC) {
      this.transferType = OtherReceiptsIssuesEnum.FOC;
    }
    if (this.transferType === OtherReceiptsIssuesEnum.EXHIBITION) {
      this.transferTypeDropDown.patchValue(this.otherReceiptsSelectedDropDown);
    }
    if (this.transferType === OtherReceiptsIssuesEnum.LOAN) {
      this.transferTypeDropDown.patchValue(OtherReceiptsIssuesEnum.LOAN);
    }
    if (this.transferType === OtherReceiptsIssuesEnum.ADJUSTMENT) {
      this.transferTypeDropDown.patchValue(this.otherReceiptsSelectedDropDown);
    }
    if (this.transferType === OtherReceiptsIssuesEnum.LOSS) {
      this.transferTypeDropDown.patchValue(this.otherReceiptsSelectedDropDown);
    }
    if (this.transferType === OtherReceiptsIssuesEnum.PSV) {
      this.transferTypeDropDown.patchValue(this.otherReceiptsSelectedDropDown);
    }
    if (this.transferType === OtherReceiptsIssuesEnum.FOC) {
      this.transferTypeDropDown.patchValue(this.otherReceiptsSelectedDropDown);
    }
    if (
      !type ||
      !(
        type === OtherReceiptsIssuesEnum.OTHER_RECEIPTS ||
        type === OtherReceiptsIssuesEnum.OTHER_ISSUES
      )
    ) {
      type = OtherReceiptsIssuesEnum.OTHER_RECEIPTS;
      this.router.navigate(['404']);
    }
    this.getStockList();
    this.onTabSelection(type);
  }

  onTabSelection(tabSelection: OtherReceiptsIssuesEnum) {
    this.clearSearch();
    let changeTransferType: OtherReceiptsIssuesEnum;
    if (this.type !== tabSelection) {
      if (tabSelection === OtherReceiptsIssuesEnum.OTHER_RECEIPTS) {
        this.otherReceiptsFacade
          .getSelectedDropDownvalue()
          .subscribe(result => {
            this.otherReceiptsSelectedDropDown = result;
          });
        this.transferTypeDropDown.patchValue(
          this.otherReceiptsSelectedDropDown
        );
        if (
          this.otherReceiptsSelectedDropDown ===
          OtherReceiptsIssuesEnum.EXHIBITION_TYPE
        ) {
          changeTransferType = OtherReceiptsIssuesEnum.EXHIBITION;
          this.transferType = OtherReceiptsIssuesEnum.EXHIBITION;
        } else if (
          this.otherReceiptsSelectedDropDown === OtherReceiptsIssuesEnum.LOAN
        ) {
          changeTransferType = OtherReceiptsIssuesEnum.LOAN;
          this.transferType = OtherReceiptsIssuesEnum.LOAN;
        } else if (
          this.otherReceiptsSelectedDropDown ===
          OtherReceiptsIssuesEnum.ADJUSTMENT_TYPE
        ) {
          changeTransferType = OtherReceiptsIssuesEnum.ADJUSTMENT;
          this.transferType = OtherReceiptsIssuesEnum.ADJUSTMENT;
        } else if (
          this.otherReceiptsSelectedDropDown ===
          OtherReceiptsIssuesEnum.LOSS_TYPE
        ) {
          changeTransferType = OtherReceiptsIssuesEnum.LOSS;
          this.transferType = OtherReceiptsIssuesEnum.LOSS;
        } else if (
          this.otherReceiptsSelectedDropDown === OtherReceiptsIssuesEnum.PSV
        ) {
          changeTransferType = OtherReceiptsIssuesEnum.PSV;
          this.transferType = OtherReceiptsIssuesEnum.PSV;
        }
        this.router.navigate(['../..', tabSelection, changeTransferType], {
          relativeTo: this.activatedRoute
        });
      }
      if (tabSelection === OtherReceiptsIssuesEnum.OTHER_ISSUES) {
        this.otherIssueFacade
          .getSelectedIssueDropDownValue()
          .subscribe(result => {
            this.otherIssuesSelectedDropDown = result;
          });
        this.transferTypeDropDown.patchValue(this.otherIssuesSelectedDropDown);
        if (
          this.otherIssuesSelectedDropDown ===
          OtherReceiptsIssuesEnum.EXHIBITION_TYPE
        ) {
          changeTransferType = OtherReceiptsIssuesEnum.EXHIBITION;
          this.transferType = OtherReceiptsIssuesEnum.EXHIBITION;
        } else if (
          this.otherIssuesSelectedDropDown === OtherReceiptsIssuesEnum.LOAN
        ) {
          changeTransferType = OtherReceiptsIssuesEnum.LOAN;
          this.transferType = OtherReceiptsIssuesEnum.LOAN;
        } else if (
          this.otherIssuesSelectedDropDown ===
          OtherReceiptsIssuesEnum.ADJUSTMENT_TYPE
        ) {
          changeTransferType = OtherReceiptsIssuesEnum.ADJUSTMENT;
          this.transferType = OtherReceiptsIssuesEnum.ADJUSTMENT;
        } else if (
          this.otherIssuesSelectedDropDown === OtherReceiptsIssuesEnum.LOSS_TYPE
        ) {
          changeTransferType = OtherReceiptsIssuesEnum.LOSS;
          this.transferType = OtherReceiptsIssuesEnum.LOSS;
        } else if (
          this.otherIssuesSelectedDropDown === OtherReceiptsIssuesEnum.PSV
        ) {
          changeTransferType = OtherReceiptsIssuesEnum.PSV;
          this.transferType = OtherReceiptsIssuesEnum.PSV;
        } else if (
          this.otherIssuesSelectedDropDown === OtherReceiptsIssuesEnum.FOC
        ) {
          changeTransferType = OtherReceiptsIssuesEnum.FOC;
          this.transferType = OtherReceiptsIssuesEnum.FOC;
        }
        this.router.navigate(['../..', tabSelection, changeTransferType], {
          relativeTo: this.activatedRoute
        });
      }
      this.type = tabSelection;

      if (
        (this.type === OtherReceiptsIssuesEnum.OTHER_RECEIPTS &&
          !this.isOtherReceiptListLoadedOnce) ||
        this.type === OtherReceiptsIssuesEnum.OTHER_ISSUES
      ) {
        this.loadStockList(0);
      }
    }
  }

  getStockList() {
    this.getReceiptsIssueUrl = getOtherReceiptsIssuesRouteUrl();
    this.otherIssueFacade.loadOtherIssuesCount();
    this.otherIssuesCount$ = this.otherIssueFacade.getOtherIssuesSTNCount();
    this.otherIssueDropDown$ = this.otherIssueFacade.getOtherIssuesDropdown();

    this.otherReceiptsFacade.loadOtherReceiptsCount();
    this.otherReceiptsCount$ = this.otherReceiptsFacade.getOtherReceiptsSTNCount();
    this.otherReceiptsDropDown$ = this.otherReceiptsFacade.getOtherReceiptsDropdown();

    if (this.transferType === OtherReceiptsIssuesEnum.EXHIBITION) {
      this.otherReceiptsList$ = this.otherReceiptsFacade.getOtherReceiptList();
      this.otherReceiptsList$.subscribe((stocks: OtherReceiptsModel[]) => {
        if (
          stocks &&
          stocks.length !== 0 &&
          !this.isOtherReceiptListLoadedOnce
        ) {
          this.isOtherReceiptListLoadedOnce = true;
        }
      });
    }
    if (this.transferType === OtherReceiptsIssuesEnum.LOAN) {
      this.otherReceiptsList$ = this.otherReceiptsFacade.getOtherReceiptLoanList();
      this.otherReceiptsList$.subscribe((stocks: OtherReceiptsModel[]) => {
        if (
          stocks &&
          stocks.length !== 0 &&
          !this.isOtherReceiptLoanListLoadedOnce
        ) {
          this.isOtherReceiptLoanListLoadedOnce = true;
        }
      });
    }
    this.otherIssuesList$ = this.otherIssueFacade.getOtherIssueList();
    this.otherIssuesList$.subscribe((stocks: OtherIssueModel[]) => {
      if (stocks && stocks.length !== 0 && !this.isOtherIssueListLoadedOnce) {
        this.isOtherIssueListLoadedOnce = true;
      }
    });
  }

  loadStockList(pageIndex) {
    let type: string;
    if (this.type === OtherReceiptsIssuesEnum.OTHER_RECEIPTS) {
      if (this.transferType === OtherReceiptsIssuesEnum.EXHIBITION) {
        type = 'EXH';
        this.otherReceiptsFacade.loadReceiptList({
          type: type,
          pageIndex: pageIndex,
          pageSize: this.pageSize
        });
      }
      if (this.transferType === OtherReceiptsIssuesEnum.LOAN) {
        type = 'LOAN';
        this.otherReceiptsFacade.loadReceiptLoanList({
          type: type,
          pageIndex: pageIndex,
          pageSize: this.pageSize
        });
      }
    } else if (this.type === OtherReceiptsIssuesEnum.OTHER_ISSUES) {
      if (this.transferType === OtherReceiptsIssuesEnum.EXHIBITION) {
        type = 'EXH';
        this.otherIssueFacade.loadIssueList({
          type: type,
          pageIndex: pageIndex,
          pageSize: this.pageSize
        });
        this.totalIssueElementCount$ = this.otherIssueFacade.getTotalIssueElementCount();
        this.otherIssuesList$ = this.otherIssueFacade.getOtherIssueList();
      }
      if (this.transferType === OtherReceiptsIssuesEnum.LOAN) {
        type = 'LOAN';
        this.otherIssueFacade.loadIssueLoanList({
          type: type,
          pageIndex: pageIndex,
          pageSize: this.pageSize
        });
        this.totalIssueElementCount$ = this.otherIssueFacade.getTotalIssueElementCount();
        this.otherIssuesList$ = this.otherIssueFacade.getOtherIssueLoanList();
      }
      if (this.transferType === OtherReceiptsIssuesEnum.ADJUSTMENT) {
        type = OtherReceiptsIssuesEnum.ADJUSTMENT_TYPE;
        this.otherIssueFacade.loadIssueADJList({
          type: type,
          pageIndex: pageIndex,
          pageSize: this.pageSize
        });
        this.totalIssueElementCount$ = this.otherIssueFacade.getTotalIssueElementCount();
        this.otherIssuesList$ = this.otherIssueFacade.getOtherIssueADJList();
      }
      if (this.transferType === OtherReceiptsIssuesEnum.LOSS) {
        type = OtherReceiptsIssuesEnum.LOSS_TYPE;
        this.otherIssueFacade.loadIssueLossList({
          type: type,
          pageIndex: pageIndex,
          pageSize: this.pageSize
        });
        this.totalIssueElementCount$ = this.otherIssueFacade.getTotalIssueElementCount();
        this.otherIssuesList$ = this.otherIssueFacade.getOtherIssueLossList();
      }
      if (this.transferType === OtherReceiptsIssuesEnum.PSV) {
        type = OtherReceiptsIssuesEnum.PSV;
        this.otherIssueFacade.loadIssuePSVList({
          type: type,
          pageIndex: pageIndex,
          pageSize: this.pageSize
        });
        this.totalIssueElementCount$ = this.otherIssueFacade.getTotalIssueElementCount();
        this.otherIssuesList$ = this.otherIssueFacade.getOtherIssuePSVList();
      }
      if (this.transferType === OtherReceiptsIssuesEnum.FOC) {
        type = OtherReceiptsIssuesEnum.FOC;
        this.otherIssueFacade.loadIssueFOCList({
          type: type,
          pageIndex: pageIndex,
          pageSize: this.pageSize
        });
        this.totalIssueElementCount$ = this.otherIssueFacade.getTotalIssueElementCount();
        this.otherIssuesList$ = this.otherIssueFacade.getOtherIssueFOCList();
      }
    }
  }

  searchStocks(srcDocnumber: number) {
    let type: string;
    if (this.type === OtherReceiptsIssuesEnum.OTHER_RECEIPTS) {
      if (this.transferType === OtherReceiptsIssuesEnum.EXHIBITION) {
        type = 'EXH';
      }
      if (this.transferType === OtherReceiptsIssuesEnum.LOAN) {
        type = 'LOAN';
      }
      if (this.transferType === OtherReceiptsIssuesEnum.ADJUSTMENT) {
        type = 'ADJ';
      }
      this.otherReceiptsFacade.searchPendingReceiptsStocks({
        srcDocnumber: srcDocnumber,
        type: type
      });
      this.hasSearchStockResults$ = this.otherReceiptsFacade.getHasSearchStockResults();
      this.isSearchingStocks$ = this.otherReceiptsFacade.getIsSearchingStocks();
    } else if (this.type === OtherReceiptsIssuesEnum.OTHER_ISSUES) {
      if (this.transferType === OtherReceiptsIssuesEnum.EXHIBITION) {
        type = OtherReceiptsIssuesEnum.EXHIBITION_TYPE;
      }
      if (this.transferType === OtherReceiptsIssuesEnum.LOAN) {
        type = OtherReceiptsIssuesEnum.LOAN;
      }
      if (this.transferType === OtherReceiptsIssuesEnum.ADJUSTMENT) {
        type = OtherReceiptsIssuesEnum.ADJUSTMENT_TYPE;
      }
      if (this.transferType === OtherReceiptsIssuesEnum.LOSS) {
        type = OtherReceiptsIssuesEnum.LOSS_TYPE;
      }
      if (this.transferType === OtherReceiptsIssuesEnum.PSV) {
        type = OtherReceiptsIssuesEnum.PSV;
      }
      if (this.transferType === OtherReceiptsIssuesEnum.FOC) {
        type = OtherReceiptsIssuesEnum.FOC;
      }
      this.otherIssueFacade.searchPendingIssuesStocks({
        srcDocnumber: srcDocnumber,
        type: type
      });
      this.hasSearchStockResults$ = this.otherIssueFacade.getHasSearchStockResults();
      this.isSearchingStocks$ = this.otherReceiptsFacade.getIsSearchingStocks();
    }
  }

  onReceiptCardSelected(stock: any) {
    if (
      this.type === OtherReceiptsIssuesEnum.OTHER_RECEIPTS &&
      (this.transferType === OtherReceiptsIssuesEnum.EXHIBITION ||
        this.transferType === OtherReceiptsIssuesEnum.LOAN)
    ) {
      this.router.navigate([stock.id, 'nonverified'], {
        relativeTo: this.activatedRoute
      });
    }
  }

  onIssueCardSelected(stock: any) {
    if (
      this.type === OtherReceiptsIssuesEnum.OTHER_ISSUES &&
      (this.transferType === OtherReceiptsIssuesEnum.EXHIBITION ||
        this.transferType === OtherReceiptsIssuesEnum.LOAN ||
        this.transferType === OtherReceiptsIssuesEnum.ADJUSTMENT ||
        this.transferType === OtherReceiptsIssuesEnum.LOSS ||
        this.transferType === OtherReceiptsIssuesEnum.PSV ||
        this.transferType === OtherReceiptsIssuesEnum.FOC)
    ) {
      if (stock.status === OtherReceiptsIssuesEnum.APVL_PENDING_STATUS) {
        this.router.navigate([
          'inventory/instock/' +
            this.type +
            '/' +
            this.transferType +
            '/' +
            stock.reqDocNo
        ]);
      }
      if (stock.status === OtherReceiptsIssuesEnum.APPROVED_STATUS) {
        this.router.navigate([
          'inventory/instock/' +
            this.type +
            '/' +
            this.transferType +
            '/' +
            stock.reqDocNo
        ]);
      }
    }
  }

  clearSearch() {
    this.searchForm.reset();
    this.otherReceiptsFacade.searchClear();
    this.otherIssueFacade.searchIssueClear();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  /**
   * To add class for color based on status sent
   * @param status: status of the request
   */
  getStatusColor(status: string) {
    switch (status) {
      case 'REQUESTED':
      case 'ACPT_REJECTED':
      case 'APRVL_PENDING':
      case 'APRVL_REJECTED':
      case 'CANCELLED':
      case 'EXPIRED':
      case 'REJECTED':
        return false;

      case 'ACCEPTED':
      case 'APPROVED':
        return true;
    }
  }

  onFilterSelect(data: any) {
    this.clearSearch();
    let tabValue: OtherReceiptsIssuesEnum;
    if (data === OtherReceiptsIssuesEnum.EXHIBITION_TYPE) {
      tabValue = OtherReceiptsIssuesEnum.EXHIBITION;
    }
    if (data === OtherReceiptsIssuesEnum.LOAN) {
      tabValue = OtherReceiptsIssuesEnum.LOAN;
    }
    if (data === OtherReceiptsIssuesEnum.ADJUSTMENT_TYPE) {
      tabValue = OtherReceiptsIssuesEnum.ADJUSTMENT;
    }
    if (data === OtherReceiptsIssuesEnum.LOSS_TYPE) {
      tabValue = OtherReceiptsIssuesEnum.LOSS;
    }
    if (data === OtherReceiptsIssuesEnum.PSV) {
      tabValue = OtherReceiptsIssuesEnum.PSV;
    }
    if (data === OtherReceiptsIssuesEnum.FOC) {
      tabValue = OtherReceiptsIssuesEnum.FOC;
    }
    this.transferType = tabValue;
    this.router.navigate(['../', tabValue], {
      relativeTo: this.activatedRoute
    });
    if (
      this.transferType === OtherReceiptsIssuesEnum.LOAN ||
      this.transferType === OtherReceiptsIssuesEnum.EXHIBITION ||
      this.transferType === OtherReceiptsIssuesEnum.ADJUSTMENT ||
      this.transferType === OtherReceiptsIssuesEnum.LOSS ||
      this.transferType === OtherReceiptsIssuesEnum.PSV ||
      this.transferType === OtherReceiptsIssuesEnum.FOC
    ) {
      this.loadStockList(0);
    }
    if (this.type === OtherReceiptsIssuesEnum.OTHER_RECEIPTS) {
      this.otherReceiptsFacade.setSelectedDropDownForReceipts(data);
      if (this.transferType === OtherReceiptsIssuesEnum.LOAN) {
        this.otherReceiptsList$ = this.otherReceiptsFacade.getOtherReceiptLoanList();
      }
      if (this.transferType === OtherReceiptsIssuesEnum.EXHIBITION) {
        this.otherReceiptsList$ = this.otherReceiptsFacade.getOtherReceiptList();
      }
      if (this.transferType === OtherReceiptsIssuesEnum.ADJUSTMENT) {
        this.otherReceiptsList$ = this.otherReceiptsFacade.getReceiptADJList();
      }
      if (this.otherReceiptsCardList) {
        this.otherReceiptsCardList.resetFocus();
      }
    }

    if (this.type === OtherReceiptsIssuesEnum.OTHER_ISSUES) {
      this.otherIssueFacade.setSelectedDropDownForIssues(data);
      if (this.transferType === OtherReceiptsIssuesEnum.LOAN) {
        this.otherIssuesList$ = this.otherIssueFacade.getOtherIssueLoanList();
      }
      if (this.transferType === OtherReceiptsIssuesEnum.EXHIBITION) {
        this.otherIssuesList$ = this.otherIssueFacade.getOtherIssueList();
      }
      if (this.transferType === OtherReceiptsIssuesEnum.ADJUSTMENT) {
        this.otherIssuesList$ = this.otherIssueFacade.getOtherIssueADJList();
      }
      if (this.transferType === OtherReceiptsIssuesEnum.LOSS) {
        this.otherIssuesList$ = this.otherIssueFacade.getOtherIssueLossList();
      }
      if (this.transferType === OtherReceiptsIssuesEnum.PSV) {
        this.otherIssuesList$ = this.otherIssueFacade.getOtherIssuePSVList();
      }
      if (this.transferType === OtherReceiptsIssuesEnum.FOC) {
        this.otherIssuesList$ = this.otherIssueFacade.getOtherIssueFOCList();
      }
      if (this.otherIssuesCardList) {
        this.otherIssuesCardList.resetFocus();
      }
    }
  }

  createNewOtherIssue(event: any) {
    if (
      event.value === OtherReceiptsIssuesEnum.EXHIBITION ||
      event.value === OtherReceiptsIssuesEnum.LOAN_TYPE ||
      event.value === OtherReceiptsIssuesEnum.LOSS
    ) {
      this.router.navigateByUrl(
        'inventory/instock/create/' +
          this.type +
          '/' +
          event.value +
          '/allProducts'
      );
    } else if (
      event.value === OtherReceiptsIssuesEnum.ADJUSTMENT ||
      event.value === OtherReceiptsIssuesEnum.PSV ||
      event.value === OtherReceiptsIssuesEnum.FOC
    ) {
      this.router.navigateByUrl(
        'inventory/instock/' + this.type + '/' + event.value
      );
    }
  }
  createNewOtherReceipts(event: any) {
    if (
      event.value === OtherReceiptsIssuesEnum.ADJUSTMENT ||
      event.value === OtherReceiptsIssuesEnum.PSV
    ) {
      this.router.navigateByUrl(
        'inventory/instock/' + this.type + '/' + event.value
      );
    }
  }
  resetListData() {
    this.otherIssueFacade.resetOtherIssueListData();
    this.otherReceiptsFacade.resetOtherReceiptListData();
  }
  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {});
  }
}
