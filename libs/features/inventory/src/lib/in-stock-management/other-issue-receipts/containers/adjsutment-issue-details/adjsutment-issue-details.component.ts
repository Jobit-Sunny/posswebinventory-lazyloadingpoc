import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy
} from '@angular/core';
import { OtherReceiptsIssuesEnum } from '../../../../in-stock-management/in-stock/in-stock.enum';
import { ActivatedRoute, Router } from '@angular/router';
import {
  SearchResponse,
  SearchListComponent,
  OverlayNotificationType,
  OverlayNotificationService,
  OverlayNotificationEventRef,
  OverlayNotificationEventType,
  SortDialogService,
  FilterService,
  Filter,
  FilterActions,
  Column,
  SearchComponent
} from '@poss-web/shared';
import { OtherIssuesFacade } from '../../+issue-state/other-issues.facade';
import { Observable, Subject, fromEvent } from 'rxjs';
import { takeUntil, debounceTime } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from '@angular/forms';
import {
  CustomErrors,
  AppsettingFacade,
  ProductCategory,
  ProductGroup
} from '@poss-web/core';
import {
  OtherIssuesItem,
  AdjustmentItemToUpdate,
  SORT_DATA,
  FilterOption
} from '../../models/other-issues.model';

@Component({
  selector: 'poss-web-adjsutment-issue-details',
  templateUrl: './adjsutment-issue-details.component.html',
  styleUrls: ['./adjsutment-issue-details.component.scss']
})
export class AdjsutmentIssueDetailsComponent implements OnInit, OnDestroy {
  destroy$: Subject<null> = new Subject<null>();
  otherIssuesTabEnumRef = OtherReceiptsIssuesEnum;
  onSearchFlag: boolean;
  cartCount = 0;
  searchCount: any;
  searchedAdjustmentItems$: Observable<OtherIssuesItem[]>;
  selectionAllSubject: Subject<any> = new Subject<any>();
  searchResult: OtherIssuesItem[] = [];
  items: OtherIssuesItem[] = [];
  itemsInCart$: Observable<OtherIssuesItem[]>;
  @ViewChild(SearchListComponent, { static: true })
  searchListRef: SearchListComponent;
  @ViewChild(SearchComponent, { static: true })
  searchRef: SearchComponent;
  totalItemCount: number;
  hasNotification = false;
  itemsToConfirm = [];
  searchForm: FormGroup;
  selectForm: FormGroup;
  itemIds = [];
  allItemIds = [];
  createStockRequestAdjustmentResponse$: Observable<any>;
  searchedAdjustmentItemsInCart$: Observable<OtherIssuesItem[]>;
  searchedAdjustmentItemsInCartResults: OtherIssuesItem[];
  adjustmentForm: FormGroup;
  isSearchingAdjustmentItem$: Observable<boolean>;
  hasSearchedAdjustmentItem$: Observable<boolean>;
  isloadingAdjsutment$: Observable<boolean>;
  isItemsInCart: boolean;
  hasSearchItemInCartSearch$: Observable<boolean>;
  hideSearchResults = false;
  allItemsInCart: OtherIssuesItem[];
  productCategory: { [key: string]: Filter[] };
  productGroup: { [key: string]: Filter[] };
  maxProductInList: any;
  maxSortLimit: any;
  maxFilterLimit: any;
  filterData: { [key: string]: Filter[] };
  filter: { key: string; value: any[] }[] = [];
  sortData: Column[] = [];
  sortOrder: any;
  sortBy: any;
  itemList: OtherIssuesItem[];
  itemCode: string;
  lotNumber: string;
  constructor(
    private otherIssueFacade: OtherIssuesFacade,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService,
    private fb: FormBuilder,
    private router: Router,
    private sortService: SortDialogService,
    private filterService: FilterService,
    private appsettingFacade: AppsettingFacade
  ) {
    this.sortService.DataSource = SORT_DATA;
    this.searchForm = new FormGroup({
      searchValue: new FormControl('')
    });
    this.selectForm = this.fb.group({
      selectRadioButton: ['']
    });
    this.adjustmentForm = this.fb.group({
      approvedBy: ['', Validators.compose([Validators.required])],
      approvalCode: ['', Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
    this.resetData();
    this.componentInit();
  }

  componentInit() {
    this.searchedAdjustmentItemsInCartResults = [];
    this.isSearchingAdjustmentItem$ = this.otherIssueFacade.getIsSearchingAdjustment();
    this.hasSearchedAdjustmentItem$ = this.otherIssueFacade.gethasSearchedItemAdjustment();
    this.isloadingAdjsutment$ = this.otherIssueFacade.getIsLoadingAdjustment();
    this.hasSearchItemInCartSearch$ = this.otherIssueFacade.getHasSearchItemInCartSearch();
    this.otherIssueFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });
    this.searchedAdjustmentItems$ = this.otherIssueFacade.getSearchedAdjustmentItems();
    this.searchedAdjustmentItems$
      .pipe(takeUntil(this.destroy$))
      .subscribe(items => {
        this.isItemsInCart = false;
        this.searchResult = [];
        // To Do Remove this loop
        for (let i = 0; i < items.length; i++) {
          if (items[i].availableQuantity !== 0) {
            this.searchResult.push(items[i]);
          }
        }
        if (this.searchResult.length === 1) {
          this.isItemsInCart = true;
          this.addToCart(this.searchResult);
        }
      });

    this.itemsInCart$ = this.otherIssueFacade.getpAdjustmentItemsInCarts();
    this.itemsInCart$.pipe(takeUntil(this.destroy$)).subscribe(items => {
      if (items) {
        this.allItemsInCart = items;
        this.allItemIds = [];
        this.itemsToConfirm = [];
        this.totalItemCount = items.length;
        items.forEach(element => {
          this.itemsToConfirm.push({
            itemCode: element.itemCode,
            inventoryId: element.id,
            lotNumber: element.lotNumber,
            quantity: element.measuredQuantity,
            measuredWeight: Number(
              element.stdWeight * element.measuredQuantity
            ).toFixed(3),
            itemDetails: {
              data: {},
              type: ''
            }
          });
          this.allItemIds.push(element.id);
        });
        this.loadItemCart();
        this.showNotification();
      }
    });
    this.createStockRequestAdjustmentResponse$ = this.otherIssueFacade.getCreateStockRequestAdjustmentResponse();
    this.createStockRequestAdjustmentResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data.reqDocNo) {
          const key = 'pw.otherReceiptsIssues.OtherIssueConfirmSuccessMessage';
          this.translate
            .get(key)
            .pipe(takeUntil(this.destroy$))
            .subscribe((translatedMessage: string) => {
              this.showSuccessMessageNotification(
                translatedMessage.replace('{0}', data.reqDocNo)
              );
            });
        }
      });
    this.adjustmentForm.statusChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        this.showNotification();
      });

    this.appsettingFacade
      .getMaxFilterLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxFilterLimit = data;
      });

    this.appsettingFacade
      .getMaxSortLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxSortLimit = data;
      });

    this.appsettingFacade
      .getMaxProductInList()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxProductInList = data;
      });

    this.otherIssueFacade.loadProductCategories();
    this.otherIssueFacade.loadProductGroups();
    this.otherIssueFacade
      .getProductCategories()
      .subscribe((productCategories: ProductCategory[]) => {
        if (productCategories !== null) {
          this.productCategory = this.mapToFilterOptions(
            'Product Category',
            productCategories.map(productCategory => ({
              id: productCategory.productCategoryCode,
              description: productCategory.description
            }))
          );
        }
      });

    this.otherIssueFacade
      .getProductGroups()
      .subscribe((productGroups: ProductGroup[]) => {
        if (productGroups !== null) {
          this.productGroup = this.mapToFilterOptions(
            'Product Group',
            productGroups.map(productGroup => ({
              id: productGroup.productGroupCode,
              description: productGroup.description
            }))
          );
        }
      });
  }
  /**
   * Searhes the items by Variant Code
   */
  onSearch(searchResponse: SearchResponse) {
    this.onSearchFlag = true;
    if (this.allItemsInCart.length < this.maxProductInList) {
      this.otherIssueFacade.SearchAdjustmentItems({
        variantCode: searchResponse.searchValue,
        lotNumber: searchResponse.lotNumber
      });
    }
  }
  addToCart(items: OtherIssuesItem[]) {
    this.clearInventorySearch();
    if (
      this.allItemsInCart.length + items.length < this.maxProductInList &&
      this.onSearchFlag === true
    ) {
      this.searchListRef.reset();
      this.otherIssueFacade.addItemsToCart(items);
    }
  }
  showConfirmIssueNotifications(key: string) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            message:
              this.totalItemCount === 50
                ? translatedMessage.replace(
                    '{0}',
                    this.totalItemCount.toString()
                  )
                : translatedMessage,
            buttonText: 'CONFIRM',
            hasRemarks: true,
            isRemarksMandatory: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              if (this.adjustmentForm.invalid) {
                this.hasNotification = true;
                this.showConfirmIssueNotifications(
                  'pw.otherReceiptsIssues.approvalDetailsNotificationMessage'
                );
              } else {
                this.hasNotification = true;
                this.otherIssueFacade.createStockRequestAdjustment({
                  reqType: OtherReceiptsIssuesEnum.ADJUSTMENT_TYPE,
                  approvalDetails: {
                    data: {
                      approvalCode: this.adjustmentForm.value.approvalCode,
                      approvedBy: this.adjustmentForm.value.approvedBy
                    },
                    type: 'approval'
                  },
                  items: this.itemsToConfirm,
                  remarks: event.data
                });
              }
            }
          });
      });
  }
  showNotification() {
    if (
      this.totalItemCount > 0 &&
      !(this.selectForm.value.selectRadioButton === '1') &&
      this.filter.length === 0 &&
      !this.itemCode
    ) {
      if (this.adjustmentForm.invalid) {
        this.showInfoNotifications(
          'pw.otherReceiptsIssues.enterApprovalDetailsMessage'
        );
      } else {
        this.showConfirmIssueNotifications(
          'pw.otherReceiptsIssues.confirmIssueNotificationMessage'
        );
      }
    } else if (
      this.totalItemCount === 50 &&
      !(this.selectForm.value.selectRadioButton === '1')
    ) {
      if (this.adjustmentForm.invalid) {
        this.showInfoNotifications(
          'pw.otherReceiptsIssues.enterApprovalDetailsMessage'
        );
      } else {
        this.showConfirmIssueNotifications(
          'pw.otherReceiptsIssues.confirmIssueNotificationCartFullMessage'
        );
      }
    }
    if (this.selectForm.value.selectRadioButton === '1') {
      this.RemoveProductsOverlay(
        this.allItemIds.length,
        'pw.otherReceiptsIssues.removeProductNotifictionMessage'
      );
    }
    if (this.itemIds.length > 0) {
      this.RemoveProductsOverlay(
        this.itemIds.length,
        'pw.otherReceiptsIssues.removeProductNotifictionMessage'
      );
    }
    if (this.totalItemCount === 0) {
      this.overlayNotification.close();
    }
  }
  updateItem(itemToUpdate: AdjustmentItemToUpdate) {
    this.otherIssueFacade.updateCartItem({
      id: itemToUpdate.id,
      quantity: itemToUpdate.newUpdate.quantity,
      weight: itemToUpdate.newUpdate.weight
    });
  }
  selectChange() {
    if (this.selectForm.value.selectRadioButton === '1') {
      this.selectAll();
    }
  }
  selectAll() {
    this.itemIds = [];
    this.selectionAllSubject.next({
      selectCheckbox: true,
      enableCheckbox: false
    });
    this.showNotification();
  }
  selectionEmit(selection: { selected: boolean; id: number }) {
    this.selectForm.patchValue({
      selectRadioButton: null
    });
    if (selection.selected === false) {
      this.selectForm.patchValue({
        selectRadioButton: null
      });
      const itemToRemove = selection.id;
      this.itemIds.splice(this.itemIds.indexOf(itemToRemove), 1);
    } else if (selection.selected === true) {
      this.itemIds.push(selection.id);
      if (this.itemIds.length === this.allItemIds.length) {
        this.selectForm.patchValue({
          selectRadioButton: '1'
        });
      }
    }
    this.overlayNotification.close();
    this.showNotification();
  }
  RemoveProductsOverlay(count: number, key: any) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            buttonText: 'REMOVE',
            message: translatedMessage.replace('{0}', count.toString())
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.otherIssueFacade.removeSelectedItems({
                ids:
                  this.selectForm.value.selectRadioButton === '1'
                    ? this.allItemIds
                    : this.itemIds
              });
              this.overlayNotification.close();
              this.itemIds = [];
              this.showNotification();
              this.selectForm.patchValue({
                selectRadioButton: null
              });
              this.clearSearchItems();
            }
          });
      });
  }
  showSuccessMessageNotification(msg: any) {
    this.hasNotification = true;
    this.overlayNotification
      .show({
        type: OverlayNotificationType.SIMPLE,
        message: msg,
        hasBackdrop: true,
        hasClose: true
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        if (event.eventType === OverlayNotificationEventType.CLOSE) {
          const url =
            '/inventory/instock/other-receipts-issues-list/otherissues/Exhibition';
          this.router.navigateByUrl(url);
        }
      });
  }
  back() {
    this.router.navigate([
      'inventory/instock/other-receipts-issues-list/otherissues/Exhibition'
    ]);
  }
  resetData() {
    this.otherIssueFacade.resetAdjustmentIssueData();
  }
  clearSearchItems() {
    this.itemCode = null;
    this.lotNumber = null;
    this.loadItemCart();
    this.overlayNotification.close();
    this.showNotification();
    this.clearAll();
  }
  clearAll() {
    this.itemIds = [];
    this.selectForm.patchValue({
      selectRadioButton: null
    });
    this.selectionAllSubject.next({
      selectCheckbox: false,
      enableCheckbox: true
    });
    this.otherIssueFacade.clearSearchCartItemAdjustment();
    this.overlayNotification.close();
    this.showNotification();
  }
  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {});
  }
  clearInventorySearch() {
    this.itemCode = null;
    this.lotNumber = null;
    this.sortBy = null;
    this.sortOrder = null;
    this.sortData = [];
    this.filter = [];
    this.filterData = {};
    if (this.searchListRef) {
      this.searchListRef.reset();
    }
    if (this.searchRef) {
      this.searchRef.reset();
    }
    this.loadItemCart();
    this.otherIssueFacade.clearSearchInventoryItemAdjustment();
  }
  showInfoNotifications(key: any) {
    this.hasNotification = true;
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification.show({
          type: OverlayNotificationType.SIMPLE,
          message: translatedMsg,
          hasClose: false
        });
      });
  }
  openFilter() {
    this.filterService.DataSource = {
      ...this.productCategory,
      ...this.productGroup
    };
    this.filterService
      .openDialog(this.maxFilterLimit, this.filterData)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (filterResult: {
          data: { [key: string]: Filter[] };
          actionfrom: string;
        }) => {
          if (filterResult.actionfrom === FilterActions.APPLY) {
            const filterData = filterResult.data;
            if (filterData == null) {
              this.filterData = {};
            } else {
              this.filterData = filterData;
            }
            this.filter = [];
            if (filterData) {
              let filterValues = [];
              if (filterData['Product Group']) {
                filterData['Product Group'].forEach(value => {
                  filterValues.push(value.description.toLowerCase());
                });
                if (filterValues.length > 0) {
                  this.filter.push({
                    key: 'productGroup',
                    value: filterValues
                  });
                }
              }
              filterValues = [];
              if (filterData['Product Category']) {
                filterData['Product Category'].forEach(value => {
                  filterValues.push(value.description.toLowerCase());
                });
                if (filterValues.length > 0) {
                  this.filter.push({
                    key: 'productCategory',
                    value: filterValues
                  });
                }
              }
            }

            // this.resetSelection();
            this.loadItemCart();
          }
        }
      );
  }
  openSort() {
    this.sortService
      .openDialog(this.maxSortLimit, this.sortData)
      .pipe(takeUntil(this.destroy$))
      .subscribe((sortResult: { data: Column[]; actionfrom: string }) => {
        if (sortResult.actionfrom === FilterActions.APPLY) {
          const sortData = sortResult.data;
          if (sortData == null || sortData.length === 0) {
            this.sortData = [];
            this.sortOrder = null;
            this.sortBy = null;
          } else {
            this.sortData = sortData;
            if (sortData.length > 0) {
              if (sortData[0].id === 0) {
                this.sortBy = 'availableWeight';
              } else if (sortData[0].id === 1) {
                this.sortBy = 'availableQuantity';
              }
              this.sortOrder = sortData[0].sortAscOrder ? 'ASC' : 'DESC';
            }
          }
          this.loadItemCart();
        }
      });
  }
  loadItemCart() {
    this.itemList = this.allItemsInCart;
    if (this.itemCode) {
      this.itemList = this.itemList.filter(
        (item: OtherIssuesItem) =>
          item.itemCode.toLowerCase() === this.itemCode.toLowerCase() &&
          (this.lotNumber
            ? this.lotNumber.toLowerCase() === item.lotNumber.toLowerCase()
            : true)
      );
    }
    if (this.filter.length > 0) {
      for (let i = 0; i < this.filter.length; i++) {
        this.itemList = this.itemList.filter((item: OtherIssuesItem) => {
          return this.filter[i].value.includes(
            typeof item[this.filter[i].key] === 'string'
              ? item[this.filter[i].key].toLowerCase()
              : item[this.filter[i].key]
          );
        });
      }
    }
    if (this.sortBy) {
      const issueItemsSort = [...this.itemList].sort(
        (item1: OtherIssuesItem, item2: OtherIssuesItem) => {
          if (item1[this.sortBy] === item2[this.sortBy]) {
            return 0;
          }
          return item1[this.sortBy] < item2[this.sortBy]
            ? this.sortOrder === 'ASC'
              ? -1
              : 1
            : this.sortOrder === 'ASC'
            ? 1
            : -1;
        }
      );
      this.itemList = issueItemsSort;
    }

    /* this.itemListId = this.itemList.map(item => item.id);

    if (this.itemListId.length === 1 && this.selectedItemsId.length === 0) {
      this.binToBinTransferFacade.changeSelectionOfAllItems({
        idList: this.itemListId,
        select: true,
        disable: false
      });
    } */
    this.clearAll();
    this.overlayNotification.close();
    this.showNotification();
  }
  searchInCart(searchData: SearchResponse) {
    this.itemCode = searchData.searchValue;
    this.lotNumber = searchData.lotNumber;
    this.loadItemCart();
  }
  removeItem($event) {}
  mapToFilterOptions(
    key: string,
    options: FilterOption[]
  ): { [key: string]: Filter[] } {
    return {
      [key]: options.map((option, index) => ({
        id: option.id,
        description: option.description,
        selected: false
      }))
    };
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.overlayNotification.close();
  }
}
