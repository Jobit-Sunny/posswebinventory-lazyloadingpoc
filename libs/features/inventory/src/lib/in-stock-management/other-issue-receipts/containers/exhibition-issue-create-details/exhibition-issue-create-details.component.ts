import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  OnDestroy
} from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { OtherIssuesFacade } from '../../+issue-state/other-issues.facade';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationEventType,
  SearchComponent,
  SearchResponse,
  Filter,
  FilterService,
  FilterActions,
  SortDialogService,
  Column
} from '@poss-web/shared';
import { OtherReceiptsIssuesEnum } from '../../../../in-stock-management/in-stock/in-stock.enum';
import { takeUntil, debounceTime, filter } from 'rxjs/operators';
import { Subject, Observable, fromEvent } from 'rxjs';
import { PageEvent } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { routerReducer } from '@ngrx/router-store';
import {
  CustomErrors,
  AppsettingFacade,
  ShortcutService,
  Command,
  ProductCategory,
  ProductGroup
} from '@poss-web/core';
import {
  OtherIssuesItem,
  SORT_DATA,
  FilterOption
} from '../../models/other-issues.model';
import { select } from '@ngrx/store';

const SEARCH_SHORTCUT_KEY_F2 = 'ExhibitionIssueCreateDetailsComponent.F2';
@Component({
  selector: 'poss-web-exhibition-issue-create-details',
  templateUrl: './exhibition-issue-create-details.component.html',
  styleUrls: ['./exhibition-issue-create-details.component.scss']
})
export class ExhibitionIssueCreateDetailsComponent
  implements OnInit, OnDestroy {
  transferType: string;
  reqType: string;
  tab: string;
  destroy$: Subject<null> = new Subject<null>();
  requestId: number;
  pageSize = 0;
  pageIndex = 0;
  initialPageEvent: PageEvent = {
    pageIndex: this.pageIndex,
    pageSize: this.pageSize,
    length: 0
  };
  AllOtherIssueItemsPageEvent: PageEvent = this.initialPageEvent;
  selectedOtherIssueItemsPageEvent: PageEvent = this.initialPageEvent;
  AllOtherIssueCreateItems$: Observable<OtherIssuesItem[]>;
  SelectedOtherIssueCreateItems$: Observable<OtherIssuesItem[]>;
  otherIssuesTabEnumRef = OtherReceiptsIssuesEnum;
  itemForm: FormGroup;
  searchForm: FormGroup;
  allOtherIssuesCreateCount = 0;
  selectedOtherIssuesCreateCount = 0;
  @ViewChild(SearchComponent, { static: true })
  searchRef: SearchComponent;

  isAllOtherIssueCreateItemsLoading$: Observable<boolean>;
  isSelectedOtherIssueCreateItemsLoading$: Observable<boolean>;
  isOtherIssueCreateItemsTotalCountLoaded$: Observable<boolean>;
  isOtherIssueCreateItemsTotalCountLoading$: Observable<boolean>;

  searchedOtherIssueCreateItems$: Observable<OtherIssuesItem[]>;
  isSearchingOtherIssueCreateItems$: Observable<boolean>;
  hasSearchedOtherIssueCreateItems$: Observable<boolean>;

  hasNotification = false;
  selectionAllSubject: Subject<any> = new Subject<any>();
  allOtherIssueIds: OtherIssuesItem[] = [];
  allOtherIssueIdslength: number;
  allOtherIssuesPageCount: number;

  selectedtabOtherIssueIds = [];
  selectedtabOtherIssueIdslength: number;
  selectedtabOtherIssuesPageCount: number;

  selectedAll = false;
  selectedPagewise = false;
  productToBeSavedCount = 0;

  selectedItemids: OtherIssuesItem[] = [];
  itemIdsLength: number;
  addressForm: FormGroup;
  employeeForm: FormGroup;
  lossForm: FormGroup;
  createOtherIssueStockRequestItemsResponse$: Observable<any>;
  RemoveOtherIssueStockRequestItemsResponse$: Observable<any>;
  updateStockRequestResponse$: Observable<any>;
  searchItemsCount: number;
  itemCode: string;
  lotNumber: string;
  productCategory: { [key: string]: Filter[] };
  productGroup: { [key: string]: Filter[] };
  maxFilterLimit: any;
  maxSortLimit: any;
  sortMapAllProducts = new Map();
  sortMapSelectedProducts = new Map();
  filterMapAllProducts = new Map();
  filterMapSelectedProducts = new Map();
  filterData: { [key: string]: Filter[] } = {};
  sortBy: string;
  sortOrder: string;
  sortData: Column[] = [];
  filterDataAllProducts: Observable<{ [key: string]: Filter[] }>;
  filterDataSelectedProducts: Observable<{ [key: string]: Filter[] }>;
  filterTabDataAllProducts: { [key: string]: Filter[] };
  filterTabDataSelectedProducts: { [key: string]: Filter[] };
  sortDataAllProducts: Observable<Column[]>;
  sortDataSelectedProducts: Observable<Column[]>;
  sortTabDataAllProducts: Column[];
  sortTabDataSelectedProducts: Column[];
  filterAllProducts: { key: string; value: any[] }[] = [];
  filterSelectedProducts: { key: string; value: any[] }[] = [];
  selectedTabTotalCountforPagination: number;
  allProdctsTabTotalCountforPagination: number;
  constructor(
    private activatedRoute: ActivatedRoute,
    private otherIssueFacade: OtherIssuesFacade,
    private router: Router,
    private fb: FormBuilder,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService,
    private appsettingFacade: AppsettingFacade,
    private shortcutService: ShortcutService,
    private filterService: FilterService,
    private sortDialogService: SortDialogService
  ) {
    this.sortDialogService.DataSource = SORT_DATA;
    this.searchForm = this.fb.group({
      searchValue: []
    });
    this.itemForm = this.createForm();
    this.addressForm = this.fb.group({
      address1: ['', Validators.required],
      address2: ['', Validators.required],
      approvalCode: ['', Validators.compose([Validators.required])],
      state: [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z]*$')
        ])
      ],
      city: [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z]*$')
        ])
      ],
      pinCode: [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[0-9]*$')
        ])
      ],
      approvedBy: ['', Validators.compose([Validators.required])]
    });
    this.employeeForm = this.fb.group({
      employeeId: ['', Validators.required],
      employeeName: ['', Validators.required],
      designation: ['', Validators.required],
      emailId: [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,3}$')
        ])
      ],
      mobileNo: [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[0-9]{10}$')
        ])
      ],
      brand: ['', Validators.compose([Validators.required])],
      approvedBy: ['', Validators.compose([Validators.required])],
      approvalCode: ['', Validators.compose([Validators.required])]
    });
    this.lossForm = this.fb.group({
      approvedBy: ['', Validators.compose([Validators.required])],
      approvalCode: ['', Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
    this.resetOtherIssueCreateList();
    this.overlayNotification.close();

    // TODO : change logic. Route state
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        const tab = this.activatedRoute.snapshot.params['tab'];
        this.changeTab(tab);
        this.showNotifications(0);
      });
    this.transferType = this.activatedRoute.snapshot.params['transferType'];

    this.tab = this.activatedRoute.snapshot.params['tab'];
    if (this.transferType === OtherReceiptsIssuesEnum.EXHIBITION) {
      this.reqType = OtherReceiptsIssuesEnum.EXHIBITION_TYPE;
    } else if (this.transferType === OtherReceiptsIssuesEnum.LOAN_TYPE) {
      this.reqType = OtherReceiptsIssuesEnum.LOAN;
    } else if (this.transferType === OtherReceiptsIssuesEnum.LOSS) {
      this.reqType = OtherReceiptsIssuesEnum.LOSS_TYPE;
    }
    this.otherIssueFacade.createOtherIssuesStockRequest({
      reqtype: this.reqType
    });
    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(resp => (this.initialPageEvent.pageSize = JSON.parse(resp)));

    this.appsettingFacade
      .getMaxFilterLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxFilterLimit = data;
      });

    this.appsettingFacade
      .getMaxSortLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxSortLimit = data;
      });

    this.createOtherIssueStockRequestItemsResponse$ = this.otherIssueFacade.getCreateStockRequestItemsResponse();
    this.createOtherIssueStockRequestItemsResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data === null) {
          this.initialLoad();
        }
      });
    this.otherIssueFacade
      .getOtherIssuesStockRequest()
      .pipe(takeUntil(this.destroy$))
      .subscribe((responseData: any) => {
        if (responseData) {
          this.requestId = responseData.id;
          this.componentInit();
        }
      });

    this.shortcutService.commands
      .pipe(takeUntil(this.destroy$))
      .subscribe(command => this.shortcutEventHandler(command));
  }

  componentInit() {
    this.otherIssueFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });
    this.AllOtherIssueCreateItems$ = this.otherIssueFacade.getAllOtherIssuesCreateItems();
    this.AllOtherIssueCreateItems$.subscribe(data => {
      this.allOtherIssueIds = data.concat();
      this.itemForm.controls.selectRadioButton.enable();
      this.allOtherIssueIdslength = this.allOtherIssueIds.length;
      this.itemIdsLength = this.allOtherIssueIdslength;
      if (data.length !== 0) {
        this.allProdctsTabTotalCountforPagination = data[0].totalElements;
        this.allOtherIssuesPageCount = data.length;
        if (
          (this.itemCode && this.itemCode !== '') ||
          (this.lotNumber && this.lotNumber !== '')
        ) {
          this.itemForm.patchValue({
            selectRadioButton: '2'
          });
          this.selectChange();
        }
      }

      /* if (this.itemForm.value.selectRadioButton === '1') {
        this.selectionAllSubject.next({
          selectCheckbox: true,
          enableCheckbox: false
        });
      } else if (this.itemForm.value.selectRadioButton === '2') {
        this.selectionAllSubject.next({
          selectCheckbox: false,
          enableCheckbox: true
        });
        this.itemForm.patchValue({
          selectRadioButton: null
        });
      } */
    });
    this.SelectedOtherIssueCreateItems$ = this.otherIssueFacade.getSelecetedIssuesCreateItems();
    this.SelectedOtherIssueCreateItems$.subscribe(data => {
      this.selectedtabOtherIssueIds = data.concat();
      this.itemForm.controls.selectRadioButton.enable();
      this.selectedtabOtherIssueIdslength = this.selectedtabOtherIssueIds.length;
      this.itemIdsLength = this.selectedtabOtherIssueIdslength;
      if (data.length !== 0) {
        this.selectedtabOtherIssuesPageCount = data.length;
        this.selectedTabTotalCountforPagination = data[0].totalElements;
        if (this.tab === OtherReceiptsIssuesEnum.SELECTED_PRODUCTS) {
          this.showNotifications(data[0].totalElements);
          if (
            (this.itemCode && this.itemCode !== '') ||
            (this.lotNumber && this.lotNumber !== '')
          ) {
            this.itemForm.patchValue({
              selectRadioButton: '2'
            });
            this.selectChange();
          }
        }
      }

      /* if (this.itemForm.value.selectRadioButton === '1') {
        this.selectionAllSubject.next({
          selectCheckbox: false,
          enableCheckbox: true
        });
      } else if (this.itemForm.value.selectRadioButton === '2') {
        this.selectionAllSubject.next({
          selectCheckbox: false,
          enableCheckbox: true
        });
        this.itemForm.patchValue({
          selectRadioButton: null
        });
      } */
    });

    this.updateStockRequestResponse$ = this.otherIssueFacade.getupdateStockRequestResponse();
    this.updateStockRequestResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data.reqDocNo) {
          const key = 'pw.otherReceiptsIssues.OtherIssueConfirmSuccessMessage';
          this.translate
            .get(key)
            .pipe(takeUntil(this.destroy$))
            .subscribe((translatedMessage: string) => {
              this.selectInfoCloseOverlay(
                translatedMessage.replace('{0}', data.reqDocNo)
              );
            });
        }
      });
    this.isAllOtherIssueCreateItemsLoading$ = this.otherIssueFacade.getIsAllOtherIssueCreateItemsLoading();
    this.isSelectedOtherIssueCreateItemsLoading$ = this.otherIssueFacade.getselectIsSelectedOtherIssueItemsLoading();
    this.isOtherIssueCreateItemsTotalCountLoaded$ = this.otherIssueFacade.getselectIsOtherIssueCreateTotalCountLoaded();
    this.isOtherIssueCreateItemsTotalCountLoading$ = this.otherIssueFacade.getselectIsOtherIssueCreateTotalCountLoading();
    this.isSearchingOtherIssueCreateItems$ = this.otherIssueFacade.getisSearchingOtherIssueCreateItems();
    this.hasSearchedOtherIssueCreateItems$ = this.otherIssueFacade.gethasSearchedOtherIssueCreateItems();
    this.initialLoad();

    this.otherIssueFacade
      .getallOtherIssueCreateItemsTotalCount()
      .pipe(takeUntil(this.destroy$))
      .subscribe((count: number) => {
        this.allOtherIssuesCreateCount = count;
      });

    this.otherIssueFacade
      .getSelectedOtherIssueCreateTotalCount()
      .pipe(takeUntil(this.destroy$))
      .subscribe((count: number) => {
        this.selectedOtherIssuesCreateCount = count;
      });

    this.RemoveOtherIssueStockRequestItemsResponse$ = this.otherIssueFacade.getRemoveOtherIssueStockRequestItemsResponse();
    this.RemoveOtherIssueStockRequestItemsResponse$.pipe(
      takeUntil(this.destroy$)
    ).subscribe(data => {
      if (data === null) {
        this.selectInfoOverlay('Removed successfully');
        this.initialLoad();
      }
    });

    this.otherIssueFacade.loadProductCategories();
    this.otherIssueFacade.loadProductGroups();
    this.otherIssueFacade
      .getProductCategories()
      .subscribe((productCategories: ProductCategory[]) => {
        if (productCategories !== null) {
          this.productCategory = this.mapToFilterOptions(
            'Product Category',
            productCategories.map(productCategory => ({
              id: productCategory.productCategoryCode,
              description: productCategory.description
            }))
          );
        }
      });

    this.otherIssueFacade
      .getProductGroups()
      .subscribe((productGroups: ProductGroup[]) => {
        if (productGroups !== null) {
          this.productGroup = this.mapToFilterOptions(
            'Product Group',
            productGroups.map(productGroup => ({
              id: productGroup.productGroupCode,
              description: productGroup.description
            }))
          );
        }
      });
    this.filterDataAllProducts = this.otherIssueFacade.getfilterDataAllProducts();
    this.filterDataAllProducts.subscribe(filterValue => {
      this.filterTabDataAllProducts = filterValue;
    });
    this.filterDataSelectedProducts = this.otherIssueFacade.getfilterDataSelectedProducts();
    this.filterDataSelectedProducts.subscribe(filterValue => {
      this.filterTabDataSelectedProducts = filterValue;
    });

    this.sortDataAllProducts = this.otherIssueFacade.getSortDataAllProducts();
    this.sortDataAllProducts.subscribe(sortValue => {
      this.sortTabDataAllProducts = sortValue;
    });
    this.sortDataSelectedProducts = this.otherIssueFacade.getSortDataSelectedProducts();
    this.sortDataSelectedProducts.subscribe(sortValue => {
      this.sortTabDataSelectedProducts = sortValue;
    });
  }

  initialLoad() {
    this.resetValue();
    this.getTotalCount();
    this.loadAllProducts();
    this.loadSelectedProducts();
  }
  conditionalLoad() {
    if (this.tab === OtherReceiptsIssuesEnum.ALL) {
      this.loadAllProducts();
    } else {
      this.loadSelectedProducts();
    }
  }
  getTotalCount() {
    this.otherIssueFacade.loadOtherIssueItemsCreateTotalCount({
      reqtype: this.reqType,
      id: this.requestId
    });
  }
  loadAllProducts() {
    if (this.requestId) {
      this.otherIssueFacade.loadAllOtherIssueCreateItems({
        id: this.requestId,
        pageIndex: this.AllOtherIssueItemsPageEvent.pageIndex,
        pageSize: this.AllOtherIssueItemsPageEvent.pageSize,
        reqtype: this.reqType,
        itemCode: this.itemCode,
        lotNumber: this.lotNumber,
        sort: this.sortMapAllProducts,
        filter: this.filterAllProducts
      });
      if (this.itemForm.value.selectRadioButton === '1') {
        this.showNotifications(this.allOtherIssuesCreateCount);
      }
    }
  }
  loadSelectedProducts() {
    if (this.requestId) {
      this.otherIssueFacade.loadSelectedOtherIssueCreateItems({
        id: this.requestId,
        pageIndex: this.selectedOtherIssueItemsPageEvent.pageIndex,
        pageSize: this.selectedOtherIssueItemsPageEvent.pageSize,
        reqtype: this.reqType,
        itemCode: this.itemCode,
        lotNumber: this.lotNumber,
        sort: this.sortMapSelectedProducts,
        filter: this.filterSelectedProducts
      });
    }
  }
  paginateAllOtherIssueCreateItems(event: PageEvent) {
    this.overlayNotification.close();
    this.selectedItemids = [];
    this.AllOtherIssueItemsPageEvent = event;
    this.loadAllProducts();
  }

  paginateSelectedOtherIssueCreateItems(event: PageEvent) {
    this.overlayNotification.close();
    this.selectedOtherIssueItemsPageEvent = event;
    this.loadSelectedProducts();
  }
  changeTab(newTab: OtherReceiptsIssuesEnum) {
    this.itemForm.patchValue({ selectRadioButton: null });
    if (this.tab !== newTab) {
      this.tab = newTab;
      /* this.sortMap.clear();
      this.filterMap.clear();
      this.filterData = {};
      this.sortData=[]; */
      this.clearSearchItems();
      this.router.navigate(['..', this.tab], {
        relativeTo: this.activatedRoute
      });
      this.resetValue();
    }
  }
  selectChange() {
    if (this.itemForm.value.selectRadioButton === '1') {
      this.selectAll();
    } else if (this.itemForm.value.selectRadioButton === '2') {
      this.selectPagewise();
    }
  }
  createForm(): FormGroup {
    return new FormGroup({
      selectRadioButton: new FormControl(null)
    });
  }

  searchItems(searchData: SearchResponse) {
    this.itemCode = searchData.searchValue;
    this.lotNumber = searchData.lotNumber;
    if (
      (this.tab === OtherReceiptsIssuesEnum.ALL &&
        this.allOtherIssuesCreateCount !== 0) ||
      (this.tab === OtherReceiptsIssuesEnum.SELECTED_PRODUCTS &&
        this.selectedOtherIssuesCreateCount !== 0)
    ) {
      this.overlayNotification.close();
      this.selectedItemids = [];
      this.itemForm.patchValue({
        selectRadioButton: null
      });
      this.selectionAllSubject.next({
        selectCheckbox: false,
        enableCheckbox: true
      });
      /* this.otherIssueFacade.searchOtherIssueCreateItems({
        id: this.requestId,
        status:
          this.tab === OtherReceiptsIssuesEnum.ALL
            ? OtherReceiptsIssuesEnum.OPEN
            : OtherReceiptsIssuesEnum.SELECTED,
        itemCode: this.itemCode,
        lotNumber: this.lotNumber,
        reqtype: this.reqType
      }); */
      this.conditionalLoad();
    }
  }
  clearSearchItems() {
    this.itemCode = null;
    this.lotNumber = null;
    if (this.searchRef) {
      this.searchRef.reset();
    }
    this.selectedItemids = [];
    //this.otherIssueFacade.clearSearchOtherIssueCreateItems();
    this.itemForm.controls.selectRadioButton.disable();
    this.itemForm.patchValue({ selectRadioButton: null });
    this.overlayNotification.close();
    this.conditionalLoad();
  }
  clearAll() {
    this.itemForm.patchValue({
      selectRadioButton: null
    });
    this.selectionAllSubject.next({
      selectCheckbox: false,
      enableCheckbox: true
    });
    //this.resetValue();
    //this.initialLoad();
    this.selectedItemids = [];
    this.overlayNotification.close();
    this.showNotifications(this.selectedItemids.length);
  }

  selectItemsOverlay(count: number, key: any) {
    this.hasNotification = true;
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            buttonText: 'SELECT PRODUCTS',
            message: count + ' ' + translatedMsg
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.hasNotification = true;
              if (this.itemForm.value.selectRadioButton === '1') {
                this.otherIssueFacade.createOtherIssueStockRequestItems({
                  id: this.requestId,
                  data: [],
                  requestType: this.reqType
                });
                this.clearSearchItems();
                // this.resetOtherIssueCreateList();
                this.resetValue();
                this.itemForm.patchValue({ selectRadioButton: null });
              } else if (this.itemForm.value.selectRadioButton === '2') {
                const itemArray = [];
                this.allOtherIssueIds.forEach(itemList => {
                  itemArray.push({
                    inventoryId: itemList.inventoryId,
                    measuredWeight: itemList.availableWeight,
                    quantity: itemList.availableQuantity
                  });
                });
                this.clearSearchItems();
                // this.resetOtherIssueCreateList();
                this.resetValue();
                this.otherIssueFacade.createOtherIssueStockRequestItems({
                  id: this.requestId,
                  data: itemArray,
                  requestType: this.reqType
                });
              } else if (
                this.itemForm.value.selectRadioButton === null &&
                this.selectedItemids.length !== 0
              ) {
                const itemArray = [];
                this.selectedItemids.forEach(selectedItemList => {
                  itemArray.push({
                    inventoryId: selectedItemList.inventoryId,
                    measuredWeight: selectedItemList.availableWeight,
                    quantity: selectedItemList.availableQuantity
                  });
                });
                this.clearSearchItems();
                // this.resetOtherIssueCreateList();
                this.resetValue();
                this.otherIssueFacade.createOtherIssueStockRequestItems({
                  id: this.requestId,
                  data: itemArray,
                  requestType: this.reqType
                });
              }
            }
          });
      });
  }
  confirmOverlay(key: any) {
    this.hasNotification = true;
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            buttonText: 'CONFIRM ISSUE',
            hasRemarks: true,
            isRemarksMandatory: true,
            message: translatedMsg
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              if (this.transferType === OtherReceiptsIssuesEnum.EXHIBITION) {
                if (this.addressForm.invalid) {
                  this.hasNotification = true;
                  this.confirmOverlay(
                    'pw.otherReceiptsIssues.enterAddressDetailsNotificationMessage'
                  );
                } else {
                  this.hasNotification = true;
                  this.showProgressNotification();
                  this.otherIssueFacade.updateStockRequest({
                    id: this.requestId,
                    reqType: this.reqType,
                    approvalDetails: {
                      data: {
                        approvalCode: this.addressForm.value.approvalCode,
                        approvedBy: this.addressForm.value.approvedBy
                      },
                      type: 'approval'
                    },
                    carrierDetails: {
                      type: 'address',
                      data: {
                        address1: this.addressForm.value.address1,
                        address2: this.addressForm.value.address2,
                        city: this.addressForm.value.city,
                        town: this.addressForm.value.state,
                        pincode: this.addressForm.value.pinCode
                      }
                    },
                    remarks: event.data,
                    status: OtherReceiptsIssuesEnum.APVL_PENDING_STATUS
                  });
                }
              } else if (
                this.transferType === OtherReceiptsIssuesEnum.LOAN_TYPE
              ) {
                if (this.employeeForm.invalid) {
                  this.hasNotification = true;
                  this.confirmOverlay(
                    'pw.otherReceiptsIssues.enterEmployeeDetailsNotificationMessage'
                  );
                } else {
                  this.hasNotification = true;
                  this.showProgressNotification();
                  this.otherIssueFacade.updateStockRequest({
                    id: this.requestId,
                    reqType: this.reqType,
                    approvalDetails: {
                      data: {
                        approvalCode: this.employeeForm.value.approvalCode,
                        approvedBy: this.employeeForm.value.approvedBy
                      },
                      type: 'approval'
                    },
                    carrierDetails: {
                      type: 'employee',
                      data: {
                        employeeId: this.employeeForm.value.employeeId,
                        employeeName: this.employeeForm.value.employeeName,
                        designation: this.employeeForm.value.designation,
                        emailId: this.employeeForm.value.emailId,
                        mobileNo: this.employeeForm.value.mobileNo,
                        brand: this.employeeForm.value.brand
                      }
                    },
                    remarks: event.data,
                    status: OtherReceiptsIssuesEnum.APVL_PENDING_STATUS
                  });
                }
              } else if (this.transferType === OtherReceiptsIssuesEnum.LOSS) {
                if (this.lossForm.invalid) {
                  this.hasNotification = true;
                  this.confirmOverlay(
                    'pw.otherReceiptsIssues.enterApprovalDetailsNotificationMessage'
                  );
                } else {
                  this.hasNotification = true;
                  this.showProgressNotification();
                  this.otherIssueFacade.updateStockRequest({
                    id: this.requestId,
                    reqType: this.reqType,
                    approvalDetails: {
                      data: {
                        approvalCode: this.lossForm.value.approvalCode,
                        approvedBy: this.lossForm.value.approvedBy
                      },
                      type: 'approval'
                    },
                    carrierDetails: {
                      type: null,
                      data: {}
                    },
                    remarks: event.data,
                    status: OtherReceiptsIssuesEnum.APVL_PENDING_STATUS
                  });
                }
              }
            }
          });
      });
  }
  selectAll() {
    if (this.tab === OtherReceiptsIssuesEnum.ALL) {
      this.selectedItemids = this.allOtherIssueIds.concat();
      this.productToBeSavedCount = this.allOtherIssuesCreateCount;
      this.itemIdsLength = this.allOtherIssueIdslength;
    } else if (this.tab === OtherReceiptsIssuesEnum.SELECTED_PRODUCTS) {
      this.selectedItemids = this.selectedtabOtherIssueIds.concat();
      this.productToBeSavedCount = this.selectedOtherIssuesCreateCount;
      this.itemIdsLength = this.selectedtabOtherIssueIdslength;
    }
    this.selectionAllSubject.next({
      selectCheckbox: true,
      enableCheckbox: false
    });
    this.showNotifications(this.productToBeSavedCount);
  }
  selectPagewise() {
    if (this.tab === OtherReceiptsIssuesEnum.ALL) {
      this.selectedItemids = this.allOtherIssueIds.concat();
      this.productToBeSavedCount = this.allOtherIssuesPageCount;
      this.itemIdsLength = this.allOtherIssueIdslength;
    } else if (this.tab === OtherReceiptsIssuesEnum.SELECTED_PRODUCTS) {
      this.selectedItemids = this.selectedtabOtherIssueIds.concat();
      this.productToBeSavedCount = this.selectedtabOtherIssuesPageCount;
      this.itemIdsLength = this.selectedtabOtherIssueIdslength;
    }
    this.selectionAllSubject.next({
      selectCheckbox: true,
      enableCheckbox: true
    });
    this.showNotifications(this.productToBeSavedCount);
  }
  selectionEmit(event) {
    if (event.selected === false) {
      this.itemForm.patchValue({
        selectRadioButton: null
      });
      this.productToBeSavedCount = 0;
      const itemToRemove = event.item;
      this.selectedItemids.splice(
        this.selectedItemids.indexOf(itemToRemove),
        1
      );
    } else if (event.selected === true) {
      this.selectedItemids.push(event.item);
      if (
        this.tab === OtherReceiptsIssuesEnum.ALL &&
        this.selectedItemids.length === this.allOtherIssueIdslength
      ) {
        this.itemForm.patchValue({
          selectRadioButton: '2'
        });
      }
      if (
        this.tab === OtherReceiptsIssuesEnum.SELECTED_PRODUCTS &&
        this.selectedItemids.length === this.selectedtabOtherIssueIdslength
      ) {
        this.itemForm.patchValue({
          selectRadioButton: '2'
        });
      }
    }
    this.productToBeSavedCount = this.selectedItemids.length;
    this.showNotifications(this.productToBeSavedCount);
  }
  showNotifications(count) {
    this.overlayNotification.close();
    if (
      this.tab === OtherReceiptsIssuesEnum.ALL &&
      this.allOtherIssueIdslength !== 0
    ) {
      if (count) {
        this.selectItemsOverlay(
          count,
          'pw.otherReceiptsIssues.productsSelectedNotificationMessage'
        );
      } else {
        this.overlayNotification.close();
      }
    } else if (
      this.tab === OtherReceiptsIssuesEnum.SELECTED_PRODUCTS &&
      this.selectedtabOtherIssueIdslength !== 0
    ) {
      if (this.itemForm.value.selectRadioButton === null) {
        if (
          this.selectedItemids.length === 0 &&
          this.filterSelectedProducts.length === 0
        ) {
          this.confirmOverlay(
            'pw.otherReceiptsIssues.issueAllProductsNotificationMessage'
          );
        } else if (this.selectedItemids.length !== 0) {
          this.deleteItemsOverlay(count, ' Products to be removed');
        }
      } else {
        this.deleteItemsOverlay(count, ' Products to be removed');
      }
    }
  }
  deleteItemsOverlay(count: number, key: any) {
    this.hasNotification = true;
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            buttonText: 'REMOVE PRODUCTS',
            message: count + ' ' + translatedMsg
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.hasNotification = true;
              if (this.itemForm.value.selectRadioButton === '1') {
                this.otherIssueFacade.removeOtherIssueStockRequestItems({
                  id: this.requestId,
                  data: [],
                  requestType: this.reqType
                });
                this.clearSearchItems();
                //this.resetOtherIssueCreateList();
                this.resetValue();
                this.itemForm.patchValue({ selectRadioButton: null });
              }
              if (this.itemForm.value.selectRadioButton === '2') {
                const itemIds = [];
                this.selectedtabOtherIssueIds.forEach(element => {
                  itemIds.push(element.id);
                });
                this.clearSearchItems();
                //this.resetOtherIssueCreateList();
                this.resetValue();
                this.otherIssueFacade.removeOtherIssueStockRequestItems({
                  id: this.requestId,
                  data: itemIds,
                  requestType: this.reqType
                });
              } else if (
                this.itemForm.value.selectRadioButton === null &&
                this.selectedItemids.length !== 0
              ) {
                const itemIds = [];
                this.selectedItemids.forEach(element => {
                  itemIds.push(element.id);
                });
                this.clearSearchItems();
                //this.resetOtherIssueCreateList();
                this.resetValue();
                this.otherIssueFacade.removeOtherIssueStockRequestItems({
                  id: this.requestId,
                  data: itemIds,
                  requestType: this.reqType
                });
              }
              this.getTotalCount();
              //this.loadSelectedProducts();
            }
          });
      });
  }
  quantity(event) {
    this.otherIssueFacade.updateStockRequestCreateItem({
      id: this.requestId,
      itemid: event.itemId,
      reqType: this.reqType,
      value: {
        inventoryId: event.inventoryId,
        measuredWeight: event.measuredWeight,
        quantity: event.quantity,
        status: event.status
      }
    });
  }
  resetValue() {
    this.allOtherIssueIds = [];
    this.selectedItemids = [];
    this.itemIdsLength = 0;
    this.productToBeSavedCount = 0;
    this.selectedtabOtherIssueIds = [];
  }
  resetOtherIssueCreateList() {
    this.otherIssueFacade.resetOtherIssueCreateListItems();
    this.otherIssueFacade.resetOtherIssueCreateResponse();
    this.sortMapAllProducts.clear();
    this.sortMapSelectedProducts.clear();
    this.filterMapAllProducts.clear();
    this.filterMapSelectedProducts.clear();
  }
  selectInfoOverlay(msg: any) {
    this.hasNotification = true;
    this.overlayNotification.show({
      type: OverlayNotificationType.SIMPLE,
      message: msg
    });
  }
  showProgressNotification() {
    const key = 'pw.stockReceiveNotificationMessages.progressMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification.show({
          type: OverlayNotificationType.PROGRESS,
          message: translatedMsg,
          hasBackdrop: true
        });
      });
  }
  selectInfoCloseOverlay(msg: any) {
    this.hasNotification = true;
    this.overlayNotification
      .show({
        type: OverlayNotificationType.SIMPLE,
        message: msg,
        hasBackdrop: true,
        hasClose: true
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        if (event.eventType === OverlayNotificationEventType.CLOSE) {
          const url =
            '/inventory/instock/other-receipts-issues-list/otherissues/' +
            this.transferType;
          this.router.navigateByUrl(url);
        }
      });
  }
  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {});
  }
  /**
   * method to handle shortcut commands
   * @param command: shortcut command
   */
  shortcutEventHandler(command: Command) {
    /* switch (command.name) {
      case SEARCH_SHORTCUT_KEY_F2: {
        if (this.searchRef) {
          this.searchRef.focus();
        }
        break;
      }
    } */
    if (command.name === SEARCH_SHORTCUT_KEY_F2) {
      if (this.searchRef) {
        this.searchRef.focus();
      }
    }
  }
  openFilter(): void {
    this.filterService.DataSource = {
      ...this.productCategory,
      ...this.productGroup
    };
    let filterTabData: { [key: string]: Filter[] } = {};
    if (this.tab === OtherReceiptsIssuesEnum.ALL) {
      filterTabData = this.filterTabDataAllProducts;
    } else if (this.tab === OtherReceiptsIssuesEnum.SELECTED_PRODUCTS) {
      filterTabData = this.filterTabDataSelectedProducts;
    }
    this.filterService
      .openDialog(this.maxFilterLimit, filterTabData)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (filterResult: {
          data: { [key: string]: Filter[] };
          actionfrom: string;
        }) => {
          if (filterResult.actionfrom === FilterActions.APPLY) {
            if (this.tab === OtherReceiptsIssuesEnum.ALL) {
              this.otherIssueFacade.setOtherIssueAllProductsFilter(
                filterResult.data
              );
              const filterData = filterResult.data;
              if (filterData == null) {
                this.filterData = {};
              } else {
                this.filterData = filterData;
              }
              this.filterAllProducts = [];
              if (filterData) {
                let filterValues = [];
                if (filterData['Product Group']) {
                  filterData['Product Group'].forEach(value => {
                    filterValues.push(value.id);
                  });
                  if (filterValues.length > 0) {
                    this.filterAllProducts.push({
                      key: 'productGroup',
                      value: filterValues
                    });
                  }
                }
                filterValues = [];
                if (filterData['Product Category']) {
                  filterData['Product Category'].forEach(value => {
                    filterValues.push(value.id);
                  });
                  if (filterValues.length > 0) {
                    this.filterAllProducts.push({
                      key: 'productCategory',
                      value: filterValues
                    });
                  }
                }
              }
              this.AllOtherIssueItemsPageEvent = this.initialPageEvent;
            } else if (this.tab === OtherReceiptsIssuesEnum.SELECTED_PRODUCTS) {
              this.otherIssueFacade.setOtherIssueSelectedProductsFilter(
                filterResult.data
              );
              const filterData = filterResult.data;
              if (filterData == null) {
                this.filterData = {};
              } else {
                this.filterData = filterData;
              }
              this.filterSelectedProducts = [];
              if (filterData) {
                let filterValues = [];
                if (filterData['Product Group']) {
                  filterData['Product Group'].forEach(value => {
                    filterValues.push(value.id);
                  });
                  if (filterValues.length > 0) {
                    this.filterSelectedProducts.push({
                      key: 'productGroup',
                      value: filterValues
                    });
                  }
                }
                filterValues = [];
                if (filterData['Product Category']) {
                  filterData['Product Category'].forEach(value => {
                    filterValues.push(value.id);
                  });
                  if (filterValues.length > 0) {
                    this.filterSelectedProducts.push({
                      key: 'productCategory',
                      value: filterValues
                    });
                  }
                }
              }
              this.selectedOtherIssueItemsPageEvent = this.initialPageEvent;
            }
            this.conditionalLoad();
            this.clearAll();
          }
        }
      );
  }
  openSort(): void {
    let sortTabData: Column[] = [];
    if (this.tab === OtherReceiptsIssuesEnum.ALL) {
      sortTabData = this.sortTabDataAllProducts;
    } else if (this.tab === OtherReceiptsIssuesEnum.SELECTED_PRODUCTS) {
      sortTabData = this.sortTabDataSelectedProducts;
    }
    this.sortDialogService
      .openDialog(this.maxSortLimit, sortTabData)
      .pipe(takeUntil(this.destroy$))
      .subscribe((sortResult: { data: Column[]; actionfrom: string }) => {
        if (sortResult.actionfrom === FilterActions.APPLY) {
          if (this.tab === OtherReceiptsIssuesEnum.ALL) {
            this.otherIssueFacade.setOtherIssueAllProductsSort(sortResult.data);
            this.sortMapAllProducts.clear();
            const sortData = sortResult.data;
            if (sortData == null || sortData.length === 0) {
              this.sortData = [];
              this.sortOrder = null;
              this.sortBy = null;
            } else {
              this.sortData = sortData;
              if (sortData.length > 0) {
                if (sortData[0].id === 0) {
                  this.sortBy = 'availableWeight';
                } else if (sortData[0].id === 1) {
                  this.sortBy = 'availableQuantity';
                }
                this.sortOrder = sortData[0].sortAscOrder ? 'ASC' : 'DESC';
              }
            }
            if (this.sortBy !== null && this.sortOrder !== null) {
              this.sortMapAllProducts.set(
                'sort',
                this.sortBy + ',' + this.sortOrder
              );
            }
            this.AllOtherIssueItemsPageEvent = this.initialPageEvent;
          } else if (this.tab === OtherReceiptsIssuesEnum.SELECTED_PRODUCTS) {
            this.otherIssueFacade.setOtherIssueSelectedProductsSort(
              sortResult.data
            );
            this.sortMapSelectedProducts.clear();
            const sortData = sortResult.data;
            if (sortData == null || sortData.length === 0) {
              this.sortData = [];
              this.sortOrder = null;
              this.sortBy = null;
            } else {
              this.sortData = sortData;
              if (sortData.length > 0) {
                if (sortData[0].id === 0) {
                  this.sortBy = 'availableWeight';
                } else if (sortData[0].id === 1) {
                  this.sortBy = 'availableQuantity';
                }
                this.sortOrder = sortData[0].sortAscOrder ? 'ASC' : 'DESC';
              }
            }
            if (this.sortBy !== null && this.sortOrder !== null) {
              this.sortMapSelectedProducts.set(
                'sort',
                this.sortBy + ',' + this.sortOrder
              );
            }
            this.selectedOtherIssueItemsPageEvent = this.initialPageEvent;
          }
          this.conditionalLoad();
          this.clearAll();
        }
      });
  }
  mapToFilterOptions(
    key: string,
    options: FilterOption[]
  ): { [key: string]: Filter[] } {
    return {
      [key]: options.map((option, index) => ({
        id: option.id,
        description: option.description,
        selected: false
      }))
    };
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.overlayNotification.close();
  }
}
