import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { AdjustmentItem, OtherReceiptsModel, OtherReceiptItem } from '../models/other-receipt.model';

export interface ItemEntity extends EntityState<OtherReceiptItem> {}
export const itemAdapter = createEntityAdapter<OtherReceiptItem>({
  selectId: item => item.id
});

export const itemSelector = itemAdapter.getSelectors();

export interface OtherReceiptEntity extends EntityState<OtherReceiptsModel> {}
export const OtherReceiptAdapter = createEntityAdapter<OtherReceiptsModel>({
  selectId: stockTransferNote => stockTransferNote.id
});
export const otheReceiptSelector = OtherReceiptAdapter.getSelectors();

export interface AdjustmentEntity extends EntityState<AdjustmentItem> {}
export const adjustmentAdaptor = createEntityAdapter<AdjustmentItem>({
  selectId: adjustmentItem => adjustmentItem.id
});
export const adjustementSelector = adjustmentAdaptor.getSelectors();
