import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../../inventory.state';
import { OtherReceiptsSelector } from './other-receipts.selector';
import * as OtherReceiptsActions from './other-receipts.actions';
import {
  LoadListItemsPayload,
  SearchPendingPayload,
  LoadItemsPayload,
  AdjustmentSearchPayload,
  ConfirmAdjustmentItemsPayload,
  UpdateAdjustementItemPayload,
  SearchItemsPayload,
  UpdateItemPayload,
  UpdateAllItemsPayload,
  ConfirmStockReceivePayload
} from './other-receipts.actions';
import { AdjustmentItem } from '../models/other-receipt.model';
import { Filter, Column } from '@poss-web/shared';

@Injectable()
export class OtherReceiptsFacade {
  private hasError$ = this.store.select(OtherReceiptsSelector.selectHasError);

  private isLoading$ = this.store.select(OtherReceiptsSelector.selectIsLoading);

  private otherReceiptsSTNCount$ = this.store.select(
    OtherReceiptsSelector.selectOtherReceiptsSTNCount
  );

  private receiptList$ = this.store.select(
    OtherReceiptsSelector.selectPendingReceiptList
  );
  private receiptLoanList$ = this.store.select(
    OtherReceiptsSelector.selectPendingReceiptLoanList
  );

  private isLoadingOtherReceiptsSTN$ = this.store.select(
    OtherReceiptsSelector.selectIsLoadingOtherReceiptsSTN
  );
  private isSearchingStocks$ = this.store.select(
    OtherReceiptsSelector.selectIsSearchingStocks
  );
  private hasSearchStockResults$ = this.store.select(
    OtherReceiptsSelector.selectHasSearchStockResults
  );

  private searchStockResults$ = this.store.select(
    OtherReceiptsSelector.selectSearchStockResults
  );
  private nonVerifiedItems$ = this.store.select(
    OtherReceiptsSelector.selectNonVerifiedItems
  );

  private verifiedItems$ = this.store.select(
    OtherReceiptsSelector.selectVerifiedItems
  );

  private isNonVerifiedItemsLoading$ = this.store.select(
    OtherReceiptsSelector.selectIsNonVerifiedItemsLoading
  );

  private isVerifiedItemsLoading$ = this.store.select(
    OtherReceiptsSelector.selectIsVerifiedItemsLoading
  );

  private itemsTotalCountLoaded$ = this.store.select(
    OtherReceiptsSelector.selectisItemsTotalCountLoaded
  );

  private isItemsTotalCountLoading$ = this.store.select(
    OtherReceiptsSelector.selectIsItemsTotalCountLoading
  );

  private isSearchingItems$ = this.store.select(
    OtherReceiptsSelector.selectIsSearchingItems
  );

  private hasSearchedItems$ = this.store.select(
    OtherReceiptsSelector.selectHasSearchedItems
  );

  private binCodes$ = this.store.select(OtherReceiptsSelector.selectBinCodes);
  private remarks$ = this.store.select(OtherReceiptsSelector.selectRemarks);

  private tolerance$ = this.store.select(OtherReceiptsSelector.selectTolerance);

  private selectedStock$ = this.store.select(
    OtherReceiptsSelector.selectSelectedStock
  );

  private nonVerifiedItemsTotalCount$ = this.store.select(
    OtherReceiptsSelector.selectNonVerifiedItemsTotalCount
  );

  private verifiedItemsTotalCount$ = this.store.select(
    OtherReceiptsSelector.selectVerifiedItemsTotalCount
  );

  private isVerifyingAllItemSuccess$ = this.store.select(
    OtherReceiptsSelector.selectIsVerifyingAllItemSuccess
  );

  private isAssigningBinToAllItemsSuccess$ = this.store.select(
    OtherReceiptsSelector.selectIsAssigningBinToAllItemsSuccess
  );

  private confirmedStock$ = this.store.select(
    OtherReceiptsSelector.selectConfirmedStock
  );

  private confirmStockReceiveErrors$ = this.store.select(
    OtherReceiptsSelector.selectConfirmStockReceiveErrors
  );

  private selectedStockLoadError$ = this.store.select(
    OtherReceiptsSelector.selectSelectedStockLoadError
  );

  private isAssigningBinToAllItems$ = this.store.select(
    OtherReceiptsSelector.selectIsAssigningBinToAllItems
  );
  private otherReceiptsDropDown$ = this.store.select(
    OtherReceiptsSelector.selectOtherReceiptsDropDown
  );
  private getSelectedDropDownvalue$ = this.store.select(
    OtherReceiptsSelector.selectOtherReceiptsSelectedDropDown
  );
  private selectTotalReceiptCount$ = this.store.select(
    OtherReceiptsSelector.selectTotalReceiptsCount
  );

  private error$ = this.store.select(OtherReceiptsSelector.selectError);

  private isLoadingBinGroups$ = this.store.select(
    OtherReceiptsSelector.selectIsLoadingBinGroups
  );

  private isLoadingRemarks$ = this.store.select(
    OtherReceiptsSelector.selectIsLoadingRemarks
  );

  private isLoadingTolerance$ = this.store.select(
    OtherReceiptsSelector.selectIsLoadingTolerance
  );

  private isLoadingSelectedStock$ = this.store.select(
    OtherReceiptsSelector.selectIsLoadingSelectedStock
  );

  private updateItemSuccess$ = this.store.select(
    OtherReceiptsSelector.selectUpdateItemSuccess
  );
  private adjustmentSearchedItems$ = this.store.select(
    OtherReceiptsSelector.selectAdjustmentSearchedItems
  );

  private itemsInCart$ = this.store.select(
    OtherReceiptsSelector.selectItemsInCart
  );
  private adjustmentItemsSearchCount$ = this.store.select(
    OtherReceiptsSelector.selectAdjustmentItemsSearchCount
  );
  private cartItemsIds = this.store.select(
    OtherReceiptsSelector.selectCartItemIds
  );
  private confirmAdjustementItemsResponse = this.store.select(
    OtherReceiptsSelector.selectConfirmAdjustementItemsResponse
  );
  private selectAdjustmentItemsInCartsSearch$ = this.store.select(
    OtherReceiptsSelector.selectAdjustmentItemsInCartsSearch
  );
  private receiptADJList$ = this.store.select(
    OtherReceiptsSelector.selectPendingReceiptADJList
  );
  private isSearchingAdjustment$ = this.store.select(
    OtherReceiptsSelector.selectIsSearchingAdjustment
  );
  private hasSearchedItemAdjustment$ = this.store.select(
    OtherReceiptsSelector.selectHasSearchedItemAdjustment
  );
  private IsLoadingAdjustment$ = this.store.select(
    OtherReceiptsSelector.selectIsLoadingAdjustment
  );
  private hasSearchItemInCartSearchAdjustment$ = this.store.select(
    OtherReceiptsSelector.selectHasSearchItemInCartSearch
  );
  //psv
  private psvSearchedItems$ = this.store.select(
    OtherReceiptsSelector.selectPSVSearchedItems
  );

  private itemsInCartPSV$ = this.store.select(
    OtherReceiptsSelector.selectItemsInCartPSV
  );
  private psvItemsSearchCount$ = this.store.select(
    OtherReceiptsSelector.selectPSVItemsSearchCount
  );
  private confirmPSVItemsResponse = this.store.select(
    OtherReceiptsSelector.selectConfirmPSVItemsResponse
  );
  private selectPSVItemsInCartsSearch$ = this.store.select(
    OtherReceiptsSelector.selectPSVItemsInCartsSearch
  );
  private isSearchingPSV$ = this.store.select(
    OtherReceiptsSelector.selectIsSearchingPSV
  );
  private hasSearchedItemPSV$ = this.store.select(
    OtherReceiptsSelector.selectHasSearchedItemPSV
  );
  private IsLoadingPSV$ = this.store.select(
    OtherReceiptsSelector.selectIsLoadingPSV
  );
  private hasSearchItemInCartSearchPSV$ = this.store.select(
    OtherReceiptsSelector.selectHasSearchItemInCartSearchPSV
  );
  private productCategories$ = this.store.select(
    OtherReceiptsSelector.selectProductCategories
  );

  private productGroups$ = this.store.select(
    OtherReceiptsSelector.selectProductGroups
  );

  private filterDataNonVerifiedProducts$ = this.store.select(
    OtherReceiptsSelector.selectfilterDataNonVerifiedProducts
  );
  private filterDataVerifiedProducts$ = this.store.select(
    OtherReceiptsSelector.selectfilterDataVerifiedProducts
  );
  private sortDataNonVerifiedProducts$ = this.store.select(
    OtherReceiptsSelector.selectSortNonVerifiedProducts
  );

  private sortDataVerifiedProducts$ = this.store.select(
    OtherReceiptsSelector.selectSortDataVerifiedProducts
  );
  private itemsCountNonVerified$ = this.store.select(
    OtherReceiptsSelector.selectItemsCountNonVerified
  );
  private itemsCountVerified$ = this.store.select(
    OtherReceiptsSelector.selectItemsCountVerified
  );
  private selectIsNonVerifiedItemsLoaded$ = this.store.select(
    OtherReceiptsSelector.selectisNonVerifiedItemsLoaded
  );
  private selectIsVerifiedItemsLoaded$ = this.store.select(
    OtherReceiptsSelector.selectisVerifiedItemsLoaded
  );
  private selectVerifyItemSuccess$ = this.store.select(
    OtherReceiptsSelector.selectVerifyItemSuccess
  );
  resetOtherReceiptListData() {
    this.store.dispatch(new OtherReceiptsActions.ResetReceiptsListData());
  }
  searchPendingReceiptsStocks(searchPendingpayload: SearchPendingPayload) {
    this.store.dispatch(
      new OtherReceiptsActions.SearchPendingReceipts(searchPendingpayload)
    );
  }

  searchClear() {
    this.store.dispatch(new OtherReceiptsActions.SearchClear());
  }

  loadOtherReceiptsCount() {
    this.store.dispatch(new OtherReceiptsActions.LoadReceiptsSTNCount());
  }

  loadReceiptList(loadReceiptListPayload: LoadListItemsPayload) {
    this.store.dispatch(
      new OtherReceiptsActions.LoadRecieptList(loadReceiptListPayload)
    );
  }
  loadReceiptLoanList(loadReceiptListPayload: LoadListItemsPayload) {
    this.store.dispatch(
      new OtherReceiptsActions.LoadRecieptLoanList(loadReceiptListPayload)
    );
  }

  loadBinCodes(binGroupCode: string) {
    this.store.dispatch(new OtherReceiptsActions.LoadBinCodes(binGroupCode));
  }

  loadRemarks() {
    this.store.dispatch(new OtherReceiptsActions.LoadRemarks());
  }

  loadTolerance() {
    this.store.dispatch(new OtherReceiptsActions.LoadTolerance());
  }

  loadSelectedStock(
    otherReceiptStockPayLoad: OtherReceiptsActions.OtherReceiptStockPayLoad
  ) {
    this.store.dispatch(
      new OtherReceiptsActions.LoadSelectedStock(otherReceiptStockPayLoad)
    );
  }

  loadItemsTotalCount(
    loadItemsTotalCountPayload: OtherReceiptsActions.LoadItemsTotalCountPayload
  ) {
    this.store.dispatch(
      new OtherReceiptsActions.LoadItemsTotalCount(loadItemsTotalCountPayload)
    );
  }

  setSelectedDropDownForReceipts(selectedDropdownValue: string) {
    this.store.dispatch(
      new OtherReceiptsActions.DropDownvalueForReceipts(selectedDropdownValue)
    );
  }

  loadNonVerifiedItems(loadItemsPayload: LoadItemsPayload) {
    this.store.dispatch(
      new OtherReceiptsActions.LoadNonVerifiedItems(loadItemsPayload)
    );
  }

  loadVerifiedItems(loadItemsPayload: LoadItemsPayload) {
    this.store.dispatch(
      new OtherReceiptsActions.LoadVerifiedItems(loadItemsPayload)
    );
  }

  verifyItem(updateItemPayload: UpdateItemPayload) {
    this.store.dispatch(new OtherReceiptsActions.VerifyItem(updateItemPayload));
  }

  updateItem(updateItemPayload: UpdateItemPayload) {
    this.store.dispatch(new OtherReceiptsActions.UpdateItem(updateItemPayload));
  }

  verifyAllItems(updateAllItemsPayload: UpdateAllItemsPayload) {
    this.store.dispatch(
      new OtherReceiptsActions.VerifyAllItems(updateAllItemsPayload)
    );
  }

  confirmStock(confirmStockReceivePayload: ConfirmStockReceivePayload) {
    this.store.dispatch(
      new OtherReceiptsActions.ConfirmStockReceive(confirmStockReceivePayload)
    );
  }
  adjustmentItemSearch(adjustmentSearchPayload: AdjustmentSearchPayload) {
    this.store.dispatch(
      new OtherReceiptsActions.AdjustmentSearch(adjustmentSearchPayload)
    );
  }
  addItemsToCart(item: AdjustmentItem[]) {
    this.store.dispatch(new OtherReceiptsActions.AddItemsToCart(item));
  }
  confirmAdjustementItems(
    confirmAdjustementItems: ConfirmAdjustmentItemsPayload
  ) {
    this.store.dispatch(
      new OtherReceiptsActions.ConfirmAdjustementItems(confirmAdjustementItems)
    );
  }
  removeAdjustementItem(item: AdjustmentItem) {
    this.store.dispatch(new OtherReceiptsActions.RemoveAdjustementItem(item));
  }
  updateAdjustementItem(updateAdjustementItem: UpdateAdjustementItemPayload) {
    this.store.dispatch(
      new OtherReceiptsActions.UpdateAdjustementItem(updateAdjustementItem)
    );
  }
  removeMultipleAdjustementItems(itemIds: number[]) {
    this.store.dispatch(
      new OtherReceiptsActions.RemoveMultipleAdjustementItems(itemIds)
    );
  }
  SearchAdjustmentCartItems(
    searchItems: OtherReceiptsActions.SearchCartItemAdjustmentPayload
  ) {
    this.store.dispatch(
      new OtherReceiptsActions.SearchCartItemsAdjustment(searchItems)
    );
  }
  clearSearchCartItemAdjustment() {
    this.store.dispatch(
      new OtherReceiptsActions.ClearSearchCartItemAdjustment()
    );
  }
  loadReceiptADJList(loadReceiptListPayload: LoadListItemsPayload) {
    this.store.dispatch(
      new OtherReceiptsActions.LoadReceiptsADJList(loadReceiptListPayload)
    );
  }
  clearSearchInventoryItemAdjustment() {
    this.store.dispatch(
      new OtherReceiptsActions.ClearSearchInventoryItemAdjustment()
    );
  }
  psvItemSearch(psvSearchPayload: AdjustmentSearchPayload) {
    this.store.dispatch(new OtherReceiptsActions.PSVSearch(psvSearchPayload));
  }
  addItemsToCartPSV(item: AdjustmentItem[]) {
    this.store.dispatch(new OtherReceiptsActions.AddItemsToCartPSV(item));
  }
  confirmPSVItems(confirmPSVItems: ConfirmAdjustmentItemsPayload) {
    this.store.dispatch(
      new OtherReceiptsActions.ConfirmPSVItems(confirmPSVItems)
    );
  }
  removePSVItem(item: AdjustmentItem) {
    this.store.dispatch(new OtherReceiptsActions.RemovePSVItem(item));
  }
  updatePSVItem(updatePSVItem: UpdateAdjustementItemPayload) {
    this.store.dispatch(new OtherReceiptsActions.UpdatePSVItem(updatePSVItem));
  }
  removeMultiplePSVItems(itemIds: number[]) {
    this.store.dispatch(
      new OtherReceiptsActions.RemoveMultiplePSVItems(itemIds)
    );
  }
  SearchPSVCartItems(
    searchItems: OtherReceiptsActions.SearchCartItemAdjustmentPayload
  ) {
    this.store.dispatch(
      new OtherReceiptsActions.SearchCartItemsPSV(searchItems)
    );
  }
  clearSearchCartItemPSV() {
    this.store.dispatch(new OtherReceiptsActions.ClearSearchCartItemPSV());
  }
  resetPSVReceiptData() {
    this.store.dispatch(new OtherReceiptsActions.ResetPSVReceiptData());
  }
  clearSearchInventoryItemPSV() {
    this.store.dispatch(new OtherReceiptsActions.ClearSearchInventoryItemPSV());
  }
  resetAdjustmentReceiptData() {
    this.store.dispatch(new OtherReceiptsActions.ResetAdjustmentReceiptData());
  }
  getHasError() {
    return this.hasError$;
  }

  getIsLoading() {
    return this.isLoading$;
  }

  getOtherReceiptsSTNCount() {
    return this.otherReceiptsSTNCount$;
  }

  getOtherReceiptList() {
    return this.receiptList$;
  }
  getOtherReceiptLoanList() {
    return this.receiptLoanList$;
  }

  getIsLoadingOtherReceiptSTN() {
    return this.isLoadingOtherReceiptsSTN$;
  }

  getIsSearchingStocks() {
    return this.isSearchingStocks$;
  }

  getHasSearchStockResults() {
    return this.hasSearchStockResults$;
  }

  getSearchStockResults() {
    return this.searchStockResults$;
  }

  assignBinToAllItems(updateAllItemsPayload: UpdateAllItemsPayload) {
    this.store.dispatch(
      new OtherReceiptsActions.AssignBinToAllItems(updateAllItemsPayload)
    );
  }

  getNonVerifiedItems() {
    return this.nonVerifiedItems$;
  }

  getVerifiedItems() {
    return this.verifiedItems$;
  }

  getIsNonVerifiedItemsLoading() {
    return this.isNonVerifiedItemsLoading$;
  }

  getIsVerifiedItemsLoading() {
    return this.isVerifiedItemsLoading$;
  }

  getItemsTotalCountLoaded() {
    return this.itemsTotalCountLoaded$;
  }

  getIsItemsTotalCountLoading() {
    return this.isItemsTotalCountLoading$;
  }

  getIsSearchingItems() {
    return this.isSearchingItems$;
  }

  getHasSearchedItems() {
    return this.hasSearchedItems$;
  }

  getBinCodes() {
    return this.binCodes$;
  }

  getRemarks() {
    return this.remarks$;
  }

  getTolerance() {
    return this.tolerance$;
  }

  getSelectedStock() {
    return this.selectedStock$;
  }

  getNonVerifiedItemsTotalCount() {
    return this.nonVerifiedItemsTotalCount$;
  }

  getVerifiedItemsTotalCount() {
    return this.verifiedItemsTotalCount$;
  }

  getIsVerifyingAllItemSuccess() {
    return this.isVerifyingAllItemSuccess$;
  }

  getIsAssigningBinToAllItemsSuccess() {
    return this.isAssigningBinToAllItemsSuccess$;
  }

  getConfirmedStock() {
    return this.confirmedStock$;
  }

  getConfirmStockReceiveErrors() {
    return this.confirmStockReceiveErrors$;
  }

  getselectedStockLoadError() {
    return this.selectedStockLoadError$;
  }

  getIsAssigningBinToAllItems() {
    return this.isAssigningBinToAllItems$;
  }
  getOtherReceiptsDropdown() {
    return this.otherReceiptsDropDown$;
  }

  getSelectedDropDownvalue() {
    return this.getSelectedDropDownvalue$;
  }

  getTotalReceiptsElementCount() {
    return this.selectTotalReceiptCount$;
  }

  getError() {
    return this.error$;
  }

  getIsLoadingBinGroups() {
    return this.isLoadingBinGroups$;
  }

  getIsLoadingRemarks() {
    return this.isLoadingRemarks$;
  }

  getIsLoadingTolerance() {
    return this.isLoadingTolerance$;
  }

  getIsLoadingSelectedStock() {
    return this.isLoadingSelectedStock$;
  }

  getUpdateItemSuccess() {
    return this.updateItemSuccess$;
  }
  getAdjustmentSearchedItems() {
    return this.adjustmentSearchedItems$;
  }
  getItemsInCart() {
    return this.itemsInCart$;
  }
  getAdjustmentItemsSearchCount() {
    return this.adjustmentItemsSearchCount$;
  }
  getCartItemIds() {
    return this.cartItemsIds;
  }
  getConfirmAdjustementItemsResponse() {
    return this.confirmAdjustementItemsResponse;
  }
  getAdjustmentItemsInCartsSearch() {
    return this.selectAdjustmentItemsInCartsSearch$;
  }
  getReceiptADJList() {
    return this.receiptADJList$;
  }

  getPSVSearchedItems() {
    return this.psvSearchedItems$;
  }
  getItemsInCartPSV() {
    return this.itemsInCartPSV$;
  }
  getPSVItemsSearchCount() {
    return this.psvItemsSearchCount$;
  }
  getConfirmPSVItemsResponse() {
    return this.confirmPSVItemsResponse;
  }
  getPSVItemsInCartsSearch() {
    return this.selectPSVItemsInCartsSearch$;
  }

  getIsSearchingAdjustment() {
    return this.isSearchingAdjustment$;
  }
  gethasSearchedItemAdjustment() {
    return this.hasSearchedItemAdjustment$;
  }
  getIsLoadingAdjustment() {
    return this.IsLoadingAdjustment$;
  }
  getHasSearchItemInCartSearchAdjustment() {
    return this.hasSearchItemInCartSearchAdjustment$;
  }
  getIsSearchingPSV() {
    return this.isSearchingPSV$;
  }
  gethasSearchedItemPSV() {
    return this.hasSearchedItemPSV$;
  }
  getIsLoadingPSV() {
    return this.IsLoadingPSV$;
  }
  getHasSearchItemInCartSearchPSV() {
    return this.hasSearchItemInCartSearchPSV$;
  }

  loadProductGroups() {
    this.store.dispatch(new OtherReceiptsActions.LoadProductGroups());
  }
  getProductGroups() {
    return this.productGroups$;
  }

  loadProductCategories() {
    this.store.dispatch(new OtherReceiptsActions.LoadProductCategories());
  }

  getProductCategories() {
    return this.productCategories$;
  }

  setOtherReciptNonVerifiedFilter(filterData: { [key: string]: Filter[] }) {
    this.store.dispatch(
      new OtherReceiptsActions.SetFilterDataNonVerifiedProducts(filterData)
    );
  }
  setOtherReciptVerifiedFilter(filterData: { [key: string]: Filter[] }) {
    this.store.dispatch(
      new OtherReceiptsActions.SetFilterDataVerifiedProducts(filterData)
    );
  }
  getfilterDataNonVerifiedProducts() {
    return this.filterDataNonVerifiedProducts$;
  }
  getfilterDataVerifiedProducts() {
    return this.filterDataVerifiedProducts$;
  }

  setOtherReceiptNonVerifiedProductsSort(sortData: Column[]) {
    this.store.dispatch(
      new OtherReceiptsActions.SetSortDataNonVerifiedProducts(sortData)
    );
  }
  setOtherReceiptVerifiedProductsSort(sortData: Column[]) {
    this.store.dispatch(
      new OtherReceiptsActions.SetSortDataVerifiedProducts(sortData)
    );
  }

  getSortDataNonVerifiedProducts() {
    return this.sortDataNonVerifiedProducts$;
  }
  getSortDataVerifiedProducts() {
    return this.sortDataVerifiedProducts$;
  }
  getItemsCountNonVerified() {
    return this.itemsCountNonVerified$;
  }
  getItemsCountVerified() {
    return this.itemsCountVerified$;
  }
  getIsNonVerifiedItemsLoaded() {
    return this.selectIsNonVerifiedItemsLoaded$;
  }
  getIsVerifiedItemsLoaded() {
    return this.selectIsVerifiedItemsLoaded$;
  }
  getVerifyItemSuccess() {
    return this.selectVerifyItemSuccess$;
  }
  constructor(private store: Store<State>) {}
}
