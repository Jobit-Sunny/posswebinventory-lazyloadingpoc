import { Effect } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { DataPersistence } from '@nrwl/angular';
import {
  OtherReceiptsActionTypes,
  LoadReceiptsSTNCount,
  LoadReceiptsSTNCountSuccess,
  LoadReceiptsSTNCountFailure,
  LoadOtherReceiptsSTNCountPayload,
  AdjustmentSearchSuccessPayload,
  LoadItemsTotalCountSuccessPayload
} from './other-receipts.actions';
import * as otherRecieptissuesActions from './other-receipts.actions';
import { map, mergeMap } from 'rxjs/operators';
import {
  NotificationService,
  Errors,
  CustomErrors,
  BinCode,
  BinDataService,
  LovDataService,
  Lov,
  ProductCategory,
  ProductGroupDataService,
  ProductCategoryDataService,
  ProductGroup
} from '@poss-web/core';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { CustomErrorAdaptor } from '@poss-web/core';
import { ProductGroupEnum } from '../../../in-stock-management/in-stock/in-stock.enum';
import {
  OtherReceiptsDataModel,
  OtherReceiptsModel,
  AdjustmentItem,
  OtherReceiptItem
} from '../models/other-receipt.model';
import { OtherReceiptIssueService } from '../services/other-receipts-issue.service';
import { OtherReceiptState } from './other-receipts.state';
import { Filter } from '@poss-web/shared';

@Injectable()
export class OtherReceiptsListingEffect {
  @Effect() loadOtherReceiptsCount$ = this.dataPersistence.fetch(
    OtherReceiptsActionTypes.LOAD_RECEIPTS_STN_COUNT,
    {
      // provides an action and the current state of the store
      run: (action: LoadReceiptsSTNCount, state: OtherReceiptState) => {
        return this.otherReceiptIssuesService
          .getOtherReceiptsSTNCount()
          .pipe(
            map(
              (data: LoadOtherReceiptsSTNCountPayload) =>
                new LoadReceiptsSTNCountSuccess(data)
            )
          );
      },

      onError: (action: LoadReceiptsSTNCount, error: HttpErrorResponse) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new LoadReceiptsSTNCountFailure(this.errorHandler(error));
      }
    }
  );

  @Effect() loadReceiptList$: Observable<Action> = this.dataPersistence.fetch(
    OtherReceiptsActionTypes.LOAD_RECEIPT_LIST,
    {
      run: (action: otherRecieptissuesActions.LoadRecieptList) => {
        return this.otherReceiptIssuesService
          .getReceiptsList(
            action.payload.type,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (stockTransferNotes: OtherReceiptsDataModel) =>
                new otherRecieptissuesActions.LoadRecieptListSuccess(
                  stockTransferNotes
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadRecieptList,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.LoadRecieptListFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadReceiptLoanList$: Observable<
    Action
  > = this.dataPersistence.fetch(
    OtherReceiptsActionTypes.LOAD_RECEIPT_LOAN_LIST,
    {
      run: (action: otherRecieptissuesActions.LoadRecieptLoanList) => {
        return this.otherReceiptIssuesService
          .getReceiptsList(
            action.payload.type,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (stockTransferNotes: OtherReceiptsDataModel) =>
                new otherRecieptissuesActions.LoadRecieptLoanListSuccess(
                  stockTransferNotes
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadRecieptLoanList,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.LoadRecieptLoanListFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() searchPendingReceiptsStocks$ = this.dataPersistence.fetch(
    OtherReceiptsActionTypes.SEARCH_PENDING_RECEIPTS,
    {
      run: (action: otherRecieptissuesActions.SearchPendingReceipts) => {
        return this.otherReceiptIssuesService
          .searchRecieptsStocks(
            action.payload.srcDocnumber,
            action.payload.type
          )
          .pipe(
            map(
              (searchresult: OtherReceiptsModel[]) =>
                new otherRecieptissuesActions.SearchPendingReceiptsSuccess(
                  searchresult
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.SearchPendingReceipts,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.SearchPendingReceiptsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadItemsTotalCount$ = this.dataPersistence.fetch(
    OtherReceiptsActionTypes.LOAD_ItEMS_COUNT,
    {
      run: (action: otherRecieptissuesActions.LoadItemsTotalCount) => {
        return this.otherReceiptIssuesService
          .getOtherReceiveItemsCount(
            action.payload.transactionType,
            action.payload.id
          )
          .pipe(
            map(
              (
                loadItemsTotalCountSuccessPayload: LoadItemsTotalCountSuccessPayload
              ) =>
                new otherRecieptissuesActions.LoadItemsTotalCountSuccess(
                  loadItemsTotalCountSuccessPayload
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadItemsTotalCount,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.LoadItemsTotalCountFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadNonVerifiedItems$ = this.dataPersistence.fetch(
    OtherReceiptsActionTypes.LOAD_NON_VERIFIED_ITEMS,
    {
      run: (action: otherRecieptissuesActions.LoadNonVerifiedItems) => {
        return this.otherReceiptIssuesService
          .getTempSortItems(
            action.payload.id,
            'ISSUED',
            action.payload.pageIndex,
            action.payload.pageSize,
            action.payload.sortBy,
            action.payload.property,
            action.payload.transactionType,
            action.payload.itemCode,
            action.payload.lotNumber,
            action.payload.sort,
            action.payload.filter
          )
          .pipe(
            map(
              (data: { items: OtherReceiptItem[]; count: number }) =>
                new otherRecieptissuesActions.LoadNonVerifiedItemsSuccess(data)
            )
          );
      },
      onError: (
        action: otherRecieptissuesActions.LoadNonVerifiedItems,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.LoadNonVerifiedItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadVerifiedItems$ = this.dataPersistence.fetch(
    OtherReceiptsActionTypes.LOAD_VERIFIED_ITEMS,
    {
      run: (action: otherRecieptissuesActions.LoadVerifiedItems) => {
        return this.otherReceiptIssuesService
          .getTempSortItems(
            action.payload.id,
            'VERIFIED',
            action.payload.pageIndex,
            action.payload.pageSize,
            action.payload.sortBy,
            action.payload.property,
            action.payload.transactionType,
            action.payload.itemCode,
            action.payload.lotNumber,
            action.payload.sort,
            action.payload.filter
          )
          .pipe(
            map(
              (data: { items: OtherReceiptItem[]; count: number }) =>
                new otherRecieptissuesActions.LoadVerifiedItemsSuccess(data)
            )
          );
      },
      onError: (
        action: otherRecieptissuesActions.LoadVerifiedItems,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.LoadVerifiedItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadBins$ = this.dataPersistence.fetch(
    OtherReceiptsActionTypes.LOAD_BIN_CODES,
    {
      run: (action: otherRecieptissuesActions.LoadBinCodes) => {
        return this.binDataService
          .getBinDetails(action.payload, true, false)
          .pipe(
            map(
              (bins: BinCode[]) =>
                new otherRecieptissuesActions.LoadBinCodesSuccess(bins)
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadBinCodes,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.LoadBinCodesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadRemarks$ = this.dataPersistence.fetch(
    OtherReceiptsActionTypes.LOAD_REMARKS,
    {
      run: (action: otherRecieptissuesActions.LoadRemarks) => {
        return this.lovDataService
          .getInventoryLovs('DEFECTTYPE')
          .pipe(
            map(
              (remarks: Lov[]) =>
                new otherRecieptissuesActions.LoadRemarksSuccess(remarks)
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadRemarks,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.LoadRemarksFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadTolerance$ = this.dataPersistence.fetch(
    OtherReceiptsActionTypes.LOAD_TOLERANCE,
    {
      run: (action: otherRecieptissuesActions.LoadTolerance) => {
        return this.otherReceiptIssuesService
          .getTolerance()
          .pipe(
            map(
              (tolerance: number) =>
                new otherRecieptissuesActions.LoadToleranceSuccess(tolerance)
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadTolerance,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.LoadToleranceFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  // TODO : check pessimisticUpdate
  @Effect()
  verifyItem$ = this.dataPersistence.pessimisticUpdate(
    OtherReceiptsActionTypes.VERIFY_ITEM,
    {
      run: (action: otherRecieptissuesActions.VerifyItem) => {
        return this.otherReceiptIssuesService
          .verifyOtherReceiptItem(
            action.payload.id,
            action.payload.itemId,
            action.payload.newUpdate,
            action.payload.transactionType
          )
          .pipe(
            map(
              (item: OtherReceiptItem) =>
                new otherRecieptissuesActions.VerifyItemSuccess(item)
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.VerifyItem,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.VerifyItemFailure({
          itemId: action.payload.itemId,
          actualDetails: action.payload.actualDetails,
          error: this.errorHandler(error)
        });
      }
    }
  );

  @Effect()
  updateItem$ = this.dataPersistence.pessimisticUpdate(
    OtherReceiptsActionTypes.UPADTE_ITEM,
    {
      run: (action: otherRecieptissuesActions.UpdateItem) => {
        return this.otherReceiptIssuesService
          .verifyOtherReceiptItem(
            action.payload.id,
            action.payload.itemId,
            action.payload.newUpdate,
            action.payload.transactionType
          )
          .pipe(
            map(
              (item: OtherReceiptItem) =>
                new otherRecieptissuesActions.UpdateItemSuccess(item)
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.UpdateItem,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.UpdateItemFailure({
          itemId: action.payload.itemId,
          actualDetails: action.payload.actualDetails,
          error: this.errorHandler(error)
        });
      }
    }
  );

  @Effect()
  verifyAllItems$ = this.dataPersistence.pessimisticUpdate(
    OtherReceiptsActionTypes.VERIFY_ALL_ITEMS,
    {
      run: (action: otherRecieptissuesActions.VerifyAllItems) => {
        return this.otherReceiptIssuesService
          .updateAllOtherReceiptItems(
            action.payload.id,
            action.payload.data,
            action.payload.transactionType
          )
          .pipe(
            map(
              (isSuccess: boolean) =>
                new otherRecieptissuesActions.VerifyAllItemsSuccess(isSuccess)
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.VerifyAllItems,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.VerifyAllItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  assignBinToAllItems$ = this.dataPersistence.pessimisticUpdate(
    OtherReceiptsActionTypes.ASSIGN_BIN_ALL_ITEMS,
    {
      run: (action: otherRecieptissuesActions.AssignBinToAllItems) => {
        return this.otherReceiptIssuesService
          .updateAllOtherReceiptItems(
            action.payload.id,
            action.payload.data,
            action.payload.transactionType
          )
          .pipe(
            map(
              (isSuccess: boolean) =>
                new otherRecieptissuesActions.AssignBinToAllItemsSuccess(
                  isSuccess
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.AssignBinToAllItems,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.AssignBinToAllItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  confirmStock$ = this.dataPersistence.pessimisticUpdate(
    OtherReceiptsActionTypes.CONFIRM_STOCK_RECEIVE,
    {
      run: (action: otherRecieptissuesActions.ConfirmStockReceive) => {
        return this.otherReceiptIssuesService
          .confirmOtherReceiveStn(
            action.payload.id,
            action.payload.confirmReceive,
            action.payload.transactionType
          )
          .pipe(
            map(
              (confirmedStock: any) =>
                new otherRecieptissuesActions.ConfirmStockReceiveSuccess(
                  confirmedStock
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.ConfirmStockReceive,
        httpErrorResponse: HttpErrorResponse
      ) => {
        this.notificationService.error(httpErrorResponse.error);

        return new otherRecieptissuesActions.ConfirmStockReceiveFailure(
          this.errorHandler(httpErrorResponse)
        );
      }
    }
  );

  @Effect() loadSeletedStock$ = this.dataPersistence.fetch(
    OtherReceiptsActionTypes.LOAD_SELECTED_STOCK,
    {
      run: (action: otherRecieptissuesActions.LoadSelectedStock) => {
        return this.otherReceiptIssuesService
          .getOtherReceiptStock(
            action.payload.id,
            action.payload.transactionType
          )
          .pipe(
            map(
              (stockTransferNote: OtherReceiptsModel) =>
                new otherRecieptissuesActions.LoadSelectedStockSuccess(
                  stockTransferNote
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadSelectedStock,
        httpErrorResponse: HttpErrorResponse
      ) => {
        this.notificationService.error(httpErrorResponse.error);
        return new otherRecieptissuesActions.LoadSelectedStockFailure(
          this.errorHandler(httpErrorResponse)
        );
      }
    }
  );
  @Effect() adjustmentSearch = this.dataPersistence.pessimisticUpdate(
    OtherReceiptsActionTypes.ADJUSTMENT_SEARCH,
    {
      run: (action: otherRecieptissuesActions.AdjustmentSearch) => {
        return this.otherReceiptIssuesService
          .adjustmentItemsSearch(action.payload)
          .pipe(
            map(
              (adjustmentSuccessPaylaod: AdjustmentSearchSuccessPayload) =>
                new otherRecieptissuesActions.AdjustmentSearchSuccess({
                  items: adjustmentSuccessPaylaod.items.filter(data => {
                    return data.productGroupId === ProductGroupEnum.PLAIN;
                  }),
                  count: adjustmentSuccessPaylaod.count
                })
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.AdjustmentSearch,
        httpErrorResponse: HttpErrorResponse
      ) => {
        this.notificationService.error(httpErrorResponse.error);
        return new otherRecieptissuesActions.AdjustmentSearchFailure(
          this.errorHandler(httpErrorResponse)
        );
      }
    }
  );
  @Effect() confirmAdjustmentItems$ = this.dataPersistence.pessimisticUpdate(
    OtherReceiptsActionTypes.CONFIRM_ADJUSTEMENT_ITEMS,
    {
      run: (action: otherRecieptissuesActions.ConfirmAdjustementItems) => {
        return this.otherReceiptIssuesService
          .confirmAdjustementItems(action.payload)
          .pipe(
            map(
              (confirmAdjustementItem: AdjustmentItem) =>
                new otherRecieptissuesActions.ConfirmAdjustementItemsSuccess(
                  confirmAdjustementItem
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.ConfirmAdjustementItems,
        httpErrorResponse: HttpErrorResponse
      ) => {
        this.notificationService.error(httpErrorResponse.error);
        return new otherRecieptissuesActions.ConfirmAdjustementItemsFailure(
          this.errorHandler(httpErrorResponse)
        );
      }
    }
  );
  @Effect() loadreceiptsADJList$: Observable<
    Action
  > = this.dataPersistence.fetch(
    OtherReceiptsActionTypes.LOAD_RECEIPTS_ADJ_LIST,
    {
      run: (action: otherRecieptissuesActions.LoadReceiptsADJList) => {
        return this.otherReceiptIssuesService
          .getReceiptsList(
            action.payload.type,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (stockTransferNotes: OtherReceiptsDataModel) =>
                new otherRecieptissuesActions.LoadReceiptsADJListSuccess(
                  stockTransferNotes
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadReceiptsADJList,
        error: HttpErrorResponse
      ) => {
        this.notificationService.error(CustomErrorAdaptor.fromJson(error));
        return new otherRecieptissuesActions.LoadReceiptsADJListFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  //psv
  @Effect() psvSearch = this.dataPersistence.pessimisticUpdate(
    OtherReceiptsActionTypes.PSV_SEARCH,
    {
      run: (action: otherRecieptissuesActions.PSVSearch) => {
        return this.otherReceiptIssuesService
          .adjustmentItemsSearch(action.payload)
          .pipe(
            map(
              (psvSuccessPaylaod: AdjustmentSearchSuccessPayload) =>
                new otherRecieptissuesActions.PSVSearchSuccess({
                  items: psvSuccessPaylaod.items.filter(data => {
                    return data.productGroupId === ProductGroupEnum.STUDDED;
                  }),
                  count: psvSuccessPaylaod.count
                })
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.PSVSearch,
        httpErrorResponse: HttpErrorResponse
      ) => {
        this.notificationService.error(httpErrorResponse.error);
        return new otherRecieptissuesActions.PSVSearchFailure(
          this.errorHandler(httpErrorResponse)
        );
      }
    }
  );
  @Effect() confirmPSVItems$ = this.dataPersistence.pessimisticUpdate(
    OtherReceiptsActionTypes.CONFIRM_PSV_ITEMS,
    {
      run: (action: otherRecieptissuesActions.ConfirmPSVItems) => {
        return this.otherReceiptIssuesService
          .confirmAdjustementItems(action.payload)
          .pipe(
            map(
              (confirmAdjustementItem: AdjustmentItem) =>
                new otherRecieptissuesActions.ConfirmPSVItemsSuccess(
                  confirmAdjustementItem
                )
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.ConfirmPSVItems,
        httpErrorResponse: HttpErrorResponse
      ) => {
        this.notificationService.error(httpErrorResponse.error);
        return new otherRecieptissuesActions.ConfirmPSVItemsFailure(
          this.errorHandler(httpErrorResponse)
        );
      }
    }
  );
  @Effect() loadProductCategories = this.dataPersistence.fetch(
    OtherReceiptsActionTypes.LOAD_PRODUCT_CATEGORIES,
    {
      run: (action: otherRecieptissuesActions.LoadProductCategories) => {
        return this.productCategoryDataService
          .getProductCategories()
          .pipe(
            map(
              (data: ProductCategory[]) =>
                new otherRecieptissuesActions.LoadProductCategoriesSuccess(data)
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadProductCategories,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.LoadProductCategoriesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadProductGroups = this.dataPersistence.fetch(
    OtherReceiptsActionTypes.LOAD_PROUDCT_GROUPS,
    {
      run: (action: otherRecieptissuesActions.LoadProductGroups) => {
        return this.productGroupDataService
          .getProductGroups()
          .pipe(
            map(
              (data: ProductGroup[]) =>
                new otherRecieptissuesActions.LoadProductGroupsSuccess(data)
            )
          );
      },

      onError: (
        action: otherRecieptissuesActions.LoadProductGroups,
        error: HttpErrorResponse
      ) => {
        return new otherRecieptissuesActions.LoadProductGroupsFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
  constructor(
    private dataPersistence: DataPersistence<OtherReceiptState>,
    private notificationService: NotificationService,
    private otherReceiptIssuesService: OtherReceiptIssueService,
    private binDataService: BinDataService,
    private lovDataService: LovDataService,
    private productGroupDataService: ProductGroupDataService,
    private productCategoryDataService: ProductCategoryDataService
  ) {}
}
