import {
  OtherReceiptsActionTypes,
  OtherReceiptsActions
} from './other-receipts.actions';
import {
  OtherReceiptAdapter,
  itemAdapter,
  ItemEntity,
  adjustmentAdaptor
} from './other-receipts.entity';
import { OtherReceiptsIssuesEnum } from '../../../in-stock-management/in-stock/in-stock.enum';
import { OtherReceiptState } from './other-receipts.state';

export const initialState: OtherReceiptState = {
  isLoading: false,

  pendingOtherReceiptsSTNCount: 0,
  otherReceiptList: OtherReceiptAdapter.getInitialState(),
  otherReceiptLoanList: OtherReceiptAdapter.getInitialState(),
  isLoadingOtherReceiptList: false,
  isLoadingOtherReceiptLoanList: true,
  isSearchingStocks: false,
  searchStockResults: OtherReceiptAdapter.getInitialState(),
  hasSearchStockResults: null,
  nonVerifiedItems: itemAdapter.getInitialState(),
  isNonVerifiedItemsLoading: false,
  nonVerifiedItemsTotalCount: 0,
  itemsCountNonVerified: 0,
  itemsCountVerified: 0,
  verifiedItems: itemAdapter.getInitialState(),
  isVerifiedItemsLoading: false,
  verifiedItemsTotalCount: 0,

  isItemsTotalCountLoaded: null,
  isItemsTotalCountLoading: false,

  isSearchingItems: false,
  hasSearchedItems: false,
  binCodes: null,
  remarks: null,
  tolerance: null,

  selectedStock: null,
  // selectedStockLoadError: null,
  confirmedStock: null,
  isVerifyingAllItem: false,
  isVerifyingAllItemSuccess: null,
  confirmStockReceiveErrors: null,
  selectedStockLoadError: null,
  isAssigningBinToAllItems: false,
  isAssigningBinToAllItemsSuccess: null,
  isConfirmStockReceiveSuccess: null,
  isConfirmingStockReceive: false,
  verifyItemSuccess: null,
  OtherReceiptsDropdownValues: null,
  selectedDropDownForReceipts: OtherReceiptsIssuesEnum.EXHIBITION_TYPE,
  error: null,
  isItemIssued: false,
  totalElementsOtherReceipts: 0,
  isLoadingRemarks: false,
  isLoadingTolerance: false,
  isLoadingSelectedStock: false,
  updateItemSuccess: false,
  isLoadingBinGroups: false,
  isSearchingAdjustmentItems: false,
  hasSearchedAdjustmentItems: false,
  IsLoadingAdjustment: false,
  adjustmentSearchedItems: adjustmentAdaptor.getInitialState(),
  itemsInCarts: adjustmentAdaptor.getInitialState(),
  adjustmentSearchedItemsCount: null,
  ConfirmAdjustementItemResponse: null,
  AdjustmentItemsInCartsSearch: adjustmentAdaptor.getInitialState(),
  hasSearchItemInCartSearchAdjustment: false,
  otherReceiptADJList: OtherReceiptAdapter.getInitialState(),
  isLoadingOtherReceiptADJList: false,

  isSearchingPSVItems: false,
  hasSearchedItemPSV: false,
  psvSearchedItems: adjustmentAdaptor.getInitialState(),
  itemsInCartsPSV: adjustmentAdaptor.getInitialState(),
  psvSearchedItemsCount: null,
  ConfirmPSVItemResponse: null,
  IsLoadingPSV: false,
  hasSearchItemInCartSearchPSV: false,
  PSVItemsInCartsSearch: adjustmentAdaptor.getInitialState(),

  productCategories: null,
  productGroups: null,

  filterDataNonVerifiedProducts: {},
  filterDataVerifiedProducts: {},

  sortDataNonVerifiedProducts: [],
  sortDataVerifiedProducts: [],
  isNonVerifiedItemsLoaded: null,
  isVerifiedItemsLoaded: null
};

export function OtherReceiptsReducer(
  state: OtherReceiptState = initialState,
  action: OtherReceiptsActions
): OtherReceiptState {
  switch (action.type) {
    case OtherReceiptsActionTypes.RESET_RECEIPTS_LIST_DATA:
      return {
        ...state,
        pendingOtherReceiptsSTNCount: 0,
        OtherReceiptsDropdownValues: null,
        otherReceiptList: OtherReceiptAdapter.getInitialState(),
        otherReceiptLoanList: OtherReceiptAdapter.getInitialState(),
        totalElementsOtherReceipts: 0,
        selectedDropDownForReceipts: OtherReceiptsIssuesEnum.EXHIBITION_TYPE,
        error: null
      };
    case OtherReceiptsActionTypes.LOAD_RECEIPTS_STN_COUNT:
      return {
        ...state,
        error: null
      };
    case OtherReceiptsActionTypes.LOAD_RECEIPTS_STN_COUNT_SUCCESS:
      return {
        ...state,
        pendingOtherReceiptsSTNCount:
          action.payload.pendingOtherReceiptsSTNCount,
        OtherReceiptsDropdownValues: action.payload.countData
      };
    case OtherReceiptsActionTypes.LOAD_RECEIPTS_STN_COUNT_FAILURE:
      return {
        ...state,
        error: action.payload
      };

    case OtherReceiptsActionTypes.LOAD_RECEIPT_LIST:
      return {
        ...state,
        isLoadingOtherReceiptList: true,
        error: null
      };

    case OtherReceiptsActionTypes.LOAD_RECEIPT_LIST_SUCCESS:
      return {
        ...state,
        otherReceiptList: OtherReceiptAdapter.addMany(
          action.payload.receiptsData,
          state.otherReceiptList
        ),
        isLoadingOtherReceiptList: false,
        totalElementsOtherReceipts: action.payload.totalElements,
        error: null
      };

    case OtherReceiptsActionTypes.LOAD_RECEIPT_LIST_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingOtherReceiptList: false
      };

    case OtherReceiptsActionTypes.SEARCH_PENDING_RECEIPTS:
      return {
        ...state,
        isSearchingStocks: true,
        hasSearchStockResults: null,
        searchStockResults: OtherReceiptAdapter.removeAll(
          state.searchStockResults
        ),
        error: null
      };

    case OtherReceiptsActionTypes.SEARCH_PENDING_RECEIPTS_SUCCESS:
      return {
        ...state,
        searchStockResults: OtherReceiptAdapter.addAll(
          action.payload,
          state.searchStockResults
        ),
        isSearchingStocks: false,
        hasSearchStockResults: true
      };

    case OtherReceiptsActionTypes.SEARCH_PENDING_RECEIPTS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isSearchingStocks: false,
        hasSearchStockResults: false
      };

    case OtherReceiptsActionTypes.SEARCH_CLEAR:
      return {
        ...state,
        searchStockResults: OtherReceiptAdapter.removeAll(
          state.searchStockResults
        ),
        error: null,
        isSearchingStocks: false,
        hasSearchStockResults: null
      };

    case OtherReceiptsActionTypes.LOAD_ItEMS_COUNT:
      return {
        ...state,
        isItemsTotalCountLoading: true,
        isItemsTotalCountLoaded: null,
        nonVerifiedItemsTotalCount: 0,
        verifiedItemsTotalCount: 0,
        error: null
      };

    case OtherReceiptsActionTypes.LOAD_ItEMS_COUNT_SUCCESS:
      return {
        ...state,
        nonVerifiedItemsTotalCount: action.payload.nonVerifiedItemsTotalCount,
        verifiedItemsTotalCount: action.payload.verifiedItemsTotalCount,
        isItemsTotalCountLoading: false,
        isItemsTotalCountLoaded: true
      };

    case OtherReceiptsActionTypes.LOAD_ItEMS_COUNT_FAILURE:
      return {
        ...state,
        error: action.payload,
        isItemsTotalCountLoading: false,
        isItemsTotalCountLoaded: false
      };

    case OtherReceiptsActionTypes.LOAD_SELECTED_STOCK:
      return {
        ...state,
        isLoading: true,
        selectedStock: null,
        selectedStockLoadError: null,

        isItemsTotalCountLoading: false,
        isItemsTotalCountLoaded: null,

        nonVerifiedItems: itemAdapter.removeAll(state.nonVerifiedItems),
        isNonVerifiedItemsLoading: false,
        nonVerifiedItemsTotalCount: 0,

        verifiedItems: itemAdapter.removeAll(state.verifiedItems),
        isVerifiedItemsLoading: false,
        verifiedItemsTotalCount: 0,

        isSearchingItems: false,
        hasSearchedItems: false,

        isVerifyingAllItem: false,
        isVerifyingAllItemSuccess: null,

        isAssigningBinToAllItems: false,
        isAssigningBinToAllItemsSuccess: null,

        confirmStockReceiveErrors: null,
        confirmedStock: null,
        isConfirmStockReceiveSuccess: null,
        isConfirmingStockReceive: false,
        error: null
      };

    case OtherReceiptsActionTypes.LOAD_SELECTED_STOCK_SUCCESS:
      return {
        ...state,
        selectedStock: action.payload,
        isLoading: false
      };

    case OtherReceiptsActionTypes.LOAD_SELECTED_STOCK_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };

    case OtherReceiptsActionTypes.LOAD_NON_VERIFIED_ITEMS:
      return {
        ...state,
        isNonVerifiedItemsLoading: true,
        isNonVerifiedItemsLoaded: null,
        error: null
      };

    case OtherReceiptsActionTypes.LOAD_NON_VERIFIED_ITEMS_SUCCESS:
      return {
        ...state,
        nonVerifiedItems: itemAdapter.addAll(
          action.payload.items,
          state.nonVerifiedItems
        ),
        itemsCountNonVerified: action.payload.count,
        isNonVerifiedItemsLoading: false,
        isNonVerifiedItemsLoaded: true
      };

    case OtherReceiptsActionTypes.LOAD_NON_VERIFIED_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isNonVerifiedItemsLoading: false,
        isNonVerifiedItemsLoaded: false
      };

    case OtherReceiptsActionTypes.LOAD_VERIFIED_ITEMS:
      return {
        ...state,
        isVerifiedItemsLoading: true,
        isVerifiedItemsLoaded: null,
        error: null
      };

    case OtherReceiptsActionTypes.LOAD_VERIFIED_ITEMS_SUCCESS:
      return {
        ...state,
        verifiedItems: itemAdapter.addAll(
          action.payload.items,
          state.verifiedItems
        ),
        itemsCountVerified: action.payload.count,
        isVerifiedItemsLoading: false,
        isVerifiedItemsLoaded: true
      };

    case OtherReceiptsActionTypes.LOAD_VERIFIED_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isVerifiedItemsLoading: false,
        isVerifiedItemsLoaded: false
      };

    case OtherReceiptsActionTypes.LOAD_BIN_CODES:
      return {
        ...state,
        isLoadingBinGroups: true,
        error: null
      };
    case OtherReceiptsActionTypes.LOAD_BIN_CODES_SUCCESS:
      return {
        ...state,
        binCodes: action.payload,
        isLoadingBinGroups: false
      };

    case OtherReceiptsActionTypes.LOAD_BIN_CODES_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingBinGroups: false
      };

    case OtherReceiptsActionTypes.VERIFY_ITEM:
      return {
        ...state,
        nonVerifiedItems: itemAdapter.updateOne(
          {
            id: action.payload.itemId,
            changes: {
              binCode: action.payload.newUpdate.binCode,
              binGroupCode: action.payload.newUpdate.binGroupCode,
              measuredWeight: action.payload.newUpdate.measuredWeight,
              remarks: action.payload.newUpdate.remarks,
              isUpdating: true,
              isUpdatingSuccess: null
            }
          },
          state.nonVerifiedItems
        ),
        error: null,
        verifyItemSuccess: null
      };

    case OtherReceiptsActionTypes.VERIFY_ITEM_SUCCESS:
      const newNonVerifiedItems: ItemEntity = itemAdapter.removeOne(
        action.payload.id,
        state.nonVerifiedItems
      );

      const countDiff =
        state.nonVerifiedItems.ids.length - newNonVerifiedItems.ids.length;

      return {
        ...state,
        nonVerifiedItems: newNonVerifiedItems,
        nonVerifiedItemsTotalCount:
          state.nonVerifiedItemsTotalCount - countDiff,
        verifiedItemsTotalCount: state.verifiedItemsTotalCount + countDiff,
        verifyItemSuccess: true
      };

    case OtherReceiptsActionTypes.VERIFY_ITEM_FAILURE:
      return {
        ...state,
        nonVerifiedItems: itemAdapter.updateOne(
          {
            id: action.payload.itemId,
            changes: {
              isUpdating: false,
              isUpdatingSuccess: false
            }
          },
          state.nonVerifiedItems
        ),

        error: action.payload.error,
        verifyItemSuccess: false
      };

    case OtherReceiptsActionTypes.UPADTE_ITEM:
      return {
        ...state,

        verifiedItems: itemAdapter.updateOne(
          {
            id: action.payload.itemId,
            changes: {
              binCode: action.payload.newUpdate.binCode,
              binGroupCode: action.payload.newUpdate.binGroupCode,
              measuredWeight: action.payload.newUpdate.measuredWeight,
              remarks: action.payload.newUpdate.remarks,
              isUpdating: true,
              isUpdatingSuccess: null
            }
          },
          state.verifiedItems
        ),
        error: null
      };

    case OtherReceiptsActionTypes.UPADTE_ITEM_SUCCESS:
      return {
        ...state,
        verifiedItems: itemAdapter.updateOne(
          {
            id: action.payload.id,
            changes: {
              ...action.payload,
              isUpdating: false,
              isUpdatingSuccess: true
            }
          },
          state.verifiedItems
        )
      };

    case OtherReceiptsActionTypes.UPADTE_ITEM_FAILURE:
      return {
        ...state,
        verifiedItems: itemAdapter.updateOne(
          {
            id: action.payload.itemId,
            changes: {
              binCode: action.payload.actualDetails.binCode,
              binGroupCode: action.payload.actualDetails.binGroupCode,
              measuredWeight: action.payload.actualDetails.measuredWeight,
              remarks: action.payload.actualDetails.remarks,
              isUpdating: false,
              isUpdatingSuccess: false
            }
          },
          state.verifiedItems
        ),

        error: action.payload.error
      };

    case OtherReceiptsActionTypes.CONFIRM_STOCK_RECEIVE:
      return {
        ...state,
        confirmedStock: null,
        isConfirmStockReceiveSuccess: null,
        isConfirmingStockReceive: true,
        confirmStockReceiveErrors: null
      };

    case OtherReceiptsActionTypes.CONFIRM_STOCK_RECEIVE_SUCCESS:
      return {
        ...state,
        confirmedStock: action.payload,
        isConfirmStockReceiveSuccess: true,
        isConfirmingStockReceive: false,

        // nonVerifiedItemsTotalCount : 0,
        // verifiedItemsTotalCount : 0,
        // TODO : check this  safe delete
        // pendingFactorySTN: stockTransferNoteAdapter.removeOne(action.payload.id, state.pendingFactorySTN),
        // pendingBoutiqueSTN: stockTransferNoteAdapter.removeOne(action.payload.id, state.pendingBoutiqueSTN),
        // pendingOthersSTN: stockTransferNoteAdapter.removeOne(action.payload.id, state.pendingOthersSTN),
        // pendingCFAInvoice: invoiceAdapter.removeOne(action.payload.id, state.pendingCFAInvoice),

        otherReceiptList: OtherReceiptAdapter.removeAll(state.otherReceiptList)
      };

    case OtherReceiptsActionTypes.CONFIRM_STOCK_RECEIVE_FAILURE:
      return {
        ...state,
        error: action.payload,
        isConfirmStockReceiveSuccess: false,
        isConfirmingStockReceive: false
      };

    case OtherReceiptsActionTypes.LOAD_REMARKS:
      return {
        ...state,
        isLoadingRemarks: true,
        error: null
      };
    case OtherReceiptsActionTypes.LOAD_REMARKS_SUCCESS:
      return {
        ...state,
        remarks: action.payload,
        isLoadingRemarks: false
      };

    case OtherReceiptsActionTypes.LOAD_REMARKS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingRemarks: false
      };
    case OtherReceiptsActionTypes.LOAD_TOLERANCE:
      return {
        ...state,
        isLoadingTolerance: true,
        error: null
      };
    case OtherReceiptsActionTypes.LOAD_TOLERANCE_SUCCESS:
      return {
        ...state,
        tolerance: action.payload,
        isLoadingTolerance: false
      };

    case OtherReceiptsActionTypes.LOAD_TOLERANCE_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingTolerance: false
      };

    case OtherReceiptsActionTypes.VERIFY_ALL_ITEMS:
      return {
        ...state,
        isVerifyingAllItem: true,
        isVerifyingAllItemSuccess: null,
        error: null
      };

    case OtherReceiptsActionTypes.VERIFY_ALL_ITEMS_SUCCESS:
      return {
        ...state,
        nonVerifiedItems: itemAdapter.removeAll(state.nonVerifiedItems),
        nonVerifiedItemsTotalCount: 0,
        verifiedItemsTotalCount:
          state.verifiedItemsTotalCount + state.nonVerifiedItemsTotalCount,
        isVerifyingAllItem: false,
        isVerifyingAllItemSuccess: true
      };

    case OtherReceiptsActionTypes.VERIFY_ALL_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isVerifyingAllItem: false,
        isVerifyingAllItemSuccess: false
      };

    case OtherReceiptsActionTypes.ASSIGN_BIN_ALL_ITEMS:
      return {
        ...state,
        isAssigningBinToAllItems: true,
        isAssigningBinToAllItemsSuccess: null,
        error: null
      };

    case OtherReceiptsActionTypes.ASSIGN_BIN_ALL_ITEMS_SUCCESS:
      return {
        ...state,
        isAssigningBinToAllItems: false,
        isAssigningBinToAllItemsSuccess: true
      };

    case OtherReceiptsActionTypes.ASSIGN_BIN_ALL_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isAssigningBinToAllItems: false,
        isAssigningBinToAllItemsSuccess: false
      };
    case OtherReceiptsActionTypes.LOAD_RECEIPT_LOAN_LIST:
      return {
        ...state,
        isLoadingOtherReceiptLoanList: true,
        error: null
      };

    case OtherReceiptsActionTypes.LOAD_RECEIPT_LOAN_LIST_SUCCESS:
      return {
        ...state,
        otherReceiptLoanList: OtherReceiptAdapter.addMany(
          action.payload.receiptsData,
          state.otherReceiptLoanList
        ),
        isLoadingOtherReceiptLoanList: false,
        totalElementsOtherReceipts: action.payload.totalElements,
        error: null
      };

    case OtherReceiptsActionTypes.LOAD_RECEIPT_LOAN_LIST_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingOtherReceiptLoanList: false
      };
    case OtherReceiptsActionTypes.DROPDOWN_SELECTED_FOR_RECEIPTS:
      return {
        ...state,
        selectedDropDownForReceipts: action.payload
      };
    case OtherReceiptsActionTypes.ADJUSTMENT_SEARCH:
      return {
        ...state,
        isSearchingAdjustmentItems: true,
        hasSearchedAdjustmentItems: false,
        hasError: null
      };
    case OtherReceiptsActionTypes.ADJUSTMENT_SEARCH_SUCCESS:
      return {
        ...state,
        isSearchingAdjustmentItems: false,
        hasSearchedAdjustmentItems: true,
        adjustmentSearchedItems: adjustmentAdaptor.addAll(
          action.payload.items,
          state.adjustmentSearchedItems
        ),
        adjustmentSearchedItemsCount: action.payload.count
      };

    case OtherReceiptsActionTypes.ADJUSTMENT_SEARCH_FAILUREE:
      return {
        ...state,
        isSearchingAdjustmentItems: false,
        hasSearchedAdjustmentItems: false,
        error: action.payload
      };
    case OtherReceiptsActionTypes.ADD_ITEMS_TO_CART:
      return {
        ...state,
        adjustmentSearchedItems: adjustmentAdaptor.removeAll(
          state.adjustmentSearchedItems
        ),
        itemsInCarts: adjustmentAdaptor.addMany(
          action.payload,
          state.itemsInCarts
        )
      };
    case OtherReceiptsActionTypes.CONFIRM_ADJUSTEMENT_ITEMS:
      return {
        ...state,
        IsLoadingAdjustment: true
      };
    case OtherReceiptsActionTypes.CONFIRM_ADJUSTEMENT_ITEMS_SUCCEESS:
      return {
        ...state,
        ConfirmAdjustementItemResponse: action.payload,
        IsLoadingAdjustment: false
      };
    case OtherReceiptsActionTypes.CONFIRM_ADJUSTEMENT_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        IsLoadingAdjustment: false
      };
    case OtherReceiptsActionTypes.REMOVE_ADJUSTEMENT_ITEM:
      return {
        ...state,
        itemsInCarts: adjustmentAdaptor.removeOne(
          action.payload.id,
          state.itemsInCarts
        ),
        AdjustmentItemsInCartsSearch: adjustmentAdaptor.removeOne(
          action.payload.id,
          state.AdjustmentItemsInCartsSearch
        ),
        hasSearchItemInCartSearchAdjustment: false
      };
    case OtherReceiptsActionTypes.REMOVE_MULTIPLE_ADJUSTEMENT_ITEMs:
      return {
        ...state,
        itemsInCarts: adjustmentAdaptor.removeMany(
          action.payload,
          state.itemsInCarts
        ),
        AdjustmentItemsInCartsSearch: adjustmentAdaptor.removeMany(
          action.payload,
          state.AdjustmentItemsInCartsSearch
        ),
        hasSearchItemInCartSearchAdjustment: false
      };
    case OtherReceiptsActionTypes.UPDATE_ADJUSTEMENT_ITEMS:
      return {
        ...state,
        itemsInCarts: adjustmentAdaptor.updateOne(
          {
            id: action.payload.itemId,
            changes: {
              itemCode: action.payload.items.itemCode,
              binCode: action.payload.items.binCode,
              binGroupCode: action.payload.items.binGroupCode,
              measuredWeight: action.payload.items.measuredWeight,
              measuredQuantity: action.payload.items.quantity
            }
          },
          state.itemsInCarts
        )
      };
    case OtherReceiptsActionTypes.SEARCH_CART_ITEM_ADJUSTMENT:
      return {
        ...state,
        AdjustmentItemsInCartsSearch: state.itemsInCarts,
        hasSearchItemInCartSearchAdjustment: true
      };
    case OtherReceiptsActionTypes.CLEAR_SEARCH_CART_ITEM_ADJUSTMENT:
      return {
        ...state,
        AdjustmentItemsInCartsSearch: adjustmentAdaptor.getInitialState(),
        hasSearchItemInCartSearchAdjustment: false
      };
    case OtherReceiptsActionTypes.RESET_ADJUSTMENT_DATA:
      return {
        ...state,
        itemsInCarts: adjustmentAdaptor.removeAll(state.itemsInCarts),
        AdjustmentItemsInCartsSearch: adjustmentAdaptor.getInitialState(),
        ConfirmAdjustementItemResponse: null,
        error: null,
        isSearchingAdjustmentItems: false,
        hasSearchedAdjustmentItems: false,
        adjustmentSearchedItems: adjustmentAdaptor.getInitialState(),
        adjustmentSearchedItemsCount: null,
        hasSearchItemInCartSearchAdjustment: false
      };
    case OtherReceiptsActionTypes.CLEAR_SEARCH_INVENTORY_ADJUSTMENT:
      return {
        ...state,
        isSearchingAdjustmentItems: false,
        hasSearchedAdjustmentItems: false,
        adjustmentSearchedItems: adjustmentAdaptor.getInitialState(),
        adjustmentSearchedItemsCount: null
      };
    case OtherReceiptsActionTypes.LOAD_RECEIPTS_ADJ_LIST:
      return {
        ...state,
        isLoadingOtherReceiptADJList: true,
        error: null
      };

    case OtherReceiptsActionTypes.LOAD_RECEIPTS_ADJ_LIST_SUCCESS:
      return {
        ...state,
        otherReceiptADJList: OtherReceiptAdapter.addMany(
          action.payload.receiptsData,
          state.otherReceiptADJList
        ),
        isLoadingOtherReceiptADJList: false,
        totalElementsOtherReceipts: action.payload.totalElements,
        error: null
      };

    case OtherReceiptsActionTypes.LOAD_RECEIPTS_ADJ_LIST_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingOtherReceiptADJList: false
      };

    case OtherReceiptsActionTypes.PSV_SEARCH:
      return {
        ...state,
        isSearchingPSVItems: true,
        hasSearchedItemPSV: false,
        hasError: null
      };
    case OtherReceiptsActionTypes.PSV_SEARCH_SUCCESS:
      return {
        ...state,
        isSearchingPSVItems: false,
        psvSearchedItems: adjustmentAdaptor.addAll(
          action.payload.items,
          state.psvSearchedItems
        ),
        psvSearchedItemsCount: action.payload.count,
        hasSearchedItemPSV: true
      };

    case OtherReceiptsActionTypes.PSV_SEARCH_FAILUREE:
      return {
        ...state,
        isSearchingPSVItems: false,
        error: action.payload,
        hasSearchedItemPSV: false
      };
    case OtherReceiptsActionTypes.PSV_ADD_ITEMS_TO_CART:
      return {
        ...state,
        psvSearchedItems: adjustmentAdaptor.removeAll(state.psvSearchedItems),
        itemsInCartsPSV: adjustmentAdaptor.addMany(
          action.payload,
          state.itemsInCartsPSV
        )
      };
    case OtherReceiptsActionTypes.CONFIRM_PSV_ITEMS:
      return {
        ...state,
        IsLoadingPSV: true
      };
    case OtherReceiptsActionTypes.CONFIRM_PSV_ITEMS_SUCCEESS:
      return {
        ...state,
        ConfirmPSVItemResponse: action.payload,
        IsLoadingPSV: false
      };
    case OtherReceiptsActionTypes.CONFIRM_PSV_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        IsLoadingPSV: false
      };
    case OtherReceiptsActionTypes.REMOVE_PSV_ITEM:
      return {
        ...state,
        itemsInCartsPSV: adjustmentAdaptor.removeOne(
          action.payload.id,
          state.itemsInCartsPSV
        ),
        PSVItemsInCartsSearch: adjustmentAdaptor.removeOne(
          action.payload.id,
          state.PSVItemsInCartsSearch
        )
      };
    case OtherReceiptsActionTypes.REMOVE_MULTIPLE_PSV_ITEMs:
      return {
        ...state,
        itemsInCartsPSV: adjustmentAdaptor.removeMany(
          action.payload,
          state.itemsInCartsPSV
        ),
        PSVItemsInCartsSearch: adjustmentAdaptor.removeMany(
          action.payload,
          state.PSVItemsInCartsSearch
        ),
        hasSearchItemInCartSearchPSV: false
      };
    case OtherReceiptsActionTypes.UPDATE_PSV_ITEMS:
      return {
        ...state,
        itemsInCartsPSV: adjustmentAdaptor.updateOne(
          {
            id: action.payload.itemId,
            changes: {
              itemCode: action.payload.items.itemCode,
              binCode: action.payload.items.binCode,
              binGroupCode: action.payload.items.binGroupCode,
              measuredWeight: action.payload.items.measuredWeight,
              measuredQuantity: action.payload.items.quantity
            }
          },
          state.itemsInCartsPSV
        )
      };
    case OtherReceiptsActionTypes.SEARCH_CART_ITEM_PSV:
      return {
        ...state,
        PSVItemsInCartsSearch: state.itemsInCartsPSV,
        hasSearchItemInCartSearchPSV: true
      };
    case OtherReceiptsActionTypes.CLEAR_SEARCH_CART_ITEM_PSV:
      return {
        ...state,
        PSVItemsInCartsSearch: adjustmentAdaptor.getInitialState(),
        hasSearchItemInCartSearchPSV: false
      };
    case OtherReceiptsActionTypes.RESET_PSV_DATA:
      return {
        ...state,
        itemsInCartsPSV: adjustmentAdaptor.removeAll(state.itemsInCartsPSV),
        PSVItemsInCartsSearch: adjustmentAdaptor.getInitialState(),
        ConfirmPSVItemResponse: null,
        error: null,
        isSearchingPSVItems: false,
        hasSearchedItemPSV: false,
        psvSearchedItems: adjustmentAdaptor.getInitialState(),
        psvSearchedItemsCount: null,
        hasSearchItemInCartSearchPSV: false
      };
    case OtherReceiptsActionTypes.CLEAR_SEARCH_INVENTORY_PSV:
      return {
        ...state,
        isSearchingPSVItems: false,
        hasSearchedItemPSV: false,
        psvSearchedItems: adjustmentAdaptor.getInitialState(),
        psvSearchedItemsCount: null
      };

    /* case OtherReceiptsActionTypes.LOAD_PRODUCT_CATEGORIES:
      return { ...state,
               isLoading: true,
               error: null }; */

    case OtherReceiptsActionTypes.LOAD_PRODUCT_CATEGORIES_SUCCESS:
      return {
        ...state,
        productCategories: action.payload,
        isLoading: false,
        error: null
      };
    /* case OtherReceiptsActionTypes.LOAD_PRODUCT_CATEGORIES_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoading: false
      }; */

    case OtherReceiptsActionTypes.LOAD_PROUDCT_GROUPS:
    case OtherReceiptsActionTypes.LOAD_PRODUCT_CATEGORIES:
      return {
        ...state,
        isLoading: true,
        error: null
      };
    case OtherReceiptsActionTypes.LOAD_PROUDCT_GROUPS_SUCCESS:
      return {
        ...state,
        productGroups: action.payload,
        isLoading: false,
        error: null
      };
    case OtherReceiptsActionTypes.LOAD_PROUDCT_GROUPS_FAILURE:
    case OtherReceiptsActionTypes.LOAD_PRODUCT_CATEGORIES_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoading: false
      };

    case OtherReceiptsActionTypes.SET_FILTER_DATA_NON_VERIFIED_PRODUCTS:
      return {
        ...state,
        filterDataNonVerifiedProducts: action.payload
      };
    case OtherReceiptsActionTypes.SET_FILTER_DATA_VERIFIED_PRODUCTS:
      return {
        ...state,
        filterDataVerifiedProducts: action.payload
      };
    case OtherReceiptsActionTypes.SET_SORT_DATA_NON_VERIFIED_PRODUCTS:
      return {
        ...state,
        sortDataNonVerifiedProducts: action.payload
      };
    case OtherReceiptsActionTypes.SET_SORT_DATA_VERIFIED_PRODUCTS:
      return {
        ...state,
        sortDataVerifiedProducts: action.payload
      };
  }

  return state;
}
