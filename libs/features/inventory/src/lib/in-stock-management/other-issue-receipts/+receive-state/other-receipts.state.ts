import {
  OtherReceiptEntity,
  AdjustmentEntity,
  ItemEntity
} from './other-receipts.entity';

import {
  OtherReceiptsModel,
  AdjustmentItem
} from '../models/other-receipt.model';

import {
  Errors,
  CustomErrors,
  BinCode,
  Lov,
  ProductGroup,
  ProductCategory
} from '@poss-web/core';

import { TransferType } from '../../../in-stock-management/in-stock/models/in-stock.model';
import { Filter, Column } from '@poss-web/shared';

export interface OtherReceiptState {
  pendingOtherReceiptsSTNCount: number;

  hasError?: string;
  isLoading: boolean;

  isSearchingStocks: boolean;
  hasSearchStockResults: boolean;
  searchStockResults: OtherReceiptEntity;
  otherReceiptList: OtherReceiptEntity;

  otherReceiptLoanList: OtherReceiptEntity;

  isLoadingOtherReceiptList: boolean;
  isLoadingOtherReceiptLoanList: boolean;

  nonVerifiedItems: ItemEntity;
  isNonVerifiedItemsLoading: boolean;
  nonVerifiedItemsTotalCount: number;
  itemsCountNonVerified: number;
  itemsCountVerified: number;
  isNonVerifiedItemsLoaded: boolean;
  isVerifiedItemsLoaded: boolean;
  isVerifyingAllItem: boolean;
  verifiedItems: ItemEntity;
  isVerifiedItemsLoading: boolean;
  verifiedItemsTotalCount: number;

  isItemsTotalCountLoaded: boolean;
  isItemsTotalCountLoading: boolean;

  isSearchingItems: boolean;
  hasSearchedItems: boolean;

  binCodes: BinCode[];
  remarks: Lov[];
  tolerance: number;

  selectedStock: OtherReceiptsModel;

  isVerifyingAllItemSuccess: boolean;
  confirmedStock: any;

  confirmStockReceiveErrors: Errors;
  selectedStockLoadError: Errors;
  isAssigningBinToAllItemsSuccess: boolean;
  isAssigningBinToAllItems: boolean;

  isConfirmStockReceiveSuccess: boolean;
  isConfirmingStockReceive: boolean;
  verifyItemSuccess: boolean;
  OtherReceiptsDropdownValues: TransferType[];

  error: CustomErrors;
  selectedDropDownForReceipts: string;
  isItemIssued: boolean;
  totalElementsOtherReceipts: number;
  isLoadingBinGroups: boolean;
  isLoadingRemarks: boolean;
  isLoadingTolerance: boolean;
  isLoadingSelectedStock: boolean;
  updateItemSuccess: boolean;

  isSearchingAdjustmentItems?: boolean;
  adjustmentSearchedItems: AdjustmentEntity;
  hasSearchedAdjustmentItems?: boolean;
  IsLoadingAdjustment: boolean;
  itemsInCarts: AdjustmentEntity;
  adjustmentSearchedItemsCount: number;
  ConfirmAdjustementItemResponse: AdjustmentItem;

  AdjustmentItemsInCartsSearch: AdjustmentEntity;
  hasSearchItemInCartSearchAdjustment: boolean;
  otherReceiptADJList: OtherReceiptEntity;
  isLoadingOtherReceiptADJList: boolean;

  isSearchingPSVItems?: boolean;
  hasSearchedItemPSV?: boolean;
  psvSearchedItems: AdjustmentEntity;
  itemsInCartsPSV: AdjustmentEntity;
  psvSearchedItemsCount: number;
  ConfirmPSVItemResponse: AdjustmentItem;
  IsLoadingPSV: boolean;
  PSVItemsInCartsSearch: AdjustmentEntity;
  hasSearchItemInCartSearchPSV: boolean;

  productCategories: ProductCategory[];
  productGroups: ProductGroup[];

  filterDataNonVerifiedProducts: { [key: string]: Filter[] };
  filterDataVerifiedProducts: { [key: string]: Filter[] };

  sortDataNonVerifiedProducts: Column[];
  sortDataVerifiedProducts: Column[];
}
