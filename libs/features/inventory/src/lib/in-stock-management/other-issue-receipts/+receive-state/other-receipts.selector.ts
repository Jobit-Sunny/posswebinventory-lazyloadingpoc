import { selectInventory } from '../../../inventory.state';
import { createSelector } from '@ngrx/store';
import {
  otheReceiptSelector,
  itemSelector,
  adjustementSelector
} from './other-receipts.entity';

const selectHasError = createSelector(
  selectInventory,
  state => state.otherReceipt.hasError
);

const selectIsLoading = createSelector(
  selectInventory,
  state => state.otherReceipt.isLoading
);

const selectOtherReceiptsSTNCount = createSelector(
  selectInventory,
  state => state.otherReceipt.pendingOtherReceiptsSTNCount
);

const PendingReceiptList = createSelector(
  selectInventory,
  state => state.otherReceipt.otherReceiptList
);

const selectPendingReceiptList = createSelector(
  PendingReceiptList,
  otheReceiptSelector.selectAll
);
const PendingReceiptLoanList = createSelector(
  selectInventory,
  state => state.otherReceipt.otherReceiptLoanList
);

const selectPendingReceiptLoanList = createSelector(
  PendingReceiptLoanList,
  otheReceiptSelector.selectAll
);

const selectIsLoadingOtherReceiptsSTN = createSelector(
  selectInventory,
  state => state.otherReceipt.isLoadingOtherReceiptList
);

const selectIsSearchingStocks = createSelector(
  selectInventory,
  state => state.otherReceipt.isSearchingStocks
);

const selectHasSearchStockResults = createSelector(
  selectInventory,
  state => state.otherReceipt.hasSearchStockResults
);

const searchStockResults = createSelector(
  selectInventory,
  state => state.otherReceipt.searchStockResults
);

const selectSearchStockResults = createSelector(
  searchStockResults,
  otheReceiptSelector.selectAll
);

const nonVerifiedItems = createSelector(
  selectInventory,
  state => state.otherReceipt.nonVerifiedItems
);

const selectNonVerifiedItems = createSelector(
  nonVerifiedItems,
  itemSelector.selectAll
);

const verifiedItems = createSelector(
  selectInventory,
  state => state.otherReceipt.verifiedItems
);

const selectVerifiedItems = createSelector(
  verifiedItems,
  itemSelector.selectAll
);

const selectIsNonVerifiedItemsLoading = createSelector(
  selectInventory,
  state => state.otherReceipt.isNonVerifiedItemsLoading
);

const selectIsVerifiedItemsLoading = createSelector(
  selectInventory,
  state => state.otherReceipt.isVerifiedItemsLoading
);

const selectisItemsTotalCountLoaded = createSelector(
  selectInventory,
  state => state.otherReceipt.isItemsTotalCountLoaded
);

const selectIsItemsTotalCountLoading = createSelector(
  selectInventory,
  state => state.otherReceipt.isItemsTotalCountLoading
);

const selectIsSearchingItems = createSelector(
  selectInventory,
  state => state.otherReceipt.isSearchingItems
);

const selectHasSearchedItems = createSelector(
  selectInventory,
  state => state.otherReceipt.hasSearchedItems
);

const selectBinCodes = createSelector(
  selectInventory,
  state => state.otherReceipt.binCodes
);
const selectRemarks = createSelector(
  selectInventory,
  state => state.otherReceipt.remarks
);

const selectTolerance = createSelector(
  selectInventory,
  state => state.otherReceipt.tolerance
);

const selectSelectedStock = createSelector(
  selectInventory,
  state => state.otherReceipt.selectedStock
);

const selectNonVerifiedItemsTotalCount = createSelector(
  selectInventory,
  state => state.otherReceipt.nonVerifiedItemsTotalCount
);

const selectVerifiedItemsTotalCount = createSelector(
  selectInventory,
  state => state.otherReceipt.verifiedItemsTotalCount
);

const selectIsVerifyingAllItemSuccess = createSelector(
  selectInventory,
  state => state.otherReceipt.isVerifyingAllItemSuccess
);

const selectIsAssigningBinToAllItemsSuccess = createSelector(
  selectInventory,
  state => state.otherReceipt.isAssigningBinToAllItemsSuccess
);

const selectConfirmedStock = createSelector(
  selectInventory,
  state => state.otherReceipt.confirmedStock
);

const selectConfirmStockReceiveErrors = createSelector(
  selectInventory,
  state => state.otherReceipt.confirmStockReceiveErrors
);

const selectSelectedStockLoadError = createSelector(
  selectInventory,
  state => state.otherReceipt.selectedStockLoadError
);

const selectIsAssigningBinToAllItems = createSelector(
  selectInventory,
  state => state.otherReceipt.isAssigningBinToAllItems
);
const selectOtherReceiptsDropDown = createSelector(
  selectInventory,
  state => state.otherReceipt.OtherReceiptsDropdownValues
);

const selectOtherReceiptsSelectedDropDown = createSelector(
  selectInventory,
  state => state.otherReceipt.selectedDropDownForReceipts
);

const selectTotalReceiptsCount = createSelector(
  selectInventory,
  state => state.otherReceipt.totalElementsOtherReceipts
);

const selectError = createSelector(
  selectInventory,
  state => state.otherReceipt.error
);

const selectIsLoadingBinGroups = createSelector(
  selectInventory,
  state => state.otherReceipt.isLoadingBinGroups
);

const selectIsLoadingRemarks = createSelector(
  selectInventory,
  state => state.otherReceipt.isLoadingRemarks
);

const selectIsLoadingTolerance = createSelector(
  selectInventory,
  state => state.otherReceipt.isLoadingTolerance
);

const selectIsLoadingSelectedStock = createSelector(
  selectInventory,
  state => state.otherReceipt.isLoadingSelectedStock
);

const selectUpdateItemSuccess = createSelector(
  selectInventory,
  state => state.otherReceipt.updateItemSuccess
);
const AdjustmentSearchedItems = createSelector(
  selectInventory,
  state => state.otherReceipt.adjustmentSearchedItems
);

const selectAdjustmentSearchedItems = createSelector(
  AdjustmentSearchedItems,
  adjustementSelector.selectAll
);
const itemsInCarts = createSelector(
  selectInventory,
  state => state.otherReceipt.itemsInCarts
);
const selectItemsInCart = createSelector(
  itemsInCarts,
  adjustementSelector.selectAll
);
const selectCartItemIds = createSelector(
  selectInventory,
  state => state.otherReceipt.itemsInCarts.ids
);
const selectAdjustmentItemsSearchCount = createSelector(
  selectInventory,
  state => state.otherReceipt.adjustmentSearchedItemsCount
);
const selectConfirmAdjustementItemsResponse = createSelector(
  selectInventory,
  state => state.otherReceipt.ConfirmAdjustementItemResponse
);
const AdjustmentItemsInCartsSearch = createSelector(
  selectInventory,
  state => state.otherReceipt.AdjustmentItemsInCartsSearch
);

const selectAdjustmentItemsInCartsSearch = createSelector(
  AdjustmentItemsInCartsSearch,
  adjustementSelector.selectAll
);
const PendingReceiptADJList = createSelector(
  selectInventory,
  state => state.otherReceipt.otherReceiptADJList
);

const selectPendingReceiptADJList = createSelector(
  PendingReceiptADJList,
  otheReceiptSelector.selectAll
);
const selectIsSearchingAdjustment = createSelector(
  selectInventory,
  state => state.otherReceipt.isSearchingAdjustmentItems
);
const selectHasSearchedItemAdjustment = createSelector(
  selectInventory,
  state => state.otherReceipt.hasSearchedAdjustmentItems
);
const selectIsLoadingAdjustment = createSelector(
  selectInventory,
  state => state.otherReceipt.IsLoadingAdjustment
);
const selectHasSearchItemInCartSearch = createSelector(
  selectInventory,
  state => state.otherReceipt.hasSearchItemInCartSearchAdjustment
);
//psv
const PSVSearchedItems = createSelector(
  selectInventory,
  state => state.otherReceipt.psvSearchedItems
);

const selectPSVSearchedItems = createSelector(
  PSVSearchedItems,
  adjustementSelector.selectAll
);
const itemsInCartsPSV = createSelector(
  selectInventory,
  state => state.otherReceipt.itemsInCartsPSV
);
const selectItemsInCartPSV = createSelector(
  itemsInCartsPSV,
  adjustementSelector.selectAll
);

const selectPSVItemsSearchCount = createSelector(
  selectInventory,
  state => state.otherReceipt.psvSearchedItemsCount
);
const selectConfirmPSVItemsResponse = createSelector(
  selectInventory,
  state => state.otherReceipt.ConfirmPSVItemResponse
);
const PSVItemsInCartsSearch = createSelector(
  selectInventory,
  state => state.otherReceipt.PSVItemsInCartsSearch
);

const selectPSVItemsInCartsSearch = createSelector(
  PSVItemsInCartsSearch,
  adjustementSelector.selectAll
);

const selectIsSearchingPSV = createSelector(
  selectInventory,
  state => state.otherReceipt.isSearchingPSVItems
);
const selectHasSearchedItemPSV = createSelector(
  selectInventory,
  state => state.otherReceipt.hasSearchedItemPSV
);
const selectIsLoadingPSV = createSelector(
  selectInventory,
  state => state.otherReceipt.IsLoadingPSV
);
const selectHasSearchItemInCartSearchPSV = createSelector(
  selectInventory,
  state => state.otherReceipt.hasSearchItemInCartSearchPSV
);
const selectProductCategories = createSelector(
  selectInventory,
  state => state.otherReceipt.productCategories
);

const selectProductGroups = createSelector(
  selectInventory,
  state => state.otherReceipt.productGroups
);

const selectfilterDataNonVerifiedProducts = createSelector(
  selectInventory,
  state => state.otherReceipt.filterDataNonVerifiedProducts
);

const selectfilterDataVerifiedProducts = createSelector(
  selectInventory,
  state => state.otherReceipt.filterDataVerifiedProducts
);
const selectSortNonVerifiedProducts = createSelector(
  selectInventory,
  state => state.otherReceipt.sortDataNonVerifiedProducts
);

const selectSortDataVerifiedProducts = createSelector(
  selectInventory,
  state => state.otherReceipt.sortDataVerifiedProducts
);
const selectItemsCountNonVerified = createSelector(
  selectInventory,
  state => state.otherReceipt.itemsCountNonVerified
);
const selectItemsCountVerified = createSelector(
  selectInventory,
  state => state.otherReceipt.itemsCountVerified
);
const selectisNonVerifiedItemsLoaded = createSelector(
  selectInventory,
  state => state.otherReceipt.isNonVerifiedItemsLoaded
);
const selectisVerifiedItemsLoaded = createSelector(
  selectInventory,
  state => state.otherReceipt.isVerifiedItemsLoaded
);
const selectVerifyItemSuccess = createSelector(
  selectInventory,
  state => state.otherReceipt.verifyItemSuccess
);
export const OtherReceiptsSelector = {
  selectHasError,
  selectIsLoading,
  selectOtherReceiptsSTNCount,
  selectPendingReceiptList,
  selectIsLoadingOtherReceiptsSTN,
  selectIsSearchingStocks,
  selectHasSearchStockResults,
  selectSearchStockResults,
  selectNonVerifiedItems,
  selectVerifiedItems,
  selectIsNonVerifiedItemsLoading,
  selectIsVerifiedItemsLoading,
  selectisItemsTotalCountLoaded,
  selectIsItemsTotalCountLoading,
  selectIsSearchingItems,
  selectHasSearchedItems,
  selectBinCodes,
  selectRemarks,
  selectTolerance,
  selectSelectedStock,
  selectNonVerifiedItemsTotalCount,
  selectVerifiedItemsTotalCount,
  selectIsVerifyingAllItemSuccess,
  selectIsAssigningBinToAllItemsSuccess,
  selectConfirmedStock,
  selectConfirmStockReceiveErrors,
  selectSelectedStockLoadError,
  selectIsAssigningBinToAllItems,
  selectOtherReceiptsDropDown,
  selectPendingReceiptLoanList,
  selectOtherReceiptsSelectedDropDown,
  selectTotalReceiptsCount,
  selectError,
  selectIsLoadingBinGroups,
  selectIsLoadingRemarks,
  selectIsLoadingTolerance,
  selectIsLoadingSelectedStock,
  selectUpdateItemSuccess,
  selectAdjustmentSearchedItems,
  selectItemsInCart,
  selectAdjustmentItemsSearchCount,
  selectCartItemIds,
  selectConfirmAdjustementItemsResponse,
  selectAdjustmentItemsInCartsSearch,
  selectPendingReceiptADJList,
  selectHasSearchItemInCartSearch,
  selectIsSearchingAdjustment,
  selectHasSearchedItemAdjustment,
  selectIsLoadingAdjustment,
  //psv
  selectPSVSearchedItems,
  selectItemsInCartPSV,
  selectPSVItemsSearchCount,
  selectConfirmPSVItemsResponse,
  PSVItemsInCartsSearch,
  selectPSVItemsInCartsSearch,
  selectIsSearchingPSV,
  selectHasSearchedItemPSV,
  selectIsLoadingPSV,
  selectHasSearchItemInCartSearchPSV,

  selectProductCategories,
  selectProductGroups,
  selectfilterDataNonVerifiedProducts,
  selectfilterDataVerifiedProducts,
  selectSortNonVerifiedProducts,
  selectSortDataVerifiedProducts,
  selectItemsCountNonVerified,
  selectItemsCountVerified,
  selectisNonVerifiedItemsLoaded,
  selectisVerifiedItemsLoaded,
  selectVerifyItemSuccess
};
