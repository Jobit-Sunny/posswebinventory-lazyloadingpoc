import {
  LoadOtherReceiptsSTNCountPayload,
  LoadDropDownPayload,
  AdjustmentSearchSuccessPayload
} from '../+receive-state/other-receipts.actions';
import { OtherReceiptsIssuesEnum } from '../../../in-stock-management/in-stock/in-stock.enum';
import {
  OtherReceiptsModel,
  AdjustmentItem,
  OtherReceiptItem
} from '../models/other-receipt.model';
import * as moment from 'moment';

export class OtherReceiptsAdaptor {
  static receiptSTNCountFromJson(data: any): LoadOtherReceiptsSTNCountPayload {
    let pendingOtherReceiptsSTNCount = 0;
    const countData = [];
    for (const stnCount of data.results) {
      if (
        stnCount.type === OtherReceiptsIssuesEnum.EXHIBITION_TYPE ||
        stnCount.type === OtherReceiptsIssuesEnum.LOAN
      ) {
        pendingOtherReceiptsSTNCount += stnCount.count;
        countData.push(stnCount);
      }
    }
    return {
      pendingOtherReceiptsSTNCount,
      countData
    };
  }

  static OtherRecieptsDropdownfromJson(data: any): LoadDropDownPayload {
    const dropDownValue = data.results;
    return {
      dropDownValue
    };
  }
  static OtherReceiptsDatafromJson(data: any): OtherReceiptsModel {
    // Thow error if not found
    if (!data) {
      // Throw
      return null;
    }

    const stockTransferNote: OtherReceiptsModel = {
      id: data.id,
      srcDocNo: data.srcDocNo,
      transactionType: data.transactionType,
      carrierDetails: {
        type: data.carrierDetails.type,
        data: data.carrierDetails.data
      },
      srcDocDate: moment(data.srcDocDate),
      currencyCode: data.currencyCode,
      weightUnit: data.weightUnit,
      status: data.status,
      srcFiscalYear: data.srcFiscalYear,
      destDocDate: moment(data.destDocDate),
      destDocNo: data.destDocNo,
      totalAvailableQuantity: data.totalAvailableQuantity,
      totalAvailableValue: data.totalAvailableValue,
      totalAvailableWeight: data.totalAvailableWeight,
      totalMeasuredQuantity: data.totalMeasuredQuantity,
      totalMeasuredValue: data.totalMeasuredValue,
      totalMeasuredWeight: data.totalMeasuredWeight,
      locationCode: data.totalMeasuredWeight
    };
    return stockTransferNote;
  }
  static AdjustmentSearchedItems(data: any): AdjustmentSearchSuccessPayload {
    let adjustmentSearchSuccess: AdjustmentSearchSuccessPayload;
    const items: AdjustmentItem[] = [];
    let count: number = null;
    count = data.totalElements;
    for (const item of data.results) {
      items.push({
        id: item.id,
        currencyCode: item.currencyCode,
        binCode: item.binCode,
        binGroupCode: item.binGroupCode,
        imageURL: item.imageURL,
        itemCode: item.itemCode,
        itemValue: item.itemValue,
        lotNumber: item.lotNumber,
        measuredQuantity: item.availableQuantity,
        measuredWeight: item.availableWeight,
        mfgDate: moment(item.mfgDate),
        orderType: item.orderType,
        productCategory: item.productCategoryDesc,
        productCategoryId: item.productCategory,
        productGroup: item.productGroupDesc,
        productGroupId: item.productGroup,
        status: item.status,
        totalQuantity: item.totalQuantity,
        totalValue: item.totalValue,
        totalWeight: item.totalWeight,
        weightUnit: item.weightUnit,
        itemWeight: item.itemWeight,
        availableQuantity: item.availableQuantity,
        availableWeight: item.availableWeight,
        stdWeight: item.stdWeight,
        stdValue: item.stdValue,
        availableValue: item.availableValue
      });
    }
    adjustmentSearchSuccess = { items: items, count: count };

    return adjustmentSearchSuccess;
  }
  static AdjustementConfirm(item: any): AdjustmentItem {
    const confirmAdjustementItem: AdjustmentItem = {
      id: item.id,
      currencyCode: item.currencyCode,
      binCode: item.binCode,
      binGroupCode: item.binGroupCode,
      imageURL: item.imageURL,
      itemCode: item.itemCode,
      itemValue: item.itemValue,
      lotNumber: item.lotNumber,
      measuredQuantity: item.measuredQuantity,
      measuredWeight: item.measuredWeight,
      mfgDate: moment(item.mfgDate),
      orderType: item.orderType,
      productCategory: item.productCategoryDesc,
      productCategoryId: item.productCategory,
      productGroup: item.productGroupDesc,
      productGroupId: item.productGroup,
      status: item.status,
      totalQuantity: item.totalQuantity,
      totalValue: item.totalValue,
      totalWeight: item.totalWeight,
      weightUnit: item.weightUnit,
      itemWeight: item.itemWeight,
      availableQuantity: item.availableQuantity,
      destDocNo: item.destDocNo,
      isSelected: false,
      availableWeight: item.availableWeight,
      stdWeight: item.stdWeight,
      stdValue: item.stdValue,
      availableValue: item.availableValue
    };

    return confirmAdjustementItem;
  }
  static OtherReceiptItemfromJson(item: any): OtherReceiptItem {
    return {
      id: item.id,
      binCode: item.binCode,
      itemCode: item.itemCode,
      itemDetails: item.itemDetails,
      itemValue: item.itemValue,
      lotNumber: item.lotNumber,
      mfgDate: moment(item.mfgDate),
      status: item.status,
      totalValue: item.totalValue,
      totalWeight: item.totalWeight,
      currencyCode: item.currencyCode,
      weightUnit: item.weightUnit,
      imageURL: item.imageURL,
      measuredQuantity: item.measuredQuantity,
      measuredWeight: item.measuredWeight,
      binGroupCode: item.binGroupCode,
      totalQuantity: item.totalQuantity,
      orderType: item.orderType,
      productCategory: item.productCategory,
      productGroup: item.productGroup,
      remarks: item.remarks,
      isUpdating: false,
      isUpdatingSuccess: null,
      stdValue: item.stdValue,
      stdWeight: item.stdWeight,
      availableQuantity: item.availableQuantity,
      availableWeight: item.availableWeight,
      availableValue: item.availableValue,
      measuredValue: item.measuredValue
    };
  }
}
