import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import {
  LoadOtherIssuesSTNCountPayload,
  LoadOtherIssueCreateItemsTotalCountSuccessPayload,
  AdjustmentSearchItemPayloadSuccess
} from '../+issue-state/other-issues.actions';
import {
  getOtherIssueSTNCountEndpointUrl,
  getOtherStockIssueByPaginationEndpointUrl,
  getOtherStockIssueBySrcDocNoEndpointUrl,
  getOtherIssueItemsByPaginationEndpointUrl,
  searchOtherIssueDetailsItemsByPaginationEndpointUrl,
  getcreateOtherIssuesStockRequestEndpointUrl,
  getOtherIssueCreateItemsByPaginationEndpointUrl,
  searchOtherIssueCreateItemsByPaginationEndpointUrl,
  getCreateOtherIssueStockRequestItemsUrl,
  getupdateStockRequestCreateItemUrl,
  getupdateStockRequestUrl,
  getConfirmOtherIssueUrl,
  getCreateOtherStockIssueItemsUrl,
  getSearchAdjustmentEndpointUrl,
  getcreateOtherIssuesAdjustmentRequestEndpointUrl,
  getOtherReciveCountEndpointUrl,
  getOtherReceiveByPaginationEndpointUrl,
  getStockReceiveBySrcDocNoEndpointUrl,
  getOtherReceiveItemsByPaginationEndpointUrl,
  getOtherReceiptsSortItemsByPaginationEndpointUrl,
  getAdjustmentConfirmUrl,
  getotherReceiveStockByIdEndpointUrl,
  getSearchOtherReceiptItemsByItemCodeUrl,
  getOtherReceiveVerifyItemEndpointUrl,
  getUpdateAllOtherReceiptItemsEndpointUrl,
  getConfirmOtherReceiptSTNEndpointUrl,
  getCancelStockRequestUrl,
  getOtherReceiveBySrcDocNoEndpointUrl,
  getPrintOtherIssuesEndpointUrl,
  getProductCategoriesUrl,
  getProductGroupsUrl
} from '../../../endpoints.constants';
import { OtherIssuesAdaptor } from '../adaptors/other-issues.adaptor';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { ApiService } from '@poss-web/core';
import { HttpClient } from '@angular/common/http';
import {
  OtherIssuedataModel,
  OtherIssueModel,
  RequestOtherIssueStockTransferNote,
  OtherIssuesItem,
  OtherIssuesCreateStockResponse
} from '../models/other-issues.model';
import { OtherIssuesDataHelper } from '../helpers/other-issues-helper';
import {
  LoadOtherReceiptsSTNCountPayload,
  LoadDropDownPayload,
  LoadItemsTotalCountSuccessPayload,
  AdjustmentSearchPayload,
  ConfirmAdjustmentItemsPayload,
  LoadItemsTotalCountPayloadInfo
} from '../+receive-state/other-receipts.actions';
import { OtherReceiptsAdaptor } from '../adaptors/other-receipts.adaptors';
import {
  OtherReceiptsDataModel,
  OtherReceiptsModel,
  OtherReceiptItem,
  ItemUpdate,
  ConfirmOtherReceive
} from '../models/other-receipt.model';
import { OtherReceiveItemHelper } from '../helpers/other-receive-item.helper';
import { responseTypeEnum } from '../models/other-receipt.enum';
import { OtherReceiptsIssuesEnum } from '../../in-stock/in-stock.enum';

const tolerance = 0.03;

@Injectable()
export class OtherReceiptIssueService {
  constructor(private apiService: ApiService, private http: HttpClient) {}

  getOtherIssuesSTNCount(): Observable<LoadOtherIssuesSTNCountPayload> {
    const STNCountUrl = getOtherIssueSTNCountEndpointUrl();
    return this.apiService
      .get(STNCountUrl)
      .pipe(
        map((data: any) => OtherIssuesAdaptor.issuesSTNCountFromJson(data))
      );
  }

  getIssueList(
    type: string,
    pageIndex: number,
    pageSize: number
  ): Observable<OtherIssuedataModel> {
    const url = getOtherStockIssueByPaginationEndpointUrl(
      type,
      pageIndex,
      pageSize
    );
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) => OtherIssuesDataHelper.getOtherisssuesData(data))
      );
  }
  searchIssueStocks(
    reqDocNo: number,
    type: string
  ): Observable<OtherIssueModel[]> {
    const url = getOtherStockIssueBySrcDocNoEndpointUrl(reqDocNo, type);
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) =>
          OtherIssuesDataHelper.getOtherisssuesSearchData(data.results)
        )
      );
  }

  getOtherStockIssue(
    reqDocNo: number,
    requestType: string
  ): Observable<RequestOtherIssueStockTransferNote> {
    const url = getOtherStockIssueBySrcDocNoEndpointUrl(reqDocNo, requestType);
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) =>
          OtherIssuesAdaptor.requestStockTransferNoteFromJson(data)
        )
      );
  }

  getOtherIssuesItems(
    id: number,
    status: string,
    pageIndex: number,
    pageSize: number,
    type: string,
    itemCode?: string,
    lotNumber?: string,
    sort?: Map<string, string>,
    filter?: { key: string; value: any[] }[]
  ): Observable<OtherIssuesItem[]> {
    const url = getOtherIssueItemsByPaginationEndpointUrl(
      id,
      status,
      pageIndex,
      pageSize,
      type,
      itemCode,
      lotNumber,
      sort,
      filter
    );
    return this.apiService
      .get(url.path, url.params)
      .pipe(
        map((data: any) => OtherIssuesAdaptor.OtherIssueItemfromJson(data))
      );
  }

  searchOtherIssueDetailsItems(
    id: number,
    itemCode: string,
    status: string,
    transferType: string,
    lotNumber: string
  ): Observable<OtherIssuesItem[]> {
    const url = searchOtherIssueDetailsItemsByPaginationEndpointUrl(
      id,
      status,
      0,
      1,
      transferType,
      itemCode,
      lotNumber
    );
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) => OtherIssuesAdaptor.OtherIssueItemfromJson(data))
      );
  }

  createOtherIssuesStockRequest(
    reqtype: string
  ): Observable<OtherIssuesCreateStockResponse> {
    const url = getcreateOtherIssuesStockRequestEndpointUrl(reqtype);
    return this.apiService
      .post(url)
      .pipe(
        map((data: any) =>
          OtherIssuesAdaptor.createOtherIssueStockRequestFromJson(data)
        )
      );
  }

  getOtherIssueCreateItems(
    id: number,
    status: string,
    pageIndex: number,
    pageSize: number,
    reqtype: string,
    itemCode?: string,
    lotNumber?: string,
    sort?: Map<string, string>,
    filter?: { key: string; value: any[] }[]
  ): Observable<OtherIssuesItem[]> {
    const url = getOtherIssueCreateItemsByPaginationEndpointUrl(
      id,
      status,
      pageIndex,
      pageSize,
      reqtype,
      itemCode,
      lotNumber,
      sort,
      filter
    );
    return this.apiService
      .get(url.path, url.params)
      .pipe(
        map((data: any) => OtherIssuesAdaptor.OtherIssueItemfromJson(data))
      );
  }

  getOtherIssuesCreateItemsCount(
    type: string,
    id: number
  ): Observable<LoadOtherIssueCreateItemsTotalCountSuccessPayload> {
    const AllOtherIssuesCreateCountUrl = getOtherIssueCreateItemsByPaginationEndpointUrl(
      id,
      OtherReceiptsIssuesEnum.OPEN,
      0,
      1,
      type
    );
    const SelectedOtherIssuesCreateCountUrl = getOtherIssueCreateItemsByPaginationEndpointUrl(
      id,
      OtherReceiptsIssuesEnum.SELECTED,
      0,
      1,
      type
    );

    return this.apiService
      .get(
        AllOtherIssuesCreateCountUrl.path,
        AllOtherIssuesCreateCountUrl.params
      )
      .pipe(map((data: any) => data.totalElements))
      .pipe(
        mergeMap(nonVerifiedItemsTotalCount =>
          this.apiService
            .get(
              SelectedOtherIssuesCreateCountUrl.path,
              SelectedOtherIssuesCreateCountUrl.params
            )
            .pipe(
              map((data: any) => ({
                AllOtherIssueCreateItemsTotalCount: nonVerifiedItemsTotalCount,
                SelectedOtherIssueCreateItemsTotalCount: data.totalElements
              }))
            )
        )
      );
  }
  searchOtherIssueCreateItems(
    id: number,
    itemCode: string,
    status: string,
    reqtype: string,
    lotNumber: string
  ): Observable<OtherIssuesItem[]> {
    const url = searchOtherIssueCreateItemsByPaginationEndpointUrl(
      id,
      status,
      0,
      1,
      reqtype,
      itemCode,
      lotNumber
    );
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) => OtherIssuesAdaptor.OtherIssueItemfromJson(data))
      );
  }

  CreateOtherIssueStockRequestItems(
    id: number,
    issueItems: any,
    requestType: string
  ) {
    const CreateOtherIssueStockRequestItemsUrl = getCreateOtherIssueStockRequestItemsUrl(
      id,
      requestType
    );
    return this.apiService
      .post(CreateOtherIssueStockRequestItemsUrl, {
        stockItems: issueItems
      })
      .pipe(map(data => data));
  }
  removeOtherIssueStockRequestItems(
    id: number,
    itemIds: any,
    requestType: string
  ) {
    const removeOtherIssueStockRequestItemsUrl = getCreateOtherIssueStockRequestItemsUrl(
      id,
      requestType
    );
    return this.apiService
      .put(removeOtherIssueStockRequestItemsUrl, {
        itemIds: itemIds
      })
      .pipe(map(data => data));
  }
  updateStockRequestCreateItem(
    id: number,
    itemid: number,
    reqType: string,
    value: any
  ) {
    const updateStockRequestCreateItemUrl = getupdateStockRequestCreateItemUrl(
      id,
      itemid,
      reqType
    );
    return this.apiService
      .patch(updateStockRequestCreateItemUrl, value)
      .pipe(map(data => data));
  }

  updateStockRequest(
    id: number,
    reqType: string,
    carrierDetails: any,
    approvalDetails: any,
    remarks: string,
    status: string
  ) {
    const updateIssueUrl = getupdateStockRequestUrl(id, reqType);
    return this.apiService
      .patch(updateIssueUrl, {
        carrierDetails,
        approvalDetails,
        remarks,
        status
      })
      .pipe(map(data => data));
  }
  CreateOtherStockIssueItems(id: number, issueItems: any, requestType: string) {
    const CreateOtherStockIssueItemsUrl = getCreateOtherStockIssueItemsUrl(
      id,
      requestType
    );
    return this.apiService
      .patch(CreateOtherStockIssueItemsUrl, {
        itemIds: issueItems,
        status: OtherReceiptsIssuesEnum.SELECTED
      })
      .pipe(map(data => data));
  }
  //v2
  confirmOtherStockIssue(
    id: number,
    requestType: string,
    carrierDetails: any,
    remarks: string,
    destinationLocationCode: string
  ) {
    const confirmOtherStockIssueUrl = getConfirmOtherIssueUrl(id, requestType);
    return this.apiService
      .patch(confirmOtherStockIssueUrl, {
        carrierDetails,
        remarks,
        destinationLocationCode
      })
      .pipe(map(data => data));
  }
  searchAdjustmentItem(
    variantCode: string,
    lotNumber: string
  ): Observable<AdjustmentSearchItemPayloadSuccess> {
    const url = getSearchAdjustmentEndpointUrl(variantCode, lotNumber);
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) => OtherIssuesAdaptor.searchedAdjustmentItems(data))
      );
  }

  createStockRequestAdjustment(
    reqType: string,
    approvalDetails: any,
    items: any,
    remarks: string
  ) {
    const updateIssueUrl = getcreateOtherIssuesAdjustmentRequestEndpointUrl(
      reqType
    );
    return this.apiService
      .post(updateIssueUrl, {
        approvalDetails,
        items,
        remarks
      })
      .pipe(map(data => data));
  }

  // other receipts
  getOtherReceiptsSTNCount(): Observable<LoadOtherReceiptsSTNCountPayload> {
    const STNCountUrl = getOtherReciveCountEndpointUrl();
    return this.apiService
      .get(STNCountUrl)
      .pipe(
        map((data: any) => OtherReceiptsAdaptor.receiptSTNCountFromJson(data))
      );
  }
  getOtherReceiptsDropDown(): Observable<LoadDropDownPayload> {
    const STNCountUrl = getOtherReciveCountEndpointUrl();
    return this.apiService
      .get(STNCountUrl)
      .pipe(
        map((data: any) =>
          OtherReceiptsAdaptor.OtherRecieptsDropdownfromJson(data)
        )
      );
  }

  getReceiptsList(
    type: string,
    pageIndex: number,
    pageSize: number
  ): Observable<OtherReceiptsDataModel> {
    const url = getOtherReceiveByPaginationEndpointUrl(
      type,
      pageIndex,
      pageSize
    );
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) => OtherReceiveItemHelper.getOtherReceiptsData(data))
      );
  }
  searchRecieptsStocks(
    srcdocno: number,
    type: string
  ): Observable<OtherReceiptsModel[]> {
    const url = getOtherReceiveBySrcDocNoEndpointUrl(srcdocno, type);
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) =>
          OtherReceiveItemHelper.getStockTransferNotes(data.results)
        )
      );
  }
  getOtherReceiveItemsCount(
    transactionType: string,
    id: number
  ): Observable<LoadItemsTotalCountSuccessPayload> {
    const nonVerifiedCountUrl = getOtherReceiveItemsByPaginationEndpointUrl(
      id,
      'ISSUED',
      0,
      1,
      transactionType
    );
    const verifiedCountUrl = getOtherReceiveItemsByPaginationEndpointUrl(
      id,
      'VERIFIED',
      0,
      1,
      transactionType
    );
    return this.apiService
      .get(nonVerifiedCountUrl)
      .pipe(map((data: any) => data.totalElements))
      .pipe(
        mergeMap(nonVerifiedItemsTotalCount =>
          this.apiService.get(verifiedCountUrl).pipe(
            map((data: any) => ({
              nonVerifiedItemsTotalCount: nonVerifiedItemsTotalCount,
              verifiedItemsTotalCount: data.totalElements
            }))
          )
        )
      );
  }
  getTempSortItems(
    id: number,
    status: string,
    pageIndex: number,
    pageSize: number,
    sortBy: string,
    property: string,
    transactionType: string,
    itemCode?: string,
    lotNumber?: string,
    sort?: Map<string, string>,
    filter?: { key: string; value: any[] }[]
  ): Observable<{ items: OtherReceiptItem[]; count: number }> {
    const url = getOtherReceiptsSortItemsByPaginationEndpointUrl(
      id,
      status,
      pageIndex,
      pageSize,
      sortBy,
      property,
      transactionType,
      itemCode,
      lotNumber,
      sort,
      filter
    );
    return this.apiService
      .get(url)
      .pipe(map((data: any) => OtherReceiveItemHelper.getItems(data)));
  }

  adjustmentItemsSearch(adjustmentSearchPayload: AdjustmentSearchPayload) {
    const url = getSearchAdjustmentEndpointUrl(
      adjustmentSearchPayload.variantCode,
      adjustmentSearchPayload.lotNumber
    );
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) => OtherReceiptsAdaptor.AdjustmentSearchedItems(data))
      );
  }
  confirmAdjustementItems(
    confirmAdjustementItems: ConfirmAdjustmentItemsPayload
  ) {
    const url = getAdjustmentConfirmUrl(confirmAdjustementItems.type);
    return this.apiService
      .post(url, {
        items: confirmAdjustementItems.items,
        remarks: confirmAdjustementItems.remarks
      })
      .pipe(map((data: any) => OtherReceiptsAdaptor.AdjustementConfirm(data)));
  }
  getOtherReceiptStock(
    id: string,
    transactionType: string
  ): Observable<OtherReceiptsModel> {
    const url = getotherReceiveStockByIdEndpointUrl(id, transactionType);
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) => OtherReceiptsAdaptor.OtherReceiptsDatafromJson(data))
      );
  }
  searchOtherReceiptItems(
    id: number,
    itemCode: string,
    lotNumber: string,
    status: string,
    transactionType: string
  ): Observable<{ items: OtherReceiptItem[]; count: number }> {
    const url = getSearchOtherReceiptItemsByItemCodeUrl(
      id,
      itemCode,
      lotNumber,
      status,
      transactionType
    );
    return this.apiService
      .get(url)
      .pipe(map((data: any) => OtherReceiveItemHelper.getItems(data)));
  }
  getTolerance(): Observable<number> {
    return of(tolerance);
  }
  verifyOtherReceiptItem(
    id: number,
    itemId: number,
    itemUpdate: ItemUpdate,
    transactionType: string
  ): Observable<OtherReceiptItem> {
    const url = getOtherReceiveVerifyItemEndpointUrl(
      id,
      itemId,
      transactionType
    );
    return this.apiService
      .patch(url, itemUpdate)
      .pipe(
        map((data: any) => OtherReceiptsAdaptor.OtherReceiptItemfromJson(data))
      );
  }
  updateAllOtherReceiptItems(
    id: number,
    request: any,
    transactionType: string
  ): Observable<any> {
    const url = getUpdateAllOtherReceiptItemsEndpointUrl(id, transactionType);
    return this.apiService.patch(url, request);
  }
  confirmOtherReceiveStn(
    id: number,
    confirmReceive: ConfirmOtherReceive,
    transactionType: string
  ): Observable<any> {
    const url = getConfirmOtherReceiptSTNEndpointUrl(id, transactionType);
    return this.apiService.patch(url, confirmReceive).pipe(
      map((data: any) => {
        return OtherReceiptsAdaptor.OtherReceiptsDatafromJson(data);
      })
    );
  }
  cancelStockRequest(id: number, requestType: string) {
    const cancelRequestURL = getCancelStockRequestUrl(id, requestType);
    return this.apiService.patch(cancelRequestURL).pipe(map(data => data));
  }

  printOtherIssue(id: number, requestType: string) {
    const printOtherissueURL = getPrintOtherIssuesEndpointUrl(id, requestType);
    this.apiService.ResponseContentType = responseTypeEnum.Text;
    return this.apiService.get(printOtherissueURL).pipe(
      map(data => {
        return data;
      })
    );
  }
}
