import { Moment } from 'moment';

export interface OtherReceiptsModel {
  id: number;
  currencyCode: string;
  carrierDetails: CourierDetails;
  srcDocNo: number;
  srcDocDate: Moment;
  srcFiscalYear: number;
  destDocDate: Moment;
  destDocNo: number;
  weightUnit: string;
  locationCode: string;
  status:string;
  totalAvailableQuantity: number;
  totalAvailableValue: number;
  totalAvailableWeight: number;
  totalMeasuredQuantity: number;
  totalMeasuredValue: number;
  totalMeasuredWeight: number;
  transactionType: string;
}
export class OtherReceiptsDataModel {
  receiptsData: OtherReceiptsModel[] = [];
  totalElements: number;
}
export interface CourierDetails {
  type: string;
  data: {
    courierCompany: string;
    docketNumber: string;
    employeeName: string;
    employeeCode: string;
    Designation: string;
    contactNo: number;
    emailId: string;
  };
}
export interface ConfirmAdjustementItem {
  binCode: string;
  binGroupCode: string;
  itemCode: string;
  measuredWeight: number;
  value: number;
  quantity: number;
}
export interface AdjustmentItem {
  id: number;
  currencyCode: string;
  imageURL: string;
  itemCode: string;
  itemValue: number;
  lotNumber: string;
  mfgDate: Moment;
  orderType: string;
  productCategory: string;
  productGroup: string;
  productCategoryId: string,
  productGroupId: string,
  status: string;
  totalQuantity: number;
  totalValue: number;
  totalWeight: number;
  weightUnit: string;
  measuredQuantity: number;
  measuredWeight: number;
  binCode: string;
  binGroupCode: string;
  itemWeight: number;
  availableQuantity?: number;
  destDocNo?: number;
  isSelected?: boolean;
  availableValue:number;
  availableWeight:number;
  stdWeight:number;
  stdValue:number;
}
export interface OtherReceiptItem {
  id: number;
  currencyCode: string;
  imageURL: string;
  itemCode: string;
  itemDetails: ItemDetail;
  itemValue: number;
  lotNumber: string;
  mfgDate: Moment;
  orderType: string;
  productCategory: string;
  productGroup: string;
  status: string;
  totalQuantity: number;
  totalValue: number;
  totalWeight: number;
  weightUnit: string;
  measuredQuantity: number;
  measuredWeight: number;
  binCode: string;
  binGroupCode: string;
  remarks: string;
  isUpdating: boolean;
  isUpdatingSuccess: boolean;
  stdValue: number;
  stdWeight: number;
  availableQuantity: number;
  availableWeight: number;
  availableValue: number;
  measuredValue: number;
}
export class ItemDetail {
  otherStoneWt: number;
  actualGoldWeight: number;
}
export interface ItemUpdate {
  binCode: string;
  binGroupCode: string;
  measuredWeight: number;
  remarks: string;
  itemDetails: Object;
}
export interface ConfirmOtherReceive {
  courierReceivedDate?: string;
  // * for l1/l2
  reasonForDelay?: string;
  // * for l3
  receivedDate?: string;
  remarks: string;
}
export interface OtherReceiptItemToUpdate {
  id: number;
  newUpdate: ItemUpdate;
  actualDetails: ItemUpdate;
}
export interface FilterOption {
  id: string;
  description: string;
}
