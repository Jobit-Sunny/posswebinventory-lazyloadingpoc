export enum StockItemBinGroupCodeEnum {
  STN = 'STN',
  PURCFA = 'PURCFA',
  DISPUTE = 'DISPUTE',
  DEFECTIVE = 'DEFECTIVE'
}
export enum StockItemBinCodeEnum {
  ZERO_BIN = 'ZEROBIN'
}
export enum OtherReceiveTabEnum {
  NON_VERIFIED = 'nonverified',
  VERIFIED = 'verified'
}
export enum otherReceiptStockItemStatusEnum {
  VERIFIED = 'VERIFIED',
  ISSUED = 'ISSUED'
}
export enum responseTypeEnum {
  BLOB = 'blob',
  Document='document',
  Text = 'text'
}
