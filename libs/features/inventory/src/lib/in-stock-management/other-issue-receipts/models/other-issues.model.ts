import { Moment } from 'moment';

export interface OtherIssueModel {
  id: number;
  srcLocationCode: string;
  destLocationCode: string;
  status: string;
  weightUnit: string;
  currencyCode: string;
  reqDocNo: number;
  reqDocDate: Moment;
  reqLocationCode: string;
  requestType: string;
  carrierDetails: CourierDetailsOtherIssue,
  destDocDate: Moment,
  destDocNo: number,
  orderType: string,
  otherDetails: OtherDetails,
  srcDocDate: Moment,
  srcDocNo: number,
  srcFiscalYear: number,
  totalAvailableQuantity: number,
  totalAvailableValue: number,
  totalAvailableWeight: number,
  totalMeasuredQuantity: number,
  totalMeasuredValue: number,
  totalMeasuredWeight: number,
}
export class OtherIssuedataModel {
  issueData: OtherIssueModel[] = [];
  totalElements: number;
}
export interface OtherIssuesItem {
  id: number;
  itemCode: string;
  lotNumber: string;
  productCategory: string;
  productCategoryId:string;
  productGroup: string;
  productGroupId:string;
  binCode: string;
  binGroupCode: string;
  orderType: string;
  itemValue: number;
  itemWeight: number;
  totalQuantity: number;
  totalValue: number;
  totalWeight: number;
  currencyCode: string;
  weightUnit: string;
  mfgDate: Moment;
  status: string;
  imageURL: string;
  itemDetails: {};
  approvedQuantity: number;
  issuedQuantity: number;
  requestedQuantity: number;
  isUpdating: boolean;
  isUpdatingSuccess: boolean;
  totalElements?: number;
  inventoryId?: number;
  measuredWeight: number;
  measuredValue: number;
  measuredQuantity: number;
  availableQuantity: number;
  availableValue:number;
  availableWeight:number;
  stdWeight:number;
  stdValue:number;
}
export interface OtherIssuesCreateStockResponse {
  id: number;
  reqDocNo: number;
  srcLocationCode: string;
  destLocationCode: string;
  totalQuantity: number;
  status: string;
  reqDocDate: Moment;
}
export interface AdjustmentItemUpdate {
  quantity: number;
  weight: number;
}
export interface AdjustmentItemToUpdate {
  id: number;
  newUpdate: AdjustmentItemUpdate;
  actualDetails: AdjustmentItemUpdate;
}
export interface PSVItemUpdate {
  quantity: number;
  weight: number;
}
export interface PSVItemToUpdate {
  id: number;
  newUpdate: PSVItemUpdate;
  actualDetails: PSVItemUpdate;
}
export interface RequestOtherIssueStockTransferNote {
  currencyUnit: string;
  destLocationCode: string;
  id: number;
  reqDocDate: Moment;
  reqDocNo: number;
  reqLocationCode: string;
  requestType: string;
  srcLocationCode: string;
  status: string;
  totalQuantity: number;
  totalValue: number;
  totalWeight: number;
  weightUnit: string;
  carrierDetails?: CourierDetailsOtherIssue;
  otherDetails?: OtherDetails;
  totalAvailableValue:number;
  totalAvailableWeight:number;
  totalAvailableQuantity:number;
}
export interface CourierDetailsOtherIssue {
  type: string;
  data?: {
    address1: string;
    address2: string;
    city: string;
    town: string;
    pinCode: string;
    employeeName: string;
    employeeId: string;
    Designation: string;
    contactNo: number;
    emailId: string;
  };
}
export interface OtherDetails {
  type: string;
  data?: {
    approvalCode: string;
    approvedBy: string;
  };
}
export const SORT_DATA = [
  {
    id: 0,
    sortByColumnName: 'Available Weight',
    sortAscOrder: false
  },
  {
    id: 1,
    sortByColumnName: 'Available Quantity',
    sortAscOrder: false
  }
];
export const SORT_DATA_FOC = [
  {
    id: 0,
    sortByColumnName: 'Item Weight',
    sortAscOrder: false
  },
  {
    id: 1,
    sortByColumnName: 'Item Quantity',
    sortAscOrder: false
  }
];
export interface FilterOption {
  id: string;
  description: string;
}
