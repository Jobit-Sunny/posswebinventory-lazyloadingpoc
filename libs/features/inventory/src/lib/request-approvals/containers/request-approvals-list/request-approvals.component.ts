import {
  Component,
  OnInit,
  OnDestroy,
  AfterViewInit,
  ViewChild,
  ElementRef
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable, Subject, fromEvent } from 'rxjs';
import { BinRequestApprovalsItems } from '../../models/bin-reuest-approvals.model';
import { PageEvent } from '@angular/material';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

import {
  SelectionDialogService,
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  Option
} from '@poss-web/shared';
import { RequestApprovalsFacade } from '../../+state/request-approvals.facade';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil, debounceTime, filter } from 'rxjs/operators';
import * as BinRequestApprovalsActions from '../../+state/request-approvals.actions';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import {
  RequestApprovalsEnum,
  RequestApprovalsAPITypesEnum
} from '../../request-approvals.enum';
import { RequestApprovals } from '../../models/request-approvals-items.model';

@Component({
  selector: 'poss-web-request-approvals',
  templateUrl: './request-approvals.component.html',
  styleUrls: ['./request-approvals.component.scss']
})
export class RequestApprovalsComponent
  implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;
  searchForm: FormGroup;
  binRequestApprovalsCount$: Observable<any>;
  ibtRequestApprovalsCount$: Observable<any>;
  reqApprovalsType: RequestApprovalsEnum;
  reqType: RequestApprovalsEnum;
  reqApprovalsTypeRef = RequestApprovalsEnum;
  isBinRequestLoadedOnce = false;
  isIbtRequestLoadedOnce = false;
  isOtherIssuezRequestLoadedOnce = false;
  isADJloadedOnce = false;
  isPSVLoadedOnce = false;
  isLOSSLoadedOnce = false;
  isLOANLoadedOnce = false;
  isFOCLoadedOnce = false;
  isEXHLoadedOnce = false;
  adjCount: Observable<number>;
  psvCount: Observable<number>;
  lossCount: Observable<number>;
  loanCount: Observable<number>;
  focCount: Observable<number>;
  exhCount: Observable<number>;
  adj$: Observable<RequestApprovals[]>;
  psv$: Observable<RequestApprovals[]>;
  loss$: Observable<RequestApprovals[]>;

  loan$: Observable<RequestApprovals[]>;
  exh$: Observable<RequestApprovals[]>;
  foc$: Observable<RequestApprovals[]>;

  storeType: string;
  isLoading: boolean;
  isLocationLoading: boolean;
  otherIssuesType: Observable<number>;
  Items$: Observable<BinRequestApprovalsItems[]>;
  ibtCard$: Observable<RequestApprovals[]>;
  isIbtLoading$: Observable<boolean>;
  isFocLoading$: Observable<boolean>;
  isLossLoading$: Observable<boolean>;
  isLoanLoading$: Observable<boolean>;
  isExhLoading$: Observable<boolean>;
  isPsvLoading$: Observable<boolean>;
  isAdjLoading$: Observable<boolean>;
  isIbtLoading: boolean;
  otherIssuesCount: Observable<number>;
  //TODO take pagesize from appsettings
  pageIndex = 0;

  locationForSelection: Option[] = [];
  selectedLocation: Option;
  location$: Observable<Location[]>;
  initailPageEvent: PageEvent = {
    pageIndex: this.pageIndex,
    pageSize: 5,
    length: 0
  };

  pageSize = 8;

  itemsPageEvent: PageEvent = this.initailPageEvent;
  isItemsLoading$: Observable<boolean>;
  isItemsLoading: boolean;
  destroy$: Subject<null> = new Subject<null>();
  hasNotification: boolean;
  searchValue: number;
  otherIssues$: Observable<RequestApprovals[]>;

  reqDocNo: number;
  locationCode: string;
  searchLocationPlaceHolder: string;
  selectLocationLableText: string;

  constructor(
    private router: Router,
    private appsettingFacade: AppsettingFacade,
    private activatedRoute: ActivatedRoute,
    private selectionDialog: SelectionDialogService,
    private formBuilder: FormBuilder,
    private binRequestFacade: RequestApprovalsFacade,
    private translate: TranslateService,
    private overlayNotification: OverlayNotificationService
  ) {
    this.translate
      .get([
        'pw.instock.selectLocationPlaceHolder',
        'pw.instock.selectDestinationBinLableText'
      ])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: any) => {
        this.selectLocationLableText =
          translatedMessages['pw.instock.selectDestinationBinLableText'];
        this.searchLocationPlaceHolder =
          translatedMessages['pw.instock.selectLocationPlaceHolder'];
      });
    this.searchForm = this.formBuilder.group({
      searchValue: [],
      selectedValue: []
    });

    // TODO : change logic. Route state
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        const type = this.activatedRoute.snapshot.params['type'];
        // const tab = this.activatedRoute.snapshot.params['tab'];

        if (
          type === RequestApprovalsEnum.BIN_COR ||
          type === RequestApprovalsEnum.IBT_COR ||
          type === RequestApprovalsEnum.Other_Issues
        ) {
          this.changeReqApprovalsType(type);
        } else if (
          type === RequestApprovalsEnum.ADJ ||
          type === RequestApprovalsEnum.FOC ||
          type === RequestApprovalsEnum.EXH ||
          type === RequestApprovalsEnum.LOAN ||
          type === RequestApprovalsEnum.LOSS ||
          type === RequestApprovalsEnum.PSV
        ) {
          this.changeReqType(type);
        }
      });
  }

  ngOnInit() {
    this.binRequestFacade.resetError();
    this.binRequestFacade.resetUpdate();
    this.binRequestFacade.resetBinRequestApprovalsCount();
    this.binRequestFacade.resetBinRequestApprovals();
    this.ibtRequestApprovalsCount$ = this.binRequestFacade.getIbtItemsCount();
    this.ibtCard$ = this.binRequestFacade.getIbtRequest();
    this.adj$ = this.binRequestFacade.getadjRequest();
    this.adjCount = this.binRequestFacade.getadjCount();
    this.exh$ = this.binRequestFacade.getexhRequest();

    this.exhCount = this.binRequestFacade.getexhCount();

    this.foc$ = this.binRequestFacade.getfocRequest();
    this.focCount = this.binRequestFacade.getfocCount();
    this.loan$ = this.binRequestFacade.getloanRequest();
    this.loanCount = this.binRequestFacade.getloanCount();
    this.loss$ = this.binRequestFacade.getlossRequest();
    this.lossCount = this.binRequestFacade.getlossCount();
    this.psv$ = this.binRequestFacade.getpsvRequest();
    this.psvCount = this.binRequestFacade.getpsvCount();

    this.binRequestFacade.resetIbtRequestApprovalsCount();
    this.binRequestFacade.resetIbtRequestApprovals();
    this.binRequestFacade.resetFocRequestApprovalsCount();
    this.binRequestFacade.resetFocRequestApprovals();
    this.binRequestFacade.resetExhRequestApprovalsCount();
    this.binRequestFacade.resetExhtRequestApprovals();
    this.binRequestFacade.resetPsvRequestApprovalsCount();
    this.binRequestFacade.resetPsvRequestApprovals();
    this.binRequestFacade.resetLossRequestApprovalsCount();
    this.binRequestFacade.resetLossRequestApprovals();
    this.binRequestFacade.resetLoanRequestApprovalsCount();
    this.binRequestFacade.resetLoanRequestApprovals();
    this.binRequestFacade.resetadjRequestApprovalsCount();
    this.binRequestFacade.resetadjRequestApprovals();

    this.otherIssuesCount = this.binRequestFacade.getOtherIssueCount();

    this.binRequestFacade.loadbinRequestApprovalsItemCount();

    this.binRequestFacade.loadibtRequestApprovalsItemCount();

    this.isItemsLoading$ = this.binRequestFacade.getIsBinRequestApprovalsItemsLoading();
    this.isItemsLoading$.pipe(takeUntil(this.destroy$)).subscribe(data => {
      if (data) {
        this.isItemsLoading = data;
      }
    });
    this.Items$ = this.binRequestFacade.getbinItems();
    // this.ibtCard$=this.binRequestFacade.getIbtRequest();
    // this.otherIssues$=this.binRequestFacade.getIbtRequest();

    this.isIbtLoading$ = this.binRequestFacade.getIsLoadingIbt();
    this.isAdjLoading$ = this.binRequestFacade.getIsLoadingadj();
    this.isFocLoading$ = this.binRequestFacade.getIsLoadingfoc();
    this.isLossLoading$ = this.binRequestFacade.getIsLoadingloss();
    this.isLoanLoading$ = this.binRequestFacade.getIsLoadingloan();
    this.isExhLoading$ = this.binRequestFacade.getIsLoadingexh();
    this.isPsvLoading$ = this.binRequestFacade.getIsLoadingpsv();

    this.isIbtLoading$.pipe(takeUntil(this.destroy$)).subscribe(data => {
      if (data) {
        this.isIbtLoading = data;
      }
    });

    this.binRequestFacade
      .getupdateItem()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          this.initialLoad();
          this.binRequestFacade.loadbinRequestApprovalsItemCount();
          this.successNotification(data.status, data.binName);
        }
      });
    // this.binRequestFacade.loadLocationCount();
    this.appsettingFacade
      .getStoreType()
      .pipe(takeUntil(this.destroy$))
      .subscribe((storeType: string) => {
        if (storeType) {
          this.storeType = storeType;
        }
      });
    this.componentOnInit();
    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        const pageSize = JSON.parse(data);
        this.initailPageEvent.pageSize = pageSize;
      });

    this.binRequestFacade
      .getIsLoading()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isLoading: boolean) => {
        this.isLoading = isLoading;
      });

    this.binRequestFacade
      .getIsLocationLoading()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isLoading: boolean) => {
        this.isLocationLoading = isLoading;
      });
    this.binRequestApprovalsCount$ = this.binRequestFacade.getItemCount();

    // this.ibtRequestApprovalsCount$.subscribe(data=>console.log(data));
    // this.binRequestFacade.getLocationCount().pipe(takeUntil(this.destroy$)).subscribe((data) => {
    //   if (data) {
    //     this.loadLocation(0, data);
    //   }
    // });
    this.binRequestFacade.loadLocations();

    this.componentInit();

    this.binRequestFacade
      .getHasUpdatingFailure()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          this.errorHandler(data);
        }
      });

    this.binRequestFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe(error => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.binRequestFacade.loadRequestCount();
  }

  componentOnInit() {
    const type = this.activatedRoute.snapshot.params['type'];
    // const tab = this.activatedRoute.snapshot.params['tab'];

    if (
      !type ||
      !(
        type === RequestApprovalsEnum.BIN_COR ||
        type === RequestApprovalsEnum.IBT_COR ||
        type === RequestApprovalsEnum.Other_Issues
      )
    ) {
      // this.router.navigate(['404']);
    } else {
      this.changeReqApprovalsType(type);
    }

    if (
      !type ||
      !(
        type === RequestApprovalsEnum.ADJ ||
        type === RequestApprovalsEnum.EXH ||
        type === RequestApprovalsEnum.FOC ||
        type === RequestApprovalsEnum.LOAN ||
        type === RequestApprovalsEnum.LOSS ||
        type === RequestApprovalsEnum.EXH
      )
    ) {
      // this.router.navigate(['404']);
    } else {
      this.changeReqType(type);
    }
  }

  changeReqApprovalsType(newType: RequestApprovalsEnum): void {
    // this.binRequestFacade.resetIbtRequestApprovals();
    // this.binRequestFacade.resetIbtRequestApprovalsCount();
    if (this.reqApprovalsType !== newType) {
      this.clear();
      this.router.navigate(['..', newType], {
        relativeTo: this.activatedRoute
      });
      this.reqApprovalsType = newType;

      if (
        this.reqApprovalsType === RequestApprovalsEnum.BIN_COR ||
        (this.reqApprovalsType === RequestApprovalsEnum.IBT_COR &&
          !this.isIbtRequestLoadedOnce) ||
        (this.reqApprovalsType === RequestApprovalsEnum.Other_Issues &&
          !this.isOtherIssuezRequestLoadedOnce)
      ) {
        this.loadRequests(0);
      }
    }
  }
  changeReqType(newType: RequestApprovalsEnum): void {
    // this.binRequestFacade.resetIbtRequestApprovals();
    // this.binRequestFacade.resetIbtRequestApprovalsCount();
    this.reqApprovalsType = RequestApprovalsEnum.Other_Issues;
    if (this.reqType !== newType) {
      this.clear();
      this.router.navigate(['..', newType], {
        relativeTo: this.activatedRoute
      });
      this.reqType = newType;

      if (
        (this.reqType === RequestApprovalsEnum.ADJ && !this.isADJloadedOnce) ||
        (this.reqType === RequestApprovalsEnum.LOAN &&
          !this.isLOANLoadedOnce) ||
        (this.reqType === RequestApprovalsEnum.LOSS &&
          !this.isLOSSLoadedOnce) ||
        (this.reqType === RequestApprovalsEnum.FOC && !this.isFOCLoadedOnce) ||
        (this.reqType === RequestApprovalsEnum.PSV && !this.isPSVLoadedOnce) ||
        (this.reqType === RequestApprovalsEnum.EXH && !this.isEXHLoadedOnce)
      ) {
        this.loadOtherIssues(0);
      }
    }
  }

  loadOtherIssues(index: number) {
    this.reqDocNo = null;
    this.locationCode = null;
    this.loadOtherIssueRequest(index);
  }

  componentInit() {
    this.binRequestFacade
      .getLocations()
      .pipe(takeUntil(this.destroy$))
      .subscribe((locations: any) => {
        if (locations) {
          this.locationForSelection = locations.map(location => ({
            id: location.locationCode,
            description: location.locationCode + ' - ' + location.description
          }));
        }
      });
  }

  ngAfterViewInit() {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe((event: any) => {
        this.searchValue = this.searchForm.get('searchValue').value;

        if (this.searchValue && !isNaN(this.searchValue)) {
          this.search(this.searchValue);
        } else if (!this.searchValue) {
          this.clearSearch();
        } else if (isNaN(this.searchValue)) {
          this.searchForm.get('searchValue').setErrors({
            nan: 'pw.stockReceive.searchNotaNumberErrorMessage'
          });
          this.searchForm.get('searchValue').markAsTouched();
        }
      });
  }
  select(selectedValue: Option) {
    this.reset();
    this.locationCode = selectedValue.id;
    if (this.reqApprovalsType === RequestApprovalsEnum.BIN_COR) {
      this.itemsPageEvent = this.initailPageEvent;
      this.loadBinApprovals();
    } else if (this.reqApprovalsType === RequestApprovalsEnum.IBT_COR) {
      this.pageIndex = 0;
      this.loadIbtRequest(this.pageIndex);
    } else if (this.reqApprovalsType === RequestApprovalsEnum.Other_Issues) {
      this.pageIndex = 0;
      this.loadOtherIssueRequest(this.pageIndex);
    }
  }

  openBinSelectionPopup() {
    this.selectionDialog
      .open({
        title: this.selectLocationLableText,
        placeholder: this.searchLocationPlaceHolder,
        options: this.locationForSelection
      })
      .pipe(takeUntil(this.destroy$))
      .subscribe((selectedOption: Option) => {
        if (selectedOption) {
          this.selectedLocation = selectedOption;
          this.select(this.selectedLocation);
        }
      });
  }

  search(searchValue: number) {
    this.reqDocNo = searchValue;
    this.reset();

    if (this.reqApprovalsType === RequestApprovalsEnum.BIN_COR) {
      this.itemsPageEvent = this.initailPageEvent;
      this.loadBinApprovals();
    } else if (this.reqApprovalsType === RequestApprovalsEnum.IBT_COR) {
      this.pageIndex = 0;
      this.loadIbtRequest(this.pageIndex);
    } else if (this.reqApprovalsType === RequestApprovalsEnum.Other_Issues) {
      this.pageIndex = 0;
      this.loadOtherIssueRequest(this.pageIndex);
    }
  }

  clear() {
    this.reqDocNo = null;
    this.searchForm.reset();
    this.locationCode = null;
  }

  clearSearch() {
    this.reqDocNo = null;
    if (this.reqApprovalsType === RequestApprovalsEnum.BIN_COR) {
      this.loadBinApprovals();
    } else if (this.reqApprovalsType === RequestApprovalsEnum.IBT_COR) {
      this.loadIbtRequest(0);
    } else if (this.reqApprovalsType === RequestApprovalsEnum.Other_Issues) {
      this.loadOtherIssueRequest(0);
    }
    this.searchForm.reset();
  }

  loadRequests(pageIndex: number): void {
    this.getRequestApprovals();

    if (this.reqApprovalsType === RequestApprovalsEnum.BIN_COR) {
      this.reqType = null;
      this.initialLoad();
    } else if (this.reqApprovalsType === RequestApprovalsEnum.IBT_COR) {
      // this.router.navigate(['404']);
      this.reqType = null;

      this.reqDocNo = null;
      this.locationCode = null;
      this.loadIbtRequest(pageIndex);
    } else if (this.reqApprovalsType === RequestApprovalsEnum.Other_Issues) {
      // this.router.navigate(['404']);

      // if()
      if (!this.reqType) {
        this.reqType = RequestApprovalsEnum.ADJ;
      }
      this.reqDocNo = null;
      this.locationCode = null;
      this.loadOtherIssueRequest(pageIndex);
    }
  }
  reset() {
    if (this.reqApprovalsType === RequestApprovalsEnum.IBT_COR) {
      this.binRequestFacade.resetIbtRequestApprovalsCount();
      this.binRequestFacade.resetIbtRequestApprovals();
    } else if (this.reqApprovalsType === RequestApprovalsEnum.Other_Issues) {
      if (this.reqType === RequestApprovalsEnum.FOC) {
        this.binRequestFacade.resetFocRequestApprovalsCount();
        this.binRequestFacade.resetFocRequestApprovals();
      } else if (this.reqType === RequestApprovalsEnum.EXH) {
        this.binRequestFacade.resetExhRequestApprovalsCount();
        this.binRequestFacade.resetExhtRequestApprovals();
      } else if (this.reqType === RequestApprovalsEnum.PSV) {
        this.binRequestFacade.resetPsvRequestApprovalsCount();
        this.binRequestFacade.resetPsvRequestApprovals();
      } else if (this.reqType === RequestApprovalsEnum.LOSS) {
        this.binRequestFacade.resetLossRequestApprovalsCount();
        this.binRequestFacade.resetLossRequestApprovals();
      } else if (this.reqType === RequestApprovalsEnum.LOAN) {
        this.binRequestFacade.resetLoanRequestApprovalsCount();
        this.binRequestFacade.resetLoanRequestApprovals();
      } else if (this.reqType === RequestApprovalsEnum.ADJ) {
        this.binRequestFacade.resetadjRequestApprovalsCount();
        this.binRequestFacade.resetadjRequestApprovals();
      }
    }
  }

  getRequestApprovals() {
    if (this.reqApprovalsType === RequestApprovalsEnum.IBT_COR) {
      this.binRequestFacade.loadIbtRequest({
        reqLocationCode: this.locationCode,
        reqDocNo: this.reqDocNo,
        requestType: RequestApprovalsAPITypesEnum.BTQ,
        pageIndex: this.pageIndex,
        pageSize: this.pageSize
      });

      this.ibtCard$ = this.binRequestFacade.getIbtRequest();

      this.ibtCard$
        .pipe(takeUntil(this.destroy$))
        .subscribe((items: RequestApprovals[]) => {
          if (items && items.length !== 0 && !this.isIbtRequestLoadedOnce) {
            this.isIbtRequestLoadedOnce = true;
          }
        });
    }
    if (this.reqType === RequestApprovalsEnum.ADJ) {
      // this.otherIssuesType=this.adjCount;

      this.adj$
        .pipe(takeUntil(this.destroy$))
        .subscribe((items: RequestApprovals[]) => {
          if (items && items.length !== 0 && !this.isADJloadedOnce) {
            this.isADJloadedOnce = true;
          }
        });
    } else if (this.reqType === RequestApprovalsEnum.EXH) {
      this.exh$
        .pipe(takeUntil(this.destroy$))
        .subscribe((items: RequestApprovals[]) => {
          if (items && items.length !== 0 && !this.isEXHLoadedOnce) {
            this.isEXHLoadedOnce = true;
          }
        });
    } else if (this.reqType === RequestApprovalsEnum.FOC) {
      // this.otherIssuesType=this.adjCount;

      this.foc$
        .pipe(takeUntil(this.destroy$))
        .subscribe((items: RequestApprovals[]) => {
          if (items && items.length !== 0 && !this.isFOCLoadedOnce) {
            this.isFOCLoadedOnce = true;
          }
        });
    } else if (this.reqType === RequestApprovalsEnum.LOAN) {
      // this.otherIssuesType=this.adjCount;

      this.loan$
        .pipe(takeUntil(this.destroy$))
        .subscribe((items: RequestApprovals[]) => {
          if (items && items.length !== 0 && !this.isLOANLoadedOnce) {
            this.isLOANLoadedOnce = true;
          }
        });
    } else if (this.reqType === RequestApprovalsEnum.LOSS) {
      // this.otherIssuesType=this.adjCount;

      this.loss$
        .pipe(takeUntil(this.destroy$))
        .subscribe((items: RequestApprovals[]) => {
          if (items && items.length !== 0 && !this.isLOSSLoadedOnce) {
            this.isLOSSLoadedOnce = true;
          }
        });
    } else if (this.reqType === RequestApprovalsEnum.PSV) {
      // this.otherIssuesType=this.adjCount;

      this.psv$
        .pipe(takeUntil(this.destroy$))
        .subscribe((items: RequestApprovals[]) => {
          if (items && items.length !== 0 && !this.isPSVLoadedOnce) {
            this.isPSVLoadedOnce = true;
          }
        });
    }
  }

  loadOtherIssueRequest(pageIndex: number) {
    let type;
    if (this.reqType === RequestApprovalsEnum.ADJ) {
      type = RequestApprovalsAPITypesEnum.ADJ;
      this.binRequestFacade.loadADJRequest({
        reqLocationCode: this.locationCode,
        reqDocNo: this.reqDocNo,
        requestType: type,
        pageIndex: pageIndex,
        pageSize: this.pageSize
      });
    } else if (this.reqType === RequestApprovalsEnum.EXH) {
      type = RequestApprovalsAPITypesEnum.EXH;
      this.binRequestFacade.loadEXHRequest({
        reqLocationCode: this.locationCode,
        reqDocNo: this.reqDocNo,
        requestType: type,
        pageIndex: pageIndex,
        pageSize: this.pageSize
      });
    } else if (this.reqType === RequestApprovalsEnum.FOC) {
      type = RequestApprovalsAPITypesEnum.FOC;
      this.binRequestFacade.loadFocRequest({
        reqLocationCode: this.locationCode,
        reqDocNo: this.reqDocNo,
        requestType: type,
        pageIndex: pageIndex,
        pageSize: this.pageSize
      });
    } else if (this.reqType === RequestApprovalsEnum.LOSS) {
      type = RequestApprovalsAPITypesEnum.LOSS;
      this.binRequestFacade.loadLOSSRequest({
        reqLocationCode: this.locationCode,
        reqDocNo: this.reqDocNo,
        requestType: type,
        pageIndex: pageIndex,
        pageSize: this.pageSize
      });
    } else if (this.reqType === RequestApprovalsEnum.LOAN) {
      type = RequestApprovalsAPITypesEnum.LOAN;
      this.binRequestFacade.loadLOANRequest({
        reqLocationCode: this.locationCode,
        reqDocNo: this.reqDocNo,
        requestType: type,
        pageIndex: pageIndex,
        pageSize: this.pageSize
      });
    } else if (this.reqType === RequestApprovalsEnum.PSV) {
      type = RequestApprovalsAPITypesEnum.PSV;
      this.binRequestFacade.loadPSVRequest({
        reqLocationCode: this.locationCode,
        reqDocNo: this.reqDocNo,
        requestType: type,
        pageIndex: pageIndex,
        pageSize: this.pageSize
      });
    }

    // if(this.reqApprovalsType===RequestApprovalsEnum.Other_Issues && (
    //   this.reqType===RequestApprovalsEnum.ADJ || this.reqType===RequestApprovalsEnum.EXH ||
    //   this.reqType===RequestApprovalsEnum.FOC || this.reqType===RequestApprovalsEnum.LOAN||
    //   this.reqType===RequestApprovalsEnum.LOSS|| this.reqType===RequestApprovalsEnum.PSV
    // ))

    // console.log(this.otherIssuesType);
  }

  onOtherIssuesSelected(stock: any): void {
    let type;
    if (this.reqType === RequestApprovalsEnum.ADJ) {
      type = RequestApprovalsEnum.ADJ;
    } else if (this.reqType === RequestApprovalsEnum.FOC) {
      type = RequestApprovalsEnum.FOC;
    } else if (this.reqType === RequestApprovalsEnum.LOSS) {
      type = RequestApprovalsEnum.LOSS;
    } else if (this.reqType === RequestApprovalsEnum.LOAN) {
      type = RequestApprovalsEnum.LOAN;
    } else if (this.reqType === RequestApprovalsEnum.EXH) {
      type = RequestApprovalsEnum.EXH;
    } else if (this.reqType === RequestApprovalsEnum.PSV) {
      type = RequestApprovalsEnum.PSV;
    }

    this.router.navigate(['inventory/approvals' + '/' + type + '/' + stock.id]);
  }

  onSelected(stock: any) {
    this.router.navigate(['inventory/approvals/IbtRequest/' + stock.id]);
  }

  loadIbtRequest(pageIndex: number) {
    this.binRequestFacade.loadIbtRequest({
      reqLocationCode: this.locationCode,
      reqDocNo: this.reqDocNo,
      requestType: RequestApprovalsAPITypesEnum.BTQ,
      pageIndex: pageIndex,
      pageSize: this.pageSize
    });
  }

  initialLoad() {
    this.locationCode = null;
    this.reqDocNo = null;
    this.binRequestFacade.loadBinRequestApprovalsItems({
      pageIndex: this.pageIndex,
      pageSize: this.initailPageEvent.pageSize
    });
  }

  paginateItems(event: PageEvent) {
    this.itemsPageEvent = event;
    this.loadBinApprovals();
  }

  loadBinApprovals() {
    this.binRequestFacade.loadBinRequestApprovalsItems({
      locationCode: this.locationCode,
      reqDocNo: this.reqDocNo,
      pageIndex: this.itemsPageEvent.pageIndex,
      pageSize: this.itemsPageEvent.pageSize
    });
  }

  approvals(approvalsValue: BinRequestApprovalsActions.BinApprovalspayload) {
    this.binRequestFacade.updateItem({
      binRequestUpdateDto: {
        remarks: approvalsValue.binRequestUpdateDto.remarks,
        status: approvalsValue.binRequestUpdateDto.status
      },
      id: approvalsValue.id
    });
  }

  clearPopup() {
    this.locationCode = null;
    if (this.reqApprovalsType === RequestApprovalsEnum.BIN_COR)
      this.loadBinApprovals();
    else if (this.reqApprovalsType === RequestApprovalsEnum.IBT_COR)
      this.loadIbtRequest(0);
    else if (this.reqApprovalsType === RequestApprovalsEnum.Other_Issues)
      this.loadOtherIssueRequest(0);
  }

  back() {
    //this.router.navigate(['inventory', 'instock', 'binTransfer']);
    this.router.navigate(['..'], { relativeTo: this.activatedRoute });
  }

  errorHandler(error: CustomErrors) {
    this.hasNotification = true;
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        this.hasNotification = false;
      });
  }

  successNotification(status: string, bin: string) {
    let key = '';
    // check if the error is the custom error
    if (status === 'APPROVED') {
      //Obtain the transation key which will be use to obtain the translated error message
      //based on the language selected. Default is the english language(refer en.json from asset folder).
      key = 'pw.requestApprovalsNotificationMessages.acceptMessage';
    } else if (status === 'APVL_REJECTED') {
      key = 'pw.requestApprovalsNotificationMessages.rejectMessage';
    }
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.SIMPLE,
            message: bin + ' ' + translatedMsg,
            hasClose: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
          });
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
