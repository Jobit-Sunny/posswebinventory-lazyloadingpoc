import { RequestApprovalEnum, RequestApprovalsEnum } from '../../request-approvals.enum';
import { RequestApprovalsItems } from '../../models/request-approvals-items.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { PageEvent } from '@angular/material';
import { Observable, Subject } from 'rxjs';
import { SelectedStockPayload } from '../../+state/request-approvals.actions';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { OverlayNotificationService, OverlayNotificationType, OverlayNotificationEventRef, OverlayNotificationEventType } from '@poss-web/shared';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import { RequestApprovalsFacade } from '../../+state/request-approvals.facade';
import { takeUntil } from 'rxjs/operators';
import { RequestApprovals } from '../../models/request-approvals-items.model';
import { RequestApprovalsAPITypesEnum } from '../../request-approvals.enum';
import { commonTranslateKeyMap } from '../../../common.map';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'poss-web-other-issues-approvals-detail',
  templateUrl: './other-issues-approvals-detail.component.html',
  styleUrls: ['./other-issues-approvals-detail.component.scss']
})
export class OtherIssuesApprovalsDetailComponent implements OnInit, OnDestroy {
  items: RequestApprovalsItems[];

  item: any;
  requestId: any;
  tab: string;
  type: string;
  requestAppType = RequestApprovalsAPITypesEnum;
  id: number;
  pageIndex = 0;
  hasNotification: boolean;
  initailPageEvent: PageEvent = {
    pageIndex: this.pageIndex,
    pageSize: 0,
    length: 0
  };
  header: string;
  isSelectedArray: string[] = [];
  pageSize = 4;
  Items$: Observable<any>;
  payload: SelectedStockPayload;
  status: string;
  approvalsTransferForm: FormGroup;
  itemsPageEvent: PageEvent = this.initailPageEvent;
  ibtRequestApprovalsItemsCount$: Observable<any>;
  isItemsLoading: boolean;
  isSelectAll = false;
  loading$: Observable<boolean>;
  selectedArrayCount: number;
  requestApprovalRef = RequestApprovalEnum;
  selectionAllSubject: Subject<any> = new Subject<any>();
  destroy$: Subject<null> = new Subject<null>();
  constructor(private router: Router,
    private translate: TranslateService,
    private formBuilder: FormBuilder, private overlayNotification: OverlayNotificationService, private appsettingFacade: AppsettingFacade,
    private activatedRoute: ActivatedRoute, private facade: RequestApprovalsFacade) {
    this.approvalsTransferForm = this.formBuilder.group({
      selectItems: ['']
    });


  }

  ngOnInit() {

    this.facade.resetError();
    this.facade.resetUpdate();

    this.requestId = this.activatedRoute.snapshot.params['id'];
    this.type = this.activatedRoute.snapshot.params['type'];
    this.show();
    this.facade.resetRequestApprovalsItemCount();
    this.facade.resetRequestApprovalsItem();
     this.facade.loadIbtRequestApprovalsItemsCount({
      requestType: this.tab,
      id: this.requestId
    })

    this.ibtRequestApprovalsItemsCount$=this.facade.getIbtCount();
    this.ibtRequestApprovalsItemsCount$.subscribe(data=>{
        this.selectedArrayCount=data
    })



    this.Items$ = this.facade.getIbtRequestItems();


    this.Items$.pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.items = data;
        if (this.items.length !== 0) {
          this.InterBoutiqueTransferStatusRequestNotifications();
        }


      }
      );

    this.loading$ = this.facade.getIsLoading();


    this.Items$.pipe(takeUntil(this.destroy$))
      .subscribe((items) => {


        if (this.approvalsTransferForm.value.selectItems === '1') {
          this.isSelectAll = true;
          this.selectionAllSubject.next({
            selectCheckbox: true,
            enableCheckbox: false
          });
        }
      }
      )
    this.facade.getIbt().pipe(takeUntil(this.destroy$))
      .subscribe(data => {


        if (data !== null) {
          this.facade.clearItemList();
          this.resetValue();

          if (data.status) {
            this.InterBoutiqueTransferStatusNotifications(data.status);
          }
        }
      });
    // this.type=this.activatedRoute.snapshot.params['type'];
    // this.tab = this.activatedRoute.snapshot.params['tab'];


    this.facade.loadSelectedRequest({
      id: this.requestId,
      requestType: this.tab
    });
    this.id = this.requestId;
    this.facade
      .getSelectedRequest()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: RequestApprovals) => {
        if (data) {
          this.item = data;
          // this.initialLoad();
        }
        else {
          // this.router.navigate(['404']);
        }

      });

    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        const pageSize = JSON.parse(data);
        this.initailPageEvent.pageSize = pageSize;

      });




    // this.facade
    //   .getSelectedRequest()
    //   .pipe(takeUntil(this.destroy$))
    //   .subscribe((data: RequestApprovals) => {
    //     if (data) {
    //       this.item = data;
    //       // this.initialLoad();
    //     }
    //     else {
    //       // this.router.navigate(['404']);
    //     }

    //   });



    this.initialLoad();


    this.facade.getIsIbtRequestApprovalsItemsLoading().pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.isItemsLoading = data
      }
      );

    // this.ibtRequestApprovalsItemsCount$ = this.facade.getIbtCount();
    // this.ibtRequestApprovalsItemsCount$.pipe(takeUntil(this.destroy$))
    //   .subscribe(data => {
    //     this.selectedArrayCount = data
    //   }
    //   );

    this.facade.getError().pipe(takeUntil(this.destroy$)).subscribe(data => {

      if (data) {
        this.errorHandler(data)
      }
    });

    this.facade.getHasUpdatingApprovalsFailure().pipe(takeUntil(this.destroy$)).subscribe(error => {

      if (error) {
        this.errorHandler(error)
      }
    });



  }

  initialLoad() {
    // this.facade.loadIbtRequestApprovalsItemsCount(this.requestId);
    this.facade.loadIbtRequestItems({
      id: this.id,
      requestType: this.tab,
      pageIndex: this.pageIndex,
      pageSize: this.initailPageEvent.pageSize,
      isSelectedArray: []
    })
  }





  clearAll() {
    this.isSelectAll = false
    this.isSelectedArray = [];
    this.approvalsTransferForm.patchValue({
      selectItems: null
    });
    this.selectionAllSubject.next({
      selectCheckbox: false,
      enableCheckbox: true
    });
    this.InterBoutiqueTransferStatusRequestNotifications()

  }


  show() {

    if (this.type === RequestApprovalsEnum.ADJ) {
      this.header = this.requestApprovalRef.ADJ;
      this.tab = this.requestAppType.ADJ
    }
    else if (this.type === RequestApprovalsEnum.LOSS) {
      this.header = this.requestApprovalRef.LOSS;
      this.tab = this.requestAppType.LOSS
    }
    else if (this.type === RequestApprovalsEnum.LOAN) {
      this.header = this.requestApprovalRef.LOAN;
      this.tab = this.requestAppType.LOAN
    }
    else if (this.type === RequestApprovalsEnum.FOC) {
      this.header = this.requestApprovalRef.FOC;
      this.tab = this.requestAppType.FOC
    }
    else if (this.type === RequestApprovalsEnum.PSV) {
      this.header = this.requestApprovalRef.PSV;
      this.tab = this.requestAppType.PSV
    }
    else if (this.type === RequestApprovalsEnum.EXH) {
      this.header = this.requestApprovalRef.EXH;
      this.tab = this.requestAppType.EXH
    }


  }

  isSelect(event) {

    if (event.isSelected) {
      this.isSelectedArray = [...this.isSelectedArray, event.id]
    }
    else {
      const valueToRemove = event.id;
      //this.isSelectedArray=[...this.isSelectedArray.splice(this.isSelectedArray.indexOf(event.id), 1)];
      this.isSelectedArray = this.isSelectedArray.filter(item => item !== valueToRemove)

    }

    if (this.isSelectedArray.length === this.selectedArrayCount) {
      this.approvalsTransferForm.get('selectItems').setValue('1');

      this.selectChange();
    }

    if (this.isSelectedArray.length !== 0) {

      this.InterBoutiqueTransferStatusRequestNotifications();
    }

  }

  selectChange() {

    if (this.approvalsTransferForm.value.selectItems === '1') {

      this.selectAll();
    }
  }


  selectAll() {
    this.isSelectAll = true;
    this.selectionAllSubject.next({
      selectCheckbox: true,
      enableCheckbox: false
    });
    this.InterBoutiqueTransferStatusRequestNotifications();
  }




  errorHandler(error: CustomErrors) {
    this.hasNotification = true
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        this.hasNotification = false
        if(event.eventType===OverlayNotificationEventType.CLOSE){
          this.InterBoutiqueTransferStatusRequestNotifications()
        }
      });
  }

  InterBoutiqueTransferStatusRequestNotifications() {
    this.hasNotification = true;


    this.translate
      .get('pw.interBoutiqueTransferNotifications.acceptRejectMsg')
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.REQUEST_ACTION,
            message: translatedMessage,
            hasRemarks: true,
            isRemarksMandatory: true,
            hasClose: true
          })
          .events.subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === 1) {


              if (this.isSelectAll === false) {
                if (this.isSelectedArray.length !== 0) {

                  const acceptStatus = {
                    id: this.requestId,
                    requestType: this.tab,


                    requestUpdateDto:
                    {
                      itemIds: this.isSelectedArray,
                      remarks: event.data,
                      status: 'APPROVED'
                    }
                  };
                  this.facade.updateIbtSucess(
                    acceptStatus
                  );
                }


                else {

                  this.InterBoutiqueTransferErrorNotifications();

                }
              }
              else {

                const acceptStatus = {
                  id: this.requestId,
                  requestType: this.tab,


                  requestUpdateDto:
                  {
                    itemIds: [],
                    remarks: event.data,
                    status: 'APPROVED'
                  }
                };
                this.facade.updateIbtSucess(
                  acceptStatus
                );

              }






            }
            if (event.eventType === 2) {
              const rejectStatus = {
                id: this.requestId,
                requestType: this.tab,
                requestUpdateDto: {
                  itemIds: [],
                  remarks: event.data,
                  status: 'APVL_REJECTED'
                }


              };
              this.facade.updateIbtSucess(
                rejectStatus
              );


            }
          });
      });


  }

  InterBoutiqueTransferStatusNotifications(status: string) {
    this.hasNotification = true;
    const key =
      'pw.interBoutiqueTransferNotifications.statusResponseNotifications';
    let statusKey = {
      status: '',
      statusColor: ''
    };
    if (commonTranslateKeyMap.has(status)) {
      statusKey = commonTranslateKeyMap.get(status);
    }
    this.translate
      .get([key, statusKey.status])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            message:
              translatedMessages[key] +
              ' ' +
              translatedMessages[statusKey.status],
            hasBackdrop: true,
            hasClose: true
          })
          .events.subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.CLOSE) {
              this.back();
            }
          });
      });
  }

  InterBoutiqueTransferErrorNotifications() {
    const key =
      'pw.interBoutiqueTransferNotifications.selectItemsNotifications';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.SIMPLE,
            message: translatedMessage,
            hasBackdrop: false,
            hasClose: true
          })
          .events.subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.CLOSE) {
              this.InterBoutiqueTransferStatusRequestNotifications();
            }
          });
      });
  }

  resetValue() {
    this.isSelectedArray = [];
  }


  paginateItems(event: PageEvent) {
    //  const selectedArray=this.isSelectedArray;
    this.itemsPageEvent = event;
    this.loadIbtApprovals();

  }

  loadIbtApprovals() {
    // this.facade.loadIbtRequestApprovalsItemsCount(this.requestId);

    this.facade.loadIbtRequestItems({
      id: this.id,
      requestType: this.tab,
      pageIndex: this.itemsPageEvent.pageIndex,
      pageSize: this.itemsPageEvent.pageSize,
      isSelectedArray: this.isSelectedArray
    }
    )

  }


  back() {
    // console.log(this.header,'kk')
    // if(this.header==='Loan'){
    //   console.log('abcd')
    //   this.router.navigate([

    //     '/inventory/approvals/OtherIssuesRequest_LOAN'

    //   ])
    // }
    this.router.navigate(['..'], { relativeTo: this.activatedRoute });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();


  }
}
