import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import { RequestApprovals } from '../../models/request-approvals-items.model';
import { RequestApprovalsFacade } from '../../+state/request-approvals.facade';
import { takeUntil } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { RequestApprovalsAPITypesEnum } from '../../request-approvals.enum';
import { PageEvent } from '@angular/material';
import { OverlayNotificationService, OverlayNotificationType, OverlayNotificationEventRef, OverlayNotificationEventType ,ErrorEnums,

}  from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { commonTranslateKeyMap } from '../../../common.map';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'poss-web-ibt-request-approvals-details',
  templateUrl: './ibt-request-approvals-details.component.html',
  styleUrls: ['./ibt-request-approvals-details.component.scss']
})
export class IbtRequestApprovalsDetailsComponent implements OnInit, OnDestroy {


  item: any;
  requestId: any;
  type: string;
  id: number;
  pageIndex = 0;
  hasNotification: boolean;
  approvalsTransferForm: FormGroup;
  initailPageEvent: PageEvent = {
    pageIndex: this.pageIndex,
    pageSize: 50,
    length: 0
  };
  isSelectedArray: string[] = [];
  pageSize = 4;
  Items$: Observable<any>;
  items: any;
  isSelectAll = false;
  itemsPageEvent: PageEvent = this.initailPageEvent;
  ibtRequestApprovalsItemsCount$: Observable<any>;
  isItemsLoading: boolean;
  updateItemListStatusResponse$: Observable<RequestApprovals>;
  selectionAllSubject: Subject<any> = new Subject<any>();
  loading$: Observable<boolean>;
  selectedArrayCount: number;
  destroy$: Subject<null> = new Subject<null>();
  constructor(private router: Router, private translate: TranslateService, private formBuilder: FormBuilder, private overlayNotification: OverlayNotificationService, private appsettingFacade: AppsettingFacade,
    private activatedRoute: ActivatedRoute, private facade: RequestApprovalsFacade) {
    this.approvalsTransferForm = this.formBuilder.group({
      selectItems: ['']
    });


  }

  ngOnInit() {

    this.facade.resetError();
    this.facade.resetUpdate();
    this.requestId = this.activatedRoute.snapshot.params['id'];
    this.type = this.activatedRoute.snapshot.params['type'];
    this.facade.resetRequestApprovalsItemCount();
    this.facade.resetRequestApprovalsItem();
    this.facade.loadIbtRequestApprovalsItemsCount({
      requestType: 'BTQ',
      id: this.requestId
    })
    this.facade
      .getSelectedRequest()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: RequestApprovals) => {
        if (data) {
          this.item = data;
          // this.initialLoad();
        }
        else {
          // this.router.navigate(['404']);
        }

      });
    this.ibtRequestApprovalsItemsCount$ = this.facade.getIbtCount();
    this.ibtRequestApprovalsItemsCount$.pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.selectedArrayCount = data;

      }
      )
    this.Items$ = this.facade.getIbtRequestItems();
    this.InterBoutiqueTransferStatusRequestNotifications()
    this.facade.loadSelectedRequest({
      id: this.requestId,
      requestType: 'BTQ'
    });


    this.Items$.pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.items = data;
        if (this.items.length !== 0) {
          this.InterBoutiqueTransferStatusRequestNotifications();
        }
      }
      )

    this.facade.getIbt().pipe(takeUntil(this.destroy$))
      .subscribe(data => {


        if (data !== null) {
          this.facade.clearItemList();
          this.resetValue();

          if (data.status) {
            this.InterBoutiqueTransferStatusNotifications(data.status);
          }
        }
      });
    this.Items$.pipe(takeUntil(this.destroy$))
      .subscribe((items) => {


        if (this.approvalsTransferForm.value.selectItems === '1') {
          this.isSelectAll = true;
          this.selectionAllSubject.next({
            selectCheckbox: true,
            enableCheckbox: false
          });
        }
      }
      )
    this.id = this.requestId;
    this.loading$ = this.facade.getIsLoading();





    this.initialLoad();

    this.facade.getIsIbtRequestApprovalsItemsLoading().pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.isItemsLoading = data
      }
      );




    this.facade.getError().pipe(takeUntil(this.destroy$)).subscribe(data => {

      if (data) {
        this.errorHandler(data)
      }
    });

    this.facade.getHasUpdatingApprovalsFailure().pipe(takeUntil(this.destroy$)).subscribe(error => {

      if (error) {
        this.errorHandler(error)
      }
    });



  }

  initialLoad() {
    // this.facade.loadIbtRequestApprovalsItemsCount(this.requestId);
    this.facade.loadIbtRequestItems({
      id: this.id,
      requestType: RequestApprovalsAPITypesEnum.BTQ,
      pageIndex: this.pageIndex,
      pageSize: this.initailPageEvent.pageSize,
      isSelectedArray: []
    })
  }

  clearAll() {
    this.isSelectAll = false
    this.isSelectedArray = [];
    this.approvalsTransferForm.patchValue({
      selectItems: null
    });
    this.selectionAllSubject.next({
      selectCheckbox: false,
      enableCheckbox: true
    });
    this.InterBoutiqueTransferStatusRequestNotifications()

  }


  isSelect(event) {

    if (event.isSelected) {
      this.isSelectedArray = [...this.isSelectedArray, event.id]
    }
    else {
      const valueToRemove = event.id;
      //this.isSelectedArray=[...this.isSelectedArray.splice(this.isSelectedArray.indexOf(event.id), 1)];
      this.isSelectedArray = this.isSelectedArray.filter(item => item !== valueToRemove)

    }


    if (this.isSelectedArray.length === this.selectedArrayCount) {
      this.approvalsTransferForm.get('selectItems').setValue('1');

      this.selectChange();
    }

    if (this.isSelectedArray.length !== 0) {

      this.InterBoutiqueTransferStatusRequestNotifications();
    }
  }

  selectChange() {

    if (this.approvalsTransferForm.value.selectItems === '1') {

      this.selectAll();
    }
  }


  selectAll() {
    this.isSelectAll = true;
    this.selectionAllSubject.next({
      selectCheckbox: true,
      enableCheckbox: false
    });
    this.InterBoutiqueTransferStatusRequestNotifications();
  }




  errorHandler(error: CustomErrors) {
    this.hasNotification = true
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        this.hasNotification = false
        if(event.eventType===OverlayNotificationEventType.CLOSE){
          this.InterBoutiqueTransferStatusRequestNotifications()
        }
      });
  }

  InterBoutiqueTransferStatusRequestNotifications() {
    this.hasNotification = true;


    this.translate
      .get('pw.interBoutiqueTransferNotifications.acceptRejectMsg')
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.REQUEST_ACTION,
            message: translatedMessage,
            hasRemarks: true,
            isRemarksMandatory: true,
            hasClose: true
          })
          .events.subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === 1) {

              if (this.isSelectAll === false) {
                if (this.isSelectedArray.length !== 0) {


                  const acceptStatus = {
                    id: this.requestId,
                    requestType: 'BTQ',


                    requestUpdateDto:
                    {
                      itemIds: this.isSelectedArray,
                      remarks: event.data,
                      status: 'APPROVED'
                    }
                  };
                  this.facade.updateIbtSucess(
                    acceptStatus
                  );
                }


                else {


                  this.InterBoutiqueTransferErrorNotifications();

                }
              }
              else {

                const acceptStatus = {
                  id: this.requestId,
                  requestType: 'BTQ',


                  requestUpdateDto:
                  {
                    itemIds: [],
                    remarks: event.data,
                    status: 'APPROVED'
                  }
                };
                this.facade.updateIbtSucess(
                  acceptStatus
                );

              }






            }
            if (event.eventType === 2) {
              const rejectStatus = {
                id: this.requestId,
                requestType: 'BTQ',
                requestUpdateDto: {
                  itemIds: [],
                  remarks: event.data,
                  status: 'APVL_REJECTED'
                }


              };
              this.facade.updateIbtSucess(
                rejectStatus
              );


            }
          });
      });


  }



  InterBoutiqueTransferStatusNotifications(status: string) {
    this.hasNotification = true;
    const key =
      'pw.interBoutiqueTransferNotifications.statusResponseNotifications';
    let statusKey = {
      status: '',
      statusColor: ''
    };
    if (commonTranslateKeyMap.has(status)) {
      statusKey = commonTranslateKeyMap.get(status);
    }
    this.translate
      .get([key, statusKey.status])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            message:
              translatedMessages[key] +
              ' ' +
              translatedMessages[statusKey.status],
            hasBackdrop: true,
            hasClose: true
          })
          .events.pipe(takeUntil(this.destroy$)).subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.CLOSE) {
              this.back();
            }
          });
      });
  }

  InterBoutiqueTransferErrorNotifications() {
    const key =
      'pw.interBoutiqueTransferNotifications.selectItemsNotifications';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.SIMPLE,
            message: translatedMessage,
            hasBackdrop: true,
            hasClose: true
          })
          .events.subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.CLOSE) {
              this.InterBoutiqueTransferStatusRequestNotifications();
            }
          });
      });
  }

  resetValue() {
    this.isSelectedArray = [];
  }


  paginateItems(event: PageEvent) {

    this.itemsPageEvent = event;
    this.loadIbtApprovals();

  }

  loadIbtApprovals() {
    // this.facade.loadIbtRequestApprovalsItemsCount(this.requestId);
    this.facade.loadIbtRequestItems({
      id: this.id,
      requestType: RequestApprovalsAPITypesEnum.BTQ,
      pageIndex: this.itemsPageEvent.pageIndex,
      pageSize: this.itemsPageEvent.pageSize,
      isSelectedArray: this.isSelectedArray
    })

  }


  back() {
    this.router.navigate([
      '/inventory/approvals/IbtRequest'


    ]);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();

  }

}
