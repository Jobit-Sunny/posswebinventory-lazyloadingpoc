import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestApprovalsComponent } from './containers/request-approvals-list/request-approvals.component';
import { AuthGuard } from '@poss-web/core';
import { IbtRequestApprovalsDetailsComponent } from './containers/ibt-request-approvals-details/ibt-request-approvals-details.component';
import { OtherIssuesApprovalsDetailComponent } from './containers/other-issues-approvals-detail/other-issues-approvals-detail.component';

const routes: Routes = [
  {
    path: ':type',
    component: RequestApprovalsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'IbtRequest/:id',
    component: IbtRequestApprovalsDetailsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: ':type/:id',
    component: OtherIssuesApprovalsDetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'BinRequest'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestApprovalsRoutingModule {}
