import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoadBinRequestResponse } from '../models/bin-reuest-approvals.model';
import {
  getBinRequestApprovalsUrl,
  getLocationsUrl,
  getAllBinRequestApprovalsCountUrl,
  getLocationCountUrl,
  updateBinRequestApprovalsCountUrl,
  getIbtsApprovalsUrl,
  getAllIBTRequestApprovalsCountUrl,
  getRequestByIdEndpointUrl,
  getIbtRequestApprovalsUrl,
  getAllIBTItemsRequestApprovalsCountUrl,
  updateIbtRequestApprovalsCountUrl,
  updateIbtApprovalsCountUrl,
  getAdjustmentApprovalsCountUrl,
  getLossApprovalsCountUrl,
  getLoanApprovalsCountUrl,
  getFocApprovalsCountUrl,
  getPsvApprovalsCountUrl,
  getExhibitionApprovalsCountUrl
} from '../../endpoints.constants';
import { map, mergeMap } from 'rxjs/operators';
import { BinRequestApprovalsItemHelper } from '../helpers/bin-request-approvals-Item.helper';
import { ApiService } from '@poss-web/core';
import { HttpClient } from '@angular/common/http';
import * as BinRequestApprovalsActions from '../+state/request-approvals.actions';
import { IbtRequestApprovalsItemHelper } from '../helpers/request-approvals-items.helper';
import {
  RequestApprovals,
  LoadRequestResponseItems
} from '../models/request-approvals-items.model';
import { RequestApprovalsItemsAdaptor } from '../adaptors/request-approvals-items.adaptor';
import { IbtApprovalsItemsHelper } from '../helpers/approval-Item.helper';
import { LoadRequestTotalCountSuccessPayload } from '../models/other-issues-request.model';
import { SelectedStockPayload } from '../+state/request-approvals.actions';

@Injectable()
export class RequestApprovalsService {
  getBinRequestApprovalsItems(
    locationCode: string,
    reqDocNo: number,
    pageIndex: number,
    pageSize: number
  ): Observable<LoadBinRequestResponse> {
    const url = getBinRequestApprovalsUrl(
      locationCode,
      reqDocNo,
      pageIndex,
      pageSize
    );
    return this.apiService
      .get(url)
      .pipe(map((data: any) => BinRequestApprovalsItemHelper.getItems(data)));
  }

  getIbtRequestApprovalsItems(
    id: number,
    requestType: string,
    pageIndex: number,
    pageSize: number,
    isSelectedData: string[]
  ): Observable<LoadRequestResponseItems> {
    const url = getIbtRequestApprovalsUrl(id, requestType, pageIndex, pageSize);
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) =>
          IbtApprovalsItemsHelper.getItems(data, isSelectedData)
        )
      );
  }

  getLocation(pageIndex: number, pageSize: number): Observable<Location[]> {
    const url = getLocationsUrl(pageIndex, pageSize);
    return this.apiService.get(url).pipe(map((data: any) => data.results));
  }

  getBinRequestApprovalsCount(): Observable<any> {
    const url = getAllBinRequestApprovalsCountUrl();

    return this.apiService
      .get(url)
      .pipe(map((data: any) => data.totalElements));
  }

  getIbtRequestApprovalsCount(): Observable<any> {
    const url = getAllIBTRequestApprovalsCountUrl();

    return this.apiService
      .get(url)
      .pipe(map((data: any) => data.totalElements));
  }

  getIbtItemsRequestApprovalsCount(
    payload: BinRequestApprovalsActions.CountPayload
  ): Observable<any> {
    const url = getAllIBTItemsRequestApprovalsCountUrl(payload);

    return this.apiService
      .get(url)
      .pipe(map((data: any) => data.totalElements));
  }

  getLocationCount(): Observable<any> {
    const url = getLocationCountUrl();

    return this.apiService
      .get(url)
      .pipe(map((data: any) => data.totalElements));
  }

  updateBinApprovalStatus(
    binRequestApprovalDto: BinRequestApprovalsActions.BinApprovalspayload
  ) {
    const url = updateBinRequestApprovalsCountUrl(binRequestApprovalDto.id);
    return this.apiService
      .patch(url, binRequestApprovalDto.binRequestUpdateDto)
      .pipe(map(data => data));
  }

  updateIbtApprovalStatus(
    ibtRequestApprovalDto: BinRequestApprovalsActions.IbtApprovalspayload
  ) {
    const url = updateIbtRequestApprovalsCountUrl(
      ibtRequestApprovalDto.id,

      ibtRequestApprovalDto.itemId
    );
    return this.apiService
      .patch(url, ibtRequestApprovalDto.itemUpdateDto)
      .pipe(map(data => data));
  }

  updateIbtApprovals(
    ibtRequestApprovalDto: BinRequestApprovalsActions.Ibtpayload
  ) {
    const url = updateIbtApprovalsCountUrl(
      ibtRequestApprovalDto.id,
      ibtRequestApprovalDto.requestType
    );
    return this.apiService
      .patch(url, ibtRequestApprovalDto.requestUpdateDto)
      .pipe(map(data => data));
  }

  getIbtsApprovals(locationCode, reqDocNo, type, pageIndex, pageSize) {
    const url = getIbtsApprovalsUrl(
      locationCode,
      reqDocNo,
      type,
      pageIndex,
      pageSize
    );

    return this.apiService
      .get(url)
      .pipe(map((data: any) => IbtRequestApprovalsItemHelper.getItems(data)));
  }

  getRequest(payload: SelectedStockPayload): Observable<RequestApprovals> {
    const url = getRequestByIdEndpointUrl(payload.id, payload.requestType);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => RequestApprovalsItemsAdaptor.fromJson(data)));
  }

  getRequestsCount(): Observable<LoadRequestTotalCountSuccessPayload> {
    const adjCountUrl = getAdjustmentApprovalsCountUrl();
    const lossCountUrl = getLossApprovalsCountUrl();
    const loanCountUrl = getLoanApprovalsCountUrl();
    const focCountUrl = getFocApprovalsCountUrl();
    const exhCountUrl = getExhibitionApprovalsCountUrl();
    const psvCountUrl = getPsvApprovalsCountUrl();

    return this.apiService
      .get(adjCountUrl)
      .pipe(map((data: any) => data.totalElements))
      .pipe(
        mergeMap(adjRequestCount =>
          this.apiService
            .get(lossCountUrl)
            .pipe(map((data: any) => data.totalElements))
            .pipe(
              mergeMap(lossRequestCount =>
                this.apiService
                  .get(loanCountUrl)
                  .pipe(map((data: any) => data.totalElements))
                  .pipe(
                    mergeMap(loanRequestCount =>
                      this.apiService
                        .get(focCountUrl)
                        .pipe(map((data: any) => data.totalElements))
                        .pipe(
                          mergeMap(focRequestCount =>
                            this.apiService
                              .get(exhCountUrl)
                              .pipe(map((data: any) => data.totalElements))
                              .pipe(
                                mergeMap(exhRequestCount =>
                                  this.apiService.get(psvCountUrl).pipe(
                                    map((data: any) => ({
                                      adjRequestCount: adjRequestCount,
                                      lossRequestCount: lossRequestCount,
                                      loanRequestCount: loanRequestCount,
                                      focRequestCount: focRequestCount,
                                      exhRequestCount: exhRequestCount,
                                      psvRequestCount: data.totalElements
                                    }))
                                  )
                                )
                              )
                          )
                        )
                    )
                  )
              )
            )
        )
      );
  }

  constructor(private apiService: ApiService, private http: HttpClient) {}
}
