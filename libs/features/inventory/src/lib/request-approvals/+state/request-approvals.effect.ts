import * as RequestActions from './request-approvals.actions';
import { Injectable } from '@angular/core';
import { DataPersistence } from '@nrwl/angular';
import {
  NotificationService,
  CustomErrors,
  CustomErrorAdaptor,
  LocationDataService
} from '@poss-web/core';
import { Effect } from '@ngrx/effects';
import { RequestApprovalsState } from './request-approvals.state';
import { map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { LoadBinRequestResponse } from '../models/bin-reuest-approvals.model';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
  LoadRequestResponse,
  RequestApprovals,
  LoadRequestResponseItems
} from '../models/request-approvals-items.model';
import { RequestApprovalsService } from '../services/request-approvals.service';
@Injectable()
export class RequestApprovalsEffects {
  constructor(
    private service: RequestApprovalsService,
    private dataPersistence: DataPersistence<any>,
    private notificationService: NotificationService,
    private locationService: LocationDataService
  ) {}

  @Effect()
  binCount$ = this.dataPersistence.fetch(
    RequestActions.RequestApprovalsActionTypes.LOAD_BINREQUESTAPPROVALS_COUNT,
    {
      // provides an action and the current state of the store
      run: (
        action: RequestActions.LoadBinRequestApprovalsCount,
        state: RequestApprovalsState
      ) => {
        return this.service
          .getBinRequestApprovalsCount()
          .pipe(
            map(
              (data: any) =>
                new RequestActions.LoadBinRequestApprovalsCountSuccess(data)
            )
          );
      },

      onError: (
        action: RequestActions.LoadBinRequestApprovalsCount,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.LoadBinRequestApprovalsCountFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  requestCount$ = this.dataPersistence.fetch(
    RequestActions.RequestApprovalsActionTypes.LOAD_ItEMS_COUNT,
    {
      // provides an action and the current state of the store
      run: (
        action: RequestActions.LoadItemsTotalCount,
        state: RequestApprovalsState
      ) => {
        return this.service
          .getRequestsCount()
          .pipe(
            map(
              (data: any) => new RequestActions.LoadItemsTotalCountSuccess(data)
            )
          );
      },

      onError: (
        action: RequestActions.LoadItemsTotalCount,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.LoadItemsTotalCountFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  locationCount$ = this.dataPersistence.fetch(
    RequestActions.RequestApprovalsActionTypes.LOAD_LOCATION_COUNT,
    {
      // provides an action and the current state of the store
      run: (
        action: RequestActions.LoadLocationCount,
        state: RequestApprovalsState
      ) => {
        return this.service
          .getLocationCount()
          .pipe(
            map(
              (data: any) => new RequestActions.LoadLocationCountSuccess(data)
            )
          );
      },

      onError: (
        action: RequestActions.LoadLocationCount,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.LoadLocationCountFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  ibtRequestCount$ = this.dataPersistence.fetch(
    RequestActions.RequestApprovalsActionTypes.LOAD_IBTREQUESTAPPROVALS_COUNT,
    {
      // provides an action and the current state of the store
      run: (
        action: RequestActions.LoadIBTRequestApprovalsCount,
        state: RequestApprovalsState
      ) => {
        return this.service
          .getIbtRequestApprovalsCount()
          .pipe(
            map(
              (data: any) =>
                new RequestActions.LoadIBTRequestApprovalsCountSuccess(data)
            )
          );
      },

      onError: (
        action: RequestActions.LoadIBTRequestApprovalsCount,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.LoadIBTRequestApprovalsCountFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  location$ = this.dataPersistence.fetch(
    RequestActions.RequestApprovalsActionTypes.LOAD_LOCATION,
    {
      run: (action: RequestActions.LoadLocation) => {
        return this.locationService
          .getLocations(null, null, null, null, false, null)
          .pipe(
            map((items: any) => new RequestActions.LoadLocationSuccess(items))
          );
      },
      onError: (
        action: RequestActions.LoadLocation,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.LoadLocationFailure(this.errorHandler(error));
      }
    }
  );
  @Effect()
  loadBinItems$ = this.dataPersistence.fetch(
    RequestActions.RequestApprovalsActionTypes.LOAD_BINREQUESTAPPROVALS,
    {
      run: (action: RequestActions.LoadBinRequestApprovals) => {
        return this.service
          .getBinRequestApprovalsItems(
            action.payload.locationCode,
            action.payload.reqDocNo,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (items: LoadBinRequestResponse) =>
                new RequestActions.LoadBinRequestApprovalsSuccess(items)
            )
          );
      },
      onError: (
        action: RequestActions.LoadBinRequestApprovals,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.LoadBinRequestApprovalsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  updateBinApprovalStatus = this.dataPersistence.pessimisticUpdate(
    RequestActions.RequestApprovalsActionTypes.UPDATE_BINREQUESTAPPROVALS,
    {
      run: (action: RequestActions.UpdateBinRequestApprovals) => {
        return this.service
          .updateBinApprovalStatus(action.payload)
          .pipe(
            map(
              (data: any) =>
                new RequestActions.UpdateBinRequestApprovalsSuccess(data)
            )
          );
      },

      onError: (
        action: RequestActions.UpdateBinRequestApprovals,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.UpdateBinRequestApprovalsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  updateIbtApprovalStatus = this.dataPersistence.pessimisticUpdate(
    RequestActions.RequestApprovalsActionTypes.UPDATE_IBTREQUESTAPPROVALSITEMS,
    {
      run: (action: RequestActions.UpdateIBTRequestApprovals) => {
        return this.service
          .updateIbtApprovalStatus(action.payload)
          .pipe(
            map(
              (data: any) =>
                new RequestActions.UpdateIbtRequestApprovalsSuccess(data)
            )
          );
      },

      onError: (
        action: RequestActions.UpdateIBTRequestApprovals,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.UpdateIbtRequestApprovalsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  IbtApprovalStatus = this.dataPersistence.pessimisticUpdate(
    RequestActions.RequestApprovalsActionTypes.IBTREQUESTAPPROVALS,
    {
      run: (action: RequestActions.IBTRequest) => {
        return this.service
          .updateIbtApprovals(action.payload)
          .pipe(map((data: any) => new RequestActions.IbtRequestSuccess(data)));
      },

      onError: (
        action: RequestActions.IBTRequest,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.IbtRequestFailure(this.errorHandler(error));
      }
    }
  );

  @Effect() loadIbtRequest$: Observable<Action> = this.dataPersistence.fetch(
    RequestActions.RequestApprovalsActionTypes.LOAD_IBT_REQUEST,
    {
      run: (action: RequestActions.LoadIBtRequest) => {
        return this.service
          .getIbtsApprovals(
            action.payload.reqLocationCode,
            action.payload.reqDocNo,
            action.payload.requestType,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (ibt: LoadRequestResponse) =>
                new RequestActions.LoadIBtRequestSuccess(ibt)
            )
          );
      },

      onError: (
        action: RequestActions.LoadIBtRequest,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.LoadIBtRequestFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadFOCRequest$: Observable<Action> = this.dataPersistence.fetch(
    RequestActions.RequestApprovalsActionTypes.LOAD_FOC_REQUEST,
    {
      run: (action: RequestActions.LoadFOCRequest) => {
        return this.service
          .getIbtsApprovals(
            action.payload.reqLocationCode,
            action.payload.reqDocNo,
            action.payload.requestType,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (ibt: LoadRequestResponse) =>
                new RequestActions.LoadFOCRequestSuccess(ibt)
            )
          );
      },

      onError: (
        action: RequestActions.LoadFOCRequest,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.LoadFOCRequestFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadPSVRequest$: Observable<Action> = this.dataPersistence.fetch(
    RequestActions.RequestApprovalsActionTypes.LOAD_PSV_REQUEST,
    {
      run: (action: RequestActions.LoadPSVRequest) => {
        return this.service
          .getIbtsApprovals(
            action.payload.reqLocationCode,
            action.payload.reqDocNo,
            action.payload.requestType,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (ibt: LoadRequestResponse) =>
                new RequestActions.LoadPSVRequestSuccess(ibt)
            )
          );
      },

      onError: (
        action: RequestActions.LoadPSVRequest,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.LoadPSVRequestFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadLOSSRequest$: Observable<Action> = this.dataPersistence.fetch(
    RequestActions.RequestApprovalsActionTypes.LOAD_LOSS_REQUEST,
    {
      run: (action: RequestActions.LoadLOSSRequest) => {
        return this.service
          .getIbtsApprovals(
            action.payload.reqLocationCode,
            action.payload.reqDocNo,
            action.payload.requestType,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (ibt: LoadRequestResponse) =>
                new RequestActions.LoadLOSSRequestSuccess(ibt)
            )
          );
      },

      onError: (
        action: RequestActions.LoadLOSSRequest,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.LoadLOSSRequestFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadLOANRequest$: Observable<Action> = this.dataPersistence.fetch(
    RequestActions.RequestApprovalsActionTypes.LOAD_LOAN_REQUEST,
    {
      run: (action: RequestActions.LoadLOANRequest) => {
        return this.service
          .getIbtsApprovals(
            action.payload.reqLocationCode,
            action.payload.reqDocNo,
            action.payload.requestType,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (ibt: LoadRequestResponse) =>
                new RequestActions.LoadLOANRequestSuccess(ibt)
            )
          );
      },

      onError: (
        action: RequestActions.LoadLOANRequest,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.LoadLOANRequestFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadEXHRequest$: Observable<Action> = this.dataPersistence.fetch(
    RequestActions.RequestApprovalsActionTypes.LOAD_EXH_REQUEST,
    {
      run: (action: RequestActions.LoadEXHRequest) => {
        return this.service
          .getIbtsApprovals(
            action.payload.reqLocationCode,
            action.payload.reqDocNo,
            action.payload.requestType,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (ibt: LoadRequestResponse) =>
                new RequestActions.LoadEXHRequestSuccess(ibt)
            )
          );
      },

      onError: (
        action: RequestActions.LoadEXHRequest,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.LoadEXHRequestFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadADJRequest$: Observable<Action> = this.dataPersistence.fetch(
    RequestActions.RequestApprovalsActionTypes.LOAD_ADJ_REQUEST,
    {
      run: (action: RequestActions.LoadADJRequest) => {
        return this.service
          .getIbtsApprovals(
            action.payload.reqLocationCode,
            action.payload.reqDocNo,
            action.payload.requestType,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (ibt: LoadRequestResponse) =>
                new RequestActions.LoadADJRequestSuccess(ibt)
            )
          );
      },

      onError: (
        action: RequestActions.LoadADJRequest,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.LoadADJRequestFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  // @Effect()
  // ibtCount$ = this.dataPersistence.fetch(RequestActions.RequestApprovalsActionTypes.LOAD_IBTREQUESTAPPROVALS_COUNT, {
  //   // provides an action and the current state of the store
  //   run: (action: RequestActions.LoadIBTRequestApprovalsCount, state: RequestApprovalsState) => {
  //     return this.service
  //       .getIbtRequestApprovalsCount()
  //       .pipe(map((data: any) => new RequestActions.LoadIBTRequestApprovalsCountSuccess(data)));
  //   },

  //   onError: (action: RequestActions.LoadIBTRequestApprovalsCount, error: HttpErrorResponse) => {
  //     return new RequestActions.LoadIBTRequestApprovalsCountFailure(this.errorHandler(error))
  //   }
  // });

  @Effect() loadSeletedRequest$ = this.dataPersistence.fetch(
    RequestActions.RequestApprovalsActionTypes.LOAD_SELECTED_REQUEST,
    {
      run: (action: RequestActions.LoadSelectedRequest) => {
        return this.service
          .getRequest(action.payload)
          .pipe(
            map(
              (data: RequestApprovals) =>
                new RequestActions.LoadSelectedRequestSuccess(data)
            )
          );
      },

      onError: (
        action: RequestActions.LoadSelectedRequest,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.LoadSelectedRequestFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadIbtItems$ = this.dataPersistence.fetch(
    RequestActions.RequestApprovalsActionTypes.LOAD_IBTREQUESTAPPROVALS,
    {
      run: (action: RequestActions.LoadIbtRequestApprovals) => {
        return this.service
          .getIbtRequestApprovalsItems(
            action.payload.id,
            action.payload.requestType,
            action.payload.pageIndex,
            action.payload.pageSize,
            action.payload.isSelectedArray
          )
          .pipe(
            map(
              (items: LoadRequestResponseItems) =>
                new RequestActions.LoadIbtRequestApprovalsSuccess(items)
            )
          );
      },
      onError: (
        action: RequestActions.LoadIbtRequestApprovals,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.LoadIbtRequestApprovalsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  ibtItemsCount$ = this.dataPersistence.fetch(
    RequestActions.RequestApprovalsActionTypes
      .LOAD_IBTREQUESTITEMSAPPROVALS_COUNT,
    {
      // provides an action and the current state of the store
      run: (
        action: RequestActions.LoadIBTRequestApprovalsItemsCount,
        state: RequestApprovalsState
      ) => {
        return this.service
          .getIbtItemsRequestApprovalsCount(action.payload)
          .pipe(
            map(
              (data: any) =>
                new RequestActions.LoadIBTRequestApprovalsItemsCountSuccess(
                  data
                )
            )
          );
      },

      onError: (
        action: RequestActions.LoadIBTRequestApprovalsItemsCount,
        error: HttpErrorResponse
      ) => {
        return new RequestActions.LoadIBTRequestApprovalsItemsCountFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
}
