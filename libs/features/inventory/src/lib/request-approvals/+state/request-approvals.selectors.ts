import { createSelector } from "@ngrx/store";
import { selectInventory } from "../../inventory.state";
import { itemSelector, ibtRequestSelector, ibtRequestItemSelector } from './request-approvals.entity';

const selectError = createSelector(
  selectInventory,
  state => state.binRequestApprovals.error
);

const selectIsLoading = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isLoading
);


const selectBinRequestItemsCount = createSelector(
  selectInventory,
  state => state.binRequestApprovals.binRequestItemsCount
);

const selectIbtRequestApprovalItemsCount = createSelector(
  selectInventory,
  state => state.binRequestApprovals.ibtRequestItemsCount

);

const selectfocRequestCount = createSelector(
  selectInventory,
  state => state.binRequestApprovals.focRequestItemsCount
);

const selectpsvRequestCount = createSelector(
  selectInventory,
  state => state.binRequestApprovals.psvRequestItemsCount
);

const selectexhRequestCount = createSelector(
  selectInventory,
  state => state.binRequestApprovals.exhRequestItemsCount
);
const selectlossRequestItemsCount = createSelector(
  selectInventory,
  state => state.binRequestApprovals.lossRequestItemsCount
);
const selectloanRequestCount = createSelector(
  selectInventory,
  state => state.binRequestApprovals.loanRequestItemsCount
);
const selectadjRequestCount = createSelector(
  selectInventory,
  state => state.binRequestApprovals.adjRequestItemsCount
);
const selectIbtRequestItemsCount = createSelector(
  selectInventory,
  state => state.binRequestApprovals.ibtRequestApprovalsItemsCount
);


const selectLocationCount = createSelector(
  selectInventory,
  state => state.binRequestApprovals.locationCount
);
const selectIsBinRequestItemsReset = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isBinRequestItemsReset
);

const selectIsBinRequestItemsCountReset = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isBinRequestItemsCountReset
);

const selectIsIbtRequestItemsReset = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isIbtRequestItemsReset
);

const selectIsIbtRequestItemsCountReset = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isIbtRequestItemsCountReset
);

const selectIsFocRequestItemsReset = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isfocRequestItemsReset
);

const selectIsFocRequestItemsCountReset = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isfocRequestItemsCountReset
);

const selectIsPsvRequestItemsReset = createSelector(
  selectInventory,
  state => state.binRequestApprovals.ispsvRequestItemsReset
);

const selectIsPsvRequestItemsCountReset = createSelector(
  selectInventory,
  state => state.binRequestApprovals.ispsvRequestItemsCountReset
);
const selectIslossRequestItemsReset = createSelector(
  selectInventory,
  state => state.binRequestApprovals.islossRequestItemsReset
);

const selectIslossRequestItemsCountReset = createSelector(
  selectInventory,
  state => state.binRequestApprovals.islossRequestItemsCountReset
);
const selectIsloanRequestItemsReset = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isloanRequestItemsReset
);

const selectIsadjRequestItemsCountReset = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isadjRequestItemsCountReset
);
const selectIsadjRequestItemsReset = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isadjRequestItemsReset
);

const selectIsloanRequestItemsCountReset = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isloanRequestItemsCountReset
);
const selectIsexhRequestItemsReset = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isexhRequestItemsReset
);

const selectIsexhRequestItemsCountReset = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isexhRequestItemsCountReset
);

const binRequestApprovalsItems = createSelector(
  selectInventory,
  state => state.binRequestApprovals.binRequestApprovalsItem
);

const selectIsbinRequestItemsLoading = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isbinRequestItemsLoading
);

const selectIsIbtRequestItemsLoading = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isibtRequestItemsLoading
);

const selectIsLocationLoading = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isLocationLoading
);

const selectIsIbtLoading = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isLoadingIbtRequest
);

const selectIsExhLoading = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isLoadingexhRequest
);

const selectIsadjLoading = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isLoadingadjRequest
);

const selectIslossLoading = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isLoadinglossequest
);

const selectIsloanLoading = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isLoadingloanRequest
);

const selectIspsvLoading = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isLoadingpsvRequest
);

const selectIsfocLoading = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isLoadingfocRequest
);

const selectLocation = createSelector(
  selectInventory,
  state => state.binRequestApprovals.location
);



const selectBinItems = createSelector(
  binRequestApprovalsItems,
  itemSelector.selectAll
);

const selectHasUpdatingFailure = createSelector(
  selectInventory,
  state => state.binRequestApprovals.hasUpdatingFailure
);
const selectIsUpdatingItem = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isUpdatingItemSuccess
);




const updateItemSuccess = createSelector(
  selectInventory,
  state => state.binRequestApprovals.binRequestApproval
);

const selectHasUpdatingIbtFailure = createSelector(
  selectInventory,
  state => state.binRequestApprovals.hasUpdatingIbtFailure
);
const selectIsUpdatingIbt = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isUpdatingIbtSuccess
);




const IbtApprovalsSuccess = createSelector(
  selectInventory,
  state => state.binRequestApprovals.ibtUpdateRequest
);

const selectHasUpdatingApprovalsFailure = createSelector(
  selectInventory,
  state => state.binRequestApprovals.hasUpadatingApprovalsFailure
);
const selectIsUpdatingSuccess = createSelector(
  selectInventory,
  state => state.binRequestApprovals.isUpdatingSuccess
);




const updateIbtSuccess = createSelector(
  selectInventory,
  state => state.binRequestApprovals.ibtRequestApproval
);


const ibtRequest = createSelector(
  selectInventory,
  state => state.binRequestApprovals.ibtRequest
);

const selectIbtRequest = createSelector(
  ibtRequest,
  ibtRequestSelector.selectAll
);

const adjRequest = createSelector(
  selectInventory,
  state => state.binRequestApprovals.adjRequest
);

const selectadjRequest = createSelector(
  adjRequest,
  ibtRequestSelector.selectAll
);

const psvRequest = createSelector(
  selectInventory,
  state => state.binRequestApprovals.psvRequest
);

const selectpsvRequest = createSelector(
  psvRequest,
  ibtRequestSelector.selectAll
);
const lossRequest = createSelector(
  selectInventory,
  state => state.binRequestApprovals.lossRequest
);

const selectlossRequest = createSelector(
  lossRequest,
  ibtRequestSelector.selectAll
);
const loanRequest = createSelector(
  selectInventory,
  state => state.binRequestApprovals.loanRequest
);

const selectloanRequest = createSelector(
  loanRequest,
  ibtRequestSelector.selectAll
);
const exhRequest = createSelector(
  selectInventory,
  state => state.binRequestApprovals.exhRequest
);

const selectexhRequest = createSelector(
  exhRequest,
  ibtRequestSelector.selectAll
);
const focRequest = createSelector(
  selectInventory,
  state => state.binRequestApprovals.focRequest
);

const selectfocRequest = createSelector(
  focRequest,
  ibtRequestSelector.selectAll
);

const ibtRequestApprovalsItems = createSelector(
  selectInventory,
  state => state.binRequestApprovals.ibtRequestApprovalsItem
);

const selectIbtRequestItems = createSelector(
  ibtRequestApprovalsItems,
  ibtRequestItemSelector.selectAll
);

const selectSelectedRequest = createSelector(
  selectInventory,
  state => state.binRequestApprovals.selectedRequest
);




export const RequestApprovalsSelectors = {
  selectError,
  selectIsLoading,
  selectBinRequestItemsCount,
  selectIsBinRequestItemsReset,
  selectIsBinRequestItemsCountReset,
  binRequestApprovalsItems,
  selectIsbinRequestItemsLoading,
  selectBinItems,
  updateItemSuccess,
  selectIsUpdatingItem,
  selectHasUpdatingFailure,
  selectIsLocationLoading,
  selectLocation,
  selectLocationCount,
  ibtRequest,
  selectIbtRequest,
  selectIsIbtLoading,
  selectIbtRequestItemsCount,
  selectIsIbtRequestItemsCountReset,
  selectIsIbtRequestItemsReset,
  selectSelectedRequest,
  selectIbtRequestItems,
  ibtRequestApprovalsItems,
  selectIbtRequestApprovalItemsCount,
  selectIsIbtRequestItemsLoading,
  selectHasUpdatingIbtFailure,
  selectIsUpdatingIbt,
  updateIbtSuccess,
  IbtApprovalsSuccess,
  selectHasUpdatingApprovalsFailure,
  selectIsUpdatingSuccess,
  selectIsFocRequestItemsReset,
  selectfocRequestCount,
  selectpsvRequestCount,
  selectexhRequestCount,
  selectlossRequestItemsCount,
  selectloanRequestCount,
  selectadjRequestCount,
  selectIsFocRequestItemsCountReset,
  selectIsPsvRequestItemsReset,
  selectIsPsvRequestItemsCountReset,
  selectIslossRequestItemsReset,
  selectIslossRequestItemsCountReset,
  selectIsloanRequestItemsReset,
  selectIsadjRequestItemsCountReset,
  selectIsadjRequestItemsReset,
  selectIsloanRequestItemsCountReset,
  selectIsexhRequestItemsReset,
  selectIsexhRequestItemsCountReset,
  selectadjRequest,
  selectpsvRequest,
  selectlossRequest,
  selectloanRequest,
  selectexhRequest,
  selectfocRequest,
  selectIsfocLoading,
  selectIspsvLoading,
  selectIslossLoading,
  selectIsloanLoading,
  selectIsadjLoading,
  selectIsExhLoading


















}
