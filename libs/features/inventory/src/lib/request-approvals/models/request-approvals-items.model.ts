
import { Moment } from 'moment';

export interface RequestApprovals {
  id: number,
  reqDocNo: number,
  srcLocationCode: string,
  destLocationCode: string,
  totalAcceptedQuantity: number,
  totalAcceptedValue: number,
  totalAcceptedWeight: number,
  totalRequestedWeight: number,
  totalRequestedQuantity: number,
  totalRequestedValue: number,
  weightUnit: string,
  currencyCode: string,

  status: string,
  reqDocDate: Moment,
  requestType: string,
  otherDetails: {
    type: string,
    data: {
      approvedCode: string,
      approvedBy: string
    }

  },
  carrierDetails: {
    type: string,
    data: {
      employeeName: string,
      employeeId: string,
      emailId: string
    }
  },
}



export interface LoadRequestResponse {
  count: number;
  items: RequestApprovals[];
}

export interface RequestApprovalsItems {
  isSelected: boolean,
  id: string,
  itemCode: string,
  lotNumber: string,
  mfgDate: Moment,
  productCategory: string,
  productGroup: string,
  binCode: string,
  binGroupCode: string,
  stdValue: number,
  stdWeight: number,
  currencyCode: string,
  weightUnit: string,
  status: string,
  imageURL: string,
  requestedQuantity: number,
  acceptedQuantity: number,
  approvedQuantity: number,
  availableQuantity: number,
  inventoryId: string,
  totalApprovedQuantity: number
}

export interface LoadRequestResponseItems {
  count: number,
  items: RequestApprovalsItems[]
}

export interface ApprovalUpdatePayload {
  quantity: number;
  status: string;
}
