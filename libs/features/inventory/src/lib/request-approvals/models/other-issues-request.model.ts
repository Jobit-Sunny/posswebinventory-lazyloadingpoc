export interface LoadRequestTotalCountSuccessPayload {
  adjRequestCount: number;
  psvRequestCount: number;
  focRequestCount: number;
  exhRequestCount: number;
  lossRequestCount: number;
  loanRequestCount: number;

}
