import { Moment } from 'moment';

export interface BinRequestApprovalsItems {
  binName: string;
  id: number;
  reqLocationCode: string;
  reqDocDate: Moment;
  reqDocNo: number;
  status: string;
  requestedRemarks: string;
}

export interface LoadBinRequestResponse {
  count: number;
  items: BinRequestApprovalsItems[];
}


export interface BinApprovals {
  remarks: string,
  status: string

}

export interface Location {
  locationCode: string,
  address: string
}
