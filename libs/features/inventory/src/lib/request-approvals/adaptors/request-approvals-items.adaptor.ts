

import { RequestApprovals } from '../models/request-approvals-items.model';
import * as moment from 'moment';

export class RequestApprovalsItemsAdaptor {
  static fromJson(item: any): RequestApprovals {


    return {
      id: item.id,
      reqDocNo: item.reqDocNo,
      srcLocationCode: item.srcLocationCode,
      destLocationCode: item.destLocationCode,
      totalAcceptedQuantity: item.totalAcceptedQuantity?  item.totalAcceptedQuantity:0,
      totalAcceptedValue:item.totalAcceptedValue?item.totalAcceptedValue:0,
      totalAcceptedWeight:item.totalAcceptedWeight?item.totalAcceptedWeight:0,
      totalRequestedQuantity: item.totalRequestedQuantity?  item.totalRequestedQuantity:0,
      totalRequestedValue:item.totalRequestedValue?item.totalRequestedValue:0,
      totalRequestedWeight:item.totalRequestedWeight?item.totalRequestedWeight:0,
      status: item.status,
      reqDocDate: moment(item.reqDocDate),
      requestType: item.requestType,
      weightUnit: item.weightUnit,
      currencyCode: item.currencyCode,


      otherDetails: item.otherDetails
        ? {
          type: item.otherDetails['type'],
          data: item.otherDetails['data']
        }
        : {
          type: null,
          data: null

        },
      carrierDetails: item.carrierDetails
        ? {
          type: item.carrierDetails['type'],
          data: item.carrierDetails['data']
        }
        : {
          type: null,
          data: null

        }
    }
  }
}
