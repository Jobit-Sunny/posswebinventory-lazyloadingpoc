import * as moment from 'moment';
import { RequestApprovalsItems } from '../models/request-approvals-items.model';

export class ApprovalsItemAdaptor {
  static fromJson(item: any, isSelectedData): RequestApprovalsItems {
    return {
      id: item.id,
      binCode: item.binCode,
      itemCode: item.itemCode,
      stdValue: item.stdValue,
      stdWeight: item.stdWeight,
      requestedQuantity: item.requestedQuantity ? item.requestedQuantity : 0,
      acceptedQuantity: item.acceptedQuantity ? item.acceptedQuantity : 0,
      approvedQuantity: item.approvedQuantity ? item.approvedQuantity : 0,
      availableQuantity: item.availableQuantity ? item.availableQuantity : 0,
      inventoryId: item.inventoryId,
      totalApprovedQuantity: item.totalApprovedQuantity
        ? item.totalApprovedQuantity
        : 0,

      lotNumber: item.lotNumber,
      mfgDate: moment(item.mfgDate),
      status: item.status,

      currencyCode: item.currencyCode,
      weightUnit: item.weightUnit,
      imageURL: item.imageURL,

      binGroupCode: item.binGroupCode,

      productCategory: item.productCategory,
      productGroup: item.productGroup,
      isSelected: isSelectedData.includes(item.id)
    };
  }
}
