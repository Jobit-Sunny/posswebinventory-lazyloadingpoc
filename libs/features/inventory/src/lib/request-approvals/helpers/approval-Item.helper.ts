import {
  LoadRequestResponseItems,
  RequestApprovalsItems
} from '../models/request-approvals-items.model';
import { ApprovalsItemAdaptor } from '../adaptors/approvals-Item.adaptor';

export class IbtApprovalsItemsHelper {
  static getItems(data: any, isSelectedData): LoadRequestResponseItems {
    const items: RequestApprovalsItems[] = [];
    for (const item of data.results) {
      items.push(ApprovalsItemAdaptor.fromJson(item, isSelectedData));
    }
    return {
      count: data.totalElements ? data.totalElements : 0,
      items: items
    };
  }
}
