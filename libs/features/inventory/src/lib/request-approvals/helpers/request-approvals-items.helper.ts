import { LoadRequestResponse, RequestApprovals } from '../models/request-approvals-items.model';
import { RequestApprovalsItemsAdaptor } from '../adaptors/request-approvals-items.adaptor';

export class IbtRequestApprovalsItemHelper {

  static getItems(data: any): LoadRequestResponse {
    const items: RequestApprovals[] = [];
    for (const item of data.results) {
      items.push(RequestApprovalsItemsAdaptor.fromJson(item));
    }
    return {

      count: data.totalElements ? data.totalElements : 0,
      items: items
    }
  }
}
