import { BinRequestApprovalsItems, LoadBinRequestResponse } from '../models/bin-reuest-approvals.model';
import { BinRequestApprovalsItemsAdaptor } from '../adaptors/bin-Request-Approvals-Items.adaptor';

export class BinRequestApprovalsItemHelper {

  static getItems(data: any): LoadBinRequestResponse {
    const items: BinRequestApprovalsItems[] = [];
    for (const item of data.results) {
      items.push(BinRequestApprovalsItemsAdaptor.fromJson(item));
    }
    return {

      count: data.totalElements ? data.totalElements : 0,
      items: items
    }
  }
}
