export enum RequestApprovalsEnum {
  BIN_COR = 'BinRequest',
  IBT_COR = 'IbtRequest',
  Other_Issues = 'OtherIssuesRequest_ADJ',
  BTQ = 'BTQ',
  LOAN = 'OtherIssuesRequest_LOAN',
  EXH = 'OtherIssuesRequest_EXH',
  FOC = 'OtherIssuesRequest_FOC',
  LOSS = 'OtherIssuesRequest_LOSS',
  ADJ = 'OtherIssuesRequest_ADJ',
  PSV = 'OtherIssuesRequest_PSV'

}

export enum RequestApprovalsAPITypesEnum {
  BTQ = 'BTQ',
  LOAN = 'LOAN',
  EXH = 'EXH',
  FOC = 'FOC',
  LOSS = 'LOSS',
  ADJ = 'ADJ',
  PSV = 'PSV'


}

export enum RequestApprovalEnum {
  ADJ = 'Adjustment',
  EXH = 'Exhibition',
  FOC = 'Foc',
  LOSS = 'Loss',
  PSV = 'Psv',
  LOAN = 'Loan',


}
