import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequestApprovalsRoutingModule } from './request-approvals-routing.module';
import { BinRequestApprovalsPopupComponent } from './components/bin-request-approvals-popup/bin-request-approvals-popup.component';
import { BinRequestItemComponent } from './components/bin-request-item/bin-request-item.component';
import { BinRequestItemListComponent } from './components/bin-request-item-list/bin-request-item-list.component';
import { IbtRequestApprovalsItemComponent } from './components/ibt-request-approvals-item/ibt-request-approvals-item.component';
import { IbtRequestApprovalsItemListComponent } from './components/ibt-request-approvals-item-list/ibt-request-approvals-item-list.component';
import { OtherIssueApprovalsItemListComponent } from './components/other-issues-approvals/other-issue-approvals-item-list/other-issue-approvals-item-list.component';
import { OtherIssuesApprovalsItemComponent } from './components/other-issues-approvals/other-issues-approvals-item/other-issues-approvals-item.component';
import { IbtRequestApprovalsDetailsComponent } from './containers/ibt-request-approvals-details/ibt-request-approvals-details.component';
import { OtherIssuesApprovalsDetailComponent } from './containers/other-issues-approvals-detail/other-issues-approvals-detail.component';
import { RequestApprovalsComponent } from './containers/request-approvals-list/request-approvals.component';
import { RequestApprovalsService } from './services/request-approvals.service';
import { RequestApprovalsFacade } from './+state/request-approvals.facade';
import { RequestApprovalsEffects } from './+state/request-approvals.effect';
import { EffectsModule } from '@ngrx/effects';
import { SharedModule, UicomponentsModule } from '@poss-web/shared';
import { MatSidenavModule } from '@angular/material';

@NgModule({
  declarations: [
    BinRequestApprovalsPopupComponent,
    BinRequestItemComponent,
    BinRequestItemListComponent,
    IbtRequestApprovalsItemComponent,
    IbtRequestApprovalsItemListComponent,
    OtherIssueApprovalsItemListComponent,
    OtherIssuesApprovalsItemComponent,
    IbtRequestApprovalsDetailsComponent,
    OtherIssuesApprovalsDetailComponent,
    RequestApprovalsComponent,
    BinRequestApprovalsPopupComponent
  ],
  imports: [
    CommonModule,
    RequestApprovalsRoutingModule,
    SharedModule,
    UicomponentsModule,
    MatSidenavModule,
    EffectsModule.forFeature([RequestApprovalsEffects])
  ],
  providers: [RequestApprovalsService, RequestApprovalsFacade],
  entryComponents: [BinRequestApprovalsPopupComponent]
})
export class RequestApprovalsModule {}
