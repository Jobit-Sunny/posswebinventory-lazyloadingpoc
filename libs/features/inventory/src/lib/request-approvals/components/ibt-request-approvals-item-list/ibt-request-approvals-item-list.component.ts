import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { RequestApprovalsItems } from '../../models/request-approvals-items.model';
import { PageEvent } from '@angular/material';
import { Subject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { RequestApprovalsFacade } from '../../+state/request-approvals.facade';
import { AppsettingFacade } from '@poss-web/core';
import { OverlayNotificationService } from '@poss-web/shared';
import { takeUntil, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'poss-web-ibt-request-approvals-item-list',
  templateUrl: './ibt-request-approvals-item-list.component.html',
  styleUrls: ['./ibt-request-approvals-item-list.component.scss']
})
export class IbtRequestApprovalsItemListComponent implements OnInit, OnDestroy {
  @Input() itemList: RequestApprovalsItems[];
  @Input() count;
  @Input() requestId;
  @Input() selectionEvents: Observable<boolean>;
  @Input() pageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 50,
    length: 0
  };
  @Output() paginator = new EventEmitter<PageEvent>();
  @Output() isSelected = new EventEmitter<any>();
  destroy$ = new Subject<null>();
  hasNotification: boolean;
  pageSizeOptions: number[] = [];
  minPageSize = 0;
  selectionAllSubscription: any;

  selectionAllSubject: Subject<any> = new Subject<any>();


  constructor(private router: Router,
    private facade: RequestApprovalsFacade,
    private appsettingFacade: AppsettingFacade,
    private overlayNotification: OverlayNotificationService) { }

  ngOnInit() {
    this.appsettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.pageSizeOptions = data;
        this.minPageSize = this.pageSizeOptions.reduce((a: number, b: number) =>
          a < b ? a : b
        );
      });


    this.selectionEvents
      .pipe(
        takeUntil(this.destroy$),
        debounceTime(10)
      )
      .subscribe(data => {
        this.selectionAllSubject.next(data);
      });

  }


  isSelect(event: any) {

    this.isSelected.emit(event);

  }

  paginate(event: PageEvent) {
    this.paginator.emit(event);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }


}
