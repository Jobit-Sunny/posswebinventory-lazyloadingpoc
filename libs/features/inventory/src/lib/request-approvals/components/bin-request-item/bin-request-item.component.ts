import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BinRequestApprovalsItems, BinApprovals } from '../../models/bin-reuest-approvals.model';
import { Subject } from 'rxjs';
import { BinRequestApprovalsPopupComponent } from '../bin-request-approvals-popup/bin-request-approvals-popup.component';
import { takeUntil } from 'rxjs/operators';
import { AppsettingFacade } from '@poss-web/core';
import { MatDialog } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { OverlayNotificationService } from '@poss-web/shared';
import * as BinRequestApprovalsAction from '../../+state/request-approvals.actions';
@Component({
  selector: 'poss-web-bin-request-item',
  templateUrl: './bin-request-item.component.html',
  styleUrls: ['./bin-request-item.component.scss']
})
export class BinRequestItemComponent implements OnInit {
  @Input() item: BinRequestApprovalsItems;
  @Output() destroy$ = new Subject<null>()
  @Input() count: number;
  id: number;


  @Output() approvalsValue: EventEmitter<BinRequestApprovalsAction.BinApprovalspayload> = new EventEmitter();

  constructor(private appsettingFacade: AppsettingFacade, public dialog: MatDialog, private translate: TranslateService,
    private overlayNotification: OverlayNotificationService) { }

  ngOnInit() {

  }

  binRequestApprovalsPopup(status:string): void {

    const dialogRef = this.dialog.open(BinRequestApprovalsPopupComponent, {
      width: '520px',
      data: {
        binName: this.item.binName,
        reqDocNo: this.item.reqDocNo,
        status:status
      }
    });

    dialogRef.afterClosed().pipe(takeUntil(this.destroy$)).subscribe(result => {

      if (result) {


        this.approvalsValue.emit({
          binRequestUpdateDto: result,
          id: this.item.id
        });
      }
    });
  }





}
