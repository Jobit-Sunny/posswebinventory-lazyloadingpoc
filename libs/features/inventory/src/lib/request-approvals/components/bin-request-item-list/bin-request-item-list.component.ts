import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { BinRequestApprovalsItems } from '../../models/bin-reuest-approvals.model';
import { PageEvent } from '@angular/material';
import { Subject } from 'rxjs';
import { RequestApprovalsFacade } from '../../+state/request-approvals.facade';
import { Router } from '@angular/router';
import { AppsettingFacade } from '@poss-web/core';
import { OverlayNotificationService } from '@poss-web/shared';
import { takeUntil } from 'rxjs/operators';
import * as RequestApprovalsAction from '../../+state/request-approvals.actions';

@Component({
  selector: 'poss-web-bin-request-item-list',
  templateUrl: './bin-request-item-list.component.html',
  styleUrls: ['./bin-request-item-list.component.scss']
})
export class BinRequestItemListComponent implements OnInit, OnDestroy {
  @Input() itemList: BinRequestApprovalsItems[];
  @Input() count = 0;
  @Input() pageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };
  @Output() paginator = new EventEmitter<PageEvent>();
  @Output() approvalsValue: EventEmitter<RequestApprovalsAction.BinApprovalspayload> = new EventEmitter();
  destroy$ = new Subject<null>();
  hasNotification: boolean;
  pageSizeOptions: number[] = [];
  minPageSize = 0;

  constructor(private router: Router,
    private facade: RequestApprovalsFacade,
    private appsettingFacade: AppsettingFacade,
    private overlayNotification: OverlayNotificationService) { }

  ngOnInit() {
    this.appsettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.pageSizeOptions = data;
        this.minPageSize = this.pageSizeOptions.reduce((a: number, b: number) =>
          a < b ? a : b
        );
      });
  }

  paginate(event: PageEvent) {
    this.paginator.emit(event);
  }

  approvals(approvalsValue: RequestApprovalsAction.BinApprovalspayload) {
    this.approvalsValue.emit(approvalsValue);
  }


  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
