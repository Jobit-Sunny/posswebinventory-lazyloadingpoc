import { Component,  Inject, OnDestroy } from '@angular/core';
import { BinRequestApprovalsItems, BinApprovals } from '../../models/bin-reuest-approvals.model';
import { Subject } from 'rxjs';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'poss-web-bin-request-approvals-popup',
  templateUrl: './bin-request-approvals-popup.component.html',
  styleUrls: ['./bin-request-approvals-popup.component.scss']
})
export class BinRequestApprovalsPopupComponent implements  OnDestroy {
  newBinRequestForm :FormGroup;
  destroy$: Subject<null> = new Subject<null>();
  binApproval: BinApprovals;
  constructor(public dialogRef: MatDialogRef<BinRequestApprovalsPopupComponent>, @Inject(MAT_DIALOG_DATA) public data: BinRequestApprovalsItems, public dialog: MatDialog, form: FormBuilder) {
    this.newBinRequestForm = form.group({

      remarks: ['', [Validators.minLength(30), Validators.maxLength(250)]],

    })

    if(data.status==='reject'){
      this.newBinRequestForm.get('remarks').setValidators([Validators.required,Validators.minLength(30), Validators.maxLength(250)])
    }
  }


  onClose(): void {
    this.dialogRef.close();
  }

  approved() {
    this.binApproval = {
      remarks: this.newBinRequestForm.get('remarks').value,
      status: "APPROVED"

    }

       if (this.newBinRequestForm.valid === true) {
      this.dialogRef.close(this.binApproval);
    }
    else {
      this.newBinRequestForm.markAllAsTouched();
    }

  }

  rejected() {
    this.binApproval = {
      remarks: this.newBinRequestForm.get('remarks').value,
      status:"APVL_REJECTED"

    }

    if (this.newBinRequestForm.valid === true) {
      this.dialogRef.close(this.binApproval);
    }
    else {
      this.newBinRequestForm.markAllAsTouched();
    }

  }


  clearRemarks() {
    this.newBinRequestForm.reset();
  }


  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }


}
