import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RequestApprovalsItems } from '../../models/request-approvals-items.model';
import { Subject, Observable } from 'rxjs';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { RequestApprovalsFacade } from '../../+state/request-approvals.facade';
import { debounceTime, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'poss-web-ibt-request-approvals-item',
  templateUrl: './ibt-request-approvals-item.component.html',
  styleUrls: ['./ibt-request-approvals-item.component.scss']
})
export class IbtRequestApprovalsItemComponent implements OnInit {

  @Input() item: RequestApprovalsItems;
  @Input() requestId: number;
  @Output() destroy$ = new Subject<null>()
  @Input() count: number;
  @Input() selectionEvents: Observable<any>;
  newIbtRequestForm: FormGroup;
  id: number;
  quantity: boolean;
  qty: number;
  display: boolean;
  @Output() isSelected = new EventEmitter<any>();

  constructor(private form: FormBuilder, private facade: RequestApprovalsFacade) {

    this.newIbtRequestForm = form.group({

      // appQuantity: ['', [Validators.required]],
      selected: ['', []],

    })

  }

  ngOnInit() {
    this.newIbtRequestForm.get('selected').setValue(this.item.isSelected);
    this.selectionEvents
      .pipe(
        takeUntil(this.destroy$),
        debounceTime(10)
      )
      .subscribe(data => {

        if (data.selectCheckbox === true) {
          this.newIbtRequestForm.patchValue({ selected: true });
        } else {
          this.newIbtRequestForm.patchValue({ selected: false });
        }

        if (data.enableCheckbox === false) {
          this.newIbtRequestForm.controls.selected.disable();
        } else {
          this.newIbtRequestForm.controls.selected.enable();
        }
      });


    // console.log(this.newIbtRequestForm.get('appQuantity').value)
    // TOD0- Quantity Edit
    //   if(this.item.approvedQuantity <= this.item.availableQuantity){
    //     this.quantity=true;
    //     this.qty=this.item.approvedQuantity;

    //       this.newIbtRequestForm.get('appQuantity').setValidators([Validators.required,Validators.min(1), Validators.max(this.qty)]);
    //       this.newIbtRequestForm.get('selected').setValue(this.item.isSelected);
    //     // this.newIbtRequestForm = this.form.group({

    //     //   appQuantity: ['', [Validators.min(1), Validators.max(this.item.approvedQuantity)]],

    //     // })

    //   }
    //   else if (this.item.approvedQuantity > this.item.availableQuantity){
    //     this.quantity=false;
    //     this.qty=this.item.availableQuantity;
    //     this.newIbtRequestForm.get('selected').setValue(this.item.isSelected);
    //     this.newIbtRequestForm.get('appQuantity').setValidators([Validators.required,Validators.min(1), Validators.max(this.qty)]);
    //     // this.newIbtRequestForm = this.form.group({

    //     //   appQuantity: ['', [Validators.min(1), Validators.max(this.item.availableQuantity)]],

    //     // })
    // console.log(this.qty)
    //     this.updateItem()

    //   }


    // if(! this.newIbtRequestForm.get('appQuantity').value){
    //   this.display=true;
    // }


  }


  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  onQuantityChange() {



    if (this.newIbtRequestForm.get('selected').value) {
      this.facade.updateIbt({
        id: this.requestId,
        itemId: this.item.id,
        itemUpdateDto: {
          quantity: this.newIbtRequestForm.get('appQuantity').value,
          status: "APPROVED"
        }
      })
    }
  }


  updateItem() {
    if (this.newIbtRequestForm.valid === true) {
      const isSelectedData = {
        id: this.item.id,
        isSelected: this.newIbtRequestForm.get('selected').value
      };

      this.isSelected.emit(isSelectedData);
    }
    else {
      this.newIbtRequestForm.markAllAsTouched();
    }




  }

}
