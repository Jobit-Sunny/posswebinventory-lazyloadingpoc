import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { RequestApprovalsItems } from '../../../models/request-approvals-items.model';
import { PageEvent } from '@angular/material';
import { Router } from '@angular/router';
import { RequestApprovalsFacade } from '../../../+state/request-approvals.facade';
import { AppsettingFacade } from '@poss-web/core';
import { OverlayNotificationService } from '@poss-web/shared';
import { takeUntil, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'poss-web-other-issue-approvals-item-list',
  templateUrl: './other-issue-approvals-item-list.component.html',
  styleUrls: ['./other-issue-approvals-item-list.component.scss']
})
export class OtherIssueApprovalsItemListComponent implements OnInit, OnDestroy {
  @Input() itemList: RequestApprovalsItems[];
  @Input() count;
  @Input() requestId;
  @Input() isSelectAll;
  @Input() selectionEvents: Observable<boolean>;
  @Input() pageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };
  @Output() paginator = new EventEmitter<PageEvent>();
  @Output() isSelected = new EventEmitter<any>();
  destroy$ = new Subject<null>();
  hasNotification: boolean;
  pageSizeOptions: number[] = [];
  minPageSize = 0;
  selectionAllSubscription: any;

  selectionAllSubject: Subject<any> = new Subject<any>();

  constructor(private router: Router,
    private facade: RequestApprovalsFacade,
    private appsettingFacade: AppsettingFacade,
    private overlayNotification: OverlayNotificationService) { }

  ngOnInit() {

    this.appsettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.pageSizeOptions = data;
        this.minPageSize = this.pageSizeOptions.reduce((a: number, b: number) =>
          a < b ? a : b
        );
      });
    this.selectionEvents
      .pipe(
        takeUntil(this.destroy$),
        debounceTime(10)
      )
      .subscribe(data => {
        this.selectionAllSubject.next(data);
      });

  }

  isSelect(event: any) {

    this.isSelected.emit(event);

  }

  paginate(event: PageEvent) {
    this.paginator.emit(event);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
