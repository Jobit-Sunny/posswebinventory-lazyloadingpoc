import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RequestApprovalsItems } from '../../../models/request-approvals-items.model';
import { Subject, Observable } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import { takeUntil, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'poss-web-other-issues-approvals-item',
  templateUrl: './other-issues-approvals-item.component.html',
  styleUrls: ['./other-issues-approvals-item.component.scss']
})
export class OtherIssuesApprovalsItemComponent implements OnInit {


  @Input() item: RequestApprovalsItems;
  @Input() requestId: number;
  @Input() isSelectAll;
  @Output() destroy$ = new Subject<null>()
  @Input() selectionEvents: Observable<any>;
  @Input() count: number;
  newIbtRequestForm: FormGroup;

  id: number;


  display: boolean;
  @Output() isSelected = new EventEmitter<any>();

  constructor(private form: FormBuilder) {

    this.newIbtRequestForm = form.group({


      selected: ['', []],

    })
  }

  ngOnInit() {
    this.newIbtRequestForm.get('selected').setValue(this.item.isSelected);


    this.selectionEvents
      .pipe(
        takeUntil(this.destroy$),
        debounceTime(10)
      )
      .subscribe(data => {

        if (data.selectCheckbox === true) {
          this.newIbtRequestForm.patchValue({ selected: true });
        } else {
          this.newIbtRequestForm.patchValue({ selected: false });
        }

        if (data.enableCheckbox === false) {
          this.newIbtRequestForm.controls.selected.disable();
        } else {
          this.newIbtRequestForm.controls.selected.enable();
        }
      });

  }

  updateItem() {

    const isSelectedData = {
      id: this.item.id,
      isSelected: this.newIbtRequestForm.get('selected').value
    };

    this.isSelected.emit(isSelectedData);
  }


}
