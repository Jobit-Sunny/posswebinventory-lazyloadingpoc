import { BoutiqueStatistics } from '../in-stock-management/in-stock/models/in-stock.model';
import { BinCodes, BinTranferProduct } from '../in-stock-management/in-stock/models/bin.model';

/**
 * Adapters for the In-Stock
 */
export class InStockAdaptor {
  /**
   * The function maps the json data to respective model type
   */
  static binProducts: BinTranferProduct[] = [];
  static binCodes: BinCodes[] = [];

  static boutiqueStatisticsfromJson(data: any): BoutiqueStatistics {
    return {
      totalBinCodes: data.totalBinCodes,
      totalBinGroups: data.totalBinGroups,
      totalProducCount: data.totalProducCount,
      totalProductCategories: data.totalProductCategories,
      totalValueOfStock: data.totalValueOfStock
    };
  }
  static BinCodesJson(data: any): BinCodes[] {
    // for (const stockTransferNote of data.results) {
    this.binCodes = [];
    for (const binCode of data) {
      this.binCodes.push({
        binCode: binCode.binCode,
        quantity: binCode.quantity
      });
    }

    return this.binCodes;
  }
}
