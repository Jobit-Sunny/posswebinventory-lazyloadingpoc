import { HttpParams } from '@angular/common/http';
import { OtherReceiptsIssuesEnum } from './in-stock-management/in-stock/in-stock.enum';
import { CountPayload } from './request-approvals/+state/request-approvals.actions';
import { BinToBinTransferTypeEnum } from './in-stock-management/bin-to-bin-transfer/models/bin-to-bin-transfer.enum';
import { ConfirmTransferAllItemsRequest } from './in-stock-management/bin-to-bin-transfer/models/bin-to-bin-transfer.model';

/**
 * Base Url for Inventory Stock API
 */

const getInventoryBaseUrl = (): string => {
  return `/inventory/v2`;
};

const getLocationBaseUrl = (): string => {
  return `/location/v1`;
};
const getNewLocationBaseUrl = (): string => {
  return `/location/v2`;
};

const getUsersBaseUrl = (): string => {
  return `/user/v1`;
};
const getProductBaseUrl = (): string => {
  return `/product/v1`;
};

const getStockReceiveUrl = (): string => {
  return `/stock-receives`;
};

const getStockRequestUrl = (): string => {
  return `/stock-requests`;
};

const getReturnInvoiceCfaUrl = (): string => {
  return `/return-invoices`;
};
const getPurchaseInvoiceUrl = (): string => {
  return `/purchase-invoices`;
};

const getStockManagementUrl = (): string => {
  return `/stock-managements`;
};
const getOtherIssuesUrl = (): string => {
  return `/other-issues/request`;
};
const getOtherRequestUrl = (): string => {
  return `/other-requests`;
};
const getOtherReceiveUrl = (): string => {
  return `/other-receives`;
};

const getStockTransferUrl = (): string => {
  return `/transfer`;
};

export const getStockIssueUrl = (): string => {
  return `/stock-issues`;
};

const getBinGroupBaseUrl = (): string => {
  return `/bingroups`;
};

const getStoreConfigBaseUrl = (): string => {
  return `/stores`;
};

const getLovsBaseUrl = (): string => {
  return `/lovs`;
};

const getInventorySchedulerUrl = (): string => {
  return `/inv-scheduler`;
};
/**
 * Retrieve the Url of the API Endpoint for listing the Products in the Stock Transfer Note
 * in paginated format.
 * @param page  The page index (i.e. zero based index) to retreive the list of products.
 * @param limit The number products that need to be provided in each page.
 */
export const getStockReceiveByPaginationEndpointUrl = (
  type,
  page,
  limit
): string => {
  return (
    getInventoryBaseUrl() +
    getStockReceiveUrl() +
    `?sort=srcDocDate,asc&transferType=${type}&page=${page}&size=${limit}`
  );
};

/**
 * Retrieve the Url of the API Endpoint for listing the Products in the Purchace Invoice (L3)
 * in paginated format.
 * @param page  The page index (i.e. zero based index) to retreive the list of products.
 * @param limit The number products that need to be provided in each page.
 */
export const getPurchaseInvoiceByPaginationEndpointUrl = (
  type,
  page,
  limit
): string => {
  return (
    getInventoryBaseUrl() +
    getPurchaseInvoiceUrl() +
    `?sort=srcDocDate,asc&invoiceType=${type}&page=${page}&size=${limit}`
  );
};

/**
 * Retrieve the list of product under particular Stock Transfer Note.
 * @param srcDocNo  This Stock Transfer Note Number.
 */
export const getStockReceiveBySrcDocNoEndpointUrl = (
  srcDocNo,
  type
): string => {
  return (
    getInventoryBaseUrl() +
    getStockReceiveUrl() +
    `?transferType=${type}&srcDocNo=${srcDocNo}`
  );
};

export const getStockByIdEndpointUrl = (id: string, type: string): string => {
  return (
    getInventoryBaseUrl() + getStockReceiveUrl() + `/${id}?transferType=${type}`
  );
};

/**
 * Retrieve the list of product under particular  Purchace Invoice  (L3)
 * @param srcDocNo  This Stock Transfer Note Number.
 */
export const getPurchaseInvoiceBySrcDocNoEndpointUrl = (
  srcDocNo,
  type
): string => {
  return (
    getInventoryBaseUrl() +
    getPurchaseInvoiceUrl() +
    `?invoiceType=${type}&srcDocNo=${srcDocNo}`
  );
};

export const getInvociePurchaseByIdEndpointUrl = (
  id: string,
  type: string
): string => {
  return (
    getInventoryBaseUrl() +
    getPurchaseInvoiceUrl() +
    `/${id}?invoiceType=${type}`
  );
};

/**
 * Retrieves the URL of the API Endpoint for retreiving the total count of pending Stock Transfer Note
 * for factory, boutique and other receipts.
 */
export const getStockTransferNoteCountEndpointUrl = (): string => {
  return getInventoryBaseUrl() + getStockReceiveUrl() + '/counts';
};

export const getReceiveInvoiceEndpointUrl = (): string => {
  return getInventoryBaseUrl() + getPurchaseInvoiceUrl() + '/counts';
};

/**
 * Base Url for IBT Request Api
 */

export const getRequestListEndpointUrl = (): string => {
  return getInventoryBaseUrl() + getStockRequestUrl();
};
/**
 * To retrive request sent and received list based on the params
 * @param type : type of request
 * @param pageIndex : page number
 * @param pageSize : number of requests in that page
 */
export const getRequestListByPaginationEndpointUrl = (
  requestGroup,
  pageIndex,
  pageSize
): string => {
  return (
    getRequestListEndpointUrl() +
    `?requestGroup=${requestGroup}&page=${pageIndex}&requestType=BTQ&size=${pageSize}&sort=${'reqDocDate'}%2C${'desc'}&sort=`
  );
};

export const getCreateRequestListEndpointUrl = (): string => {
  return getInventoryBaseUrl() + getStockRequestUrl() + `?requestType=BTQ`;
};

export const getBoutiqueListEndpointUrl = (): string => {
  return getInventoryBaseUrl() + `/stock-managements/items/locations`;
};
/**
 * To retrive boutique list based on the items entered to create request
 * @param pageIndex: page number
 * @param pageSize: number of boutiques to be displayed in that page
 */
export const getBoutiqueListByPaginationEndpointUrl = (
  pageIndex,
  pageSize
): string => {
  return getBoutiqueListEndpointUrl() + `?page=${pageIndex}&size=${pageSize}`;
};

export const getRequestEndpointUrl = (
  id: number,
  requestGroup: string
): string => {
  return (
    getInventoryBaseUrl() +
    getStockRequestUrl() +
    `/${id}?requestType=BTQ&requestGroup=${requestGroup}`
  );
};
/**
 * To retrive product variant based on params
 * @param requestId : selected request id
 * @param pageIndex : page number
 * @param pageSize : number of products to be displayed in that page
 */
export const getItemListEndpointUrl = (
  id: number,
  requestGroup: string
): string => {
  return (
    getInventoryBaseUrl() +
    getStockRequestUrl() +
    `/${id}/items?requestType=BTQ&requestGroup=${requestGroup}`
  );
};

/**
 * To update quantity for the product based on params
 * @param id : selected request id
 * @param itemId : selected product variant id
 */
export const updateItemListEndpointUrl = (
  id: number,
  itemId: string,
  requestGroup: string
): string => {
  return (
    getInventoryBaseUrl() +
    getStockRequestUrl() +
    `/${id}/items/${itemId}` +
    `?requestType=BTQ&requestGroup=${requestGroup}`
  );
};

/**
 * To update status of request for selected products
 * @param type : type of status
 * @param id : selected request id
 */

export const updateItemListStatusEndpointUrl = (
  id: number,
  requestGroup: string
): string => {
  return (
    getInventoryBaseUrl() +
    getStockRequestUrl() +
    `/${id}` +
    `?requestType=BTQ&requestGroup=${requestGroup}`
  );
};

export const getBoutiqueStatisticsEndpointUrl = (): string => {
  return `/boutiqueStatistics`;
};

export const getStockReceiveVerifyItemEndpointUrl = (
  storeType: string,
  type: string,
  id: number,
  itemId: string
): string => {
  if (isL1L2Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getStockReceiveUrl() +
      `/${id}/items/${itemId}?transferType=${type}`
    );
  } else if (isL3Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getPurchaseInvoiceUrl() +
      `/${id}/items/${itemId}?invoiceType=${type}`
    );
  }
};

export const getStockReceiveValidateItemEndpointUrl = (
  productGroupCode: string,
  availableWeight: number,
  measuredWeight: number,
  measuredQuantity: number,
  availableQuantity: number,
  configType: string
): { path: string; params: HttpParams } => {
  const path = getInventoryBaseUrl() + getStoreConfigBaseUrl() + '/validate';
  const params = new HttpParams()
    .set('productGroupCode', productGroupCode)
    .set('availableWeight', '' + availableWeight)
    .set('measuredWeight', '' + measuredWeight)
    .set('measuredQuantity', '' + measuredQuantity)
    .set('availableQuantity', '' + availableQuantity)
    .set('configType', configType);
  return { path, params };
};

export const getStockReceiveUpdateAllItemsEndpointUrl = (
  storeType: string,
  type: string,
  id: number
): string => {
  if (isL1L2Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getStockReceiveUrl() +
      `/${id}/items?transferType=${type}`
    );
  } else if (isL3Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getPurchaseInvoiceUrl() +
      `/${id}/items?invoiceType=${type}`
    );
  }
};

export const getStockReceiveItemsEndpointUrl = (
  storeType: string,
  type: string,
  status: string,
  id: number,
  itemCode: string,
  lotNumber: string,
  pageIndex: number,
  pageSize: number,
  sortBy: string,
  sortOrder: string,
  filter: { key: string; value: any[] }[]
): { path: string; params: HttpParams } => {
  let path = getInventoryBaseUrl();
  if (isL1L2Store(storeType)) {
    path = path + getStockReceiveUrl();
  } else if (isL3Store(storeType)) {
    path = path + getPurchaseInvoiceUrl();
  }
  path = path + `/${id}/items`;

  let params = new HttpParams();
  if (itemCode) {
    params = params.set('itemCode', itemCode);
  }
  if (lotNumber) {
    params = params.set('lotNumber', lotNumber);
  }
  if (status) {
    params = params.set('status', status);
  }
  if (type) {
    if (isL1L2Store(storeType)) {
      params = params.set('transferType', type);
    } else if (isL3Store(storeType)) {
      params = params.set('invoiceType', type);
    }
  }
  if (sortBy) {
    params = params.set('sort', `${sortBy},${sortOrder}`);
  }
  if (pageIndex !== null && pageSize !== null) {
    params = params.set('page', pageIndex.toString());
    params = params.set('size', pageSize.toString());
  }
  if (filter && filter.length > 0) {
    for (let i = 0; i < filter.length; i++) {
      for (let j = 0; j < filter[i].value.length; j++) {
        params = params.append(filter[i].key, filter[i].value[j]);
      }
    }
  }
  return {
    path,
    params
  };
};

export const getStockReceiveConfirmSTNEndpointUrl = (
  storeType: string,
  type: string,
  id: number
): string => {
  if (isL1L2Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getStockReceiveUrl() +
      `/${id}?transferType=${type}`
    );
  } else if (isL3Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getPurchaseInvoiceUrl() +
      `/${id}?invoiceType=${type}`
    );
  }
};

/*
  Endpoints for Stock-issue module
*/
//new
export const getStockIssueRequestUrl = (): string => {
  return `/stock-issues/request`;
};

export const getStockIssueByPaginationEndpointUrl = (
  transferType,
  requestType,
  page,
  limit
): string => {
  return (
    getInventoryBaseUrl() +
    getStockIssueRequestUrl() +
    `?requestType=${requestType}&page=${page}&size=${limit}&sort=${'reqDocDate'}%2C${'desc'}`
  );
};

export const getIssueSTNCountEndpointUrl = (): string => {
  return getInventoryBaseUrl() + getStockIssueRequestUrl() + `/counts`;
};
export const getOtherIssueSTNCountEndpointUrl = (): string => {
  return getInventoryBaseUrl() + getOtherIssuesUrl() + '/counts';
};

/*
  Stock Issue-L3
*/
const getInvoiceReturnUrl = (): string => {
  return `/invoice-return`;
};

export const getIssueInvoiceByPaginationEndpointUrl = (
  type,
  page,
  limit
): string => {
  return (
    getInventoryBaseUrl() +
    getInvoiceReturnUrl() +
    `?invoicetype=${type}&page=${page}&size=${limit}&sort=${'id'}%2C${'desc'}`
  );
};

/*
 Issue Invoice Count
*/

export const getIssueInvoiceEndpointUrl = (): string => {
  return getInventoryBaseUrl() + getInvoiceReturnUrl() + '/counts';
};

/*
Issue Search
*/

export const getStockIssueBySrcDocNoEndpointUrl = (
  srcDocNo,
  type,
  requestType
): string => {
  return (
    getInventoryBaseUrl() +
    getStockIssueRequestUrl() +
    `?requestType=${requestType}&reqDocNo=${srcDocNo}`
  );
};

/*
Search Issue Invoice
*/
export const getInvoiceReturnBySrcDocNoEndpointUrl = (
  srcDocNo,
  type
): string => {
  return (
    getInventoryBaseUrl() +
    getInvoiceReturnUrl() +
    `?invoicetype=${type}&srcdocno=${srcDocNo}`
  );
};

export const getInvoiceReturnByIdEndpointUrl = (
  srcDocNo,
  type,
  status
): string => {
  return (
    getInventoryBaseUrl() +
    getInvoiceReturnUrl() +
    `/${srcDocNo}?status=${status}`
  );
};

export const getStockIssueByReqDocNoEndpointUrl = (
  reqDocNo,
  transferType,
  requestType
): string => {
  return (
    getInventoryBaseUrl() +
    getStockIssueRequestUrl() +
    `?requestType=${requestType}&reqDocNo=${reqDocNo}`
  );
};

// ISSUE DETAILS
export const getIssueItemsCountEndpointUrl = (
  storeType: string,
  id: number,
  pageIndex: number,
  pageSize: number,
  requestType: string,
  status: string
): string => {
  if (isL1L2Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getStockIssueRequestUrl() +
      `/ ${id} /items?page=${pageIndex}&size=${pageSize}&requestType=${requestType}&status=${status}`
    );
  }
  // else if (isL3Store(storeType)) {
  //   return (
  //     getInventoryBaseUrl() +
  //     getInvoiceReturnUrl() +
  //     `/${id}/items?page=${pageIndex}&size=${pageSize}`
  //   );
  // }
  else {
    // TODO Throw an error
    return null;
  }
};

export const getIssueItemsByPaginationEndpointUrl = (
  id: number,
  itemCode: string,
  lotNumber: string,
  requestType: string,
  storeType: string,
  status: string,
  pageIndex: number,
  pageSize: number,
  sort?: Map<string, string>,
  filter?: { key: string; value: any[] }[]
): string => {
  if (isL1L2Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getStockIssueRequestUrl() +
      `/${id}/items?status=${status}&requestType=${requestType}&page=${pageIndex}` +
      (pageSize && pageSize !== 0 ? `&size=${pageSize}` : ``) +
      (itemCode && itemCode !== '' ? `&itemCode=${itemCode}` : ``) +
      (lotNumber && lotNumber !== '' ? `&lotNumber=${lotNumber}` : ``) +
      (sort ? generateSortUrl(sort) : ``) +
      (filter ? generateFilterIssuesUrl(filter) : ``)
    );
  }
  // else if (isL3Store(storeType)) {
  //   return (
  //     getInventoryBaseUrl() +
  //     getInvoiceReturnUrl() +
  //     `/${id}/items?page=${pageIndex}&size=${pageSize}`
  //   );
  // }
  else {
    // TODO Throw an error
    return null;
  }
};

// ISSUE CONFIRM
export const getIssueConfirmEndpointUrl = (
  id: any,
  requestType: string
): string => {
  return (
    getInventoryBaseUrl() +
    getStockIssueRequestUrl() +
    `/${id}?requestType=${requestType}`
  );
};
// Search Item
export const getSearchIssueItemsByItemCodeUrl = (
  storeType: string,
  id: number,
  itemCode: string,
  lotNumber: string,
  pageIndex: number,
  pageSize: number,
  requestType: string,
  status: string
): { path: string; params: HttpParams } => {
  //  TODO : LOT number
  if (isL1L2Store(storeType)) {
    if (lotNumber && lotNumber !== '') {
      const path =
        getInventoryBaseUrl() + getStockIssueRequestUrl() + `/${id}/items/`;
      const params = new HttpParams()
        .set('itemCode', itemCode)
        .set('lotNumber', lotNumber)
        .set('page', pageIndex.toString())
        .set('size', pageSize.toString())
        .set('requestType', requestType)
        .set('status', status);
      return { path, params };
    } else {
      const path =
        getInventoryBaseUrl() + getStockIssueRequestUrl() + `/${id}/items/`;
      const params = new HttpParams()
        .set('itemCode', itemCode)
        .set('page', pageIndex.toString())
        .set('size', pageSize.toString())
        .set('requestType', requestType)
        .set('status', status);
      return { path, params };
    }

    // return (
    //   getInventory2BaseUrl() +
    //   getStockIssueRequestUrl() +
    //   `/${id}/items/?itemCode=${itemCode}&page=${pageIndex}&size=${pageSize}&requestType=${requestType}&status=${status}`
    // );
  } else {
    // TODO Throw an error
    return null;
  }
};
// update Item
export const getIssueUpdateItemEndpointUrl = (
  requestType: string,
  storeType: string,
  id: number,
  itemId: number
): string => {
  if (isL1L2Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getStockIssueRequestUrl() +
      `/${id}/items/${itemId}?requestType=${requestType}`
    );
  } else {
    // TODO Throw an error
    return null;
  }
};
export const getIssueUpdateAllItemEndpointUrl = (
  requestType: string,
  storeType: string,
  id: number
): string => {
  return (
    getInventoryBaseUrl() +
    getStockIssueRequestUrl() +
    `/${id}/items?requestType=${requestType}`
  );
};

const isL1L2Store = (storeType: string): boolean => {
  return storeType === 'L1' || storeType === 'L2';
};

const isL3Store = (storeType: string): boolean => {
  return storeType === 'L3';
};
export const getCreateIssueItemsEndPointUrl = (id: number): string => {
  return (
    getInventoryBaseUrl() +
    getReturnInvoiceCfaUrl() +
    `/${id}/items` +
    '?invoiceType=BTQ_CFA'
  );
};
export const getCFAddressUrl = (): string => {
  return getNewLocationBaseUrl() + '/stores' + '/locations';
};

export const getConfirmReturnInvoiceCfaEndpointUrl = (id): string => {
  return (
    getInventoryBaseUrl() +
    getReturnInvoiceCfaUrl() +
    `/${id}` +
    '?invoiceType=BTQ_CFA'
  );
};
export const getSearchReturnInvoiceItemCfaEndpointUrl = (
  variantCode: string,
  lotNumber: string
): string => {
  let url =
    getInventoryBaseUrl() +
    '/stock-managements' +
    '/items' +
    '?binGroupCode=PURCFA';

  // if (lotNumber) {
  //   url = url + `?lotNumber=${lotNumber}`;
  // }
  if (variantCode) {
    url = url + `&itemCode=${variantCode}`;
  }

  return url;
};

export const getCreateRequestEndpointUrl = (): string => {
  return (
    getInventoryBaseUrl() + getReturnInvoiceCfaUrl() + '?invoiceType=BTQ_CFA'
  );
};

export const getRemoveSelectedItemEndPointUrl = (id, itemId): string => {
  return (
    getInventoryBaseUrl() +
    getReturnInvoiceCfaUrl() +
    `/${id}` +
    '/items' +
    `/${itemId}`
  );
};

export const getItemsToCFAUrl = (
  id: number,
  pageIndex: number,
  pageSize: number,
  sortBy: string,
  sortOrder: string,
  itemCode: string,
  filter: { key: string; value: any[] }[]
): string => {
  let url =
    getInventoryBaseUrl() +
    getReturnInvoiceCfaUrl() +
    `/${id}` +
    '/items?' +
    'invoiceType=BTQ_CFA' +
    '&status=SELECTED';
  if (itemCode) {
    url = url + `&itemCode=${itemCode}`;
  }
  if (pageIndex !== null && pageSize !== null) {
    url = url + `&page=${pageIndex}&size=${pageSize}`;
  }
  if (sortBy) {
    url = url + `&sort=${sortBy}%2C${sortOrder}`;
  }
  if (filter && filter.length > 0) {
    url = url + `&page=${pageIndex}&size=${pageSize}`;
    for (let i = 0; i < filter.length; i++) {
      for (let j = 0; j < filter[i].value.length; j++) {
        url = url + `&${filter[i].key}=${filter[i].value[j]}`;
      }
    }
  }

  return url;
};

export const getReturnInvoiceToCFAProductGroupsForFilterUrl = (): string => {
  return getProductBaseUrl() + getStoreConfigBaseUrl() + '/product-groups';
};
export const getReturnInvoiceToCFAProductCatrgoryForFilterUrl = (): string => {
  return getProductBaseUrl() + getStoreConfigBaseUrl() + '/product-categories';
};
export const getSortByWeightUrl = (id, pageIndex, pageSize): string => {
  return (
    getInventoryBaseUrl() +
    getReturnInvoiceCfaUrl() +
    `/${id}` +
    '/items' +
    '?page=' +
    `${pageIndex}` +
    '&size=' +
    `${pageSize}` +
    `&sort=totalWeight%2Cdesc`
  );
};

/**
 * Base Url for stock-issue TEP/GEP  Api
 */

export const getCreateStockIssueUrl = (
  transferType: string,
  storeType: string
): string => {
  if (isL1L2Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getStockIssueUrl() +
      getStockTransferUrl() +
      `?transferType=${transferType}`
    );
  } else if (isL3Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getReturnInvoiceCfaUrl() +
      `?invoiceType=${transferType}`
    );
  }
};

export const updateStockIssueUrl = (
  id: number,
  transferType: string,
  storeType: string
): string => {
  if (isL1L2Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getStockIssueUrl() +
      getStockTransferUrl() +
      `/${id}?transferType=${transferType}`
    );
  } else if (isL3Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getReturnInvoiceCfaUrl() +
      `/${id}?invoiceType=${transferType}`
    );
  }
};

export const getCreateStockIssueItemsUrl = (
  id: number,
  transferType: string,
  storeType: string
): string => {
  if (isL1L2Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getStockIssueUrl() +
      getStockTransferUrl() +
      `/${id}/items?transferType=${transferType}`
    );
  } else if (isL3Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getReturnInvoiceCfaUrl() +
      `/${id}/items?invoiceType=${transferType}`
    );
  }
};

export const getStockIssueItemsUrl = (
  id: number,
  itemCode: string,
  lotNumber: string,
  transferType: string,
  storeType: string,
  status: string,
  pageIndex: number,
  pageSize: number,
  sort,
  filter
): string => {
  if (isL1L2Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getStockIssueUrl() +
      getStockTransferUrl() +
      `/${id}/items?status=${status}&transferType=${transferType}&page=${pageIndex}` +
      (pageSize && pageSize !== 0 ? `&size=${pageSize}` : ``) +
      (itemCode && itemCode !== '' ? `&itemCode=${itemCode}` : ``) +
      (lotNumber && lotNumber !== '' ? `&lotNumber=${lotNumber}` : ``) +
      (sort ? generateSortUrl(sort) : ``) +
      (filter ? generateFilterUrl(filter) : ``)
    );
  } else if (isL3Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getReturnInvoiceCfaUrl() +
      `/${id}/items?status=${status}&invoiceType=${transferType}&page=${pageIndex}` +
      (pageSize && pageSize !== 0 ? `&size=${pageSize}` : ``) +
      (itemCode && itemCode !== '' ? `&itemCode=${itemCode}` : ``) +
      (lotNumber && lotNumber !== '' ? `&lotNumber=${lotNumber}` : ``) +
      (sort ? generateSortUrl(sort) : ``) +
      (filter ? generateFilterUrl(filter) : ``)
    );
  }
};

export const getStockIssueItemsCountUrl = (
  id: number,
  itemCode: string,
  lotNumber: string,
  transferType: string,
  storeType: string,
  status: string,
  sort: Map<string, string>,
  filter: Map<string, string>
): string => {
  if (isL1L2Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getStockIssueUrl() +
      getStockTransferUrl() +
      `/${id}/items?status=${status}&transferType=${transferType}&page=0&size=1` +
      (itemCode && itemCode !== '' ? `&itemCode=${itemCode}` : ``) +
      (lotNumber && lotNumber !== '' ? `&lotNumber=${lotNumber}` : ``) +
      (sort ? generateSortUrl(sort) : ``) +
      (filter ? generateFilterUrl(filter) : ``)
    );
  } else if (isL3Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getReturnInvoiceCfaUrl() +
      `/${id}/items?status=${status}&invoiceType=${transferType}&page=0&size=1` +
      (itemCode && itemCode !== '' ? `&itemCode=${itemCode}` : ``) +
      (lotNumber && lotNumber !== '' ? `&lotNumber=${lotNumber}` : ``) +
      (sort ? generateSortUrl(sort) : ``) +
      (filter ? generateFilterUrl(filter) : ``)
    );
  }
};

export const updateStockIssueItemUrl = (
  id: number,
  itemId: number,
  transferType: string,
  storeType: string
): string => {
  if (isL1L2Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getStockIssueUrl() +
      getStockTransferUrl() +
      `/${id}/items/${itemId}?transferType=${transferType}`
    );
  } else if (isL3Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getReturnInvoiceCfaUrl() +
      `/${id}/items/${itemId}?invoiceType=${transferType}`
    );
  }
};

export const generateFilterUrl = (filter: Map<string, string>): string => {
  if (filter.size !== 0) {
    let filterUrl = '';
    filter.forEach((value, key) => {
      filterUrl = filterUrl + `&${key}=${value}`;
    });
    return filterUrl;
  } else {
    return '';
  }
};

export const generateSortUrl = (sort: Map<string, string>): string => {
  if (sort.size !== 0) {
    let sortUrl = '';
    sort.forEach((value, key) => {
      sortUrl = sortUrl + `&${key}=${value}`;
    });
    return sortUrl;
  } else {
    return '';
  }
};

export const getProductCategoriesUrl = (): string => {
  return getProductBaseUrl() + '/stores/product-categories';
};

export const getProductGroupsUrl = (): string => {
  return getProductBaseUrl() + '/stores/product-groups';
};

export const getFactoryAddressEndpointUrl = (): string => {
  return getNewLocationBaseUrl() + `/stores/locations`;
};

export const getReceiptItemsByPaginationEndpointUrl = (
  storeType: string,
  id: number,
  status: string,
  pageIndex: number,
  pageSize: number
): string => {
  if (isL1L2Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getStockReceiveUrl() +
      `/${id}/items?page=${pageIndex}&size=${pageSize}&status=${status}`
    );
  } else if (isL3Store(storeType)) {
    return (
      getInventoryBaseUrl() +
      getPurchaseInvoiceUrl() +
      `/${id}/items?page=${pageIndex}&size=${pageSize}&status=${status}`
    );
  } else {
    // TODO Throw an error
    return null;
  }
};

export const getOtherIssueItemsByPaginationEndpointUrl = (
  id: number,
  status: string,
  pageIndex: number,
  pageSize: number,
  requestType: string,
  itemCode?: string,
  lotNumber?: string,
  sort?: Map<string, string>,
  filter?: { key: string; value: any[] }[]
): { path: string; params: HttpParams } => {
  const path =
    getInventoryBaseUrl() +
    getOtherIssuesUrl() +
    `/${id}/items?page=${pageIndex}&size=${pageSize}&requestType=${requestType}&status=${status}`;
  let params = new HttpParams();
  if (itemCode && itemCode !== '') {
    params = params.append('itemCode', itemCode);
  }
  if (lotNumber && lotNumber !== '') {
    params = params.append('lotNumber', lotNumber);
  }
  if (sort) {
    sort.forEach((value, key) => {
      params = params.append(key, value);
    });
  }
  if (filter && filter.length > 0) {
    for (let i = 0; i < filter.length; i++) {
      for (let j = 0; j < filter[i].value.length; j++) {
        params = params.append(filter[i].key, filter[i].value[j]);
      }
    }
  }
  return {
    path,
    params
  };
};

export const getOtherStockIssueByPaginationEndpointUrl = (
  requestType,
  pageIndex,
  pageSize
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherIssuesUrl() +
    `?requestType=${requestType}&page=${pageIndex}&size=${pageSize}`
  );
};
export const getOtherStockIssueBySrcDocNoEndpointUrl = (
  reqDocNo,
  requestType
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherIssuesUrl() +
    `?requestType=${requestType}&reqDocNo=${reqDocNo}`
  );
};
export const getRemoveSelectedItemsUrl = (id): string => {
  return (
    getInventoryBaseUrl() +
    getReturnInvoiceCfaUrl() +
    `/${id}` +
    '/items' +
    '?invoiceType=BTQ_CFA'
  );
};
export const getCourierDetailsByLocCodeEndpointUrl = (locationCode): string => {
  return (
    getInventoryBaseUrl() +
    `/couriers?isActive=true&locationCode=${locationCode}`
  );
};

export const getOtherReceiptsSortItemsByPaginationEndpointUrl = (
  id: number,
  status: string,
  pageIndex: number,
  pageSize: number,
  sortBy: string,
  property: string,
  transactionType: string,
  itemCode?: string,
  lotNumber?: string,
  sort?: Map<string, string>,
  filter?: { key: string; value: any[] }[]
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherReceiveUrl() +
    `/${id}/items?page=${pageIndex}&size=${pageSize}&status=${status}&transactionType=${transactionType}` +
    (itemCode && itemCode !== '' ? `&itemCode=${itemCode}` : ``) +
    (lotNumber && lotNumber !== '' ? `&lotNumber=${lotNumber}` : ``) +
    (sort ? generateSortUrl(sort) : ``) +
    (filter ? generateFilterIssuesUrl(filter) : ``)
  );
};

export const generateFilterIssuesUrl = (
  filter?: { key: string; value: any[] }[]
): string => {
  if (filter.length !== 0) {
    let filterUrl = '';
    if (filter && filter.length > 0) {
      for (let i = 0; i < filter.length; i++) {
        for (let j = 0; j < filter[i].value.length; j++) {
          filterUrl += '&' + filter[i].key + '=' + filter[i].value[j];
        }
      }
    }
    return filterUrl;
  } else {
    return '';
  }
};
export const getcreateOtherIssuesStockRequestEndpointUrl = (
  reqtype: string
): string => {
  return (
    getInventoryBaseUrl() + getOtherRequestUrl() + `?requestType=${reqtype}`
  );
};
//v2
export const getOtherIssueCreateItemsByPaginationEndpointUrl = (
  id: number,
  status: string,
  pageIndex: number,
  pageSize: number,
  requestType: string,
  itemCode?: string,
  lotNumber?: string,
  sort?: Map<string, string>,
  filter?: { key: string; value: any[] }[]
): { path: string; params: HttpParams } => {
  const path =
    getInventoryBaseUrl() +
    getOtherRequestUrl() +
    `/${id}/items?page=${pageIndex}&size=${pageSize}&requestType=${requestType}&status=${status}`;

  let params = new HttpParams();
  if (itemCode && itemCode !== '') {
    params = params.append('itemCode', itemCode);
  }
  if (lotNumber && lotNumber !== '') {
    params = params.append('lotNumber', lotNumber);
  }
  if (sort) {
    sort.forEach((value, key) => {
      params = params.append(key, value);
    });
  }
  if (filter && filter.length > 0) {
    for (let i = 0; i < filter.length; i++) {
      for (let j = 0; j < filter[i].value.length; j++) {
        params = params.append(filter[i].key, filter[i].value[j]);
      }
    }
  }
  return {
    path,
    params
  };
};
//v2
export const searchOtherIssueCreateItemsByPaginationEndpointUrl = (
  id: number,
  status: string,
  pageIndex: number,
  pageSize: number,
  requestType: string,
  itemCode: string,
  lotNumber: string
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherRequestUrl() +
    `/${id}/items?page=${pageIndex}&size=${pageSize}&requestType=${requestType}&status=${status}&itemCode=${itemCode}` +
    (lotNumber && lotNumber !== '' ? `&lotNumber=${lotNumber}` : ``)
  );
};

export const getCreateOtherIssueStockRequestItemsUrl = (
  id: number,
  requestType: string
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherRequestUrl() +
    `/${id}/items?requestType=${requestType}`
  );
};
export const getupdateStockRequestCreateItemUrl = (
  id: number,
  itemid: number,
  requestType: string
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherRequestUrl() +
    `/${id}/items/${itemid}?requestType=${requestType}`
  );
};
export const getupdateStockRequestUrl = (
  id: number,
  requestType: string
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherRequestUrl() +
    `/${id}?requestType=${requestType}`
  );
};

export const searchOtherIssueDetailsItemsByPaginationEndpointUrl = (
  id: number,
  status: string,
  pageIndex: number,
  pageSize: number,
  requestType: string,
  itemCode: string,
  lotNumber: string
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherIssuesUrl() +
    `/${id}/items?page=${pageIndex}&size=${pageSize}&requestType=${requestType}&status=${status}&itemCode=${itemCode}` +
    (lotNumber && lotNumber !== '' ? `&lotNumber=${lotNumber}` : ``)
  );
};

//v2
export const getCreateOtherStockIssueItemsUrl = (
  id: number,
  requestType: string
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherIssuesUrl() +
    `/${id}/items?requestType=${requestType}`
  );
};
//v2
export const getConfirmOtherIssueUrl = (
  id: number,
  requestType: string
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherIssuesUrl() +
    `/${id}/?requestType=${requestType}`
  );
};
// Bin to Bin Transfer

export const getBinToBinTransferGetItemListGroupsUrl = (
  type: string,
  pageIndex: number,
  pageSize: number,
  value: string,
  binType: string
): { path: string; params: HttpParams } => {
  let path = getInventoryBaseUrl() + getStockManagementUrl() + '/';
  let params = new HttpParams().set('binType', binType);
  if (type === BinToBinTransferTypeEnum.BIN_CODE) {
    path = path + 'bins';
  } else if (type === BinToBinTransferTypeEnum.PRODUCT_GROUP) {
    path = path + 'product-group';
  } else if (type === BinToBinTransferTypeEnum.PRODUCT_CATEGORY) {
    path = path + 'product-category';
  }
  if (pageIndex !== null && pageSize !== null) {
    params = params.set('page', pageIndex.toString());
    params = params.set('size', pageSize.toString());
  }
  if (value) {
    params = params.set(type, value);
  }
  return {
    path,
    params
  };
};

export const getBinToBinTransferSearchItemsUrl = (
  itemCode: string,
  lotNumber: string,
  binType: string
): { path: string; params: HttpParams } => {
  const path = getInventoryBaseUrl() + getStockManagementUrl() + `/items`;
  let params = new HttpParams().set('binType', binType);
  if (itemCode) {
    params = params.set('itemCode', itemCode);
  }
  if (lotNumber) {
    params = params.set('lotNumber', lotNumber);
  }
  return {
    path,
    params
  };
};

export const getBinToBinTransferGetItemsUrl = (
  itemCode: string,
  lotNumber: string,
  type: string,
  value: string,
  pageIndex: number,
  pageSize: number,
  sortBy: string,
  sortOrder: string,
  filter: { key: string; value: any[] }[],
  binType: string
): { path: string; params: HttpParams } => {
  const path = getInventoryBaseUrl() + getStockManagementUrl() + `/items`;
  let params = new HttpParams().set('binType', binType);
  if (itemCode) {
    params = params.set('itemCode', itemCode);
  }
  if (lotNumber) {
    params = params.set('lotNumber', lotNumber);
  }
  if (type && value) {
    params = params.set(type, value);
  }
  if (sortBy) {
    params = params.set('sort', `${sortBy},${sortOrder}`);
  }
  if (pageIndex !== null && pageSize !== null) {
    params = params.set('page', pageIndex.toString());
    params = params.set('size', pageSize.toString());
  }
  if (filter && filter.length > 0) {
    for (let i = 0; i < filter.length; i++) {
      for (let j = 0; j < filter[i].value.length; j++) {
        params = params.append(filter[i].key, filter[i].value[j]);
      }
    }
  }
  return {
    path,
    params
  };
};

export const getBinToBinTransferConfirmTransferItemsUrl = (): string => {
  return getInventoryBaseUrl() + getStockManagementUrl() + '/bins/items';
};

export const getBinToBinTransferProductGroupsForFilterUrl = (): string => {
  return getProductBaseUrl() + getStoreConfigBaseUrl() + '/product-groups';
};

export const getBinToBinTransferProductCatrgoryForFilterUrl = (): string => {
  return getProductBaseUrl() + getStoreConfigBaseUrl() + '/product-categories';
};

export const getBinToBinTransferConfirmTransferAllItemsUrl = (
  request: ConfirmTransferAllItemsRequest
): { path: string; params: HttpParams } => {
  let typeUrl = '';
  let typeParam = '';
  let path = getInventoryBaseUrl() + getStockManagementUrl();
  if (request.type === BinToBinTransferTypeEnum.BIN_CODE) {
    typeUrl = 'bin';
    typeParam = 'srcBincode';
  } else if (request.type === BinToBinTransferTypeEnum.PRODUCT_GROUP) {
    typeUrl = 'product-group';
    typeParam = 'productGroup';
  } else if (request.type === BinToBinTransferTypeEnum.PRODUCT_CATEGORY) {
    typeUrl = 'product-category';
    typeParam = 'productCategory';
  }
  path = path + `/${typeUrl}/items`;
  const params = new HttpParams()
    .set('destinationBincode', request.destinationBinCode)
    .set('destinationBinGroup', request.destinationBinGroupCode)
    .set(typeParam, request.value);

  return {
    path,
    params
  };
};
export const getAdjustmentSearchUrl = (
  variantCode: string,
  lotNumber: string
): string => {
  let url = getInventoryBaseUrl() + '/stock-management' + '/items';

  if (lotNumber) {
    url = url + `?lotNumber=${lotNumber}`;
  }
  if (variantCode) {
    url = url + `&itemCode=${variantCode}`;
  }

  return url;
};
export const getSearchAdjustmentEndpointUrl = (
  variantCode: string,
  lotNumber: string
): string => {
  let url = getInventoryBaseUrl() + '/stock-managements' + '/items';

  if (lotNumber) {
    url = url + `?lotNumber=${lotNumber}`;
  }
  if (variantCode) {
    url = url + `?itemCode=${variantCode}`;
  }
  return url;
};
export const getAdjustmentConfirmUrl = (type: string): string => {
  const url =
    getInventoryBaseUrl() + getOtherReceiveUrl() + `?transactionType=${type}`;
  return url;
};

export const getAllBinCodesUrl = (): string => {
  return getInventoryBaseUrl() + `/stock-managements/bincodes`;
};

export const getAllBinCodesCountUrl = (): string => {
  return getInventoryBaseUrl() + `/stock-managements/bins`;
};

export const requestBinUrl = () => {
  return getInventoryBaseUrl() + `/stock-managements/bins/requests`;
};

export const getBinRequestApprovalsUrl = (
  locationCode,
  reqDocNo,
  pageIndex,
  pageSize
): string => {
  let url = `/approvals/bins/requests`;
  let append = '?';
  if (locationCode) {
    url = url + append + `locationCode=${locationCode}`;
    append = '&';
  }
  if (reqDocNo) {
    url = url + append + `reqDocNo=${reqDocNo}`;
    append = '&';
  }

  if (pageIndex !== null && pageSize !== null) {
    url = url + append + `page=${pageIndex}&size=${pageSize}`;
  }

  return getInventoryBaseUrl() + url;
};

export const getLocationsUrl = (pageIndex, pageSize): string => {
  return getLocationBaseUrl() + `/locations?page=${pageIndex}&size=${pageSize}`;
};

export const getLocationCountUrl = (): string => {
  return getLocationBaseUrl() + `/locations`;
};

export const getOtherReciveCountEndpointUrl = (): string => {
  return getInventoryBaseUrl() + getOtherReceiveUrl() + '/counts';
};
export const getOtherReceiveByPaginationEndpointUrl = (
  type,
  page,
  limit
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherReceiveUrl() +
    `?page=${page}&size=${limit}&transactionType=${type}`
  );
};
export const getcreateOtherIssuesAdjustmentRequestEndpointUrl = (
  reqtype: string
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherRequestUrl() +
    `/items?requestType=${reqtype}`
  );
};
export const getotherReceiveStockByIdEndpointUrl = (
  id,
  transactionType
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherReceiveUrl() +
    `/${id}` +
    `?transactionType=${transactionType}`
  );
};

export const getIbtRequestApprovalsUrl = (
  id,
  requestType,
  pageIndex,
  pageSize
): string => {
  return (
    getInventoryBaseUrl() +
    `/approvals/${id}/items?page=${pageIndex}&requestType=${requestType}&size=${pageSize}&status=APVL_PENDING`
  );
};

export const getIbtsApprovalsUrl = (
  locationCode,
  reqDocNo,
  type,
  pageIndex,
  pageSize
): string => {
  let url = `/approvals`;
  let append = '?';

  if (pageIndex !== null) {
    url = url + append + `page=${pageIndex}`;
    append = '&';
  }

  if (reqDocNo) {
    url = url + append + `reqDocNo=${reqDocNo}`;
    append = '&';
  }

  if (locationCode) {
    url = url + append + `reqLocationCode=${locationCode}`;
    append = '&';
  }

  if (type) {
    url = url + append + `requestType=${type}`;
    append = '&';
  }

  if (pageSize !== null) {
    url = url + append + `size=${pageSize}`;
  }

  return (
    getInventoryBaseUrl() + url + `&sort=reqDocDate,ASC&status=APVL_PENDING`
  );
};
export const getAllBinRequestApprovalsCountUrl = (): string => {
  return getInventoryBaseUrl() + `/approvals/bins/requests`;
};

export const getAllIBTRequestApprovalsCountUrl = (): string => {
  return (
    getInventoryBaseUrl() + `/approvals?requestType=BTQ&status=APVL_PENDING`
  );
};

export const getAllIBTItemsRequestApprovalsCountUrl = (
  payload: CountPayload
): string => {
  return (
    getInventoryBaseUrl() +
    `/approvals/${payload.id}/items?requestType=${
      payload.requestType
    }&status=APVL_PENDING`
  );
};

export const updateBinRequestApprovalsCountUrl = (id): string => {
  return getInventoryBaseUrl() + `/approvals/bins/requests/${id}`;
};
export const updateIbtRequestApprovalsCountUrl = (
  id: number,
  itemId: string
): string => {
  return (
    getInventoryBaseUrl() + `/approvals/${id}/items/${itemId}?requestType=BTQ`
  );
};

export const updateIbtApprovalsCountUrl = (
  id: number,
  requestType: string
): string => {
  return getInventoryBaseUrl() + `/approvals/${id}?requestType=${requestType}`;
};

// export const getLocationsUrl = (pageIndex, pageSize): string => {
//   return (
//     getLocationBaseUrl() +
//     `/locations`

//   );
// };
export const getOtherReceiveItemsByPaginationEndpointUrl = (
  id: number,
  status: string,
  pageIndex: number,
  pageSize: number,
  transactionType: string
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherReceiveUrl() +
    `/${id}/items?page=${pageIndex}&size=${pageSize}&status=${status}&transactionType=${transactionType}`
  );
};
export const getSearchOtherReceiptItemsByItemCodeUrl = (
  id: number,
  itemCode: string,
  lotNumber: string,
  status: string,
  transactionType: string
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherReceiveUrl() +
    `/${id}/items/?itemCode=${itemCode}&status=${status}&transactionType=${transactionType}` +
    (lotNumber && lotNumber !== '' ? `&lotNumber=${lotNumber}` : ``)
  );
};
export const getOtherReceiveVerifyItemEndpointUrl = (
  id: number,
  itemId: number,
  transactionType: string
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherReceiveUrl() +
    `/${id}/items/${itemId}?transactionType=${transactionType}`
  );
};
export const getUpdateAllOtherReceiptItemsEndpointUrl = (
  id: number,
  transactionType: string
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherReceiveUrl() +
    `/${id}/items?transactionType=${transactionType}`
  );
};
export const getConfirmOtherReceiptSTNEndpointUrl = (
  id: number,
  transactionType: string
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherReceiveUrl() +
    `/${id}?transactionType=${transactionType}`
  );
};

export const getCancelStockRequestUrl = (
  id: number,
  requestType: string
): string => {
  if (
    requestType === OtherReceiptsIssuesEnum.ADJUSTMENT_TYPE ||
    requestType === OtherReceiptsIssuesEnum.PSV ||
    requestType === OtherReceiptsIssuesEnum.FOC
  ) {
    return (
      getInventoryBaseUrl() +
      getOtherRequestUrl() +
      `/${id}/cancel?requestType=${requestType}`
    );
  } else if (
    requestType === OtherReceiptsIssuesEnum.LOAN ||
    requestType === OtherReceiptsIssuesEnum.EXHIBITION_TYPE ||
    requestType === OtherReceiptsIssuesEnum.LOSS_TYPE
  ) {
    return (
      getInventoryBaseUrl() +
      getOtherRequestUrl() +
      `/items/${id}/cancel?requestType=${requestType}`
    );
  }
};
export const getOtherReceiveBySrcDocNoEndpointUrl = (
  srcDocNo,
  type
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherReceiveUrl() +
    `?transactionType=${type}&srcDocNo=${srcDocNo}`
  );
};
////////////////////////CONVERSION///////////////////////////////

const getStockManagementsUrl = (): string => {
  return `/stock-managements`;
};
export const getConversionSearchItemsEndpointUrl = (
  itemCode: string
): string => {
  return (
    getInventoryBaseUrl() +
    getStockManagementsUrl() +
    `/items?itemCode=${itemCode}`
  );
};
//
export const getConversionReqCountEndpointUrl = (): string => {
  return (
    getInventoryBaseUrl() +
    getStockManagementsUrl() +
    `/conversion/requests?page=${0}&size=${1}`
  );
};
//
export const getConversionRequestsBySrcDocNoUrl = (
  srcDocNo: number
): string => {
  return (
    getInventoryBaseUrl() +
    getStockManagementsUrl() +
    `/conversion/requests?srcDocNo=${srcDocNo}`
  );
};
export const getConversionRequestByIdEnpointUrl = (id): string => {
  return getInventoryBaseUrl() + getStockIssueUrl() + `/${id}`;
};
//
export const getConversionRequestDataByIdEnpointUrl = (id): string => {
  return (
    getInventoryBaseUrl() +
    getStockManagementsUrl() +
    `/conversion//requests/${id}` +
    `/items`
  );
};
//
export const getConversionItemsEndpointUrl = (loadItemsPayload): string => {
  return (
    getInventoryBaseUrl() +
    getStockManagementsUrl() +
    `/conversion/items?itemCode=${loadItemsPayload.itemCode}&lotNumber=${
      loadItemsPayload.lotNumber
    }&binCode=${loadItemsPayload.binCode}&itemWeight=${
      loadItemsPayload.itemWeight
    }`
  );
};
//
export const getConversionSplitItemEndpointUrl = (): string => {
  return getInventoryBaseUrl() + getStockManagementsUrl() + `/conversion/items`;
};
export const getConfirmConversionEndpointUrl = (id): string => {
  return (
    getInventoryBaseUrl() +
    getStockManagementsUrl() +
    `/conversion/requests/${id}`
  );
};
export const getConversionRequestEndpointUrl = (): string => {
  return getInventoryBaseUrl() + `/other-requests/items?requestType=${'CONV'}`;
};
//
export const getConversionRequestsEndpointUrl = (
  pageIndex,
  pageSize
): string => {
  return (
    getInventoryBaseUrl() +
    getStockManagementsUrl() +
    `/conversion/requests?page=${pageIndex}&size=${pageSize}&sort=createdDate,desc`
  );
};
export const getRsoDetailsEndpointUrl = (): string => {
  return getUsersBaseUrl() + `/store/user?isActive=true`;
};

export const getRequestByIdEndpointUrl = (id, type): string => {
  return getInventoryBaseUrl() + `/approvals/${id}?requestType=${type}`;
};

export const getAdjustmentApprovalsCountUrl = (): string => {
  return (
    getInventoryBaseUrl() + `/approvals?requestType=ADJ&status=APVL_PENDING`
  );
};

export const getLossApprovalsCountUrl = (): string => {
  return (
    getInventoryBaseUrl() + `/approvals?requestType=LOSS&status=APVL_PENDING`
  );
};

export const getLoanApprovalsCountUrl = (): string => {
  return (
    getInventoryBaseUrl() + `/approvals?requestType=LOAN&status=APVL_PENDING`
  );
};

export const getPsvApprovalsCountUrl = (): string => {
  return (
    getInventoryBaseUrl() + `/approvals?requestType=PSV&status=APVL_PENDING`
  );
};

export const getExhibitionApprovalsCountUrl = (): string => {
  return (
    getInventoryBaseUrl() + `/approvals?requestType=EXH&status=APVL_PENDING`
  );
};

export const getFocApprovalsCountUrl = (): string => {
  return (
    getInventoryBaseUrl() + `/approvals?requestType=FOC&status=APVL_PENDING`
  );
};

export const getPrintOtherIssuesEndpointUrl = (
  id: number,
  reqtype: string
): string => {
  return (
    getInventoryBaseUrl() +
    getOtherIssuesUrl() +
    `/${id}/prints?otherIssueType=${reqtype}`
  );
};

/**
 * file upload urls
 */
export const getFIRFileUploadUrl = (): string => {
  return getInventorySchedulerUrl() + `/v1/jobs/file/fir`;
};

export const getMERFileUploadUrl = (): string => {
  return getInventorySchedulerUrl() + `/v1/jobs/file/mer`;
};

export const getInvoiceUploadUrl = (): string => {
  return getInventorySchedulerUrl() + `/v1/jobs/file/upload?fileType=INVOICE`;
};

export const getSTNUploadUrl = (): string => {
  return getInventorySchedulerUrl() + `/v1/jobs/file/upload?fileType=STN`;
};
