import { DashboardEffects } from './dashboard/+state/dashboard.effect';
import { NgModule } from '@angular/core';
import { SharedModule, UicomponentsModule } from '@poss-web/shared';
import { FEATURE_NAME, reducers } from './inventory.state';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/containers/dashboard/dashboard.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MatSidenavModule } from '@angular/material/sidenav';
import { InStockFacade } from './in-stock-management/in-stock/+state/in-stock.facade';
import { DashboardFacade } from './dashboard/+state/dashboard.facade';
import { DashboardService } from './dashboard/services/dashboard.service';
import { AuthGuard } from '@poss-web/core';

import { InStockService } from './in-stock-management/in-stock/in-stock.service';
import { OtherReceiptsListingEffect } from './in-stock-management/other-issue-receipts/+receive-state/other-receipts.effects';
import { OtherIssuesEffect } from './in-stock-management/other-issue-receipts/+issue-state/other-issues.effects';
import { OtherReceiptsIssuesListComponent } from './in-stock-management/other-issue-receipts/containers/other-receipts-issues-list/other-receipts-issues-list.component';
import { OtherReceiptDetailsComponent } from './in-stock-management/other-issue-receipts/containers/other-receipt-details/other-receipt-details.component';
import { ExhibitionIssueCreateDetailsComponent } from './in-stock-management/other-issue-receipts/containers/exhibition-issue-create-details/exhibition-issue-create-details.component';
import { ExhibitionIssueDetailsComponent } from './in-stock-management/other-issue-receipts/containers/exhibition-issue-details/exhibition-issue-details.component';
import { AdjustmentReceiptsDetailsComponent } from './in-stock-management/other-issue-receipts/containers/adjustment-receipts-details/adjustment-receipts-details.component';
import { PsvIssueDetailsComponent } from './in-stock-management/other-issue-receipts/containers/psv-issue-details/psv-issue-details.component';
import { AdjsutmentIssueDetailsComponent } from './in-stock-management/other-issue-receipts/containers/adjsutment-issue-details/adjsutment-issue-details.component';
import { PsvReceiptDetailsComponent } from './in-stock-management/other-issue-receipts/containers/psv-receipt-details/psv-receipt-details.component';
import { FocIssueDetailsComponent } from './in-stock-management/other-issue-receipts/containers/foc-issue-details/foc-issue-details.component';
import { OtherReceiptItemListComponent } from './in-stock-management/other-issue-receipts/components/other-receipt-item-list/other-receipt-item-list.component';
import { OtherReceiptItemComponent } from './in-stock-management/other-issue-receipts/components/other-receipt-item/other-receipt-item.component';
import { ExhibitionIssueItemComponent } from './in-stock-management/other-issue-receipts/components/exhibition-issue-item/exhibition-issue-item.component';
import { ExhibitionIssueItemListComponent } from './in-stock-management/other-issue-receipts/components/exhibition-issue-item-list/exhibition-issue-item-list.component';
import { ExhibitionIssueCreateItemComponent } from './in-stock-management/other-issue-receipts/components/exhibition-issue-create-item/exhibition-issue-create-item.component';
import { ExhibitionIssueCreateItemListComponent } from './in-stock-management/other-issue-receipts/components/exhibition-issue-create-item-list/exhibition-issue-create-item-list.component';
import { AdjustmentReceiptsItemComponent } from './in-stock-management/other-issue-receipts/components/adjustment-receipts-item/adjustment-receipts-item.component';
import { AdjustmentReceiptsItemListComponent } from './in-stock-management/other-issue-receipts/components/adjustment-receipts-item-list/adjustment-receipts-item-list.component';
import { AdjsutmentIssueItemComponent } from './in-stock-management/other-issue-receipts/components/adjsutment-issue-item/adjsutment-issue-item.component';
import { AdjsutmentIssueItemListComponent } from './in-stock-management/other-issue-receipts/components/adjsutment-issue-item-list/adjsutment-issue-item-list.component';
import { PsvIssueItemComponent } from './in-stock-management/other-issue-receipts/components/psv-issue-item/psv-issue-item.component';
import { PsvIssueItemListComponent } from './in-stock-management/other-issue-receipts/components/psv-issue-item-list/psv-issue-item-list.component';
import { PsvReceiptItemComponent } from './in-stock-management/other-issue-receipts/components/psv-receipt-item/psv-receipt-item.component';
import { PsvReceiptItemListComponent } from './in-stock-management/other-issue-receipts/components/psv-receipt-item-list/psv-receipt-item-list.component';
import { FocIssueItemComponent } from './in-stock-management/other-issue-receipts/components/foc-issue-item/foc-issue-item.component';
import { FocIssueItemListComponent } from './in-stock-management/other-issue-receipts/components/foc-issue-item-list/foc-issue-item-list.component';
import { OtherIssuesFacade } from './in-stock-management/other-issue-receipts/+issue-state/other-issues.facade';
import { OtherReceiptsFacade } from './in-stock-management/other-issue-receipts/+receive-state/other-receipts.facade';
import { OtherReceiptIssueService } from './in-stock-management/other-issue-receipts/services/other-receipts-issue.service';
import { OtherIssueReceiptsModule } from './in-stock-management/other-issue-receipts/other-issue-receipts.module';
import { DataUploadFacade } from './data-upload/+state/data-upload.facade';
import { StockIssueFacade } from './issue/stock-issue/+state/stock-issue.facade';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    UicomponentsModule,
    MatSidenavModule,
    StoreModule.forFeature(FEATURE_NAME, reducers),
    EffectsModule.forFeature([
      DashboardEffects,
      OtherReceiptsListingEffect,
      OtherIssuesEffect
    ]),
    RouterModule.forChild([
      {
        path: '',
        component: DashboardComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'stockreceive',
        loadChildren: () =>
          import('./receive-stock/receive-stock.module').then(
            m => m.ReceiveStockModule
          ),
        canActivate: [AuthGuard]
      },

      {
        path: 'instock',
        loadChildren: () =>
          import('./in-stock-management/in-stock-management.module').then(
            m => m.InStockManagementModule
          ),
        canActivate: [AuthGuard]
      },

      // TODO : check the bin to bin route with guards

      {
        path: 'stockissue',
        loadChildren: () =>
          import('./issue/issue-stock.module').then(m => m.IssueStockModule),
        canActivate: [AuthGuard]
      },

      {
        path: 'instock/other-receipts-issues-list/:tabType/:type',
        component: OtherReceiptsIssuesListComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'instock/other-receipts-issues-list/:tabType/:type/:id/:tab',
        component: OtherReceiptDetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'instock/create/:type/:transferType/:tab',
        component: ExhibitionIssueCreateDetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'instock/:tabType/:type/:reqDocNo',
        component: ExhibitionIssueDetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'instock/otherreceipts/Adjustment',
        component: AdjustmentReceiptsDetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'instock/otherissues/PSV',
        component: PsvIssueDetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'instock/otherissues/Adjustment',
        component: AdjsutmentIssueDetailsComponent,
        canActivate: [AuthGuard]
      },

      {
        path: 'instock/otherreceipts/PSV',
        component: PsvReceiptDetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'instock/otherissues/FOC',
        component: FocIssueDetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'approvals',
        loadChildren: () =>
          import('./request-approvals/request-approvals.module').then(
            m => m.RequestApprovalsModule
          ),
        canActivate: [AuthGuard]
      },
      {
        path: 'approvals',
        loadChildren: () =>
          import('./request-approvals/request-approvals.module').then(
            m => m.RequestApprovalsModule
          ),
        canActivate: [AuthGuard]
      }
    ])
  ],
  declarations: [
    DashboardComponent,

    OtherReceiptsIssuesListComponent,
    ExhibitionIssueDetailsComponent,
    OtherReceiptItemListComponent,
    OtherReceiptItemComponent,
    OtherReceiptDetailsComponent,
    ExhibitionIssueItemComponent,
    ExhibitionIssueItemListComponent,
    ExhibitionIssueCreateItemComponent,
    ExhibitionIssueCreateItemListComponent,
    ExhibitionIssueCreateDetailsComponent,
    AdjustmentReceiptsDetailsComponent,
    AdjustmentReceiptsItemComponent,
    AdjustmentReceiptsItemListComponent,
    AdjsutmentIssueDetailsComponent,
    AdjsutmentIssueItemComponent,
    AdjsutmentIssueItemListComponent,
    PsvIssueDetailsComponent,
    PsvIssueItemComponent,
    PsvIssueItemListComponent,

    PsvReceiptDetailsComponent,
    PsvReceiptItemComponent,
    PsvReceiptItemListComponent,
    FocIssueDetailsComponent,
    FocIssueItemComponent,
    FocIssueItemListComponent
  ],
  providers: [
    InStockFacade,
    DashboardFacade,
    InStockService,
    DashboardService,
    OtherIssuesFacade,
    OtherReceiptsFacade,
    OtherReceiptIssueService,
    OtherIssueReceiptsModule,
    DataUploadFacade,
    StockIssueFacade
  ]
})
export class InventoryModule {}
