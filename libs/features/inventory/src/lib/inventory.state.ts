import { InStockReducer } from './in-stock-management/in-stock/+state/in-stock.reducers';

import { AppState } from '@poss-web/core';
import { createFeatureSelector, ActionReducerMap } from '@ngrx/store';
import { BoutiqueStatisticsState } from './in-stock-management/in-stock/+state/in-stock.state';
import { DashboardState } from './dashboard/+state/dashboard.state';
import { DashboardReducer } from './dashboard/+state/dashboard.reducer';
import { DataUploadState } from './data-upload/+state/data-upload.state';
import { DataUploadReducer } from './data-upload/+state/data-upload.reducers';
import { StockIssueState } from './issue/stock-issue/+state/stock-issue.state';
import { ReturnInvoiceCfaState } from './issue/return-invoice-cfa/+state/return-invoice-cfa.state';
import { IssueTEPState } from './issue/stock-issue-tep-gep/+state/stock-issue-tep-gep.state';
import { StockIssueReducer } from './issue/stock-issue/+state/stock-issue.reducer';
import { returnInvoiceCfaReducer } from './issue/return-invoice-cfa/+state/return-invoice-cfa.reducers';
import { IssueTEPReducer } from './issue/stock-issue-tep-gep/+state/stock-issue-tep-gep.reducers';
import { StockReceiveState } from './receive-stock/stock-receive/+state/stock-receive.state';
import { StockReceiveReducer } from './receive-stock/stock-receive/+state/stock-receive.reducer';
import { InterBoutiqueTransferState } from './in-stock-management/inter-boutique-transfer/+state/inter-boutique-transfer.state';
import { BinToBinTransferState } from './in-stock-management/bin-to-bin-transfer/+state/bin-to-bin-transfer.state';
import { OtherIssuesState } from './in-stock-management/other-issue-receipts/+issue-state/other-issues.state';
import { OtherReceiptState } from './in-stock-management/other-issue-receipts/+receive-state/other-receipts.state';
import { RequestApprovalsState } from './request-approvals/+state/request-approvals.state';
import { ConversionState } from './in-stock-management/conversion/+state/conversion.state';
import { InterBoutiqueTransferReducer } from './in-stock-management/inter-boutique-transfer/+state/inter-boutique-transfer.reducer';
import { BinToBinTransferReducer } from './in-stock-management/bin-to-bin-transfer/+state/bin-to-bin-transfer.reducer';
import { OtherIssuesReducer } from './in-stock-management/other-issue-receipts/+issue-state/other-issues.reducer';
import { OtherReceiptsReducer } from './in-stock-management/other-issue-receipts/+receive-state/other-receipts.reducer';
import { RequestApprovalsReducer } from './request-approvals/+state/request-approval.reducer';
import { ConversionReducer } from './in-stock-management/conversion/+state/conversion.reducer';

export const FEATURE_NAME = 'inventory';
export interface InventoryState {
  instock: BoutiqueStatisticsState;
  dashboard: DashboardState;
  stockReceive: StockReceiveState;
  stockIssue: StockIssueState;
  returnInvoiceCfa: ReturnInvoiceCfaState;
  interBoutiqueTransfer: InterBoutiqueTransferState;
  binToBinTransfer: BinToBinTransferState;
  issueTEP: IssueTEPState;
  otherIssues: OtherIssuesState;
  otherReceipt: OtherReceiptState;
  binRequestApprovals: RequestApprovalsState;
  conversion: ConversionState;
  dataUpload: DataUploadState;
}
export interface State extends AppState {
  inventory: InventoryState;
}

export const selectInventory = createFeatureSelector<State, InventoryState>(
  FEATURE_NAME
);

export const reducers: ActionReducerMap<InventoryState> = {
  instock: InStockReducer,
  dashboard: DashboardReducer,
  stockReceive: StockReceiveReducer,
  stockIssue: StockIssueReducer,
  interBoutiqueTransfer: InterBoutiqueTransferReducer,
  binToBinTransfer: BinToBinTransferReducer,
  returnInvoiceCfa: returnInvoiceCfaReducer,
  issueTEP: IssueTEPReducer,
  otherIssues: OtherIssuesReducer,
  otherReceipt: OtherReceiptsReducer,
  binRequestApprovals: RequestApprovalsReducer,
  conversion: ConversionReducer,
  dataUpload: DataUploadReducer
};
