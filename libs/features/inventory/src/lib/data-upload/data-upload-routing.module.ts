import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DataUploadComponent } from './components/data-upload/data-upload.component';
import { AuthGuard } from '@poss-web/core';

const routes: Routes = [
  {
    path: 'data-upload',
    component: DataUploadComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataUploadRoutingModule {}
