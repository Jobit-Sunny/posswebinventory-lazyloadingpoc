import { CustomErrors } from '@poss-web/core';

export interface DataUploadState {
  FIRFileUploadResponse: boolean;
  MERFileUploadResponse: boolean;
  InvoiceUploadResponse: any;
  STNUploadResponse: any;
  hasError?: CustomErrors;
  isLoading?: boolean;
}
