import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../inventory.state';
import * as DataUploadActions from './data-upload.actions';
import { DataUploadSelectors } from './data-upload.selectors';

/**
 * Data upload Facade for accesing Data-upload-State
 * */
@Injectable()
export class DataUploadFacade {
  constructor(private store: Store<State>) {}

  private FIRFileUploadResponse$ = this.store.select(
    DataUploadSelectors.selectFIRFileUploadResponse
  );

  private MERFileUploadResponse$ = this.store.select(
    DataUploadSelectors.selectMERFileUploadResponse
  );

  private InvoiceUploadResponse$ = this.store.select(
    DataUploadSelectors.selectInvoiceUploadResponse
  );

  private STNUploadResponse$ = this.store.select(
    DataUploadSelectors.selectSTNUploadResponse
  );

  private hasError$ = this.store.select(DataUploadSelectors.selectHasError);

  private isLoading$ = this.store.select(DataUploadSelectors.selectIsLoading);
  /**
   * Access for state selectors
   */

  loadFIRFileUpload(FIRFile: FormData) {
    this.store.dispatch(new DataUploadActions.FIRFileUpload(FIRFile));
  }

  getFIRFileUploadResponse() {
    return this.FIRFileUploadResponse$;
  }

  loadMERFileUpload(MERFile: FormData) {
    this.store.dispatch(new DataUploadActions.MERFileUpload(MERFile));
  }

  getMERFileUploadResponse() {
    return this.MERFileUploadResponse$;
  }

  loadInvoiceUpload(invoiceFile: FormData) {
    this.store.dispatch(new DataUploadActions.InvoiceUpload(invoiceFile));
  }

  getInvoiceUploadResponse() {
    return this.InvoiceUploadResponse$;
  }

  loadSTNUpload(STNFile: FormData) {
    this.store.dispatch(new DataUploadActions.STNUpload(STNFile));
  }

  getSTNUploadResponse() {
    return this.STNUploadResponse$;
  }

  clearResponse() {
    this.store.dispatch(new DataUploadActions.ResetResponse());
  }

  getError() {
    return this.hasError$;
  }

  getIsLoading() {
    return this.isLoading$;
  }
}
