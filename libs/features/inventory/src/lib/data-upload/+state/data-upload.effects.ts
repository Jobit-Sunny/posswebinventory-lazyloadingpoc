import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { DataPersistence } from '@nrwl/angular';
import {
  NotificationService,
  CustomErrors,
  CustomErrorAdaptor
} from '@poss-web/core';
import { DataUploadService } from '../services/data-upload.service';
import { DataUploadActionTypes } from './data-upload.actions';
import * as DataUploadActions from './data-upload.actions';
import { map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Action } from '@ngrx/store';

@Injectable()
export class DataUploadEffect {
  constructor(
    public dataPersistence: DataPersistence<any>,
    public dataUploadService: DataUploadService,
    public notificationService: NotificationService
  ) {}

  @Effect()
  FIRFileUpload$: Observable<Action> = this.dataPersistence.fetch(
    DataUploadActionTypes.FIR_FILE_UPLOAD,
    {
      run: (action: DataUploadActions.FIRFileUpload) => {
        return this.dataUploadService
          .FIRFileUpload(action.payload)
          .pipe(map(data => new DataUploadActions.FIRFileUploadSuccess(true)));
      },
      onError: (
        action: DataUploadActions.FIRFileUpload,
        error: HttpErrorResponse
      ) => {
        return new DataUploadActions.FIRFileUploadFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  MERFileUpload$: Observable<Action> = this.dataPersistence.fetch(
    DataUploadActionTypes.MER_FILE_UPLOAD,
    {
      run: (action: DataUploadActions.MERFileUpload) => {
        return this.dataUploadService
          .MERFileUpload(action.payload)
          .pipe(map(data => new DataUploadActions.MERFileUploadSuccess(true)));
      },
      onError: (
        action: DataUploadActions.MERFileUpload,
        error: HttpErrorResponse
      ) => {
        return new DataUploadActions.MERFileUploadFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  InvoiceUpload$: Observable<Action> = this.dataPersistence.fetch(
    DataUploadActionTypes.INVOICE_UPLOAD,
    {
      run: (action: DataUploadActions.InvoiceUpload) => {
        return this.dataUploadService
          .InvoiceUpload(action.payload)
          .pipe(map(data => new DataUploadActions.InvoiceUploadSuccess(data)));
      },
      onError: (
        action: DataUploadActions.InvoiceUpload,
        error: HttpErrorResponse
      ) => {
        return new DataUploadActions.InvoiceUploadFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  STNUpload$: Observable<Action> = this.dataPersistence.fetch(
    DataUploadActionTypes.STN_UPLOAD,
    {
      run: (action: DataUploadActions.STNUpload) => {
        return this.dataUploadService
          .STNUpload(action.payload)
          .pipe(map(data => new DataUploadActions.STNUploadSuccess(data)));
      },
      onError: (
        action: DataUploadActions.STNUpload,
        error: HttpErrorResponse
      ) => {
        return new DataUploadActions.STNUploadFailure(this.errorHandler(error));
      }
    }
  );

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
}
