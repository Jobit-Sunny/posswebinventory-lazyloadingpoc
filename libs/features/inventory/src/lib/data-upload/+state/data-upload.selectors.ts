import { createSelector } from '@ngrx/store';
import { selectInventory } from '../../inventory.state';

/**
 * The selectors for Data-Upload store
 */
const selectFIRFileUploadResponse = createSelector(
  selectInventory,
  state => state.dataUpload.FIRFileUploadResponse
);

const selectMERFileUploadResponse = createSelector(
  selectInventory,
  state => state.dataUpload.MERFileUploadResponse
);

const selectInvoiceUploadResponse = createSelector(
  selectInventory,
  state => state.dataUpload.InvoiceUploadResponse
);

const selectSTNUploadResponse = createSelector(
  selectInventory,
  state => state.dataUpload.STNUploadResponse
);

const selectHasError = createSelector(
  selectInventory,
  state => state.dataUpload.hasError
);

const selectIsLoading = createSelector(
  selectInventory,
  state => state.dataUpload.isLoading
);

export const DataUploadSelectors = {
  selectFIRFileUploadResponse,
  selectInvoiceUploadResponse,
  selectSTNUploadResponse,
  selectMERFileUploadResponse,
  selectHasError,
  selectIsLoading
};
