import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataUploadRoutingModule } from './data-upload-routing.module';
import { MatSidenavModule } from '@angular/material';
import { UicomponentsModule, SharedModule } from '@poss-web/shared';
import { EffectsModule } from '@ngrx/effects';
import { DataUploadEffect } from './+state/data-upload.effects';
import { DataUploadComponent } from './components/data-upload/data-upload.component';
import { DataUploadFacade } from './+state/data-upload.facade';
import { DataUploadService } from './services/data-upload.service';

@NgModule({
  declarations: [DataUploadComponent],
  imports: [
    CommonModule,
    DataUploadRoutingModule,
    SharedModule,
    UicomponentsModule,
    MatSidenavModule,
    EffectsModule.forFeature([DataUploadEffect])
  ],
  providers: [DataUploadFacade, DataUploadService]
})
export class DataUploadModule {}
