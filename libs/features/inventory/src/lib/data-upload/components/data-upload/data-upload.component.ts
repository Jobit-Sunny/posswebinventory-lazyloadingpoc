import { Component, OnInit } from '@angular/core';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { DataUploadFacade } from '../../+state/data-upload.facade';
import { Router } from '@angular/router';
import { FileTypesEnum } from '../../models/data-upload.enum';
import { CustomErrors } from '@poss-web/core';
import { getInventoryRouteUrl } from '../../../page-route.constants';

const CSVExtn = 'csv';
const reqfileKey = 'reqfile';
const stockFileKey = 'stockFile';

@Component({
  selector: 'poss-web-data-upload',
  templateUrl: './data-upload.component.html',
  styleUrls: ['./data-upload.component.scss']
})
export class DataUploadComponent implements OnInit {
  formData: FormData = new FormData();
  destroy$: Subject<null> = new Subject<null>();
  FIRFileUploadResponse$: Observable<boolean>;
  MERFileUploadResponse$: Observable<boolean>;
  InvoiceUploadResponse$: Observable<any>;
  STNUploadResponse$: Observable<any>;
  isLoading$: Observable<boolean>;

  constructor(
    private dataUploadFacade: DataUploadFacade,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService,
    private router: Router
  ) {}

  ngOnInit() {
    this.dataUploadFacade.clearResponse();
    this.componentInit();
  }

  componentInit() {
    this.FIRFileUploadResponse$ = this.dataUploadFacade.getFIRFileUploadResponse();
    this.MERFileUploadResponse$ = this.dataUploadFacade.getMERFileUploadResponse();
    this.InvoiceUploadResponse$ = this.dataUploadFacade.getInvoiceUploadResponse();
    this.STNUploadResponse$ = this.dataUploadFacade.getSTNUploadResponse();
    this.isLoading$ = this.dataUploadFacade.getIsLoading();

    this.dataUploadFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.FIRFileUploadResponse$.pipe(takeUntil(this.destroy$)).subscribe(
      data => {
        if (data === true) {
          const successKey = 'pw.fileUpload.FIRFileUploadSuccessMessage';
          this.showNotifications(successKey);
        }
      }
    );

    this.MERFileUploadResponse$.pipe(takeUntil(this.destroy$)).subscribe(
      data => {
        if (data === true) {
          const successKey = 'pw.fileUpload.MERFileUploadSuccessMessage';
          this.showNotifications(successKey);
        }
      }
    );

    // this.InvoiceUploadResponse$.pipe(takeUntil(this.destroy$)).subscribe(
    //   data => {
    //     if (data !== null) {
    //       this.showNotifications(data.message);
    //     }
    //   }
    // );

    // this.STNUploadResponse$.pipe(takeUntil(this.destroy$)).subscribe(data => {
    //   if (data !== null) {
    //     this.showNotifications(data.message);
    //   }
    // });
  }

  uploadFile(event) {
    this.dataUploadFacade.clearResponse();
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      if (file.size > 10000000) {
        const errorKey = 'pw.fileUpload.maximumFileSizeErrorMessage';
        this.showNotifications(errorKey);
      }
      const extn = file.name.split('.').pop();
      if (extn !== CSVExtn) {
        const errorKey = 'pw.fileUpload.CSVFileTypeErrorMessage';
        this.showNotifications(errorKey);
      }

      const type = file.name.substring(0, 3);
      this.formData.append(reqfileKey, file);
      if (extn === CSVExtn && file.size < 10000000) {
        if (type === FileTypesEnum.FIR) {
          this.dataUploadFacade.loadFIRFileUpload(this.formData);
        }
        if (type === FileTypesEnum.MER) {
          this.dataUploadFacade.loadMERFileUpload(this.formData);
        }
      }
    }
  }

  invoiceUpload(event) {
    this.dataUploadFacade.clearResponse();
    this.overlayNotification.close();
    const fileList: FileList = event.target.files;
    const formData: FormData = new FormData();
    if (fileList.length > 0) {
      const file: File = fileList[0];
      formData.append(stockFileKey, file);
    }
    this.dataUploadFacade.loadInvoiceUpload(formData);
  }

  STNUpload(event) {
    this.dataUploadFacade.clearResponse();
    this.overlayNotification.close();
    const fileList: FileList = event.target.files;
    const formData: FormData = new FormData();
    if (fileList.length > 0) {
      const file: File = fileList[0];
      formData.append(stockFileKey, file);
    }
    this.dataUploadFacade.loadSTNUpload(formData);
  }

  showNotifications(key: string) {
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification.show({
          type: OverlayNotificationType.SIMPLE,
          message: translatedMsg,
          hasClose: true
        });
      });
  }

  /**
   * to display error message
   * @param error : error from api
   */
  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true, // optional
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        // Action based event
      });
  }

  back(): void {
    this.router.navigate([getInventoryRouteUrl()]);
  }
}
