import { Injectable } from '@angular/core';
import { ApiService } from '@poss-web/core';
import { map } from 'rxjs/operators';
import {
  getFIRFileUploadUrl,
  getInvoiceUploadUrl,
  getSTNUploadUrl,
  getMERFileUploadUrl
} from '../../endpoints.constants';
import { Observable } from 'rxjs';

@Injectable()
export class DataUploadService {
  constructor(private apiService: ApiService) {}
  FIRFileUpload(file: FormData): Observable<any> {
    const FIRFileUploadUrl = getFIRFileUploadUrl();
    return this.apiService
      .postFile(FIRFileUploadUrl, file)
      .pipe(map((data: any) => data));
  }

  MERFileUpload(file: FormData): Observable<any> {
    const MERFileUploadUrl = getMERFileUploadUrl();
    return this.apiService
      .postFile(MERFileUploadUrl, file)
      .pipe(map((data: any) => data));
  }

  InvoiceUpload(file: FormData) {
    const InvoiceUploadUrl = getInvoiceUploadUrl();
    return this.apiService
      .postFile(InvoiceUploadUrl, file)
      .pipe(map((data: any) => data));
  }

  STNUpload(file: FormData) {
    const STNUploadUrl = getSTNUploadUrl();
    return this.apiService
      .postFile(STNUploadUrl, file)
      .pipe(map((data: any) => data));
  }
}
