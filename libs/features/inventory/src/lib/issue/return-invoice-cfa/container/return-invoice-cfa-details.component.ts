import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  TemplateRef
} from '@angular/core';
import {
  OverlayNotificationType,
  OverlayNotificationEventType,
  OverlayNotificationEventRef,
  SearchResponse,
  SearchListComponent,
  SortDialogService,
  Column,
  FilterService,
  Filter,
  OverlayNotificationService,
  ErrorEnums
} from '@poss-web/shared';
import { Observable, Subject, fromEvent } from 'rxjs';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { takeUntil, debounceTime } from 'rxjs/operators';
import { PageEvent, MatPaginator } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import {
  Item,
  InvoiceItems,
  CfaDetails,
  CFAItemToUpdate,
  CFAMaster,
  CFAddress,
  FilterOption
} from '../models/return-invoice-to-cfa.model';

import { ReturnInvoiceFacade } from '../+state/return-invoice-cfa.facade';
import { TranslateService } from '@ngx-translate/core';
import {
  CustomErrors,
  ShortcutService,
  Command,
  AppsettingFacade
} from '@poss-web/core';
import {
  ReturnInvoiceCFATabEnum,
  ReturnInvoiceCFAEnum
} from '../models/return-invoice-cfa.enum';
import {
  SORT_DATA,
  PRODUCT_CATEGORIES,
  SOURCE_BIN,
  PRODUCT_GROUP
} from '../models/return-invoice-to-cfa-data';

@Component({
  selector: 'poss-web-return-invoice-cfa-details',
  templateUrl: './return-invoice-cfa-details.component.html',
  styleUrls: ['./return-invoice-cfa-details.component.scss']
})
export class ReturnInvoiceCfaDetailsComponent
  implements OnInit, OnDestroy, AfterViewInit {
  returnInvoiceCFATabRef = ReturnInvoiceCFATabEnum;
  pageSize = 5;
  pageIndex = 0;
  initailPageEvent: PageEvent = {
    pageIndex: this.pageIndex,
    pageSize: this.pageSize,
    length: 0
  };
  sortData: Column[] = [];
  issuedItemsPageEvent: PageEvent = this.initailPageEvent;
  requestId: number;
  type: string;
  stockId = '';
  select: boolean;
  storeType: string;
  destroy$: Subject<null> = new Subject<null>();
  invoiceNumber: number;
  selectedProductsSearchCount: number;
  hasNotification = false;
  selected = 'select cfa';
  searchedItems$: Observable<Item[]>;
  searchResult: Item[] = [];
  isLoading$: Observable<boolean>;
  CFAItems: Item[] = [];
  CFAItems$: Observable<Item[]>;
  isItems: boolean;
  items: Item[] = [];
  cfaCode$: Observable<CfaDetails>;
  totalItemsCount$: Observable<number>;
  id: number;
  totalCount: number;
  cartItemIds$: Observable<number[] | string[]>;
  itemsInCart$: Observable<Item[]>;
  invoiceItems: InvoiceItems[] = [];
  hasSearchedItem$: Observable<boolean>;
  hasIssueItemSuccess$: Observable<boolean>;
  hasCartProducts$: Observable<boolean>;
  hasLoaded$: Observable<boolean>;
  error$: Observable<CustomErrors>;
  searchValue = null;
  cartCount = 0;
  isSearchingItems$: Observable<boolean>;
  hasSearchedItems$: Observable<boolean>;
  hasSelectedProductsSearchedItems$: Observable<boolean>;
  totalItemsCount = 0;
  cfaMasterDetails: CFAMaster[] = [];
  isRemoving: boolean;
  selectedItemsTotalCount = 0;
  sortBy: string;
  sortOrder: string;
  itemId = null;
  onSearchFlag: boolean;
  selectForm = this.fb.group({
    selectRadioButton: ['']
  });
  removeLineItem = [];
  selectionAll: boolean;
  selectionAllSubscription: any;
  totalRows: number;
  selectionAllSubject: Subject<any> = new Subject<any>();
  resetPagination: Subject<null> = new Subject<null>();
  itemIds: number[] = [];
  ids: number[] = [];
  rowNumber = 0;
  searchCount: number;
  hasSelectedProductsSearch: boolean;
  filterData: { [key: string]: Filter[] } = {};
  filter: { key: string; value: any[] }[] = [];
  @ViewChild('searchBox', { static: false }) searchBox: ElementRef;
  searchForm: FormGroup;
  @ViewChild(SearchListComponent, { static: true })
  searchListRef: SearchListComponent;
  maxSortLimit: number;
  maxFilterLimit: number;
  CFAddress: CFAddress;
  pageSizeOptions: number[] = [];
  minPageSize;
  isLoadingFilterOptions$: Observable<boolean>;

  @ViewChild('confirmSuccessNotificationTemplate', { static: true })
  confirmSuccessNotificationTemplate: TemplateRef<any>;

  constructor(
    private returnInvoiceFacade: ReturnInvoiceFacade,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService,
    private fb: FormBuilder,
    private sortService: SortDialogService,
    private filterService: FilterService,
    private shortcutService: ShortcutService,
    private appsettingFacade: AppsettingFacade
  ) {
    this.searchForm = new FormGroup({
      searchValue: new FormControl('')
    });
    this.shortcutService.commands
      .pipe(takeUntil(this.destroy$))
      .subscribe(command => {
        this.shortcutEventHandler(command);
      });
    // this.filterService.DataSource = {
    //   ...(this.type !== ReturnInvoiceCFAEnum.PRODUCT_CATEGORY
    //     ? PRODUCT_CATEGORIES
    //     : null),
    //   ...(this.type !== ReturnInvoiceCFAEnum.PRODUCT_GROUP
    //     ? PRODUCT_GROUP
    //     : null)
    // };
    this.sortService.DataSource = SORT_DATA;
  }

  ngOnInit() {
    this.componentInit();
    this.stockId = this.activatedRoute.snapshot.params['id'];

    if (this.stockId) {
      this.requestId = Number(this.stockId);
    }
    this.isLoadingFilterOptions$ = this.returnInvoiceFacade.getIsLoadingFilterOptions();
    this.returnInvoiceFacade.loadFilterOptions();
    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(resp => (this.pageSize = JSON.parse(resp)));
    this.issuedItemsPageEvent.pageSize = this.pageSize;

    this.appsettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.pageSizeOptions = data;
        this.minPageSize = this.pageSizeOptions.reduce((a: number, b: number) =>
          a < b ? a : b
        );
      });
    this.appsettingFacade
      .getMaxFilterLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxFilterLimit = data;
      });
    this.appsettingFacade
      .getMaxSortLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxSortLimit = data;
      });
    this.returnInvoiceFacade
      .getFilterOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.filterService.DataSource = {
          ...(this.type !== ReturnInvoiceCFAEnum.PRODUCT_CATEGORY
            ? this.mapToFilterOptions('Product Category', data.productCategory)
            : null),
          ...(this.type !== ReturnInvoiceCFAEnum.PRODUCT_GROUP
            ? this.mapToFilterOptions('Product Group', data.productGroups)
            : null)
        };
      });
    this.getCFAItems();
  }

  ngAfterViewInit(): void {}
  shortcutEventHandler(command: Command): void {
    if (this.searchListRef) {
      this.searchListRef.focus();
    }
  }
  mapToFilterOptions(
    key: string,
    options: FilterOption[]
  ): { [key: string]: Filter[] } {
    return {
      [key]: options.map((option, index) => ({
        id: option.id,
        description: option.description,
        selected: false
      }))
    };
  }
  search(searchData: SearchResponse) {
    this.itemId = searchData.searchValue;
    this.resetPagination.next();
    //this.getCFAItems();
    this.returnInvoiceFacade.selectedProductsSearch({
      id: this.requestId,
      pageIndex: this.issuedItemsPageEvent.pageIndex,
      pageSize: this.issuedItemsPageEvent.pageSize,
      sortBy: this.sortBy,
      sortOrder: this.sortOrder,
      itemId: this.itemId,
      filter: this.filter
    });
  }
  onSearchClear(): void {
    this.clearSearchItems();
  }

  componentInit() {
    this.isLoading$ = this.returnInvoiceFacade.getIsloading();
    this.isSearchingItems$ = this.returnInvoiceFacade.getIsSearching();
    this.returnInvoiceFacade.loadCFAddress();
    this.itemsInCart$ = this.returnInvoiceFacade.getItemsInCart();
    this.CFAItems$ = this.returnInvoiceFacade.getCFAItems();
    this.searchedItems$ = this.returnInvoiceFacade.getSearchedItems();
    this.hasLoaded$ = this.returnInvoiceFacade.getHasLoaded();
    this.hasSearchedItems$ = this.returnInvoiceFacade.getHasSearchResult();
    this.hasSelectedProductsSearchedItems$ = this.returnInvoiceFacade.getHasSelectedProductsSearch();
    this.returnInvoiceFacade
      .getCFACode()
      .pipe(takeUntil(this.destroy$))
      .subscribe((cfaAddress: CFAddress) => {
        this.CFAddress = cfaAddress;
        console.log('address', this.CFAddress);
      });
    //Todo if no CFA is found ->Jobit
    this.returnInvoiceFacade
      .getSelectedProductsSearchCount()
      .pipe(takeUntil(this.destroy$))
      .subscribe(selectedProductsSearchCount => {
        this.selectedProductsSearchCount = selectedProductsSearchCount;
      });

    this.returnInvoiceFacade
      .getSearchCount()
      .pipe(takeUntil(this.destroy$))
      .subscribe(count => {
        this.searchCount = count;
        this.showNotification();
      });
    this.searchedItems$.pipe(takeUntil(this.destroy$)).subscribe(items => {
      if (items.length === 1) {
        this.invoiceItems = [];
        this.invoiceItems.push({
          inventoryId: items[0].id
        });
        this.issueItems();
      }
    });
    this.CFAItems$.pipe(takeUntil(this.destroy$)).subscribe((data: any) => {
      this.ids = [];
      this.itemIds = [];
      if (this.selectForm.value.selectRadioButton === '1') {
        this.selectionAllSubject.next({
          selectCheckbox: true,
          enableCheckbox: false
        });
      } else if (this.selectForm.value.selectRadioButton === '2') {
        this.selectionAllSubject.next({
          selectCheckbox: false,
          enableCheckbox: true
        });
        this.selectForm.patchValue({
          selectRadioButton: null
        });
      }
      data.forEach(element => {
        this.ids.push(element.id);
      });
      if (this.hasSelectedProductsSearch) {
        this.selectionAllSubject.next({
          selectCheckbox: true,
          enableCheckbox: true
        });
        this.itemIds = this.ids;
      }

      this.showNotification();
    });
    this.returnInvoiceFacade
      .getHasRemovedMultipleItems()
      .pipe(takeUntil(this.destroy$))
      .subscribe(hasRemovedMultipleItems => {
        if (hasRemovedMultipleItems) {
          this.clearAll();
          this.clearSearchItems();
          this.getCFAItems();
        }
      });
    this.returnInvoiceFacade
      .getTotalItemsCount()
      .pipe(takeUntil(this.destroy$))
      .subscribe(totalItemsCount => {
        this.totalItemsCount = totalItemsCount;
        this.showNotification();
      });

    this.returnInvoiceFacade
      .getIsRemoving()
      .pipe(takeUntil(this.destroy$))
      .subscribe(isRemoving => {
        this.isRemoving = isRemoving;
      });

    this.returnInvoiceFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe(error => {
        if (error && error.code) {
          this.errorHandler(error);
        }
      });

    this.returnInvoiceFacade
      .getHasItemsIssued()
      .pipe(takeUntil(this.destroy$))
      .subscribe(hasIssued => {
        if (hasIssued) {
          this.searchListRef.reset();
          this.getCFAItems();
        }
      });

    this.returnInvoiceFacade
      .getconfirmedReturnInvoiceCfa()
      .pipe(takeUntil(this.destroy$))
      .subscribe((invoiceNumber: number) => {
        this.invoiceNumber = invoiceNumber;

        if (this.invoiceNumber !== null) {
          this.showConfirmIssueSuccessNotification();
        }
      });
  }
  showNotification() {
    if (this.totalItemsCount > 0 && this.selectedProductsSearchCount === null) {
      this.showConfirmIssueNotifications();
    } else this.overlayNotification.close();
    if (
      this.selectForm.value.selectRadioButton === '1' &&
      this.totalItemsCount > 0
    ) {
      this.removeProductsOverlay('Selected All Products');
    }
    if (this.itemIds.length > 0 && this.totalItemsCount > 0) {
      this.removeProductsOverlay(
        +this.itemIds.length + ' ' + 'Products Selected'
      );
    }
    if (this.totalItemsCount === 0) this.overlayNotification.close();
  }
  removeProductsOverlay(msg: any) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ACTION,
        buttonText: 'Remove Products',
        message: msg
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        if (event.eventType === OverlayNotificationEventType.TRUE) {
          this.returnInvoiceFacade.removeSelectedItems({
            requestId: this.requestId,
            itemIds: this.itemIds
          });
          this.ids = [];
          //this.overlayNotification.close();
        }
      });
  }
  /**
   * Searhes the items by Variant Code
   */
  onSearch(searchResponse: SearchResponse) {
    this.onSearchFlag = true;

    this.returnInvoiceFacade.searchItems({
      variantCode: searchResponse.searchValue,
      lotNumber: searchResponse.lotNumber
    });

    this.showNotification();
  }

  getCFAItems() {
    if (this.stockId || this.requestId || this.CFAddress) {
      this.returnInvoiceFacade.loadItemCFA({
        id: this.requestId,
        pageIndex: this.issuedItemsPageEvent.pageIndex,
        pageSize: this.issuedItemsPageEvent.pageSize,
        sortBy: this.sortBy,
        sortOrder: this.sortOrder,
        itemId: this.itemId,
        filter: this.filter
      });
    }
  }

  addToCart(items: Item[]) {
    this.invoiceItems = [];
    items.forEach(item => {
      this.invoiceItems.push({
        inventoryId: item.id
        // measuredQuantity: item.availableQuantity,
        // measuredWeight: item.availableWeight
      });
    });
    this.issueItems();
    this.searchCount = null;
    this.showNotification();
  }

  /**
   * Updates the item in store
   * @param itemToUpdate:item to update
   */
  updateItem(itemToUpdate: CFAItemToUpdate) {
    this.returnInvoiceFacade.updateSelectedItem({
      itemId: itemToUpdate.itemId,
      requestId: Number(this.stockId),
      newUpdate: itemToUpdate.newUpdate,
      actualUpdate: itemToUpdate.actualDetails
    });
  }
  /**
   *
   */

  issueItems() {
    this.returnInvoiceFacade.createIssueItems({
      id: this.requestId,
      invoiceItems: this.invoiceItems
    });
  }
  /**
   *
   * Handler for overlay notification
   */
  openSortDailog() {
    this.sortService
      .openDialog(this.maxSortLimit, this.sortData)
      .pipe(takeUntil(this.destroy$))
      .subscribe((sortResult: { data: Column[]; actionfrom: string }) => {
        // TODO : Enum
        if (sortResult.actionfrom === 'apply') {
          const sortData = sortResult.data;
          if (sortData == null || sortData.length === 0) {
            this.sortData = [];
            this.sortOrder = null;
            this.sortBy = null;
          } else {
            this.sortData = sortData;
            if (sortData.length > 0) {
              if (sortData[0].id === 0) {
                this.sortBy = 'availableWeight';
              } else if (sortData[0].id === 1) {
                this.sortBy = 'availableQuantity';
              }
              this.sortOrder = sortData[0].sortAscOrder ? 'asc' : 'desc';
            }
          }
          this.issuedItemsPageEvent = this.initailPageEvent;
          this.getCFAItems();
        }
      });
  }
  openFilter() {
    this.filterService
      .openDialog(this.maxFilterLimit, this.filterData)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (filterResult: {
          data: { [key: string]: Filter[] };
          actionfrom: string;
        }) => {
          // TODO : Enum
          if (filterResult.actionfrom === 'apply') {
            const filterData = filterResult.data;
            if (filterData == null) {
              this.filterData = {};
            } else {
              this.filterData = filterData;
            }
            this.filter = [];
            if (filterData) {
              let filterValues = [];
              if (filterData['Source Bin']) {
                filterData['Source Bin'].forEach(value => {
                  filterValues.push(value.description);
                });
                if (filterValues.length > 0) {
                  this.filter.push({
                    key: 'binCode',
                    value: filterValues
                  });
                }
              }
              filterValues = [];
              if (filterData['Product Group']) {
                filterData['Product Group'].forEach(value => {
                  filterValues.push(value.description);
                });
                if (filterValues.length > 0) {
                  this.filter.push({
                    key: 'productGroup',
                    value: filterValues
                  });
                }
              }
              filterValues = [];
              if (filterData['Product Category']) {
                filterData['Product Category'].forEach(value => {
                  filterValues.push(value.description);
                });
                if (filterValues.length > 0) {
                  this.filter.push({
                    key: 'productCategory',
                    value: filterValues
                  });
                }
              }
            }
            this.issuedItemsPageEvent = this.initailPageEvent;
            this.getCFAItems();
          }
        }
      );
  }

  showConfirmIssueNotifications() {
    const key = 'pw.returnInvoiceCFA.confirmIssueMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            message:
              this.totalItemsCount +
              ' ' +
              translatedMessage +
              ' ' +
              this.requestId,
            buttonText: 'CONFIRM ISSUE',
            hasRemarks: true,
            isRemarksMandatory: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              // if (!this.select) {
              //   this.showSelectCFANotifcation();
              // } else {
              this.showProgressNotification();
              this.returnInvoiceFacade.ConfirmIssue({
                id: this.requestId,
                cfadetails: {
                  cfaLocationCode: this.CFAddress.locationCode,
                  remarks: event.data
                }
              });
            }
          });
      });
  }

  showConfirmIssueSuccessNotification() {
    const key = 'pw.returnInvoiceCFA.confirmIssueSuccessMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.CUSTOM,
            hasBackdrop: true,
            hasClose: true,
            message: translatedMessage + ' ' + this.invoiceNumber,
            template: this.confirmSuccessNotificationTemplate
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
            if (event.eventType === OverlayNotificationEventType.CLOSE) {
              this.returnInvoiceFacade.clearSearch();
              this.router.navigate(['inventory/stockissue/cfa']);
            }
          });
      });
  }

  showSelectCFANotifcation() {
    const key = 'pw.returnInvoiceCFA.errorMessageForSelect';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.SIMPLE,
            message: translatedMessage,
            hasClose: true
          })
          .events.subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
            if (event.eventType === OverlayNotificationEventType.CLOSE) {
              this.showNotification();
            }
          });
      });
  }

  showProgressNotification() {
    //  TODO
    const key = 'pw.returnInvoiceCFA.progressMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.PROGRESS,
            message: translatedMsg,
            hasBackdrop: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe(() => {
            this.hasNotification = false;
          });
      });
  }

  /**
   * Removes the item from cart
   * @param item :item to remove
   */
  removeItem(item: Item) {
    if (this.isRemoving === false) {
      this.removeLineItem = [];
      this.removeLineItem.push(item.id);
      this.returnInvoiceFacade.removeSelectedItems({
        requestId: this.requestId,
        itemIds: this.removeLineItem
      });
    }
    this.showNotification();
  }

  change(event) {
    this.select = true;
    this.selected = event.value;
    this.showNotification();
  }
  CFAPaginatedItems(event: PageEvent) {
    this.issuedItemsPageEvent = event;
    if (this.itemId) {
      this.returnInvoiceFacade.selectedProductsSearch({
        id: this.requestId,
        pageIndex: this.issuedItemsPageEvent.pageIndex,
        pageSize: this.issuedItemsPageEvent.pageSize,
        sortBy: this.sortBy,
        sortOrder: this.sortOrder,
        itemId: this.itemId,
        filter: this.filter
      });
    } else this.getCFAItems();
  }

  back() {
    //todo
    this.returnInvoiceFacade.clearSearch();
    this.router.navigate(['inventory/stockissue/cfa']);
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  selectAll() {
    this.itemIds = [];
    this.selectionAllSubject.next({
      selectCheckbox: true,
      enableCheckbox: false
    });
    this.showNotification();
  }
  selectPagewise() {
    this.itemIds = [];
    this.selectionAllSubject.next({
      selectCheckbox: true,
      enableCheckbox: true
    });

    this.itemIds = this.ids;
    this.showNotification();
  }
  selectionEmit(selection: { selected: boolean; id: number }) {
    this.selectForm.patchValue({
      selectRadioButton: null
    });
    if (selection.selected) {
      const index = this.itemIds.indexOf(selection.id);
      if (index === -1) this.itemIds.push(selection.id);
    } else {
      const index = this.itemIds.indexOf(selection.id);
      if (index !== -1) this.itemIds.splice(index, 1);
    }
    this.showNotification();
  }
  selectChange() {
    if (this.selectForm.value.selectRadioButton === '1') {
      this.selectAll();
    } else if (this.selectForm.value.selectRadioButton === '2') {
      this.selectPagewise();
    }
  }
  resetRadiobutton() {
    this.selectForm.patchValue({
      selectRadioButton: null
    });
  }
  clearAll() {
    this.itemIds = [];
    //this.ids = [];
    this.selectForm.patchValue({
      selectRadioButton: null
    });
    this.selectionAllSubject.next({
      selectCheckbox: false,
      enableCheckbox: true
    });
    this.showNotification();
  }
  clearSearchItems() {
    this.itemId = null;
    this.searchForm.reset();
    this.getCFAItems();
    this.clearAll();
    this.returnInvoiceFacade.clearSearch();
  }

  //TODO check is working->Jobit
  errorNaviagtion(msg: string) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.SIMPLE,
        message: msg,
        hasClose: true
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.router.navigate(['inventory/stockissue']);
      });
  }

  errorHandler(error: any) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        this.hasNotification = false;
      });
    this.showNotification();
  }
}
