import { Injectable } from '@angular/core';
import {
  getCreateRequestEndpointUrl,
  getConfirmReturnInvoiceCfaEndpointUrl,
  getSearchReturnInvoiceItemCfaEndpointUrl,
  getCreateIssueItemsEndPointUrl,
  getRemoveSelectedItemsUrl,
  getItemsToCFAUrl,
  getCFAddressUrl,
  getReturnInvoiceToCFAProductGroupsForFilterUrl,
  getReturnInvoiceToCFAProductCatrgoryForFilterUrl,
  getRemoveSelectedItemEndPointUrl
} from '../../../endpoints.constants';
import { map, mergeMap } from 'rxjs/operators';
import { ApiService } from '@poss-web/core';
import { Observable, of } from 'rxjs';
import {
  SearchItemPayloadSuccess,
  CreateIssueItemsPayload,
  LoadItemsPayload
} from '../+state/return-invoice-cfa.actions';
import { ReturnInvoiceToCfaAdaptor } from '../adaptors/return-invoice-to-cfa.adaptor';
import {
  CFAMaster,
  CfaDetails,
  CFAddress,
  FilterData
} from '../models/return-invoice-to-cfa.model';
import { FilterOptionHelper } from '../helpers/return-invoice-cfa-helper';

@Injectable({
  providedIn: 'root'
})
export class ReturnInvoiceToCfaService {
  constructor(private apiService: ApiService) {}
  createReturnRequestToCfa() {
    const url = getCreateRequestEndpointUrl();
    return this.apiService
      .post(url)
      .pipe(
        map((data: any) => ReturnInvoiceToCfaAdaptor.invoiceIdFromJson(data))
      );
  }
  confirmIssueCfa(id: number, cfaDetails: CfaDetails): Observable<number> {
    const url = getConfirmReturnInvoiceCfaEndpointUrl(id);
    return this.apiService
      .patch(url, cfaDetails)
      .pipe(
        map((data: any) =>
          ReturnInvoiceToCfaAdaptor.confirmedIssueInvoiceTocfa(data)
        )
      );
  }
  searchItem(
    variantCode: string,
    lotNumber: string
  ): Observable<SearchItemPayloadSuccess> {
    const url = getSearchReturnInvoiceItemCfaEndpointUrl(
      variantCode,
      lotNumber
    );

    return this.apiService
      .get(url)
      .pipe(map((data: any) => ReturnInvoiceToCfaAdaptor.searchedItems(data)));
  }
  createIssueItems(
    id: number,
    invoiceItems: CreateIssueItemsPayload
  ): Observable<any> {
    const url = getCreateIssueItemsEndPointUrl(id);
    return this.apiService.post(url, {
      invoiceItems: invoiceItems.invoiceItems
    });
  }
  getCFAddress(): Observable<CFAddress> {
    const url = getCFAddressUrl();
    return this.apiService
      .get(url)
      .pipe(map((data: any) => ReturnInvoiceToCfaAdaptor.getCFAddress(data)));
  }
  getItemsCFA(loadItem: LoadItemsPayload) {
    const url = getItemsToCFAUrl(
      loadItem.id,
      loadItem.pageIndex,
      loadItem.pageSize,
      loadItem.sortBy,
      loadItem.sortOrder,
      loadItem.itemId,
      loadItem.filter
    );
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) => ReturnInvoiceToCfaAdaptor.getItemsFromJson(data))
      );
  }
  updateSelectedItem(id: number, itemId: number, quantity: number) {
    const url = getRemoveSelectedItemEndPointUrl(id, itemId);
    return this.apiService
      .patch(url, { inventoryId: itemId, measuredQuantity: quantity })
      .pipe(map((data: any) => ReturnInvoiceToCfaAdaptor.getUpdatedItem(data)));
  }
  removeSelectedItems(id: number, items: number[]) {
    const url = getRemoveSelectedItemsUrl(id);
    return this.apiService.put(url, { itemIds: items });
  }
  // selectedProductsSearch(loadItem: LoadItemsPayload) {
  //   const url = getItemsToCFAUrl(
  //     loadItem.id,
  //     loadItem.pageIndex,
  //     loadItem.pageSize,
  //     loadItem.sortBy,
  //     loadItem.sortOrder,
  //     loadItem.itemId,
  //     loadItem.filter
  //   );
  //   return this.apiService
  //     .get(url)
  //     .pipe(
  //       map((data: any) => ReturnInvoiceToCfaAdaptor.getItemsFromJson(data))
  //     );
  // }
  getFilterOptions(): Observable<FilterData> {
    const productGroupsUrl = getReturnInvoiceToCFAProductGroupsForFilterUrl();
    const productCategroyUrl = getReturnInvoiceToCFAProductCatrgoryForFilterUrl();
    return this.apiService
      .get(productGroupsUrl)
      .pipe(
        map((data: any) => ({
          productGroups: FilterOptionHelper.getOptions(
            data.results,
            'productGroupCode'
          )
        }))
      )
      .pipe(
        mergeMap(productGroup =>
          this.apiService.get(productCategroyUrl).pipe(
            map((data: any) => ({
              ...productGroup,
              productCategory: FilterOptionHelper.getOptions(
                data.results,
                'productCategoryCode'
              )
            }))
          )
        )
      );
  }
}
