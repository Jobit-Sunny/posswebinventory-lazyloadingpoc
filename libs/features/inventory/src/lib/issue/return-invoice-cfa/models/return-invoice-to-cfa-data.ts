import { Filter } from '@poss-web/shared';

export const SORT_DATA = [
  {
    id: 0,
    sortByColumnName: 'Item Weight',
    sortAscOrder: false
  },
  {
    id: 1,
    sortByColumnName: 'Item Quantity',
    sortAscOrder: false
  }
];
export const PRODUCT_CATEGORIES: { [key: string]: Filter[] } = {
  'Product Category': [
    {
      id: 1,
      description: 'Bangle',
      selected: false
    },
    {
      id: 2,
      description: 'Chain',
      selected: false
    },
    {
      id: 3,
      description: 'Ear Ring',
      selected: false
    },
    {
      id: 4,
      description: 'Necklace',
      selected: false
    }
  ]
};
export const PRODUCT_GROUP: { [key: string]: Filter[] } = {
  'Product Group': [
    {
      id: 1,
      description: 'Gold',
      selected: false
    },
    {
      id: 2,
      description: 'Gold Plain',
      selected: false
    },
    {
      id: 3,
      description: 'Plain',
      selected: false
    },
    {
      id: 4,
      description: 'Studded',
      selected: false
    }
  ]
};
export const SOURCE_BIN: { [key: string]: Filter[] } = {
  'Source Bin': [
    {
      id: 1,
      selected: false,
      description: 'BANGLE'
    },
    {
      id: 2,
      selected: false,
      description: 'BANGLE COLOR'
    },
    {
      id: 3,
      selected: false,
      description: 'BEST DEAL'
    },
    {
      id: 4,
      selected: false,
      description: 'EAR RING'
    },
    {
      id: 5,
      selected: false,
      description: 'NECKLACE'
    },
    {
      id: 6,
      selected: false,
      description: 'RING'
    },
    {
      id: 7,
      selected: false,
      description: 'LOSS'
    },
    {
      id: 8,
      selected: false,
      description: 'EXHIBITION'
    },
    {
      id: 9,
      selected: false,
      description: 'LOAN'
    }
  ]
};
