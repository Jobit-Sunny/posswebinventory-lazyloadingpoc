export enum ReturnInvoiceCFATabEnum {
  LIST_OF_PRODUCTS = 'List Of Products',
  SELECTED_PRODUCTS = 'Selected Products'
}
export enum ReturnInvoiceCFAEnum {
  PRODUCT_GROUP = 'productGroup',
  PRODUCT_CATEGORY = 'productCategory',
  BIN_CODE = 'binCode'
}
