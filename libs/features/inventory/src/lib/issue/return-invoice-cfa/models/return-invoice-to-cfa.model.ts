import { Moment } from 'moment';

export interface CfaDetails {
  cfaLocationCode: string;
  remarks: string;
}
export interface CFAMaster {
  CFACode: string;
  CFAName: string;
}
export interface InvoiceItems {
  inventoryId: number;
  // measuredQuantity: number;
  // measuredWeight: number;
}
export interface IssueItemUpdate {
  measuredQuantity: number;
  status: string;
  inventoryId: string;
  measuredWeight: number;
}
export interface CFAItemUpdate {
  quantity: number;
  weight: number;
}
export interface CFAItemToUpdate {
  itemId: number;
  newUpdate: CFAItemUpdate;
  actualDetails: CFAItemUpdate;
}
export interface Item {
  id: number;
  currencyCode: string;
  imageURL: string;
  itemCode: string;
  itemValue: number;
  lotNumber: string;
  mfgDate: Moment;
  orderType: string;
  productCategory: string;
  productGroup: string;
  status: string;
  totalQuantity: number;
  totalValue: number;
  totalWeight: number;
  weightUnit: string;
  measuredQuantity: number;
  measuredWeight: number;
  binCode: string;
  binGroupCode: string;
  itemWeight: number;
  availableQuantity?: number;
  availableWeight?: number;
  availableValue?: number;
  inventoryId?: number;
}
export interface CFAddress {
  locationCode: string;
  brandCode: string;
  townCode: number;
  stateCode: number;
  regionCode: string;
  locationTypeCode: string;
  isActive: boolean;
  address: string;
  phoneNo: string;
  description: string;
}
export interface FilterData {
  productGroups: FilterOption[];
  productCategory: FilterOption[];
}
export interface FilterOption {
  id: number;
  description: string;
}
