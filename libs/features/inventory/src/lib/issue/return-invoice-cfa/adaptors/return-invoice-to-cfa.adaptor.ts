import * as moment from 'moment';
import {
  SearchItemPayloadSuccess,
  LoadItemsSuccessPayload
} from '../+state/return-invoice-cfa.actions';
import { Item, CFAddress } from '../models/return-invoice-to-cfa.model';

export class ReturnInvoiceToCfaAdaptor {
  static invoiceIdFromJson(data: any): number {
    return data.id;
  }
  static confirmedIssueInvoiceTocfa(data: any): number {
    return data.srcDocNo;
  }
  static searchedItems(data: any): SearchItemPayloadSuccess {
    let searchItemPayloadSuccess: SearchItemPayloadSuccess;
    const items: Item[] = [];
    let count: number = null;
    count = data.totalElements;
    for (const item of data.results) {
      items.push({
        id: item.id,
        currencyCode: item.currencyCode,
        binCode: item.binCode,
        binGroupCode: item.binGroupCode,
        imageURL: item.imageURL,
        itemCode: item.itemCode,
        itemValue: item.itemValue,
        lotNumber: item.lotNumber,
        measuredQuantity: item.measuredQuantity,
        measuredWeight: item.measuredWeight,
        mfgDate: moment(item.mfgDate),
        orderType: item.orderType,
        productCategory: item.productCategory,
        productGroup: item.productGroup,
        status: item.status,
        totalQuantity: item.totalQuantity,
        totalValue: item.totalValue,
        totalWeight: item.totalWeight,
        weightUnit: item.weightUnit,
        itemWeight: item.itemWeight,
        availableQuantity: item.availableQuantity,
        availableWeight: item.availableWeight,
        availableValue: item.availableValue
      });
    }

    searchItemPayloadSuccess = { items: items, count: count };

    return searchItemPayloadSuccess;
  }
  static getItemsFromJson(data: any): LoadItemsSuccessPayload {
    let loadItemSuccessPayload: LoadItemsSuccessPayload;
    const items: Item[] = [];
    const count = data.totalElements;
    for (const item of data.results) {
      items.push({
        id: item.id,
        inventoryId: item.inventoryId,
        currencyCode: item.currencyCode,
        binCode: item.binCode,
        binGroupCode: item.binGroupCode,
        imageURL: item.imageURL,
        itemCode: item.itemCode,
        itemValue: item.itemValue,
        lotNumber: item.lotNumber,
        measuredQuantity: item.measuredQuantity,
        measuredWeight: item.measuredWeight,
        mfgDate: moment(item.mfgDate),
        orderType: item.orderType,
        productCategory: item.productCategory,
        productGroup: item.productGroup,
        status: item.status,
        totalQuantity: item.measuredQuantity,
        totalValue: item.measuredValue,
        totalWeight: item.measuredWeight,
        weightUnit: item.weightUnit,
        itemWeight: item.itemWeight,
        availableQuantity: item.availableQuantity,
        availableWeight: item.availableWeight
      });
    }
    loadItemSuccessPayload = { items: items, count: count };
    return loadItemSuccessPayload;
  }
  static getUpdatedItem(item: any): Item {
    const updatedItemResponse: Item = {
      id: item.id,
      currencyCode: item.currencyCode,
      binCode: item.binCode,
      binGroupCode: item.binGroupCode,
      imageURL: item.imageURL,
      itemCode: item.itemCode,
      itemValue: item.itemValue,
      lotNumber: item.lotNumber,
      measuredQuantity: item.measuredQuantity,
      measuredWeight: item.measuredWeight,
      mfgDate: moment(item.mfgDate),
      orderType: item.orderType,
      productCategory: item.productCategory,
      productGroup: item.productGroup,
      status: item.status,
      totalQuantity: item.totalQuantity,
      totalValue: item.totalValue,
      totalWeight: item.totalWeight,
      weightUnit: item.weightUnit,
      itemWeight: item.itemWeight,
      availableQuantity: item.availableQuantity
    };
    return updatedItemResponse;
  }
  static getCFAddress(data: any): CFAddress {
    if (data.cfaDetails === null) {
      return null;
    }
    const CFAdreess: CFAddress = {
      locationCode: data.cfaDetails.locationCode,
      brandCode: data.cfaDetails.brandCode,
      townCode: data.cfaDetails.townCode,
      stateCode: data.cfaDetails.stateCode,
      regionCode: data.cfaDetails.regionCode,
      locationTypeCode: data.cfaDetails.locationTypeCode,
      isActive: data.cfaDetails.isActive,
      address: data.cfaDetails.address,
      phoneNo: data.cfaDetails.phoneNo,
      description: data.cfaDetails.description
    };

    return CFAdreess;
  }
}
