import {
  CfaDetails,
  InvoiceItems,
  Item,
  CFAMaster,
  CFAItemUpdate,
  CFAddress,
  FilterData
} from '../models/return-invoice-to-cfa.model';

import { Action } from '@ngrx/store';
import { CustomErrors } from '@poss-web/core';

/**
 * The interface for Action payload
 */
export interface UpdateCartItemPayload {
  inventoryId: number;
  quantity: number;
  weight: number;
}
export interface UpdateSeletedItemPayload {
  requestId: number;
  itemId: number;
  newUpdate: CFAItemUpdate;
  actualUpdate: CFAItemUpdate;
}
export interface ConfirmIssuePayload {
  id: number;
  cfadetails: CfaDetails;
}

export interface CreateRequestPayload {
  invoiceId: number;
}
export interface CreateIssueItemsPayload {
  id: number;
  invoiceItems: InvoiceItems[];
}
export interface SearchItemPayload {
  variantCode: string;
  lotNumber: string;
  rowNumber?: number;
}
export interface LoadItemsPayload {
  id: number;
  pageSize?: number;
  pageIndex?: number;
  sortBy?: string;
  sortOrder?: string;
  itemId?: string;
  filter?: { key: string; value: any[] }[];
}
export interface SearchItemPayloadSuccess {
  items: Item[];
  count: number;
  rowNumber?: number;
}
export interface LoadItemsSuccessPayload {
  items: Item[];
  count: number;
}
export interface RemoveSelectedItemsPayload {
  requestId: number;
  itemIds: number[];
}
export interface SelectedProductSearchPayload {
  requestId: number;
  itemId: number;
}

/**
 * The  enum defined for  list of actions of return invoice
 */
export enum RetrunInvoiceCfaActionTypes {
  CREATE_REQUEST_TO_CFA = '[return-invoice-cfa] Create Request To Cfa',
  CREATE_REQUEST_TO_CFA_SUCCESS = ' [return-invoice-cfa] Create Request To Cfa Success',
  CREATE_REQUEST_TO_CFA_FAILURE = "[return-invoice-cfa'] Create Request To Cfa Failure",

  CONFIRM_ISSUE = '[return-invoice-cfa] Confirm Issue',
  CONFIRM_ISSUE_SUCCESS = '[return-invoice-cfa] Confirm Issue Success',
  CONFIRM_ISSUE_FAILURE = '[return-invoice-cfa] Confirm Issue Failure',

  SEARCH_ITEM = '[return-invoice-cfa] Search Item',
  SEARCH_ITEM_SUCCESS = '[return-invoice-cfa] Search Item Success',
  SEARCH_ITEM_FAILURE = '[return-invoice-cfa] Search Item Failure',
  CLEAR_SEARCH = '[return-invoce-cfa] Clear Search',

  CREATE_ISSUE_ITEMS = '[return-invoice-cfa] Create Issue Items',
  CREATE_ISSUE_ITEMS_SUCCESS = '[return-invoice-cfa] Create Issue Items Success',
  CREATE_ISSUE_ITEMS_FAILURE = '[return-invoice-cfa] Create Issue Items Failure',

  ADD_ITEMS_TO_CART = '[return-invoice-cfa-item-list] Add Items To Cart',

  LOAD_CFA_LOCATION_CODE = '[return-invoice-cfa] Load CFA Location Code',
  LOAD_CFA_LOCATION_CODE_SUCCESS = '[return-invoice-cfa] Load CFA Location Code Success',
  LOAD_CFA_LOCATION_CODE_FAILURE = '[return-invoice-cfa] Load CFA Location Code Failure',

  LOAD_ITEMS = '[return-invoice-cfa] Load Item ',
  LOAD_ITEM_SUCCESS = '[return-invoice-cfa]Load Item Success',
  LOAD_ITEM_FAILURE = '[return-invoice-cfa]Load Item Failure',

  UPDATE_CART_ITEMS = '[return-invoice-cfa] Update Cart Items',

  REMOVE_CART_ITEM = '[return-invoice-cfa] Remove Cart Item',

  CLEAR_CART_ITEM = '[return-invoice-cfa] Clear Cart Item',
  UPDATE_SELECTED_ITEM = '[return-invoice-cfa] Update Selected Item',
  UPDATE_SELECTED_ITEM_SUCCESS = '[return-invoice-cfa] Update Selected Item Success',
  UPDATE_SELECTED_ITEM_FAILURE = '[return-invoice-cfa] Update Selected Item Failure',

  REMOVE_SELECTED_ITEMS = '[return-invoice-cfa]Remove Selected Items',
  REMOVE_SELECTED_ITEMS_SUCCESS = '[return-invoice-cfa]Remove Selected Items Success',
  REMOVE_SELECTED_ITEMS_FAILURE = '[return-invoice-cfa]Remove Selected Items Failure',

  SELECTED_PRODUCTS_SEARCH = '[return-invoice-cfa] Selected Products Search ',
  SELECTED_PRODUCTS_SEARCH_SUCESSS = '[return-invoice-cfa] Selected Products Search Success',
  SELECTED_PRODUCTS_SEARCH_FAILURE = '[return-invoice-cfa] Search Prodcuts Search Failure',
  CLEAR_SEARCH_ITEMS = '[return-invoce-cfa] Clear Search',
  LOAD_FILTER_OPTIONS = '[ return-invoce-cfa ] Load filter options ',
  LOAD_FILTER_OPTIONS_SUCCESS = '[ return-invoce-cfa ] Load filter options Success ',
  LOAD_FILTER_OPTIONS_FAILURE = '[ return-invoce-cfa ] Load filter options Failure '
}
/**
 * Return Invoice Actions
 */

export class CreateRequestToCfa implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.CREATE_REQUEST_TO_CFA;
}
export class CreateRequestToCfaSuccess implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.CREATE_REQUEST_TO_CFA_SUCCESS;
  constructor(public payload: number) {}
}
export class CreateRequestToCfaFailure implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.CREATE_REQUEST_TO_CFA_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class ConfirmIssue implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.CONFIRM_ISSUE;
  constructor(public payload: ConfirmIssuePayload) {}
}

export class ConfirmIssueSuccess implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.CONFIRM_ISSUE_SUCCESS;
  constructor(public payload: number) {}
}

export class ConfirmIssueFailure implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.CONFIRM_ISSUE_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class SearchItem implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.SEARCH_ITEM;
  constructor(public payload: SearchItemPayload) {}
}

export class SearchItemSuccess implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.SEARCH_ITEM_SUCCESS;

  constructor(public payload: SearchItemPayloadSuccess) {}
}
export class SearchItemFailure implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.SEARCH_ITEM_FAILURE;
  //TODO: check whether type string or Erros
  constructor(public payload: CustomErrors) {}
}

export class ClearSearch implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.CLEAR_SEARCH;
}

export class CreateIssueItems implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.CREATE_ISSUE_ITEMS;
  constructor(public payload: CreateIssueItemsPayload) {}
}

export class CreateIssueItemsSuccess implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.CREATE_ISSUE_ITEMS_SUCCESS;
  constructor(public payload: CreateIssueItemsPayload) {}
}

export class CreateIssueItemsFailure implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.CREATE_ISSUE_ITEMS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class AddItemsToCart implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.ADD_ITEMS_TO_CART;
  constructor(public payload: Item[]) {}
}

export class LoadCFALocationCode implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.LOAD_CFA_LOCATION_CODE;
}
export class LoadCFALocationCodeSuccess implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.LOAD_CFA_LOCATION_CODE_SUCCESS;
  constructor(public payload: CFAddress) {}
}
export class LoadCFALocationCodeFailure implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.LOAD_CFA_LOCATION_CODE_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class UpdateCartItems implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.UPDATE_CART_ITEMS;
  constructor(public payload: UpdateCartItemPayload) {}
}
export class UpdateSelectedItem implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.UPDATE_SELECTED_ITEM;
  constructor(public payload: UpdateSeletedItemPayload) {}
}
export class UpdateSelectedItemSuccess implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.UPDATE_SELECTED_ITEM_SUCCESS;
  constructor(public payload: Item) {}
}
export class UpdateSelectedItemFailure implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.UPDATE_SELECTED_ITEM_FAILURE;
  constructor(public payload: UpdateSeletedItemPayload) {}
}

export class RemoveCartItem implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.REMOVE_CART_ITEM;
  constructor(public payload: Item) {}
}

export class LoadItems implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.LOAD_ITEMS;
  constructor(public payload: LoadItemsPayload) {}
}

export class LoadItemSuccess implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.LOAD_ITEM_SUCCESS;
  constructor(public payload: LoadItemsSuccessPayload) {}
}
export class LoadItemsFailure implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.LOAD_ITEM_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export class ClearCart implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.CLEAR_CART_ITEM;
}
export class RemoveSelectedItems implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.REMOVE_SELECTED_ITEMS;
  constructor(public payload: RemoveSelectedItemsPayload) {}
}
export class RemoveSelectedItemsSuccess implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.REMOVE_SELECTED_ITEMS_SUCCESS;
}
export class RemoveSelectedItemsFailure implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.REMOVE_SELECTED_ITEMS_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export class SelectedProductsSearch implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.SELECTED_PRODUCTS_SEARCH;
  constructor(public payload: LoadItemsPayload) {}
}

export class SelectedProdutsSearchSuccess implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.SELECTED_PRODUCTS_SEARCH_SUCESSS;
  constructor(public payload: LoadItemsSuccessPayload) {}
}
export class SelectedProductsSearchFailure implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.SELECTED_PRODUCTS_SEARCH_FAILURE;
  //TODO: check whether type string or Erros
  constructor(public payload: CustomErrors) {}
}
export class LoadFilterOptions implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.LOAD_FILTER_OPTIONS;
}
export class LoadFilterOptionsSuccess implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.LOAD_FILTER_OPTIONS_SUCCESS;
  constructor(public payload: FilterData) {}
}
export class LoadFilterOptionsFailure implements Action {
  readonly type = RetrunInvoiceCfaActionTypes.LOAD_FILTER_OPTIONS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

/**
 * Return Invoice Action types
 */
export type RetrunInvoiceCfaActions =
  | CreateRequestToCfa
  | CreateRequestToCfaSuccess
  | CreateRequestToCfaFailure
  | ConfirmIssue
  | ConfirmIssueSuccess
  | ConfirmIssueFailure
  | SearchItem
  | SearchItemSuccess
  | SearchItemFailure
  | ClearSearch
  | CreateIssueItems
  | CreateIssueItemsSuccess
  | CreateIssueItemsFailure
  | AddItemsToCart
  | LoadCFALocationCode
  | LoadCFALocationCodeSuccess
  | LoadCFALocationCodeFailure
  | UpdateCartItems
  | UpdateSelectedItem
  | UpdateSelectedItemSuccess
  | UpdateSelectedItemFailure
  | LoadItems
  | LoadItemSuccess
  | LoadItemsFailure
  | ClearCart
  | RemoveCartItem
  | RemoveSelectedItems
  | RemoveSelectedItemsSuccess
  | RemoveSelectedItemsFailure
  | SelectedProductsSearch
  | SelectedProdutsSearchSuccess
  | SelectedProductsSearchFailure
  | LoadFilterOptions
  | LoadFilterOptionsSuccess
  | LoadFilterOptionsFailure;
