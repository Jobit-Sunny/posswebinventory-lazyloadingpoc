import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { Item } from '../models/return-invoice-to-cfa.model';

export interface ItemEntity extends EntityState<Item> {}

export const itemAdaptor = createEntityAdapter<Item>({
  selectId: item => item.id
});

export const itemSelector = itemAdaptor.getSelectors();
