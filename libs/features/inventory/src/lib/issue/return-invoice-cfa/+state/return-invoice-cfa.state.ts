import {
  CfaDetails,
  CFAMaster,
  CFAddress,
  FilterData
} from '../models/return-invoice-to-cfa.model';

import { CreateIssueItemsPayload } from './return-invoice-cfa.actions';
import { ItemEntity } from './return-invoice-cfa-entity';
import { CustomErrors } from '@poss-web/core';
import { NumericDictionary } from 'lodash';

export interface ReturnInvoiceCfaState {
  newRequestId: number;
  hasError?: string;
  invoiceNumber: number;
  confirmedReturnInvoiceCfaError: CustomErrors;
  hasSearchedItem?: boolean;
  isLoading?: boolean;
  searchedItems: ItemEntity;
  itemsInCarts: ItemEntity;
  loadedItems: ItemEntity;
  sortedItems: ItemEntity;
  CFAddress: CFAddress;
  invoiceItems: CreateIssueItemsPayload;
  hasCartProducts: boolean;
  totalItemCount: number;
  hasRemoved?: boolean;
  hasIssued?: boolean;
  isSearching?: boolean;
  hasloaded?: boolean;
  error: CustomErrors;
  hasUpdated?: boolean;
  isRemoving?: boolean;
  hasSorted?: boolean;
  hasRemovedMultipleItems: boolean;
  hasRemovedBulkItems: boolean;
  selectedProductsSearchResult: ItemEntity;
  hasSelectedSearch: boolean;
  hasSelectedProductsSearch: boolean;
  selectedProductsSearchCount: number;
  searchCount: number;
  isSelectedProductsSearch: boolean;
  filterOptions: FilterData;
  isLoadingFilterOptions: boolean;
}
