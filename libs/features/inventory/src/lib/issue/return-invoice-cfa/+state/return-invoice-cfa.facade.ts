import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../../inventory.state';
import * as RetrunInvoiceCfaAction from '../+state/return-invoice-cfa.actions';
import {
  ConfirmIssuePayload,
  SearchItemPayload,
  CreateIssueItemsPayload,
  UpdateCartItemPayload,
  UpdateSeletedItemPayload,
  RemoveSelectedItemsPayload,
  LoadItemsPayload
} from '../+state/return-invoice-cfa.actions';
import { ReturnInvoiceCfaSelectors } from './return-invoice-cfa.selectors';
import { Item } from '../models/return-invoice-to-cfa.model';

/**
 * Return Invoice Facade for accesing Return-Invoice-State
 * */
@Injectable()
export class ReturnInvoiceFacade {
  constructor(private store: Store<State>) {}

  private newRequestId$ = this.store.select(
    ReturnInvoiceCfaSelectors.selectNewRequestId
  );
  private confirmedReturnInvoiceCfa$ = this.store.select(
    ReturnInvoiceCfaSelectors.selectConfirmedReturnInvoiceCfa
  );

  private confirmedReturnInvoiceCfaError$ = this.store.select(
    ReturnInvoiceCfaSelectors.selectConfirmedReturnInvoiceCfaError
  );

  private error$ = this.store.select(ReturnInvoiceCfaSelectors.selectError);
  private hasLoaded$ = this.store.select(
    ReturnInvoiceCfaSelectors.selectHasLoaded
  );

  private isLoading$ = this.store.select(
    ReturnInvoiceCfaSelectors.selectisLoading
  );
  private hasSearcheResult$ = this.store.select(
    ReturnInvoiceCfaSelectors.selectHasSearchResult
  );
  private searchedItems$ = this.store.select(
    ReturnInvoiceCfaSelectors.selectSearchedItems
  );
  private issueItemSuccess$ = this.store.select(
    ReturnInvoiceCfaSelectors.selectHasIssued
  );
  private itemsInCarts$ = this.store.select(
    ReturnInvoiceCfaSelectors.selectItemsInCart
  );
  private cfaCode$ = this.store.select(ReturnInvoiceCfaSelectors.selectCFA);

  private selectCartItemIds$ = this.store.select(
    ReturnInvoiceCfaSelectors.selectCartItemIds
  );

  private selectCFAItems$ = this.store.select(
    ReturnInvoiceCfaSelectors.selectCFAItems
  );

  private selectTotalItemsCount$ = this.store.select(
    ReturnInvoiceCfaSelectors.selectTotalItemCount
  );
  private isSearchResult$ = this.store.select(
    ReturnInvoiceCfaSelectors.selectIsSearchResult
  );
  private isRemoving$ = this.store.select(
    ReturnInvoiceCfaSelectors.selectIsRemoving
  );
  private hasUpdated$ = this.store.select(
    ReturnInvoiceCfaSelectors.selectHasUpdated
  );
  private hasRemovedMultipleItems = this.store.select(
    ReturnInvoiceCfaSelectors.selectHasRemovedMultipleItes
  );
  private cartItemsIds = this.store.select(
    ReturnInvoiceCfaSelectors.selectCartItemIds
  );
  private hasSelectedProductsSearch = this.store.select(
    ReturnInvoiceCfaSelectors.selecteHasSelectedProductsSearch
  );
  private selectedProductsSearchCount = this.store.select(
    ReturnInvoiceCfaSelectors.selectSelectedProductsSearchCount
  );
  private selectSearchhCount = this.store.select(
    ReturnInvoiceCfaSelectors.selectSearchCount
  );
  private filterOptions$ = this.store.select(
    ReturnInvoiceCfaSelectors.selectFilterOptions
  );

  private isLoadingFilterOptions$ = this.store.select(
    ReturnInvoiceCfaSelectors.selectIsLoadingFilterOptions
  );
  /**
   * Access for state selectors
   */

  getNewRequestId() {
    return this.newRequestId$;
  }
  getSelectedProductsSearchCount() {
    return this.selectedProductsSearchCount;
  }
  getHasSelectedProductsSearch() {
    return this.hasSelectedProductsSearch;
  }
  getCartItemsIds() {
    return this.cartItemsIds;
  }
  getHasRemovedMultipleItems() {
    return this.hasRemovedMultipleItems;
  }
  getHasUpdated() {
    return this.hasUpdated$;
  }
  getHasLoaded() {
    return this.hasLoaded$;
  }
  getIsRemoving() {
    return this.isRemoving$;
  }

  getCFAItems() {
    return this.selectCFAItems$;
  }
  getTotalItemsCount() {
    return this.selectTotalItemsCount$;
  }

  getCartItemIds() {
    return this.selectCartItemIds$;
  }

  getCFACode() {
    return this.cfaCode$;
  }

  getconfirmedReturnInvoiceCfa() {
    return this.confirmedReturnInvoiceCfa$;
  }
  getconfirmedReturnInvoiceCfaError() {
    return this.confirmedReturnInvoiceCfaError$;
  }

  getError() {
    return this.error$;
  }

  getIsloading() {
    return this.isLoading$;
  }

  getHasSearchResult() {
    return this.hasSearcheResult$;
  }

  getSearchedItems() {
    return this.searchedItems$;
  }

  getHasItemsIssued() {
    return this.issueItemSuccess$;
  }

  getItemsInCart() {
    return this.itemsInCarts$;
  }
  getIsSearching() {
    return this.isSearchResult$;
  }
  getSearchCount() {
    return this.selectSearchhCount;
  }
  getFilterOptions() {
    return this.filterOptions$;
  }
  getIsLoadingFilterOptions() {
    return this.isLoadingFilterOptions$;
  }

  /**
 *Dispatch an action for remove cart Item
 @param item: payload of item to remove
 */

  removeCartItem(item: Item) {
    this.store.dispatch(new RetrunInvoiceCfaAction.RemoveCartItem(item));
  }
  selectedProductsSearch(loadedItemsPayload: LoadItemsPayload) {
    this.store.dispatch(
      new RetrunInvoiceCfaAction.SelectedProductsSearch(loadedItemsPayload)
    );
  }

  /**
   * Dispatch an Action for update cart Item
   * @param updateItemPayload : payload to update
   */
  updateCartItem(updateCartItemPayload: UpdateCartItemPayload) {
    this.store.dispatch(
      new RetrunInvoiceCfaAction.UpdateCartItems(updateCartItemPayload)
    );
  }

  /**
   * Dispatch an Action to Load CFA
   */
  loadCFAddress() {
    this.store.dispatch(new RetrunInvoiceCfaAction.LoadCFALocationCode());
  }

  loadItemCFA(loadItem: RetrunInvoiceCfaAction.LoadItemsPayload) {
    this.store.dispatch(new RetrunInvoiceCfaAction.LoadItems(loadItem));
  }
  /**
   * Dispatch an Action to additems to cart
   * @param items :Payload of items
   */
  addItemsToCart(items: Item[]) {
    this.store.dispatch(new RetrunInvoiceCfaAction.AddItemsToCart(items));
  }
  updateSelectedItem(updateItem: UpdateSeletedItemPayload) {
    this.store.dispatch(
      new RetrunInvoiceCfaAction.UpdateSelectedItem(updateItem)
    );
  }

  /**
   * Dispatch an Action to Create Request
   */
  CreateRequestToCfa() {
    this.store.dispatch(new RetrunInvoiceCfaAction.CreateRequestToCfa());
  }

  /**
   *Dispatch an Action to Confirm Issue
   * @param confirmIssuePayload :Payload of confirm issue
   */
  ConfirmIssue(confirmIssuePayload: ConfirmIssuePayload) {
    this.store.dispatch(
      new RetrunInvoiceCfaAction.ConfirmIssue(confirmIssuePayload)
    );
  }

  /**
   * Dispatch an Action for searching Items
   * @param searchItemPayload:Payload for serach item
   */
  searchItems(searchItemPayload: SearchItemPayload) {
    this.store.dispatch(
      new RetrunInvoiceCfaAction.SearchItem(searchItemPayload)
    );
  }
  removeSelectedItems(removeItems: RemoveSelectedItemsPayload) {
    this.store.dispatch(
      new RetrunInvoiceCfaAction.RemoveSelectedItems(removeItems)
    );
  }
  /**
   * Dispatch an Action for creating itms to issue
   * @param createIssueItems :payload issue items
   */
  createIssueItems(createIssueItems: CreateIssueItemsPayload) {
    this.store.dispatch(
      new RetrunInvoiceCfaAction.CreateIssueItems(createIssueItems)
    );
  }

  clearCart() {
    this.store.dispatch(new RetrunInvoiceCfaAction.ClearCart());
  }
  /**
   * Dispacth an Action for clearing search
   */
  clearSearch() {
    this.store.dispatch(new RetrunInvoiceCfaAction.ClearSearch());
  }
  loadFilterOptions() {
    this.store.dispatch(new RetrunInvoiceCfaAction.LoadFilterOptions());
  }
}
