import { createSelector } from '@ngrx/store';
import { selectInventory } from '../../../inventory.state';
import { itemSelector } from './return-invoice-cfa-entity';

/**
 * The selectors for Return-Invoice store
 */

const selectNewRequestId = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.newRequestId
);

const selectConfirmedReturnInvoiceCfa = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.invoiceNumber
);

const selectTotalItemCount = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.totalItemCount
);
const selectHasUpdated = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.hasUpdated
);
const selectConfirmedReturnInvoiceCfaError = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.confirmedReturnInvoiceCfaError
);

const selectError = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.error
);
const sortedItems = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.loadedItems
);
const selectSortedItems = createSelector(
  sortedItems,
  itemSelector.selectAll
);
const selectisLoading = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.isLoading
);
const selectHasSearchResult = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.hasSearchedItem
);

const searchedItems = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.searchedItems
);

const selectSearchedItems = createSelector(
  searchedItems,
  itemSelector.selectAll
);

const selectHasIssued = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.hasIssued
);
const selectHasLoaded = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.hasloaded
);

const itemsInCarts = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.itemsInCarts
);
const selectItemsInCart = createSelector(
  itemsInCarts,
  itemSelector.selectAll
);

const selectCFA = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.CFAddress
);

const selectCartItemIds = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.itemsInCarts.ids
);

const loadedItems = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.loadedItems
);
const selectCFAItems = createSelector(
  loadedItems,
  itemSelector.selectAll
);

const selectHasRemoved = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.hasRemoved
);
const selectIsRemoving = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.isRemoving
);
const selectIsSearchResult = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.isSearching
);
const selectHasRemovedMultipleItes = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.hasRemovedMultipleItems
);
const selectCartItemsIds = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.itemsInCarts.ids
);
const selecteHasSelectedProductsSearch = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.hasSelectedProductsSearch
);
const selectSelectedProductsSearchCount = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.selectedProductsSearchCount
);
const selectSearchCount = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.searchCount
);
const selectFilterOptions = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.filterOptions
);

const selectIsLoadingFilterOptions = createSelector(
  selectInventory,
  state => state.returnInvoiceCfa.isLoadingFilterOptions
);

export const ReturnInvoiceCfaSelectors = {
  selectNewRequestId,
  selectConfirmedReturnInvoiceCfa,
  selectConfirmedReturnInvoiceCfaError,
  selectError,
  selectisLoading,
  selectHasSearchResult,
  selectSearchedItems,
  selectHasIssued,
  selectItemsInCart,
  selectCFA,
  selectCartItemIds,
  selectCFAItems,
  selectTotalItemCount,
  selectHasRemoved,
  selectIsSearchResult,
  selectHasLoaded,
  selectIsRemoving,
  selectHasRemovedMultipleItes,
  selectHasUpdated,
  selectSortedItems,
  selectCartItemsIds,
  selecteHasSelectedProductsSearch,
  selectSelectedProductsSearchCount,
  selectSearchCount,
  selectFilterOptions,
  selectIsLoadingFilterOptions
};
