import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { DataPersistence } from '@nrwl/angular';
import {
  RetrunInvoiceCfaActionTypes,
  SearchItemPayloadSuccess,
  LoadItemsSuccessPayload
} from './return-invoice-cfa.actions';
import * as RetrunInvoiceCfaAction from './return-invoice-cfa.actions';
import { NotificationService, CustomErrors } from '@poss-web/core';
import {
  CfaDetails,
  CFAMaster,
  Item
} from '../models/return-invoice-to-cfa.model';
import { HttpErrorResponse } from '@angular/common/http';
import { CustomErrorAdaptor } from '@poss-web/core';
import { ReturnInvoiceToCfaService } from '../services/return-invoice-cfa.service';
import { CFAddress } from '../models/return-invoice-to-cfa.model';

@Injectable()
export class ReturnInvoiceEffect {
  constructor(
    public dataPersistence: DataPersistence<any>,
    public returnInvoiceToCfaService: ReturnInvoiceToCfaService,
    public notificationService: NotificationService
  ) {}

  /**
   * The effect which handles the  createRequestTocfa Action
   */
  @Effect()
  createRequestTocfa$: Observable<Action> = this.dataPersistence.fetch(
    RetrunInvoiceCfaActionTypes.CREATE_REQUEST_TO_CFA,
    {
      run: (action: RetrunInvoiceCfaAction.CreateRequestToCfa) => {
        return this.returnInvoiceToCfaService
          .createReturnRequestToCfa()
          .pipe(
            map(
              (invoicenumber: number) =>
                new RetrunInvoiceCfaAction.CreateRequestToCfaSuccess(
                  invoicenumber
                )
            )
          );
      },
      onError: (
        action: RetrunInvoiceCfaAction.CreateRequestToCfa,
        error: HttpErrorResponse
      ) => {
        return new RetrunInvoiceCfaAction.CreateRequestToCfaFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  /**
   * The effect which handles the  confirmIssue Action
   */

  @Effect() confirmIssue$: Observable<
    Action
  > = this.dataPersistence.pessimisticUpdate(
    RetrunInvoiceCfaActionTypes.CONFIRM_ISSUE,
    {
      run: (action: RetrunInvoiceCfaAction.ConfirmIssue) => {
        return this.returnInvoiceToCfaService
          .confirmIssueCfa(action.payload.id, action.payload.cfadetails)
          .pipe(
            map(
              (invoicenumber: number) =>
                new RetrunInvoiceCfaAction.ConfirmIssueSuccess(invoicenumber)
            )
          );
      },
      onError: (
        action: RetrunInvoiceCfaAction.ConfirmIssue,
        error: HttpErrorResponse
      ) => {
        return new RetrunInvoiceCfaAction.ConfirmIssueFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  /**
   * The effect which handles the  searchItem Action
   */
  @Effect() searchItem$: Observable<Action> = this.dataPersistence.fetch(
    RetrunInvoiceCfaActionTypes.SEARCH_ITEM,
    {
      run: (action: RetrunInvoiceCfaAction.SearchItem) => {
        return this.returnInvoiceToCfaService
          .searchItem(action.payload.variantCode, action.payload.lotNumber)
          .pipe(
            map(
              (searchItemPayloadSuccess: SearchItemPayloadSuccess) =>
                new RetrunInvoiceCfaAction.SearchItemSuccess({
                  items: searchItemPayloadSuccess.items,
                  count: searchItemPayloadSuccess.count,
                  rowNumber: action.payload.rowNumber
                })
            )
          );
      },
      onError: (
        action: RetrunInvoiceCfaAction.SearchItem,
        error: HttpErrorResponse
      ) => {
        return new RetrunInvoiceCfaAction.SearchItemFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  /**
   * The effect which handles the  CreateIssueItems Action
   */
  @Effect() CreateIssueItems$: Observable<
    Action
  > = this.dataPersistence.pessimisticUpdate(
    RetrunInvoiceCfaActionTypes.CREATE_ISSUE_ITEMS,
    {
      run: (action: RetrunInvoiceCfaAction.CreateIssueItems) => {
        return this.returnInvoiceToCfaService
          .createIssueItems(action.payload.id, action.payload)
          .pipe(
            map(
              () =>
                new RetrunInvoiceCfaAction.CreateIssueItemsSuccess(
                  action.payload
                )
            )
          );
      },

      onError: (
        action: RetrunInvoiceCfaAction.CreateIssueItems,
        error: HttpErrorResponse
      ) => {
        return new RetrunInvoiceCfaAction.CreateIssueItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  /**
   * The effect which handles the  loadCFALocationCode Action
   */

  @Effect()
  loadCFALocationCode$ = this.dataPersistence.fetch(
    RetrunInvoiceCfaActionTypes.LOAD_CFA_LOCATION_CODE,
    {
      run: (action: RetrunInvoiceCfaAction.LoadCFALocationCode) => {
        return this.returnInvoiceToCfaService
          .getCFAddress()
          .pipe(
            map(
              (cfaAddress: CFAddress) =>
                new RetrunInvoiceCfaAction.LoadCFALocationCodeSuccess(
                  cfaAddress
                )
            )
          );
      },

      onError: (
        action: RetrunInvoiceCfaAction.LoadCFALocationCode,
        error: HttpErrorResponse
      ) => {
        return new RetrunInvoiceCfaAction.LoadCFALocationCodeFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() LoadItem$ = this.dataPersistence.fetch(
    RetrunInvoiceCfaActionTypes.LOAD_ITEMS,
    {
      run: (action: RetrunInvoiceCfaAction.LoadItems) => {
        return this.returnInvoiceToCfaService
          .getItemsCFA(action.payload)
          .pipe(
            map(
              (loadItemSuccessPayload: LoadItemsSuccessPayload) =>
                new RetrunInvoiceCfaAction.LoadItemSuccess(
                  loadItemSuccessPayload
                )
            )
          );
      },

      onError: (
        action: RetrunInvoiceCfaAction.LoadItems,
        error: HttpErrorResponse
      ) => {
        return new RetrunInvoiceCfaAction.LoadItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect() updateSelectedItem$: Observable<
    Action
  > = this.dataPersistence.pessimisticUpdate(
    RetrunInvoiceCfaActionTypes.UPDATE_SELECTED_ITEM,
    {
      run: (action: RetrunInvoiceCfaAction.UpdateSelectedItem) => {
        return this.returnInvoiceToCfaService
          .updateSelectedItem(
            action.payload.requestId,
            action.payload.itemId,
            action.payload.newUpdate.quantity
          )
          .pipe(
            map(
              (updatedItem: Item) =>
                new RetrunInvoiceCfaAction.UpdateSelectedItemSuccess(
                  updatedItem
                )
            )
          );
      },
      onError: (
        action: RetrunInvoiceCfaAction.UpdateSelectedItem,
        error: HttpErrorResponse
      ) => {
        return new RetrunInvoiceCfaAction.UpdateSelectedItemFailure(
          action.payload
        );
      }
    }
  );
  @Effect() RemoveMultipleItems$: Observable<
    Action
  > = this.dataPersistence.pessimisticUpdate(
    RetrunInvoiceCfaActionTypes.REMOVE_SELECTED_ITEMS,
    {
      run: (action: RetrunInvoiceCfaAction.RemoveSelectedItems) => {
        return this.returnInvoiceToCfaService
          .removeSelectedItems(action.payload.requestId, action.payload.itemIds)
          .pipe(
            map(() => new RetrunInvoiceCfaAction.RemoveSelectedItemsSuccess())
          );
      },
      onError: (
        action: RetrunInvoiceCfaAction.RemoveSelectedItems,
        error: HttpErrorResponse
      ) => {
        return new RetrunInvoiceCfaAction.RemoveSelectedItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect() SelectedProductsSearch$: Observable<
    Action
  > = this.dataPersistence.pessimisticUpdate(
    RetrunInvoiceCfaActionTypes.SELECTED_PRODUCTS_SEARCH,
    {
      run: (action: RetrunInvoiceCfaAction.SelectedProductsSearch) => {
        return this.returnInvoiceToCfaService
          .getItemsCFA(action.payload)
          .pipe(
            map(
              (loadItemSuccessPayload: LoadItemsSuccessPayload) =>
                new RetrunInvoiceCfaAction.SelectedProdutsSearchSuccess(
                  loadItemSuccessPayload
                )
            )
          );
      },
      onError: (
        action: RetrunInvoiceCfaAction.SelectedProductsSearch,
        error: HttpErrorResponse
      ) => {
        return new RetrunInvoiceCfaAction.SelectedProductsSearchFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect() loadFilterOptions$ = this.dataPersistence.fetch(
    RetrunInvoiceCfaActionTypes.LOAD_FILTER_OPTIONS,
    {
      run: (action: RetrunInvoiceCfaAction.LoadFilterOptions) => {
        return this.returnInvoiceToCfaService
          .getFilterOptions()
          .pipe(
            map(
              (data: any) =>
                new RetrunInvoiceCfaAction.LoadFilterOptionsSuccess(data)
            )
          );
      },

      onError: (
        action: RetrunInvoiceCfaAction.LoadFilterOptions,
        error: HttpErrorResponse
      ) => {
        return new RetrunInvoiceCfaAction.LoadFilterOptionsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
}
