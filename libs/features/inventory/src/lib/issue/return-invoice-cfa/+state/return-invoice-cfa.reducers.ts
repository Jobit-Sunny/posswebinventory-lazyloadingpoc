import { ReturnInvoiceCfaState } from './return-invoice-cfa.state';
import {
  RetrunInvoiceCfaActions,
  RetrunInvoiceCfaActionTypes
} from './return-invoice-cfa.actions';
import { itemAdaptor } from './return-invoice-cfa-entity';

/**
 * Initial state of the store
 */
const initialState: ReturnInvoiceCfaState = {
  newRequestId: null,
  loadedItems: itemAdaptor.getInitialState(),
  sortedItems: itemAdaptor.getInitialState(),
  hasError: '',
  isLoading: false,
  hasSearchedItem: null,
  searchedItems: itemAdaptor.getInitialState(),
  invoiceNumber: null,
  confirmedReturnInvoiceCfaError: null,
  hasIssued: null,
  itemsInCarts: itemAdaptor.getInitialState(),
  CFAddress: null,
  invoiceItems: null,
  hasCartProducts: false,
  totalItemCount: 0,
  selectedProductsSearchCount: null,
  hasRemoved: null,
  isSearching: false,
  hasSelectedSearch: false,
  hasloaded: false,
  error: null,
  hasUpdated: null,
  isRemoving: false,
  hasSorted: false,
  hasRemovedMultipleItems: null,
  hasRemovedBulkItems: null,
  selectedProductsSearchResult: null,
  hasSelectedProductsSearch: null,
  searchCount: null,
  isSelectedProductsSearch: false,
  filterOptions: {
    productGroups: [],
    productCategory: []
  },

  isLoadingFilterOptions: false
};

/**
 * The reducer function which manipulates the store for respective Action
 */
export function returnInvoiceCfaReducer(
  state: ReturnInvoiceCfaState = initialState,
  action: RetrunInvoiceCfaActions
): ReturnInvoiceCfaState {
  switch (action.type) {
    case RetrunInvoiceCfaActionTypes.LOAD_CFA_LOCATION_CODE:
      return {
        ...state,
        CFAddress: null
      };
    case RetrunInvoiceCfaActionTypes.LOAD_CFA_LOCATION_CODE_SUCCESS:
      console.log('cfa', action.payload);
      return {
        ...state,
        CFAddress: action.payload
      };
    case RetrunInvoiceCfaActionTypes.CONFIRM_ISSUE_FAILURE:
    case RetrunInvoiceCfaActionTypes.CREATE_REQUEST_TO_CFA_FAILURE:
    case RetrunInvoiceCfaActionTypes.LOAD_CFA_LOCATION_CODE_FAILURE:
      return {
        ...state,
        error: action.payload
      };
    case RetrunInvoiceCfaActionTypes.CREATE_REQUEST_TO_CFA:
      return { ...state, newRequestId: null };
    case RetrunInvoiceCfaActionTypes.CREATE_REQUEST_TO_CFA_SUCCESS:
      return {
        ...state,
        newRequestId: action.payload
      };
    // case RetrunInvoiceCfaActionTypes.CREATE_REQUEST_TO_CFA_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload
    //   };

    case RetrunInvoiceCfaActionTypes.CONFIRM_ISSUE:
      return {
        ...state,
        invoiceNumber: null
      };

    case RetrunInvoiceCfaActionTypes.CONFIRM_ISSUE_SUCCESS:
      return {
        ...state,
        invoiceNumber: action.payload
      };

    // case RetrunInvoiceCfaActionTypes.CONFIRM_ISSUE_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload
    //   };

    case RetrunInvoiceCfaActionTypes.SEARCH_ITEM:
      return {
        ...state,
        isSearching: true,
        hasSearchedItem: false,
        hasError: null
      };
    case RetrunInvoiceCfaActionTypes.SEARCH_ITEM_SUCCESS:
      console.log('searched items', action.payload);
      return {
        ...state,
        isSearching: false,
        hasSearchedItem: true,
        searchedItems: itemAdaptor.addAll(
          action.payload.items,
          state.searchedItems
        ),
        searchCount: action.payload.count
      };

    case RetrunInvoiceCfaActionTypes.SEARCH_ITEM_FAILURE:
      return {
        ...state,
        isSearching: false,
        hasSearchedItem: false,
        error: action.payload
      };
    case RetrunInvoiceCfaActionTypes.CREATE_ISSUE_ITEMS:
      return {
        ...state,
        hasIssued: null
      };
    case RetrunInvoiceCfaActionTypes.CREATE_ISSUE_ITEMS_SUCCESS:
      return {
        ...state,
        hasIssued: true,
        itemsInCarts: itemAdaptor.removeAll(state.itemsInCarts)
      };
    case RetrunInvoiceCfaActionTypes.CREATE_ISSUE_ITEMS_FAILURE:
      return {
        ...state,
        hasIssued: false,
        error: action.payload
      };

    case RetrunInvoiceCfaActionTypes.UPDATE_SELECTED_ITEM:
      return {
        ...state,
        hasUpdated: false,
        loadedItems: itemAdaptor.updateOne(
          {
            id: action.payload.itemId,
            changes: {
              totalQuantity: action.payload.newUpdate.quantity,
              totalWeight: action.payload.newUpdate.weight
            }
          },
          state.loadedItems
        )
      };
    case RetrunInvoiceCfaActionTypes.UPDATE_SELECTED_ITEM_SUCCESS:
      return {
        ...state,
        hasUpdated: true,
        loadedItems: itemAdaptor.updateOne(
          {
            id: action.payload.id,
            changes: {
              totalQuantity: action.payload.totalQuantity,
              totalWeight: action.payload.totalWeight
            }
          },
          state.loadedItems
        )
      };
    case RetrunInvoiceCfaActionTypes.UPDATE_SELECTED_ITEM_FAILURE:
      return {
        ...state,
        hasUpdated: false,
        loadedItems: itemAdaptor.updateOne(
          {
            id: action.payload.itemId,
            changes: {
              totalQuantity: action.payload.actualUpdate.quantity,
              totalWeight: action.payload.actualUpdate.weight
            }
          },
          state.loadedItems
        )
      };

    case RetrunInvoiceCfaActionTypes.CLEAR_SEARCH:
      // const searchClear = itemAdaptor.removeAll(state.searchedItems);

      return {
        ...state,
        searchedItems: itemAdaptor.removeAll(state.searchedItems),
        isLoading: false,
        totalItemCount: 0,
        selectedProductsSearchCount: null,
        invoiceNumber: null,
        loadedItems: itemAdaptor.removeAll(state.loadedItems),
        itemsInCarts: itemAdaptor.removeAll(state.itemsInCarts),
        hasSearchedItem: null
      };

    case RetrunInvoiceCfaActionTypes.LOAD_ITEMS:
      return {
        ...state,
        isLoading: true,
        selectedProductsSearchCount: null
        //totalItemCount: 0
      };
    case RetrunInvoiceCfaActionTypes.LOAD_ITEM_SUCCESS:
      console.log('load', action.payload);
      return {
        ...state,
        isLoading: false,
        hasloaded: true,
        loadedItems: itemAdaptor.addAll(
          action.payload.items,
          state.loadedItems
        ),
        totalItemCount: action.payload.count
      };
    case RetrunInvoiceCfaActionTypes.LOAD_ITEM_FAILURE:
      return {
        ...state,
        isLoading: false,
        hasloaded: false,
        error: action.payload,
        totalItemCount: 0
      };

    case RetrunInvoiceCfaActionTypes.REMOVE_SELECTED_ITEMS:
      return {
        ...state,
        hasRemovedMultipleItems: false
      };
    case RetrunInvoiceCfaActionTypes.REMOVE_SELECTED_ITEMS_SUCCESS:
      return {
        ...state,
        hasRemovedMultipleItems: true
      };
    case RetrunInvoiceCfaActionTypes.REMOVE_SELECTED_ITEMS_FAILURE:
      return {
        ...state,
        hasRemovedMultipleItems: false,
        error: action.payload
      };
    case RetrunInvoiceCfaActionTypes.SELECTED_PRODUCTS_SEARCH:
      return {
        ...state,
        hasSelectedProductsSearch: null
      };
    case RetrunInvoiceCfaActionTypes.SELECTED_PRODUCTS_SEARCH_SUCESSS:
      return {
        ...state,
        hasSelectedProductsSearch: true,
        loadedItems: itemAdaptor.addAll(
          action.payload.items,
          state.loadedItems
        ),
        selectedProductsSearchCount: action.payload.count
      };
    case RetrunInvoiceCfaActionTypes.SELECTED_PRODUCTS_SEARCH_FAILURE:
      return {
        ...state,
        hasSelectedProductsSearch: false,
        error: action.payload
      };
    case RetrunInvoiceCfaActionTypes.LOAD_FILTER_OPTIONS:
      return {
        ...state,
        isLoadingFilterOptions: true,
        error: null
      };

    case RetrunInvoiceCfaActionTypes.LOAD_FILTER_OPTIONS_SUCCESS:
      console.log('filter', action.payload);
      return {
        ...state,
        filterOptions: action.payload,
        isLoadingFilterOptions: false
      };

    case RetrunInvoiceCfaActionTypes.LOAD_FILTER_OPTIONS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingFilterOptions: false
      };
    default:
      return {
        ...state
      };
  }
}
