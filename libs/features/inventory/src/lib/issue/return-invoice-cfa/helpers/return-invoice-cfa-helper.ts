import { FilterOption } from '../models/return-invoice-to-cfa.model';
import { FilterOptionAdaptor } from '../adaptors/filter-option.adaptor';

export class FilterOptionHelper {
  static getOptions(
    data: any,
    idField: string,
    descriptionField: string = 'description'
  ): FilterOption[] {
    const filters: FilterOption[] = [];
    for (const filter of data) {
      filters.push(
        FilterOptionAdaptor.fromJson(filter, idField, descriptionField)
      );
    }
    return filters;
  }
}
