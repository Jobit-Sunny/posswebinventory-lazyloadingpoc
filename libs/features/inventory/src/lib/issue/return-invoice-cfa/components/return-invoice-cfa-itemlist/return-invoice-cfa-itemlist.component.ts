import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
  OnChanges,
  ViewChild,
  ElementRef,
  OnDestroy
} from '@angular/core';
import { INIT } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { PageEvent, MatPaginator } from '@angular/material';
import { UpdateCartItems } from '../../+state/return-invoice-cfa.actions';
import { takeUntil, debounceTime } from 'rxjs/operators';
import { AppsettingFacade } from '@poss-web/core';
import { Item } from '../../models/return-invoice-to-cfa.model';

@Component({
  selector: 'poss-web-return-invoice-cfa-itemlist',
  templateUrl: './return-invoice-cfa-itemlist.component.html',
  styleUrls: ['./return-invoice-cfa-itemlist.component.scss']
})
export class ReturnInvoiceCfaItemlistComponent
  implements OnInit, OnChanges, OnDestroy {
  @Input() itemList: Item[] = [];
  @Input() count = 0;
  @Input() minPageSize;
  @Input() pageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };

  @Input() selectionEvents: Observable<boolean>;
  @ViewChild(MatPaginator, { static: false }) paginationRef: MatPaginator;
  @Input() haspaginator = false;
  @Output() updateItem = new EventEmitter<UpdateCartItems>();
  @Output() removeItem = new EventEmitter<Item>();
  @Output() paginator = new EventEmitter<PageEvent>();
  @Input() pageSizeOptions: number[] = [];
  @Output() selection: EventEmitter<{
    selected: boolean;
    id: number;
  }> = new EventEmitter();
  selectionAllSubscription: any;

  selectionAllSubject: Subject<any> = new Subject<any>();
  destroy$: Subject<null> = new Subject<null>();
  @Input()
  resetPagination: Observable<null>;

  constructor(private appsettingFacade: AppsettingFacade) {}

  ngOnInit() {
    this.resetPagination.subscribe(() => {
      if (this.paginationRef.hasPreviousPage()) {
        this.paginationRef.firstPage();
      } else {
        this.paginate(this.pageEvent);
      }
    });
    this.selectionEvents
      .pipe(
        takeUntil(this.destroy$),
        debounceTime(10)
      )
      .subscribe(data => {
        this.selectionAllSubject.next(data);
      });
  }
  /**
   * emits selected items
   * @param item
   */

  paginate(event: PageEvent) {
    this.paginator.emit(event);
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes &&
      changes['itemList'] &&
      changes['itemList'].currentValue &&
      changes['itemList'].previousValue &&
      (changes['itemList'].currentValue as []).length === 0 &&
      (changes['itemList'].previousValue as []).length > 0
    ) {
      const newPageEvent: PageEvent = {
        ...this.pageEvent,
        pageIndex: this.pageEvent.pageIndex - 1
      };
      if (newPageEvent.pageIndex >= 0) {
        this.paginate(newPageEvent);
      }
    }
  }
  /**
   * emits an event  to remove item from cart
   * @param item
   */
  remove(item: Item) {
    this.removeItem.emit(item);
  }

  /**
   * emits event to upadte the item
   * @param item
   */
  updateItems(item: UpdateCartItems) {
    this.updateItem.emit(item);
  }
  selectionEmit(selection: { selected: boolean; id: number }) {
    this.selection.emit(selection);
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
