import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { takeUntil, debounceTime } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';

import { ReturnInvoiceCFATabEnum } from '../../models/return-invoice-cfa.enum';
import {
  Item,
  CFAItemToUpdate
} from '../../models/return-invoice-to-cfa.model';

@Component({
  selector: 'poss-web-return-invoice-cfa-item',
  templateUrl: './return-invoice-cfa-item.component.html',
  styleUrls: ['./return-invoice-cfa-item.component.scss']
})
export class ReturnInvoiceCfaItemComponent implements OnInit, OnDestroy {
  @Input() item: Item;
  returnInvoiceCFATabRef = ReturnInvoiceCFATabEnum;
  @Input() cart = true;
  @Input() tab: ReturnInvoiceCFATabEnum;

  @Output() selectedItem = new EventEmitter<Item>();
  @Output() checkBox = new EventEmitter<boolean>();
  @Output() updateItem = new EventEmitter<CFAItemToUpdate>();
  @Output() remove = new EventEmitter<Item>();
  readonlyqty = true;
  checkBoxValue = false;
  itemForm: FormGroup;
  destroy$ = new Subject();
  singleItemWt: number;
  maxValue: number;
  @Input() selectionEvents: Observable<any>;
  @Output() selection: EventEmitter<{
    selected: boolean;
    id: number;
  }> = new EventEmitter();
  selectedAll: boolean;
  constructor() {}

  ngOnInit() {
    this.maxValue = this.item.totalQuantity;
    this.itemForm = this.createForm(this.item);
    // changed
    this.singleItemWt = this.item.totalWeight / this.item.totalQuantity;
    this.selectionEvents.pipe(takeUntil(this.destroy$)).subscribe(data => {
      if (data.selectCheckbox === true) {
        this.itemForm.patchValue({ isItemSelected: true });
      } else {
        this.itemForm.patchValue({ isItemSelected: false });
      }
      if (data.enableCheckbox === true) {
        this.itemForm.controls.isItemSelected.enable();
      } else {
        this.itemForm.controls.isItemSelected.disable();
      }
    });
    this.itemForm
      .get('measuredQuantity')
      .valueChanges.pipe(debounceTime(1000))
      .subscribe(() => {
        if (
          this.itemForm.get('measuredQuantity').value >
          this.item.availableQuantity
        ) {
          this.itemForm.patchValue({
            measuredQuantity: this.item.totalQuantity
          });
        }
        if (this.itemForm.valid) {
          this.itemForm.patchValue({
            measuredWeight:
              this.singleItemWt * this.itemForm.value.measuredQuantity
          });
          this.updateItem.emit(
            this.createUpdateItemPayload(
              this.itemForm.value.measuredQuantity,
              this.itemForm.value.measuredWeight
            )
          );
        }
      });

    if (
      this.tab === ReturnInvoiceCFATabEnum.SELECTED_PRODUCTS &&
      this.item.availableQuantity > 1 &&
      this.cart === true
    ) {
      this.readonlyqty = false;
    }
  }

  createForm(item: Item): FormGroup {
    return new FormGroup({
      measuredQuantity: new FormControl(item.totalQuantity, [
        Validators.required,
        Validators.min(1),
        Validators.max(this.item.availableQuantity)
      ]),
      isItemSelected: new FormControl(''),
      measuredWeight: new FormControl(item.totalWeight),
      binGroupCode: new FormControl()
    });
  }

  /**
   * emits update event
   */

  /**
   * emits event for remove item from cart
   */
  removeItem() {
    this.remove.emit(this.item);
  }

  /**
   * emits the selected items
   */
  emitSelectedItem() {
    this.selectedItem.emit(this.item);
    this.checkBox.emit(this.checkBoxValue);
  }
  /**
   * creates item payload with updated quantity
   * @param quantity :updated quantity
   */
  createUpdateItemPayload(quantity: number, weight: number): CFAItemToUpdate {
    return {
      itemId: this.item.id,
      newUpdate: { quantity: quantity, weight: weight },
      actualDetails: {
        quantity: this.item.totalQuantity,
        weight: this.item.totalWeight
      }
    };
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
  selectItem() {
    this.selection.emit({
      id: this.item.id,
      selected: this.itemForm.get('isItemSelected').value
    });
  }
}
