import * as moment from 'moment';
import {
  CreateStockIssueResponse,
  StockIssueItem
} from '../models/stock-issue-tep-gep.model';

export class StockIssueTEPGEPAdaptor {
  static createStockIssueResponseFromJson(data: any): CreateStockIssueResponse {
    if (!data) {
      return null;
    }
    return {
      id: data.id,
      srcLocationCode: data.srcLocationCode,
      destLocationCode: data.destLocationCode,
      status: data.status,
      weightUnit: data.weightUnit,
      currencyCode: data.currencyCode,
      srcDocNo: data.srcDocNo,
      srcFiscalYear: data.srcFiscalYear,
      srcDocDate: moment(data.srcDocDate),
      destDocNo: data.destDocNo,
      destDocDate: moment(data.destDocDate),
      orderType: data.orderType,
      totalAvailableQuantity: data.totalAvailableQuantity,
      totalMeasuredQuantity: data.totalMeasuredQuantity,
      totalAvailableValue: data.totalAvailableValue,
      totalMeasuredValue: data.totalMeasuredValue,
      totalAvailableWeight: data.totalMeasuredValue,
      totalMeasuredWeight: data.totalMeasuredWeight,
      // for l1/l2
      transferType: data.transferType,
      // for l3
      invoiceType: data.invoiceType,
      // for l1/l2
      courierReceivedDate: moment(data.courierReceivedDate),
      courierDetails: data.courierDetails,
      // for l3
      issuedRemarks: data.issuedRemarks
    };
  }

  static stockIssueItemFromJson(
    stockIssueItem: any,
    data: any
  ): StockIssueItem {
    if (!data) {
      return null;
    }
    return {
      id: stockIssueItem.id,
      itemCode: stockIssueItem.itemCode,
      lotNumber: stockIssueItem.lotNumber,
      mfgDate: moment(stockIssueItem.mfgDate),
      productCategory: stockIssueItem.productCategory,
      productGroup: stockIssueItem.productGroup,
      binCode: stockIssueItem.binCode,
      binGroupCode: stockIssueItem.binGroupCode,
      stdValue: stockIssueItem.stdValue,
      stdWeight: stockIssueItem.stdWeight,
      currencyCode: stockIssueItem.currencyCode,
      weightUnit: stockIssueItem.weightUnit,
      status: stockIssueItem.status,
      imageURL: stockIssueItem.imageURL,
      itemDetails: stockIssueItem.itemDetails,
      availableQuantity: stockIssueItem.availableQuantity,
      availableWeight: stockIssueItem.availableWeight,
      availableValue: stockIssueItem.availableValue,
      measuredQuantity: stockIssueItem.measuredQuantity,
      measuredWeight: stockIssueItem.measuredWeight,
      measuredValue: stockIssueItem.measuredValue,
      orderType: stockIssueItem.orderType,
      inventoryId: stockIssueItem.inventoryId,
      // for l3
      remarks: stockIssueItem.remarks,
      totalElements: data.totalElements
    };
  }
}
