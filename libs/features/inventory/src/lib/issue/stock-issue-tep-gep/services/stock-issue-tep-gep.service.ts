import { Injectable } from '@angular/core';
import {
  getCreateStockIssueUrl,
  getStockIssueItemsUrl,
  getCreateStockIssueItemsUrl,
  updateStockIssueUrl,
  getStockIssueItemsCountUrl
} from '../../../endpoints.constants';
import { ApiService } from '@poss-web/core';
import { map } from 'rxjs/operators';
import { StockIssueTEPGEPAdaptor } from '../adaptors/stock-issue-tep-gep.adaptor';
import { Observable } from 'rxjs';
import {
  CreateStockIssueResponse,
  StockIssueItem
} from '../models/stock-issue-tep-gep.model';
import { StockIssueTEPGEPHelper } from '../helpers/stock-issue-tep-gep.helper';

@Injectable()
export class StockIssueTepGepService {
  /**
   * Issue TEP/GEP APIS
   */
  constructor(private apiService: ApiService) {}

  isL1L2Store = (storeType: string): boolean => {
    return storeType === 'L1' || storeType === 'L2';
  };

  isL3Store = (storeType: string): boolean => {
    return storeType === 'L3';
  };

  createStockIssue(
    transferType: string,
    storeType: string
  ): Observable<CreateStockIssueResponse> {
    const createStockIssueUrl = getCreateStockIssueUrl(transferType, storeType);
    return this.apiService
      .post(createStockIssueUrl)
      .pipe(
        map((data: any) =>
          StockIssueTEPGEPAdaptor.createStockIssueResponseFromJson(data)
        )
      );
  }

  updateStockIssue(
    id: number,
    transferType: string,
    storeType: string,
    remarks: string,
    carrierDetails: any,
    locationCode?: string
  ): Observable<CreateStockIssueResponse> {
    const updateIssueUrl = updateStockIssueUrl(id, transferType, storeType);
    if (this.isL1L2Store(storeType)) {
      return this.apiService
        .patch(updateIssueUrl, {
          carrierDetails,
          destinationLocationCode: locationCode,
          remarks
        })
        .pipe(
          map((data: any) =>
            StockIssueTEPGEPAdaptor.createStockIssueResponseFromJson(data)
          )
        );
    } else if (this.isL3Store(storeType)) {
      return this.apiService
        .patch(updateIssueUrl, {
          carrierDetails,
          cfaLocationCode: locationCode,
          remarks
        })
        .pipe(
          map((data: any) =>
            StockIssueTEPGEPAdaptor.createStockIssueResponseFromJson(data)
          )
        );
    }
  }

  createStockIssueItems(
    id: number,
    transferType: string,
    storeType: string,
    issueItems: any[]
  ): Observable<any> {
    const createStockIssueItemsUrl = getCreateStockIssueItemsUrl(
      id,
      transferType,
      storeType
    );
    if (this.isL1L2Store(storeType)) {
      return this.apiService
        .post(createStockIssueItemsUrl, {
          stockItems: issueItems
        })
        .pipe(map((data: any) => data));
    } else if (this.isL3Store(storeType)) {
      return this.apiService
        .post(createStockIssueItemsUrl, {
          invoiceItems: issueItems
        })
        .pipe(map((data: any) => data));
    }
  }

  loadStockIssueItems(
    id: number,
    itemCode: string,
    lotNumber: string,
    transferType: string,
    storeType: string,
    status: string,
    pageIndex: number,
    pageSize: number,
    sort: Map<string, string>,
    filter: Map<string, string>
  ): Observable<StockIssueItem[]> {
    const getIssueItemsUrl = getStockIssueItemsUrl(
      id,
      itemCode,
      lotNumber,
      transferType,
      storeType,
      status,
      pageIndex,
      pageSize,
      sort,
      filter
    );
    return this.apiService
      .get(getIssueItemsUrl)
      .pipe(
        map((data: any) => StockIssueTEPGEPHelper.getStockIssueItems(data))
      );
  }

  updateAllStockIssueItems(
    id: number,
    transferType: string,
    storeType: string,
    itemIds: any[]
  ): Observable<any> {
    const updateAllIssueItemsUrl = getCreateStockIssueItemsUrl(
      id,
      transferType,
      storeType
    );
    return this.apiService
      .put(updateAllIssueItemsUrl, { itemIds })
      .pipe(map((data: any) => data));
  }

  loadTotalCount(
    id: number,
    itemCode: string,
    lotNumber: string,
    transferType: string,
    storeType: string,
    status: string,
    sort: Map<string, string>,
    filter: Map<string, string>
  ): Observable<number> {
    const loadTotalCountUrl = getStockIssueItemsCountUrl(
      id,
      itemCode,
      lotNumber,
      transferType,
      storeType,
      status,
      sort,
      filter
    );
    return this.apiService
      .get(loadTotalCountUrl)
      .pipe(map((data: any) => data.totalElements));
  }
}
