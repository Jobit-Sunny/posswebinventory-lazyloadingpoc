import { Component, OnInit, OnDestroy, EventEmitter, Input, Output } from '@angular/core';
import { PageEvent } from '@angular/material';
import { Subject, Observable } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { AppsettingFacade } from '@poss-web/core';
import { StockIssueItem, ItemSelection, ItemSelectionAll } from '../../models/stock-issue-tep-gep.model';

@Component({
  selector: 'poss-web-stock-issue-tep-gep-item-list',
  templateUrl: './stock-issue-tep-gep-item-list.component.html',
  styleUrls: ['./stock-issue-tep-gep-item-list.component.scss']
})
export class StockIssueTepItemListComponent implements OnInit, OnDestroy {
  @Input() items: Observable<StockIssueItem[]>;
  @Input() totalItemsCount: number;
  @Input() pageEvent: PageEvent;
  @Input() selectionEvents: Observable<ItemSelectionAll>;

  @Output() selection: EventEmitter<ItemSelection> = new EventEmitter();
  @Output() paginator = new EventEmitter<PageEvent>();

  pageSize = 0;
  pageSizeOptions: [];
  destroy$: Subject<null> = new Subject<null>();
  selectionAllSubject: Subject<ItemSelectionAll> = new Subject<ItemSelectionAll>();

  constructor(private appsetttingFacade: AppsettingFacade) {}

  paginate(event: PageEvent) {
    this.paginator.emit(event);
  }

  onSelectionEmit(selection: ItemSelection) {
    this.selection.emit(selection);
  }

  ngOnInit() {
    this.appsetttingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(resp => (this.pageSize = JSON.parse(resp)));

    this.appsetttingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(resp => (this.pageSizeOptions = resp));

    this.selectionEvents
      .pipe(
        debounceTime(10),
        takeUntil(this.destroy$)
      )
      .subscribe(data => {
        this.selectionAllSubject.next(data);
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
