import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
import {
  StockIssueItem,
  ItemSelection,
  ItemSelectionAll
} from '../../models/stock-issue-tep-gep.model';
import { AppsettingFacade } from '@poss-web/core';

@Component({
  selector: 'poss-web-stock-issue-tep-gep-item',
  templateUrl: './stock-issue-tep-gep-item.component.html',
  styleUrls: ['./stock-issue-tep-gep-item.component.scss']
})
export class StockIssueTepItemComponent implements OnInit, OnDestroy {
  @Input() item: StockIssueItem;
  @Input() selectionEvents: Observable<ItemSelectionAll>;

  @Output() selection: EventEmitter<ItemSelection> = new EventEmitter();

  destroy$: Subject<null> = new Subject<null>();
  dateFormat$: Observable<string>;

  itemForm: FormGroup;
  weightToDisplay: string;
  disabled = false;
  totalQuantity = 0;
  totalWeight = 0;

  constructor(private appsettingFacade: AppsettingFacade) {}

  ngOnInit() {
    this.itemForm = this.createForm(this.item);
    this.dateFormat$ = this.appsettingFacade.getDateFormat();
    if (this.item.availableQuantity === 0) {
      this.itemForm.disable();
      this.disabled = true;
    }

    this.selectionEvents
      .pipe(
        debounceTime(10),
        takeUntil(this.destroy$)
      )
      .subscribe(data => {
        if (data.selectCheckbox === true) {
          this.itemForm.patchValue({ isSelected: true });
        } else {
          this.itemForm.patchValue({ isSelected: false });
        }
        if (data.enableCheckbox === true) {
          this.itemForm.controls.isSelected.enable();
        } else {
          this.itemForm.controls.isSelected.disable();
        }
      });
  }

  createForm(item: StockIssueItem): FormGroup {
    this.totalQuantity =
      item.measuredQuantity === null
        ? item.availableQuantity
        : item.measuredQuantity;
    this.totalWeight =
      item.measuredWeight === null ? item.availableWeight : item.measuredWeight;
    this.weightToDisplay = this.totalWeight.toFixed(3);

    return new FormGroup({
      isSelected: new FormControl(false),
      totalWeight: new FormControl(this.weightToDisplay),
      totalQuantity: new FormControl(this.totalQuantity)
    });
  }

  onSelectionEmit() {
    this.selection.emit({
      isSelected: this.itemForm.value.isSelected,
      item: this.item
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
