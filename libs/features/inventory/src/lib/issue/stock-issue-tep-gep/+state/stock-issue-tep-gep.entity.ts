import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { StockIssueItem } from '../models/stock-issue-tep-gep.model';

export interface ItemsEntity extends EntityState<StockIssueItem> {}
export const itemsAdapter = createEntityAdapter<StockIssueItem>({
  selectId: items => items.inventoryId
});
export const itemsSelector = itemsAdapter.getSelectors();

export interface StockIssueItemsEntity extends EntityState<StockIssueItem> {}
export const stockIssueItemsAdapter = createEntityAdapter<StockIssueItem>({
  selectId: items => items.id
});
export const stockIssueItemsSelector = stockIssueItemsAdapter.getSelectors();
