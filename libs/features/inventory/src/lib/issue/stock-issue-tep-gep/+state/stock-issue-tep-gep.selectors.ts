import { createSelector } from '@ngrx/store';
import { selectInventory } from '../../../inventory.state';
import {
  itemsSelector,
  stockIssueItemsSelector
} from './stock-issue-tep-gep.entity';

const selectCreateStockIssueResponse = createSelector(
  selectInventory,
  state => state.issueTEP.createStockIssueResponse
);

const selectupdateStockIssueResponse = createSelector(
  selectInventory,
  state => state.issueTEP.updateStockIssueResponse
);

const items = createSelector(
  selectInventory,
  state => state.issueTEP.items
);

const selectItems = createSelector(
  items,
  itemsSelector.selectAll
);

const selectupdateAllStockIssueItemsResponse = createSelector(
  selectInventory,
  state => state.issueTEP.updateAllStockIssueItemsResponse
);

const selectCreateStockIssueItemsResponse = createSelector(
  selectInventory,
  state => state.issueTEP.createStockIssueItemsResponse
);

const stockIssueItems = createSelector(
  selectInventory,
  state => state.issueTEP.stockIssueItems
);

const selectStockIssueItems = createSelector(
  stockIssueItems,
  stockIssueItemsSelector.selectAll
);

const selectHasError = createSelector(
  selectInventory,
  state => state.issueTEP.hasError
);

const selectIsLoading = createSelector(
  selectInventory,
  state => state.issueTEP.isLoading
);

const selectTotalItemsCount = createSelector(
  selectInventory,
  state => state.issueTEP.totalItemsCount
);

const selectTotalStockIssueItemsCount = createSelector(
  selectInventory,
  state => state.issueTEP.totalStockIssueItemsCount
);

const selectFactoryAddress = createSelector(
  selectInventory,
  state => state.issueTEP.factoryAddress
);

const selectProductCategories = createSelector(
  selectInventory,
  state => state.issueTEP.productCategories
);

const selectProductGroups = createSelector(
  selectInventory,
  state => state.issueTEP.productGroups
);

const selectCourierDetails = createSelector(
  selectInventory,
  state => state.issueTEP.courierDetails
);

const selectEmployeeCodes = createSelector(
  selectInventory,
  state => state.issueTEP.employeeCodes
);

const selectEmployeeDetails = createSelector(
  selectInventory,
  state => state.issueTEP.employeeDetails
);

const selectSortDataItems = createSelector(
  selectInventory,
  state => state.issueTEP.sortDataItems
);

const selectSortDataStockIssueItems = createSelector(
  selectInventory,
  state => state.issueTEP.sortDataStockIssueItems
);

const selectfilterDataItems = createSelector(
  selectInventory,
  state => state.issueTEP.filterDataItems
);

const selectfilterDataStockIssueItems = createSelector(
  selectInventory,
  state => state.issueTEP.filterDataStockIssueItems
);
export const IssueTEPSelectors = {
  selectCreateStockIssueResponse,
  selectupdateStockIssueResponse,
  selectItems,
  selectupdateAllStockIssueItemsResponse,
  selectCreateStockIssueItemsResponse,
  selectStockIssueItems,
  selectHasError,
  selectIsLoading,
  selectTotalItemsCount,
  selectTotalStockIssueItemsCount,
  selectFactoryAddress,
  selectProductCategories,
  selectProductGroups,
  selectCourierDetails,
  selectEmployeeCodes,
  selectEmployeeDetails,
  selectSortDataItems,
  selectSortDataStockIssueItems,
  selectfilterDataItems,
  selectfilterDataStockIssueItems
};
