import { IssueTEPState } from './stock-issue-tep-gep.state';
import {
  IssueTEPActionTypes,
  IssueTEPActions
} from './stock-issue-tep-gep.action';
import {
  itemsAdapter,
  stockIssueItemsAdapter
} from './stock-issue-tep-gep.entity';
import { CreateStockIssueResponse } from '../models/stock-issue-tep-gep.model';

export const initialIssueTEPState: IssueTEPState = {
  createStockIssueResponse: {} as CreateStockIssueResponse,
  updateStockIssueResponse: {} as CreateStockIssueResponse,
  createStockIssueItemsResponse: false,
  updateAllStockIssueItemsResponse: false,
  items: itemsAdapter.getInitialState(),
  stockIssueItems: stockIssueItemsAdapter.getInitialState(),
  searchItems: itemsAdapter.getInitialState(),
  searchStockIssueItems: stockIssueItemsAdapter.getInitialState(),
  totalItemsCount: 0,
  totalStockIssueItemsCount: 0,
  factoryAddress: null,
  productCategories: [],
  productGroups: [],
  courierDetails: [],
  employeeCodes: [],
  employeeDetails: null,
  sortDataItems: [],
  sortDataStockIssueItems: [],
  filterDataItems: {},
  filterDataStockIssueItems: {},
  hasError: null,
  isLoading: false
};

export function IssueTEPReducer(
  state: IssueTEPState = initialIssueTEPState,
  action: IssueTEPActions
): IssueTEPState {
  switch (action.type) {
    case IssueTEPActionTypes.CREATE_STOCK_ISSUE:
    case IssueTEPActionTypes.UPDATE_STOCK_ISSUE:
    case IssueTEPActionTypes.LOAD_ITEMS:
    case IssueTEPActionTypes.UPDATE_ALL_STOCK_ISSUE_ITEMS:
    case IssueTEPActionTypes.CREATE_STOCK_ISSUE_ITEMS:
    case IssueTEPActionTypes.LOAD_STOCK_ISSUE_ITEMS:
    case IssueTEPActionTypes.LOAD_TOTAL_ITEMS_COUNT:
    case IssueTEPActionTypes.LOAD_TOTAL_STOCK_ISSUE_ITEMS_COUNT:
    case IssueTEPActionTypes.LOAD_FACTORY_ADDRESS:
    case IssueTEPActionTypes.LOAD_PRODUCT_CATEGORIES:
    case IssueTEPActionTypes.LOAD_PROUDCT_GROUPS:
    case IssueTEPActionTypes.LOAD_COURIER_DETAILS:
      return { ...state, isLoading: true, hasError: null };

    case IssueTEPActionTypes.CREATE_STOCK_ISSUE_FAILURE:
    case IssueTEPActionTypes.UPDATE_STOCK_ISSUE_FAILURE:
    case IssueTEPActionTypes.LOAD_ITEMS_FAILURE:
    case IssueTEPActionTypes.UPDATE_ALL_STOCK_ISSUE_ITEMS_FAILURE:
    case IssueTEPActionTypes.CREATE_STOCK_ISSUE_ITEMS_FAILURE:
    case IssueTEPActionTypes.LOAD_STOCK_ISSUE_ITEMS_FAILURE:
    case IssueTEPActionTypes.LOAD_TOTAL_ITEMS_COUNT_FAILURE:
    case IssueTEPActionTypes.LOAD_TOTAL_STOCK_ISSUE_ITEMS_COUNT_FAILURE:
    case IssueTEPActionTypes.LOAD_FACTORY_ADDRESS_FAILURE:
    case IssueTEPActionTypes.LOAD_PRODUCT_CATEGORIES_FAILURE:
    case IssueTEPActionTypes.LOAD_PROUDCT_GROUPS_FAILURE:
    case IssueTEPActionTypes.LOAD_COURIER_DETAILS_FAILURE:
      return {
        ...state,
        hasError: action.payload,
        isLoading: false
      };

    case IssueTEPActionTypes.CREATE_STOCK_ISSUE_SUCCESS:
      return {
        ...state,
        createStockIssueResponse: action.payload,
        isLoading: false,
        hasError: null
      };

    case IssueTEPActionTypes.UPDATE_STOCK_ISSUE_SUCCESS:
      return {
        ...state,
        updateStockIssueResponse: action.payload,
        isLoading: false,
        hasError: null
      };

    case IssueTEPActionTypes.LOAD_ITEMS_SUCCESS:
      return {
        ...state,
        items: itemsAdapter.addAll(action.payload, state.items),
        isLoading: false,
        hasError: null
      };

    case IssueTEPActionTypes.UPDATE_ALL_STOCK_ISSUE_ITEMS_SUCCESS:
      return {
        ...state,
        updateAllStockIssueItemsResponse: action.payload,
        isLoading: false,
        hasError: null
      };

    case IssueTEPActionTypes.CREATE_STOCK_ISSUE_ITEMS_SUCCESS:
      return {
        ...state,
        createStockIssueItemsResponse: action.payload,
        isLoading: false,
        hasError: null
      };

    case IssueTEPActionTypes.LOAD_STOCK_ISSUE_ITEMS_SUCCESS:
      return {
        ...state,
        stockIssueItems: stockIssueItemsAdapter.addAll(
          action.payload,
          state.stockIssueItems
        ),
        isLoading: false,
        hasError: null
      };

    case IssueTEPActionTypes.SEARCH_CLEAR:
      return {
        ...state,
        searchItems: itemsAdapter.removeAll(state.searchItems),
        searchStockIssueItems: itemsAdapter.removeAll(
          state.searchStockIssueItems
        ),
        hasError: null,
        isLoading: false
      };

    case IssueTEPActionTypes.RESET_LIST:
      return {
        ...state,
        items: itemsAdapter.removeAll(state.items),
        stockIssueItems: itemsAdapter.removeAll(state.stockIssueItems),
        hasError: null,
        isLoading: false
      };

    case IssueTEPActionTypes.RESET_RESPONSE:
      return {
        ...state,
        updateAllStockIssueItemsResponse: false,
        createStockIssueItemsResponse: false,
        totalItemsCount: 0,
        totalStockIssueItemsCount: 0,
        hasError: null,
        isLoading: false
      };

    case IssueTEPActionTypes.RESET_ALL:
      return {
        ...state,
        createStockIssueResponse: {} as CreateStockIssueResponse,
        updateStockIssueResponse: {} as CreateStockIssueResponse,
        createStockIssueItemsResponse: false,
        updateAllStockIssueItemsResponse: false,
        items: itemsAdapter.removeAll(state.items),
        stockIssueItems: itemsAdapter.removeAll(state.stockIssueItems),
        searchItems: itemsAdapter.removeAll(state.searchItems),
        searchStockIssueItems: stockIssueItemsAdapter.removeAll(
          state.searchStockIssueItems
        ),
        totalItemsCount: 0,
        totalStockIssueItemsCount: 0,
        factoryAddress: null,
        productCategories: null,
        productGroups: null,
        courierDetails: null,
        employeeCodes: null,
        employeeDetails: null,
        sortDataItems: [],
        sortDataStockIssueItems: [],
        filterDataItems: {},
        filterDataStockIssueItems: {},
        hasError: null,
        isLoading: false
      };

    case IssueTEPActionTypes.LOAD_TOTAL_ITEMS_COUNT_SUCCESS:
      return {
        ...state,
        totalItemsCount: action.payload,
        isLoading: false,
        hasError: null
      };

    case IssueTEPActionTypes.LOAD_TOTAL_STOCK_ISSUE_ITEMS_COUNT_SUCCESS:
      return {
        ...state,
        totalStockIssueItemsCount: action.payload,
        isLoading: false,
        hasError: null
      };

    case IssueTEPActionTypes.LOAD_FACTORY_ADDRESS_SUCCESS:
      return {
        ...state,
        factoryAddress: action.payload,
        isLoading: false,
        hasError: null
      };

    case IssueTEPActionTypes.LOAD_PRODUCT_CATEGORIES_SUCCESS:
      return {
        ...state,
        productCategories: action.payload,
        isLoading: false,
        hasError: null
      };

    case IssueTEPActionTypes.LOAD_PROUDCT_GROUPS_SUCCESS:
      return {
        ...state,
        productGroups: action.payload,
        isLoading: false,
        hasError: null
      };

    case IssueTEPActionTypes.LOAD_COURIER_DETAILS_SUCCESS:
      return {
        ...state,
        courierDetails: action.payload,
        isLoading: false,
        hasError: null
      };

    case IssueTEPActionTypes.LOAD_EMPLOYEE_CODES:
      return {
        ...state,
        isLoading: true,
        hasError: null
      };
    case IssueTEPActionTypes.LOAD_EMPLOYEE_CODES_SUCCESS:
      return {
        ...state,
        employeeCodes: action.payload,
        isLoading: false,
        hasError: null
      };
    case IssueTEPActionTypes.LOAD_EMPLOYEE_CODES_FAILURE:
      return {
        ...state,
        isLoading: false,
        hasError: action.payload
      };

    case IssueTEPActionTypes.LOAD_EMPLOYEE_DETAILS:
      return {
        ...state,
        isLoading: true,
        hasError: null
      };
    case IssueTEPActionTypes.LOAD_EMPLOYEE_DETAILS_SUCCESS:
      return {
        ...state,
        employeeDetails: action.payload,
        isLoading: false,
        hasError: null
      };
    case IssueTEPActionTypes.LOAD_EMPLOYEE_DETAILS_FAILURE:
      return {
        ...state,
        isLoading: false,
        hasError: action.payload
      };

    case IssueTEPActionTypes.SET_SORT_DATA_ITEMS:
      return {
        ...state,
        sortDataItems: action.payload
      };
    case IssueTEPActionTypes.SET_SORT_DATA_STOCK_ISSUE_ITEMS:
      return {
        ...state,
        sortDataStockIssueItems: action.payload
      };
    case IssueTEPActionTypes.SET_FILTER_DATA_ITEMS:
      return {
        ...state,
        filterDataItems: action.payload
      };
    case IssueTEPActionTypes.SET_FILTER_DATA_STOCK_ISSUE_ITEMS:
      return {
        ...state,
        filterDataStockIssueItems: action.payload
      };
    default:
      return state;
  }
}
