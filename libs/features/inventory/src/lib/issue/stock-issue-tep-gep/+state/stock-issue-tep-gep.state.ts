import {
  ItemsEntity,
  StockIssueItemsEntity
} from './stock-issue-tep-gep.entity';
import {
  CustomErrors,
  ProductCategory,
  ProductGroup,
  StoreUserDetails,
  LocationSummaryDetails
} from '@poss-web/core';
import { CreateStockIssueResponse } from '../models/stock-issue-tep-gep.model';
import { Filter, Column } from '@poss-web/shared';

export class IssueTEPState {
  createStockIssueResponse: CreateStockIssueResponse;
  updateStockIssueResponse: CreateStockIssueResponse;
  items: ItemsEntity;
  updateAllStockIssueItemsResponse: boolean;
  createStockIssueItemsResponse: boolean;
  stockIssueItems: StockIssueItemsEntity;
  searchItems: ItemsEntity;
  searchStockIssueItems: StockIssueItemsEntity;
  totalItemsCount: number;
  totalStockIssueItemsCount: number;
  factoryAddress: LocationSummaryDetails;
  productCategories: ProductCategory[];
  productGroups: ProductGroup[];
  courierDetails: string[];
  employeeCodes: string[];
  employeeDetails: StoreUserDetails;
  sortDataItems: Column[];
  sortDataStockIssueItems: Column[];
  filterDataItems: { [key: string]: Filter[] };
  filterDataStockIssueItems: { [key: string]: Filter[] };
  hasError?: CustomErrors;
  isLoading?: boolean;
}
