import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../../inventory.state';
import * as IssueTEPActions from './stock-issue-tep-gep.action';
import { IssueTEPSelectors } from './stock-issue-tep-gep.selectors';
import { Column, Filter } from '@poss-web/shared';

@Injectable()
export class IssueTEPFacade {
  private createStockIssueResponse$ = this.store.select(
    IssueTEPSelectors.selectCreateStockIssueResponse
  );

  private updateStockIssueResponse$ = this.store.select(
    IssueTEPSelectors.selectupdateStockIssueResponse
  );

  private getItems$ = this.store.select(IssueTEPSelectors.selectItems);

  private updateAllStockIssueItemsResponse$ = this.store.select(
    IssueTEPSelectors.selectupdateAllStockIssueItemsResponse
  );

  private createStockIssueItemsResponse$ = this.store.select(
    IssueTEPSelectors.selectCreateStockIssueItemsResponse
  );

  private getStockIssueItems$ = this.store.select(
    IssueTEPSelectors.selectStockIssueItems
  );

  private hasError$ = this.store.select(IssueTEPSelectors.selectHasError);

  private isLoading$ = this.store.select(IssueTEPSelectors.selectIsLoading);

  private totalItemsCount$ = this.store.select(
    IssueTEPSelectors.selectTotalItemsCount
  );

  private totalStockIssueItemsCount$ = this.store.select(
    IssueTEPSelectors.selectTotalStockIssueItemsCount
  );

  private factoryAddress$ = this.store.select(
    IssueTEPSelectors.selectFactoryAddress
  );

  private productCategories$ = this.store.select(
    IssueTEPSelectors.selectProductCategories
  );

  private productGroups$ = this.store.select(
    IssueTEPSelectors.selectProductGroups
  );

  private courierDetails$ = this.store.select(
    IssueTEPSelectors.selectCourierDetails
  );

  private employeeCodes$ = this.store.select(
    IssueTEPSelectors.selectEmployeeCodes
  );

  private employeeDetails$ = this.store.select(
    IssueTEPSelectors.selectEmployeeDetails
  );

  private sortDataItems$ = this.store.select(
    IssueTEPSelectors.selectSortDataItems
  );

  private sortDataStockIssueItems$ = this.store.select(
    IssueTEPSelectors.selectSortDataStockIssueItems
  );

  private filterDataItems$ = this.store.select(
    IssueTEPSelectors.selectfilterDataItems
  );

  private filterDataStockIssueItems$ = this.store.select(
    IssueTEPSelectors.selectfilterDataStockIssueItems
  );
  constructor(private store: Store<State>) {}

  createStockIssue(
    createStockIssuePayload: IssueTEPActions.CreateStockIssuePayload
  ) {
    this.store.dispatch(
      new IssueTEPActions.CreateStockIssue(createStockIssuePayload)
    );
  }

  getCreateStockIssueResponse() {
    return this.createStockIssueResponse$;
  }

  updateStockIssue(
    updateStockIssuePayload: IssueTEPActions.UpdateStockIssuePayload
  ) {
    this.store.dispatch(
      new IssueTEPActions.UpdateStockIssue(updateStockIssuePayload)
    );
  }

  getUpdateStockIssueResponse() {
    return this.updateStockIssueResponse$;
  }

  loadItems(
    loadStockIssueItemsPayload: IssueTEPActions.LoadStockIssueItemsPayload
  ) {
    this.store.dispatch(
      new IssueTEPActions.LoadItems(loadStockIssueItemsPayload)
    );
  }

  getItems() {
    return this.getItems$;
  }

  updateAllStockIssueItems(
    updateAllStockIssueItemsPayload: IssueTEPActions.CreateStockIssueItemsPayload
  ) {
    this.store.dispatch(
      new IssueTEPActions.UpdateAllStockIssueItems(
        updateAllStockIssueItemsPayload
      )
    );
  }

  getUpdateAllStockIssueItemsResponse() {
    return this.updateAllStockIssueItemsResponse$;
  }

  createStockIssueItems(
    createStockIssueItemsPayload: IssueTEPActions.CreateStockIssueItemsPayload
  ) {
    this.store.dispatch(
      new IssueTEPActions.CreateStockIssueItems(createStockIssueItemsPayload)
    );
  }

  getCreateStockIssueItemsResponse() {
    return this.createStockIssueItemsResponse$;
  }

  loadStockIssueItems(
    loadStockIssueItemsPayload: IssueTEPActions.LoadStockIssueItemsPayload
  ) {
    this.store.dispatch(
      new IssueTEPActions.LoadStockIssueItems(loadStockIssueItemsPayload)
    );
  }

  getStockIssueItems() {
    return this.getStockIssueItems$;
  }

  searchClear() {
    this.store.dispatch(new IssueTEPActions.SearchClear());
  }

  resetList() {
    this.store.dispatch(new IssueTEPActions.ResetList());
  }

  resetResponse() {
    this.store.dispatch(new IssueTEPActions.ResetResponse());
  }

  resetAll() {
    this.store.dispatch(new IssueTEPActions.ResetAll());
  }

  getError() {
    return this.hasError$;
  }

  getIsLoading() {
    return this.isLoading$;
  }

  loadTotalItemsCount(
    loadTotalCountPayload: IssueTEPActions.LoadStockIssueItemsPayload
  ) {
    this.store.dispatch(
      new IssueTEPActions.LoadTotalItemsCount(loadTotalCountPayload)
    );
  }

  getTotalItemsCount() {
    return this.totalItemsCount$;
  }

  loadTotalStockIssueItemsCount(
    loadTotalCountPayload: IssueTEPActions.LoadStockIssueItemsPayload
  ) {
    this.store.dispatch(
      new IssueTEPActions.LoadTotalStockIssueItemsCount(loadTotalCountPayload)
    );
  }

  getTotalStockIssueItemsCount() {
    return this.totalStockIssueItemsCount$;
  }

  loadFactoryAddress() {
    this.store.dispatch(new IssueTEPActions.LoadFactoryAddress());
  }

  getFactoryAddress() {
    return this.factoryAddress$;
  }

  loadProductCategories() {
    this.store.dispatch(new IssueTEPActions.LoadProductCategories());
  }

  getProductCategories() {
    return this.productCategories$;
  }

  loadProductGroups() {
    this.store.dispatch(new IssueTEPActions.LoadProductGroups());
  }

  getProductGroups() {
    return this.productGroups$;
  }

  loadCourierDetails(locationCode: string) {
    this.store.dispatch(new IssueTEPActions.LoadCourierDetails(locationCode));
  }

  getCourierDetails() {
    return this.courierDetails$;
  }

  loadEmployeeCodes() {
    this.store.dispatch(new IssueTEPActions.LoadEmployeeCodes());
  }

  getEmployeeCodes() {
    return this.employeeCodes$;
  }

  loadEmployeeDetails(employeeCode: string) {
    this.store.dispatch(new IssueTEPActions.LoadEmployeeDetails(employeeCode));
  }

  getEmployeeDetails() {
    return this.employeeDetails$;
  }

  setSortDataItems(sortData: Column[]) {
    this.store.dispatch(new IssueTEPActions.SetSortDataItems(sortData));
  }
  setSortDataStockIssueItems(sortData: Column[]) {
    this.store.dispatch(
      new IssueTEPActions.SetSortDataStockIssueItems(sortData)
    );
  }

  getSortDataItems() {
    return this.sortDataItems$;
  }
  getSortDataStockIssueItems() {
    return this.sortDataStockIssueItems$;
  }

  setFilterDataItems(filterData: { [key: string]: Filter[] }) {
    this.store.dispatch(new IssueTEPActions.SetFilterDataItems(filterData));
  }
  setFilterDataStockIssueItems(filterData: { [key: string]: Filter[] }) {
    this.store.dispatch(
      new IssueTEPActions.SetFilterDataStockIssueItems(filterData)
    );
  }

  getfilterDataItems() {
    return this.filterDataItems$;
  }
  getfilterDataStockIssueItems() {
    return this.filterDataStockIssueItems$;
  }
}
