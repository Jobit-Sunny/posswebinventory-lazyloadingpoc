import { StockIssueItem } from '../models/stock-issue-tep-gep.model';
import { StockIssueTEPGEPAdaptor } from '../adaptors/stock-issue-tep-gep.adaptor';

export class StockIssueTEPGEPHelper {
  static getStockIssueItems(data: any): StockIssueItem[] {
    const stockIssueItems: StockIssueItem[] = [];
    if (!data && data.results && data.results.length === 0) {
      return null;
    }
    for (const stockIssueItem of data.results) {
      stockIssueItems.push(
        StockIssueTEPGEPAdaptor.stockIssueItemFromJson(stockIssueItem, data)
      );
    }
    return stockIssueItems;
  }
}
