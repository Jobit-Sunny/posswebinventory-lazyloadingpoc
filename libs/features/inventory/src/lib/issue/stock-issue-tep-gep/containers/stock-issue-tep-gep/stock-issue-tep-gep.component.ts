import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  TemplateRef
} from '@angular/core';
import { IssueTEPFacade } from '../../+state/stock-issue-tep-gep.facade';
import { Observable, Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { PageEvent } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationEventType,
  SortDialogService,
  FilterService,
  Column,
  Filter,
  SearchComponent,
  SearchResponse,
  FilterActions
} from '@poss-web/shared';
import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import {
  CustomErrors,
  AppsettingFacade,
  ShortcutService,
  Command,
  AuthFacade,
  ProductCategory,
  ProductGroup,
  StoreUserDetails,
  LocationSummaryDetails
} from '@poss-web/core';
import {
  StockIssueTEPGEPTabTypesEnum,
  StockIssueTEPGEPStatusesEnum,
  StockIssueTEPGEPBinCodesEnum,
  StockIssueTEPGEPTransferTypesEnum,
  StockIssueTEPGEPTypesEnum,
  StockIssueTEPGEPRadioButtonTypesEnum,
  StockIssueTEPGEPCarrierTypesEnum
} from '../../models/stock-issue-tep-gep.enum';
import {
  CreateStockIssueResponse,
  StockIssueItem,
  ItemSelection,
  TransferType,
  ItemSelectionAll
} from '../../models/stock-issue-tep-gep.model';
import { getStockIssueRouteUrl } from '../../../../page-route.constants';

const SEARCH_SHORTCUT_KEY_F2 = 'StockIssueTEPComponent.F2';
@Component({
  selector: 'poss-web-stock-issue-tep-gep',
  templateUrl: './stock-issue-tep-gep.component.html',
  styleUrls: ['./stock-issue-tep-gep.component.scss']
})
export class StockIssueTEPComponent implements OnInit, OnDestroy {
  createStockIssueResponse$: Observable<CreateStockIssueResponse>;
  updateStockIssueResponse$: Observable<CreateStockIssueResponse>;
  getItems$: Observable<StockIssueItem[]>;
  getStockIssueItems$: Observable<StockIssueItem[]>;
  createStockIssueItemsResponse$: Observable<boolean>;
  updateAllStockIssueItemsResponse$: Observable<boolean>;
  factoryAddressResponse$: Observable<LocationSummaryDetails>;
  courierDetailsResponse$: Observable<string[]>;
  employeeDetailsResponse$: Observable<StoreUserDetails>;
  employeeCodesResponse$: Observable<string[]>;
  isLoading$: Observable<boolean>;
  sortDataItems: Observable<Column[]>;
  sortDataStockIssueItems: Observable<Column[]>;
  filterDataItems: Observable<{ [key: string]: Filter[] }>;
  filterDataStockIssueItems: Observable<{ [key: string]: Filter[] }>;

  stockIssueTEPGEPTabTypesEnumRef = StockIssueTEPGEPTabTypesEnum;
  stockIssueTEPGEPTypesEnumRef = StockIssueTEPGEPTypesEnum;
  stockIssueTEPGEPCarrierTypesEnumRef = StockIssueTEPGEPCarrierTypesEnum;

  itemForm: FormGroup;
  form: FormGroup;
  formList: FormArray;
  selectedFormName: string;

  selectedTransferType: TransferType;
  selectedProductsTabType: StockIssueTEPGEPTabTypesEnum;
  itemIdsLength: number;
  issueId: number;
  transferType: string;
  issueType: string;
  locationCode: string;
  storeType: string;
  factoryAddress: LocationSummaryDetails;

  itemIds = [];
  selectedItemIds = [];
  hasNotification = false;
  totalItemsCount = 0;
  totalStockIssueItemsCount = 0;
  totalItemsCountDisplay = 0;
  totalStockIssueItemsCountDisplay = 0;
  pageItemsCount = 0;
  pageStockIssueItemsCount = 0;
  productToBeSavedCount = 0;
  srcDocNo: number;
  sortBy: string;
  sortOrder: string;
  sortData: Column[] = [];
  sortDataItemsTab: Column[] = [];
  sortDataStockIssueItemsTab: Column[] = [];
  filterData: { [key: string]: Filter[] } = {};
  filterDataItemsTab: { [key: string]: Filter[] } = {};
  filterDataStockIssueItemsTab: { [key: string]: Filter[] } = {};

  sortDataItemsByMap = new Map();
  filterDataItemsByMap = new Map();
  sortDataStockIssueItemsByMap = new Map();
  filterDataStockIssueItemsByMap = new Map();
  maxFilterLimit: number;
  maxSortLimit: number;
  PRODUCT_CATEGORIES: { [key: string]: Filter[] } = {};
  PRODUCT_GROUP: { [key: string]: Filter[] } = {};
  itemCode = null;
  lotNumber = null;
  employeeCodes: string[];
  isReadOnly: boolean;
  SORT_DATA: Column[];

  intialPageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 0,
    length: 0
  };
  itemsPageEvent: PageEvent = this.intialPageEvent;
  stockIssueItemsPageEvent: PageEvent = this.intialPageEvent;

  @ViewChild(SearchComponent, { static: true })
  searchRef: SearchComponent;
  @ViewChild('confirmSuccessNotificationTemplate', { static: true })
  confirmSuccessNotificationTemplate: TemplateRef<any>;

  destroy$: Subject<null> = new Subject<null>();
  selectionAllSubject: Subject<ItemSelectionAll> = new Subject<
    ItemSelectionAll
  >();

  constructor(
    private issueTEPFacade: IssueTEPFacade,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService,
    private appsettingFacade: AppsettingFacade,
    private authFacade: AuthFacade,
    private router: Router,
    private shortcutService: ShortcutService,
    private sortDialogService: SortDialogService,
    private filterService: FilterService
  ) {
    this.form = this.formBuilder.group({
      formCollection: this.formBuilder.array([this.createcourierForm()])
    });
    this.formList = this.form.get('formCollection') as FormArray;
    this.itemForm = this.formBuilder.group({
      selectRadioButton: null
    });
    this.issueTEPFacade.loadProductCategories();
    this.issueTEPFacade.loadProductGroups();
  }

  ngOnInit(): void {
    this.issueTEPFacade.resetAll();
    this.issueType = this.activatedRoute.snapshot.params['type'];
    this.transferType = this.activatedRoute.snapshot.params['transferType'];
    this.selectedTransferType = this.selectTransferType(this.transferType);

    this.translate
      .get([
        'pw.stockIssueTepGep.availableWeightSortLabel',
        'pw.stockIssueTepGep.availableQuantitySortLabel'
      ])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: any) => {
        this.SORT_DATA = [
          {
            id: 0,
            sortByColumnName:
              translatedMessages[
                'pw.stockIssueTepGep.availableWeightSortLabel'
              ],
            sortAscOrder: false
          },
          {
            id: 1,
            sortByColumnName:
              translatedMessages[
                'pw.stockIssueTepGep.availableQuantitySortLabel'
              ],
            sortAscOrder: false
          }
        ];
      });

    this.appsettingFacade
      .getStoreType()
      .pipe(takeUntil(this.destroy$))
      .subscribe((storeType: string) => {
        if (storeType) {
          this.storeType = storeType;
        }
      });

    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(resp => (this.intialPageEvent.pageSize = JSON.parse(resp)));

    this.appsettingFacade
      .getMaxFilterLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxFilterLimit = data;
      });

    this.appsettingFacade
      .getMaxSortLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxSortLimit = data;
      });

    this.authFacade
      .getLocationCode()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.locationCode = data;
      });

    this.issueTEPFacade.createStockIssue({
      transferType: this.transferType,
      storeType: this.storeType
    });

    this.createStockIssueResponse$ = this.issueTEPFacade.getCreateStockIssueResponse();
    this.createStockIssueResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.issueId = data.id;
        if (data.id) {
          this.changeProductsTab(StockIssueTEPGEPTabTypesEnum.ALLPRODUCTS);
          this.getTotalCount();
        }
      });

    this.form.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
      if (
        this.selectedProductsTabType ===
          StockIssueTEPGEPTabTypesEnum.SELECTEDPRODUCTS &&
        this.selectedItemIds.length === 0 &&
        this.itemForm.value.selectRadioButton === null
      ) {
        this.showNotifications(0);
      } else if (
        this.selectedProductsTabType ===
        StockIssueTEPGEPTabTypesEnum.SELECTEDPRODUCTS
      ) {
        this.showNotifications(this.selectedItemIds.length);
      }
    });

    this.issueTEPFacade
      .getProductCategories()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: ProductCategory[]) => {
        if (data !== null) {
          const PRODUCT_CATEGORIES: { [key: string]: Filter[] } = {
            'Product Category': data.map(productCategory => ({
              id: productCategory.productCategoryCode,
              description: productCategory.description,
              selected: false
            }))
          };
          this.PRODUCT_CATEGORIES = PRODUCT_CATEGORIES;
        }
      });

    this.issueTEPFacade
      .getProductGroups()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: ProductGroup[]) => {
        if (data !== null) {
          const PRODUCT_GROUP: { [key: string]: Filter[] } = {
            'Product Group': data.map(productGroup => ({
              id: productGroup.productGroupCode,
              description: productGroup.description,
              selected: false
            }))
          };
          this.PRODUCT_GROUP = PRODUCT_GROUP;
        }
      });

    this.sortDataItems = this.issueTEPFacade.getSortDataItems();
    this.sortDataItems.pipe(takeUntil(this.destroy$)).subscribe(sortValue => {
      this.sortDataItemsTab = sortValue;
    });
    this.sortDataStockIssueItems = this.issueTEPFacade.getSortDataStockIssueItems();
    this.sortDataStockIssueItems
      .pipe(takeUntil(this.destroy$))
      .subscribe(sortValue => {
        this.sortDataStockIssueItemsTab = sortValue;
      });

    this.filterDataItems = this.issueTEPFacade.getfilterDataItems();
    this.filterDataItems
      .pipe(takeUntil(this.destroy$))
      .subscribe(filterValue => {
        this.filterDataItemsTab = filterValue;
      });
    this.filterDataStockIssueItems = this.issueTEPFacade.getfilterDataStockIssueItems();
    this.filterDataStockIssueItems
      .pipe(takeUntil(this.destroy$))
      .subscribe(filterValue => {
        this.filterDataStockIssueItemsTab = filterValue;
      });

    this.issueTEPFacade.loadCourierDetails(this.locationCode);
    this.issueTEPFacade.loadFactoryAddress();
    this.issueTEPFacade.loadEmployeeCodes();
    this.componentInit();
    this.shortcutService.commands
      .pipe(takeUntil(this.destroy$))
      .subscribe(command => this.shortcutEventHandler(command));
  }

  /**
   * method to handle shortcut commands
   * @param command: shortcut command
   */
  shortcutEventHandler(command: Command) {
    if (command.name === SEARCH_SHORTCUT_KEY_F2) {
      if (this.searchRef) {
        this.searchRef.focus();
      }
    }
  }

  componentInit(): void {
    this.getItems$ = this.issueTEPFacade.getItems();
    this.getStockIssueItems$ = this.issueTEPFacade.getStockIssueItems();
    this.updateStockIssueResponse$ = this.issueTEPFacade.getUpdateStockIssueResponse();
    this.createStockIssueItemsResponse$ = this.issueTEPFacade.getCreateStockIssueItemsResponse();
    this.updateAllStockIssueItemsResponse$ = this.issueTEPFacade.getUpdateAllStockIssueItemsResponse();
    this.isLoading$ = this.issueTEPFacade.getIsLoading();
    this.factoryAddressResponse$ = this.issueTEPFacade.getFactoryAddress();
    this.courierDetailsResponse$ = this.issueTEPFacade.getCourierDetails();
    this.employeeCodesResponse$ = this.issueTEPFacade.getEmployeeCodes();
    this.employeeDetailsResponse$ = this.issueTEPFacade.getEmployeeDetails();
    this.issueTEPFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.issueTEPFacade
      .getTotalItemsCount()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: number) => {
        this.totalItemsCountDisplay = data;
      });

    this.issueTEPFacade
      .getTotalStockIssueItemsCount()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: number) => {
        this.totalStockIssueItemsCountDisplay = data;
      });

    this.getItems$.pipe(takeUntil(this.destroy$)).subscribe(data => {
      this.pageItemsCount = data.length;

      if (data.length !== 0) {
        this.totalItemsCount = data[0].totalElements;
        this.itemIds = data.concat();
        this.itemIdsLength = this.itemIds.length;
        if (
          this.itemForm.value.selectRadioButton ===
          StockIssueTEPGEPRadioButtonTypesEnum.ALLPRODUCTS
        ) {
          this.selectionAllSubject.next({
            selectCheckbox: true,
            enableCheckbox: false
          });
          this.showNotifications(this.totalItemsCount);
        } else if (
          this.itemForm.value.selectRadioButton ===
          StockIssueTEPGEPRadioButtonTypesEnum.CURRENTPAGEPRODUCTS
        ) {
          this.selectionAllSubject.next({
            selectCheckbox: false,
            enableCheckbox: true
          });
          this.itemForm.patchValue({
            selectRadioButton: null
          });
        } else if (this.itemCode !== null) {
          if (data.length === 1) {
            this.itemForm.patchValue({
              selectRadioButton:
                StockIssueTEPGEPRadioButtonTypesEnum.CURRENTPAGEPRODUCTS
            });
            this.selectPagewise();
          }
        }
      } else {
        this.overlayNotification.close();
      }
    });

    this.getStockIssueItems$.pipe(takeUntil(this.destroy$)).subscribe(data => {
      this.pageStockIssueItemsCount = data.length;
      if (data.length !== 0) {
        this.totalStockIssueItemsCount = data[0].totalElements;
        this.itemIds = data.concat();
        this.itemIdsLength = this.itemIds.length;
        this.showNotifications(0);
        if (
          this.itemForm.value.selectRadioButton ===
          StockIssueTEPGEPRadioButtonTypesEnum.ALLPRODUCTS
        ) {
          this.selectionAllSubject.next({
            selectCheckbox: true,
            enableCheckbox: false
          });
          this.showNotifications(this.totalStockIssueItemsCount);
        } else if (
          this.itemForm.value.selectRadioButton ===
          StockIssueTEPGEPRadioButtonTypesEnum.CURRENTPAGEPRODUCTS
        ) {
          this.selectionAllSubject.next({
            selectCheckbox: false,
            enableCheckbox: true
          });
          this.itemForm.patchValue({
            selectRadioButton: null
          });
          this.showNotifications(0);
        } else if (this.itemCode !== null) {
          if (data.length === 1) {
            this.itemForm.patchValue({
              selectRadioButton:
                StockIssueTEPGEPRadioButtonTypesEnum.CURRENTPAGEPRODUCTS
            });
            this.selectPagewise();
          }
        }
      } else {
        this.overlayNotification.close();
      }
    });

    this.createStockIssueItemsResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.loadStockIssueItems(data);
      });

    this.updateStockIssueResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data.srcDocNo) {
          this.srcDocNo = data.srcDocNo;
          this.showConfirmIssueSuccessNotification(data.srcDocNo);
        }
      });

    this.updateAllStockIssueItemsResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.loadStockIssueItems(data);
      });

    this.factoryAddressResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data !== null) {
          this.factoryAddress = data;
        }
      });

    this.employeeCodesResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data !== null) {
          this.employeeCodes = data;
        }
      });

    this.employeeDetailsResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data !== null) {
          this.formList.controls[0].patchValue(
            {
              employeeName: data.empName,
              employeeMobileNumber: data.mobileNo
            },
            { eventEmit: false }
          );
        }
      });
  }

  loadStockIssueItems(data: boolean) {
    if (data === true) {
      this.clearSearch();
      this.getTotalCount();
    }
  }

  back(): void {
    this.router.navigate([getStockIssueRouteUrl(this.issueType)]);
  }

  showSelectProductsNotification(count: number, key: string): void {
    this.hasNotification = true;
    const buttonKey =
      'pw.stockIssueNotificationMessages.btnText-SelectProducts';
    this.translate
      .get([key, buttonKey])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            buttonText: translatedMessages[buttonKey],
            message: count + ' ' + translatedMessages[key]
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.resetList();
              if (
                this.itemForm.value.selectRadioButton ===
                StockIssueTEPGEPRadioButtonTypesEnum.ALLPRODUCTS
              ) {
                this.issueTEPFacade.createStockIssueItems({
                  id: this.issueId,
                  transferType: this.transferType,
                  storeType: this.storeType,
                  itemIds: []
                });
                this.itemForm.patchValue({ selectRadioButton: null });
              } else if (
                this.itemForm.value.selectRadioButton ===
                  StockIssueTEPGEPRadioButtonTypesEnum.CURRENTPAGEPRODUCTS ||
                (this.itemForm.value.selectRadioButton === null &&
                  this.selectedItemIds.length !== 0)
              ) {
                const itemArray = [];
                this.selectedItemIds.forEach(element => {
                  itemArray.push({
                    inventoryId: element.inventoryId
                  });
                });
                this.issueTEPFacade.createStockIssueItems({
                  id: this.issueId,
                  transferType: this.transferType,
                  storeType: this.storeType,
                  itemIds: itemArray
                });
                this.itemForm.patchValue({ selectRadioButton: null });
              }
            }
          });
      });
  }

  showRemoveProductsNotification(count: number, key: string): void {
    this.hasNotification = true;
    const buttonKey =
      'pw.stockIssueNotificationMessages.btnText-RemoveProducts';
    this.translate
      .get([key, buttonKey])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            buttonText: translatedMessages[buttonKey],
            message: count + ' ' + translatedMessages[key]
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.resetList();
              if (
                this.itemForm.value.selectRadioButton ===
                StockIssueTEPGEPRadioButtonTypesEnum.ALLPRODUCTS
              ) {
                this.issueTEPFacade.updateAllStockIssueItems({
                  id: this.issueId,
                  transferType: this.transferType,
                  storeType: this.storeType,
                  itemIds: []
                });
                this.itemForm.patchValue({ selectRadioButton: null });
              } else if (
                this.itemForm.value.selectRadioButton ===
                  StockIssueTEPGEPRadioButtonTypesEnum.CURRENTPAGEPRODUCTS ||
                (this.itemForm.value.selectRadioButton === null &&
                  this.selectedItemIds.length !== 0)
              ) {
                const itemArray = [];
                this.selectedItemIds.forEach(element => {
                  itemArray.push(element.id);
                });
                this.issueTEPFacade.updateAllStockIssueItems({
                  id: this.issueId,
                  transferType: this.transferType,
                  storeType: this.storeType,
                  itemIds: itemArray
                });
                this.itemForm.patchValue({ selectRadioButton: null });
              }
            }
          });
      });
  }

  showConfirmIssueNotification(key: string): void {
    this.hasNotification = true;
    const buttonKey = 'pw.stockIssueNotificationMessages.btnText-ConfirmIssue';
    this.translate
      .get([key, buttonKey])
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessages: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            buttonText: translatedMessages[buttonKey],
            hasRemarks: true,
            isRemarksMandatory: true,
            message: translatedMessages[key]
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (this.form.invalid) {
              this.showNotifications(0);
            } else {
              this.showProgressNotification();
              if (
                this.selectedFormName ===
                StockIssueTEPGEPCarrierTypesEnum.HandCarry
              ) {
                this.issueTEPFacade.updateStockIssue({
                  id: this.issueId,
                  transferType: this.transferType,
                  storeType: this.storeType,
                  remarks: event.data,
                  carrierDetails: {
                    type: StockIssueTEPGEPCarrierTypesEnum.employee,
                    data: {
                      employeeId: this.form.value.formCollection[0].employeeID,
                      employeeName: this.form.value.formCollection[0]
                        .employeeName,
                      designation: '',
                      emailId: '',
                      mobileNo: this.form.value.formCollection[0]
                        .employeeMobileNumber,
                      brand: '',
                      numberOfBoxes: 0,
                      boxDetails: []
                    }
                  },
                  destinationLocationCode: this.factoryAddress.locationCode
                });
              } else {
                this.issueTEPFacade.updateStockIssue({
                  id: this.issueId,
                  transferType: this.transferType,
                  storeType: this.storeType,
                  remarks: event.data,
                  carrierDetails: {
                    type: StockIssueTEPGEPCarrierTypesEnum.courier,
                    data: {
                      companyName: this.selectedFormName,
                      docketNumber: this.form.value.formCollection[0]
                        .courierDocketNumber,
                      lockNumber: this.form.value.formCollection[0]
                        .courierLockNumber,
                      roadPermitNumber: this.form.value.formCollection[0]
                        .courierRoadPermitNumber,
                      numberOfBoxes: 0,
                      boxDetails: []
                    }
                  },
                  destinationLocationCode: this.factoryAddress.locationCode
                });
              }
            }
          });
      });
  }

  showConfirmIssueSuccessNotification(srcDocNo: number): void {
    this.hasNotification = true;
    const key = 'pw.stockIssueNotificationMessages.confirmSuccessMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.CUSTOM,
            message: translatedMessage + srcDocNo,
            hasBackdrop: true,
            hasClose: true,
            template: this.confirmSuccessNotificationTemplate
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.CLOSE) {
              this.back();
            }
          });
      });
  }

  showEnterCourierDetailsNotification(key: string): void {
    this.hasNotification = true;
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification.show({
          type: OverlayNotificationType.SIMPLE,
          message: translatedMessage,
          hasClose: true
        });
      });
  }

  showProgressNotification(): void {
    const key = 'pw.stockReceiveNotificationMessages.progressMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMessage: string) => {
        this.overlayNotification.show({
          type: OverlayNotificationType.PROGRESS,
          message: translatedMessage,
          hasBackdrop: true
        });
      });
  }

  selectAll(): void {
    this.selectedItemIds = this.itemIds.concat();
    this.selectionAllSubject.next({
      selectCheckbox: true,
      enableCheckbox: false
    });

    if (
      this.selectedProductsTabType === StockIssueTEPGEPTabTypesEnum.ALLPRODUCTS
    ) {
      this.productToBeSavedCount = this.totalItemsCount;
    }
    if (
      this.selectedProductsTabType ===
      StockIssueTEPGEPTabTypesEnum.SELECTEDPRODUCTS
    ) {
      this.productToBeSavedCount = this.totalStockIssueItemsCount;
    }
    if (this.productToBeSavedCount !== 0) {
      this.showNotifications(this.productToBeSavedCount);
    }
  }

  selectPagewise(): void {
    this.selectedItemIds = this.itemIds.concat();
    this.selectionAllSubject.next({
      selectCheckbox: true,
      enableCheckbox: true
    });
    if (
      this.selectedProductsTabType === StockIssueTEPGEPTabTypesEnum.ALLPRODUCTS
    ) {
      this.productToBeSavedCount = this.pageItemsCount;
    }
    if (
      this.selectedProductsTabType ===
      StockIssueTEPGEPTabTypesEnum.SELECTEDPRODUCTS
    ) {
      this.productToBeSavedCount = this.pageStockIssueItemsCount;
    }
    if (this.productToBeSavedCount !== 0) {
      this.showNotifications(this.productToBeSavedCount);
    }
  }

  onSelectionEmit(event: ItemSelection): void {
    if (event.isSelected === false) {
      this.itemForm.patchValue({
        selectRadioButton: null
      });
      this.productToBeSavedCount = 0;
      const itemToRemove = event.item;
      this.selectedItemIds.splice(
        this.selectedItemIds.indexOf(itemToRemove),
        1
      );
    } else if (event.isSelected === true) {
      this.selectedItemIds.push(event.item);
      if (this.selectedItemIds.length === this.itemIdsLength) {
        this.itemForm.patchValue({
          selectRadioButton:
            StockIssueTEPGEPRadioButtonTypesEnum.CURRENTPAGEPRODUCTS
        });
      }
    }
    this.productToBeSavedCount = this.selectedItemIds.length;
    if (this.productToBeSavedCount !== 0) {
      this.showNotifications(this.productToBeSavedCount);
    } else {
      if (
        this.selectedProductsTabType ===
        StockIssueTEPGEPTabTypesEnum.SELECTEDPRODUCTS
      ) {
        this.showNotifications(0);
      } else {
        this.overlayNotification.close();
      }
    }
  }

  resetList(): void {
    this.issueTEPFacade.resetList();
    this.issueTEPFacade.resetResponse();
  }

  resetValue(): void {
    this.itemIds = [];
    this.selectedItemIds = [];
    this.itemIdsLength = 0;
    this.productToBeSavedCount = 0;
  }

  selectTransferType(reqType: string): TransferType {
    switch (reqType) {
      case StockIssueTEPGEPTypesEnum.TEP_PLAIN:
        return {
          binCode: StockIssueTEPGEPBinCodesEnum.TEP,
          transferType: StockIssueTEPGEPTransferTypesEnum.TEP_PLAIN
        };
      case StockIssueTEPGEPTypesEnum.TEP_STUDDED:
        return {
          binCode: StockIssueTEPGEPBinCodesEnum.TEP,
          transferType: StockIssueTEPGEPTransferTypesEnum.TEP_STUDDED
        };
      case StockIssueTEPGEPTypesEnum.GEP:
        return {
          binCode: StockIssueTEPGEPBinCodesEnum.GEP,
          transferType: StockIssueTEPGEPTransferTypesEnum.OTHERS
        };
    }
  }

  changeProductsTab(newProductsTabType: StockIssueTEPGEPTabTypesEnum): void {
    if (this.selectedProductsTabType !== newProductsTabType) {
      this.itemForm.patchValue({ selectRadioButton: null });
      this.selectedProductsTabType = newProductsTabType;
      this.clearSearch();
    }
  }

  showNotifications(count: number): void {
    this.overlayNotification.close();
    if (
      this.selectedProductsTabType === StockIssueTEPGEPTabTypesEnum.ALLPRODUCTS
    ) {
      this.showSelectProductsNotification(
        count,
        'pw.stockIssueNotificationMessages.productsSelectedOverlayMessage'
      );
    } else if (
      this.selectedProductsTabType ===
      StockIssueTEPGEPTabTypesEnum.SELECTEDPRODUCTS
    ) {
      if (
        this.itemForm.value.selectRadioButton === null &&
        this.selectedItemIds.length === 0 &&
        this.totalStockIssueItemsCount !== 0
      ) {
        if (this.form.invalid && this.factoryAddress.locationCode !== null) {
          this.showEnterCourierDetailsNotification(
            'pw.stockIssueNotificationMessages.enterCarrierDetailsMessage'
          );
        } else {
          this.showConfirmIssueNotification(
            'pw.stockIssueNotificationMessages.confirmOverlayMessage'
          );
        }
      } else if (this.totalStockIssueItemsCount !== 0) {
        this.showRemoveProductsNotification(
          count,
          'pw.stockIssueNotificationMessages.productsSelectedOverlayMessage'
        );
      }
    }
  }

  loadProducts(): void {
    this.resetValue();
    this.clearAll();
    if (
      this.selectedProductsTabType === StockIssueTEPGEPTabTypesEnum.ALLPRODUCTS
    ) {
      this.issueTEPFacade.loadItems({
        id: this.issueId,
        itemCode: this.itemCode,
        lotNumber: this.lotNumber,
        transferType: this.transferType,
        storeType: this.storeType,
        status: StockIssueTEPGEPStatusesEnum.OPEN,
        pageIndex: this.itemsPageEvent.pageIndex,
        pageSize: this.itemsPageEvent.pageSize,
        sort: this.sortDataItemsByMap,
        filter: this.filterDataItemsByMap
      });
    } else if (
      this.selectedProductsTabType ===
      StockIssueTEPGEPTabTypesEnum.SELECTEDPRODUCTS
    ) {
      this.issueTEPFacade.loadStockIssueItems({
        id: this.issueId,
        itemCode: this.itemCode,
        lotNumber: this.lotNumber,
        transferType: this.transferType,
        storeType: this.storeType,
        status: StockIssueTEPGEPStatusesEnum.SELECTED,
        pageIndex: this.stockIssueItemsPageEvent.pageIndex,
        pageSize: this.stockIssueItemsPageEvent.pageSize,
        sort: this.sortDataStockIssueItemsByMap,
        filter: this.filterDataStockIssueItemsByMap
      });
    }
  }

  itemsListPaginate(event: PageEvent): void {
    this.overlayNotification.close();
    this.itemsPageEvent = event;
    this.loadProducts();
  }

  stockIssueItemsListPaginate(event: PageEvent): void {
    this.overlayNotification.close();
    this.stockIssueItemsPageEvent = event;
    this.loadProducts();
  }

  selectRadioButtonChange(): void {
    if (
      this.itemForm.value.selectRadioButton ===
      StockIssueTEPGEPRadioButtonTypesEnum.ALLPRODUCTS
    ) {
      this.selectAll();
    } else if (
      this.itemForm.value.selectRadioButton ===
      StockIssueTEPGEPRadioButtonTypesEnum.CURRENTPAGEPRODUCTS
    ) {
      this.selectPagewise();
    }
  }

  /**
   * Search searchItems based on variant code
   */
  searchItems(searchData: SearchResponse) {
    this.itemCode = searchData.searchValue;
    this.lotNumber = searchData.lotNumber;
    this.itemsPageEvent = this.intialPageEvent;
    this.stockIssueItemsPageEvent = this.intialPageEvent;
    this.loadProducts();
  }

  /**
   * to display error message
   * @param error : error from api
   */
  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true, // optional
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        // Action based event
      });
  }

  getTotalCount(): void {
    this.issueTEPFacade.loadTotalItemsCount({
      id: this.issueId,
      itemCode: this.itemCode,
      lotNumber: this.lotNumber,
      transferType: this.transferType,
      storeType: this.storeType,
      status: StockIssueTEPGEPStatusesEnum.OPEN,
      sort: this.sortDataItemsByMap,
      filter: this.filterDataItemsByMap
    });
    this.issueTEPFacade.loadTotalStockIssueItemsCount({
      id: this.issueId,
      itemCode: this.itemCode,
      lotNumber: this.lotNumber,
      transferType: this.transferType,
      storeType: this.storeType,
      status: StockIssueTEPGEPStatusesEnum.SELECTED,
      sort: this.sortDataStockIssueItemsByMap,
      filter: this.filterDataStockIssueItemsByMap
    });
  }

  loadForm(event): void {
    this.formList.clear();
    this.selectedFormName = event.value;
    if (event.value !== StockIssueTEPGEPCarrierTypesEnum.HandCarry) {
      this.formList.push(this.createcourierForm());
    } else {
      this.formList.push(this.createemployeeForm());
    }
  }

  createcourierForm(): FormGroup {
    return this.formBuilder.group({
      courierDocketNumber: [
        '',
        Validators.compose([Validators.required, Validators.maxLength(30)])
      ],
      courierLockNumber: [
        '',
        Validators.compose([Validators.required, Validators.maxLength(30)])
      ],
      courierRoadPermitNumber: [
        '',
        Validators.compose([Validators.required, Validators.maxLength(30)])
      ]
    });
  }

  createemployeeForm(): FormGroup {
    return this.formBuilder.group({
      employeeID: [
        '',
        Validators.compose([Validators.required, Validators.maxLength(30)])
      ],
      employeeName: [
        '',
        Validators.compose([Validators.required, Validators.maxLength(30)])
      ],
      employeeMobileNumber: [
        '',
        Validators.compose([
          Validators.required,
          Validators.maxLength(10),
          Validators.minLength(10),
          Validators.pattern('[0-9]+')
        ])
      ]
    });
  }

  openSort(): void {
    this.sortDialogService.DataSource = this.SORT_DATA;
    if (
      this.selectedProductsTabType === StockIssueTEPGEPTabTypesEnum.ALLPRODUCTS
    ) {
      this.sortData = this.sortDataItemsTab;
    } else if (
      this.selectedProductsTabType ===
      StockIssueTEPGEPTabTypesEnum.SELECTEDPRODUCTS
    ) {
      this.sortData = this.sortDataStockIssueItemsTab;
    }
    this.sortDialogService
      .openDialog(this.maxSortLimit, this.sortData)
      .pipe(takeUntil(this.destroy$))
      .subscribe((sortResult: { data: Column[]; actionfrom: string }) => {
        if (sortResult.actionfrom === FilterActions.APPLY) {
          if (
            this.selectedProductsTabType ===
            StockIssueTEPGEPTabTypesEnum.ALLPRODUCTS
          ) {
            this.issueTEPFacade.setSortDataItems(sortResult.data);
            this.sortDataItemsByMap.clear();
            const sortData = sortResult.data;
            if (sortData == null || sortData.length === 0) {
              this.sortData = [];
              this.sortOrder = null;
              this.sortBy = null;
            } else {
              this.sortData = sortData;
              if (sortData.length > 0) {
                if (sortData[0].id === 0) {
                  this.sortBy = 'availableWeight';
                } else if (sortData[0].id === 1) {
                  this.sortBy = 'availableQuantity';
                }
                this.sortOrder = sortData[0].sortAscOrder ? 'ASC' : 'DESC';
              }
            }
            if (this.sortBy !== null && this.sortOrder !== null) {
              this.sortDataItemsByMap.set(
                'sort',
                this.sortBy + ',' + this.sortOrder
              );
            }
            this.itemsPageEvent = this.intialPageEvent;
            this.loadProducts();
          } else if (
            this.selectedProductsTabType ===
            StockIssueTEPGEPTabTypesEnum.SELECTEDPRODUCTS
          ) {
            this.issueTEPFacade.setSortDataStockIssueItems(sortResult.data);
            this.sortDataStockIssueItemsByMap.clear();
            const sortData = sortResult.data;
            if (sortData == null || sortData.length === 0) {
              this.sortData = [];
              this.sortOrder = null;
              this.sortBy = null;
            } else {
              this.sortData = sortData;
              if (sortData.length > 0) {
                if (sortData[0].id === 0) {
                  this.sortBy = 'availableWeight';
                } else if (sortData[0].id === 1) {
                  this.sortBy = 'availableQuantity';
                }
                this.sortOrder = sortData[0].sortAscOrder ? 'ASC' : 'DESC';
              }
            }
            if (this.sortBy !== null && this.sortOrder !== null) {
              this.sortDataStockIssueItemsByMap.set(
                'sort',
                this.sortBy + ',' + this.sortOrder
              );
            }
            this.stockIssueItemsPageEvent = this.intialPageEvent;
            this.loadProducts();
          }
        }
      });
  }

  openFilter(): void {
    this.filterService.DataSource = {
      ...this.PRODUCT_CATEGORIES,
      ...this.PRODUCT_GROUP
    };
    if (
      this.selectedProductsTabType === StockIssueTEPGEPTabTypesEnum.ALLPRODUCTS
    ) {
      this.filterData = this.filterDataItemsTab;
    } else if (
      this.selectedProductsTabType ===
      StockIssueTEPGEPTabTypesEnum.SELECTEDPRODUCTS
    ) {
      this.filterData = this.filterDataStockIssueItemsTab;
    }
    const productCategory = [];
    const productGroup = [];
    this.filterService
      .openDialog(this.maxFilterLimit, this.filterData)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (filterResult: {
          data: { [key: string]: Filter[] };
          actionfrom: string;
        }) => {
          if (filterResult.actionfrom === FilterActions.APPLY) {
            if (
              this.selectedProductsTabType ===
              StockIssueTEPGEPTabTypesEnum.ALLPRODUCTS
            ) {
              this.issueTEPFacade.setFilterDataItems(filterResult.data);
              this.filterDataItemsByMap.clear();
              const filterData = filterResult.data;
              if (filterData == null) {
                this.filterData = {};
              } else {
                this.filterData = filterData;
              }
              filterData['Product Category'].forEach(value => {
                productCategory.push(value.id);
              });
              filterData['Product Group'].forEach(value => {
                productGroup.push(value.id);
              });
              if (productCategory.length !== 0) {
                this.filterDataItemsByMap.set(
                  'productCategory',
                  productCategory
                );
              }
              if (productGroup.length !== 0) {
                this.filterDataItemsByMap.set('productGroup', productGroup);
              }
              this.itemsPageEvent = this.intialPageEvent;
              this.loadProducts();
            } else if (
              this.selectedProductsTabType ===
              StockIssueTEPGEPTabTypesEnum.SELECTEDPRODUCTS
            ) {
              this.issueTEPFacade.setFilterDataStockIssueItems(
                filterResult.data
              );
              this.filterDataStockIssueItemsByMap.clear();
              const filterData = filterResult.data;
              if (filterData == null) {
                this.filterData = {};
              } else {
                this.filterData = filterData;
              }
              filterData['Product Category'].forEach(value => {
                productCategory.push(value.id);
              });
              filterData['Product Group'].forEach(value => {
                productGroup.push(value.id);
              });
              if (productCategory.length !== 0) {
                this.filterDataStockIssueItemsByMap.set(
                  'productCategory',
                  productCategory
                );
              }
              if (productGroup.length !== 0) {
                this.filterDataStockIssueItemsByMap.set(
                  'productGroup',
                  productGroup
                );
              }
              this.stockIssueItemsPageEvent = this.intialPageEvent;
              this.loadProducts();
            }
          }
        }
      );
  }

  clearSearch(): void {
    this.itemCode = null;
    this.lotNumber = null;
    if (this.searchRef) {
      this.searchRef.reset();
    }
    this.issueTEPFacade.searchClear();
    this.overlayNotification.close();
    this.loadProducts();
  }

  clearAll(): void {
    this.itemForm.patchValue({
      selectRadioButton: null
    });
    this.selectionAllSubject.next({
      selectCheckbox: false,
      enableCheckbox: true
    });
    this.selectedItemIds = [];
    if (
      this.selectedProductsTabType ===
        StockIssueTEPGEPTabTypesEnum.SELECTEDPRODUCTS &&
      this.itemIds.length !== 0
    ) {
      this.showNotifications(0);
    } else {
      this.overlayNotification.close();
    }
  }

  employeeIDSelected(event: any, employeeCode: string) {
    if (event.isUserInput) {
      this.isReadOnly = true;
      if (this.employeeCodes.includes(employeeCode)) {
        this.formList.controls[0].patchValue({
          employeeID: employeeCode
        });
        this.issueTEPFacade.loadEmployeeDetails(employeeCode);
      }
    }
  }

  employeeIDChange(value) {
    const employeeInput = value;
    if (this.employeeCodes.includes(employeeInput)) {
      this.isReadOnly = true;
      this.issueTEPFacade.loadEmployeeDetails(employeeInput);
    } else {
      this.isReadOnly = false;
      this.formList.controls[0].patchValue({
        employeeName: '',
        employeeMobileNumber: ''
      });
    }
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
