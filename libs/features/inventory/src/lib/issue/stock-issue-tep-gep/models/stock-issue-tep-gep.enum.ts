export enum StockIssueTEPGEPTabTypesEnum {
  ALLPRODUCTS = 'ALLPRODUCTS',
  SELECTEDPRODUCTS = 'SELECTEDPRODUCTS'
}

export enum StockIssueTEPGEPStatusesEnum {
  OPEN = 'OPEN',
  SELECTED = 'SELECTED'
}

export enum StockIssueTEPGEPBinCodesEnum {
  TEP = 'TEP',
  GEP = 'GEP'
}

export enum StockIssueTEPGEPTransferTypesEnum {
  TEP_PLAIN = 'TEP_PLAIN',
  TEP_STUDDED = 'TEP_STUDDED',
  OTHERS = 'OTHERS'
}

export enum StockIssueTEPGEPTypesEnum {
  TEP_PLAIN = 'TEP_PLAIN',
  TEP_STUDDED = 'TEP_STUDDED',
  GEP = 'GEP'
}

export enum StockIssueTEPGEPRadioButtonTypesEnum {
  ALLPRODUCTS = '1',
  CURRENTPAGEPRODUCTS = '2'
}

export enum StockIssueTEPGEPCarrierTypesEnum {
  courier = 'courier',
  employee = 'employee',
  HandCarry = 'HAND CARRY'
}
