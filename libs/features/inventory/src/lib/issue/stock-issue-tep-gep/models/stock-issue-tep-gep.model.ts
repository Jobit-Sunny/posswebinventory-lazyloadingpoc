import { Moment } from 'moment';

export interface CreateStockIssueResponse {
  id: number;
  srcLocationCode: string;
  destLocationCode: string;
  status: string;
  weightUnit: string;
  currencyCode: string;
  srcDocNo: number;
  srcFiscalYear: number;
  srcDocDate: Moment;
  destDocNo: number;
  destDocDate: Moment;
  orderType: string;
  totalAvailableQuantity: number;
  totalMeasuredQuantity: number;
  totalAvailableValue: number;
  totalMeasuredValue: number;
  totalAvailableWeight: number;
  totalMeasuredWeight: number;
  // for l1/l2
  transferType?: string;
  // for l3
  invoiceType?: string;
  // for l1/l2
  courierReceivedDate?: Moment;
  courierDetails?: {};
  // for l3
  issuedRemarks?: string;
}

export interface StockIssueItem {
  id: number;
  itemCode: string;
  lotNumber: string;
  mfgDate: Moment;
  productCategory: string;
  productGroup: string;
  binCode: string;
  binGroupCode: string;
  stdValue: number;
  stdWeight: number;
  currencyCode: string;
  weightUnit: string;
  status: string;
  imageURL: string;
  itemDetails: {};
  availableQuantity: number;
  availableWeight: number;
  availableValue: number;
  measuredQuantity: number;
  measuredWeight: number;
  measuredValue: number;
  orderType: string;
  inventoryId: string;
  // for l3
  remarks?: string;
  totalElements: number;
}

export interface ItemSelection {
  isSelected: boolean;
  item: StockIssueItem;
}

export interface TransferType {
  binCode: string;
  transferType: string;
}

export interface Form {
  value: string;
  viewValue: string;
}

export interface ItemSelectionAll {
  selectCheckbox: boolean;
  enableCheckbox: boolean;
}
