import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StockIssueListComponent } from './stock-issue/containers/stock-issue-list/stock-issue-list.component';
import { AuthGuard } from '@poss-web/core';
import { StockIssueTEPComponent } from './stock-issue-tep-gep/containers/stock-issue-tep-gep/stock-issue-tep-gep.component';
import { ReturnInvoiceCfaDetailsComponent } from './return-invoice-cfa/container/return-invoice-cfa-details.component';
import { StockIssueDetailsComponent } from './stock-issue/containers/stock-issue-details/stock-issue-details.component';

const routes: Routes = [
  {
    path: 'cfa/:id',
    component: ReturnInvoiceCfaDetailsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: ':type/:reqDocNo',
    component: StockIssueDetailsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'returninvoice',
    component: ReturnInvoiceCfaDetailsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: ':type/tep-gep/:transferType',
    component: StockIssueTEPComponent,
    canActivate: [AuthGuard]
  },
  {
    path: ':type',
    component: StockIssueListComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IssueRoutingModule {}
