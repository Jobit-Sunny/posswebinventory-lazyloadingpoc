import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IssueRoutingModule } from './issue-routing.module';
import { EffectsModule } from '@ngrx/effects';
import { IssueTEPEffects } from './stock-issue-tep-gep/+state/stock-issue-tep-gep.effect';
import { ReturnInvoiceEffect } from './return-invoice-cfa/+state/return-invoice-cfa.effects';
import { StockIssueEffect } from './stock-issue/+state/stock-issue.effects';
import { OutOfStockPopupComponent } from './stock-issue/components/out-of-stock-popup/out-of-stock-popup.component';
import { CourierDetailsPopupComponent } from './stock-issue/components/courier-details-popup/courier-details-popup.component';
import { StockIssueTepGepService } from './stock-issue-tep-gep/services/stock-issue-tep-gep.service';
import { IssueTEPFacade } from './stock-issue-tep-gep/+state/stock-issue-tep-gep.facade';
import { ReturnInvoiceFacade } from './return-invoice-cfa/+state/return-invoice-cfa.facade';
import { StockIssueFacade } from './stock-issue/+state/stock-issue.facade';
import { StockIssueService } from './stock-issue/services/stock-issue.service';
import { StockIssueTepItemListComponent } from './stock-issue-tep-gep/components/stock-issue-tep-gep-item-list/stock-issue-tep-gep-item-list.component';
import { StockIssueTepItemComponent } from './stock-issue-tep-gep/components/stock-issue-tep-gep-item/stock-issue-tep-gep-item.component';
import { StockIssueTEPComponent } from './stock-issue-tep-gep/containers/stock-issue-tep-gep/stock-issue-tep-gep.component';
import { ReturnInvoiceCfaItemlistComponent } from './return-invoice-cfa/components/return-invoice-cfa-itemlist/return-invoice-cfa-itemlist.component';
import { ReturnInvoiceCfaItemComponent } from './return-invoice-cfa/components/return-invoice-cfa-item/return-invoice-cfa-item.component';
import { ReturnInvoiceCfaDetailsComponent } from './return-invoice-cfa/container/return-invoice-cfa-details.component';
import { StockIssueItemComponent } from './stock-issue/components/stock-issue-item/stock-issue-item.component';
import { StockIssueItemListComponent } from './stock-issue/components/stock-issue-item-list/stock-issue-item-list.component';
import { StockIssueDetailsComponent } from './stock-issue/containers/stock-issue-details/stock-issue-details.component';
import { StockIssueListComponent } from './stock-issue/containers/stock-issue-list/stock-issue-list.component';
import { SharedModule, UicomponentsModule } from '@poss-web/shared';
import { MatSidenavModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    IssueRoutingModule,
    SharedModule,
    UicomponentsModule,
    MatSidenavModule,

    EffectsModule.forFeature([
      StockIssueEffect,
      ReturnInvoiceEffect,
      IssueTEPEffects
    ])
  ],
  declarations: [
    StockIssueListComponent,
    StockIssueDetailsComponent,
    StockIssueItemListComponent,
    StockIssueItemComponent,
    ReturnInvoiceCfaDetailsComponent,
    ReturnInvoiceCfaItemComponent,
    ReturnInvoiceCfaItemlistComponent,
    StockIssueTEPComponent,
    StockIssueTepItemComponent,
    StockIssueTepItemListComponent,
    // LoanIssueDetailsComponent,
    OutOfStockPopupComponent,
    CourierDetailsPopupComponent
  ],
  providers: [
    StockIssueService,
    StockIssueFacade,
    ReturnInvoiceFacade,
    IssueTEPFacade,
    StockIssueTepGepService
  ],
  entryComponents: [OutOfStockPopupComponent, CourierDetailsPopupComponent]
})
export class IssueStockModule {}
