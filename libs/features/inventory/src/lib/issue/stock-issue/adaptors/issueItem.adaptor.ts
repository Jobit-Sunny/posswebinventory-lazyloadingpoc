import { IssueInventoryItem } from '../models/stock-issue-models';
import * as moment from 'moment';

export class IssueItemAdaptor {
  static fromJson(issueItem: any): IssueInventoryItem {
    return {
      availableQuantity: issueItem.availableQuantity,
      availableValue: issueItem.availableValue,
      availableWeight: issueItem.availableWeight,
      binCode: issueItem.binCode,
      binGroupCode: issueItem.binGroupCode,
      currencyCode: issueItem.currencyCode,
      id: issueItem.id,
      imageURL: issueItem.imageURL,
      inventoryId: issueItem.inventoryId,
      itemCode: issueItem.itemCode,
      itemDetails: {},
      lotNumber: issueItem.lotNumber,
      measuredQuantity: issueItem.measuredQuantity,
      measuredValue: issueItem.measuredValue,
      measuredWeight: issueItem.measuredWeight,
      mfgDate: moment(issueItem.mfgDate),
      orderType: issueItem.orderType,
      productCategory: issueItem.productCategory,
      productGroup: issueItem.productGroup,
      status: issueItem.status,
      stdValue: issueItem.stdValue,
      stdWeight: issueItem.stdWeight,
      weightUnit: issueItem.weightUnit,
      isUpdating: null,
      isUpdatingSuccess: null
    };
  }
}
