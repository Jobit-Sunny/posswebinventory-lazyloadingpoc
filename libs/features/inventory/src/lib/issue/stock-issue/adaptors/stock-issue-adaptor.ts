import * as moment from 'moment';
import {
  StockRequestNote,
  RequestInvoice,
  IssueConfirmResponse,
  RequestStockTransferNote
} from '../models/stock-issue-models';
import {
  LoadIssueInvoicePayload,
  LoadIssueSTNCountsPayload,
  CourierDetailsPayload
} from '../+state/stock-issue.actions';
import { Filter } from '@poss-web/shared';

// import { IssueCountState } from './+state/Issue-count/issue-count.state';

/**
 * Adapters for the Stock Receive
 */
export class StockIssueAdaptor {
  //new
  static stockRequestNotesFromJson(data: any): StockRequestNote[] {
    const stockRequestNotes: StockRequestNote[] = [];
    for (const stockRequestNote of data.results) {
      stockRequestNotes.push({
        carrierDetails: stockRequestNote.carrierDetails,
        currencyCode: stockRequestNote.currencyCode,
        destDocDate: moment(stockRequestNote.destDocDate),
        destDocNo: stockRequestNote.destDocNo,
        destLocationCode: stockRequestNote.destLocationCode,
        id: stockRequestNote.id,
        orderType: stockRequestNote.orderType,
        otherDetails: stockRequestNote.otherDetails,
        reqDocDate: moment(stockRequestNote.reqDocDate),
        reqDocNo: stockRequestNote.reqDocNo,
        reqLocationCode: stockRequestNote.reqLocationCode,
        requestType: stockRequestNote.requestType,
        srcDocDate: moment(stockRequestNote.srcDocDate),
        srcDocNo: stockRequestNote.srcDocNo,
        srcFiscalYear: stockRequestNote.srcFiscalYear,
        srcLocationCode: stockRequestNote.srcFiscalYear,
        status: stockRequestNote.status,
        totalAvailableQuantity: stockRequestNote.totalAvailableQuantity,
        totalAvailableValue: stockRequestNote.totalAvailableValue,
        totalAvailableWeight: stockRequestNote.totalAvailableWeight,
        totalMeasuredQuantity: stockRequestNote.totalMeasuredQuantity,
        totalMeasuredValue: stockRequestNote.totalMeasuredValue,
        totalMeasuredWeight: stockRequestNote.totalMeasuredWeight,
        weightUnit: stockRequestNote.weightUnit
      });
    }
    return stockRequestNotes;
  }

  //new
  static stockRequestNoteFromJson(data: any): StockRequestNote {
    if (!data && data.results && data.results.length === 0) {
      return null;
    }
    const stockRequestNoteData = data.results[0];
    const stockRequestNote: StockRequestNote = {
      carrierDetails: stockRequestNoteData.carrierDetails,
      currencyCode: stockRequestNoteData.currencyCode,
      destDocDate: moment(stockRequestNoteData.destDocDate),
      destDocNo: stockRequestNoteData.destDocNo,
      destLocationCode: stockRequestNoteData.destLocationCode,
      id: stockRequestNoteData.id,
      orderType: stockRequestNoteData.orderType,
      otherDetails: stockRequestNoteData.otherDetails,
      reqDocDate: moment(stockRequestNoteData.reqDocDate),
      reqDocNo: stockRequestNoteData.reqDocNo,
      reqLocationCode: stockRequestNoteData.reqLocationCode,
      requestType: stockRequestNoteData.requestType,
      srcDocDate: moment(stockRequestNoteData.srcDocDate),
      srcDocNo: stockRequestNoteData.srcDocNo,
      srcFiscalYear: stockRequestNoteData.srcFiscalYear,
      srcLocationCode: stockRequestNoteData.srcFiscalYear,
      status: stockRequestNoteData.status,
      totalAvailableQuantity: stockRequestNoteData.totalAvailableQuantity,
      totalAvailableValue: stockRequestNoteData.totalAvailableQuantity,
      totalAvailableWeight: stockRequestNoteData.totalAvailableWeight,
      totalMeasuredQuantity: stockRequestNoteData.totalMeasuredQuantity,
      totalMeasuredValue: stockRequestNoteData.totalMeasuredValue,
      totalMeasuredWeight: stockRequestNoteData.totalMeasuredWeight,
      weightUnit: stockRequestNoteData.weightUnit
    };
    return stockRequestNote;
  }

  /**
   * The function maps the json data to respective model type
   */
  // static stockTransferNotesFromJson(data: any): RequestStockTransferNote[] {
  //   // for (const stockTransferNote of data.results) {
  //   const requestStockTransferNotes: RequestStockTransferNote[] = [];
  //   for (const requestStockTransferNote of data.results) {
  //     requestStockTransferNotes.push({
  //       currencyUnit: requestStockTransferNote.currencyUnit,
  //       destLocationCode: requestStockTransferNote.destLocationCode,
  //       id: requestStockTransferNote.id,
  //       reqDocDate: moment(requestStockTransferNote.reqDocDate),
  //       reqDocNo: requestStockTransferNote.reqDocNo,
  //       reqLocationCode: requestStockTransferNote.reqLocationCode,
  //       requestType: requestStockTransferNote.requestType,
  //       srcLocationCode: requestStockTransferNote.srcLocationCode,
  //       status: requestStockTransferNote.status,
  //       totalQuantity: requestStockTransferNote.totalQuantity,
  //       totalValue: requestStockTransferNote.totalValue,
  //       totalWeight: requestStockTransferNote.totalWeight,
  //       weightUnit: requestStockTransferNote.weightUnit
  //     });
  //     // }
  //   }
  //   return requestStockTransferNotes;
  // }

  static requestInvoiceFromJson(data: any): RequestInvoice[] {
    const requestInvoices: RequestInvoice[] = [];
    for (const requestInvoice of data.results) {
      requestInvoices.push({
        currencyCode: requestInvoice.currencyCode,
        destDocDate: moment(requestInvoice.destDocDate),
        destDocNo: requestInvoice.destDocNo,
        destLocationCode: requestInvoice.destLocationCode,
        id: requestInvoice.id,
        invoiceType: requestInvoice.invoiceType,
        orderType: requestInvoice.orderType,
        reason: requestInvoice.reason,
        srcDocDate: moment(requestInvoice.srcDocDate),
        srcDocNo: requestInvoice.srcDocNo,
        srcFiscalYear: requestInvoice.srcFiscalYear,
        srcLocationCode: requestInvoice.srcLocationCode,
        status: requestInvoice.status,
        totalQuantity: requestInvoice.totalQuantity,
        totalValue: requestInvoice.totalValue,
        totalWeight: requestInvoice.totalWeight,
        weightUnit: requestInvoice.weightUnit
      });
    }

    return requestInvoices;
  }

  static requestStockTransferNoteFromJson(data: any): RequestStockTransferNote {
    if (!data && data.results && data.results.length === 0) {
      return null;
    }
    const requestStockTransferNoteData = data.results[0];
    const requestStockTransferNote: RequestStockTransferNote = {
      currencyUnit: requestStockTransferNoteData.currencyUnit,
      destLocationCode: requestStockTransferNoteData.destLocationCode,
      id: requestStockTransferNoteData.id,
      reqDocDate: moment(requestStockTransferNoteData.reqDocDate),
      reqDocNo: requestStockTransferNoteData.reqDocNo,
      reqLocationCode: requestStockTransferNoteData.reqLocationCode,
      requestType: requestStockTransferNoteData.requestType,
      srcLocationCode: requestStockTransferNoteData.srcLocationCode,
      status: requestStockTransferNoteData.status,
      totalQuantity: requestStockTransferNoteData.totalQuantity,
      totalValue: requestStockTransferNoteData.totalValue,
      totalWeight: requestStockTransferNoteData.totalWeight,
      weightUnit: requestStockTransferNoteData.weightUnit
    };
    return requestStockTransferNote;
  }

  static issueConfirmResponseFromJson(data: any): IssueConfirmResponse {
    if (!data) {
      // Throw
      return null;
    }
    const issueConfirmResponse: IssueConfirmResponse = {
      courierDetails: data.courierDetails,
      courierReceivedDate: moment(data.courierReceivedDate),
      currencyCode: data.currencyCode,
      destDocDate: moment(data.destDocDate),
      destDocNo: data.destDocNo,
      destLocationCode: data.destLocationCode,
      id: data.id,
      orderType: data.orderType,
      srcDocDate: moment(data.srcDocDate),
      srcDocNo: data.srcDocNo,
      srcFiscalYear: data.srcFiscalYear,
      srcLocationCode: data.srcLocationCode,
      status: data.status,
      totalAvailableQuantity: data.totalAvailableQuantity,
      totalAvailableValue: data.totalAvailableValue,
      totalAvailableWeight: data.totalAvailableWeight,
      totalMeasuredQuantity: data.totalMeasuredQuantity,
      totalMeasuredValue: data.totalMeasuredValue,
      totalMeasuredWeight: data.totalMeasuredWeight,
      transferType: data.transferType,
      weightUnit: data.weightUnit
    };
    return issueConfirmResponse;
  }

  static SearchInvoiceFromJson(data: any): RequestInvoice[] {
    const requestInvoices: RequestInvoice[] = [];
    requestInvoices.push({
      currencyCode: data.currencyCode,
      destDocDate: moment(data.destDocDate),
      destDocNo: data.destDocNo,
      destLocationCode: data.destLocationCode,
      id: data.id,
      invoiceType: data.invoiceType,
      orderType: data.orderType,
      reason: data.reason,
      srcDocDate: moment(data.srcDocDate),
      srcDocNo: data.srcDocNo,
      srcFiscalYear: data.srcFiscalYear,
      srcLocationCode: data.srcLocationCode,
      status: data.status,
      totalQuantity: data.totalQuantity,
      totalValue: data.totalValue,
      totalWeight: data.totalWeight,
      weightUnit: data.weightUnit
    });
    return requestInvoices;
  }

  static IssueSTNCountFromJson(data: any): LoadIssueSTNCountsPayload {
    let pendingIssueBTQ_BTQ_STNCount = 0;
    let pendingIssueBTQ_FAC_STNCount = 0;
    let pendingIssueBTQ_MER_STNCount = 0;

    for (const issueSTNCount of data.results) {
      if (issueSTNCount.type === 'BTQ') {
        pendingIssueBTQ_BTQ_STNCount = issueSTNCount.count;
      }
      if (issueSTNCount.type === 'FAC') {
        pendingIssueBTQ_FAC_STNCount = issueSTNCount.count;
      }
      if (issueSTNCount.type === 'MER') {
        pendingIssueBTQ_MER_STNCount = issueSTNCount.count;
      }
    }
    return {
      pendingIssueBTQ_BTQ_STNCount,
      pendingIssueBTQ_FAC_STNCount,
      pendingIssueBTQ_MER_STNCount
    };
  }

  static IssueInvoiceCountFromJson(data: any): LoadIssueInvoicePayload {
    let pendingIssueBTQ_CFA_InvoiceCount = 0;

    for (const CFACount of data.results) {
      if (CFACount.type === 'BTQ_CFA') {
        pendingIssueBTQ_CFA_InvoiceCount = CFACount.count;
      }
    }
    return {
      pendingIssueBTQ_CFA_InvoiceCount
    };
  }

  static CourierDetailsFromJson(data: any): string[] {
    const courierNames: string[] = [];
    for (const courier of data.results) {
      courierNames.push(courier.courierName);
    }
    return courierNames;
  }

  static productCategoriesFromJson(data: any): Filter {
    return {
      id: data.productCategoryCode,
      description: data.description,
      selected: false
    };
  }

  static productGroupsFromJson(data: any): Filter {
    return {
      id: data.productGroupCode,
      description: data.description,
      selected: false
    };
  }
}
