import { Injectable } from '@angular/core';
import { ApiService, CourierDataService } from '@poss-web/core';
import { Observable, of } from 'rxjs';

import {
  getStockIssueByPaginationEndpointUrl,
  getIssueSTNCountEndpointUrl,
  getIssueInvoiceByPaginationEndpointUrl,
  getIssueInvoiceEndpointUrl,
  getStockIssueBySrcDocNoEndpointUrl,
  getIssueItemsByPaginationEndpointUrl,
  getStockIssueByReqDocNoEndpointUrl,
  getIssueConfirmEndpointUrl,
  getInvoiceReturnByIdEndpointUrl,
  getSearchIssueItemsByItemCodeUrl,
  getIssueUpdateAllItemEndpointUrl,
  getIssueUpdateItemEndpointUrl,
  getIssueItemsCountEndpointUrl,
  getCourierDetailsByLocCodeEndpointUrl,
  getProductGroupsUrl,
  getProductCategoriesUrl
} from '../../../endpoints.constants';
import { map, mergeMap } from 'rxjs/operators';
import {
  LoadIssueSearchedItemsCountPayload,
  CourierDetailsPayload
} from '../+state/stock-issue.actions';
import {
  StockRequestNote,
  RequestInvoice,
  IssueInventoryItem,
  IssueItemUpdate,
  RequestStockTransferNote
} from '../models/stock-issue-models';

import {
  LoadIssueItemsTotalCountSuccessPayload,
  LoadIssueSTNCountsPayload,
  LoadIssueInvoicePayload
} from '../+state/stock-issue.actions';

import { IssueItemHelper } from '../helpers/issueItem.helper';
import { StockIssueAdaptor } from '../adaptors/stock-issue-adaptor';
import { IssueItemAdaptor } from '../adaptors/issueItem.adaptor';
import { Filter } from '@poss-web/shared';

@Injectable()
export class StockIssueService {
  constructor(
    private apiService: ApiService,
    private courierService: CourierDataService
  ) {}
  getIssues(
    transferType: string,
    requsetType: string,
    pageIndex: number,
    pageSize: number
  ): Observable<StockRequestNote[]> {
    const url = getStockIssueByPaginationEndpointUrl(
      transferType,
      requsetType,
      pageIndex,
      pageSize
    );
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) => StockIssueAdaptor.stockRequestNotesFromJson(data))
      );
  }

  getCount(): Observable<LoadIssueSTNCountsPayload> {
    const IssueSTNCountUrl = getIssueSTNCountEndpointUrl();
    return this.apiService
      .get(IssueSTNCountUrl)
      .pipe(map((data: any) => StockIssueAdaptor.IssueSTNCountFromJson(data)));
  }

  getIssueInvoices(
    type: string,
    pageIndex: number,
    pageSize: number
  ): Observable<RequestInvoice[]> {
    const url = getIssueInvoiceByPaginationEndpointUrl(
      type,
      pageIndex,
      pageSize
    );
    return this.apiService
      .get(url)
      .pipe(map((data: any) => StockIssueAdaptor.requestInvoiceFromJson(data)));
  }

  getInvoiceCount(): Observable<LoadIssueInvoicePayload> {
    const url = getIssueInvoiceEndpointUrl();
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) => StockIssueAdaptor.IssueInvoiceCountFromJson(data))
      );
  }

  searchIssues(
    reqDocNo: number,
    type: string,
    requestType: string
  ): Observable<StockRequestNote[]> {
    const url = getStockIssueBySrcDocNoEndpointUrl(reqDocNo, type, requestType);
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) => StockIssueAdaptor.stockRequestNotesFromJson(data))
      );
  }
  searchInvoices(
    reqDocNo: number,
    type: string,
    status: string
  ): Observable<RequestInvoice[]> {
    const url = getInvoiceReturnByIdEndpointUrl(reqDocNo, type, status);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => StockIssueAdaptor.SearchInvoiceFromJson(data)));
  }

  getIssue(
    reqDocNo: number,
    transferType: string,
    requestType: string
  ): Observable<StockRequestNote> {
    const url = getStockIssueByReqDocNoEndpointUrl(
      reqDocNo,
      transferType,
      requestType
    );
    return this.apiService
      .get(url)
      .pipe(
        map((data: any) => StockIssueAdaptor.stockRequestNoteFromJson(data))
      );
  }

  getItems(
    id: number,
    itemCode: string,
    lotNumber: string,
    requestType: string,
    storeType: string,
    status: string,
    pageIndex: number,
    pageSize: number,
    sort?: Map<string, string>,
    filter?: { key: string; value: any[] }[]
  ): Observable<{ items: IssueInventoryItem[]; count: number }> {
    const url = getIssueItemsByPaginationEndpointUrl(
      id,
      itemCode,
      lotNumber,
      requestType,
      storeType,
      status,
      pageIndex,
      pageSize,
      sort,
      filter
    );
    return this.apiService
      .get(url)
      .pipe(map((data: any) => IssueItemHelper.getItems(data)));
  }
  getIssueItemsCount(
    storeType: string,
    id: number,
    type: string
  ): Observable<LoadIssueItemsTotalCountSuccessPayload> {
    const approvedItemsCountUrl = getIssueItemsCountEndpointUrl(
      storeType,
      id,
      0,
      1,
      type,
      'APPROVED'
    );
    const selecetedItemsCountUrl = getIssueItemsCountEndpointUrl(
      storeType,
      id,
      0,
      1,
      type,
      'SELECTED'
    );
    return this.apiService
      .get(approvedItemsCountUrl)
      .pipe(map((data: any) => data.totalElements))
      .pipe(
        mergeMap(approvedItemsTotalCount =>
          this.apiService.get(selecetedItemsCountUrl).pipe(
            map((data: any) => ({
              approvedItemsTotalCount: approvedItemsTotalCount,
              selectedItemsTotalCount: data.totalElements
            }))
          )
        )
      );
  }
  /**
   * Service to create the request return invoice
   */

  /**
   * service to handle the confirm issue
   * @param id :invoiceId
   * @param cfaDetails :cfaLocation with remarks
   */

  /**
   * service which searches the item by variant code
   * @param variantCode :variant code
   */

  confirmIssue(id: number, data: any, requestType: string) {
    const url = getIssueConfirmEndpointUrl(id, requestType);
    return this.apiService.patch(url, data).pipe(
      map((response: any) => {
        return StockIssueAdaptor.issueConfirmResponseFromJson(response);
      })
    );
  }

  // searchIssueItem(
  //   storeType: string,
  //   id: number,
  //   itemCode: string,
  //   lotNumber: string,
  //   pageIndex: number,
  //   pageSize: number,
  //   requestType: string,
  //   status: string
  // ): Observable<IssueInventoryItem[]> {
  //   const url = getSearchIssueItemsByItemCodeUrl(
  //     storeType,
  //     id,
  //     itemCode,
  //     lotNumber,
  //     pageIndex,
  //     pageSize,
  //     requestType,
  //     status
  //   );
  //   return this.apiService
  //     .get(url.path, url.params)
  //     .pipe(map((data: any) => IssueItemHelper.getItems(data.results)));
  // }

  // getSearchIssueItemsCount(
  //   storeType: string,
  //   id: number,
  //   itemCode: string,
  //   lotNumber: string,
  //   requestType: string,
  //   status: string
  // ): Observable<LoadIssueSearchedItemsCountPayload> {
  //   const url = getSearchIssueItemsByItemCodeUrl(
  //     storeType,
  //     id,
  //     itemCode,
  //     lotNumber,
  //     0,
  //     1,
  //     requestType,
  //     status
  //   );
  //   return this.apiService
  //     .get(url.path, url.params)
  //     .pipe(map((data: any) => ({ searchedItemsCount: data.totalElements })));
  // }

  updateItem(
    transferType: string,
    storeType: string,
    id: number,
    itemId: number,
    itemUpdate: IssueItemUpdate
  ): Observable<IssueInventoryItem> {
    const url = getIssueUpdateItemEndpointUrl(
      transferType,
      storeType,
      id,
      itemId
    );
    return this.apiService
      .patch(url, {
        inventoryId: itemUpdate.inventoryId,
        measuredQuantity: itemUpdate.measuredQuantity,
        measuredWeight: itemUpdate.measuredWeight
      })
      .pipe(map((data: any) => IssueItemAdaptor.fromJson(data)));
  }

  updateAllItem(
    requestType: string,
    storeType: string,
    id: number,
    itemIds: any,
    status: string
  ): Observable<any> {
    const url = getIssueUpdateAllItemEndpointUrl(requestType, storeType, id);
    return this.apiService.patch(url, { itemIds: itemIds, status: status });
  }

  loadProductCategories(): Observable<Filter[]> {
    const loadProductCategoriesUrl = getProductCategoriesUrl();
    return this.apiService
      .get(loadProductCategoriesUrl)
      .pipe(map((data: any) => IssueItemHelper.getProductCategories(data)));
  }

  loadProductGroups(): Observable<Filter[]> {
    const loadProductGroupsUrl = getProductGroupsUrl();
    return this.apiService
      .get(loadProductGroupsUrl)
      .pipe(map((data: any) => IssueItemHelper.getProductGroups(data)));
  }
}
