import { createSelector } from '@ngrx/store';
import { selectInventory } from '../../../inventory.state';
import {
  issueItemSelector,
  requestStockTransferNoteSelector
} from './stock-issue.entity';

const pendingIssuetoFactorySTN = createSelector(
  selectInventory,
  state => state.stockIssue.issueFactorySTN
);
const selectIssueToFactorySTN = createSelector(
  pendingIssuetoFactorySTN,
  requestStockTransferNoteSelector.selectAll
);

const pendingIssuetoBoutiqueSTN = createSelector(
  selectInventory,
  state => state.stockIssue.issueBoutiqueSTN
);
const selectIssueToBoutiqueSTN = createSelector(
  pendingIssuetoBoutiqueSTN,
  requestStockTransferNoteSelector.selectAll
);

const pendingIssuetoMerchantSTN = createSelector(
  selectInventory,
  state => state.stockIssue.issueMerchantSTN
);
const selectIssuetoMerchantSTN = createSelector(
  pendingIssuetoMerchantSTN,
  requestStockTransferNoteSelector.selectAll
);

const selectIsStockIssueListReset = createSelector(
  selectInventory,
  state => state.stockIssue.isStockIssueListReset
);

const selectError = createSelector(
  selectInventory,
  state => state.stockIssue.error
);

const searchIssueResults = createSelector(
  selectInventory,
  state => state.stockIssue.searchIssueResult
);
const selectSearchIssueResult = createSelector(
  searchIssueResults,
  requestStockTransferNoteSelector.selectAll
);

const selectIsLoadingIssueFactorySTN = createSelector(
  selectInventory,
  state => state.stockIssue.isLoadingIssueFactorySTN
);
const selectIsLoadingIssueBoutiqueSTN = createSelector(
  selectInventory,
  state => state.stockIssue.isLoadingIssueBoutiqueSTN
);
const selectIsLoadingIssueMerchantSTN = createSelector(
  selectInventory,
  state => state.stockIssue.isLoadingIssueMerchantSTN
);

const selectIsSearchingIssues = createSelector(
  selectInventory,
  state => state.stockIssue.isSearchingIssues
);
const selectHasSearchIssueResults = createSelector(
  selectInventory,
  state => state.stockIssue.hasSearchIssueResults
);

const selectSelectedIssue = createSelector(
  selectInventory,
  state => state.stockIssue.selectedIssue
);
const selectIsLoadingSelectedIssue = createSelector(
  selectInventory,
  state => state.stockIssue.isLoadingSelectedIssue
);

const approvedItems = createSelector(
  selectInventory,
  state => state.stockIssue.approvedItems
);
const selectIsApprovedItemsLoading = createSelector(
  selectInventory,
  state => state.stockIssue.isApprovedItemsLoading
);
const selectedItems = createSelector(
  selectInventory,
  state => state.stockIssue.selectedItems
);
const selectIsSelectedItemsLoading = createSelector(
  selectInventory,
  state => state.stockIssue.isSelectedItemsLoading
);
const selectApprovedItems = createSelector(
  approvedItems,
  issueItemSelector.selectAll
);
const selectSelectedItems = createSelector(
  selectedItems,
  issueItemSelector.selectAll
);

const selectApprovedItemsTotalCount = createSelector(
  selectInventory,
  state => state.stockIssue.approvedItemsTotalCount
);
const selectSelectedItemsTotalCount = createSelector(
  selectInventory,
  state => state.stockIssue.selectedItemsTotalCount
);
const selectIsItemsTotalCountLoading = createSelector(
  selectInventory,
  state => state.stockIssue.isItemsTotalCountLoading
);

const selectIsItemsTotalCountLoaded = createSelector(
  selectInventory,
  state => state.stockIssue.isItemsTotalCountLoaded
);

// ///////////////////////////////////////////////////
const issueItems = createSelector(
  selectInventory,
  state => state.stockIssue.issueItems
);
const selectIsIssueItemsLoading = createSelector(
  selectInventory,
  state => state.stockIssue.isIssueItemsLoading
);
const selectIssueItems = createSelector(
  issueItems,
  issueItemSelector.selectAll
);
const selectIssueItemsTotalCount = createSelector(
  selectInventory,
  state => state.stockIssue.issueItemsTotalCount
);
const selectIssueItemsTotalCountLoaded = createSelector(
  selectInventory,
  state => state.stockIssue.isItemsTotalCountLoaded
);

const selectConfirmIssue = createSelector(
  selectInventory,
  state => state.stockIssue.confirmIssue
);
const selectConfirmIssueStatus = createSelector(
  selectInventory,
  state => state.stockIssue.isItemIssued
);
const selectConfirmationSrcDocNo = createSelector(
  selectInventory,
  state => state.stockIssue.confirmIssue.srcDocNo
);

const searchedItems = createSelector(
  selectInventory,
  state => state.stockIssue.searchedItems
);
const selectSearchedItems = createSelector(
  searchedItems,
  issueItemSelector.selectAll
);
const selectIsSearchingItems = createSelector(
  selectInventory,
  state => state.stockIssue.isSearchingItems
);
const selectHasSearchedItems = createSelector(
  selectInventory,
  state => state.stockIssue.hasSearchedItems
);

const selectIsItemUpdateStatus = createSelector(
  selectInventory,
  state => state.stockIssue.isItemUpdated
);
const selectIsItemUpdating = createSelector(
  selectInventory,
  state => state.stockIssue.isitemUpdating
);
const selectSearchedIssueItemsCount = createSelector(
  selectInventory,
  state => state.stockIssue.searchedIssueItemsCount
);
const selectSearchedissueItemsCountLoaded = createSelector(
  selectInventory,
  state => state.stockIssue.isSearchIssueItemsCountLoaded
);

const selectIsUpdatingAll = createSelector(
  selectInventory,
  state => state.stockIssue.isUpdatingAllItems
);
const selectIsUpdatingAllSuccess = createSelector(
  selectInventory,
  state => state.stockIssue.isUpdatingAllItemsSuccess
);

// REQUEST COUNT
const selectPendingBTQ_FAC_STNCount = createSelector(
  selectInventory,
  state => state.stockIssue.pendingBTQ_FAC_STNCount
);

const selectPendingBTQ_BTQ_STNCount = createSelector(
  selectInventory,
  state => state.stockIssue.pendingBTQ_BTQ_STNCount
);
const selectPendingBTQ_MER_STNCount = createSelector(
  selectInventory,
  state => state.stockIssue.pendingBTQ_MER_STNCount
);

const selectIsLoadingIssueCount = createSelector(
  selectInventory,
  state => state.stockIssue.isLoadingIssueCount
);
const selectCourierDetails = createSelector(
  selectInventory,
  state => state.stockIssue.courierDetails
);
const selectIsLoadingCourierDetails = createSelector(
  selectInventory,
  state => state.stockIssue.isLoadingCourierDetails
);
const selectHasCourierDetails = createSelector(
  selectInventory,
  state => state.stockIssue.hasCourierDetails
);

const selectfilterDataApprovedProducts = createSelector(
  selectInventory,
  state => state.stockIssue.filterDataAllProducts
);

const selectfilterDataSelectedProducts = createSelector(
  selectInventory,
  state => state.stockIssue.filterDataSelectedProducts
);
const selectSortDataApprovedProducts = createSelector(
  selectInventory,
  state => state.stockIssue.sortDataAllProducts
);

const selectSortDataSelectedProducts = createSelector(
  selectInventory,
  state => state.stockIssue.sortDataSelectedProducts
);

const selectEmployeeCodes = createSelector(
  selectInventory,
  state => state.issueTEP.employeeCodes
);

const selectEmployeeDetails = createSelector(
  selectInventory,
  state => state.issueTEP.employeeDetails
);

const selectItemsCount = createSelector(
  selectInventory,
  state => state.stockIssue.itemsCount
);

const items = createSelector(
  selectInventory,
  state => state.stockIssue.items
);

const selectIsItemsLoading = createSelector(
  selectInventory,
  state => state.stockIssue.isItemsLoading
);

const selectIsItemsLoaded = createSelector(
  selectInventory,
  state => state.stockIssue.isItemsLoaded
);

const selectItems = createSelector(
  items,
  issueItemSelector.selectAll
);
const selectProductCategories = createSelector(
  selectInventory,
  state => state.stockIssue.productCategories
);

const selectProductGroups = createSelector(
  selectInventory,
  state => state.stockIssue.productGroups
);

export const StockIssueSelectors = {
  selectIssueToFactorySTN,
  selectIssueToBoutiqueSTN,
  selectIssuetoMerchantSTN,

  selectSearchIssueResult,
  selectHasSearchIssueResults,
  selectIsSearchingIssues,

  selectIsLoadingIssueFactorySTN,
  selectIsLoadingIssueBoutiqueSTN,
  selectIsLoadingIssueMerchantSTN,

  selectPendingBTQ_BTQ_STNCount,
  selectPendingBTQ_FAC_STNCount,
  selectPendingBTQ_MER_STNCount,
  selectIsLoadingIssueCount,

  selectSelectedIssue,
  selectIsLoadingSelectedIssue,
  selectIsSelectedItemsLoading,

  selectIsStockIssueListReset,

  selectError,

  selectApprovedItems,
  selectIsApprovedItemsLoading,
  selectApprovedItemsTotalCount,

  selectIsIssueItemsLoading,
  selectSelectedItems,
  selectSelectedItemsTotalCount,

  selectIsItemsTotalCountLoading,
  selectIsItemsTotalCountLoaded,

  // .
  selectIssueItems,
  // .
  selectIssueItemsTotalCount,
  selectIssueItemsTotalCountLoaded,

  selectIsSearchingItems,
  selectHasSearchedItems,
  selectSearchedItems,

  selectSearchedIssueItemsCount,
  selectSearchedissueItemsCountLoaded,

  selectIsItemUpdateStatus,
  selectIsItemUpdating,

  selectIsUpdatingAll,
  selectIsUpdatingAllSuccess,

  selectConfirmIssue,
  selectConfirmIssueStatus,

  selectConfirmationSrcDocNo,

  selectCourierDetails,
  selectHasCourierDetails,
  selectIsLoadingCourierDetails,

  selectEmployeeCodes,
  selectEmployeeDetails,

  selectProductCategories,
  selectProductGroups,

  selectfilterDataApprovedProducts,
  selectfilterDataSelectedProducts,
  selectSortDataApprovedProducts,
  selectSortDataSelectedProducts,
  selectItemsCount,
  selectIsItemsLoading,
  selectIsItemsLoaded,
  selectItems
};
