import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { IssueInventoryItem, StockRequestNote, RequestInvoice } from '../models/stock-issue-models';

export interface RequestStockTransferNoteEntity extends EntityState<StockRequestNote> { }
export const requestStockTransferNoteAdaptor = createEntityAdapter<StockRequestNote>({
  selectId: requestStockTransferNote => requestStockTransferNote.id
})
export const requestStockTransferNoteSelector = requestStockTransferNoteAdaptor.getSelectors();


export interface IssueItemEntity extends EntityState<IssueInventoryItem> { }
export const issueItemAdaptor = createEntityAdapter<IssueInventoryItem>({
  selectId: issueItem => issueItem.id
});
export const issueItemSelector = issueItemAdaptor.getSelectors();

