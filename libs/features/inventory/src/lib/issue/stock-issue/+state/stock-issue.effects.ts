import { Injectable } from '@angular/core';
import {
  NotificationService,
  CustomErrors,
  CourierDataService,
  StoreUserDetails,
  StoreUserDataService,
  StoreUser
} from '@poss-web/core';
import { DataPersistence } from '@nrwl/angular';
import { Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import * as StockIssueActions from './stock-issue.actions';
import { StockIssueActionTypes } from './stock-issue.actions';

import { map } from 'rxjs/operators';
import { StockIssueService } from '../services/stock-issue.service';
import {
  StockRequestNote,
  IssueInventoryItem,
  IssueConfirmResponse
} from '../models/stock-issue-models';
import { LoadIssueItemsTotalCountSuccessPayload } from './stock-issue.actions';
import { HttpErrorResponse } from '@angular/common/http';

import { CustomErrorAdaptor } from '@poss-web/core';
import { Filter } from '@poss-web/shared';

@Injectable()
export class StockIssueEffect {
  constructor(
    private stockIssueService: StockIssueService,
    private dataPersistence: DataPersistence<any>,
    private notificationService: NotificationService,
    private courierService: CourierDataService,
    private storeUserDataService: StoreUserDataService
  ) {}

  @Effect()
  loadPendingIssueToFactorySTN$: Observable<
    Action
  > = this.dataPersistence.fetch(
    StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_FACTORY_STN,
    {
      run: (action: StockIssueActions.LoadFactoryIssuePendingSTN) => {
        return this.stockIssueService
          .getIssues(
            action.payload.transferType,
            action.payload.requestType,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (requestStockTransferNotes: StockRequestNote[]) =>
                new StockIssueActions.LoadFactoryIssuePendingSTNSuccess(
                  requestStockTransferNotes
                )
            )
          );
      },
      onError: (
        action: StockIssueActions.LoadFactoryIssuePendingSTN,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new StockIssueActions.LoadFactoryIssuePendingSTNFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadPendingIssueToBoutiqueSTN$: Observable<
    Action
  > = this.dataPersistence.fetch(
    StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_BOUTIQUE_STN,
    {
      run: (action: StockIssueActions.LoadBoutiqueIssuePendingSTN) => {
        return this.stockIssueService
          .getIssues(
            action.payload.transferType,
            action.payload.requestType,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (requestStockTransferNotes: StockRequestNote[]) =>
                new StockIssueActions.LoadBoutiqueIssuePendingSTNSuccess(
                  requestStockTransferNotes
                )
            )
          );
      },
      onError: (
        action: StockIssueActions.LoadBoutiqueIssuePendingSTN,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new StockIssueActions.LoadBoutiqueIssuePendingSTNFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadPendingIssueToMerchantSTN$: Observable<
    Action
  > = this.dataPersistence.fetch(
    StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_MERCHANT_STN,
    {
      run: (action: StockIssueActions.LoadMerchantIssuePendingSTN) => {
        return this.stockIssueService
          .getIssues(
            action.payload.transferType,
            action.payload.requestType,
            action.payload.pageIndex,
            action.payload.pageSize
          )
          .pipe(
            map(
              (requestStockTransferNotes: StockRequestNote[]) =>
                new StockIssueActions.LoadMerchantIssuePendingSTNSuccess(
                  requestStockTransferNotes
                )
            )
          );
      },
      onError: (
        action: StockIssueActions.LoadMerchantIssuePendingSTN,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new StockIssueActions.LoadMerchantIssuePendingSTNFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  searchPendingIssues$ = this.dataPersistence.fetch(
    StockIssueActionTypes.SEARCH_PENDING_ISSUES,
    {
      run: (action: StockIssueActions.SearchPendingIssues) => {
        return this.stockIssueService
          .searchIssues(
            action.payload.reqDocNo,
            action.payload.type,
            action.payload.requestType
          )
          .pipe(
            map(
              (searchResult: StockRequestNote[]) =>
                new StockIssueActions.SearchPendingIssuesSuccess(searchResult)
            )
          );
      },
      onError: (
        action: StockIssueActions.SearchPendingIssues,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new StockIssueActions.SeachPendingIssuesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadSelectedIssue$ = this.dataPersistence.fetch(
    StockIssueActionTypes.LOAD_SELECTED_ISSUE,
    {
      run: (action: StockIssueActions.LoadSelectedIssue) => {
        return this.stockIssueService
          .getIssue(
            action.payload.reqDocNo,
            action.payload.transferType,
            action.payload.requestType
          )
          .pipe(
            map(
              (requestStockTransferNoteData: StockRequestNote) =>
                new StockIssueActions.LoadSelectedIssueSuccess(
                  requestStockTransferNoteData
                )
            )
          );
      },
      onError: (
        action: StockIssueActions.LoadSelectedIssue,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new StockIssueActions.LoadSelectedIssueFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect()
  loadItems$ = this.dataPersistence.fetch(StockIssueActionTypes.LOAD_ITEMS, {
    run: (action: StockIssueActions.LoadItems) => {
      return this.stockIssueService
        .getItems(
          action.payload.id,
          action.payload.itemCode,
          action.payload.lotNumber,
          action.payload.requestType,
          action.payload.storeType,
          action.payload.status,
          action.payload.pageIndex,
          action.payload.pageSize,
          action.payload.sort,
          action.payload.filter
        )
        .pipe(
          map(
            (data: { items: IssueInventoryItem[]; count: number }) =>
              new StockIssueActions.LoadItemsSuccess({ ...data })
          )
        );
    },
    onError: (
      action: StockIssueActions.LoadItems,
      error: HttpErrorResponse
    ) => {
      return new StockIssueActions.LoadItemsFailure(this.errorHandler(error));
    }
  });

  @Effect()
  LoadIssueItemsTotalCount$ = this.dataPersistence.fetch(
    StockIssueActionTypes.LOAD_ISSUE_ITEMS_COUNT,
    {
      run: (action: StockIssueActions.LoadIssueItemsTotalCount) => {
        return this.stockIssueService
          .getIssueItemsCount(
            action.payload.storeType,
            action.payload.id,
            action.payload.requestType
          )
          .pipe(
            map(
              (
                loadItemsTotalCountSuccessPayload: LoadIssueItemsTotalCountSuccessPayload
              ) =>
                new StockIssueActions.LoadIssueItemsTotalCountSuccess(
                  loadItemsTotalCountSuccessPayload
                )
            )
          );
      },
      onError: (
        action: StockIssueActions.LoadIssueItemsTotalCount,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new StockIssueActions.LoadIssueItemsTotalCountFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  confirmIssue$ = this.dataPersistence.pessimisticUpdate(
    StockIssueActionTypes.CONFIRM_ISSUE,
    {
      run: (action: StockIssueActions.ConfirmIssue) => {
        return this.stockIssueService
          .confirmIssue(
            action.payload.id,
            action.payload.data,
            action.payload.requestType
          )
          .pipe(
            map(
              (issueConfirmResponse: IssueConfirmResponse) =>
                new StockIssueActions.ConfirmIssueSuccess(issueConfirmResponse)
            )
          );
      },
      onError: (
        action: StockIssueActions.ConfirmIssue,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new StockIssueActions.ConfirmIssueFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect()
  updateItem$ = this.dataPersistence.pessimisticUpdate(
    StockIssueActionTypes.UPDATE_ITEM,
    {
      run: (action: StockIssueActions.UpdateItem) => {
        return this.stockIssueService
          .updateItem(
            action.payload.requestType,
            action.payload.storeType,
            action.payload.id,
            action.payload.itemId,
            action.payload.newUpdate
          )
          .pipe(
            map(
              (item: IssueInventoryItem) =>
                new StockIssueActions.UpdateItemSuccess(item)
            )
          );
      },
      onError: (
        action: StockIssueActions.UpdateItem,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new StockIssueActions.UpdateItemFailure({
          itemId: action.payload.itemId,
          actualDetails: action.payload.actualDetails,
          error: this.errorHandler(error)
        });
      }
    }
  );
  @Effect()
  updateAllItem$ = this.dataPersistence.pessimisticUpdate(
    StockIssueActionTypes.UPDATE_ALL_ITEM,
    {
      run: (action: StockIssueActions.UpdateAllItems) => {
        return this.stockIssueService
          .updateAllItem(
            action.payload.requestType,
            action.payload.storeType,
            action.payload.id,
            action.payload.itemId,
            action.payload.status
          )
          .pipe(
            map(
              (isSuccess: boolean) =>
                new StockIssueActions.UpdateAllItemsSuccess(isSuccess)
            )
          );
      },
      onError: (
        action: StockIssueActions.UpdateAllItems,
        error: HttpErrorResponse
      ) => {
        return new StockIssueActions.UpdateAllItemsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  // REQUEST COUNT
  @Effect()
  LoadIssueStockTransferNoteCount$ = this.dataPersistence.fetch(
    StockIssueActionTypes.LOAD_ISSUES_COUNT,
    {
      run: (action: StockIssueActions.LoadIssueSTNCount) => {
        return this.stockIssueService
          .getCount()
          .pipe(
            map(
              (data: StockIssueActions.LoadIssueSTNCountsPayload) =>
                new StockIssueActions.LoadIssueSTNCountSuccess(data)
            )
          );
      },
      onError: (
        action: StockIssueActions.LoadIssueSTNCount,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new StockIssueActions.LoadIssueSTNCountFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  LoadCourierDetails$ = this.dataPersistence.fetch(
    StockIssueActionTypes.LOAD_COURIER_DETAILS,
    {
      run: (action: StockIssueActions.LoadCourierDetails) => {
        return this.courierService
          .getCouriers(0, 50, true, action.payload)
          .pipe(
            map((data: any) => {
              const courierNames: string[] = [];
              for (const courier of data) {
                courierNames.push(courier.courierName);
              }
              return new StockIssueActions.LoadCourierDetailsSuccess(
                courierNames
              );
            })
          );
      },
      onError: (
        action: StockIssueActions.LoadCourierDetails,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new StockIssueActions.LoadCourierDetailsFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect()
  loadEmployeeCodes$: Observable<Action> = this.dataPersistence.fetch(
    StockIssueActionTypes.LOAD_EMPLOYEE_CODES,
    {
      run: (action: StockIssueActions.LoadEmployeeCodes) => {
        return this.storeUserDataService.getStoreUsers().pipe(
          map((data: StoreUser[]) => {
            const employeeCodes: string[] = [];
            for (const employee of data) {
              employeeCodes.push(employee.employeeCode);
            }
            return new StockIssueActions.LoadEmployeeCodesSuccess(
              employeeCodes
            );
          })
        );
      },
      onError: (
        action: StockIssueActions.LoadEmployeeCodes,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new StockIssueActions.LoadEmployeeCodesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadEmployeeDetails$: Observable<Action> = this.dataPersistence.fetch(
    StockIssueActionTypes.LOAD_EMPLOYEE_DETAILS,
    {
      run: (action: StockIssueActions.LoadEmployeeDetails) => {
        return this.storeUserDataService
          .getStoreUserDetails(action.payload)
          .pipe(
            map((data: StoreUserDetails) => {
              return new StockIssueActions.LoadEmployeeDetailsSuccess(data);
            })
          );
      },
      onError: (
        action: StockIssueActions.LoadEmployeeDetails,
        error: HttpErrorResponse
      ) => {
        const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
        this.notificationService.error(customError);
        return new StockIssueActions.LoadEmployeeDetailsFailure(
          this.errorHandler(error)
        );
      }
    }
  );
  @Effect() loadProductCategories = this.dataPersistence.fetch(
    StockIssueActionTypes.LOAD_PRODUCT_CATEGORIES,
    {
      run: (action: StockIssueActions.LoadProductCategories) => {
        return this.stockIssueService
          .loadProductCategories()
          .pipe(
            map(
              (data: Filter[]) =>
                new StockIssueActions.LoadProductCategoriesSuccess(data)
            )
          );
      },

      onError: (
        action: StockIssueActions.LoadProductCategories,
        error: HttpErrorResponse
      ) => {
        return new StockIssueActions.LoadProductCategoriesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect() loadProductGroups = this.dataPersistence.fetch(
    StockIssueActionTypes.LOAD_PROUDCT_GROUPS,
    {
      run: (action: StockIssueActions.LoadProductGroups) => {
        return this.stockIssueService
          .loadProductGroups()
          .pipe(
            map(
              (data: Filter[]) =>
                new StockIssueActions.LoadProductGroupsSuccess(data)
            )
          );
      },

      onError: (
        action: StockIssueActions.LoadProductGroups,
        error: HttpErrorResponse
      ) => {
        return new StockIssueActions.LoadProductGroupsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
}
