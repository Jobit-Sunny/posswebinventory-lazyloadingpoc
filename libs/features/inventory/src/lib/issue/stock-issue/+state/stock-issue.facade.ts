import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { StockIssueSelectors } from './stock-issue.selector';
import * as StockIssueActions from './stock-issue.actions';
import { State } from '../../../inventory.state';
import { of } from 'rxjs';
import { combineAll, map } from 'rxjs/operators';
import { Filter, Column } from '@poss-web/shared';

@Injectable()
export class StockIssueFacade {
  constructor(private store: Store<State>) {}

  private issueToFactorySTN$ = this.store.select(
    StockIssueSelectors.selectIssueToFactorySTN
  );

  private issueToBoutique$ = this.store.select(
    StockIssueSelectors.selectIssueToBoutiqueSTN
  );

  private issueToMerchant$ = this.store.select(
    StockIssueSelectors.selectIssuetoMerchantSTN
  );

  private selectedIssue$ = this.store.select(
    StockIssueSelectors.selectSelectedIssue
  );

  private error$ = this.store.select(StockIssueSelectors.selectError);

  private isLoadingIssueFactorySTN$ = this.store.select(
    StockIssueSelectors.selectIsLoadingIssueFactorySTN
  );
  private isLoadingIssueBoutiqueSTN$ = this.store.select(
    StockIssueSelectors.selectIsLoadingIssueBoutiqueSTN
  );
  private isLoadingIssueMerchantSTN$ = this.store.select(
    StockIssueSelectors.selectIsLoadingIssueMerchantSTN
  );

  private isSearchingIssues$ = this.store.select(
    StockIssueSelectors.selectIsSearchingIssues
  );
  private hasSearchSearchIssueResults$ = this.store.select(
    StockIssueSelectors.selectHasSearchIssueResults
  );

  private selectSearchIssueResults$ = this.store.select(
    StockIssueSelectors.selectSearchIssueResult
  );

  private approvedItemsTotalCount$ = this.store.select(
    StockIssueSelectors.selectApprovedItemsTotalCount
  );
  private selectedItemsTotalCount$ = this.store.select(
    StockIssueSelectors.selectSelectedItemsTotalCount
  );
  private isItemsTotalCountLoading$ = this.store.select(
    StockIssueSelectors.selectIsItemsTotalCountLoading
  );
  private isLoadingSelectedIssue$ = this.store.select(
    StockIssueSelectors.selectIsLoadingSelectedIssue
  );

  private approvedItems$ = this.store.select(
    StockIssueSelectors.selectApprovedItems
  );
  private isApprovedItemsLoading$ = this.store.select(
    StockIssueSelectors.selectIsApprovedItemsLoading
  );
  private selectedItems$ = this.store.select(
    StockIssueSelectors.selectSelectedItems
  );
  private isSelectedItemsLoading$ = this.store.select(
    StockIssueSelectors.selectIsSelectedItemsLoading
  );

  private issueItems$ = this.store.select(StockIssueSelectors.selectIssueItems);
  private isIssueItemsLoading$ = this.store.select(
    StockIssueSelectors.selectIsIssueItemsLoading
  );
  private issueItemsTotalCount$ = this.store.select(
    StockIssueSelectors.selectIssueItemsTotalCount
  );
  private issueItemsTotalCountLoaded$ = this.store.select(
    StockIssueSelectors.selectIssueItemsTotalCountLoaded
  );

  private issueConfirmStatus$ = this.store.select(
    StockIssueSelectors.selectConfirmIssueStatus
  );
  private issueConfirm$ = this.store.select(
    StockIssueSelectors.selectConfirmIssue
  );
  private confirmationDocNo$ = this.store.select(
    StockIssueSelectors.selectConfirmationSrcDocNo
  );

  // REQUEST COUNT
  private pendingBTQ_FAC_STNCount$ = this.store.select(
    StockIssueSelectors.selectPendingBTQ_FAC_STNCount
  );

  private pendingBTQ_BTQ_STNCount$ = this.store.select(
    StockIssueSelectors.selectPendingBTQ_BTQ_STNCount
  );
  private pendingBTQ_MER_STNCount$ = this.store.select(
    StockIssueSelectors.selectPendingBTQ_MER_STNCount
  );

  private isLoadingCount$ = this.store.select(
    StockIssueSelectors.selectIsLoadingIssueCount
  );
  private searchedItems$ = this.store.select(
    StockIssueSelectors.selectSearchedItems
  );
  private hasSearchedItems$ = this.store.select(
    StockIssueSelectors.selectHasSearchedItems
  );
  private isSearchingItems$ = this.store.select(
    StockIssueSelectors.selectIsSearchingItems
  );

  private selectIsUpdateAllItems$ = this.store.select(
    StockIssueSelectors.selectIsUpdatingAll
  );
  private selectIsUpdateAllItemsSuccess$ = this.store.select(
    StockIssueSelectors.selectIsUpdatingAllSuccess
  );

  private searchedIssueItemsTotalCount$ = this.store.select(
    StockIssueSelectors.selectSearchedIssueItemsCount
  );
  private searchIssueItemsTotalCountLoaded$ = this.store.select(
    StockIssueSelectors.selectSearchedissueItemsCountLoaded
  );

  private isItemUpdated$ = this.store.select(
    StockIssueSelectors.selectIsItemUpdateStatus
  );
  private isItemUpdating$ = this.store.select(
    StockIssueSelectors.selectIsItemUpdating
  );
  private courierDetails$ = this.store.select(
    StockIssueSelectors.selectCourierDetails
  );
  private isLoadingCourierDetails$ = this.store.select(
    StockIssueSelectors.selectIsLoadingCourierDetails
  );
  private hasCourierDetails$ = this.store.select(
    StockIssueSelectors.selectHasCourierDetails
  );

  private employeeCodes$ = this.store.select(
    StockIssueSelectors.selectEmployeeCodes
  );

  private employeeDetails$ = this.store.select(
    StockIssueSelectors.selectEmployeeDetails
  );

  private productCategories$ = this.store.select(
    StockIssueSelectors.selectProductCategories
  );

  private productGroups$ = this.store.select(
    StockIssueSelectors.selectProductGroups
  );

  private filterDataApprovedProducts$ = this.store.select(
    StockIssueSelectors.selectfilterDataApprovedProducts
  );

  private filterDataSelectedProducts$ = this.store.select(
    StockIssueSelectors.selectfilterDataSelectedProducts
  );
  private sortDataApprovedProducts$ = this.store.select(
    StockIssueSelectors.selectSortDataApprovedProducts
  );

  private sortDataSelectedProducts$ = this.store.select(
    StockIssueSelectors.selectSortDataSelectedProducts
  );

  private isItemsLoaded$ = this.store.select(
    StockIssueSelectors.selectIsItemsLoaded
  );
  private items$ = this.store.select(StockIssueSelectors.selectItems);
  private itemsLoading$ = this.store.select(
    StockIssueSelectors.selectIsItemsLoading
  );

  private itemsCount$ = this.store.select(StockIssueSelectors.selectItemsCount);

  getBTQ_FAC_PendingSTN() {
    return this.issueToFactorySTN$;
  }

  getBTQ_BTQ_PendingSTN() {
    return this.issueToBoutique$;
  }
  getBTQ_MER_PendingSTN() {
    return this.issueToMerchant$;
  }

  getSearchIssueResult() {
    return this.selectSearchIssueResults$;
  }

  getIsSearchingIssues() {
    return this.isSearchingIssues$;
  }
  getHasSearchIssueResults() {
    return this.hasSearchSearchIssueResults$;
  }

  getHasSearchInvoiceResults() {
    return this.hasSearchSearchIssueResults$;
  }

  getError() {
    return this.error$;
  }
  getisLoadingIssueToFactory() {
    return this.isLoadingIssueFactorySTN$;
  }
  getisLoadingIssueToBoutique() {
    return this.isLoadingIssueBoutiqueSTN$;
  }
  getIsLoadingIssueToMerchant() {
    return this.isLoadingIssueMerchantSTN$;
  }

  getIsLoadingSelectedIssue() {
    return this.isLoadingSelectedIssue$;
  }
  getSelectedIssue() {
    return this.selectedIssue$;
  }
  // ///////////

  getApprovedItemsTotalCount() {
    return this.approvedItemsTotalCount$;
  }

  getSelectedItemsTotalCount() {
    return this.selectedItemsTotalCount$;
  }

  getIsItemsTotalCountLoading() {
    return this.isItemsTotalCountLoading$;
  }

  getItemsTotalCountLoaded() {
    return this.issueItemsTotalCountLoaded$;
  }

  getApprovedItems() {
    return this.approvedItems$;
  }

  getSelectedItems() {
    return this.selectedItems$;
  }

  getIsApprovedItemsLoading() {
    return this.isApprovedItemsLoading$;
  }

  getIsSelectedItemsLoading() {
    return this.isSelectedItemsLoading$;
  }
  getSelectedIssueItems() {
    return this.issueItems$;
  }
  getIsIssueItemsLoading() {
    return this.isIssueItemsLoading$;
  }
  getIssueItemsTotalCount() {
    return this.issueItemsTotalCount$;
  }
  getIssueItemsTotalCountLoaded() {
    return this.issueItemsTotalCountLoaded$;
  }

  getIssueConfirmStatus() {
    return this.issueConfirmStatus$;
  }
  getIssueConfirm() {
    return this.issueConfirm$;
  }
  getConfirmationSrcDocNo() {
    return this.confirmationDocNo$;
  }
  getSearchedItems() {
    return this.searchedItems$;
  }
  getHasSearchedItems() {
    return this.hasSearchedItems$;
  }
  getIsSearchingItems() {
    return this.isSearchingItems$;
  }

  // REQUEST COUNT
  getPendingBTQ_BTQ_STNCount() {
    return this.pendingBTQ_BTQ_STNCount$;
  }
  getPendingBTQ_MER_STNCount() {
    return this.pendingBTQ_MER_STNCount$;
  }
  getPendingBTQ_FAC_STNCount() {
    return this.pendingBTQ_FAC_STNCount$;
  }

  getIsLoadingCount() {
    return this.isLoadingCount$;
  }

  getPendingIssueSTNCount() {
    const source = of(
      this.pendingBTQ_BTQ_STNCount$,
      this.pendingBTQ_FAC_STNCount$,
      this.pendingBTQ_MER_STNCount$
    );
    return source.pipe(
      combineAll(),
      map(numbers => numbers.reduce((sum, n) => sum + n, 0))
    );
  }

  getSearchedIssueItemsCount() {
    return this.searchedIssueItemsTotalCount$;
  }
  getSearchedIssueItemsCountLoaded() {
    return this.searchIssueItemsTotalCountLoaded$;
  }
  getItemUpdateStatus() {
    return this.isItemUpdated$;
  }
  getIsItemUpdating() {
    return this.isItemUpdating$;
  }

  getIsUpdateAllItems() {
    return this.selectIsUpdateAllItems$;
  }
  getselectIsUpdateAllItemsSuccess() {
    return this.selectIsUpdateAllItemsSuccess$;
  }
  getCourierDetails() {
    return this.courierDetails$;
  }
  getIsLoadingCourierDetails() {
    return this.isLoadingCourierDetails$;
  }
  getHasCourierDetails() {
    return this.hasCourierDetails$;
  }

  loadEmployeeCodes() {
    this.store.dispatch(new StockIssueActions.LoadEmployeeCodes());
  }

  getEmployeeCodes() {
    return this.employeeCodes$;
  }

  loadEmployeeDetails(employeeCode: string) {
    this.store.dispatch(
      new StockIssueActions.LoadEmployeeDetails(employeeCode)
    );
  }

  getEmployeeDetails() {
    return this.employeeDetails$;
  }

  loadProductCategories() {
    this.store.dispatch(new StockIssueActions.LoadProductCategories());
  }

  getProductCategories() {
    return this.productCategories$;
  }

  loadProductGroups() {
    this.store.dispatch(new StockIssueActions.LoadProductGroups());
  }

  getProductGroups() {
    return this.productGroups$;
  }

  setIssueAppovedProductsFilter(filterData: { [key: string]: Filter[] }) {
    this.store.dispatch(
      new StockIssueActions.SetFilterDataApprovedProducts(filterData)
    );
  }
  setIssueSelectedProductsFilter(filterData: { [key: string]: Filter[] }) {
    this.store.dispatch(
      new StockIssueActions.SetFilterDataSelectedProducts(filterData)
    );
  }
  getfilterDataApprovedProducts() {
    return this.filterDataApprovedProducts$;
  }
  getfilterDataSelectedProducts() {
    return this.filterDataSelectedProducts$;
  }
  setIssueApprovedProductsSort(sortData: Column[]) {
    this.store.dispatch(
      new StockIssueActions.SetSortDataApprovedProducts(sortData)
    );
  }
  setIssueSelectedProductsSort(sortData: Column[]) {
    this.store.dispatch(
      new StockIssueActions.SetSortDataSelectedProducts(sortData)
    );
  }

  getSortDataApprovedProducts() {
    return this.sortDataApprovedProducts$;
  }
  getSortDataSelectedProducts() {
    return this.sortDataSelectedProducts$;
  }

  getItems() {
    return this.items$;
  }

  getIsItemsLoading() {
    return this.itemsLoading$;
  }
  getItemsCount() {
    return this.itemsCount$;
  }
  getIsItemsLoaded() {
    return this.isItemsLoaded$;
  }
  clearSortAndFilter() {
    this.store.dispatch(new StockIssueActions.ClearSortAndFilter());
  }

  loadIssueToFactorySTN(
    loadIssueToFactorySTNPayload: StockIssueActions.LoadPendingIssuePayload
  ) {
    this.store.dispatch(
      new StockIssueActions.LoadFactoryIssuePendingSTN(
        loadIssueToFactorySTNPayload
      )
    );
  }

  loadIssueToBoutiqueSTN(
    loadIssueToBoutiqueSTNPayload: StockIssueActions.LoadPendingIssuePayload
  ) {
    this.store.dispatch(
      new StockIssueActions.LoadBoutiqueIssuePendingSTN(
        loadIssueToBoutiqueSTNPayload
      )
    );
  }
  loadIssueToMerchantSTN(
    loadIssueToMerchantPayload: StockIssueActions.LoadPendingIssuePayload
  ) {
    this.store.dispatch(
      new StockIssueActions.LoadMerchantIssuePendingSTN(
        loadIssueToMerchantPayload
      )
    );
  }

  resetStockIssueList() {
    this.store.dispatch(new StockIssueActions.ResetStockIssueList());
  }

  searchPendingIssues(
    searchPendingIssuesPayload: StockIssueActions.SearchPendingPayload
  ) {
    this.store.dispatch(
      new StockIssueActions.SearchPendingIssues(searchPendingIssuesPayload)
    );
  }

  searchClear() {
    this.store.dispatch(new StockIssueActions.SearchClear());
  }

  loadSelectedIssue(
    loadSelectedIssuePayload: StockIssueActions.LoadSelectedPayload
  ) {
    this.store.dispatch(
      new StockIssueActions.LoadSelectedIssue(loadSelectedIssuePayload)
    );
  }
  loadItems(loadItemsPayload: StockIssueActions.LoadIssueItemPayload) {
    this.store.dispatch(new StockIssueActions.LoadItems(loadItemsPayload));
  }
  loadApprovedItems(loadItemsPayload: StockIssueActions.LoadIssueItemPayload) {
    this.store.dispatch(
      new StockIssueActions.LoadApprovedItems(loadItemsPayload)
    );
  }

  loadSelectedItems(loadItemsPayload: StockIssueActions.LoadIssueItemPayload) {
    this.store.dispatch(
      new StockIssueActions.LoadSelectedItems(loadItemsPayload)
    );
  }

  // loadIssueItems(
  //   loadIssueItemsPayload: StockIssueActions.LoadIssueItemPayload
  // ) {
  //   this.store.dispatch(
  //     new StockIssueActions.LoadIssueItems(loadIssueItemsPayload)
  //   );
  // }
  loadItemsTotalCount(
    loadIssueItemsTotalCountPayload: StockIssueActions.LoadIssueItemsTotalCountPayload
  ) {
    this.store.dispatch(
      new StockIssueActions.LoadIssueItemsTotalCount(
        loadIssueItemsTotalCountPayload
      )
    );
  }

  confirmIssue(issueConfirmPayload: StockIssueActions.ConfirmIssuePayload) {
    this.store.dispatch(
      new StockIssueActions.ConfirmIssue(issueConfirmPayload)
    );
  }

  searchItems(
    searchIssueItemsPayload: StockIssueActions.SearchIssueItemsPayload
  ) {
    this.store.dispatch(
      new StockIssueActions.SearchIssueItems(searchIssueItemsPayload)
    );
  }

  // REQUESR COUNT
  LoadIssueSTNCount() {
    this.store.dispatch(new StockIssueActions.LoadIssueSTNCount());
  }

  clearSearchItems() {
    this.store.dispatch(new StockIssueActions.ClearSearchItems());
  }
  loadSearchedIssueItemCount(
    loadSearchedIssueCountPayload: StockIssueActions.LoadSearchIssueItemsTotalCountPayload
  ) {
    this.store.dispatch(
      new StockIssueActions.LoadSearchIssueItemsTotalCount(
        loadSearchedIssueCountPayload
      )
    );
  }

  updateItem(updateItemPayload: StockIssueActions.UpdateItemPayload) {
    this.store.dispatch(new StockIssueActions.UpdateItem(updateItemPayload));
  }

  UpdateAllItems(
    updateAllItemsPayload: StockIssueActions.UpdateAllItemPayload
  ) {
    this.store.dispatch(
      new StockIssueActions.UpdateAllItems(updateAllItemsPayload)
    );
  }
  loadCourierDetails(locationCode: string) {
    this.store.dispatch(new StockIssueActions.LoadCourierDetails(locationCode));
  }
  resetError() {
    this.store.dispatch(new StockIssueActions.ResetError());
  }
}
