import { Action } from '@ngrx/store';
import {
  StockRequestNote,
  IssueInventoryItem,
  IssueConfirmResponse,
  IssueItemUpdate
} from '../models/stock-issue-models';
import { CustomErrors, StoreUserDetails } from '@poss-web/core';
import { Filter, Column } from '@poss-web/shared';

export interface LoadPendingIssuePayload {
  transferType: string;
  requestType: string;
  pageIndex: number;
  pageSize: number;
}
export interface LoadPendingCFAIssuePayload {
  type: string;
  pageIndex: number;
  pageSize: number;
}
export interface LoadSelectedPayload {
  reqDocNo: number;
  transferType: string;
  requestType: string;
}

export interface SearchPendingPayload {
  reqDocNo: number;
  type: string;
  requestType: string;
}
export interface SearchPendingInvoicePayload {
  reqDocNo: number;
  status: string;
  type: string;
}
//////////////////////////////////////
export interface LoadIssueItemPayload {
  id: number;
  itemCode?: string;
  lotNumber?: string;
  requestType: string;
  storeType: string;
  status: string;
  pageIndex?: number;
  pageSize?: number;
  sort?: Map<string, string>;
  filter?: { key: string; value: any[] }[];
}
export interface SearchIssueItemsPayload {
  id: number;
  itemCode?: string;
  lotNumber?: string;
  requestType: string;
  storeType: string;
  status: string;
  pageIndex?: number;
  pageSize?: number;
  sort?: Map<string, string>;
  filter?: Map<string, string>;
}
export interface LoadIssueItemsTotalCountPayload {
  id: number;
  requestType: string;
  storeType: string;
}
export interface LoadSearchIssueItemsTotalCountPayload {
  id: number;
  itemCode?: string;
  lotNumber?: string;
  requestType: string;
  storeType: string;
  status: string;
  sort?: Map<string, string>;
  filter?: Map<string, string>;
}
export interface LoadIssueItemsTotalCountSuccessPayload {
  approvedItemsTotalCount: number;
  selectedItemsTotalCount: number;
  // searchedItemsCount: number;
}
export interface LoadIssueSearchedItemsCountPayload {
  searchedItemsCount: number;
}

export interface FormStatusPayload {
  status: boolean;
}

export interface ConfirmIssuePayload {
  requestType: string;
  id: number;
  data: {
    carrierDetails: {
      data: any;
      type: any;
    };
    remarks: string;
  };
}

export interface UpdateItemPayload {
  requestType: string;
  storeType: string;
  id: number;
  itemId: any;
  newUpdate: IssueItemUpdate;
  actualDetails: IssueItemUpdate;
}
export interface UpdateAllItemPayload {
  requestType: string;
  storeType: string;
  id: number;
  itemId: any;
  status: string;
}
export interface UpdateItemFailurePayload {
  itemId: number;
  actualDetails: IssueItemUpdate;
  error: CustomErrors;
}
export interface CheckStatusPayload {
  id: number;
  status: boolean;
}
// REQUEST COUNT
export interface LoadIssueSTNCountsPayload {
  pendingIssueBTQ_BTQ_STNCount: number;
  pendingIssueBTQ_FAC_STNCount: number;
  pendingIssueBTQ_MER_STNCount: number;
}
export interface LoadIssueInvoicePayload {
  pendingIssueBTQ_CFA_InvoiceCount: number;
}
export interface CourierDetailsPayload {
  courierNames: string[];
}

export enum StockIssueActionTypes {
  LOAD_PENDING_ISSUE_TO_FACTORY_STN = '[ Stock-Issue ] Load pending issue To Factory STN List',
  LOAD_PENDING_ISSUE_TO_FACTORY_STN_SUCCESS = '[ Stock-Issue ] Load pending issue To Factory STN List Success',
  LOAD_PENDING_ISSUE_TO_FACTORY_STN_FAILURE = '[ Stock-Issue ] Load pending issue To Factory STN List Failure',

  LOAD_PENDING_ISSUE_TO_BOUTIQUE_STN = '[ Stock-Issue ] Load pending issue  STN List to Boutique',
  LOAD_PENDING_ISSUE_TO_BOUTIQUE_STN_SUCCESS = '[ Stock-Issue ] Load pending issue  STN List to Boutique Success',
  LOAD_PENDING_ISSUE_TO_BOUTIQUE_STN_FAILURE = '[ Stock-Issue ] Load pending issue  STN List to Boutique Failure',

  LOAD_PENDING_ISSUE_TO_MERCHANT_STN = '[ Stock-Issue ] Load pending issue  STN List to Merchant',
  LOAD_PENDING_ISSUE_TO_MERCHANT_STN_SUCCESS = '[ Stock-Issue ] Load pending issue  STN List to Merchant Success',
  LOAD_PENDING_ISSUE_TO_MERCHANT_STN_FAILURE = '[ Stock-Issue ] Load pending issue  STN List to Merchant Failure',

  LOAD_ISSUES_COUNT = '[Stock-issue] Load IssueSTNCount',
  LOAD_ISSUES_COUNT_SUCCESS = '[Stock-issue] Load IssueSTNCount Success',
  LOAD_ISSUES_COUNT_FAILURE = '[Stock-issue] Load IssueSTNCount Failure',

  SEARCH_PENDING_ISSUES = '[Stock-issue] Search Pending Issues',
  SEARCH_PENDING_ISSUES_SUCCESS = '[Stock-issue] Search Pending Issues Success',
  SEARCH_PENDING_ISSUES_FAILURE = '[Stock-issue] Search Pending Issues Failure',

  SEARCH_CLEAR = '[Stock-issue] search-clear',

  RESET_STOCK_ISSUE_LIST = '[Stock-issue] Reset Stock Issue List',

  LOAD_SELECTED_ISSUE = '[Stock-issue-details] Load Selected Issue',
  LOAD_SELECTED_ISSUE_SUCCESS = '[Stock-issue-details] Load Selected Issue Success',
  LOAD_SELECTED_ISSUE_FAILURE = '[Stock-issue-details] Load Selected Issue Failure',

  //  LOAD_SELECTED_INVOICE
  LOAD_ITEMS = '[Stocl-issue-details] Load Items',
  LOAD_ITEMS_SUCCESS = '[Stocl-issue-details] Load Items Succedd',
  LOAD_ITEMS_FAILURE = '[Stocl-issue-details] Load Items Failure',

  LOAD_APPROVED_ITEMS = '[ Stock-issue-details ] Load approved Items ',
  LOAD_APPROVED_ITEMS_SUCCESS = '[ Stock-issue-details ] Load approved Items Success',
  LOAD_APPROVED_ITEMS_FAILURE = '[ Stock-issue-details ] Load approved Items Failure',

  LOAD_SELECTED_ITEMS = '[ Stock-issue-details ] Load selected Items ',
  LOAD_SELECTED_ITEMS_SUCCESS = '[ Stock-issue-details ] Load selected  Items Success ',
  LOAD_SELECTED_ITEMS_FAILURE = '[ Stock-issue-details ] Load selected Items Failure ',

  LOAD_ISSUE_ITEMS = '[Stock-issue-Details] Load Issue Items',
  LOAD_ISSUE_ITEMS_SUCCESS = '[Stock-issue-Details] Load Issue Items Success',
  LOAD_ISSUE_ITEMS_FAILURE = '[Stock-issue-Details] Load Issue Items Failure',

  LOAD_ISSUE_ITEMS_COUNT = '[Stock-issue-Details] Load Issue Items Count',
  LOAD_ISSUE_ITEMS_COUNT_SUCCESS = '[Stock-issue-Details] Load Issue Items Count Success',
  LOAD_ISSUE_ITEMS_COUNT_FAILURE = '[Stock-issue-Details] Load Issue Items Count Failure',

  CONFIRM_ISSUE = '[Stock-issue Details] Confirm Issue',
  CONFIRM_ISSUE_SUCCESS = '[Stock-issue Details] Confirm Issue Success',
  CONFIRM_ISSUE_FAILURE = '[Stock-issue Details] Confirm Issue Failure',

  // REMOVE_SELECTED = '[Stock-issue Details] Remove selected on  confirmation'

  LOAD_SEARCH_ISSUE_ITEMS_COUNT = '[Stock-issue-Details] Load SearchIssue Items Count',
  LOAD_SEARCH_ISSUE_ITEMS_COUNT_SUCCESS = '[Stock-issue-Details] Load SearchIssue Items Count Success',
  LOAD_SEARCH_ISSUE_ITEMS_COUNT_FAILURE = '[Stock-issue-Details] Load Search Issue Items Count Failure',

  CLEAR_ISSUE_SEARCH_ITEMS = '[Stock-issue Details] Clear Search Items',

  // Update Item
  UPDATE_ITEM = '[Stock-issue Details] Update Item',
  UPDATE_ITEM_SUCCESS = '[Stock-issue Details] Update Item Success',
  UPDATE_ITEM_FAILURE = '[Stock-issue Details] Update Item Failure',

  UPDATE_ALL_ITEM = '[Stock-issue Details] Update All Item',
  UPDATE_ALL_ITEM_SUCCESS = '[Stock-issue Details] Update All Item Success',
  UPDATE_ALL_ITEM_FAILURE = '[Stock-issue Details] Update All Item Failure',

  SEARCH_ISSUE_ITEMS = '[Stock-issue Details] Search Items',
  SEARCH_ISSUE_ITEMS_SUCCESS = '[Stock-issue Details] Search Items Success',
  SEARCH_ISSUE_ITEMS_FAILURE = '[Stock-issue Details] Search Items Failure',

  LOAD_COURIER_DETAILS = '[Stock-issue Details] Load Courier Details',
  LOAD_COURIER_DETAILS_SUCCESS = '[Stock-issue Details] Load Courier Details Success',
  LOAD_COURIER_DETAILS_FAILURE = '[Stock-issue Details] Load Courier Details Failure',

  LOAD_PRODUCT_CATEGORIES = '[Stock-issue Details] Load product categories',
  LOAD_PRODUCT_CATEGORIES_SUCCESS = '[Stock-issue Details] Load product categories Success',
  LOAD_PRODUCT_CATEGORIES_FAILURE = '[Stock-issue Details] Load product categories Failure',

  LOAD_PROUDCT_GROUPS = '[Stock-issue Details] Load product groups',
  LOAD_PROUDCT_GROUPS_SUCCESS = '[Stock-issue Details] Load product groups Success',
  LOAD_PROUDCT_GROUPS_FAILURE = '[Stock-issue Details] Load product groups Failure',

  LOAD_EMPLOYEE_CODES = '[Stock-issue Details] Load Employee Codes',
  LOAD_EMPLOYEE_CODES_SUCCESS = '[Stock-issue Details] Load Employee Codes Success',
  LOAD_EMPLOYEE_CODES_FAILURE = '[Stock-issue Details] Load Employee Codes Failure',

  LOAD_EMPLOYEE_DETAILS = '[Stock-issue Details] Load Employee Details',
  LOAD_EMPLOYEE_DETAILS_SUCCESS = '[Stock-issue Details] Load Employee Details Success',
  LOAD_EMPLOYEE_DETAILS_FAILURE = '[Stock-issue Details] Load Employee Details Failure',

  SET_FILTER_DATA_APPROVED_PRODUCTS = '[Stock-issue Details] Set Filter Data All Products',
  SET_FILTER_DATA_SELECTED_PRODUCTS = '[Stock-issue Details] Set Filter Data Selected Products',
  SET_SORT_DATA_APPROVED_PRODUCTS = '[Stock-issue Details] Set Sort Data All Products',
  SET_SORT_DATA_SELECTED_PRODUCTS = '[Stock-issue Details] Set Sort Data Selected Products',

  CLEAR_SORT_AND_FILTER = '[Stock-issue Details] Clear Sort and Filter',

  RESET_ERROR = '[stock-issue] Reset Error'
}

export class LoadFactoryIssuePendingSTN implements Action {
  readonly type = StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_FACTORY_STN;
  constructor(public payload: LoadPendingIssuePayload) {}
}
export class LoadFactoryIssuePendingSTNSuccess implements Action {
  readonly type =
    StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_FACTORY_STN_SUCCESS;
  constructor(public payload: StockRequestNote[]) {}
}
export class LoadFactoryIssuePendingSTNFailure implements Action {
  readonly type =
    StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_FACTORY_STN_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadBoutiqueIssuePendingSTN implements Action {
  readonly type = StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_BOUTIQUE_STN;
  constructor(public payload: LoadPendingIssuePayload) {}
}
export class LoadBoutiqueIssuePendingSTNSuccess implements Action {
  readonly type =
    StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_BOUTIQUE_STN_SUCCESS;
  constructor(public payload: StockRequestNote[]) {}
}
export class LoadBoutiqueIssuePendingSTNFailure implements Action {
  readonly type =
    StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_BOUTIQUE_STN_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadMerchantIssuePendingSTN implements Action {
  readonly type = StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_MERCHANT_STN;
  constructor(public payload: LoadPendingIssuePayload) {}
}
export class LoadMerchantIssuePendingSTNSuccess implements Action {
  readonly type =
    StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_MERCHANT_STN_SUCCESS;
  constructor(public payload: StockRequestNote[]) {}
}
export class LoadMerchantIssuePendingSTNFailure implements Action {
  readonly type =
    StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_MERCHANT_STN_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadIssueSTNCount implements Action {
  readonly type = StockIssueActionTypes.LOAD_ISSUES_COUNT;
}
export class LoadIssueSTNCountSuccess implements Action {
  readonly type = StockIssueActionTypes.LOAD_ISSUES_COUNT_SUCCESS;
  constructor(public payload: LoadIssueSTNCountsPayload) {}
}
export class LoadIssueSTNCountFailure implements Action {
  readonly type = StockIssueActionTypes.LOAD_ISSUES_COUNT_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class SearchPendingIssues implements Action {
  readonly type = StockIssueActionTypes.SEARCH_PENDING_ISSUES;
  constructor(public payload: SearchPendingPayload) {}
}
export class SearchPendingIssuesSuccess implements Action {
  readonly type = StockIssueActionTypes.SEARCH_PENDING_ISSUES_SUCCESS;
  constructor(public payload: StockRequestNote[]) {}
}
export class SeachPendingIssuesFailure implements Action {
  readonly type = StockIssueActionTypes.SEARCH_PENDING_ISSUES_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class SearchClear implements Action {
  readonly type = StockIssueActionTypes.SEARCH_CLEAR;
}

export class ResetStockIssueList implements Action {
  readonly type = StockIssueActionTypes.RESET_STOCK_ISSUE_LIST;
}

export class LoadSelectedIssue implements Action {
  readonly type = StockIssueActionTypes.LOAD_SELECTED_ISSUE;
  constructor(public payload: LoadSelectedPayload) {}
}
export class LoadSelectedIssueSuccess implements Action {
  readonly type = StockIssueActionTypes.LOAD_SELECTED_ISSUE_SUCCESS;
  constructor(public payload: StockRequestNote) {}
}
export class LoadSelectedIssueFailure implements Action {
  readonly type = StockIssueActionTypes.LOAD_SELECTED_ISSUE_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadItems implements Action {
  readonly type = StockIssueActionTypes.LOAD_ITEMS;
  constructor(public payload: LoadIssueItemPayload) {}
}
export class LoadItemsSuccess implements Action {
  readonly type = StockIssueActionTypes.LOAD_ITEMS_SUCCESS;
  constructor(public payload: { items: IssueInventoryItem[]; count: number }) {}
}
export class LoadItemsFailure implements Action {
  readonly type = StockIssueActionTypes.LOAD_ITEMS_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export class LoadApprovedItems implements Action {
  readonly type = StockIssueActionTypes.LOAD_APPROVED_ITEMS;
  constructor(public payload: LoadIssueItemPayload) {}
}
export class LoadApprovedItemsSuccess implements Action {
  readonly type = StockIssueActionTypes.LOAD_APPROVED_ITEMS_SUCCESS;
  constructor(public payload: IssueInventoryItem[]) {}
}
export class LoadApprovedItemsFailure implements Action {
  readonly type = StockIssueActionTypes.LOAD_APPROVED_ITEMS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadSelectedItems implements Action {
  readonly type = StockIssueActionTypes.LOAD_SELECTED_ITEMS;
  constructor(public payload: LoadIssueItemPayload) {}
}
export class LoadSelectedItemsSuccess implements Action {
  readonly type = StockIssueActionTypes.LOAD_SELECTED_ITEMS_SUCCESS;
  constructor(public payload: IssueInventoryItem[]) {}
}
export class LoadSelectedItemsFailure implements Action {
  readonly type = StockIssueActionTypes.LOAD_SELECTED_ITEMS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

// export class LoadIssueItems implements Action {
//   readonly type = StockIssueActionTypes.LOAD_ISSUE_ITEMS;
//   constructor(public payload: LoadIssueItemPayload) {}
// }
// export class LoadIssueItemsSuccess implements Action {
//   readonly type = StockIssueActionTypes.LOAD_ISSUE_ITEMS_SUCCESS;
//   constructor(public payload: IssueInventoryItem[]) {}
// }
// export class LoadIssueItemsFailure implements Action {
//   readonly type = StockIssueActionTypes.LOAD_ISSUE_ITEMS_FAILURE;
//   constructor(public payload: CustomErrors) {}
// }
export class LoadIssueItemsTotalCount implements Action {
  readonly type = StockIssueActionTypes.LOAD_ISSUE_ITEMS_COUNT;
  constructor(public payload: LoadIssueItemsTotalCountPayload) {}
}
export class LoadIssueItemsTotalCountSuccess implements Action {
  readonly type = StockIssueActionTypes.LOAD_ISSUE_ITEMS_COUNT_SUCCESS;
  constructor(public payload: LoadIssueItemsTotalCountSuccessPayload) {}
}
export class LoadIssueItemsTotalCountFailure implements Action {
  readonly type = StockIssueActionTypes.LOAD_ISSUE_ITEMS_COUNT_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class SearchIssueItems implements Action {
  readonly type = StockIssueActionTypes.SEARCH_ISSUE_ITEMS;
  constructor(public payload: SearchIssueItemsPayload) {}
}
export class SearchIssueItemsSuccess implements Action {
  readonly type = StockIssueActionTypes.SEARCH_ISSUE_ITEMS_SUCCESS;
  constructor(public payload: IssueInventoryItem[]) {}
}
export class SearchIssueItemsFailure implements Action {
  readonly type = StockIssueActionTypes.SEARCH_ISSUE_ITEMS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadSearchIssueItemsTotalCount implements Action {
  readonly type = StockIssueActionTypes.LOAD_SEARCH_ISSUE_ITEMS_COUNT;
  constructor(public payload: LoadSearchIssueItemsTotalCountPayload) {}
}
export class LoadSearchIssueItemsTotalCountSuccess implements Action {
  readonly type = StockIssueActionTypes.LOAD_SEARCH_ISSUE_ITEMS_COUNT_SUCCESS;
  constructor(public payload: LoadIssueSearchedItemsCountPayload) {}
}
export class LoadSearchIssueItemsTotalCountFailure implements Action {
  readonly type = StockIssueActionTypes.LOAD_SEARCH_ISSUE_ITEMS_COUNT_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class ClearSearchItems implements Action {
  readonly type = StockIssueActionTypes.CLEAR_ISSUE_SEARCH_ITEMS;
}

export class UpdateAllItems implements Action {
  readonly type = StockIssueActionTypes.UPDATE_ALL_ITEM;
  constructor(public payload: UpdateAllItemPayload) {}
}
export class UpdateAllItemsSuccess implements Action {
  readonly type = StockIssueActionTypes.UPDATE_ALL_ITEM_SUCCESS;
  constructor(public payload: boolean) {}
}
export class UpdateAllItemsFailure implements Action {
  readonly type = StockIssueActionTypes.UPDATE_ALL_ITEM_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class UpdateItem implements Action {
  readonly type = StockIssueActionTypes.UPDATE_ITEM;
  constructor(public payload: UpdateItemPayload) {}
}
export class UpdateItemSuccess implements Action {
  readonly type = StockIssueActionTypes.UPDATE_ITEM_SUCCESS;
  constructor(public payload: IssueInventoryItem) {}
}
export class UpdateItemFailure implements Action {
  readonly type = StockIssueActionTypes.UPDATE_ITEM_FAILURE;
  constructor(public payload: UpdateItemFailurePayload) {}
}

export class ConfirmIssue implements Action {
  readonly type = StockIssueActionTypes.CONFIRM_ISSUE;
  constructor(public payload: ConfirmIssuePayload) {}
}
export class ConfirmIssueSuccess implements Action {
  readonly type = StockIssueActionTypes.CONFIRM_ISSUE_SUCCESS;
  constructor(public payload: IssueConfirmResponse) {}
}
export class ConfirmIssueFailure implements Action {
  readonly type = StockIssueActionTypes.CONFIRM_ISSUE_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export class LoadCourierDetails implements Action {
  readonly type = StockIssueActionTypes.LOAD_COURIER_DETAILS;
  constructor(public payload: string) {}
}
export class LoadCourierDetailsSuccess implements Action {
  readonly type = StockIssueActionTypes.LOAD_COURIER_DETAILS_SUCCESS;
  constructor(public payload: string[]) {}
}
export class LoadCourierDetailsFailure implements Action {
  readonly type = StockIssueActionTypes.LOAD_COURIER_DETAILS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadEmployeeCodes implements Action {
  readonly type = StockIssueActionTypes.LOAD_EMPLOYEE_CODES;
}
export class LoadEmployeeCodesSuccess implements Action {
  readonly type = StockIssueActionTypes.LOAD_EMPLOYEE_CODES_SUCCESS;
  constructor(public payload: string[]) {}
}
export class LoadEmployeeCodesFailure implements Action {
  readonly type = StockIssueActionTypes.LOAD_EMPLOYEE_CODES_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadEmployeeDetails implements Action {
  readonly type = StockIssueActionTypes.LOAD_EMPLOYEE_DETAILS;
  constructor(public payload: string) {}
}
export class LoadEmployeeDetailsSuccess implements Action {
  readonly type = StockIssueActionTypes.LOAD_EMPLOYEE_DETAILS_SUCCESS;
  constructor(public payload: StoreUserDetails) {}
}
export class LoadEmployeeDetailsFailure implements Action {
  readonly type = StockIssueActionTypes.LOAD_EMPLOYEE_DETAILS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadProductCategories implements Action {
  readonly type = StockIssueActionTypes.LOAD_PRODUCT_CATEGORIES;
}

export class LoadProductCategoriesSuccess implements Action {
  readonly type = StockIssueActionTypes.LOAD_PRODUCT_CATEGORIES_SUCCESS;
  constructor(public payload: Filter[]) {}
}

export class LoadProductCategoriesFailure implements Action {
  readonly type = StockIssueActionTypes.LOAD_PRODUCT_CATEGORIES_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export class LoadProductGroups implements Action {
  readonly type = StockIssueActionTypes.LOAD_PROUDCT_GROUPS;
}

export class LoadProductGroupsSuccess implements Action {
  readonly type = StockIssueActionTypes.LOAD_PROUDCT_GROUPS_SUCCESS;
  constructor(public payload: Filter[]) {}
}

export class LoadProductGroupsFailure implements Action {
  readonly type = StockIssueActionTypes.LOAD_PROUDCT_GROUPS_FAILURE;
  constructor(public payload: CustomErrors) {}
}
export class SetFilterDataApprovedProducts implements Action {
  readonly type = StockIssueActionTypes.SET_FILTER_DATA_APPROVED_PRODUCTS;
  constructor(public payload: { [key: string]: Filter[] }) {}
}
export class SetFilterDataSelectedProducts implements Action {
  readonly type = StockIssueActionTypes.SET_FILTER_DATA_SELECTED_PRODUCTS;
  constructor(public payload: { [key: string]: Filter[] }) {}
}
export class SetSortDataApprovedProducts implements Action {
  readonly type = StockIssueActionTypes.SET_SORT_DATA_APPROVED_PRODUCTS;
  constructor(public payload: Column[]) {}
}
export class SetSortDataSelectedProducts implements Action {
  readonly type = StockIssueActionTypes.SET_SORT_DATA_SELECTED_PRODUCTS;
  constructor(public payload: Column[]) {}
}
export class ClearSortAndFilter implements Action {
  readonly type = StockIssueActionTypes.CLEAR_SORT_AND_FILTER;
}
export class ResetError implements Action {
  readonly type = StockIssueActionTypes.RESET_ERROR;
}

export type StockIssueActions =
  | LoadFactoryIssuePendingSTN
  | LoadFactoryIssuePendingSTNSuccess
  | LoadFactoryIssuePendingSTNFailure
  | LoadBoutiqueIssuePendingSTN
  | LoadBoutiqueIssuePendingSTNSuccess
  | LoadBoutiqueIssuePendingSTNFailure
  | LoadMerchantIssuePendingSTN
  | LoadMerchantIssuePendingSTNSuccess
  | LoadMerchantIssuePendingSTNFailure
  | LoadIssueSTNCount
  | LoadIssueSTNCountSuccess
  | LoadIssueSTNCountFailure
  | SearchPendingIssues
  | SearchPendingIssuesSuccess
  | SeachPendingIssuesFailure
  | SearchClear
  | ResetStockIssueList
  | LoadSelectedIssue
  | LoadSelectedIssueSuccess
  | LoadSelectedIssueFailure
  | LoadItems
  | LoadItemsSuccess
  | LoadItemsFailure
  | LoadApprovedItems
  | LoadApprovedItemsSuccess
  | LoadApprovedItemsFailure
  | LoadSelectedItems
  | LoadSelectedItemsSuccess
  | LoadSelectedItemsFailure
  // | LoadIssueItems
  // | LoadIssueItemsSuccess
  // | LoadIssueItemsFailure
  | LoadIssueItemsTotalCount
  | LoadIssueItemsTotalCountSuccess
  | LoadIssueItemsTotalCountFailure
  | SearchIssueItems
  | SearchIssueItemsSuccess
  | SearchIssueItemsFailure
  | LoadSearchIssueItemsTotalCount
  | LoadSearchIssueItemsTotalCountSuccess
  | LoadSearchIssueItemsTotalCountFailure
  | ClearSearchItems
  | UpdateAllItems
  | UpdateAllItemsSuccess
  | UpdateAllItemsFailure
  | UpdateItem
  | UpdateItemSuccess
  | UpdateItemFailure
  | ConfirmIssue
  | ConfirmIssueSuccess
  | ConfirmIssueFailure
  | LoadCourierDetails
  | LoadCourierDetailsSuccess
  | LoadCourierDetailsFailure
  | LoadEmployeeCodes
  | LoadEmployeeCodesSuccess
  | LoadEmployeeCodesFailure
  | LoadEmployeeDetails
  | LoadEmployeeDetailsSuccess
  | LoadEmployeeDetailsFailure
  | LoadProductGroups
  | LoadProductGroupsSuccess
  | LoadProductGroupsFailure
  | LoadProductCategories
  | LoadProductCategoriesSuccess
  | LoadProductCategoriesFailure
  | SetFilterDataApprovedProducts
  | SetFilterDataSelectedProducts
  | SetSortDataApprovedProducts
  | SetSortDataSelectedProducts
  | ClearSortAndFilter
  | ResetError;
