import {
  StockIssueActions,
  StockIssueActionTypes
} from './stock-issue.actions';
import { StockIssueState } from './stock-issue.state';
import {
  issueItemAdaptor,
  requestStockTransferNoteAdaptor
} from './stock-issue.entity';

export const initialState: StockIssueState = {
  issueFactorySTN: requestStockTransferNoteAdaptor.getInitialState(),
  issueBoutiqueSTN: requestStockTransferNoteAdaptor.getInitialState(),
  issueMerchantSTN: requestStockTransferNoteAdaptor.getInitialState(),

  isLoadingIssueFactorySTN: false,
  isLoadingIssueBoutiqueSTN: false,
  isLoadingIssueMerchantSTN: false,

  pendingBTQ_FAC_STNCount: 0,
  pendingBTQ_BTQ_STNCount: 0,
  pendingBTQ_MER_STNCount: 0,

  isLoadingIssueCount: false,

  searchIssueResult: requestStockTransferNoteAdaptor.getInitialState(),
  isSearchingIssues: false,
  hasSearchIssueResults: null,

  isStockIssueListReset: false,

  //DETAILS PAGE
  selectedIssue: null,
  isLoadingSelectedIssue: false,

  isItemsTotalCountLoading: false,
  isItemsTotalCountLoaded: null,

  issueItems: issueItemAdaptor.getInitialState(),
  isIssueItemsLoading: false,
  issueItemsTotalCount: 0,

  approvedItems: issueItemAdaptor.getInitialState(),
  isApprovedItemsLoading: false,
  approvedItemsTotalCount: 0,

  selectedItems: issueItemAdaptor.getInitialState(),
  isSelectedItemsLoading: false,
  selectedItemsTotalCount: 0,

  searchedItems: issueItemAdaptor.getInitialState(),
  isSearchingItems: false,
  hasSearchedItems: false,
  items: issueItemAdaptor.getInitialState(),
  isItemsLoading: false,
  isItemsLoaded: null,
  itemsCount: 0,

  isSearchIssueItemsCountLoaded: false,
  searchedIssueItemsCount: 0,

  isUpdatingAllItems: false,
  isUpdatingAllItemsSuccess: null,

  updateItemSuccess: null,
  isupdateItemLoading: false,
  isitemUpdating: false,
  isItemUpdated: false,

  confirmIssue: null,
  isItemIssued: null,

  courierDetails: null,
  isLoadingCourierDetails: false,
  hasCourierDetails: false,

  employeeCodes: [],
  employeeDetails: null,

  productCategories: [],
  productGroups: [],
  isLoading: false,

  filterDataAllProducts: {},
  filterDataSelectedProducts: {},
  sortDataAllProducts: [],
  sortDataSelectedProducts: [],

  error: null
};

export function StockIssueReducer(
  state: StockIssueState = initialState,
  action: StockIssueActions
): StockIssueState {
  switch (action.type) {
    case StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_FACTORY_STN:
      return {
        ...state,
        isLoadingIssueFactorySTN: true,
        error: null
      };
    case StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_FACTORY_STN_SUCCESS:
      return {
        ...state,
        issueFactorySTN: requestStockTransferNoteAdaptor.addMany(
          action.payload,
          state.issueFactorySTN
        ),
        isLoadingIssueFactorySTN: false
      };
    case StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_FACTORY_STN_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingIssueFactorySTN: false
      };

    case StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_BOUTIQUE_STN:
      return {
        ...state,
        isLoadingIssueBoutiqueSTN: true,
        error: null
      };
    case StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_BOUTIQUE_STN_SUCCESS:
      return {
        ...state,
        issueBoutiqueSTN: requestStockTransferNoteAdaptor.addMany(
          action.payload,
          state.issueBoutiqueSTN
        ),
        isLoadingIssueBoutiqueSTN: false
      };
    case StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_BOUTIQUE_STN_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingIssueBoutiqueSTN: false
      };

    case StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_MERCHANT_STN:
      return {
        ...state,
        isLoadingIssueMerchantSTN: true,
        error: null
      };
    case StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_MERCHANT_STN_SUCCESS:
      return {
        ...state,
        issueMerchantSTN: requestStockTransferNoteAdaptor.addMany(
          action.payload,
          state.issueMerchantSTN
        ),
        isLoadingIssueMerchantSTN: false
      };
    case StockIssueActionTypes.LOAD_PENDING_ISSUE_TO_MERCHANT_STN_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingIssueMerchantSTN: false
      };

    case StockIssueActionTypes.SEARCH_PENDING_ISSUES:
      return {
        ...state,
        isSearchingIssues: true,
        hasSearchIssueResults: false,
        searchIssueResult: requestStockTransferNoteAdaptor.removeAll(
          state.searchIssueResult
        ),
        error: null
      };
    case StockIssueActionTypes.SEARCH_PENDING_ISSUES_SUCCESS:
      return {
        ...state,
        isSearchingIssues: false,
        hasSearchIssueResults: true,
        searchIssueResult: requestStockTransferNoteAdaptor.addAll(
          action.payload,
          state.searchIssueResult
        )
      };
    case StockIssueActionTypes.SEARCH_PENDING_ISSUES_FAILURE:
      return {
        ...state,
        isSearchingIssues: false,
        hasSearchIssueResults: false,
        error: action.payload
      };

    case StockIssueActionTypes.RESET_STOCK_ISSUE_LIST:
      return {
        ...state,
        issueBoutiqueSTN: requestStockTransferNoteAdaptor.getInitialState(),
        issueFactorySTN: requestStockTransferNoteAdaptor.getInitialState(),
        issueMerchantSTN: requestStockTransferNoteAdaptor.getInitialState(),
        isStockIssueListReset: true,
        hasSearchIssueResults: false,
        error: null
      };

    case StockIssueActionTypes.LOAD_ISSUES_COUNT:
      return {
        ...state,
        isLoadingIssueCount: true,
        error: null
      };
    case StockIssueActionTypes.LOAD_ISSUES_COUNT_SUCCESS:
      return {
        ...state,
        pendingBTQ_BTQ_STNCount: action.payload.pendingIssueBTQ_BTQ_STNCount,
        pendingBTQ_FAC_STNCount: action.payload.pendingIssueBTQ_FAC_STNCount,
        pendingBTQ_MER_STNCount: action.payload.pendingIssueBTQ_MER_STNCount,
        isLoadingIssueCount: false
      };
    case StockIssueActionTypes.LOAD_ISSUES_COUNT_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoadingIssueCount: false
      };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    case StockIssueActionTypes.LOAD_SELECTED_ISSUE:
      return {
        ...state,
        isLoadingSelectedIssue: true,
        selectedIssue: null,

        isItemsTotalCountLoaded: null,
        isItemsTotalCountLoading: false,

        approvedItems: issueItemAdaptor.removeAll(state.approvedItems),
        isApprovedItemsLoading: false,
        approvedItemsTotalCount: 0,

        selectedItems: issueItemAdaptor.removeAll(state.selectedItems),
        isSelectedItemsLoading: false,
        selectedItemsTotalCount: 0,

        searchedItems: issueItemAdaptor.removeAll(state.searchedItems),
        isSearchingItems: false,
        hasSearchedItems: false,

        isItemIssued: null,

        error: null
      };
    case StockIssueActionTypes.LOAD_SELECTED_ISSUE_SUCCESS:
      return {
        ...state,
        selectedIssue: action.payload,
        isLoadingSelectedIssue: false
      };
    case StockIssueActionTypes.LOAD_SELECTED_ISSUE_FAILURE:
      return {
        ...state,
        isLoadingSelectedIssue: false,
        error: action.payload
      };
    case StockIssueActionTypes.LOAD_ISSUE_ITEMS_COUNT:
      return {
        ...state,
        isItemsTotalCountLoading: true,
        isItemsTotalCountLoaded: null,
        approvedItemsTotalCount: 0,
        selectedItemsTotalCount: 0,
        error: null
      };
    case StockIssueActionTypes.LOAD_ISSUE_ITEMS_COUNT_SUCCESS:
      return {
        ...state,
        approvedItemsTotalCount: action.payload.approvedItemsTotalCount,
        selectedItemsTotalCount: action.payload.selectedItemsTotalCount,
        isItemsTotalCountLoading: false,
        isItemsTotalCountLoaded: true
      };

    case StockIssueActionTypes.LOAD_ISSUE_ITEMS_COUNT_FAILURE:
      return {
        ...state,
        error: action.payload,
        isItemsTotalCountLoading: false,
        isItemsTotalCountLoaded: false
      };

    case StockIssueActionTypes.LOAD_ITEMS:
      return {
        ...state,
        items: issueItemAdaptor.getInitialState(),
        isItemsLoading: true,
        isItemsLoaded: false,
        error: null
      };

    case StockIssueActionTypes.LOAD_ITEMS_SUCCESS:
      return {
        ...state,
        items: issueItemAdaptor.addAll(action.payload.items, state.items),
        itemsCount: action.payload.count,
        isItemsLoading: false,
        isItemsLoaded: true
      };
    case StockIssueActionTypes.LOAD_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isItemsLoading: false,
        isItemsLoaded: false
      };

    case StockIssueActionTypes.LOAD_APPROVED_ITEMS:
      return {
        ...state,
        isApprovedItemsLoading: true,
        error: null
      };

    case StockIssueActionTypes.LOAD_APPROVED_ITEMS_SUCCESS:
      return {
        ...state,
        approvedItems: issueItemAdaptor.addAll(
          action.payload,
          state.approvedItems
        ),
        isApprovedItemsLoading: false
      };

    case StockIssueActionTypes.LOAD_APPROVED_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isApprovedItemsLoading: false
      };

    case StockIssueActionTypes.LOAD_SELECTED_ITEMS:
      return {
        ...state,
        isSelectedItemsLoading: true,
        error: null
      };

    case StockIssueActionTypes.LOAD_SELECTED_ITEMS_SUCCESS:
      return {
        ...state,
        selectedItems: issueItemAdaptor.addAll(
          action.payload,
          state.selectedItems
        ),
        isSelectedItemsLoading: false
      };

    case StockIssueActionTypes.LOAD_SELECTED_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isSelectedItemsLoading: false
      };

    // TO DELETE
    // case StockIssueActionTypes.LOAD_ISSUE_ITEMS:
    //   return {
    //     ...state,
    //     issueItems: issueItemAdaptor.getInitialState(),
    //     isIssueItemsLoading: true,

    //     error: null
    //   }
    // case StockIssueActionTypes.LOAD_ISSUE_ITEMS_SUCCESS:
    //   return {
    //     ...state,
    //     issueItems: issueItemAdaptor.addAll(
    //       action.payload,
    //       state.issueItems
    //     ),
    //     isIssueItemsLoading: false,

    //   }
    // case StockIssueActionTypes.LOAD_ISSUE_ITEMS_FAILURE:
    //   return {
    //     ...state,
    //     error: action.payload,
    //     isIssueItemsLoading: false
    //   }
    /////////////////////////////////////////////////////////
    case StockIssueActionTypes.SEARCH_ISSUE_ITEMS:
      return {
        ...state,
        searchedItems: issueItemAdaptor.getInitialState(),
        isSearchingItems: true,
        hasSearchedItems: false,
        error: null
      };
    case StockIssueActionTypes.SEARCH_ISSUE_ITEMS_SUCCESS:
      return {
        ...state,
        searchedItems: issueItemAdaptor.addAll(
          action.payload,
          state.searchedItems
        ),
        isSearchingItems: false,
        hasSearchedItems: true
      };
    case StockIssueActionTypes.SEARCH_ISSUE_ITEMS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isSearchingItems: false
      };
    case StockIssueActionTypes.LOAD_SEARCH_ISSUE_ITEMS_COUNT:
      return {
        ...state,
        isSearchIssueItemsCountLoaded: false,
        searchedIssueItemsCount: 0,
        error: null
      };
    case StockIssueActionTypes.LOAD_SEARCH_ISSUE_ITEMS_COUNT_SUCCESS:
      return {
        ...state,
        searchedIssueItemsCount: action.payload.searchedItemsCount,
        isSearchIssueItemsCountLoaded: true
      };

    case StockIssueActionTypes.LOAD_SEARCH_ISSUE_ITEMS_COUNT_FAILURE:
      return {
        ...state,
        error: action.payload
      };
    case StockIssueActionTypes.CLEAR_ISSUE_SEARCH_ITEMS:
      return {
        ...state,
        searchedItems: issueItemAdaptor.getInitialState(),
        isSearchingItems: false,
        hasSearchedItems: false,
        error: null
      };
    case StockIssueActionTypes.UPDATE_ALL_ITEM:
      return {
        ...state,
        isUpdatingAllItems: true,
        isUpdatingAllItemsSuccess: null,
        error: null
      };
    case StockIssueActionTypes.UPDATE_ALL_ITEM_SUCCESS:
      return {
        ...state,
        isUpdatingAllItems: false,
        isUpdatingAllItemsSuccess: true
      };
    case StockIssueActionTypes.UPDATE_ALL_ITEM_FAILURE:
      return {
        ...state,
        error: action.payload,
        isUpdatingAllItems: false,
        isUpdatingAllItemsSuccess: false
      };

    case StockIssueActionTypes.UPDATE_ITEM:
      return {
        ...state,
        isItemUpdated: false,
        isitemUpdating: true,
        items: issueItemAdaptor.updateOne(
          {
            id: action.payload.itemId,
            changes: {
              measuredQuantity: action.payload.newUpdate.measuredQuantity,
              status: action.payload.newUpdate.status,
              measuredWeight: action.payload.newUpdate.measuredWeight,
              inventoryId: action.payload.newUpdate.inventoryId,
              isUpdating: true,
              isUpdatingSuccess: null
            }
          },
          state.items
        ),

        error: null
      };
    case StockIssueActionTypes.UPDATE_ITEM_SUCCESS:
      return {
        ...state,
        isItemUpdated: true,
        isitemUpdating: false,
        items: issueItemAdaptor.updateOne(
          {
            id: action.payload.id,
            changes: {
              ...action.payload,
              isUpdating: false,
              isUpdatingSuccess: true
            }
          },
          state.items
        )
      };
    case StockIssueActionTypes.UPDATE_ITEM_FAILURE:
      return {
        ...state,
        isItemUpdated: false,
        isitemUpdating: false,
        items: issueItemAdaptor.updateOne(
          {
            id: action.payload.itemId,
            changes: {
              measuredQuantity: action.payload.actualDetails.measuredQuantity,
              status: action.payload.actualDetails.status,
              measuredWeight: action.payload.actualDetails.measuredWeight,
              inventoryId: action.payload.actualDetails.inventoryId,
              isUpdating: false,
              isUpdatingSuccess: false
            }
          },
          state.items
        ),
        error: action.payload.error
      };

    case StockIssueActionTypes.CONFIRM_ISSUE:
      return {
        ...state,
        isItemIssued: null,
        error: null
      };
    case StockIssueActionTypes.CONFIRM_ISSUE_SUCCESS:
      return {
        ...state,
        isItemIssued: true,
        confirmIssue: action.payload
      };
    case StockIssueActionTypes.CONFIRM_ISSUE_FAILURE:
      return {
        ...state,
        isItemIssued: false,
        error: action.payload
      };

    case StockIssueActionTypes.LOAD_COURIER_DETAILS:
      return {
        ...state,
        isLoadingCourierDetails: true,
        error: null
      };
    case StockIssueActionTypes.LOAD_COURIER_DETAILS_SUCCESS:
      return {
        ...state,
        courierDetails: action.payload,
        isLoadingCourierDetails: false,
        hasCourierDetails: true
      };
    case StockIssueActionTypes.LOAD_COURIER_DETAILS_FAILURE:
      return {
        ...state,
        error: action.payload,
        hasCourierDetails: false,
        isLoadingCourierDetails: false
      };
    case StockIssueActionTypes.LOAD_EMPLOYEE_CODES:
      console.log('codessssssssssssss');

      return {
        ...state,
        isLoading: true,
        error: null
      };
    case StockIssueActionTypes.LOAD_EMPLOYEE_CODES_SUCCESS:
      console.log('codes', action.payload);

      return {
        ...state,
        employeeCodes: action.payload,
        isLoading: false,
        error: null
      };
    case StockIssueActionTypes.LOAD_EMPLOYEE_CODES_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };

    case StockIssueActionTypes.LOAD_EMPLOYEE_DETAILS:
      return {
        ...state,
        isLoading: true,
        error: null
      };
    case StockIssueActionTypes.LOAD_EMPLOYEE_DETAILS_SUCCESS:
      return {
        ...state,
        employeeDetails: action.payload,
        isLoading: false,
        error: null
      };
    case StockIssueActionTypes.LOAD_EMPLOYEE_DETAILS_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };

    case StockIssueActionTypes.LOAD_PROUDCT_GROUPS:
    case StockIssueActionTypes.LOAD_PRODUCT_CATEGORIES:
      return {
        ...state,
        isLoading: true,
        error: null
      };
    case StockIssueActionTypes.LOAD_PRODUCT_CATEGORIES_SUCCESS:
      return {
        ...state,
        productCategories: action.payload,
        isLoading: false,
        error: null
      };
    case StockIssueActionTypes.LOAD_PROUDCT_GROUPS_FAILURE:
    case StockIssueActionTypes.LOAD_PRODUCT_CATEGORIES_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoading: false
      };
    case StockIssueActionTypes.LOAD_PROUDCT_GROUPS_SUCCESS:
      return {
        ...state,
        productGroups: action.payload,
        isLoading: false,
        error: null
      };
    case StockIssueActionTypes.SET_FILTER_DATA_APPROVED_PRODUCTS:
      return {
        ...state,
        filterDataAllProducts: action.payload
      };
    case StockIssueActionTypes.SET_FILTER_DATA_SELECTED_PRODUCTS:
      return {
        ...state,
        filterDataSelectedProducts: action.payload
      };
    case StockIssueActionTypes.SET_SORT_DATA_APPROVED_PRODUCTS:
      return {
        ...state,
        sortDataAllProducts: action.payload
      };
    case StockIssueActionTypes.SET_SORT_DATA_SELECTED_PRODUCTS:
      return {
        ...state,
        sortDataSelectedProducts: action.payload
      };
    case StockIssueActionTypes.CLEAR_SORT_AND_FILTER:
      return {
        ...state,
        filterDataAllProducts: {},
        filterDataSelectedProducts: {},
        sortDataAllProducts: [],
        sortDataSelectedProducts: []
      };

    case StockIssueActionTypes.RESET_ERROR:
      return {
        ...state,
        error: null
      };
    default:
      return state;
  }
}
