import {
  StockRequestNote,
  IssueConfirmResponse
} from '../models/stock-issue-models';
import {
  IssueItemEntity,
  RequestStockTransferNoteEntity
} from './stock-issue.entity';
import { CustomErrors, StoreUserDetails } from '@poss-web/core';
import { Filter, Column } from '@poss-web/shared';

export interface StockIssueState {
  issueFactorySTN: RequestStockTransferNoteEntity;
  issueBoutiqueSTN: RequestStockTransferNoteEntity;
  issueMerchantSTN: RequestStockTransferNoteEntity;

  isLoadingIssueFactorySTN: boolean;
  isLoadingIssueBoutiqueSTN: boolean;
  isLoadingIssueMerchantSTN: boolean;

  pendingBTQ_FAC_STNCount: number;
  pendingBTQ_BTQ_STNCount: number;
  pendingBTQ_MER_STNCount: number;
  isLoadingIssueCount: boolean;

  searchIssueResult: RequestStockTransferNoteEntity;
  isSearchingIssues: boolean;
  hasSearchIssueResults: boolean;

  isStockIssueListReset: boolean;

  //DETAILS PAGE

  selectedIssue: StockRequestNote;
  isLoadingSelectedIssue: boolean;

  isItemsTotalCountLoading: boolean;
  isItemsTotalCountLoaded: boolean;

  issueItems: IssueItemEntity;
  isIssueItemsLoading: boolean;
  issueItemsTotalCount: number;

  approvedItems: IssueItemEntity;
  isApprovedItemsLoading: boolean;
  approvedItemsTotalCount: number;

  selectedItems: IssueItemEntity;
  isSelectedItemsLoading: boolean;
  selectedItemsTotalCount: number;

  searchedItems: IssueItemEntity;
  isSearchingItems: boolean;
  hasSearchedItems: boolean;

  isSearchIssueItemsCountLoaded: boolean;
  searchedIssueItemsCount: number;

  updateItemSuccess: boolean;
  isitemUpdating: boolean;

  isupdateItemLoading: boolean;
  isItemUpdated: boolean;

  isUpdatingAllItems: boolean;
  isUpdatingAllItemsSuccess: boolean;

  confirmIssue: IssueConfirmResponse;
  isItemIssued: boolean;

  courierDetails: string[];
  isLoadingCourierDetails: boolean;
  hasCourierDetails: boolean;

  employeeCodes: string[];
  employeeDetails: StoreUserDetails;

  productCategories: Filter[];
  productGroups: Filter[];
  isLoading: boolean;

  filterDataAllProducts: { [key: string]: Filter[] };
  filterDataSelectedProducts: { [key: string]: Filter[] };

  sortDataAllProducts: Column[];
  sortDataSelectedProducts: Column[];

  items: IssueItemEntity;
  isItemsLoading: boolean;
  isItemsLoaded: boolean;
  itemsCount: number;

  error: CustomErrors;
}
