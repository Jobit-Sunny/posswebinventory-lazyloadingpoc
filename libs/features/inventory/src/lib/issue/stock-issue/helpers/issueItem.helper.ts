import { IssueInventoryItem } from '../models/stock-issue-models';
import { IssueItemAdaptor } from '../adaptors/issueItem.adaptor';
import { StockIssueAdaptor } from '../adaptors/stock-issue-adaptor';
import { Filter } from '@poss-web/shared';

export class IssueItemHelper {
  static getItems(data: any): { items: IssueInventoryItem[]; count: number } {
    const items: IssueInventoryItem[] = [];
    for (const item of data.results) {
      items.push(IssueItemAdaptor.fromJson(item));
    }
    return {
      items,
      count: data.totalElements
    };
  }

  static getProductCategories(data: any): Filter[] {
    const productCategories: Filter[] = [];
    for (const productCategory of data.results) {
      productCategories.push(
        StockIssueAdaptor.productCategoriesFromJson(productCategory)
      );
    }
    return productCategories;
  }

  static getProductGroups(data: any): Filter[] {
    const productGroups: Filter[] = [];
    for (const productGroup of data.results) {
      productGroups.push(StockIssueAdaptor.productGroupsFromJson(productGroup));
    }
    return productGroups;
  }
}
