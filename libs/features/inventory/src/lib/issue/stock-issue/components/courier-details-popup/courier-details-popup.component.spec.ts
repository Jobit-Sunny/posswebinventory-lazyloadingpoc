import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourierDetailsPopupComponent } from './courier-details-popup.component';

describe('CourierDetailsPopupComponent', () => {
  let component: CourierDetailsPopupComponent;
  let fixture: ComponentFixture<CourierDetailsPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourierDetailsPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourierDetailsPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
