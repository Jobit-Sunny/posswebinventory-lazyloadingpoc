import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Subject } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

@Component({
  selector: 'poss-web-out-of-stock-popup',
  templateUrl: './out-of-stock-popup.component.html',
  styleUrls: ['./out-of-stock-popup.component.scss']
})
export class OutOfStockPopupComponent implements OnInit, OnDestroy {
  destroy$: Subject<null> = new Subject<null>();

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA)
    public data: any,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    console.log('pop', this.data);
  }
  onClose(): void {
    this.dialogRef.close();
  }

  removeProducts() {
    this.dialogRef.close(true);
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
