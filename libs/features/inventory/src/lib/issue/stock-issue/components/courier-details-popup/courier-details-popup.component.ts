import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormArray,
  FormControl,
  Validators
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'poss-web-courier-details-popup',
  templateUrl: './courier-details-popup.component.html',
  styleUrls: ['./courier-details-popup.component.scss']
})
export class CourierDetailsPopupComponent implements OnInit, OnDestroy {
  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA)
    public data: any,
    public fb: FormBuilder
  ) {}

  carrierDetails: FormGroup;
  noOfBoxes = 0;
  destroy$: Subject<null> = new Subject<null>();

  ngOnInit() {
    this.carrierDetails = this.fb.group({
      box_No: new FormControl('', [
        Validators.max(10),
        Validators.min(0),
        Validators.pattern('^[0-9]*$')
      ]),
      box_Details: this.fb.array([
        // this.fb.group({ boxNo: '', lockNo: '', boxWt: '' })
      ])
    });
    for (const formValue of this.data) {
      this.boxDetails.push(
        this.fb.group({
          serialNumber: formValue.serialNumber,
          boxNumber: formValue.boxNumber,
          lockNumber: formValue.lockNumber,
          boxWeight: formValue.boxWeight,
          weightUnit: formValue.weightUnit
        })
      );
    }
    this.carrierDetails.patchValue({ box_No: this.data.length });
    this.carrierDetails.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.noOfBoxes = data.box_Details.length;
      });
  }

  get boxDetails() {
    return this.carrierDetails.get('box_Details') as FormArray;
  }

  addRow() {
    // this.boxDetails.clear();
    if (this.boxDetails.length < this.carrierDetails.controls.box_No.value) {
      for (
        let i = this.boxDetails.length;
        i < this.carrierDetails.controls.box_No.value;
        i++
      ) {
        this.boxDetails.push(
          this.fb.group({
            serialNumber: new FormControl(i + 1, [Validators.required]),
            boxNumber: new FormControl('', [
              Validators.required,
              Validators.pattern('^[a-zA-Z0-9]+$')
            ]),
            lockNumber: new FormControl('', [
              Validators.required,
              Validators.pattern('^[0-9]*$')
            ]),
            boxWeight: new FormControl('', [
              Validators.required,
              Validators.min(0.001),
              Validators.pattern('^[0-9]{1,11}(?:.[0-9]{1,3})?$')
            ]),
            weightUnit: new FormControl('gms', [Validators.required])
          })
        );
      }
    }
    this.carrierDetails.patchValue({ box_No: this.boxDetails.length });
  }

  deleteRow(index) {
    this.boxDetails.removeAt(index);
  }

  onClose(): void {
    this.dialogRef.close();
  }
  onSave() {
    this.dialogRef.close({
      count: this.boxDetails.length > 0 ? this.boxDetails.length : 0,
      value: this.carrierDetails.controls.box_Details.value
    });
  }
  ngOnDestroy() {
    this.boxDetails.clear();
    this.destroy$.next();
    this.destroy$.complete();
  }
}
