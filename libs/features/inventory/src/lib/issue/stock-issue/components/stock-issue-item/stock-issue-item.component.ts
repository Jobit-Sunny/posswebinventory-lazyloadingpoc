import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import {
  IssueInventoryItem,
  IssueItemToUpdate
} from '../../models/stock-issue-models';
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl,
  ValidatorFn,
  FormArray
} from '@angular/forms';
import { Subject, of, Observable } from 'rxjs';
import { debounceTime, takeUntil, delay } from 'rxjs/operators';

@Component({
  selector: 'poss-web-stock-issue-item',
  templateUrl: './stock-issue-item.component.html',
  styleUrls: ['./stock-issue-item.component.scss']
})
export class StockIssueItemComponent implements OnInit, OnDestroy, OnChanges {
  @Input() item: IssueInventoryItem;
  @Input() selectionEvents: Observable<any>;
  @Input() tab: any;

  @Input() parentForm: FormArray;

  @Output() selection: EventEmitter<{
    selected: boolean;
    id: string;
  }> = new EventEmitter();
  @Output() update = new EventEmitter<IssueItemToUpdate>();

  itemForm: FormGroup;
  destroy$ = new Subject<null>();

  tolerance = 0.03;
  showUpdateStatus = true;
  weight = 0;
  status: string;
  quantity: number;
  measuredWeight: number;
  itemWeight: any = 0;
  availableQty: number;
  initialQty: number;
  initialWt: number;
  selectionAllSubscription: any;
  disabled = false;
  weightErrorMsg = '';
  constructor() {}

  ngOnInit() {
    this.selectionEvents
      .pipe(
        takeUntil(this.destroy$),
        debounceTime(10)
      )
      .subscribe(data => {
        // if ((this.tab == 1) || (this.item.availableQuantity > 0 && this.tab == 0)) {
        if (data.selectCheckbox === true) {
          this.itemForm.patchValue({ isItemSelected: true });
        } else {
          this.itemForm.patchValue({ isItemSelected: false });
        }

        if (data.enableCheckbox === false) {
          this.itemForm.controls.isItemSelected.disable();
        } else {
          this.itemForm.controls.isItemSelected.enable();
        }
        // }
      });

    this.itemForm = this.createForm(this.item);
    this.parentForm.push(this.itemForm);

    if (this.item.availableQuantity <= 0) {
      if (this.tab !== 1) {
        this.itemForm.disable();
      }
    } else {
      this.itemWeight = (
        this.item.availableWeight / this.item.availableQuantity
      ).toFixed(3);
    }

    this.itemForm
      .get('issueQuantity')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.itemForm.patchValue({ weight: null }, { emitEvent: false });
        if (this.item.availableQuantity === 0) {
          this.itemForm.patchValue(
            { issueQuantity: 0, weight: 0 },
            { emitEvent: false }
          );
          this.itemForm.markAsPristine();
        } else if (this.itemForm.get('issueQuantity').invalid) {
          of(true)
            .pipe(delay(2000))
            .subscribe(() => {
              this.itemForm.patchValue(
                { issueQuantity: this.quantity, weight: null },
                { emitEvent: false }
              );
            });
          this.makeFormDirty();
        }
      });

    this.itemForm
      .get('weight')
      .valueChanges.pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe(value => {
        const mWeight =
          (this.item.availableWeight / this.item.availableQuantity) *
            this.itemForm.get('issueQuantity').value -
          value;
        if (value != null) {
          if (Math.abs(Number(mWeight.toFixed(3))) > this.tolerance) {
            this.weightErrorMsg = 'Weight Mismatch';
            this.itemForm.markAsDirty();
          } else {
            this.weightErrorMsg = '';
            this.itemForm.markAsPristine();

            of(true)
              .pipe(delay(1000))
              .subscribe(() => {
                this.updateItem();
              });
          }
        }
      });

    this.itemForm
      .get('isItemSelected')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (value) {
          this.itemForm.markAsDirty();
        } else {
          this.itemForm.get('isItemSelected').markAsPristine();
        }
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes &&
      changes['item'] &&
      changes['item']['currentValue'] &&
      (changes['item']['currentValue']['isUpdatingSuccess'] === true ||
        changes['item']['currentValue']['isUpdatingSuccess'] === false)
    ) {
      of(true)
        .pipe(delay(2000))
        .pipe(takeUntil(this.destroy$))
        .subscribe(() => (this.showUpdateStatus = false));
    }
  }
  createForm(item: IssueInventoryItem): FormGroup {
    item.availableQuantity == null
      ? (this.availableQty = 0)
      : (this.availableQty = this.item.availableQuantity);

    if (item.measuredQuantity === null || item.measuredQuantity === 0) {
      this.quantity = this.availableQty;
      this.weight = item.availableWeight;
    } else {
      this.quantity = item.measuredQuantity;
      this.weight = item.measuredWeight;
    }
    if (item.availableQuantity <= 0 || item.availableQuantity === null) {
      return new FormGroup({
        isItemSelected: new FormControl(''),
        issueQuantity: new FormControl(
          '0',
          Validators.compose([Validators.required])
        ),
        weight: new FormControl('0', Validators.compose([Validators.required])),
        bin: new FormControl(item.binCode)
      });
    }

    return new FormGroup({
      isItemSelected: new FormControl(''),
      issueQuantity: new FormControl(
        this.quantity,
        Validators.compose([
          Validators.required,
          Validators.min(1),
          Validators.max(item.availableQuantity)
        ])
      ),
      weight: new FormControl(
        this.weight.toFixed(3),
        Validators.compose([Validators.required])
      ),
      bin: new FormControl(item.binCode)
    });
  }

  selectItem() {
    this.selection.emit({
      id: this.item.id,
      selected: this.itemForm.get('isItemSelected').value
    });
  }
  updateItem() {
    if (this.itemForm.valid) {
      this.update.emit(this.createItemUpdatePayload());
    }
  }

  createItemUpdatePayload(): IssueItemToUpdate {
    if (
      this.item.measuredQuantity === null ||
      this.item.measuredQuantity === 0
    ) {
      this.quantity = this.availableQty;
      this.weight = this.item.availableWeight;
    } else {
      this.quantity = this.item.measuredQuantity;
      this.weight = this.item.measuredWeight;
    }
    return {
      id: this.item.id,
      newUpdate: {
        measuredQuantity: this.itemForm.get('issueQuantity').value,
        status: this.item.status,
        measuredWeight: this.itemForm.get('weight').value,
        inventoryId: this.item.inventoryId
      },
      actualDetails: {
        measuredQuantity: this.quantity,
        status: this.item.status,
        measuredWeight: this.weight,
        inventoryId: this.item.inventoryId
      }
    };
  }

  makeFormDirty() {
    this.itemForm.markAsDirty();
    this.itemForm.controls['issueQuantity'].markAsDirty();
    this.itemForm.controls['weight'].markAsDirty();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    while (this.parentForm.length !== 0) {
      this.parentForm.removeAt(0);
    }
  }
}
