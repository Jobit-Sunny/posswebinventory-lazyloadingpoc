import {
  Component,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { PageEvent } from '@angular/material';
import { IssueInventoryItem, IssueItemToUpdate } from '../../models/stock-issue-models';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { AppsettingFacade } from '@poss-web/core';
import { FormArray } from '@angular/forms';

@Component({
  selector: 'poss-web-stock-issue-item-list',
  templateUrl: './stock-issue-item-list.component.html',
  styleUrls: ['./stock-issue-item-list.component.scss']
})
export class StockIssueItemListComponent implements OnDestroy, OnInit {
  @Input() itemList: IssueInventoryItem[];
  @Input() count: number;
  @Input() pageSize = 0;
  @Input() pageEvent: PageEvent;
  @Input() selectionEvents: Observable<boolean>;
  @Input() tab: any;

  @Output() paginator = new EventEmitter<PageEvent>();
  @Output() selection: EventEmitter<{
    selected: boolean;
    id: number;
  }> = new EventEmitter();
  @Output() update = new EventEmitter<IssueItemToUpdate>();
  @Output() parentFormDirty = new EventEmitter<boolean>();

  parentForm: FormArray;
  isFormDirty = false;

  pageSizeOptions: number[] = [];
  selectionAllSubscription: any;

  selectionAllSubject: Subject<any> = new Subject<any>();
  destroy$ = new Subject<null>();

  constructor(private appsettingFacade: AppsettingFacade) {
    this.parentForm = new FormArray([]);
    this.parentForm.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        // console.log("inside itemList", value);
        let isDirty = false;
        for (
          let i = 0;
          i < this.parentForm.controls.length && !isDirty;
          i++
        ) {
          if (!this.parentForm.controls[i].pristine) {
            isDirty = true;
          }
        }
        if (this.isFormDirty !== isDirty) {
          this.isFormDirty = isDirty;
          this.parentFormDirty.emit(isDirty);
        }
      })
  }

  ngOnInit() {
    this.appsettingFacade
      .getPageSize()
      .pipe(takeUntil(this.destroy$))
      .subscribe(resp => (this.pageSize = JSON.parse(resp)));
    this.pageEvent.pageSize = this.pageSize;

    this.appsettingFacade
      .getPageSizeOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(resp => (this.pageSizeOptions = resp));


    this.selectionEvents
      .pipe(
        takeUntil(this.destroy$),
        debounceTime(10)
      )
      .subscribe(data => {
        this.selectionAllSubject.next(data);
      });
  }
  selectionEmit(selection: { selected: boolean; id: number }) {
    this.selection.emit(selection);
  }

  paginate(event: PageEvent) {
    this.paginator.emit(event);
  }

  updateItem(itemToUpdate: IssueItemToUpdate) {
    this.update.emit(itemToUpdate);
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
