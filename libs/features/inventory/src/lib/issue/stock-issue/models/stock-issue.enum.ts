export enum StockIssueTypesEnum {
  FAC_BTQ = 'factory',
  BTQ_BTQ = 'boutique',
  BTQ_MER = 'merchandise',
  OTHER_RECEIPTS = 'otherIssues',
  BTQ_CFA = 'cfa',
  OTHER_INVOICES = 'otherReturns'
}
