import { Filter } from '@poss-web/shared';

export const PRODUCT_CATEGORIES: { [key: string]: Filter[] } = {
  'Product Category': [
    {
      id: 1,
      description: 'A',
      selected: false
    },
    {
      id: 2,
      description: 'D',
      selected: false
    },
    {
      id: 3,
      description: 'F',
      selected: false
    },
    {
      id: 4,
      description: 'S',
      selected: false
    },
    {
      id: 5,
      description: 'V',
      selected: false
    }
  ]
};

export const PRODUCT_GROUP: { [key: string]: Filter[] } = {
  'Product Group': [
    {
      id: 1,
      description: '71',
      selected: false
    },
    {
      id: 2,
      description: '72',
      selected: false
    }
  ]
};

export const SORT_DATA = [
  {
    id: 0,
    sortByColumnName: 'Requested Weight',
    sortAscOrder: false
  },
  {
    id: 1,
    sortByColumnName: 'Approved Quantity',
    sortAscOrder: false
  }
];
