export interface CourierDetails {
  address: 'string';
  contactPerson: 'string';
  courierName: 'string';
  isActive: true;
  mailId: 'string';
  mobileNumber: 'string';
  phoneNumber: 'string';
  stateCode: 'string';
  townCode: 'string';
}
