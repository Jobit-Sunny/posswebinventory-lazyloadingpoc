import { Moment } from 'moment';
//new
export interface StockRequestNote {
  carrierDetails: {};
  currencyCode: string;
  destDocDate: Moment;
  destDocNo: number;
  destLocationCode: string;
  id: number;
  orderType: string;
  otherDetails: {};
  reqDocDate: Moment;
  reqDocNo: number;
  reqLocationCode: string;
  requestType: string;
  srcDocDate: Moment;
  srcDocNo: number;
  srcFiscalYear: number;
  srcLocationCode: string;
  status: string;
  totalAvailableQuantity: number;
  totalAvailableValue: number;
  totalAvailableWeight: number;
  totalMeasuredQuantity: number;
  totalMeasuredValue: number;
  totalMeasuredWeight: number;
  weightUnit: string;
}
/**
 *Model for request STN
 */
export interface RequestStockTransferNote {
  currencyUnit: string;
  destLocationCode: string;
  id: number;
  reqDocDate: Moment;
  reqDocNo: number;
  reqLocationCode: string;
  requestType: string;
  srcLocationCode: string;
  status: string;
  totalQuantity: number;
  totalValue: number;
  totalWeight: number;
  weightUnit: string;
}

/**
 *Model for request Invoice
 */

export interface RequestInvoice {
  currencyCode: string;
  destDocDate: Moment;
  destDocNo: number;
  destLocationCode: string;
  id: number;
  invoiceType: string;
  orderType: string;
  reason: string;
  srcDocDate: Moment;
  srcDocNo: number;
  srcFiscalYear: number;
  srcLocationCode: string;
  status: string;
  totalQuantity: number;
  totalValue: number;
  totalWeight: number;
  weightUnit: string;
}
/**
 * Model for request STN count
 */
export interface RequestStockTransferNoteCount {
  stncount: number;
  stntype: string;
}
export interface SelectedItemsIds {
  id: number;
}
/*
Model for Issue Item Details */
export interface IssueItem {
  approvedQuantity: number;
  // availableQuantity: number;
  binCode: string;
  binGroupCode: string;
  currencyCode: string;
  id: number;
  imageURL: string;
  inventoryId?: number;
  issuedQuantity: number;
  itemCode: string;
  itemDetails: {};
  stdValue: 0;
  stdWeight: number;
  lotNumber: string;
  measuredWeight: number;
  mfgDate: Moment;
  orderType: string;
  productCategory: string;
  productGroup: string;
  requestedQuantity: number;
  selectedQuantity: number;
  status: string;
  availableQuantity: number;
  availableValue: number;
  availableWeight: number;
  weightUnit: string;
  isUpdating: boolean;
  isUpdatingSuccess: boolean;
  totalElements?: number;
}
export interface IssueInventoryItem {
  availableQuantity: number;
  availableValue: number;
  availableWeight: number;
  binCode: string;
  binGroupCode: string;
  currencyCode: string;
  id: string;
  imageURL: string;
  inventoryId: string;
  itemCode: string;
  itemDetails: {};
  lotNumber: string;
  measuredQuantity: number;
  measuredValue: number;
  measuredWeight: number;
  mfgDate: Moment;
  orderType: string;
  productCategory: string;
  productGroup: string;
  status: string;
  stdValue: number;
  stdWeight: number;
  weightUnit: string;
  isUpdating: boolean;
  isUpdatingSuccess: boolean;
}
export interface CourierDetails {
  courierCompany: string;
  courierDocketNumber: string;
  courierLockNumber: string;
  courierRoadPermitNumber: string;
}

export interface IssueConfirmResponse {
  courierDetails: any;
  courierReceivedDate: Moment;
  currencyCode: string;
  destDocDate: Moment;
  destDocNo: number;
  destLocationCode: string;
  id: number;
  orderType: string;
  srcDocDate: Moment;
  srcDocNo: number;
  srcFiscalYear: number;
  srcLocationCode: string;
  status: string;
  totalAvailableQuantity: number;
  totalAvailableValue: number;
  totalAvailableWeight: number;
  totalMeasuredQuantity: number;
  totalMeasuredValue: number;
  totalMeasuredWeight: number;
  transferType: number;
  weightUnit: string;
}
export interface IssueItemToUpdate {
  id: string;
  newUpdate: IssueItemUpdate;
  actualDetails: IssueItemUpdate;
  //TODO : to be checked with jobit
  courierDetails?: null;
}
export interface IssueItemUpdate {
  measuredQuantity: number;
  status: string;
  inventoryId: string;
  measuredWeight: number;
}
