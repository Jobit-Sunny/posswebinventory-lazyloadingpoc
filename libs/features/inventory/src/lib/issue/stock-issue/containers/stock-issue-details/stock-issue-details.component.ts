import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  TemplateRef
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { StockIssueFacade } from '../../+state/stock-issue.facade';
import {
  AppsettingFacade,
  CustomErrors,
  ShortcutService,
  Command,
  AuthFacade,
  StoreUserDetails
} from '@poss-web/core';
import { Router, ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import {
  StockRequestNote,
  IssueItemToUpdate,
  IssueInventoryItem
} from '../../models/stock-issue-models';
import { PageEvent, MatDialog } from '@angular/material';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef,
  OverlayNotificationEventType,
  SearchComponent,
  SearchResponse,
  Column,
  FilterService,
  SortDialogService,
  Filter,
  FilterActions,
  ErrorEnums
} from '@poss-web/shared';
import { TranslateService } from '@ngx-translate/core';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE
} from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { StockIssueTypesEnum } from '../../models/stock-issue.enum';
import { Location } from '@angular/common';
import { OutOfStockPopupComponent } from '../../components/out-of-stock-popup/out-of-stock-popup.component';
import { SORT_DATA } from '../../models/issue-item-data';
import { IssueTEPFacade } from '../../../stock-issue-tep-gep/+state/stock-issue-tep-gep.facade';
import { CourierDetailsPopupComponent } from '../../components/courier-details-popup/courier-details-popup.component';

export const DATE_FORMATS = {
  parse: {
    dateInput: 'DD-MMM-YYYY'
  },
  display: {
    dateInput: 'DD-MMM-YYYY',
    monthYearLabel: 'MMM-YYYY',
    dateA11yLabel: 'DD-MMM-YYYY',
    monthYearA11yLabel: 'DD-MMM-YYYY'
  }
};
export enum StockIssueTabEnum {
  approvedProducts = 0,
  selectedProducts = 1
}
export interface Form {
  value: string;
  viewValue: string;
}

const SEARCH_SHORTCUT_KEY_F2 = 'StockIssueDetailsComponent.F2';
@Component({
  selector: 'poss-web-stock-issue-details',
  templateUrl: './stock-issue-details.component.html',
  styleUrls: ['./stock-issue-details.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    { provide: MAT_DATE_FORMATS, useValue: DATE_FORMATS }
  ]
})
export class StockIssueDetailsComponent implements OnInit, OnDestroy {
  selectedIssue: any;
  tab: StockIssueTabEnum;

  pageSize = 5;
  pageIndex = 0;
  initialPageEvent: PageEvent = {
    pageIndex: this.pageIndex,
    pageSize: this.pageSize,
    length: 0
  };
  issueItemsPageEvent: PageEvent = this.initialPageEvent;
  // searchItemsPageEvent: PageEvent = this.initialPageEvent;
  // approvedProductsPageEvent: PageEvent = this.initialPageEvent;
  // selectedProductsPageEvent: PageEvent = this.initialPageEvent;

  searchedItems$: Observable<IssueInventoryItem[]>;
  isSearchingItems$: Observable<boolean>;
  hasSearchedItems$: Observable<boolean>;
  hasSearchItems: boolean;
  approvedItemsTotalCount = 0;
  selectedItemsTotalCount = 0;

  selectedItemsIds$: Observable<any>;

  issueItemsCount = 0;
  issueItemsCountLoaded$: Observable<boolean>;

  issueItemsCountLoading$: Observable<boolean>;
  approvedItems$: Observable<IssueInventoryItem[]>;
  selectedItems$: Observable<IssueInventoryItem[]>;

  issueItems$: Observable<IssueInventoryItem[]>;

  type: string;
  reqDocNumber: number;
  storeType: string;
  isRemarksMandatory: boolean;

  error$: Observable<CustomErrors>;
  isLoading$: Observable<boolean>;
  isItemsLoading$: Observable<boolean>;
  isItemsLoaded$: Observable<boolean>;
  items$: Observable<IssueInventoryItem[]>;
  itemsCount$: Observable<number>;

  isLoadingSelectedIssue$: Observable<boolean>;
  isApprovedItemsLoading$: Observable<boolean>;
  isSelectedItemsLoading$: Observable<boolean>;
  itemsTotalCountLoaded$: Observable<boolean>;
  itemsTotalCountLoading$: Observable<boolean>;

  isIssued$: Observable<boolean>;
  isAllSelected$: Observable<boolean>;
  isAllSelected: boolean;

  itemCode = '';
  lotNumber = '';

  @ViewChild(SearchComponent, { static: true })
  searchRef: SearchComponent;

  destroy$: Subject<null> = new Subject<null>();

  selectedProductsId: number[] = [];
  confirmedWeight = 0;
  confirmationSrcDocNo: number;

  searchedItemCode: string;
  searchedItemsCount = 0;

  hasNotification = true;

  issueType: string;
  requestType: string;

  selectionAll: boolean;
  selectionAllSubscription: any;
  selectionAllSubject: Subject<any> = new Subject<any>();

  isDirty = false;
  isFormValid = false;
  itemIds: string[] = [];
  selectedIds: string[] = [];

  quantityCheck: number;

  form: FormGroup;
  formList: FormArray;
  selectedFormName: string;
  carrierData: any;

  isItemUpdating = false;
  isItemUpdating$: Observable<boolean>;

  selectForm = this.fb.group({
    selectRadioButton: ['']
  });
  locationCode: string;
  courierDetails: any;

  sortBy: string;
  sortOrder: string;
  sortData: Column[] = [];
  maxSortLimit: number;
  sortByMap = new Map();

  PRODUCT_CATEGORIES: { [key: string]: Filter[] } = {};
  PRODUCT_GROUP: { [key: string]: Filter[] } = {};

  maxFilterLimit: number;

  filterData: { [key: string]: Filter[] } = {};
  filterByMap = new Map();

  //To do check
  sortMapApprovedProducts = new Map();
  sortMapSelectedProducts = new Map();
  filterMapApprovedProducts = new Map();
  filterMapSelectedProducts = new Map();

  sortTabDataApprovedProducts: Column[];
  sortTabDataSelectedProducts: Column[];

  filterTabDataApprovedProducts: { [key: string]: Filter[] };
  filterTabDataSelectedProducts: { [key: string]: Filter[] };

  filterApprovedProducts: { key: string; value: any[] }[] = [];
  filterSelectedProducts: { key: string; value: any[] }[] = [];

  boxDetails_count = 0;
  boxDetails_values: any;

  isReadOnly: boolean;
  employeeCodes: string[];
  employeeDetailsResponse$: Observable<StoreUserDetails>;
  employeeCodesResponse$: Observable<string[]>;

  @ViewChild('confirmSuccessNotificationTemplate', { static: true })
  confirmSuccessNotificationTemplate: TemplateRef<any>;

  constructor(
    private stockIssueFacade: StockIssueFacade,
    private appSettingFacade: AppsettingFacade,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private overlayNotification: OverlayNotificationService,
    private translate: TranslateService,
    public dialog: MatDialog,
    private shortcutService: ShortcutService,
    private sortDialogService: SortDialogService,
    private filterService: FilterService,
    private authFacade: AuthFacade,
    private issueTEPFacade: IssueTEPFacade
  ) {}

  ngOnInit() {
    this.form = this.fb.group({
      formCollection: this.fb.array([this.createcourierForm()])
    });
    this.formList = this.form.get('formCollection') as FormArray;

    this.tab = StockIssueTabEnum.approvedProducts;
    this.stockIssueFacade.resetError();
    this.reqDocNumber = this.activatedRoute.snapshot.params['reqDocNo'];
    this.type = this.activatedRoute.snapshot.params['type'];
    if (this.type === StockIssueTypesEnum.FAC_BTQ) {
      this.issueType = 'BTQ_FAC';
      this.requestType = 'FAC';
    } else if (this.type === StockIssueTypesEnum.BTQ_BTQ) {
      this.issueType = 'BTQ_BTQ';
      this.requestType = 'BTQ';
    } else if (this.type === StockIssueTypesEnum.BTQ_MER) {
      this.issueType = 'BTQ_BTQ';
      this.requestType = 'MER';
    } else {
      //todo
      this.router.navigate(['404']);
    }
    this.stockIssueFacade.loadProductCategories();
    this.stockIssueFacade.loadProductGroups();

    this.authFacade
      .getLocationCode()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.locationCode = data;
      });
    this.appSettingFacade
      .getStoreType()
      .pipe(takeUntil(this.destroy$))
      .subscribe((storeType: string) => {
        if (storeType) {
          this.storeType = storeType;
        }
      });
    this.appSettingFacade
      .getMaxSortLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxSortLimit = data;
      });
    this.appSettingFacade
      .getMaxFilterLimit()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.maxFilterLimit = data;
      });
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.showNotification();
    });

    this.shortcutService.commands
      .pipe(takeUntil(this.destroy$))
      .subscribe(command => this.shortcutEventHandler(command));

    this.stockIssueFacade
      .getProductCategories()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: Filter[]) => {
        if (data !== null) {
          const PRODUCT_CATEGORIES: { [key: string]: Filter[] } = {
            'Product Category': data
          };
          this.PRODUCT_CATEGORIES = PRODUCT_CATEGORIES;
        }
      });

    this.stockIssueFacade
      .getProductGroups()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: Filter[]) => {
        if (data !== null) {
          const PRODUCT_GROUP: { [key: string]: Filter[] } = {
            'Product Group': data
          };
          this.PRODUCT_GROUP = PRODUCT_GROUP;
        }
      });
    //app setting pqge size
    this.componentInit();

    this.showNotification();
  }

  /**
   * method to handle shortcut commands
   * @param command: shortcut command
   */
  shortcutEventHandler(command: Command) {
    if (command.name === SEARCH_SHORTCUT_KEY_F2) {
      if (this.searchRef) {
        this.searchRef.focus();
      }
    }
  }

  isL1L2Store(): boolean {
    return this.storeType === 'L1' || this.storeType === 'L2';
  }

  isL3Store(): boolean {
    return this.storeType === 'L3';
  }

  componentInit() {
    this.sortDialogService.DataSource = SORT_DATA;
    this.itemIds = [];
    this.stockIssueFacade.clearSortAndFilter();
    this.isLoadingSelectedIssue$ = this.stockIssueFacade.getIsLoadingSelectedIssue();
    // this.approvedItems$ = this.stockIssueFacade.getApprovedItems();
    // this.selectedItems$ = this.stockIssueFacade.getSelectedItems();
    this.items$ = this.stockIssueFacade.getItems();

    // this.items$ = this.stockIssueFacade.getItems();
    // this.items$.subscribe((data: any) => {
    //   console.log('items', data);
    this.employeeCodesResponse$ = this.issueTEPFacade.getEmployeeCodes();
    this.employeeDetailsResponse$ = this.issueTEPFacade.getEmployeeDetails();
    // });

    this.isItemsLoading$ = this.stockIssueFacade.getIsItemsLoading();
    this.isItemsLoaded$ = this.stockIssueFacade.getIsItemsLoaded();
    this.itemsCount$ = this.stockIssueFacade.getItemsCount();

    //total count

    this.stockIssueFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });

    this.stockIssueFacade
      .getCourierDetails()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        this.courierDetails = data;
      });

    this.items$.pipe(takeUntil(this.destroy$)).subscribe(items => {
      this.selectedIds = [];
      if (this.itemCode !== '') {
        this.selectionAllSubject.next({
          selectCheckbox: true,
          enableCheckbox: true
        });
      } else if (this.selectForm.value.selectRadioButton === '1') {
        this.selectionAllSubject.next({
          selectCheckbox: true,
          enableCheckbox: false
        });
      } else if (this.selectForm.value.selectRadioButton === '2') {
        this.selectionAllSubject.next({
          selectCheckbox: false,
          enableCheckbox: true
        });
        this.selectForm.patchValue({
          selectRadioButton: null
        });
      }
      items.forEach(item => {
        const index = this.selectedIds.indexOf(item.id);
        if (index === -1) {
          this.selectedIds.push(item.id);
        }
        // console.log("selected", this.selectedIds)
      });
      // console.log('selected', this.selectedIds);
      if (this.itemCode !== '') {
        this.itemIds = [...this.selectedIds];
      }

      this.showNotification();
    });
    if (this.storeType === 'L1' || this.storeType === 'L2') {
      this.stockIssueFacade.loadSelectedIssue({
        reqDocNo: this.reqDocNumber,
        transferType: this.issueType,
        requestType: this.requestType
      });
      this.stockIssueFacade
        .getSelectedIssue()
        .pipe(takeUntil(this.destroy$))
        .subscribe((requestStockTransferNote: StockRequestNote) => {
          //  check
          if (requestStockTransferNote) {
            this.selectedIssue = requestStockTransferNote;
            this.initialLoad();
          } else {
            //todo
            // this.router.navigate(['404']);
          }
        });
    } else {
      //todo
      this.router.navigate(['404']);
    }
    // this.isItemUpdating$ = this.stockIssueFacade.getIsItemUpdating();
    this.stockIssueFacade
      .getIsItemUpdating()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isUpdating: any) => {
        this.isItemUpdating = isUpdating;
        this.showNotification();
      });

    this.stockIssueFacade
      .getApprovedItemsTotalCount()
      .pipe(takeUntil(this.destroy$))
      .subscribe((count: number) => {
        this.approvedItemsTotalCount = count;
        // this.loadApprovedProducts();
        // this.showNotification();
      });
    this.stockIssueFacade
      .getSelectedItemsTotalCount()
      .pipe(takeUntil(this.destroy$))
      .subscribe((count: number) => {
        this.selectedItemsTotalCount = count;
        this.showNotification();
      });
    // this.itemsTotalCountLoaded$
    this.stockIssueFacade
      .getIssueItemsTotalCountLoaded()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isLoaded: boolean) => {
        if (isLoaded) {
          this.showNotification();
        }
      });

    this.stockIssueFacade
      .getfilterDataApprovedProducts()
      .pipe(takeUntil(this.destroy$))
      .subscribe(filterValue => {
        this.filterTabDataApprovedProducts = filterValue;
      });
    this.stockIssueFacade
      .getfilterDataSelectedProducts()
      .pipe(takeUntil(this.destroy$))
      .subscribe(filterValue => {
        this.filterTabDataSelectedProducts = filterValue;
      });

    this.stockIssueFacade
      .getSortDataApprovedProducts()
      .pipe(takeUntil(this.destroy$))
      .subscribe(sortValue => {
        this.sortTabDataApprovedProducts = sortValue;
      });
    this.stockIssueFacade
      .getSortDataSelectedProducts()
      .pipe(takeUntil(this.destroy$))
      .subscribe(sortValue => {
        this.sortTabDataSelectedProducts = sortValue;
      });
    this.issueTEPFacade.loadEmployeeCodes();
    this.employeeCodesResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data !== null) {
          this.employeeCodes = data;
        }
      });

    this.employeeDetailsResponse$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data !== null) {
          this.formList.controls[0].patchValue(
            {
              employeeName: data.empName,
              employeeMobileNumber: data.mobileNo
            },
            { eventEmit: false }
          );
        }
      });
  }

  initialLoad() {
    // this.selectedIds = [];
    // this.itemIds = [];
    this.clearSearchItems();
    this.clearAll();
    this.resetFilterSort();
    // this.loadTotalItemsCount();
    this.stockIssueFacade.loadCourierDetails(this.locationCode);
    this.loadProducts();
    this.showNotification();
  }
  loadTotalItemsCount() {
    this.stockIssueFacade.loadItemsTotalCount({
      id: this.selectedIssue.id,
      requestType: this.requestType,
      storeType: this.storeType
    });
  }
  searchItems(searchData: SearchResponse) {
    this.resetRadiobutton();
    this.itemCode = searchData.searchValue;
    this.lotNumber = searchData.lotNumber;
    this.overlayNotification.close();
    this.issueItemsPageEvent = this.initialPageEvent;
    this.loadProducts();
  }

  clearSearchItems() {
    this.itemIds = [];
    this.selectedIds = [];
    this.itemCode = '';
    this.lotNumber = '';
    if (this.searchRef) {
      this.searchRef.reset();
    }
    this.stockIssueFacade.clearSearchItems();
    this.loadProducts();
    this.showNotification();
  }

  changeType(newTab: StockIssueTabEnum) {
    if (this.tab !== newTab) {
      this.selectedIds = [];
      this.itemIds = [];
      // todo check above assigned $ this.shwNtification
      this.clearSearchItems();
      this.tab = newTab;
      this.clearAll();
      this.loadProducts();

      this.issueItemsPageEvent = this.initialPageEvent;
    }
  }

  updateItem(itemToUpdate: IssueItemToUpdate) {
    this.clearAll();
    this.stockIssueFacade.updateItem({
      requestType: this.requestType,
      storeType: this.storeType,
      id: this.selectedIssue.id,
      itemId: itemToUpdate.id,
      newUpdate: itemToUpdate.newUpdate,
      actualDetails: itemToUpdate.actualDetails
    });
    this.showNotification();
  }

  back(event) {
    this.router.navigate(['..'], { relativeTo: this.activatedRoute });
  }

  paginateIssueItems(event: PageEvent) {
    this.itemIds = [];
    // this.selectedIds = [];
    this.issueItemsPageEvent = event;
    this.loadProducts();
  }
  showNotification() {
    this.overlayNotification.close();
    if (this.tab === 0) {
      if (
        this.selectForm.value.selectRadioButton === '1' &&
        this.approvedItemsTotalCount > 0
      ) {
        this.saveProductsOverlay(
          '',
          'pw.stockIssueNotificationMessages.allItemsSelectionNotificationMsg'
        );
      } else if (
        this.selectForm.value.selectRadioButton === '2' &&
        this.itemIds.length > 0
      ) {
        this.saveProductsOverlay(
          '',
          'pw.stockIssueNotificationMessages.currentPageSelectionNotificationMsg'
        );
      } else if (this.itemIds.length > 0) {
        this.saveProductsOverlay(
          this.itemIds.length,
          'pw.stockIssueNotificationMessages.productsSelectedOverlayMessage'
        );
      }
    } else if (this.tab === 1) {
      if (this.selectedItemsTotalCount !== 0) {
        if (
          this.selectForm.value.selectRadioButton === '1' &&
          this.selectedItemsTotalCount > 0
        ) {
          this.deSelectProductsOverlay(
            this.selectedItemsTotalCount,
            'pw.stockIssueNotificationMessages.productsSelectedOverlayMessage'
          );
        } else if (
          this.selectForm.value.selectRadioButton === '2' ||
          this.itemIds.length > 0
        ) {
          this.deSelectProductsOverlay(
            this.itemIds.length,
            'pw.stockIssueNotificationMessages.productsSelectedOverlayMessage'
          );
        } else if (
          this.form.invalid ||
          ((this.selectedFormName === undefined ||
            this.selectedFormName === null) &&
            this.itemIds.length !== 0 &&
            !(
              this.selectForm.value.selectRadioButton === '1' ||
              this.selectForm.value.selectRadioButton === '2'
            ))
        ) {
          this.messageNotification(
            'pw.stockIssueNotificationMessages.enterCarrierDetailsMessage'
          );
        } else if (this.boxDetails_count < 1) {
          this.messageNotification(
            'pw.stockIssueNotificationMessages.enterBoxDetailsMesage'
          );
        } else if (this.itemCode !== '') {
          this.overlayNotification.close();
        } else if (this.itemCode === '') {
          if (
            this.itemIds.length === 0 &&
            this.form.valid &&
            !this.isDirty &&
            !this.isItemUpdating
          ) {
            this.confirmOverlay(
              'pw.stockIssueNotificationMessages.confirmOverlayMessage'
            );
          }
        }
      }
    }
  }
  selectionEmit(selection: { selected: boolean; id: string }) {
    if (selection.selected) {
      const index = this.itemIds.indexOf(selection.id);
      if (index === -1) {
        this.itemIds.push(selection.id);
      }
      if (this.selectedIds.length === this.itemIds.length) {
        this.selectForm.patchValue({
          selectRadioButton: '2'
        });
      }
    } else {
      const index = this.itemIds.indexOf(selection.id);
      if (index !== -1) {
        this.itemIds.splice(index, 1);
      }
      this.selectForm.patchValue({
        selectRadioButton: null
      });
    }
    // console.log("all", this.itemIds);
    // console.log("selected", this.selectedIds);
    this.showNotification();
  }
  selectChange() {
    if (this.selectForm.value.selectRadioButton === '1') {
      this.selectAll();
    } else if (this.selectForm.value.selectRadioButton === '2') {
      this.selectPagewise();
    } else {
      this.clearAll();
    }
  }
  selectAll() {
    this.itemIds = [];
    this.selectionAllSubject.next({
      selectCheckbox: true,
      enableCheckbox: false
    });
    this.showNotification();
  }
  selectPagewise() {
    this.itemIds = [];
    this.selectionAllSubject.next({
      selectCheckbox: true,
      enableCheckbox: true
    });
    this.itemIds = [...this.selectedIds];
    // console.log('all', this.itemIds);
    // console.log('selected', this.selectedIds);
    this.showNotification();
  }
  resetRadiobutton() {
    this.selectForm.patchValue({
      selectRadioButton: null
    });
  }
  clearAll() {
    this.itemIds = [];
    this.selectForm.patchValue({
      selectRadioButton: null
    });
    this.selectionAllSubject.next({
      selectCheckbox: false,
      enableCheckbox: true
    });
    this.showNotification();
  }

  onParentFormDirty(isDirty) {
    this.isDirty = isDirty;
    if (isDirty) {
      this.overlayNotification.close();
    } else {
      this.showNotification();
    }
    this.showNotification();
  }

  errorHandler(error: any) {
    if (error.code === ErrorEnums.ERR_INV_017) {
      this.overlayNotification.close();
      this.itemIds = [];
      for (const item of error.error.error.errorCause) {
        this.itemIds.push(item.itemId);
      }
      this.popup(error.error.error.errorCause);
    } else {
      this.overlayNotification
        .show({
          type: OverlayNotificationType.ERROR,
          hasClose: true,
          error: error,
          hasBackdrop:
            error.code === ErrorEnums.ERR_INV_029 ||
            error.code === ErrorEnums.ERR_INV_034 ||
            error.code === ErrorEnums.ERR_INV_034
        })
        .events.pipe(takeUntil(this.destroy$))
        .subscribe((event: OverlayNotificationEventRef) => {
          this.hasNotification = false;
        });
    }
    this.showNotification();
  }
  saveProductsOverlay(count: any, msg: any) {
    this.translate
      .get(msg)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            buttonText: 'SELECT PRODUCTS',
            message: count + ' ' + translatedMsg
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.showProgressNotification();

              this.stockIssueFacade.UpdateAllItems({
                requestType: this.requestType,
                storeType: this.storeType,
                id: this.selectedIssue.id,
                itemId: this.itemIds,
                status: 'SELECTED'
              });
              this.stockIssueFacade
                .getselectIsUpdateAllItemsSuccess()
                .pipe(takeUntil(this.destroy$))
                .subscribe((isSuccess: boolean) => {
                  this.onSuccessReloadComponent(isSuccess);
                });
            }
          });
      });
  }
  deSelectProductsOverlay(count: number, msg: string) {
    this.translate
      .get(msg)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            buttonText: 'REMOVE PRODUCTS',
            message: count + ' ' + translatedMsg
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.showProgressNotification();

              this.stockIssueFacade.UpdateAllItems({
                requestType: this.requestType,
                storeType: this.storeType,
                id: this.selectedIssue.id,
                itemId: this.itemIds,
                status: 'APPROVED'
              });
              this.stockIssueFacade
                .getselectIsUpdateAllItemsSuccess()
                .pipe(takeUntil(this.destroy$))
                .subscribe((isSuccess: boolean) => {
                  this.onSuccessReloadComponent(isSuccess);
                });
            }
          });
      });
  }

  confirmOverlay(msg: any) {
    this.overlayNotification.close();
    this.translate
      .get(msg)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.ACTION,
            buttonText: 'CONFIRM ISSUE',
            hasRemarks: true,
            isRemarksMandatory: true,
            message: translatedMsg
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            if (event.eventType === OverlayNotificationEventType.TRUE) {
              this.showProgressNotification();
              if (this.selectedFormName === 'HandCarry') {
                this.stockIssueFacade.confirmIssue({
                  requestType: this.requestType,
                  id: this.selectedIssue.id,
                  data: {
                    carrierDetails: {
                      data: {
                        employeeId: this.form.value.formCollection[0]
                          .employeeID,
                        employeeName: this.form.value.formCollection[0]
                          .employeeName,
                        designation: null,
                        emailId: null,
                        mobileNo: this.form.value.formCollection[0]
                          .employeeMobileNumber,
                        brand: null,
                        numberOfBoxes: '0',
                        boxDetails: []
                      },
                      // this.form.value.formCollection[0],

                      type: 'employee'
                    },
                    remarks: event.data
                  }
                });
              } else {
                this.stockIssueFacade.confirmIssue({
                  requestType: this.requestType,
                  id: this.selectedIssue.id,
                  data: {
                    carrierDetails: {
                      data: {
                        companyname: this.selectedFormName,
                        docketNumber: this.form.value.formCollection[0]
                          .courierDocketNumber,
                        lockNumber: this.form.value.formCollection[0]
                          .courierLockNumber,
                        roadPermitNumber: this.form.value.formCollection[0]
                          .courierRoadPermitNumber,
                        numberOfBoxes: this.boxDetails_count,
                        boxDetails: this.boxDetails_values
                      },
                      type: 'courier'
                    },
                    remarks: event.data
                  }
                });
              }
            }
          });
      });
    //todo handle error
    this.stockIssueFacade
      .getIssueConfirmStatus()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isSuccess: boolean) => {
        if (isSuccess === true) {
          this.stockIssueFacade
            .getConfirmationSrcDocNo()
            .subscribe((data: any) => {
              this.confirmationSrcDocNo = data;
              // this.hasNotification = true;
              this.notificationOverlay();
            });
          // } else if (isSuccess === false) {
          //   this.errorNotificationOverlay();
        }
      });
  }
  messageNotification(msg: any) {
    this.translate
      .get(msg)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.overlayNotification
          .show({
            type: OverlayNotificationType.SIMPLE,
            hasClose: true,
            message: translatedMsg
          })
          .events.pipe(takeUntil(this.destroy$));
      });
  }
  notificationOverlay() {
    this.hasNotification = true;
    this.overlayNotification
      .show({
        type: OverlayNotificationType.CUSTOM,
        hasClose: true,
        hasBackdrop: true,
        message:
          'All Products have been outwarded successfully and return STN number is ' +
          this.confirmationSrcDocNo,
        template: this.confirmSuccessNotificationTemplate
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        this.hasNotification = false;
        if (event.eventType === OverlayNotificationEventType.CLOSE) {
          // this.router.navigate(['inventory/stockissue/']);
          this.router.navigate(['..'], { relativeTo: this.activatedRoute });
        }
      });
  }
  showProgressNotification() {
    const key = 'pw.stockReceiveNotificationMessages.progressMessage';
    this.translate
      .get(key)
      .pipe(takeUntil(this.destroy$))
      .subscribe((translatedMsg: string) => {
        this.hasNotification = true;
        this.overlayNotification
          .show({
            type: OverlayNotificationType.PROGRESS,
            message: translatedMsg,
            hasBackdrop: true
          })
          .events.pipe(takeUntil(this.destroy$))
          .subscribe((event: OverlayNotificationEventRef) => {
            this.hasNotification = false;
          });
      });
  }
  errorNotificationOverlay() {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.SIMPLE,
        hasClose: true,
        hasBackdrop: true,
        message: 'Confirmation Failed. Try Confirm Again'
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {
        this.hasNotification = false;
        if (event.eventType === OverlayNotificationEventType.CLOSE) {
          this.router.navigate(['inventory/stockissue']);
        }
      });
  }
  createcourierForm(): FormGroup {
    return this.fb.group({
      courierDocketNumber: [
        '',
        Validators.compose([Validators.required, Validators.maxLength(30)])
      ],
      // courierLockNumber: [
      //   '',
      //   Validators.compose([Validators.required, Validators.maxLength(30)])
      // ],
      courierRoadPermitNumber: [
        '',
        Validators.compose([Validators.required, Validators.maxLength(30)])
      ]
    });
  }
  createemployeeForm(): FormGroup {
    return this.fb.group({
      employeeID: [
        '',
        Validators.compose([Validators.required, Validators.maxLength(30)])
      ],
      employeeName: [
        '',
        Validators.compose([Validators.required, Validators.maxLength(30)])
      ],
      employeeMobileNumber: [
        '',
        Validators.compose([
          Validators.required,
          Validators.maxLength(10),
          Validators.minLength(10),
          Validators.pattern('[0-9]+')
        ])
      ]
    });
  }
  loadForm(event) {
    this.formList.clear();
    this.selectedFormName = event.value;
    if (event.value !== 'HAND CARRY') {
      this.formList.push(this.createcourierForm());
    } else {
      this.formList.push(this.createemployeeForm());
    }
  }
  ngOnDestroy() {
    this.formList.clear();
    this.overlayNotification.close();
    this.destroy$.next();
    this.destroy$.complete();
  }
  popup(errorCause: any) {
    this.overlayNotification.close();
    const dialogRef = this.dialog.open(OutOfStockPopupComponent, {
      width: '75vw',
      data: errorCause
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => {
        if (res) {
          // console.log('res', res);
          this.removeOutOfStockItems();
        }
      });
  }
  removeOutOfStockItems() {
    this.showProgressNotification();

    this.stockIssueFacade.UpdateAllItems({
      requestType: this.requestType,
      storeType: this.storeType,
      id: this.selectedIssue.id,
      itemId: this.itemIds,
      status: 'APPROVED'
    });
    this.stockIssueFacade
      .getselectIsUpdateAllItemsSuccess()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isSuccess: boolean) => {
        this.onSuccessReloadComponent(isSuccess);
      });
  }
  openSort(): void {
    // this.sortDialogService.DataSource = SORT_DATA;
    let sortTabData: Column[] = [];
    if (this.tab === StockIssueTabEnum.approvedProducts) {
      sortTabData = this.sortTabDataApprovedProducts;
    } else if (this.tab === StockIssueTabEnum.selectedProducts) {
      sortTabData = this.sortTabDataSelectedProducts;
    }
    this.sortDialogService
      .openDialog(this.maxSortLimit, sortTabData)
      .pipe(takeUntil(this.destroy$))
      .subscribe((sortResult: { data: Column[]; actionfrom: string }) => {
        if (sortResult.actionfrom === FilterActions.APPLY) {
          if (this.tab === StockIssueTabEnum.approvedProducts) {
            this.stockIssueFacade.setIssueApprovedProductsSort(sortResult.data);
            this.sortMapApprovedProducts.clear();
            const sortData = sortResult.data;
            if (sortData === null || sortData.length === 0) {
              this.sortData = [];
              this.sortOrder = null;
              this.sortBy = null;
            } else {
              this.sortData = sortData;
              if (sortData.length > 0) {
                if (sortData[0].id === 0) {
                  this.sortBy = 'requestedWeight';
                } else if (sortData[0].id === 1) {
                  this.sortBy = 'approvedQuantity';
                }
                this.sortOrder = sortData[0].sortAscOrder ? 'ASC' : 'DESC';
              }
            }
            if (this.sortBy != null && this.sortOrder !== null) {
              this.sortMapApprovedProducts.set(
                'sort',
                this.sortBy + ',' + this.sortOrder
              );
            }
            this.issueItemsPageEvent = this.initialPageEvent;
          } else if (this.tab === StockIssueTabEnum.selectedProducts) {
            this.stockIssueFacade.setIssueSelectedProductsSort(sortResult.data);
            this.sortMapSelectedProducts.clear();
            const sortData = sortResult.data;
            if (sortData == null || sortData.length === 0) {
              this.sortData = [];
              this.sortOrder = null;
              this.sortByMap = null;
            } else {
              this.sortData = sortData;
              if (sortData.length > 0) {
                if (sortData[0].id === 0) {
                  this.sortBy = 'requestedWeight';
                } else if (sortData[0].id === 1) {
                  this.sortBy = 'approvedQuantity';
                }
                this.sortOrder = sortData[0].sortAscOrder ? 'ASC' : 'DESC';
              }
            }
            if (this.sortBy !== null && this.sortOrder != null) {
              this.sortMapSelectedProducts.set(
                'sort',
                this.sortBy + ',' + this.sortOrder
              );
            }
            this.issueItemsPageEvent = this.initialPageEvent;
          }

          this.loadProducts();
          // todo clear all
        }
      });
  }
  loadProducts() {
    this.overlayNotification.close();
    this.itemIds = [];
    if (this.storeType && this.selectedIssue) {
      if (this.tab === StockIssueTabEnum.approvedProducts) {
        this.stockIssueFacade.loadItems({
          id: this.selectedIssue.id,
          itemCode: this.itemCode,
          lotNumber: this.lotNumber,
          requestType: this.requestType,
          storeType: this.storeType,
          status: 'APPROVED',
          pageIndex: this.issueItemsPageEvent.pageIndex,
          pageSize: this.issueItemsPageEvent.pageSize,
          sort: this.sortMapApprovedProducts,
          filter: this.filterApprovedProducts
        });
      } else {
        this.stockIssueFacade.loadItems({
          id: this.selectedIssue.id,
          itemCode: this.itemCode,
          lotNumber: this.lotNumber,
          requestType: this.requestType,
          storeType: this.storeType,
          status: 'SELECTED',
          pageIndex: this.issueItemsPageEvent.pageIndex,
          pageSize: this.issueItemsPageEvent.pageSize,
          sort: this.sortMapSelectedProducts,
          filter: this.filterSelectedProducts
        });
      }
      this.loadTotalItemsCount();
      this.showNotification();
    }
  }
  openFilter(): void {
    this.filterService.DataSource = {
      ...this.PRODUCT_CATEGORIES,
      ...this.PRODUCT_GROUP
    };
    let filterTabData: { [key: string]: Filter[] } = {};
    if (this.tab === StockIssueTabEnum.approvedProducts) {
      filterTabData = this.filterTabDataApprovedProducts;
    } else if (this.tab === StockIssueTabEnum.selectedProducts) {
      filterTabData = this.filterTabDataSelectedProducts;
    }
    this.filterService
      .openDialog(this.maxFilterLimit, filterTabData)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (filterResult: {
          data: { [key: string]: Filter[] };
          actionfrom: string;
        }) => {
          if (filterResult.actionfrom === FilterActions.APPLY) {
            if (this.tab === StockIssueTabEnum.approvedProducts) {
              this.stockIssueFacade.setIssueAppovedProductsFilter(
                filterResult.data
              );
              const filterData = filterResult.data;
              if (filterData == null) {
                this.filterData = {};
              } else {
                this.filterData = filterData;
              }
              this.filterApprovedProducts = [];
              if (filterData) {
                let filterValues = [];
                if (filterData['Product Group']) {
                  filterData['Product Group'].forEach(value => {
                    filterValues.push(value.id);
                  });
                  if (filterValues.length > 0) {
                    this.filterApprovedProducts.push({
                      key: 'productGroup',
                      value: filterValues
                    });
                  }
                }
                filterValues = [];
                if (filterData['Product Category']) {
                  filterData['Product Category'].forEach(value => {
                    filterValues.push(value.id);
                  });
                  if (filterValues.length > 0) {
                    this.filterApprovedProducts.push({
                      key: 'productCategory',
                      value: filterValues
                    });
                  }
                }
              }
              this.issueItemsPageEvent = this.initialPageEvent;
            } else if (this.tab === StockIssueTabEnum.selectedProducts) {
              this.stockIssueFacade.setIssueSelectedProductsFilter(
                filterResult.data
              );
              const filterData = filterResult.data;
              if (filterData == null) {
                this.filterData = {};
              } else {
                this.filterData = filterData;
              }
              this.filterSelectedProducts = [];
              if (filterData) {
                let filterValues = [];
                if (filterData['Product Group']) {
                  filterData['Product Group'].forEach(value => {
                    filterValues.push(value.id);
                  });
                  if (filterValues.length > 0) {
                    this.filterSelectedProducts.push({
                      key: 'productGroup',
                      value: filterValues
                    });
                  }
                }
                filterValues = [];
                if (filterData['Product Category']) {
                  filterData['Product Category'].forEach(value => {
                    filterValues.push(value.id);
                  });
                  if (filterValues.length > 0) {
                    this.filterSelectedProducts.push({
                      key: 'productCategory',
                      value: filterValues
                    });
                  }
                }
              }
              this.issueItemsPageEvent = this.initialPageEvent;
            }
            this.loadProducts();
            // this.clearAll();
          }
        }
      );
  }
  resetFilterSort() {
    this.filterByMap.clear();
    this.sortByMap.clear();
  }
  onSuccessReloadComponent(isSuccess: boolean) {
    this.overlayNotification.close();
    if (isSuccess) {
      this.initialLoad();
    } else {
      // TODO: Custom error
    }
  }
  popMeUp() {
    this.overlayNotification.close();
    if (!this.boxDetails_values) {
      this.boxDetails_values = [];
    }
    const dialogRef = this.dialog.open(CourierDetailsPopupComponent, {
      width: '60vw',
      data: this.boxDetails_values
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => {
        if (res) {
          this.boxDetails_count = res.count;
          this.boxDetails_values = res.value;
          console.log('values', this.boxDetails_values);

          this.showNotification();
        }
      });
  }

  employeeIDSelected(event: any, employeeCode: string) {
    if (event.isUserInput) {
      this.isReadOnly = true;
      if (this.employeeCodes.includes(employeeCode)) {
        this.formList.controls[0].patchValue({
          employeeID: employeeCode
        });
        this.issueTEPFacade.loadEmployeeDetails(employeeCode);
      }
    }
  }

  employeeIDChange(value) {
    const employeeInput = value;
    if (this.employeeCodes.includes(employeeInput)) {
      this.isReadOnly = true;
      this.issueTEPFacade.loadEmployeeDetails(employeeInput);
    } else {
      this.isReadOnly = false;
      this.formList.controls[0].patchValue({
        employeeName: '',
        employeeMobileNumber: ''
      });
    }
  }
}
