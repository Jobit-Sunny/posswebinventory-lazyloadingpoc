import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  AfterViewInit
} from '@angular/core';
import {
  StockRequestNote,
  RequestInvoice
} from '../../models/stock-issue-models';
import { Observable, Subject, fromEvent } from 'rxjs';
import { StockIssueFacade } from '../../+state/stock-issue.facade';
import { Router, ActivatedRoute } from '@angular/router';
import { takeUntil, debounceTime } from 'rxjs/operators';
import { AppsettingFacade, CustomErrors } from '@poss-web/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { StockIssueTypesEnum } from '../../models/stock-issue.enum';
import { TranslateService } from '@ngx-translate/core';
import {
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef
} from '@poss-web/shared';
import { getStockIssueTEPGEPRouteUrl } from '../../../../page-route.constants';
import { StockIssueTEPGEPTypesEnum } from '../../../stock-issue-tep-gep/models/stock-issue-tep-gep.enum';
import { ReturnInvoiceFacade } from '../../../return-invoice-cfa/+state/return-invoice-cfa.facade';

@Component({
  selector: 'poss-web-stock-issue-list',
  templateUrl: './stock-issue-list.component.html',
  styleUrls: ['./stock-issue-list.component.scss']
})
export class StockIssueListComponent
  implements OnInit, OnDestroy, AfterViewInit {
  storeType: string;

  isLoadingCount$: Observable<boolean>;
  BTQ_FAC_IssuePendingSTNCount$: Observable<number>;
  BTQ_BTQ_IssuePendingSTNCount$: Observable<number>;
  BTQ_MER_IssuePendingSTNCount$: Observable<number>;
  allPendingIssuePendingSTNCount$: Observable<number>;

  BTQ_FAC_PendingSTN$: Observable<StockRequestNote[]>;
  BTQ_BTQ_PendingSTN$: Observable<StockRequestNote[]>;
  BTQ_MER_PendingSTN$: Observable<StockRequestNote[]>;
  searchIssueSTNResult$: Observable<StockRequestNote[]>;

  isLoadingBTQ_FAC_STN: boolean;
  isLoadingBTQ_BTQ_STN: boolean;
  isLoadingBTQ_MER_STN: boolean;

  isSearchingIssues$: Observable<boolean>;
  hasSearchIssueResults$: Observable<boolean>;

  isPendingBTQ_FACSTNLoadedOnce = false;
  isPendingBTQ_BTQSTNLoadedOnce = false;
  isPendingBTQ_MERSTNLoadedOnce = false;

  BTQ_CFAInvoice$: Observable<RequestInvoice[]>;
  BTQ_CFAInvoiceCount$: Observable<number>;
  searchIssueInvoiceResult$: Observable<RequestInvoice[]>;

  isLoadingBTQ_CFAInvoice: boolean;
  isSearchingInvoices$: Observable<boolean>;
  hasSearchInvoiceResults$: Observable<boolean>;
  isPendingCFAInvoiceLoadedOnce = false;

  error$: Observable<CustomErrors>;

  isLoading = false;
  pageSize = 4;
  initialPageSize = 8;

  type: StockIssueTypesEnum;
  destroy$: Subject<null> = new Subject<null>();

  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;
  searchForm: FormGroup;

  transferTypeForm: FormGroup;
  requestTypeForm: FormGroup;

  stockIssueTEPGEPTypesEnumRef = StockIssueTEPGEPTypesEnum;

  constructor(
    private returnInvoiceFacade: ReturnInvoiceFacade,
    private stockIssueFacade: StockIssueFacade,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private appsettingFacade: AppsettingFacade,
    private formBuilder: FormBuilder,
    private overlayNotification: OverlayNotificationService
  ) {
    this.searchForm = this.formBuilder.group({
      searchValue: []
    });
    this.transferTypeForm = this.formBuilder.group({
      selectedTransferType: ''
    });
    this.requestTypeForm = this.formBuilder.group({
      selectedRequestType: ''
    });
  }

  ngOnInit() {
    this.requestTypeForm
      .get('selectedRequestType')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(type => {
        this.changeStockIssueType(type);
      });
    this.stockIssueFacade.resetError();
    this.appsettingFacade
      .getStoreType()
      .pipe(takeUntil(this.destroy$))
      .subscribe((storeType: string) => {
        if (storeType) {
          this.storeType = storeType;
          this.componentInit();
        }
      });
  }

  ngAfterViewInit(): void {
    /**
     * Event Handler for search Box input with debounce
     */
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe((event: any) => {
        const searchValue = this.searchForm.get('searchValue').value;
        if (searchValue !== '' && !isNaN(searchValue)) {
          this.searchIssues(searchValue);
        } else if (searchValue === '') {
          this.clearSearch();
        } else if (isNaN(searchValue)) {
          this.searchForm.get('searchValue').setErrors({
            nan: 'pw.stockIssue.searchNotaNumberErrorMessage'
          });
          this.searchForm.get('searchValue').markAsTouched();
        }
      });
  }

  newIssueToCFA() {
    this.returnInvoiceFacade.clearCart();
    this.returnInvoiceFacade.clearSearch();
    this.returnInvoiceFacade.CreateRequestToCfa();
    this.returnInvoiceFacade
      .getNewRequestId()
      .pipe(takeUntil(this.destroy$))
      .subscribe((id: number) => {
        if (id) {
          // console.log(id);
          this.router.navigate([id], {
            relativeTo: this.activatedRoute
          });
        }
      });
  }

  isL1L2Store(): boolean {
    return this.storeType === 'L1' || this.storeType === 'L2';
  }

  isL3Store(): boolean {
    return this.storeType === 'L3';
  }

  componentInit() {
    this.stockIssueFacade
      .getError()
      .pipe(takeUntil(this.destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error);
        }
      });
    this.stockIssueFacade.resetStockIssueList();

    this.isLoadingCount$ = this.stockIssueFacade.getIsLoadingCount();
    this.isSearchingIssues$ = this.stockIssueFacade.getIsSearchingIssues();
    this.hasSearchIssueResults$ = this.stockIssueFacade.getHasSearchIssueResults();

    this.hasSearchInvoiceResults$ = this.stockIssueFacade.getHasSearchInvoiceResults();
    this.searchIssueSTNResult$ = this.stockIssueFacade.getSearchIssueResult();

    this.stockIssueFacade
      .getisLoadingIssueToFactory()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isLoading: boolean) => {
        this.isLoadingBTQ_FAC_STN = isLoading;
      });
    this.stockIssueFacade
      .getisLoadingIssueToBoutique()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isLoading: boolean) => {
        this.isLoadingBTQ_BTQ_STN = isLoading;
      });
    this.stockIssueFacade
      .getIsLoadingIssueToMerchant()
      .pipe(takeUntil(this.destroy$))
      .subscribe((isLoading: boolean) => {
        this.isLoadingBTQ_MER_STN = isLoading;
      });
    // this.stockIssueFacade
    //   .getisLoadingIssueToCFA()
    //   .pipe(takeUntil(this.destroy$))
    //   .subscribe((isLoading: boolean) => {
    //     this.isLoadingBTQ_CFAInvoice = isLoading;
    //   });

    this.type = this.activatedRoute.snapshot.params['type'];
    if (this.type === 'merchandise') {
      this.requestTypeForm.patchValue({
        selectedRequestType: 'merchandise'
      });
    } else if (this.type === 'boutique') {
      this.requestTypeForm.patchValue({ selectedRequestType: 'boutique' });
    }
    this.getIssues();
    this.changeStockIssueType(this.type);
    this.loadStocks(0);
  }

  getIssues() {
    if (this.isL1L2Store()) {
      this.stockIssueFacade.LoadIssueSTNCount();
      this.BTQ_BTQ_IssuePendingSTNCount$ = this.stockIssueFacade.getPendingBTQ_BTQ_STNCount();
      this.BTQ_FAC_IssuePendingSTNCount$ = this.stockIssueFacade.getPendingBTQ_FAC_STNCount();
      this.BTQ_MER_IssuePendingSTNCount$ = this.stockIssueFacade.getPendingBTQ_MER_STNCount();

      this.BTQ_FAC_PendingSTN$ = this.stockIssueFacade.getBTQ_FAC_PendingSTN();
      this.BTQ_FAC_PendingSTN$.pipe(takeUntil(this.destroy$)).subscribe(
        (issues: StockRequestNote[]) => {
          if (
            issues &&
            issues.length !== 0 &&
            !this.isPendingBTQ_FACSTNLoadedOnce
          ) {
            this.isPendingBTQ_FACSTNLoadedOnce = true;
          }
        }
      );
      this.BTQ_BTQ_PendingSTN$ = this.stockIssueFacade.getBTQ_BTQ_PendingSTN();
      this.BTQ_BTQ_PendingSTN$.pipe(takeUntil(this.destroy$)).subscribe(
        (issues: StockRequestNote[]) => {
          if (
            issues &&
            issues.length !== 0 &&
            !this.isPendingBTQ_BTQSTNLoadedOnce
          ) {
            this.isPendingBTQ_BTQSTNLoadedOnce = true;
          }
        }
      );
      this.BTQ_MER_PendingSTN$ = this.stockIssueFacade.getBTQ_MER_PendingSTN();
      this.BTQ_MER_PendingSTN$.pipe(takeUntil(this.destroy$)).subscribe(
        (issues: StockRequestNote[]) => {
          if (
            issues &&
            issues.length !== 0 &&
            !this.isPendingBTQ_MERSTNLoadedOnce
          ) {
            this.isPendingBTQ_MERSTNLoadedOnce = true;
          }
        }
      );
    }
  }

  back() {
    this.router.navigate(['..'], { relativeTo: this.activatedRoute });
  }

  changeStockIssueType(newType: StockIssueTypesEnum) {
    if (this.type !== newType) {
      this.clearSearch();
      this.router.navigate(['..', newType], {
        relativeTo: this.activatedRoute
      });
      this.type = newType;
      if (
        (this.type === StockIssueTypesEnum.FAC_BTQ &&
          !this.isPendingBTQ_FACSTNLoadedOnce) ||
        (this.type === StockIssueTypesEnum.BTQ_BTQ &&
          !this.isPendingBTQ_BTQSTNLoadedOnce) ||
        (this.type === StockIssueTypesEnum.BTQ_MER &&
          !this.isPendingBTQ_MERSTNLoadedOnce) ||
        (this.type === StockIssueTypesEnum.BTQ_CFA &&
          !this.isPendingCFAInvoiceLoadedOnce)
      ) {
        this.loadStocks(0);
      }
      if (this.type === 'merchandise') {
        this.requestTypeForm.patchValue({
          selectedRequestType: 'merchandise'
        });
      } else if (this.type === 'boutique') {
        this.requestTypeForm.patchValue({
          selectedRequestType: 'boutique'
        });
      }
    }
  }

  searchIssues(reqDocNo: number) {
    let type: string;
    let requestType: string;

    if (this.type === StockIssueTypesEnum.FAC_BTQ) {
      type = 'BTQ_FAC';
      requestType = 'FAC';
    } else if (this.type === StockIssueTypesEnum.BTQ_BTQ) {
      type = 'BTQ_BTQ';
      requestType = 'BTQ';
    } else if (this.type === StockIssueTypesEnum.BTQ_MER) {
      type = 'BTQ_BTQ';
      requestType = 'MER';
    }
    if (this.isL1L2Store()) {
      this.stockIssueFacade.searchPendingIssues({
        reqDocNo: reqDocNo,
        type: type,
        requestType: requestType
      });
    }
  }

  loadStocks(pageIndex) {
    if (!this.isLoading) {
      if (this.type === StockIssueTypesEnum.FAC_BTQ) {
        this.stockIssueFacade.loadIssueToFactorySTN({
          transferType: 'BTQ_FAC',
          requestType: 'FAC',
          pageIndex: pageIndex,
          pageSize: this.isPendingBTQ_FACSTNLoadedOnce
            ? this.pageSize
            : this.initialPageSize
        });
      } else if (this.type === StockIssueTypesEnum.BTQ_MER) {
        this.stockIssueFacade.loadIssueToMerchantSTN({
          transferType: 'BTQ_BTQ',
          requestType: 'MER',
          pageIndex: pageIndex,
          pageSize: this.isPendingBTQ_MERSTNLoadedOnce
            ? this.pageSize
            : this.initialPageSize
        });
      } else if (this.type === StockIssueTypesEnum.BTQ_BTQ) {
        this.stockIssueFacade.loadIssueToBoutiqueSTN({
          transferType: 'BTQ_BTQ',
          requestType: 'BTQ',
          pageIndex: pageIndex,
          pageSize: this.isPendingBTQ_BTQSTNLoadedOnce
            ? this.pageSize
            : this.initialPageSize
        });
      }
    }
  }
  onSelected(issue: any) {
    if (this.isL1L2Store()) {
      this.router.navigate([issue.reqDocNo], {
        relativeTo: this.activatedRoute
      });
    } else if (this.isL3Store()) {
      this.router.navigate([issue.id], {
        relativeTo: this.activatedRoute
      });
    } else {
      // TODO : throw an error
    }
    //
  }

  /**
   * method to call the transfer type
   * @param selectedTransferType : type selected from dropdown
   */
  createStockIssueTransferType() {
    const selectedTransferType = this.transferTypeForm.value
      .selectedTransferType;
    if (selectedTransferType === 'NEW_ISSUE_TO_CFA') {
      this.newIssueToCFA();
    } else {
      this.getIssueTEPGEPUrl(selectedTransferType);
    }
  }

  getIssueTEPGEPUrl(selectedTransferType) {
    const type = this.activatedRoute.snapshot.params['type'];
    this.router.navigate([
      getStockIssueTEPGEPRouteUrl(type, selectedTransferType)
    ]);
  }

  clearSearch() {
    this.searchForm.reset();
    this.stockIssueFacade.searchClear();
  }
  errorHandler(error: CustomErrors) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error
      })
      .events.pipe(takeUntil(this.destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {});
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
