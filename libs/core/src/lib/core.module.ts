import {
  NgModule,
  ModuleWithProviders,
  Optional,
  SkipSelf,
  ErrorHandler,
  APP_INITIALIZER
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { ApiService } from './http/api.service';
import { AppErrorHandler } from './error-handler/app-error-handler.service';
import { NotificationService } from './notification/notification.service';
import { HttpErrorInterceptor } from './http/http-error-interceptor';

import { CustomSerializer } from './router-state.serializer';

import { AppsettingFacade } from './app-setting/appsetting.facade';
import { RouterStateSerializer } from '@ngrx/router-store';
import { HotkeyModule } from 'angular2-hotkeys';
import { ShortcutService } from './hot-keys/shortcut.service';
import { BarcodeService } from './barcode-reader/barcode-reader.service';
import { AppConfigService } from './config/app-config.service';
import { JwtInterceptor } from './auth/jwt.interceptor';
import { AuthFacade } from './auth/+state/auth.facade';
import { LocationMappingFacade } from './location-mapping/+state/location-mapping.facade';
import { LocationMappingService } from './location-mapping/location-mapping.service';
import { EffectsModule } from '@ngrx/effects';
//import { fakeBackendProvider } from './auth/fake-backend.service';

/** [TODO] : Need to check isDevMode vs Environment */
// @NgModule({
//   imports: [
//     CommonModule,
//     HttpClientModule,
//     NxModule.forRoot(),
//     StoreModule.forRoot(reducers, {
//       metaReducers: isDevMode() ? [debug] : [],
//       runtimeChecks: {
//         strictStateImmutability: true,
//         strictActionImmutability: true
//       }
//     }),
//     //    EffectsModule.forRoot(),
//     isDevMode()
// //      ? StoreDevtoolsModule.instrument({
//           maxAge: 25,
//           logOnly: isDevMode()
//         })
//       : []
//   ],
//   providers: []
// })

export function initApp(appConfigService: AppConfigService) {
  console.log('initsetting');

  return () => appConfigService.loadAppSetting();
}
@NgModule({
  imports: [CommonModule, HttpClientModule, HotkeyModule.forRoot()],
  providers: [LocationMappingService, LocationMappingFacade],
  exports: []
})
export class CoreModule {
  /**
   * Use this to import module in root application only once
   */
  static forRoot(environment: any): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        AppConfigService,
        {
          provide: 'env',
          useValue: environment
        },
        { provide: RouterStateSerializer, useClass: CustomSerializer },
        ApiService,
        { provide: ErrorHandler, useClass: AppErrorHandler },
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        {
          provide: APP_INITIALIZER,
          useFactory: initApp,
          deps: [AppConfigService],
          multi: true
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: HttpErrorInterceptor,
          multi: true
        },
        //  fakeBackendProvider,
        NotificationService,
        ShortcutService,
        BarcodeService,
        AppsettingFacade,
        AuthFacade
      ]
    };
  }

  constructor(
    @Optional()
    @SkipSelf()
    parentModule: CoreModule
  ) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import only in AppModule');
    }
  }
}
