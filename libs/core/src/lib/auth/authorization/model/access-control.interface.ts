export default interface AccessControl {
  ID: number
  code: string
  name: string
  group: string
}
