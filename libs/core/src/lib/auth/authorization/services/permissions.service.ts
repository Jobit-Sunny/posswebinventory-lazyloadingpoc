import { Injectable } from '@angular/core';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { AuthFacade } from '../../+state/auth.facade';

@Injectable({
  providedIn: 'root'
})
export class PermissionsService {
  constructor(private authfacade: AuthFacade) {}

  hasPermission(key: string): Observable<boolean> {
    return this.authfacade.getACL().pipe(
      take(1),
      map(aclmap =>
        key.length === 1
          ? aclmap.has(key)
          : aclmap.has(key.charAt(0))
          ? aclmap.get(key.charAt(0)).includes(key)
          : false
      )
    );
  }

  getACL(): Observable<Map<string, string[]>> {
    return this.authfacade.getACL();
  }
}
