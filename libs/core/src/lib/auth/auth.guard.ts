import { Injectable, OnDestroy } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { take, map, skipWhile, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { AuthFacade } from './+state/auth.facade';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate, OnDestroy {
  destroy$: Subject<null> = new Subject<null>();
  refreshTokenExp: Date;

  constructor(private authfacade: AuthFacade, private router: Router) {
    authfacade
      .getRefreshTokenExp()
      .pipe(
        skipWhile(time => !time),
        takeUntil(this.destroy$)
      )
      .subscribe(time => (this.refreshTokenExp = time));
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.authfacade.getAccessToken().pipe(
      take(1),
      map(accessToken => {
        if (accessToken) {
          if (
            this.refreshTokenExp &&
            this.refreshTokenExp.getTime() < new Date().getTime()
          ) {
            this.authfacade.logOut();
            return false;
          }
          return true;
        } else {
          this.authfacade.reload(state.url);
          return false;
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
