import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

import { User } from '../model/user.model';
import { ApiService } from '../http/api.service';
import { AppsettingFacade } from '../app-setting/appsetting.facade';
import { JwtHelper } from './jwt.helper';
import { Token } from '../model/token.model';
import {
  getLoginEndpointUrl,
  getLogoutEndpointUrl,
  getReloadEndpointUrl,
  getRefreshEndpointUrl
} from './auth-endpoints.constants';
import ACLEncoder from './Authorization/ACLEncoder';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(
    private api: ApiService,
    private appSettingFacade: AppsettingFacade,
    @Inject('env') private env
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem('currentUser'))
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string) {
    return this.evaluateCredentialsBeta(username, password);
  }

  // TODO: to be removed in future release
  // getStoreType(userData: Token) {
  //   if (userData.upn === 'L1User') {
  //     return 'L1';
  //   } else if (userData.upn === 'L3User') {
  //     return 'L3';
  //   } else if (userData.upn === 'Corp') {
  //     return 'ORG';
  //   } else {
  //     return 'NONE';
  //   }
  // }

  // evaluateCredentials(username: string, password: string) {
  //   return this.api.post(loginEndpointURL).pipe(
  //     catchError(val => throwError(of(val.error)))
  //   );
  // }

  evaluateCredentialsBeta(username: string, password: string) {
    const loginEndpointURL = getLoginEndpointUrl(this.env);
    this.api.AuthorizationHeader = window.btoa(password);
    this.api.FromHeader = username;
    return this.api.post(loginEndpointURL).pipe(
      map(token => this.getUser(token, password))
      // catchError(val => throwError(of(val.error)))
    );

    // return this.api.get(getActiveAccessTokenEndpointUrl(this.env)).pipe(
    //   map(key => {
    //     password = this.cryptoService.encryptPassword(key, password);
    //     this.api.AuthorizationHeader = window.btoa(password);
    //   }),
    //   switchMap(key =>
    //     this.api.post(loginEndpointURL).pipe(
    //       map(token => this.getUser(token, password))
    //       // catchError(val => throwError(of(val.error)))
    //     )
    //   )
    // );
  }

  logout(): Observable<void> {
    const logoutEndpointURL = getLogoutEndpointUrl(this.env);
    return this.api.delete(logoutEndpointURL).pipe(
      map(_ => {
        this.appSettingFacade.changeStoreType('');
        this.appSettingFacade.changeLoginStatus('LOGGED_OUT');
      })
      // catchError(err =>{
      //   this.appSettingFacade.changeStoreType('');
      //   this.appSettingFacade.changeLoginStatus('LOGGED_OUT');
      //   throw(err);
      // })
    );
  }

  reload(): Observable<any> {
    // return of(this.getUser({}, ''));
    return this.api
      .get(getReloadEndpointUrl(this.env))
      .pipe(map(token => this.getUser(token, '')));
  }

  refresh(refreshtoken: string): Observable<any> {
    this.api.ContentTypeHeader = 'application/x-www-form-urlencoded';
    return this.api
      .post(getRefreshEndpointUrl(this.env, refreshtoken))
      .pipe(map(token => this.getUser(token, '')));
  }

  getUser(token: any, password: string): User {
    // token.accessToken =
    // 'eyJhbGciOiJIUzI1NiJ9.eyJsb2MiOiJUSiIsIm1vYiI6Ijg2MDg0NTU4NDMiLCJhcGlLZXkiOmZhbHNlLCJvcmciOiJUSiIsImlzcyI6InBvc3MudGFuaXNocS5jby5pbiIsImFjbCI6WyJVLXFyc0EiLCJJLUFBQUFBQVkiLCJNLUJBUUFBQUFBQUFCLy9nIl0sInR5cGUiOiJPUkciLCJhdWQiOiJlYzItMTMtMjM0LTEyNC0yNTAuYXAtc291dGgtMS5jb21wdXRlLmFtYXpvbmF3cy5jb20iLCJ1cG4iOiJhZG1pbiIsIm5iZiI6MTU3NjE2MzYzOCwiZXhwIjoxNTc2MTY3MjM4LCJpYXQiOjE1NzYxNjM2MzgsImVtYWlsIjoiYWRtaW5AbWluZHRyZWUuY29tIn0.UgCgoD6QnCNBBDVRZp8-l1i48qEFIoMWxnWjiOMe7L4';
    // CORP/ADMIN

    // 'eyJhbGciOiJIUzI1NiJ9.eyJsb2MiOiJVUkIiLCJtb2IiOiI4NjA4NDU1ODQ1IiwiYXBpS2V5IjpmYWxzZSwib3JnIjoiVEoiLCJpc3MiOiJwb3NzLnRhbmlzaHEuY28uaW4iLCJhY2wiOlsiVS1RQSIsIkktLy8vdjhBRUEiLCJNLUxWYXExVlZXcXFxcXJ3QSJdLCJ0eXBlIjoiTDEiLCJhdWQiOiJlYzItMTMtMjM0LTEyNC0yNTAuYXAtc291dGgtMS5jb21wdXRlLmFtYXpvbmF3cy5jb20iLCJ1cG4iOiJib3MudXJiIiwibmJmIjoxNTc2NDc5MTMwLCJleHAiOjE1NzY0ODI3MzAsImlhdCI6MTU3NjQ3OTEzMCwiZW1haWwiOiJib3MudXJiQG1pbmR0cmVlLmNvbSJ9.2wr9qPyL7QoahMehvuxXBOUaQLmYD_-ppkd_iqExyCY';
    // SM

    const userData: Token = JwtHelper.decodeToken(token.accessToken);
    this.appSettingFacade.changeStoreType(userData.type);
    this.appSettingFacade.changeLoginStatus('LOGGED_IN');
    const ACLlist = ACLEncoder.getAllAssignedACL(
      ACLEncoder.getMapFromStringArray(userData.acl)
    );
    console.log('ACLList', ACLlist);
    return {
      username: userData.upn,
      password: password,
      firstName: userData.unique_name,
      lastName: userData.name,
      locationCode: userData.loc,
      storeType: userData.type,
      exptime: new Date(+userData.exp * 1000),
      accessToken: token.accessToken,
      refreshToken: token.refreshTokenId,
      refreshTokenExp: new Date(token.refreshTokenExpiresAt),
      acl: ACLlist
    };
  }
}
