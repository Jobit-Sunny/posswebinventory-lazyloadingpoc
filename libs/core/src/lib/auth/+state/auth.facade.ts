import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../core.state';
import { authDataQuery } from './auth.selectors';
import * as AuthActions from './auth.actions';
import { LoginPayload } from './auth.actions';

@Injectable()
export class AuthFacade {
  selectUserName$ = this.store.select(authDataQuery.selectUserName);
  selectLoggedInStatus$ = this.store.select(
    authDataQuery.selectIsLoggedInStatus
  );
  selectLoggedOutStatus$ = this.store.select(
    authDataQuery.selectIsLoggedOutStatus
  );

  selectAuthData$ = this.store.select(authDataQuery.selectAuthData);

  selectLoginError$ = this.store.select(authDataQuery.selectAuthError);

  getAccessToken$ = this.store.select(authDataQuery.selectAccessToken);

  getReloadStatus$ = this.store.select(authDataQuery.selectReloadState);

  getExpTime$ = this.store.select(authDataQuery.selectExpTime);

  getRefreshToken$ = this.store.select(authDataQuery.selectRefreshToken);

  getRefreshTokenExp$ = this.store.select(authDataQuery.selectRefreshTokenExp);

  getStoreType$ = this.store.select(authDataQuery.selectStoreType);

  getLocationCode$ = this.store.select(authDataQuery.selectLocationCode);

  getACL$ = this.store.select(authDataQuery.selectACL);

  isLoading$ = this.store.select(authDataQuery.selectIsLoading);

  constructor(private store: Store<AppState>) {}

  getUserName() {
    return this.selectUserName$;
  }

  getAuthData() {
    return this.selectAuthData$;
  }

  getLoginError() {
    return this.selectLoginError$;
  }

  getAccessToken() {
    return this.getAccessToken$;
  }

  getStoreType() {
    return this.getStoreType$;
  }

  getLocationCode() {
    return this.getLocationCode$;
  }

  isUserLoggedIn() {
    return this.selectLoggedInStatus$;
  }

  isLoading() {
    return this.isLoading$;
  }

  isUserLoggedOut() {
    return this.selectLoggedOutStatus$;
  }

  getReloadState() {
    return this.getReloadStatus$;
  }

  getExpTime() {
    return this.getExpTime$;
  }

  getRefreshToken() {
    return this.getRefreshToken$;
  }

  getRefreshTokenExp() {
    return this.getRefreshTokenExp$;
  }

  getACL() {
    return this.getACL$;
  }

  login(loginInputs: LoginPayload) {
    this.store.dispatch(new AuthActions.Login(loginInputs));
  }

  logOut() {
    this.store.dispatch(new AuthActions.Logout());
  }

  reload(returnUrl: string) {
    this.store.dispatch(new AuthActions.Reload(returnUrl));
  }

  refresh(refreshToken: string) {
    this.store.dispatch(new AuthActions.Refresh(refreshToken));
  }
}
