import { AuthActions, AuthActionTypes } from './auth.actions';
import { AuthState } from './auth.state';

export const Auth_FEATURE_KEY = 'auth';

const initialAuthState: AuthState = {
  userName: '',
  firstName: '',
  lastName: '',
  storeType: '',
  locationCode: '',
  accessToken: '',
  error: null,
  isLoading: false,
  isLoggedIn: false,
  isLoggedOut: false,
  isReloaded: false,
  exptime: null,
  refreshToken: '',
  refreshTokenExp: null,
  acl: new Map<string, string[]>()
};

export function authReducer(
  state: AuthState = initialAuthState,
  action: AuthActions
): AuthState {
  switch (action.type) {
    case AuthActionTypes.LOGIN: {
      return {
        ...state,
        isLoggedIn: false,
        isLoading: true,
        error: null
      };
    }

    case AuthActionTypes.LOGIN_SUCCESS: {
      return {
        ...state,
        isLoggedIn: true,
        isLoading: false,
        error: null,
        accessToken: action.payload.accessToken,
        userName: action.payload.username,
        firstName: action.payload.firstName,
        lastName: action.payload.lastName,
        storeType: action.payload.storeType,
        locationCode: action.payload.locationCode,
        exptime: action.payload.exptime,
        refreshToken: action.payload.refreshToken,
        refreshTokenExp: action.payload.refreshTokenExp,
        acl: action.payload.acl
      };
    }

    case AuthActionTypes.LOGIN_FAILURE: {
      return {
        ...state,
        isLoggedOut: true,
        isLoading: false,
        error: action.payload
      };
    }

    case AuthActionTypes.LOGOUT: {
      return {
        ...state,
        isLoading: true,
        error: null
      };
    }

    case AuthActionTypes.LOGOUT_SUCCESS: {
      return {
        ...state,
        isLoggedIn: false,
        isLoggedOut: true,
        isLoading: false,
        accessToken: '',
        error: null
      };
    }

    case AuthActionTypes.LOGOUT_FAILURE: {
      return {
        ...state,
        isLoggedOut: false,
        isLoading: false,
        error: action.payload
      };
    }

    case AuthActionTypes.RELOAD: {
      return {
        ...state,
        isReloaded: false,
        isLoading: true,
        error: null
      };
    }

    case AuthActionTypes.RELOAD_SUCCESS: {
      return {
        ...state,
        isReloaded: true,
        isLoggedIn: true,
        isLoading: false,
        error: null,
        accessToken: action.payload.accessToken,
        userName: action.payload.username,
        firstName: action.payload.firstName,
        lastName: action.payload.lastName,
        storeType: action.payload.storeType,
        locationCode: action.payload.locationCode,
        exptime: action.payload.exptime,
        refreshToken: action.payload.refreshToken,
        refreshTokenExp: action.payload.refreshTokenExp
        // acl: action.payload.acl
      };
    }

    case AuthActionTypes.RELOAD_FAILURE: {
      return {
        ...state,
        isLoggedOut: true,
        isLoading: false,
        accessToken: '',
        error: action.payload
      };
    }

    case AuthActionTypes.REFRESH_SUCCESS: {
      return {
        ...state,
        error: null,
        accessToken: action.payload.accessToken,
        userName: action.payload.username,
        firstName: action.payload.firstName,
        lastName: action.payload.lastName,
        storeType: action.payload.storeType,
        locationCode: action.payload.locationCode,
        exptime: action.payload.exptime,
        refreshToken: action.payload.refreshToken,
        refreshTokenExp: action.payload.refreshTokenExp
        // acl: action.payload.acl
      };
    }

    case AuthActionTypes.REFRESH_FAILURE: {
      return {
        ...state,
        isLoggedIn: false,
        isLoggedOut: true,
        accessToken: '',
        error: action.payload
      };
    }

    default: {
      return state;
    }
  }
}
