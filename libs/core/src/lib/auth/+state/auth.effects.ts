import { Injectable } from '@angular/core';
import { DataPersistence } from '@nrwl/angular';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { map, tap, withLatestFrom, delay, take, filter } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import * as AuthActions from './auth.actions';
import { AuthState } from './auth.state';
import { AuthenticationService } from '../auth.service';
import { AuthActionTypes } from './auth.actions';
import { NotificationService } from '../../notification/notification.service';
import { CustomErrors } from '../../model/error.model';
import { CustomErrorAdaptor } from '../../adaptors/error.adaptors';
import { AuthFacade } from './auth.facade';
import { AppsettingFacade } from '../../app-setting/appsetting.facade';

@Injectable()
export class AuthEffect {
  refreshTokenSub$: Subscription = new Subscription();
  refreshTokenTimerSub$: Subscription = new Subscription();

  constructor(
    private authService: AuthenticationService,
    private dataPersistence: DataPersistence<any>,
    private notificationService: NotificationService,
    private router: Router,
    private actions$: Actions,
    private authfacade: AuthFacade,
    private appSettingFacade: AppsettingFacade
  ) {}

  @Effect()
  login = this.dataPersistence.pessimisticUpdate(AuthActionTypes.LOGIN, {
    run: (a: AuthActions.Login, state: AuthState) => {
      return this.authService
        .login(a.payload.userName, a.payload.authorizationCode)
        .pipe(
          tap(_ => this.refreshCheck()),
          map(response => new AuthActions.LoginSuccess(response))
        );
    },
    onError: (a: AuthActions.Login, e: any) => {
      return new AuthActions.LoginFailure(this.errorHandler(e));
    }
  });

  @Effect()
  logout = this.dataPersistence.pessimisticUpdate(AuthActionTypes.LOGOUT, {
    run: (a: AuthActions.Logout, state: AuthState) => {
      return this.authService.logout().pipe(
        tap(response => {
          // to unsubscribe the AccessToken Observable and delay timer observable on Logout
          this.refreshTokenSub$.unsubscribe();
          this.refreshTokenTimerSub$.unsubscribe();
          this.router.navigate(['user-management/login']);
        }),
        map(response => new AuthActions.LogoutSuccess())
      );
    },
    onError: (a: AuthActions.Logout, e: any) => {
      return new AuthActions.LogoutFailure(this.errorHandler(e));
    }
  });

  @Effect()
  reload = this.dataPersistence.fetch(AuthActionTypes.RELOAD, {
    run: (action: AuthActions.Reload) => {
      return this.authService.reload().pipe(
        tap(response => {
          // to unsubscribe the AccessToken Observable and delay timer observable on Logout
          this.refreshTokenSub$.unsubscribe();
          this.refreshTokenTimerSub$.unsubscribe();
          this.refreshCheck();
          this.router.navigate([action.payload]);
        }),
        map(response => new AuthActions.ReloadSuccess(response))
      );
    },
    onError: (action: AuthActions.Reload, error: HttpErrorResponse) => {
      this.router.navigate(['user-management/login'], {
        queryParams: { returnUrl: action.payload }
      });
      return new AuthActions.ReloadFailure(this.errorHandler(error));
    }
  });

  @Effect()
  refresh = this.dataPersistence.fetch(AuthActionTypes.REFRESH, {
    run: (action: AuthActions.Refresh) => {
      return this.authService
        .refresh(action.payload)
        .pipe(map(response => new AuthActions.RefreshSuccess(response)));
    },
    onError: (action: AuthActions.Refresh, error: HttpErrorResponse) => {
      return new AuthActions.RefreshFailure(this.errorHandler(error));
    }
  });

  @Effect({ dispatch: false }) topNavToggle = this.actions$.pipe(
    ofType('@ngrx/router-store/navigated'),
    withLatestFrom(this.authfacade.isUserLoggedIn()),
    map(([_, isloggedin]) => {
      if (isloggedin) {
        this.appSettingFacade.changeLoginStatus('LOGGED_IN');
      } else {
        this.appSettingFacade.changeLoginStatus('LOGGED_OUT');
      }
    })
  );

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }

  refreshCheck() {
    this.refreshTokenSub$ = this.authfacade
      .getAccessToken()
      .pipe(
        filter(token => !!token),
        withLatestFrom(this.authfacade.getExpTime())
      )
      .subscribe(([accesstoken, exptime]) => {
        exptime.setMinutes(exptime.getMinutes() - 2);
        this.refreshTokenTimerSub$ = this.authfacade
          .getRefreshToken()
          .pipe(
            take(1),
            delay(exptime.getTime() - new Date().getTime())
          )
          .subscribe(token => this.authfacade.refresh(token));
      });
  }
}
