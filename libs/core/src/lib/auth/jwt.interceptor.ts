import { Injectable, OnDestroy } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { AuthFacade } from './+state/auth.facade';
import { takeUntil } from 'rxjs/operators';

@Injectable()
export class JwtInterceptor implements HttpInterceptor, OnDestroy {
  private authdata;
  destroy$: Subject<null> = new Subject<null>();

  constructor(
    // private authenticationService: AuthenticationService,
    private authfacade: AuthFacade
  ) {
    this.authfacade
      .getAccessToken()
      .pipe(takeUntil(this.destroy$))
      .subscribe(token => (this.authdata = token));
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    //  add authorization header with jwt token if available
    if (this.authdata) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.authdata}`
        }
      });
    }

    return next.handle(request);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
