import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BarcodeService {
  private data = [];
  private input = '';

  public get ItemCode() {
    return this.data[0];
  }

  public get LotNumber() {
    return this.data[1];
  }

  public clear() {
    this.data = [];
    this.input = '';
  }

  filterKeyCode(event: KeyboardEvent) {
    const regexpNumber: RegExp = new RegExp('^[A-Z a-z 0-9]$');
    if (
      regexpNumber.test(event.key) ||
      event.key === 'Enter' ||
      event.key === 'Tab'
    ) {
      if (event.key === 'Tab') {
        event.preventDefault();
      }

      if (event.key !== 'Enter' && event.key !== 'Tab') {
        this.input += event.key;
      } else if (event.key === 'Tab' || event.key === 'Enter') {
        this.data.push(this.input);
        this.input = '';
      }
    }
  }
  constructor() {}
}
