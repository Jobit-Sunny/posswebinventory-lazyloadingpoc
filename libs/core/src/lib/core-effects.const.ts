import { AppsettingEffects } from './app-setting/appsetting.effects';
import { OverlayNotificationEffects } from './router-state/overlay-notification.effects';
import { LocationMappingEffect } from './location-mapping/+state/location-mapping.effect';
import { AuthEffect } from './auth/+state/auth.effects';

export const CORE_EFFECTS = [
  AppsettingEffects,
  OverlayNotificationEffects,
  AuthEffect,
  LocationMappingEffect
];
