import { AppConfig } from './app-config.model';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {
  static setting: AppConfig;
  constructor(
    private http: HttpClient,
    @Inject('env') private environment,
    private translate: TranslateService
  ) {}

  loadAppSetting() {
    const appSettingJsonFile = `assets/config/config.${
      this.environment.name
    }.json`;
    console.log(appSettingJsonFile);
    return new Promise<void>((resolve, reject) => {
      this.http
        .get(appSettingJsonFile)
        .toPromise()
        .then((response: AppConfig) => {
          AppConfigService.setting = response;

          resolve();
        })
        .catch((response: any) => {
          reject(
            `Could not load file '${appSettingJsonFile}': ${JSON.stringify(
              response
            )}`
          );
        });
    });
  }
}
