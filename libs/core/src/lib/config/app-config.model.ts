export interface AppConfig {
  env: {
    name: string;
  };

  apiServer: {
    apiURL: string;
  };

  shortcutConfigSetting: {
    shortcutConfigFilePath: string;
  };
  translations: {
    prefix: string;
  };
}
