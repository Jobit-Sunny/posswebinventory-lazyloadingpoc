import { AppsettingAction, AppsettingActionTypes } from './appsetting.actions';
import { AppsettingsState, LIGHT_THEME } from './appsetting.model';

export const APPSETTING_FEATURE_KEY = 'appsetting';

export const initialState: AppsettingsState = {
  language: 'en',
  theme: LIGHT_THEME,
  pageSize: 5,
  pageSizeOptions: [5, 10, 25, 50],
  // storeType: '',
  // status: '',
  dateFormat:'DD-MMM-YYYY',
  storeType: localStorage.getItem('storeType'),
  status: localStorage.getItem('authStatus'),
  maxFilterLimit: 10,
  maxSortLimit: 1,
  maxProductInList: 50,
  mobileNoMaxLength: 10
};

export function appsettingReducer(
  state: AppsettingsState = initialState,
  action: AppsettingAction
): AppsettingsState {
  switch (action.type) {
    case AppsettingActionTypes.CHANGE_LANGUAGE: {
      state = {
        ...state,
        language: action.payload.language
      };
      break;
    }
    case AppsettingActionTypes.CHANGE_THEME: {
      state = {
        ...state,
        theme: action.payload.theme
      };
      break;
    }
    case AppsettingActionTypes.CHANGE_STORETYPE: {
      state = {
        ...state,
        storeType: action.payload.storetype
      };
      break;
    }
    case AppsettingActionTypes.CHANGE_STATUS: {
      state = {
        ...state,
        status: action.payload.status
      };
      break;
    }
  }
  return state;
}
