import { Injectable } from '@angular/core';

import { select, Store } from '@ngrx/store';
import { AppState } from '../core.state';
import { Language, AppsettingsState } from './appsetting.model';
import { appQuery } from './appsetting.selectors';
import { Observable } from 'rxjs';
import {
  ActionSettingChangeStoreType,
  ActionSettingChangeStatus
} from './appsetting.actions';

@Injectable()
export class AppsettingFacade {
  selectedSetting$: Observable<AppsettingsState> = this.store.select(
    appQuery.selectSettings
  );
  selectedLanguage$: Observable<Language> = this.store.select(
    appQuery.selectLanguage
  );
  currentStoreType$: Observable<any> = this.store.select(
    appQuery.selectStoreType
  );
  authStatus$: Observable<any> = this.store.select(appQuery.selectAuthStatus);
  pageSize$: Observable<any> = this.store.select(appQuery.selectPageSize);
  pageSizeOptions$: Observable<any> = this.store.select(
    appQuery.selectPageSizeOptions
  );

  maxFilterLimit$: Observable<any> = this.store.select(
    appQuery.selectMaxFilterLimit
  );

  maxSortLimit$: Observable<any> = this.store.select(
    appQuery.selectMaxSortLimit
  );

  maxProductInList$: Observable<any> = this.store.select(
    appQuery.selectMaxProductInList
  );

  dateFormat$: Observable<any> = this.store.select(appQuery.selectDateFormat);

  mobileMaxLength$: Observable<any> = this.store.select(
    appQuery.selectMobileMaxlength
  );

  constructor(private store: Store<AppState>) {}

  getLanguage() {
    return this.selectedLanguage$;
  }
  getSetting() {
    return this.selectedSetting$;
  }
  getStoreType() {
    return this.currentStoreType$;
  }
  getAuthStatus() {
    return this.authStatus$;
  }

  getPageSize() {
    return this.pageSize$;
  }
  getPageSizeOptions() {
    return this.pageSizeOptions$;
  }

  getMaxFilterLimit() {
    return this.maxFilterLimit$;
  }

  getMaxSortLimit() {
    return this.maxSortLimit$;
  }

  getMaxProductInList() {
    return this.maxProductInList$;
  }

  getDateFormat() {
    return this.dateFormat$;
  }

  getMobileMaxLength() {
    return this.mobileMaxLength$;
  }

  changeStoreType(storetype: string) {
    this.store.dispatch(new ActionSettingChangeStoreType({ storetype }));
  }

  changeLoginStatus(status: string) {
    this.store.dispatch(new ActionSettingChangeStatus({ status }));
  }
}
