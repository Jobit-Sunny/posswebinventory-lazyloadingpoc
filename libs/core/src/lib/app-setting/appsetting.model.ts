export const LIGHT_THEME = 'LIGHT-THEME';

export type Language = 'en' | 'ar';

export interface AppsettingsState {
  language: Language;
  theme: string;
  pageSize: number;
  pageSizeOptions: number[];
  storeType: string;
  status: string;
  dateFormat:string;
  maxFilterLimit: number;
  maxSortLimit: number;
  maxProductInList: number;
  mobileNoMaxLength: number;
}
