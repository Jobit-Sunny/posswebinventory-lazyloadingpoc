import { createSelector } from '@ngrx/store';
import { selectAppsettingState } from '../core.state';
import { AppsettingsState } from './appsetting.model';

export const selectSettings = createSelector(
  selectAppsettingState,
  (state: AppsettingsState) => state
);

export const selectLanguage = createSelector(
  selectSettings,
  (state: AppsettingsState) => state.language
);

export const selectStoreType = createSelector(
  selectSettings,
  (state: AppsettingsState) => state.storeType
);

export const selectAuthStatus = createSelector(
  selectSettings,
  (state: AppsettingsState) => state.status
);

export const selectDateFormat = createSelector(
  selectSettings,
  (state: AppsettingsState) => state.dateFormat
);

export const selectPageSize = createSelector(
  selectSettings,
  (state: AppsettingsState) => state.pageSize
);

export const selectPageSizeOptions = createSelector(
  selectSettings,
  (state: AppsettingsState) => state.pageSizeOptions
);

export const selectMaxFilterLimit = createSelector(
  selectSettings,
  (state: AppsettingsState) => state.maxFilterLimit
);

export const selectMaxSortLimit = createSelector(
  selectSettings,
  (state: AppsettingsState) => state.maxSortLimit
);

export const selectMaxProductInList = createSelector(
  selectSettings,
  (state: AppsettingsState) => state.maxProductInList
);

export const selectMobileMaxlength = createSelector(
  selectSettings,
  (state: AppsettingsState) => state.mobileNoMaxLength
);

export const appQuery = {
  selectSettings,
  selectLanguage,
  selectStoreType,
  selectAuthStatus,
  selectPageSize,
  selectPageSizeOptions,
  selectMaxFilterLimit,
  selectMaxSortLimit,
  selectMaxProductInList,
  selectDateFormat,
  selectMobileMaxlength
};
