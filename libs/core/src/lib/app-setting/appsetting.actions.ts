import { Action } from '@ngrx/store';
import { Language } from './appsetting.model';

export enum AppsettingActionTypes {
  CHANGE_LANGUAGE = '[Settings] Change Language',
  CHANGE_THEME = '[Settings] Change Theme',
  CHANGE_STORETYPE = '[Settings] Change StoreType',
  CHANGE_STATUS = '[Settings] Change Status'

}

export class ActionSettingsChangeLanguage implements Action {
  readonly type = AppsettingActionTypes.CHANGE_LANGUAGE;
  constructor(readonly payload: { language: Language }) { }
}

export class ActionSettingsChangeTheme implements Action {
  readonly type = AppsettingActionTypes.CHANGE_THEME;
  constructor(readonly payload: { theme: string }) { }
}
export class ActionSettingChangeStoreType implements Action {
  readonly type = AppsettingActionTypes.CHANGE_STORETYPE;
  constructor(readonly payload: { storetype: string, }) { }
}

export class ActionSettingChangeStatus implements Action {
  readonly type = AppsettingActionTypes.CHANGE_STATUS;
  constructor(readonly payload: { status: string }) { }
}

export type AppsettingAction =
  | ActionSettingsChangeLanguage
  | ActionSettingsChangeTheme
  | ActionSettingChangeStoreType
  | ActionSettingChangeStatus;
