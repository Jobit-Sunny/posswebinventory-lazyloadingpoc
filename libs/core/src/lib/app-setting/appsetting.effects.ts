import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';

import {
  ActionSettingsChangeLanguage,
  AppsettingAction
} from './appsetting.actions';
import { AppsettingsState } from './appsetting.model';
import { Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';
import { OverlayNotificationService } from '@poss-web/shared';

@Injectable()
export class AppsettingEffects {
  constructor(
    private actions$: Actions,
    private store: Store<AppsettingsState>,
    private overlayNotification: OverlayNotificationService
  ) {}
  // @Effect() overlayClose = this.actions$.pipe(
  //   ofType('@ngrx/router-store/navigated'),
  //   tap(action => {
  //     console.log(action), this.overlayNotification.close();
  //   })
  // );
}
