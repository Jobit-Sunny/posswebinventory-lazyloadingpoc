import { Injectable } from '@angular/core';
import { ApiService } from '../http/api.service';
import { Observable } from 'rxjs';
import { State } from '../model/state.model';
import { map } from 'rxjs/operators';
import { StateDataAdaptor } from '../adaptors/state.adaptors';
import { getStatesUrl, getStateByCodeUrl } from '../endpoints.constants';

@Injectable({
  providedIn: 'root',
})
export class StateDataService {
  constructor(private apiService: ApiService) {}

  getStates(
    pageIndex?: number,
    pageSize?: number,
    isActive?:boolean,
    countryCode?:string,
    isPageable?: boolean,
    sort?:string[]
    ): Observable<State[]> {
    const url = getStatesUrl(pageIndex, pageSize,isActive,countryCode,isPageable,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => StateDataAdaptor.stateDataFromJson(data)));
  }
  getStatesCount(
    pageIndex?: number,
    pageSize?: number,
    isActive?:boolean,
    countryCode?:string,
    isPageable?: boolean,
    sort?:string[]
  ):Observable<number>{
    const url = getStatesUrl(pageIndex, pageSize,isActive,countryCode,isPageable,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => data.totalElements));
  }
  getStateByCode(stateCode: number): Observable<State> {
    const url = getStateByCodeUrl(stateCode);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => StateDataAdaptor.stateFromJson(data)));
  }
}
