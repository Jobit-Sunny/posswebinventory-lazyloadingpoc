import { Injectable } from '@angular/core';
import { ApiService } from '../http/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Bin } from '../model/bin.model';
import { getBinByCodeUrl, getBinDetailsUrl } from '../endpoints.constants';
import { BinDataAdaptor } from '../adaptors/bin.adaptors';
import { BinCode } from '../model/bin-codes.model';
import { BinCodeHelper } from '../helpers/bin-code.helper';


@Injectable({
  providedIn: 'root',
})
export class BinDataService {
  constructor(private apiService: ApiService) {}


  getBinByCode(binCode: string): Observable<Bin> {
    const url = getBinByCodeUrl(binCode);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => BinDataAdaptor.binFromJson(data)));
  }

  getBinDetails(
    binGroupCode: string,
    isActive?: boolean,
    isPageable?: boolean,
    locationCode?: string
 ): Observable<BinCode[]> {
   const url = getBinDetailsUrl(binGroupCode,isActive,isPageable,locationCode);
   return this.apiService
     .get(url.path,url.params)
     .pipe(map((data: any) => BinCodeHelper.getBinCodes(data.results)));
 }
 getBinCount(
  binGroupCode: string,
  isActive?: boolean,
  isPageable?: boolean,
  locationCode?: string
   ):Observable<number>{
    const url = getBinDetailsUrl(binGroupCode,isActive,isPageable,locationCode);
  return this.apiService
    .get(url.path,url.params)
    .pipe(map((data: any) => data.totalElements));
}
}
