import { Injectable } from '@angular/core';
import { ApiService } from '../http/api.service';
import { Observable } from 'rxjs';
import { getLocationsUrl, getLocationByCodeUrl, getIBTLocationsSummaryUrl, getLocationSummaryByCodeUrl, getLocationSummaryUrl } from '../endpoints.constants';
import { Location, LocationSummary } from '../model/location.model';
import { LocationFilter } from '../model/location-filter.model';
import { map } from 'rxjs/operators';
import { LocationDataAdaptor } from '../adaptors/location.adaptors';

@Injectable({
  providedIn: 'root',
})
export class LocationDataService {
  constructor(private apiService: ApiService) {}

  getLocations(
    filter?: LocationFilter,
    isActive?: boolean,
    pageIndex?: number,
    pageSize?: number,
    isPageable?: boolean,
    sort?:string[]
    ): Observable<Location[]> {
    const url = getLocationsUrl(filter,isActive,pageIndex, pageSize,isPageable,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => LocationDataAdaptor.locationDataFromJson(data)));
  }

  getLocationsCount(
    filter?: LocationFilter,
    isActive?: boolean,
    pageIndex?: number,
    pageSize?: number,
    isPageable?: boolean,
    sort?:string[]
  ):Observable<number>{
    const url = getLocationsUrl(filter,isActive,pageIndex, pageSize,isPageable,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => data.totalElements));
  }


  getLocationByCode(locationCode: string): Observable<Location> {
    const url = getLocationByCodeUrl(locationCode);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => LocationDataAdaptor.locationFromJson(data)));
  }
  
  getLocationSummary(): Observable<LocationSummary> {
    const url = getLocationSummaryUrl();
    return this.apiService
      .get(url)
      .pipe(map((data: any) => LocationDataAdaptor.locationSummaryFromJson(data)));
  }

  getIBTLocationsSummary(
    pageIndex?: number,
    pageSize?: number,
    regionType?:string,
    locationType?:string[],
    ownerTypeCode?:string[],
    sort?:string[]
  ):Observable<Location[]> {
    const url = getIBTLocationsSummaryUrl(pageIndex, pageSize,regionType,locationType,ownerTypeCode,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => LocationDataAdaptor.locationDataFromJson(data)));
  }

  getLocationSummaryByLocationCode(locationCode: string): Observable<Location> {
    const url = getLocationSummaryByCodeUrl(locationCode);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => LocationDataAdaptor.locationFromJson(data)));
  }


}
