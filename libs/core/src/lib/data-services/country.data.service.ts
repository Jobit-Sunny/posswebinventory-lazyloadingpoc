import { Injectable } from '@angular/core';
import { ApiService } from '../http/api.service';
import { Observable } from 'rxjs';
import { Country } from '../model/country.model';
import { map } from 'rxjs/operators';
import { CountryDataAdaptor } from '../adaptors/country.adaptors';
import { getCountriesUrl, getCountryByCodeUrl } from '../endpoints.constants';

@Injectable({
  providedIn: 'root',
})
export class CountryDataService {
  constructor(private apiService: ApiService) {}

  getCountries(
    pageIndex?: number,
    pageSize?: number,
    isActive?:boolean,
    isPageable?: boolean,
    sort?:string[]
    ): Observable<Country[]> {
    const url = getCountriesUrl(pageIndex, pageSize,isActive,isPageable,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => CountryDataAdaptor.countryDataFromJson(data)));
  }
  getCountriesCount(
    pageIndex?: number,
    pageSize?: number,
    isActive?:boolean,
    isPageable?: boolean,
    sort?:string[]
  ):Observable<number>{
    const url = getCountriesUrl(pageIndex, pageSize,isActive,isPageable,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => data.totalElements));
  }

  getCountryByCode(countryCode: number): Observable<Country> {
    const url = getCountryByCodeUrl(countryCode);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => CountryDataAdaptor.countryFromJson(data)));
  }
}
