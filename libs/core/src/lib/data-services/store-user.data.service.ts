import { Injectable } from '@angular/core';
import { ApiService } from '../http/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { StoreUser, StoreUserDetails } from '../model/store-user.model';
import { getStoreUserUrl, getStoreUserDetailsUrl } from '../endpoints.constants';
import { StoreUserHelper } from '../helpers/store-user.helper';
import { StoreUserAdaptor } from '../adaptors/store-user.adaptors';
@Injectable({
  providedIn: 'root',
})
export class StoreUserDataService {
  constructor(private apiService: ApiService) {}

  getStoreUsers(
    pageIndex?:number,
    pageSize?:number,
    isActive?: boolean,
    searchField?: string,
    roleCodes?:string[],
    sort?:string[]
    ): Observable<StoreUser[]> {
    const url = getStoreUserUrl(pageIndex,pageSize,isActive,searchField,roleCodes,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => StoreUserHelper.getStoreUsers(data.results)));
  }
  getStoreUsersCount(
    pageIndex?:number,
    pageSize?:number,
    isActive?: boolean,
    searchField?: string,
    roleCodes?:string[],
    sort?:string[]
  ):Observable<number>{
    const url = getStoreUserUrl(pageIndex,pageSize,isActive,searchField,roleCodes,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => data.totalElements));
  }

  getStoreUserDetails(
    employeeCode:string
    ): Observable<StoreUserDetails> {
    const url = getStoreUserDetailsUrl(employeeCode);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => StoreUserAdaptor.StoreUserDetailsFromJson(data)));
  }
}
