import { Injectable } from '@angular/core';
import { ApiService } from '../http/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Lov } from '../model/lov.model';
import {  getInventoryLovsEndpointUrl, getLocationLovsEndpointUrl, getProductLovsEndpointUrl } from '../endpoints.constants';
import { LovHelper } from '../helpers/lov.helper';

@Injectable({
  providedIn: 'root',
})
export class LovDataService {
  constructor(private apiService: ApiService) {}

  getInventoryLovs(
    lovType: string,
  ): Observable<Lov[]> {
    const url = getInventoryLovsEndpointUrl(lovType);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => LovHelper.getLovs(data.values)));
  }

  getLocationLovs(
    lovType: string,
  ): Observable<Lov[]> {
    const url = getLocationLovsEndpointUrl(lovType);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => LovHelper.getLovs(data.values)));
  }
  getProductLovs(
    lovType: string,
  ): Observable<Lov[]> {
    const url = getProductLovsEndpointUrl(lovType);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => LovHelper.getLovs(data.values)));
  }
}
