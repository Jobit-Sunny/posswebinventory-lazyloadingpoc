import { Injectable } from '@angular/core';
import { ApiService } from '../http/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { StoreConfig } from '../model/store-config.model';
import { StoreConfigAdaptor } from '../adaptors/store-config.adaptors';
import {  getStoreBinsUrl, getLocationSummaryUrl } from '../endpoints.constants';
import { StoreBin } from '../model/store-config-bins.model';
import { StoreConfigHelper } from '../helpers/store-config.helper';
@Injectable({
  providedIn: 'root',
})
export class StoreConfigDataService {
  constructor(private apiService: ApiService) {}

  /* getStoreConfig(): Observable<StoreConfig> {
    const url = getLocationSummaryUrl();
    return this.apiService
      .get(url)
      .pipe(map((data: any) => StoreConfigAdaptor.StoreConfigDataFromJson(data)));
  } */
  getStoreBins(binType:string): Observable<StoreBin[]> {
    const url = getStoreBinsUrl(binType);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => StoreConfigHelper.getbins(data.results)));
  }
}
