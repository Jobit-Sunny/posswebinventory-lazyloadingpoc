import { Injectable } from '@angular/core';
import { ApiService } from '../http/api.service';
import { Observable } from 'rxjs';
import { Town } from '../model/town.model';
import { map } from 'rxjs/operators';
import { TownDataAdaptor } from '../adaptors/town.adaptors';
import { getTownsUrl, getTownByCodeUrl } from '../endpoints.constants';

@Injectable({
  providedIn: 'root',
})
export class TownDataService {
  constructor(private apiService: ApiService) {}

  getTowns(
    pageIndex?: number,
    pageSize?: number,
    isActive?:boolean,
    stateCode?:string,
    isPageable?: boolean,
    sort?:string[]
    ): Observable<Town[]> {
    const url = getTownsUrl(pageIndex, pageSize,isActive,stateCode,isPageable,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => TownDataAdaptor.townDataFromJson(data)));
  }
  getStoreUsersCount(
    pageIndex?: number,
    pageSize?: number,
    isActive?:boolean,
    stateCode?:string,
    isPageable?: boolean,
    sort?:string[]
  ):Observable<number>{
    const url = getTownsUrl(pageIndex, pageSize,isActive,stateCode,isPageable,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => data.totalElements));
  }
  getTownByCode(townCode: number): Observable<Town> {
    const url = getTownByCodeUrl(townCode);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => TownDataAdaptor.townFromJson(data)));
  }
}
