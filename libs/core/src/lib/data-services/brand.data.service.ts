import { Injectable } from '@angular/core';
import { ApiService } from '../http/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Brand } from '../model/brand.model';
import { getBrandsUrl, getBrandsbyBrandCodeUrl } from '../endpoints.constants';
import {BrandHelper} from '../helpers/brand.helper';
import { BrandAdaptor } from '../adaptors/brand.adaptors';
@Injectable({
  providedIn: 'root',
})
export class BrandDataService {
  constructor(private apiService: ApiService) {}

  getBrands(
    pageIndex?:number,
    pageSize?:number,
    isActive?: boolean,
    isPageable?: boolean,
    sort?:string[]): Observable<Brand[]> {
    const url = getBrandsUrl(pageIndex,pageSize,isActive,isPageable,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => BrandHelper.getBrands(data.results)));
  }
  getBrandsCount(
    pageIndex?:number,
    pageSize?:number,
    isActive?: boolean,
    isPageable?: boolean,
    sort?:string[]
  ):Observable<number>{
    const url = getBrandsUrl(pageIndex,pageSize,isActive,isPageable,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => data.totalElements));
  }
  getBrandByCode(BrandCode: string): Observable<Brand> {
    const url = getBrandsbyBrandCodeUrl(BrandCode);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => BrandAdaptor.brandDataFromJson(data)));
  }
}
