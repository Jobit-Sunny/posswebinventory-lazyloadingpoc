import { Injectable } from '@angular/core';
import { ApiService } from '../http/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Region, RegionSummary } from '../model/region.model';
import { RegionHelper } from '../helpers/region.helper';
import { getRegionsUrl, getRegionSummaryByCodeUrl } from '../endpoints.constants';
import { RegionAdaptor } from '../adaptors/region.adaptors';
@Injectable({
  providedIn: 'root',
})
export class RegionDataService {
  constructor(private apiService: ApiService) {}

  getRegions(
    regionType: string,
    pageIndex?:number,
    pageSize?:number,
    isActive?: boolean,
    isPageable?: boolean,
    parentRegionCode?:string,
    sort?:string[]
    ): Observable<Region[]> {
    const url = getRegionsUrl(regionType,pageIndex,pageSize,isActive,isPageable,parentRegionCode,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => RegionHelper.getRegions(data.results)));
  }

  getRegionsCount(
    regionType: string,
    pageIndex?:number,
    pageSize?:number,
    isActive?: boolean,
    isPageable?: boolean,
    parentRegionCode?:string,
    sort?:string[]
  ):Observable<number>{
    const url = getRegionsUrl(regionType,pageIndex,pageSize,isActive,isPageable,parentRegionCode,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => data.totalElements));
  }

  getRegionSummaryByCode(regionCode: string): Observable<RegionSummary> {
    const url = getRegionSummaryByCodeUrl(regionCode);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => RegionAdaptor.regionSummaryDataFromJson(data)));
  }
}
