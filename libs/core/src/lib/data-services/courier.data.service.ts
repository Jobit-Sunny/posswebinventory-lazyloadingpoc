import { Injectable } from '@angular/core';
import { ApiService } from '../http/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Courier } from '../model/courier.model';
import { CourierHelper } from '../helpers/courier.helper';
import { getCourierUrl, getCourierSummaryUrl } from '../endpoints.constants';
@Injectable({
  providedIn: 'root',
})
export class CourierDataService {
  constructor(private apiService: ApiService) {}

  getCouriers(
    pageIndex?:number,
    pageSize?:number,
    isActive?: boolean,
    locationCode?: string,
    isPageable?: boolean,
    sort?:string[]
    ): Observable<Courier[]> {
    const url = getCourierUrl(pageIndex,pageSize,isActive,locationCode,isPageable,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => CourierHelper.getCouriers(data.results)));
  }
  getCouriersCount(
    pageIndex?:number,
    pageSize?:number,
    isActive?: boolean,
    locationCode?: string,
    isPageable?: boolean,
    sort?:string[]
  ):Observable<number>{
    const url = getCourierUrl(pageIndex,pageSize,isActive,locationCode,isPageable,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => data.totalElements));
  }

  getCouriersSummary(
    pageIndex?:number,
    pageSize?:number,
    isActive?: boolean,
    sort?:string[]
    ): Observable<Courier[]> {
    const url = getCourierSummaryUrl(pageIndex,pageSize,isActive,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => CourierHelper.getCouriers(data.results)));
  }
}
