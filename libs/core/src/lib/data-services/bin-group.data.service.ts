import { Injectable } from '@angular/core';
import { ApiService } from '../http/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { getBinGroupsUrl, getBinGroupByCodeUrl, getBinDetailsUrl } from '../endpoints.constants';
import { BinGroupDataAdaptor } from '../adaptors/bin-group.adaptors';
import { BinGroup } from '../model/bin-group.model';
@Injectable({
  providedIn: 'root'
})
export class BinGroupDataService {
  constructor(private apiService: ApiService) {}

  getBinGroups(
     pageIndex?: number,
     pageSize?: number,
     locationCode?:string,
     isActive?:boolean,
     isPageable?:boolean,
     sort?:string[]
     ): Observable<BinGroup[]> {
    const url = getBinGroupsUrl(pageIndex, pageSize,locationCode,isActive,isPageable,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => BinGroupDataAdaptor.binGroupDataFromJson(data)));
  }
  getBingroupCount(
    pageIndex?: number,
    pageSize?: number,
    locationCode?:string,
    isActive?:boolean,
    isPageable?:boolean,
    sort?:string[]):Observable<number>{
      const url = getBinGroupsUrl(pageIndex, pageSize,locationCode,isActive,isPageable,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => data.totalElements));
  }
  getBinGroupByCode(binGroupCode: string): Observable<BinGroup> {
    const url = getBinGroupByCodeUrl(binGroupCode);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => BinGroupDataAdaptor.binGroupFromJson(data)));
  }

}
