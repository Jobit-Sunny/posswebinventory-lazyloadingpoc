import { Injectable } from '@angular/core';
import { ApiService } from '../http/api.service';
import { Observable } from 'rxjs';
import { Item, ItemSummary, ItemSummaryConversion } from '../model/Item.model';
import { map } from 'rxjs/operators';
import { ItemDataAdaptor } from '../adaptors/item.adaptors';
import { getItemByCodeUrl, getItemSummaryByCodeUrl, getItemSummaryConversionCodeUrl } from '../endpoints.constants';
@Injectable({
  providedIn: 'root',
})
export class ItemDataService {
  constructor(private apiService: ApiService) {}

  getItemByCode(itemCode: string): Observable<Item> {
    const url = getItemByCodeUrl(itemCode);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => ItemDataAdaptor.ItemFromJson(data)));
  }

  getItemSummaryByCode(itemCode: string): Observable<ItemSummary> {
    const url = getItemSummaryByCodeUrl(itemCode);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => ItemDataAdaptor.ItemSummaryFromJson(data)));
  }
  getCoversionItemSummaryByItemCode(
    itemCode: string,
    lotNumber?:string): Observable<ItemSummaryConversion> {
      const url = getItemSummaryConversionCodeUrl(itemCode,lotNumber);
      return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => ItemDataAdaptor.ItemSummaryConversionFromJson(data)));

  }
}
