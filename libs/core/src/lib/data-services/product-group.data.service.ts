import { Injectable } from '@angular/core';
import { ApiService } from '../http/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { getProductGroupUrl, getProductGroupMasterUrl, getProductGroupSummaryByCodeUrl } from '../endpoints.constants';
import { ProductGroup, ProductGroupMaster } from '../model/product-group.model';
import { ProductGroupHelper, ProductGroupMasterHelper } from '../helpers/product-group.helper';
import { ProductGroupAdaptor } from '../adaptors/product-group.adaptors';
@Injectable({
  providedIn: 'root',
})
export class ProductGroupDataService {
  constructor(private apiService: ApiService) {}

  getProductGroups(): Observable<ProductGroup[]> {
    const url = getProductGroupUrl();
    return this.apiService
      .get(url)
      .pipe(map((data: any) => ProductGroupHelper.getProductGroups(data.results)));
  }
  getProductGroupMaster(
    pageIndex?:number,
    pageSize?:number,
    isActive?: boolean,
    isPageable?: boolean,
    sort?:string[]
    ): Observable<ProductGroupMaster[]> {
    const url = getProductGroupMasterUrl(pageIndex,pageSize,isActive,isPageable,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => ProductGroupMasterHelper.getProductGroupsMaster(data.results)));
  }
  getProductGroupMasterCount(
    pageIndex?:number,
    pageSize?:number,
    isActive?: boolean,
    isPageable?: boolean,
    sort?:string[]
  ):Observable<number>{
    const url = getProductGroupMasterUrl(pageIndex,pageSize,isActive,isPageable,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => data.totalElements));
  }

  getProductGroupSummaryByCode(productGroupCode: string): Observable<ProductGroup> {
    const url = getProductGroupSummaryByCodeUrl(productGroupCode);
    return this.apiService
      .get(url)
      .pipe(map((data: any) => ProductGroupAdaptor.productGroupDataFromJson(data)));
  }
}
