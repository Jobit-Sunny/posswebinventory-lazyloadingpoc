import { Injectable } from '@angular/core';
import { ApiService } from '../http/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {  getProductCategoryUrl, getProductCategoryMasterUrl } from '../endpoints.constants';
import { ProductCategory, ProductCategoryMaster } from '../model/product-category.model';
import { ProductCategoryHelper, ProductCategoryMasterHelper } from '../helpers/product-category.helper';

@Injectable({
  providedIn: 'root',
})
export class ProductCategoryDataService {
  constructor(private apiService: ApiService) {}

  getProductCategories(): Observable<ProductCategory[]> {
    const url = getProductCategoryUrl();
    return this.apiService
      .get(url)
      .pipe(map((data: any) => ProductCategoryHelper.getProductCategorys(data.results)));
  }

  getProductCategoriesMaster(
    pageIndex?:number,
    pageSize?:number,
    isActive?: boolean,
    isPageable?: boolean,
    sort?:string[]
    ): Observable<ProductCategoryMaster[]> {
    const url = getProductCategoryMasterUrl(pageIndex,pageSize,isActive,isPageable,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => ProductCategoryMasterHelper.getProductCategorysMaster(data.results)));
  }

  getProductCategoriesMasterCount(
    pageIndex?:number,
    pageSize?:number,
    isActive?: boolean,
    isPageable?: boolean,
    sort?:string[]
  ):Observable<number>{
    const url = getProductCategoryMasterUrl(pageIndex,pageSize,isActive,isPageable,sort);
    return this.apiService
      .get(url.path,url.params)
      .pipe(map((data: any) => data.totalElements));
  }
}
