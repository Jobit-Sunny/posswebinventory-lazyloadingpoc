import { createSelector } from '@ngrx/store';
import { selectlocationMappingState } from '../../core.state';

const selectLocations = createSelector(
  selectlocationMappingState,
  state => state.locations
);

const selectBrands = createSelector(
  selectlocationMappingState,
  state => state.brands
);

const selectRegions = createSelector(
  selectlocationMappingState,
  state => state.regions
);

const selectLevels = createSelector(
  selectlocationMappingState,
  state => state.levels
);

const selectCountries = createSelector(
  selectlocationMappingState,
  state => state.countries
);

const selectStates = createSelector(
  selectlocationMappingState,
  state => state.states
);

const selectTowns = createSelector(
  selectlocationMappingState,
  state => state.towns
);

const selectError = createSelector(
  selectlocationMappingState,
  state => state.error
);

export const LocationMappingSelectors = {
  selectLocations,
  selectBrands,
  selectRegions,
  selectLevels,
  selectCountries,
  selectStates,
  selectTowns,
  selectError
};
