import { Location } from './../../model/location.model';
import { State } from './../../model/state.model';
import { Town } from './../../model/town.model';
import { Country } from './../../model/country.model';
import { Region } from './../../model/region.model';
import { Brand } from './../../model/brand.model';
import { CustomErrors } from '@poss-web/core';
import { Lov } from '../../model/lov.model';

export interface LocationMappingState {
  locations: Location[];
  brands: Brand[];
  regions: Region[];
  levels: Lov[];
  countries: Country[];
  states: State[];
  towns: Town[];
  error: CustomErrors;
}
