import { LocationMappingState } from './location-mapping.state';
import {
  LocationMappingActionTypes,
  LocationMappingActions
} from './location-mapping.actions';

export const LOCATION_MAPPING_FEATURE_KEY = 'locationMapping';

/**
 * The initial state of the store
 */
const initialState: LocationMappingState = {
  locations: [],
  brands: [],
  regions: [],
  levels: [],
  countries: [],
  states: [],
  towns: [],
  error: null
};

/**
 * The reducer function which manipulates the store for respective Action
 */
export function LocationMappingReducer(
  state: LocationMappingState = initialState,
  action: LocationMappingActions
): LocationMappingState {
  switch (action.type) {
    case LocationMappingActionTypes.SEARCH_LOCAITONS_SUCCESS:
      return {
        ...state,
        locations: action.payload
      };
    case LocationMappingActionTypes.SEARCH_LOCAITONS_FAILURE:
    case LocationMappingActionTypes.LOAD_BRANDS_FAILURE:
    case LocationMappingActionTypes.LOAD_REGIONS_FAILURE:
    case LocationMappingActionTypes.LOAD_LEVELS_FAILURE:
    case LocationMappingActionTypes.LOAD_COUNTRIES_FAILURE:
    case LocationMappingActionTypes.LOAD_STATES_FAILURE:
    case LocationMappingActionTypes.LOAD_TOWNS_FAILURE:
      return {
        ...state,
        error: action.payload
      };
    case LocationMappingActionTypes.CLEAR:
      return {
        ...state,
        locations: [],
        brands: [],
        regions: [],
        levels: [],
        countries: [],
        states: [],
        towns: [],
        error: null
      };
    case LocationMappingActionTypes.LOAD_BRANDS_SUCCESS:
      return {
        ...state,
        brands: action.payload
      };

    case LocationMappingActionTypes.LOAD_REGIONS_SUCCESS:
      return {
        ...state,
        regions: action.payload
      };

    case LocationMappingActionTypes.LOAD_LEVELS_SUCCESS:
      return {
        ...state,
        levels: action.payload
      };

    case LocationMappingActionTypes.LOAD_COUNTRIES_SUCCESS:
      return {
        ...state,
        countries: action.payload
      };

    case LocationMappingActionTypes.LOAD_STATES_SUCCESS:
      return {
        ...state,
        states: action.payload
      };

    case LocationMappingActionTypes.LOAD_TOWNS_SUCCESS:
      return {
        ...state,
        towns: action.payload
      };

    default:
      return state;
  }
}
