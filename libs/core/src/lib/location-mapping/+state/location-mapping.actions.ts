import { Location } from './../../model/location.model';
import { Town } from './../../model/town.model';
import { State } from './../../model/state.model';
import { Country } from './../../model/country.model';
import { Lov } from './../../model/lov.model';
import { Region } from './../../model/region.model';
import { Action } from '@ngrx/store';
import { CustomErrors } from '@poss-web/core';
import { LocationMappingSearchResponse } from '@poss-web/shared';
import { Brand } from './../../model/brand.model';

export enum LocationMappingActionTypes {
  SEARCH_LOCAITONS = '[ Location-Mapping ] Search locations',
  SEARCH_LOCAITONS_SUCCESS = '[ Location-Mapping ] Search locations Success',
  SEARCH_LOCAITONS_FAILURE = '[ Location-Mapping ] Search locations Failure',

  CLEAR = '[ Location-Mapping ] Clear Data',

  LOAD_BRANDS = '[Location-Mapping] Load Brands',
  LOAD_BRANDS_SUCCESS = '[Location-Mapping] Load Brands Success',
  LOAD_BRANDS_FAILURE = '[Location-Mapping] Load Brands Failure',

  LOAD_REGIONS = '[Location-Mapping] Load Region',
  LOAD_REGIONS_SUCCESS = '[Location-Mapping] Load Region Success',
  LOAD_REGIONS_FAILURE = '[Location-Mapping] Load Region Failure',

  LOAD_LEVELS = '[Location-Mapping] Load Levels',
  LOAD_LEVELS_SUCCESS = '[Location-Mapping] Load Levels Success',
  LOAD_LEVELS_FAILURE = '[Location-Mapping]Load Levels Failure',

  LOAD_COUNTRIES = '[Location-Mapping] Load Countries',
  LOAD_COUNTRIES_SUCCESS = '[Location-Mapping] Load Countries Success',
  LOAD_COUNTRIES_FAILURE = '[Location-Mapping] Load Countries Failure',

  LOAD_STATES = '[Location-Mapping] Load States',
  LOAD_STATES_SUCCESS = '[Location-Mapping] Load States Success',
  LOAD_STATES_FAILURE = '[Location-Mapping] Load States Failure',

  LOAD_TOWNS = '[Location-Mapping] Load towns',
  LOAD_TOWNS_SUCCESS = '[Location-Mapping] Load Towns Success',
  LOAD_TOWNS_FAILURE = '[Location-Mapping] Load Towns Failure',

  RESET_ERROR = '[ Location-Mapping ] Reset Error'
}

export class SearchLocations implements Action {
  readonly type = LocationMappingActionTypes.SEARCH_LOCAITONS;
  constructor(public payload: LocationMappingSearchResponse) {}
}

export class SearchLocationsSuccess implements Action {
  readonly type = LocationMappingActionTypes.SEARCH_LOCAITONS_SUCCESS;
  constructor(public payload: Location[]) {}
}

export class SearchLocationsFailure implements Action {
  readonly type = LocationMappingActionTypes.SEARCH_LOCAITONS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class Clear implements Action {
  readonly type = LocationMappingActionTypes.CLEAR;
  constructor() {}
}

export class LoadBrands implements Action {
  readonly type = LocationMappingActionTypes.LOAD_BRANDS;
}

export class LoadBrandsSuccess implements Action {
  readonly type = LocationMappingActionTypes.LOAD_BRANDS_SUCCESS;
  constructor(public payload: Brand[]) {}
}

export class LoadBrandsFailure implements Action {
  readonly type = LocationMappingActionTypes.LOAD_BRANDS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadLevels implements Action {
  readonly type = LocationMappingActionTypes.LOAD_LEVELS;
}
export class LoadLevelsSuccess implements Action {
  readonly type = LocationMappingActionTypes.LOAD_LEVELS_SUCCESS;
  constructor(public payload: Lov[]) {}
}

export class LoadLevelsFailure implements Action {
  readonly type = LocationMappingActionTypes.LOAD_LEVELS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadRegions implements Action {
  readonly type = LocationMappingActionTypes.LOAD_REGIONS;
}
export class LoadRegionsSuccess implements Action {
  readonly type = LocationMappingActionTypes.LOAD_REGIONS_SUCCESS;
  constructor(public payload: Region[]) {}
}

export class LoadRegionsFailure implements Action {
  readonly type = LocationMappingActionTypes.LOAD_REGIONS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadCountries implements Action {
  readonly type = LocationMappingActionTypes.LOAD_COUNTRIES;
}

export class LoadCountriesSuccess implements Action {
  readonly type = LocationMappingActionTypes.LOAD_COUNTRIES_SUCCESS;
  constructor(public payload: Country[]) {}
}

export class LoadCountriesFailure implements Action {
  readonly type = LocationMappingActionTypes.LOAD_COUNTRIES_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadStates implements Action {
  readonly type = LocationMappingActionTypes.LOAD_STATES;
  constructor(public payload: string) {}
}

export class LoadStatesSuccess implements Action {
  readonly type = LocationMappingActionTypes.LOAD_STATES_SUCCESS;
  constructor(public payload: State[]) {}
}

export class LoadStatesFailure implements Action {
  readonly type = LocationMappingActionTypes.LOAD_STATES_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class LoadTowns implements Action {
  readonly type = LocationMappingActionTypes.LOAD_TOWNS;
  constructor(public payload: string) {}
}

export class LoadTownsSuccess implements Action {
  readonly type = LocationMappingActionTypes.LOAD_TOWNS_SUCCESS;
  constructor(public payload: Town[]) {}
}

export class LoadTownsFailure implements Action {
  readonly type = LocationMappingActionTypes.LOAD_TOWNS_FAILURE;
  constructor(public payload: CustomErrors) {}
}

export class ResetError implements Action {
  readonly type = LocationMappingActionTypes.RESET_ERROR;
}
/**
 * Stock Receive Actions Type
 */
export type LocationMappingActions =
  | SearchLocations
  | SearchLocationsSuccess
  | SearchLocationsFailure
  | Clear
  | LoadBrands
  | LoadBrandsSuccess
  | LoadBrandsFailure
  | LoadRegions
  | LoadRegionsSuccess
  | LoadRegionsFailure
  | LoadLevels
  | LoadLevelsSuccess
  | LoadLevelsFailure
  | LoadCountries
  | LoadCountriesSuccess
  | LoadCountriesFailure
  | LoadStates
  | LoadStatesSuccess
  | LoadStatesFailure
  | LoadTowns
  | LoadTownsSuccess
  | LoadTownsFailure
  | ResetError;
