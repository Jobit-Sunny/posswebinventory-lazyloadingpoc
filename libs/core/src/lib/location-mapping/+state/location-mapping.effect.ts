import { LocationDataService } from './../../data-services/location.data.service';
import { TownDataService } from './../../data-services/town.data.service';
import { StateDataService } from './../../data-services/state.data.service';
import { Town } from './../../model/town.model';
import { CountryDataService } from './../../data-services/country.data.service';
import { LovDataService } from './../../data-services/lov.data.service';
import { Lov } from './../../model/lov.model';
import { CustomErrorAdaptor } from './../../adaptors/error.adaptors';

import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';

import { map } from 'rxjs/operators';

import { DataPersistence } from '@nrwl/angular';
import { HttpErrorResponse } from '@angular/common/http';
import * as LocationMappingActions from './location-mapping.actions';
import { LocationMappingActionTypes } from './location-mapping.actions';
import { CustomErrors } from '../../model/error.model';
import { NotificationService } from '../../notification/notification.service';
import { BrandDataService } from '../../data-services/brand.data.service';
import { Brand } from '../../model/brand.model';
import { Region } from '../../model/region.model';
import { RegionDataService } from '../../data-services/region.data.service';
import { Country } from '../../model/country.model';
import { State } from '../../model/state.model';
import { Location } from '../../model/location.model';

/**
 * Location Master Effects
 */
@Injectable()
export class LocationMappingEffect {
  constructor(
    private dataPersistence: DataPersistence<any>,
    private notificationService: NotificationService,
    private brandDataService: BrandDataService,
    private regionDataService: RegionDataService,
    private lovDataService: LovDataService,
    private countryDataService: CountryDataService,
    private stateDataService: StateDataService,
    private townDataService: TownDataService,
    private locationDataService: LocationDataService
  ) {}
  /**
   *  The effect which handles the loadPendingFactorySTN Action
   */

  @Effect()
  searchLocations$ = this.dataPersistence.fetch(
    LocationMappingActionTypes.SEARCH_LOCAITONS,
    {
      run: (action: LocationMappingActions.SearchLocations) => {
        return this.locationDataService
          .getLocations(
            {
              brands: action.payload.brands,
              regions: action.payload.regions,
              levels: action.payload.levels,
              countries: action.payload.countries,
              states: action.payload.states,
              towns: action.payload.towns
            },
            true,
            null,
            null,
            false
          )
          .pipe(
            map(
              (data: Location[]) =>
                new LocationMappingActions.SearchLocationsSuccess(data)
            )
          );
      },
      onError: (
        action: LocationMappingActions.SearchLocations,
        error: HttpErrorResponse
      ) => {
        return new LocationMappingActions.SearchLocationsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadBrands$ = this.dataPersistence.fetch(
    LocationMappingActionTypes.LOAD_BRANDS,
    {
      run: (action: LocationMappingActions.LoadBrands) => {
        return this.brandDataService
          .getBrands(null, null, true, false)
          .pipe(
            map(
              (data: Brand[]) =>
                new LocationMappingActions.LoadBrandsSuccess(data)
            )
          );
      },
      onError: (
        action: LocationMappingActions.LoadBrands,
        error: HttpErrorResponse
      ) => {
        return new LocationMappingActions.LoadBrandsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadRegions$ = this.dataPersistence.fetch(
    LocationMappingActionTypes.LOAD_REGIONS,
    {
      run: (action: LocationMappingActions.LoadRegions) => {
        return this.regionDataService
          .getRegions('REGION', null, null, true, false)
          .pipe(
            map(
              (data: Region[]) =>
                new LocationMappingActions.LoadRegionsSuccess(data)
            )
          );
      },
      onError: (
        action: LocationMappingActions.LoadRegions,
        error: HttpErrorResponse
      ) => {
        return new LocationMappingActions.LoadRegionsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadLevels$ = this.dataPersistence.fetch(
    LocationMappingActionTypes.LOAD_LEVELS,
    {
      run: (action: LocationMappingActions.LoadLevels) => {
        return this.lovDataService
          .getLocationLovs('OWNERTYPE')
          .pipe(
            map(
              (data: Lov[]) =>
                new LocationMappingActions.LoadLevelsSuccess(data)
            )
          );
      },
      onError: (
        action: LocationMappingActions.LoadLevels,
        error: HttpErrorResponse
      ) => {
        return new LocationMappingActions.LoadLevelsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadCountries$ = this.dataPersistence.fetch(
    LocationMappingActionTypes.LOAD_COUNTRIES,
    {
      run: (action: LocationMappingActions.LoadCountries) => {
        return this.countryDataService
          .getCountries(null, null, true, false)
          .pipe(
            map(
              (data: Country[]) =>
                new LocationMappingActions.LoadCountriesSuccess(data)
            )
          );
      },
      onError: (
        action: LocationMappingActions.LoadCountries,
        error: HttpErrorResponse
      ) => {
        return new LocationMappingActions.LoadCountriesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadStates$ = this.dataPersistence.fetch(
    LocationMappingActionTypes.LOAD_STATES,
    {
      run: (action: LocationMappingActions.LoadStates) => {
        return this.stateDataService
          .getStates(null, null, true, action.payload, false)
          .pipe(
            map(
              (data: State[]) =>
                new LocationMappingActions.LoadStatesSuccess(data)
            )
          );
      },
      onError: (
        action: LocationMappingActions.LoadStates,
        error: HttpErrorResponse
      ) => {
        return new LocationMappingActions.LoadStatesFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  @Effect()
  loadTowns$ = this.dataPersistence.fetch(
    LocationMappingActionTypes.LOAD_TOWNS,
    {
      run: (action: LocationMappingActions.LoadTowns) => {
        return this.townDataService
          .getTowns(null, null, true, action.payload, false)
          .pipe(
            map(
              (data: Town[]) =>
                new LocationMappingActions.LoadTownsSuccess(data)
            )
          );
      },
      onError: (
        action: LocationMappingActions.LoadTowns,
        error: HttpErrorResponse
      ) => {
        return new LocationMappingActions.LoadTownsFailure(
          this.errorHandler(error)
        );
      }
    }
  );

  errorHandler(error: HttpErrorResponse): CustomErrors {
    const customError: CustomErrors = CustomErrorAdaptor.fromJson(error);
    this.notificationService.error(customError);
    return customError;
  }
}
