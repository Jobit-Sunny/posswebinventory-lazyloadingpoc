import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { LocationMappingSelectors } from './location-mapping.selectors';
import * as LocationMappingActions from './location-mapping.actions';
import { LocationMappingSearchResponse } from '@poss-web/shared';
import { AppState } from '@poss-web/core';

@Injectable()
export class LocationMappingFacade {
  private location$ = this.store.select(
    LocationMappingSelectors.selectLocations
  );
  private brands$ = this.store.select(LocationMappingSelectors.selectBrands);
  private regions$ = this.store.select(LocationMappingSelectors.selectRegions);
  private levels$ = this.store.select(LocationMappingSelectors.selectLevels);
  private countries$ = this.store.select(
    LocationMappingSelectors.selectCountries
  );
  private states$ = this.store.select(LocationMappingSelectors.selectStates);
  private towns$ = this.store.select(LocationMappingSelectors.selectTowns);

  private error$ = this.store.select(LocationMappingSelectors.selectError);

  constructor(private store: Store<AppState>) {}

  /**
   * Access for the State selectors
   */
  getLocations() {
    return this.location$;
  }

  getBrands() {
    return this.brands$;
  }

  getRegions() {
    return this.regions$;
  }

  getLevels() {
    return this.levels$;
  }

  getCountries() {
    return this.countries$;
  }
  getStates() {
    return this.states$;
  }
  getTowns() {
    return this.towns$;
  }

  getError() {
    return this.error$;
  }

  loadBrands() {
    this.store.dispatch(new LocationMappingActions.LoadBrands());
  }
  loadRegions() {
    this.store.dispatch(new LocationMappingActions.LoadRegions());
  }
  loadLevels() {
    this.store.dispatch(new LocationMappingActions.LoadLevels());
  }

  loadCountries() {
    this.store.dispatch(new LocationMappingActions.LoadCountries());
  }

  loadStates(stateCode: string) {
    this.store.dispatch(new LocationMappingActions.LoadStates(stateCode));
  }
  loadTowns(townCode: string) {
    this.store.dispatch(new LocationMappingActions.LoadTowns(townCode));
  }

  clear() {
    this.store.dispatch(new LocationMappingActions.Clear());
  }

  searchLocations(filter: LocationMappingSearchResponse) {
    this.store.dispatch(new LocationMappingActions.SearchLocations(filter));
  }
}
