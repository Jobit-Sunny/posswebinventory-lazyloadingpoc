import { Location } from './../model/location.model';
import { Lov } from './../model/lov.model';
import { CustomErrors } from './../model/error.model';
import {
  LocationMappingServiceConfig,
  LocationMappingServiceResponse
} from './models/location-mapping.model';
import { LocationMappingFacade } from './+state/location-mapping.facade';
import { takeUntil } from 'rxjs/operators';
import { Injectable, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import {
  LocationMappingComponent,
  LocationMappingSearchResponse,
  LocationMappingApplyResponse,
  LocationMappingConfig,
  OverlayNotificationService,
  OverlayNotificationType,
  OverlayNotificationEventRef
} from '@poss-web/shared';
import { Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Brand } from '../model/brand.model';
import { Region } from '../model/region.model';
import { Country } from '../model/country.model';
import { Town } from '../model/town.model';
import { State } from '../model/state.model';
@Injectable()
export class LocationMappingService {
  initialLocationMappingConfig: LocationMappingConfig = {
    filterOptions: {
      brands: [],
      regions: [],
      levels: [],
      countries: [],
      states: [],
      towns: []
    },
    minFilterSelection: 1,
    maxFilterOptionsSelection: 5,
    selectedLocations: null
  };

  locations: string[] = [];

  constructor(
    private dialog: MatDialog,
    private locationMappingFacade: LocationMappingFacade,
    public translateService: TranslateService,
    private overlayNotification: OverlayNotificationService
  ) {}

  open(
    serviceConfig: LocationMappingServiceConfig
  ): EventEmitter<LocationMappingServiceResponse> {
    this.locationMappingFacade.clear();
    this.locationMappingFacade.loadBrands();
    this.locationMappingFacade.loadRegions();
    this.locationMappingFacade.loadLevels();
    this.locationMappingFacade.loadCountries();

    const destroy$ = new Subject();
    const locationMappingConfig = this.initialLocationMappingConfig;

    const config = {
      ...locationMappingConfig,
      selectedLocations: serviceConfig.selectedLocations,
      template: serviceConfig.template
    };
    const dialogref = this.dialog.open(LocationMappingComponent, {
      width: '1000px',
      autoFocus: false,
      data: config
    });
    const componentInstance = dialogref.componentInstance;
    const event = new EventEmitter<LocationMappingServiceResponse>();

    this.locationMappingFacade
      .getError()
      .pipe(takeUntil(destroy$))
      .subscribe((error: CustomErrors) => {
        if (error) {
          this.errorHandler(error, destroy$);
        }
      });

    this.locationMappingFacade
      .getBrands()
      .pipe(takeUntil(destroy$))
      .subscribe((brands: Brand[]) => {
        locationMappingConfig.filterOptions.brands = brands.map(brand => ({
          id: brand.brandCode,
          description: brand.brandCode
        }));
      });

    this.locationMappingFacade
      .getRegions()
      .pipe(takeUntil(destroy$))
      .subscribe((regions: Region[]) => {
        locationMappingConfig.filterOptions.regions = regions.map(region => ({
          id: region.regionCode,
          description: region.regionCode
        }));
      });

    this.locationMappingFacade
      .getLevels()
      .pipe(takeUntil(destroy$))
      .subscribe((levels: Lov[]) => {
        locationMappingConfig.filterOptions.levels = levels.map(level => ({
          id: level.code,
          description: level.code
        }));
      });

    this.locationMappingFacade
      .getCountries()
      .pipe(takeUntil(destroy$))
      .subscribe((countries: Country[]) => {
        locationMappingConfig.filterOptions.countries = countries.map(
          country => ({
            id: '' + country.countryCode,
            description: country.description
          })
        );
      });

    componentInstance.search
      .pipe(takeUntil(destroy$))
      .subscribe((response: LocationMappingSearchResponse) => {
        this.locationMappingFacade.searchLocations(response);
      });

    this.locationMappingFacade
      .getLocations()
      .pipe(takeUntil(destroy$))
      .subscribe((locations: Location[]) => {
        componentInstance.setLocations(
          locations.map(location => ({
            id: location.locationCode,
            description: location.locationCode
          }))
        );
      });
    this.locationMappingFacade
      .getStates()
      .pipe(takeUntil(destroy$))
      .subscribe((states: State[]) => {
        locationMappingConfig.filterOptions.states = states.map(country => ({
          id: '' + country.stateCode,
          description: country.description
        }));
      });

    this.locationMappingFacade
      .getTowns()
      .pipe(takeUntil(destroy$))
      .subscribe((towns: Town[]) => {
        locationMappingConfig.filterOptions.towns = towns.map(country => ({
          id: '' + country.townCode,
          description: country.description
        }));
      });

    componentInstance.countryChange
      .pipe(takeUntil(destroy$))
      .subscribe((countryCode: string) => {
        this.locationMappingFacade.loadStates(countryCode);
      });

    componentInstance.stateChange
      .pipe(takeUntil(destroy$))
      .subscribe((stateCode: string) => {
        this.locationMappingFacade.loadTowns(stateCode);
      });

    dialogref
      .afterClosed()
      .pipe(takeUntil(destroy$))
      .subscribe(
        (dailogResponse: {
          type: string;
          data: LocationMappingApplyResponse;
        }) => {
          event.emit(dailogResponse);
          destroy$.next();
          destroy$.complete();
        }
      );

    return event;
  }

  errorHandler(error: CustomErrors, destroy$: Subject<any>) {
    this.overlayNotification
      .show({
        type: OverlayNotificationType.ERROR,
        hasClose: true,
        error: error
      })
      .events.pipe(takeUntil(destroy$))
      .subscribe((event: OverlayNotificationEventRef) => {});
  }
}
