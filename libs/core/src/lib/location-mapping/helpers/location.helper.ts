import { LocationAdaptor } from './../adaptors/location.adaptors';
import { LocationData } from '../models/location-mapping.model';

export class LocationHelper {
  static getLocations(data: any): LocationData[] {
    const locations: LocationData[] = [];
    for (const location of data) {
      locations.push(LocationAdaptor.fromJson(location));
    }
    return locations;
  }
}
