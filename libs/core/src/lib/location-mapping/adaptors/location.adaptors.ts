import { LocationData } from '../models/location-mapping.model';

export class LocationAdaptor {
  static fromJson(data: any): LocationData {
    return {
      locationCode: data.locationCode,
      address: data.address
    };
  }
}
