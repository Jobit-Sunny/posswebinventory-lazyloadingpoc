import { LocationMappingApplyResponse } from '@poss-web/shared';
import { TemplateRef } from '@angular/core';

export interface LocationMappingServiceConfig {
  selectedLocations: { id: string; description: string }[];
  template?: TemplateRef<any>;
}

export interface LocationMappingServiceResponse {
  type: string;
  data?: LocationMappingApplyResponse;
}
