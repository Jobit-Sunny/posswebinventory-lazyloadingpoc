import { BinCode } from '../model/bin-codes.model';
import { BinCodeAdaptor } from '../adaptors/bin-code.adaptor';

export class BinCodeHelper {
  static getBinCodes(data: any): BinCode[] {
    const BinCodes: BinCode[] = [];
    for (const binCode of data) {
      BinCodes.push(BinCodeAdaptor.binCodeDataFromJson(binCode));
    }
    return BinCodes;
  }
}
