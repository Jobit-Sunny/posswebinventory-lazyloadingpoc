import { Lov } from '../model/lov.model';
import { LovAdaptor } from '../adaptors/lov.adaptors';

export class LovHelper {
  static getLovs(data: any): Lov[] {
    const Lovs: Lov[] = [];
    for (const lov of data) {
      Lovs.push(LovAdaptor.LovDataFromJson(lov));
    }
    return Lovs;
  }
}
