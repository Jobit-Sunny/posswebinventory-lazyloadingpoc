import { ProductGroup, ProductGroupMaster } from '../model/product-group.model';
import { ProductGroupAdaptor, ProductGroupMasterAdaptor } from '../adaptors/product-group.adaptors';

export class ProductGroupHelper {
  static getProductGroups(data: any): ProductGroup[] {
    const productGroups: ProductGroup[] = [];
    for (const productGroup of data) {
      productGroups.push(ProductGroupAdaptor.productGroupDataFromJson(productGroup));
    }
    return productGroups;
  }
}
export class ProductGroupMasterHelper {
  static getProductGroupsMaster(data: any): ProductGroupMaster[] {
    const productGroups: ProductGroupMaster[] = [];
    for (const productGroup of data) {
      productGroups.push(ProductGroupMasterAdaptor.productGroupMasterDataFromJson(productGroup));
    }
    return productGroups;
  }
}
