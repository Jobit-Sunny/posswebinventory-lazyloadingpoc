import { ProductCategory, ProductCategoryMaster } from '../model/product-category.model';
import { ProductCategoryAdaptor, ProductCategoryMasterAdaptor } from '../adaptors/product-category.adaptors';

export class ProductCategoryHelper {
  static getProductCategorys(data: any): ProductCategory[] {
    const productCategorys: ProductCategory[] = [];
    for (const productCategory of data) {
      productCategorys.push(ProductCategoryAdaptor.productCategoryDataFromJson(productCategory));
    }
    return productCategorys;
  }
}
export class ProductCategoryMasterHelper {
  static getProductCategorysMaster(data: any): ProductCategoryMaster[] {
    const productCategorys: ProductCategoryMaster[] = [];
    for (const productCategory of data) {
      productCategorys.push(ProductCategoryMasterAdaptor.productCategoryMasterDataFromJson(productCategory));
    }
    return productCategorys;
  }
}
