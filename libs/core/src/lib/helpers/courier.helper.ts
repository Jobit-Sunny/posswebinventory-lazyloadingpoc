import { Courier } from '../model/courier.model';
import { CourierAdaptor } from '../adaptors/courier.adaptors';

export class CourierHelper {
  static getCouriers(data: any): Courier[] {
    const couriers: Courier[] = [];
    for (const courier of data) {
      couriers.push(CourierAdaptor.courierDataFromJson(courier));
    }
    return couriers;
  }
}
