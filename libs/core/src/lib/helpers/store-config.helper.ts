import { StoreConfig } from '../model/store-config.model';
import { StoreConfigAdaptor } from '../adaptors/store-config.adaptors';
import { StoreBin } from '../model/store-config-bins.model';

export class StoreConfigHelper {
  static getbins(data: any): StoreBin[] {
    const storeBins: StoreBin[] = [];
    for (const storeBin of data) {
      storeBins.push(StoreConfigAdaptor.StoreBinDataFromJson(storeBin));
    }
    return storeBins;
  }
}
