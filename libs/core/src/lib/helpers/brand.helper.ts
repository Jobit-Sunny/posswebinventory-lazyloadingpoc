import { Brand } from '../model/brand.model';
import {BrandAdaptor} from '../adaptors/brand.adaptors';
export class BrandHelper {
  static getBrands(data: any): Brand[] {
    const brands: Brand[] = [];
    for (const brand of data) {
      brands.push(BrandAdaptor.brandDataFromJson(brand));
    }
    return brands;
  }
}
