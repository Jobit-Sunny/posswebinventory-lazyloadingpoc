import { Region } from '../model/region.model';
import {RegionAdaptor} from '../adaptors/region.adaptors';
export class RegionHelper {
  static getRegions(data: any): Region[] {
    const regions: Region[] = [];
    for (const region of data) {
      regions.push(RegionAdaptor.regionDataFromJson(region));
    }
    return regions;
  }
}
