import { StoreUser } from '../model/store-user.model';
import { StoreUserAdaptor } from '../adaptors/store-user.adaptors';

export class StoreUserHelper {
  static getStoreUsers(data: any): StoreUser[] {
    const StoreUsers: StoreUser[] = [];
    for (const storeUser of data) {
      StoreUsers.push(StoreUserAdaptor.StoreUserDataFromJson(storeUser));
    }
    return StoreUsers;
  }
}
