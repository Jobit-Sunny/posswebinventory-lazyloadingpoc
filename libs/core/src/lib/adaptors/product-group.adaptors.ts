import { ProductGroup, ProductGroupMaster } from '../model/product-group.model';

export class ProductGroupAdaptor {
  static productGroupDataFromJson(data: any): ProductGroup{
       return{
        productGroupCode: data.productGroupCode,
        description: data.description
       }

  }
}
export class ProductGroupMasterAdaptor {
  static productGroupMasterDataFromJson(data: any): ProductGroupMaster{
       return{
        configDetails: data.configDetails,
        description: data.description,
        isActive: data.isActive,
        orgCode: data.orgCode,
        productGroupCode: data.productGroupCode,
        productType:data.productType,
        materialCode:data.materialCode
       }

  }
}
