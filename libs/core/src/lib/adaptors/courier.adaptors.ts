import { Courier } from '../model/courier.model';

export class CourierAdaptor {
  static courierDataFromJson(data: any): Courier {
    return {
      address: data.address,
      contactPerson: data.contactPerson,
      courierName: data.courierName,
      isActive: data.isActive,
      mailId: data.mailId,
      mobileNumber: data.mobileNumber,
      phoneNumber: data.phoneNumber,
      stateCode: data.stateCode,
      townCode: data.townCode
    };
  }
}
