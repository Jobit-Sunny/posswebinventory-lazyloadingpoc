import { Country } from '../model/country.model';

export class CountryDataAdaptor {
  static countryDataFromJson(data: any): Country[] {
    const countryData: Country[] = [];
    for (const country of data.results) {
      countryData.push({
        countryCode: country.countryCode,
        currencyCode: country.currencyCode,
        currencySymbol: country.currencySymbol,
        dateFormat: country.dateFormat,
        description: country.description,
        isActive: country.isActive,
        locale: country.locale,
        phoneLength:country.phoneLength,
      });
    }
    return countryData;
  }

  static countryFromJson(data: any): Country {
    if (!data ) {
      return null;
    }

    const country: Country = {
      countryCode: data.countryCode,
        currencyCode: data.currencyCode,
        currencySymbol: data.currencySymbol,
        dateFormat: data.dateFormat,
        description: data.description,
        isActive: data.isActive,
        locale: data.locale,
        phoneLength:data.phoneLength,
    };
    return country;
  }
}
