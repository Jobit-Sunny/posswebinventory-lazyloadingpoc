import { Region, RegionSummary } from '../model/region.model';

export class RegionAdaptor {
  static regionDataFromJson(data: any): Region{
       return{
        configDetails: data.configDetails,
        description: data.description,
        isActive: data.isActive,
        parentRegionCode: data.parentRegionCode,
        regionCode: data.regionCode,
       }

  }
  static regionSummaryDataFromJson(data: any): RegionSummary{
    return{
     isActive: data.isActive,
     regionCode: data.regionCode,
    }

}
}
