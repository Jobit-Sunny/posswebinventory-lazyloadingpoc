import { Brand } from '../model/brand.model';

export class BrandAdaptor {
  static brandDataFromJson(data: any): Brand{
       return{
        brandCode: data.brandCode,
        configDetails: data.configDetails,
        description: data.description,
        isActive: data.isActive,
        orgCode: data.orgCode,
        parentBrandCode: data.parentBrandCode,
       }

  }
}
