import { BinCode } from '../model/bin-codes.model';

export class BinCodeAdaptor {
  static binCodeDataFromJson(data: any): BinCode{
       return{
        binCode: data.binCode,
        description: data.binCode,
        isActive: data.binCode
       }

  }
}
