import { Item, ItemSummary, ItemSummaryConversion } from '../model/item.model';
export class ItemDataAdaptor {

  static ItemFromJson(data: any): Item {
    if (!data ) {
      return null;
    }

    const item: Item = {
      brandCode: data.brandCode,
      complexityCode: data.complexityCode,
      configDetails: data.configDetails,
      description: data.description,
      indentTypeCode:data.indentTypeCode,
      isActive: data.isActive,
      itemCode: data.itemCode,
      itemDetails: data.itemDetails,
      leadTime: data.leadTime,
      materialCode: data.materialCode,
      orgCode:data.orgCode,
      parentItemCode: data.parentItemCode,
      pricingGroupType: data.pricingGroupType,
      productCategoryCode: data.productCategoryCode,
      productGroupCode: data.productGroupCode,
      stdValue: data.stdValue,
      stdWeight: data.stdWeight,
    };
    return item;
  }

  static ItemSummaryFromJson(data: any): ItemSummary {
    if (!data ) {
      return null;
    }

    const item: ItemSummary = {
      itemCode: data.itemCode,
      productCategoryCode: data.productCategoryCode,
      productGroupCode: data.productGroupCode,
      stdValue: data.stdValue,
    };
    return item;
  }
  static ItemSummaryConversionFromJson(data: any): ItemSummaryConversion {
    if (!data ) {
      return null;
    }

    const item: ItemSummaryConversion = {
      childItems: data.childItems,
      complexityCode: data.complexityCode,
      description: data.description,
      itemCode: data.itemCode,
      lotNumber: data.lotNumber,
      mfgDate: data.mfgDate,
      parentItemCode: data.parentItemCode,
      productCategory: data.productCategory,
      productGroupCode: data.productGroupCode,
      productType: data.productType,
      stdValue: data.stdValue,
      stdWeight: data.stdWeight,
      stoneDetails: {}

    };
    return item;
  }
}
