import { Lov } from '../model/lov.model';

export class LovAdaptor {
  static LovDataFromJson(data: any): Lov{
       return{
        code: data.code,
        isActive: data.isActive,
        value: data.value,
       }

  }
}
