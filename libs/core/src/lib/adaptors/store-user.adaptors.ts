import { StoreUser, StoreUserDetails } from '../model/store-user.model';
import * as moment from 'moment';
export class StoreUserAdaptor {
  static StoreUserDataFromJson(data: any): StoreUser{
       return{
        empName: data.empName,
        employeeCode: data.employeeCode,
        isActive: data.isActive,
        isLocked: data.isLocked,
        isLoginActive: data.isLoginActive,
        locationCode: data.locationCode,
        orgCode: data.orgCode,
        primaryRoleCode: data.primaryRoleCode,
        primaryRoleName: data.primaryRoleName,
        regionCode: data.regionCode,
        userType: data.userType
       }

  }
  static StoreUserDetailsFromJson(data: any): StoreUserDetails{
    return{
      address: data.address,
      birthDate: moment(data.birthDate),
      emailId: data.emailId,
      empName: data.empName,
      employeeCode: data.employeeCode,
      forcePasswordChange: data.forcePasswordChange,
      hasLoginAccess: data.hasLoginAccess,
      isActive: data.isActive,
      isLocked: data.isLocked,
      isLoginActive: data.isLoginActive,
      joiningDate: moment(data.joiningDate),
      locationCode: data.locationCode,
      mobileNo: data.mobileNo,
      orgCode: data.orgCode,
      regionCode: data.regionCode,
      resignationDate: moment(data.resignationDate),
      roles: data.roles,
      userType: data.userType,

    }

}
}
