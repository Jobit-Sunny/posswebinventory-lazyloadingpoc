import { ProductCategory, ProductCategoryMaster } from '../model/product-category.model';

export class ProductCategoryAdaptor {
  static productCategoryDataFromJson(data: any): ProductCategory{
       return{
        productCategoryCode: data.productCategoryCode,
        description: data.description
       }

  }
}
export class ProductCategoryMasterAdaptor {
  static productCategoryMasterDataFromJson(data: any): ProductCategoryMaster{
       return{
        configDetails: data.configDetails,
        description: data.description,
        isActive: data.isActive,
        orgCode: data.orgCode,
        productCategoryCode: data.productCategoryCode
       }

  }
}
