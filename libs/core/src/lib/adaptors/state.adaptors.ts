import { State } from '../model/state.model';


export class StateDataAdaptor {
  static stateDataFromJson(data: any): State[] {
    const stateData: State[] = [];
    for (const state of data.results) {
      stateData.push({
        configDetails: state.configDetails,
        countryCode: state.countryCode,
        description: state.description,
        isActive: state.isActive,
        stateCode: state.stateCode
      });
    }
    return stateData;
  }

  static stateFromJson(data: any): State {
    if (!data ) {
      return null;
    }

    const state: State = {
        configDetails: data.configDetails,
        countryCode: data.countryCode,
        description: data.description,
        isActive: data.isActive,
        stateCode: data.stateCode
    };
    return state;
  }
}
