import { Town } from '../model/town.model';


export class TownDataAdaptor {
  static townDataFromJson(data: any): Town[] {
    const townData: Town[] = [];
    for (const town of data.results) {
      townData.push({
        description: town.description,
        isActive: town.isActive,
        markupAmount: town.markupAmount,
        markupFactor: town.markupFactor,
        regionCode: town.regionCode,
        stateCode: town.stateCode,
        townCode: town.townCode
      });
    }
    return townData;
  }

  static townFromJson(data: any): Town {
    if (!data ) {
      return null;
    }

    const town: Town = {
      description: data.description,
      isActive: data.isActive,
      markupAmount: data.markupAmount,
      markupFactor: data.markupFactor,
      regionCode: data.regionCode,
      stateCode: data.stateCode,
      townCode: data.townCode
    };
    return town;
  }
}
