import { HttpParams } from '@angular/common/http';
import { LocationFilter } from './model/location-filter.model';

const getLocationBaseUrl = (): string => {
  return `/location/v1`;
};
const getStoreConfigLocationBaseUrl = (): string => {
  return `/location/v2`;
};
const getInventoryBaseUrl = (): string => {
  return `/inventory/v2`;
};

export const getProductBaseUrl = (): string => {
  return `/product/v1`;
};
export const getStoreUserBaseUrl = (): string => {
  return `/user/v1`;
};
export const getStoreBaseUrl = (): string => {
  return `/product/v1`;
}
export const getLocationsUrl = (
  filter?: LocationFilter,
  isActive?: boolean,
  pageIndex?: number,
  pageSize?: number,
  isPageable?: boolean,
  sort?: string[]
): {
  path: string;
  params: HttpParams;
} => {
  let params = new HttpParams();
  if (isActive !== null && isActive !== undefined) {
    params = params.append('isActive', isActive.toString());
  }
  if (pageIndex !== null && pageIndex !== undefined) {
    params = params.append('page', pageIndex.toString());
  }
  if (pageSize !== null && pageSize !== undefined) {
    params = params.append('size', pageSize.toString());
  }
  if (filter && filter.brands) {
    for (let i = 0; i < filter.brands.length; i++) {
      params = params.append('brandCode', filter.brands[i]);
    }
  }
  if (filter && filter.regions) {
    for (let i = 0; i < filter.regions.length; i++) {
      params = params.append('regionCode', filter.regions[i]);
    }
  }
  if (filter && filter.levels) {
    for (let i = 0; i < filter.levels.length; i++) {
      params = params.append('ownerTypeCode', filter.levels[i]);
    }
  }
  if (filter && filter.countries) {
    for (let i = 0; i < filter.countries.length; i++) {
      params = params.append('countryCode', filter.countries[i]);
    }
  }
  if (filter && filter.states) {
    for (let i = 0; i < filter.states.length; i++) {
      params = params.append('stateCode', filter.states[i]);
    }
  }
  if (filter && filter.towns) {
    for (let i = 0; i < filter.towns.length; i++) {
      params = params.append('townCode', filter.towns[i]);
    }
  }
  if (filter && filter.cfa) {
    for (let i = 0; i < filter.cfa.length; i++) {
      params = params.append('cfaCodes', filter.cfa[i]);
    }
  }
  if (filter && filter.factory) {
    for (let i = 0; i < filter.factory.length; i++) {
      params = params.append('factoryCode', filter.factory[i]);
    }
  }
  if (filter && filter.locationTypes) {
    for (let i = 0; i < filter.locationTypes.length; i++) {
      params = params.append('locationTypes', filter.locationTypes[i]);
    }
  }
  if (sort) {
    sort.forEach(sortvalue => {
      params = params.append('sort', sortvalue);
    });
  }
  if (isPageable !== null && isPageable !== undefined) {
    params = params.append('isPageable', isPageable.toString());
  }
  return { path: getLocationBaseUrl() + '/locations', params };
};
export const getLocationByCodeUrl = (locationCode): string => {
  return getLocationBaseUrl() + `/locations/${locationCode}`;
};

export const getCountriesUrl = (
  pageIndex?: number,
  pageSize?: number,
  isActive?: boolean,
  isPageable?: boolean,
  sort?: string[]
): {
  path: string;
  params: HttpParams;
} => {
  const path = getLocationBaseUrl() + `/countries`;
  let params = new HttpParams();
  if (isActive !== null && isActive !== undefined) {
    params = params.append('isActive', isActive.toString());
  }
  if (pageIndex !== null && pageIndex !== undefined) {
    params = params.append('page', pageIndex.toString());
  }
  if (pageSize !== null && pageSize !== undefined) {
    params = params.append('size', pageSize.toString());
  }
  if (sort) {
    sort.forEach(sortvalue => {
      params = params.append('sort', sortvalue);
    });
  }
  if (isPageable !== null && isPageable !== undefined) {
    params = params.append('isPageable', isPageable.toString());
  }
  return {
    path,
    params
  };
};
export const getCountryByCodeUrl = (countryCode): string => {
  return getLocationBaseUrl() + `/countries/${countryCode}`;
};

export const getStatesUrl = (
  pageIndex?: number,
  pageSize?: number,
  isActive?: boolean,
  countryCode?: string,
  isPageable?: boolean,
  sort?: string[]
): {
  path: string;
  params: HttpParams;
} => {
  const path = getLocationBaseUrl() + `/states`;
  let params = new HttpParams();
  if (countryCode) {
    params = params.append('countryCode', countryCode);
  }
  if (isActive !== null && isActive !== undefined) {
    params = params.append('isActive', isActive.toString());
  }
  if (pageIndex !== null && pageIndex !== undefined) {
    params = params.append('page', pageIndex.toString());
  }
  if (pageSize !== null && pageSize !== undefined) {
    params = params.append('size', pageSize.toString());
  }
  if (sort) {
    sort.forEach(sortvalue => {
      params = params.append('sort', sortvalue);
    });
  }
  if (isPageable !== null && isPageable !== undefined) {
    params = params.append('isPageable', isPageable.toString());
  }
  return {
    path,
    params
  };
};
export const getStateByCodeUrl = (stateCode): string => {
  return getLocationBaseUrl() + `/states/${stateCode}`;
};

export const getTownsUrl = (
  pageIndex?: number,
  pageSize?: number,
  isActive?: boolean,
  stateCode?: string,
  isPageable?: boolean,
  sort?: string[]
): {
  path: string;
  params: HttpParams;
} => {
  const path = getLocationBaseUrl() + `/towns`;
  let params = new HttpParams();
  if (stateCode) {
    params = params.append('stateCode', stateCode);
  }
  if (isActive !== null && isActive !== undefined) {
    params = params.append('isActive', isActive.toString());
  }
  if (pageIndex !== null && pageIndex !== undefined) {
    params = params.append('page', pageIndex.toString());
  }
  if (pageSize !== null && pageSize !== undefined) {
    params = params.append('size', pageSize.toString());
  }
  if (sort) {
    sort.forEach(sortvalue => {
      params = params.append('sort', sortvalue);
    });
  }
  if (isPageable !== null && isPageable !== undefined) {
    params = params.append('isPageable', isPageable.toString());
  }
  return {
    path,
    params
  };
};
export const getTownByCodeUrl = (townCode): string => {
  return getLocationBaseUrl() + `/towns/${townCode}`;
};

export const getBinGroupsUrl = (
  pageIndex?: number,
  pageSize?: number,
  locationCode?: string,
  isActive?: boolean,
  isPageable?: boolean,
  sort?: string[]
): {
  path: string;
  params: HttpParams;
} => {
  /* return (
    getInventoryBaseUrl() + `/bingroups?page=${pageIndex}&size=${pageSize}`
  ); */
  const path = getInventoryBaseUrl() + '/bingroups';
  let params = new HttpParams();
  if (locationCode) {
    params = params.append('locationCode', locationCode);
  }
  if (isActive !== null && isActive !== undefined) {
    params = params.append('isActive', isActive.toString());
  }
  if (isPageable !== null && isPageable !== undefined) {
    params = params.append('isPageable', isPageable.toString());
  }
  if (pageIndex !== null && pageIndex !== undefined) {
    params = params.append('page', pageIndex.toString());
  }
  if (pageSize !== null && pageSize !== undefined) {
    params = params.append('size', pageSize.toString());
  }
  if (sort) {
    sort.forEach(sortvalue => {
      params = params.append('sort', sortvalue);
    });
  }
  return {
    path,
    params
  };
};
export const getBinGroupByCodeUrl = (binGroupCode): string => {
  return getInventoryBaseUrl() + `/bingroups/${binGroupCode}`;
};

export const getBinByCodeUrl = (binCode): string => {
  return getInventoryBaseUrl() + `/bins/${binCode}`;
};
const getLovsBaseUrl = (): string => {
  return `/lovs`;
};

export const getProductCategoryUrl = (): string => {
  return getProductBaseUrl() + '/stores/product-categories';
};
export const getProductGroupUrl = (): string => {
  return getProductBaseUrl() + '/stores/product-groups';
};
export const getCourierUrl = (
  pageIndex?: number,
  pageSize?: number,
  isActive?: boolean,
  locationCode?: string,
  isPageable?: boolean,
  sort?: string[]
): { path: string; params: HttpParams } => {
  const path = getInventoryBaseUrl() + '/couriers';
  let params = new HttpParams();
  if (locationCode) {
    params = params.append('locationCode', locationCode);
  }
  if (isActive !== null && isActive !== undefined) {
    params = params.append('isActive', isActive.toString());
  }
  if (pageIndex !== null && pageIndex !== undefined) {
    params = params.append('page', pageIndex.toString());
  }
  if (pageSize !== null && pageSize !== undefined) {
    params = params.append('size', pageSize.toString());
  }
  if (sort) {
    sort.forEach(sortvalue => {
      params = params.append('sort', sortvalue);
    });
  }
  if (isPageable !== null && isPageable !== undefined) {
    params = params.append('isPageable', isPageable.toString());
  }
  return {
    path,
    params
  };
};
export const getStoreUserUrl = (
  pageIndex?: number,
  pageSize?: number,
  isActive?: boolean,
  searchField?: string,
  roleCodes?: string[],
  sort?: string[]
): { path: string; params: HttpParams } => {
  const path = getStoreUserBaseUrl() + '/store/user';
  let params = new HttpParams();
  if (searchField) {
    params = params.append('searchField', searchField);
  }
  if (isActive !== null && isActive !== undefined) {
    params = params.append('isActive', isActive.toString());
  }
  if (pageIndex !== null && pageIndex !== undefined) {
    params = params.append('page', pageIndex.toString());
  }
  if (pageSize !== null && pageSize !== undefined) {
    params = params.append('size', pageSize.toString());
  }
  if (sort) {
    sort.forEach(sortvalue => {
      params = params.append('sort', sortvalue);
    });
  }
  if (roleCodes) {
    roleCodes.forEach(roleCodevalue => {
      params = params.append('roleCodes', roleCodevalue);
    });
  }
  return {
    path,
    params
  };
};

export const getLocationSummaryUrl = (): string => {
  return getStoreConfigLocationBaseUrl() + '/stores/locations';
};
export const getItemByCodeUrl = (itemCode): string => {
  return getProductBaseUrl() + `/items/${itemCode}`;
};
export const getInventoryLovsEndpointUrl = (lovType: string): string => {
  return getInventoryBaseUrl() + getLovsBaseUrl() + `/${lovType}`;
};
export const getLocationLovsEndpointUrl = (lovType: string): string => {
  return getLocationBaseUrl() + getLovsBaseUrl() + `/${lovType}`;
};
export const getProductLovsEndpointUrl = (lovType: string): string => {
  return getProductBaseUrl() + getLovsBaseUrl() + `/${lovType}`;
};
export const getBinDetailsUrl = (
  binGroupCode: string,
  isActive?: boolean,
  isPageable?: boolean,
  locationCode?: string
): { path: string; params: HttpParams } => {
  const path = getInventoryBaseUrl() + `/bingroups/${binGroupCode}/bins`;
  let params = new HttpParams();
  if (locationCode) {
    params = params.append('locationCode', locationCode);
  }
  if (isActive !== null && isActive !== undefined) {
    params = params.append('isActive', isActive.toString());
  }
  if (isPageable !== null && isPageable !== undefined) {
    params = params.append('isPageable', isPageable.toString());
  }
  return {
    path,
    params
  };
};
export const getStoreBinsUrl = (
  binType: string
): { path: string; params: HttpParams } => {
  const path = getInventoryBaseUrl() + '/stores/bins';
  const params = new HttpParams().set('binType', binType);
  return {
    path,
    params
  };
};
export const getBrandsUrl = (
  pageIndex?: number,
  pageSize?: number,
  isActive?: boolean,
  isPageable?: boolean,
  sort?: string[]
): { path: string; params: HttpParams } => {
  const path = getProductBaseUrl() + '/brands';
  let params = new HttpParams();
  if (isActive !== null && isActive !== undefined) {
    params = params.append('isActive', isActive.toString());
  }
  if (pageIndex !== null && pageIndex !== undefined) {
    params = params.append('page', pageIndex.toString());
  }
  if (pageSize !== null && pageSize !== undefined) {
    params = params.append('size', pageSize.toString());
  }
  if (sort) {
    sort.forEach(sortvalue => {
      params = params.append('sort', sortvalue);
    });
  }
  if (isPageable !== null && isPageable !== undefined) {
    params = params.append('isPageable', isPageable.toString());
  }
  return {
    path,
    params
  };
};
export const getBrandsbyBrandCodeUrl = (brandCode: string): string => {
  return getProductBaseUrl() + `/brands/${brandCode}`;
};
export const getRegionsUrl = (
  regionType: string,
  pageIndex?: number,
  pageSize?: number,
  isActive?: boolean,
  isPageable?: boolean,
  parentRegionCode?: string,
  sort?: string[]
): { path: string; params: HttpParams } => {
  const path = getLocationBaseUrl() + '/regions';
  let params = new HttpParams();

  if (regionType !== null && regionType !== undefined) {
    params = params.append('regionType', regionType);
  }

  if (isActive !== null && isActive !== undefined) {
    params = params.append('isActive', isActive.toString());
  }
  if (pageIndex !== null && pageIndex !== undefined) {
    params = params.append('page', pageIndex.toString());
  }
  if (pageSize !== null && pageSize !== undefined) {
    params = params.append('size', pageSize.toString());
  }
  if (parentRegionCode) {
    params = params.append('parentRegionCode', parentRegionCode);
  }
  if (sort) {
    sort.forEach(sortvalue => {
      params = params.append('sort', sortvalue);
    });
  }
  if (isPageable !== null && isPageable !== undefined) {
    params = params.append('isPageable', isPageable.toString());
  }
  return {
    path,
    params
  };
};
export const getProductCategoryMasterUrl = (
  pageIndex?: number,
  pageSize?: number,
  isActive?: boolean,
  isPageable?: boolean,
  sort?: string[]
): { path: string; params: HttpParams } => {
  const path = getProductBaseUrl() + '/product-categories';
  let params = new HttpParams();
  if (isActive !== null && isActive !== undefined) {
    params = params.append('isActive', isActive.toString());
  }
  if (pageIndex !== null && pageIndex !== undefined) {
    params = params.append('page', pageIndex.toString());
  }
  if (pageSize !== null && pageSize !== undefined) {
    params = params.append('size', pageSize.toString());
  }
  if (sort) {
    sort.forEach(sortvalue => {
      params = params.append('sort', sortvalue);
    });
  }
  if (isPageable !== null && isPageable !== undefined) {
    params = params.append('isPageable', isPageable.toString());
  }
  return {
    path,
    params
  };
};
export const getProductGroupMasterUrl = (
  pageIndex?: number,
  pageSize?: number,
  isActive?: boolean,
  isPageable?: boolean,
  sort?: string[]
): { path: string; params: HttpParams } => {
  const path = getProductBaseUrl() + '/product-groups';
  let params = new HttpParams();
  if (isActive !== null && isActive !== undefined) {
    params = params.append('isActive', isActive.toString());
  }
  if (pageIndex !== null && pageIndex !== undefined) {
    params = params.append('page', pageIndex.toString());
  }
  if (pageSize !== null && pageSize !== undefined) {
    params = params.append('size', pageSize.toString());
  }
  if (sort) {
    sort.forEach(sortvalue => {
      params = params.append('sort', sortvalue);
    });
  }
  if (isPageable !== null && isPageable !== undefined) {
    params = params.append('isPageable', isPageable.toString());
  }
  return {
    path,
    params
  };
};

export const getIBTLocationsSummaryUrl = (
  pageIndex?: number,
  pageSize?: number,
  regionType?:string,
  locationType?:string[],
  ownerTypeCode?:string[],
  sort?:string[]
): {
  path: string;
  params: HttpParams;
} => {
  let params = new HttpParams();
  if (pageIndex !== null && pageIndex !== undefined) {
    params = params.append('page', pageIndex.toString());
  }
  if (pageSize !== null && pageSize !== undefined) {
    params = params.append('size', pageSize.toString());
  }

  if (ownerTypeCode) {
    for (let i = 0; i < ownerTypeCode.length; i++) {
      params = params.append('ownerTypeCode', ownerTypeCode[i]);
    }
  }
  if (locationType) {
    for (let i = 0; i < locationType.length; i++) {
      params = params.append('locationType', locationType[i]);
    }
  }
  if (sort) {
    sort.forEach(sortvalue => {
      params = params.append('sort', sortvalue);
    });
  }
  if (regionType && regionType!=='') {
      params = params.append('regionType', regionType);
  }
  return { path: getStoreConfigLocationBaseUrl() + '/stores/ibt/locations', params };
};
export const getLocationSummaryByCodeUrl = (locationCode): string => {
  return getStoreConfigLocationBaseUrl() + `/stores/locations/${locationCode}`;
};

export const getRegionSummaryByCodeUrl = (regionCode): string => {
  return getStoreConfigLocationBaseUrl() + `/stores/regions/${regionCode}`;
};
export const getProductGroupSummaryByCodeUrl = (productGroupCode): string => {
  return getProductBaseUrl() + `/stores/product-groups/${productGroupCode}`;
};
export const getItemSummaryByCodeUrl = (itemCode): string => {
  return getProductBaseUrl() + `/stores/items/${itemCode}`;
};
export const getItemSummaryConversionCodeUrl = (
  itemCode: string,
  lotNumber?:string): { path: string; params: HttpParams } => {
   let params = new HttpParams();
   if(lotNumber && lotNumber!==''){
     params = params.append('lotNumber',lotNumber);
   }
  return{
    path:getProductBaseUrl() + `/stores/conversion/${itemCode}`,
    params:params
  }

};
export const getCourierSummaryUrl = (
  pageIndex?: number,
  pageSize?: number,
  isActive?: boolean,
  sort?: string[]
): { path: string; params: HttpParams } => {
  const path = getInventoryBaseUrl() + '/stores/couriers';
  let params = new HttpParams();
  if (isActive !== null && isActive !== undefined) {
    params = params.append('isActive', isActive.toString());
  }
  if (pageIndex !== null && pageIndex !== undefined) {
    params = params.append('page', pageIndex.toString());
  }
  if (pageSize !== null && pageSize !== undefined) {
    params = params.append('size', pageSize.toString());
  }
  if (sort) {
    sort.forEach(sortvalue => {
      params = params.append('sort', sortvalue);
    });
  }
  return {
    path,
    params
  };
};


export const getStoreUserDetailsUrl =(employeeCode:string): string => {
    return getStoreUserBaseUrl() + `/store/user/${employeeCode}`;
}
