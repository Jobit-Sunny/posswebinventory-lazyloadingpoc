import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ApiService } from '../http/api.service';
import {
  generateOtpUrl,
  verifyOtpUrl
} from './one-time-password-endpoints.constants';

@Injectable({
  providedIn: 'root'
})
export class OneTimePasswordService {
  constructor(private apiService: ApiService) {}

  generateOtp(empCode: string): Observable<boolean> {
    return this.apiService
      .post(generateOtpUrl(), { empCode })
      .pipe(map((data: any) => !!data));
  }

  verifyOtp(otpDetails: any) {
    return this.apiService
      .patch(verifyOtpUrl(), {
        ...otpDetails,
        newPassword: window.btoa(otpDetails.newPassword)
      })
      .pipe(map((data: any) => !!data));
  }
}
