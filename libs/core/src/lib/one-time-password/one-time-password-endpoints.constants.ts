const getUserRoleMgmntBaseUrl = (): string => '/user/v1';

export const generateOtpUrl = (): string => {
  return getUserRoleMgmntBaseUrl() + '/user/forgot-password';
};

export const verifyOtpUrl = (): string => {
  return getUserRoleMgmntBaseUrl() + '/user/verify-otp';
};
