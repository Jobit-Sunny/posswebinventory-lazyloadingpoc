import { AppsettingsState } from './app-setting/appsetting.model';
import { createFeatureSelector, ActionReducerMap } from '@ngrx/store';
import { routerReducer, RouterReducerState } from '@ngrx/router-store';
import {
  appsettingReducer,
  APPSETTING_FEATURE_KEY
} from './app-setting/appsetting.reducer';
import { Auth_FEATURE_KEY, authReducer } from './auth/+state/auth.reducer';
import { Params } from '@angular/router';
import { AuthState } from './auth/+state/auth.state';
import {
  LocationMappingReducer,
  LOCATION_MAPPING_FEATURE_KEY
} from './location-mapping/+state/location-mapping.reducer';
import { LocationMappingState } from './location-mapping/+state/location-mapping.state';

export const reducers: ActionReducerMap<AppState> = {
  appsetting: appsettingReducer,
  router: routerReducer,
  auth: authReducer,
  locationMapping: LocationMappingReducer
};

export interface RouterStateUrl {
  url: string;
  params: Params;
  queryParams: Params;
}

export interface AppState {
  appsetting: AppsettingsState;
  router: RouterReducerState<RouterStateUrl>;
  auth: AuthState;
  locationMapping: LocationMappingState;
}

export const selectAppsettingState = createFeatureSelector<
  AppState,
  AppsettingsState
>(APPSETTING_FEATURE_KEY);

export const selectAuthState = createFeatureSelector<AppState, AuthState>(
  Auth_FEATURE_KEY
);

export const selectlocationMappingState = createFeatureSelector<
  AppState,
  LocationMappingState
>(LOCATION_MAPPING_FEATURE_KEY);
