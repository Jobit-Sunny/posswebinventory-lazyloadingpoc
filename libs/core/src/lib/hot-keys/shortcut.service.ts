import { Injectable, Inject } from '@angular/core';
import { HotkeyModule, Hotkey, HotkeysService } from 'angular2-hotkeys';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AppConfigService } from '../config/app-config.service';

class HotkeyConfig {
  [key: string]: string[];
}

// hotkey model
class ConfigModel {
  hotkeys: HotkeyConfig;
}

// command model
export class Command {
  name: string;
  combo: string;
  event: KeyboardEvent;
}

@Injectable({
  providedIn: 'root'
})
export class ShortcutService {
  private subject: Subject<Command>;
  commands: Observable<Command>;

  constructor(
    private hotkeysService: HotkeysService,
    private http: HttpClient,
    @Inject('env') private environment
  ) {
    this.subject = new Subject<Command>();
    this.commands = this.subject.asObservable();

    console.log(
      'shortcut.service constructor -- ' +
        `${
          AppConfigService.setting.shortcutConfigSetting.shortcutConfigFilePath
        }`
    );
    // call to get data from config.json
    http
      .get(
        `${
          AppConfigService.setting.shortcutConfigSetting.shortcutConfigFilePath
        }`
      )
      .toPromise()
      .then((data: any) => data as ConfigModel)
      .then(dataJson => {
        for (const key in dataJson.hotkeys) {
          if (dataJson.hotkeys.hasOwnProperty(key)) {
            const commands = dataJson.hotkeys[key];
            hotkeysService.add(
              new Hotkey(
                key,
                (event, combo) =>
                  this.shortcutkeyCommand(event, combo, commands),
                ['INPUT', 'SELECT', 'TEXTAREA']
              )
            );
          }
        }
      });
  }
  // command for the clicked key
  shortcutkeyCommand(
    eachEvent: KeyboardEvent,
    eachCombo: string,
    commands: string[]
  ): boolean {
    commands.forEach(eachCommand => {
      const command: Command = {
        name: eachCommand,
        event: eachEvent,
        combo: eachCombo
      };

      this.subject.next(command);
    });
    return true;
  }
}
