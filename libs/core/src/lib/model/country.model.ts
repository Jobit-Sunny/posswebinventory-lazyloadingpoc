export interface Country {
      countryCode: number,
      currencyCode: string,
      currencySymbol: string,
      dateFormat: string,
      description: string,
      isActive: true,
      locale: string,
      phoneLength: number
}
