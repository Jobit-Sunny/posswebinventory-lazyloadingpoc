import { Moment } from 'moment';

export interface StoreUser {
  empName: string,
  employeeCode: string,
  isActive: boolean,
  isLocked: boolean,
  isLoginActive: boolean,
  locationCode: string,
  orgCode: string,
  primaryRoleCode: string,
  primaryRoleName: string,
  regionCode: string,
  userType: string

}

export interface StoreUserDetails {
  address: {},
  birthDate: Moment,
  emailId: string,
  empName: string,
  employeeCode: string,
  forcePasswordChange: boolean,
  hasLoginAccess: boolean,
  isActive: boolean,
  isLocked: boolean,
  isLoginActive: boolean,
  joiningDate: Moment,
  locationCode: string,
  mobileNo: string,
  orgCode: string,
  regionCode: string,
  resignationDate: Moment,
  roles: UserRole[],
  userType: string

}
export interface UserRole {
  corpAccess: boolean;
  description: string;
  expiryTime: Moment;
  isPrimary: boolean;
  roleCode: string;
  roleName: string;
  startTime: Moment;
}
