export interface Lov {
  code: string;
  isActive: true;
  value: string;
}
