export interface Town {
  description: string,
  isActive: boolean,
  markupAmount: number,
  markupFactor: number,
  regionCode: string,
  stateCode: number,
  townCode: number
}
