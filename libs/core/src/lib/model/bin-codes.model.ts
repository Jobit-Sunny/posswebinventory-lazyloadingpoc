export interface BinCode {
  binCode: string;
  description: string;
  isActive: true;
}
