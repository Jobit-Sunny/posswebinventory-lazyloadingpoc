export interface State {
  configDetails: {},
  countryCode: number,
  description: string,
  isActive: boolean,
  stateCode: number
}
