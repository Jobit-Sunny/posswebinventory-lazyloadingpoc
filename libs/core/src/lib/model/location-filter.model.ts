export interface LocationFilter {
  brands?: string[];
  regions?: string[];
  levels?: string[];
  countries?: string[];
  states?: string[];
  towns?: string[];
  cfa?: string[];
  factory?: string[];
  locationTypes?: string[];
}
