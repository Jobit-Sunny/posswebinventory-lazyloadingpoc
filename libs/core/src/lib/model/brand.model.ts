export interface Brand {
  brandCode: string;
  configDetails: {};
  description: string;
  isActive: boolean;
  orgCode: string;
  parentBrandCode: string;
}
