import { Moment } from 'moment';


export interface Item {
  brandCode: string,
  complexityCode: string,
  configDetails: {},
  description: string,
  indentTypeCode: string,
  isActive: boolean,
  itemCode: string,
  itemDetails: {},
  leadTime: number,
  materialCode: string,
  orgCode: string,
  parentItemCode: string,
  pricingGroupType: string,
  productCategoryCode: string,
  productGroupCode: string,
  stdValue: number,
  stdWeight: number
}
export interface ItemSummary {
  itemCode: string,
  productCategoryCode: string,
  productGroupCode: string,
  stdValue: number
}
export interface ItemSummaryConversion {
  childItems: any[],
  complexityCode: string,
  description: string,
  itemCode: string,
  lotNumber: string,
  mfgDate: Moment,
  parentItemCode: string,
  productCategory: string,
  productGroupCode: string,
  productType: string,
  stdValue: number,
  stdWeight: number,
  stoneDetails: {}
}
