export interface Region {
  configDetails: {};
  description: string;
  isActive: boolean;
  parentRegionCode: string;
  regionCode: string;
}
export interface RegionSummary {
  isActive: boolean;
  regionCode: string;
}
