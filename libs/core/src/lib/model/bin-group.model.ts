export interface BinGroup {
  binGroupCode: string;
  description: string;
  isActive: boolean;
}
