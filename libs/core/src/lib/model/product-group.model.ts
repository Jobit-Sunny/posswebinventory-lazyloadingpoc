export interface ProductGroup {
  description: string,
  productGroupCode: string
}
export interface ProductGroupMaster{
  configDetails: {},
  description: string,
  isActive: boolean,
  materialCode: string,
  orgCode: string,
  productGroupCode: string,
  productType: string

}
