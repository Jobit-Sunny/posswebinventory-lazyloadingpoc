import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppConfigService } from '../config/app-config.service';
import { tap } from 'rxjs/internal/operators/tap';
import { catchError } from 'rxjs/internal/operators/catchError';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private hostUrl: string;
  private authroizationHeader: string = null;
  private contentTypeHeader: string = null;
  private fromHeader: string = null;
  private responseContentType: string = null;

  get ContentTypeHeader(): string {
    return this.contentTypeHeader;
  }
  set ContentTypeHeader(value: string) {
    this.contentTypeHeader = value;
  }

  get AuthorizationHeader(): string {
    return this.authroizationHeader;
  }
  set AuthorizationHeader(value: string) {
    this.authroizationHeader = value;
  }

  get ResponseContentType(): any {
    return this.responseContentType;
  }
  set ResponseContentType(value: any) {
    this.responseContentType = value;
  }

  get FromHeader(): string {
    return this.fromHeader;
  }
  set FromHeader(value: string) {
    this.fromHeader = value;
  }
  constructor(private http: HttpClient) {}

  get(url: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http
      .get(`${AppConfigService.setting.apiServer.apiURL}${url}`, {
        headers: this.headers,
        responseType: this.ResponseContentType,
        params
      })
      .pipe(
        tap(data => {
          this.resetProperties();
        }),
        catchError(err => {
          this.resetProperties();
          throw err;
        })
      );
  }
  post(
    url: string,
    data: Object = {},
    params: HttpParams = new HttpParams()
  ): Observable<any> {
    return this.http
      .post(
        `${AppConfigService.setting.apiServer.apiURL}${url}`,
        JSON.stringify(data),
        {
          headers: this.headers,
          params
        }
      )
      .pipe(
        tap(() => {
          this.resetProperties();
        }),
        catchError(err => {
          this.resetProperties();
          throw err;
        })
      );
  }

  postFile(url: string, data: Object = {}): Observable<any> {
    return this.http
      .post(`${AppConfigService.setting.apiServer.apiURL}${url}`, data)
      .pipe(
        tap(() => {
          this.resetProperties();
        })
      );
  }

  put(
    url: string,
    data: Object = {},
    params: HttpParams = new HttpParams()
  ): Observable<any> {
    return this.http
      .put(
        `${AppConfigService.setting.apiServer.apiURL}${url}`,
        JSON.stringify(data),
        {
          headers: this.headers,
          params
        }
      )
      .pipe(
        tap(() => {
          this.resetProperties();
        }),
        catchError(err => {
          this.resetProperties();
          throw err;
        })
      );
  }

  patch(
    url: string,
    data: Object = {},
    params: HttpParams = new HttpParams()
  ): Observable<any> {
    return this.http
      .patch(
        `${AppConfigService.setting.apiServer.apiURL}${url}`,
        JSON.stringify(data),
        {
          headers: this.headers,
          params
        }
      )
      .pipe(
        tap(() => {
          this.resetProperties();
        }),
        catchError(err => {
          this.resetProperties();
          throw err;
        })
      );
  }

  delete(url: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http
      .delete(`${AppConfigService.setting.apiServer.apiURL}${url}`, {
        headers: this.headers,
        params
      })
      .pipe(
        tap(() => {
          this.resetProperties();
        }),
        catchError(err => {
          this.resetProperties();
          throw err;
        })
      );
  }

  get headers(): HttpHeaders {
    const headersConfig = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      From: '',
      Authorization: ''
    };

    if (this.contentTypeHeader !== null) {
      headersConfig['Content-Type'] = this.contentTypeHeader;
    }
    if (this.authroizationHeader !== null) {
      headersConfig.Authorization = this.authroizationHeader;
    }
    if (this.fromHeader !== null) {
      headersConfig.From = this.fromHeader;
    }
    return new HttpHeaders(headersConfig);
  }

  private resetProperties() {
    this.contentTypeHeader = 'application/json';
    this.responseContentType = '';
    this.authroizationHeader = '';
  }
}
