import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import { OverlayNotificationService } from '@poss-web/shared';

@Injectable()
export class OverlayNotificationEffects {
  constructor(
    private actions$: Actions,
    private overlayNotification: OverlayNotificationService
  ) {}
  @Effect({ dispatch: false }) overlayClose = this.actions$.pipe(
    ofType('@ngrx/router-store/navigated'),
    tap(action => {
      this.overlayNotification.close();
    })
  );
}
