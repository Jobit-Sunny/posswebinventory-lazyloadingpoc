/**
 *  This file is used to store the mapping between Custom Error Code and translated Error Messages.
 *  Also hold the Custom Error code Enumeration. if the for some reason the Error code is changed in API
 *  layer then we can check the value of the Enumeration element without change the other coding logic in the
 *  code base.
 */

export enum ErrorEnums {
  // General Errors
  ERR_401 = '401',
  ERR_403 = '403',

  ERR_CORE_001 = 'ERR-CORE-001',
  ERR_CORE_002 = 'ERR-CORE-002',
  ERR_CORE_003 = 'ERR-CORE-003',
  ERR_CORE_004 = 'ERR-CORE-004',
  ERR_CORE_005 = 'ERR-CORE-005',
  ERR_CORE_006 = 'ERR-CORE-006',
  ERR_CORE_007 = 'ERR-CORE-007',
  ERR_CORE_008 = 'ERR-CORE-008',
  ERR_CORE_009 = 'ERR-CORE-009',
  ERR_CORE_010 = 'ERR-CORE-010',
  ERR_CORE_011 = 'ERR-CORE-011',
  ERR_CORE_012 = 'ERR-CORE-012',
  ERR_AUTH_001 = 'ERR-AUTH-001',
  ERR_AUTH_002 = 'ERR-AUTH-002',
  ERR_AUTH_003 = 'ERR-AUTH-003',
  ERR_AUTH_004 = 'ERR-AUTH-004',
  ERR_AUTH_005 = 'ERR-AUTH-005',
  ERR_AUTH_006 = 'ERR-AUTH-006',
  ERR_AUTH_007 = 'ERR-AUTH-007',
  ERR_AUTH_008 = 'ERR-AUTH-008',
  ERR_AUTH_009 = 'ERR-AUTH-009',
  ERR_AUTH_010 = 'ERR-AUTH-010',
  ERR_AUTH_011 = 'ERR-AUTH-011',
  ERR_AUTH_012 = 'ERR-AUTH-012',
  ERR_AUTH_013 = 'ERR-AUTH-013',
  ERR_AUTH_014 = 'ERR-AUTH-014',
  ERR_AUTH_015 = 'ERR-AUTH-015',
  ERR_INV_001 = 'ERR-INV-001',
  ERR_INV_002 = 'ERR-INV-002',
  ERR_INV_003 = 'ERR-INV-003',
  ERR_INV_004 = 'ERR-INV-004',
  ERR_INV_005 = 'ERR-INV-005',
  ERR_INV_006 = 'ERR-INV-006',
  ERR_INV_007 = 'ERR-INV-007',
  ERR_INV_008 = 'ERR-INV-008',
  ERR_INV_009 = 'ERR-INV-009',
  ERR_INV_010 = 'ERR-INV-010',
  ERR_INV_011 = 'ERR-INV-011',
  ERR_INV_012 = 'ERR-INV-012',
  ERR_INV_013 = 'ERR-INV-013',
  ERR_INV_014 = 'ERR-INV-014',
  ERR_INV_015 = 'ERR-INV-015',
  ERR_INV_016 = 'ERR-INV-016',
  ERR_INV_017 = 'ERR-INV-017',
  ERR_INV_018 = 'ERR-INV-018',
  ERR_INV_019 = 'ERR-INV-019',
  ERR_INV_020 = 'ERR-INV-020',
  ERR_INV_021 = 'ERR-INV-021',
  ERR_INV_022 = 'ERR-INV-022',
  ERR_INV_023 = 'ERR-INV-023',
  ERR_INV_024 = 'ERR-INV-024',
  ERR_INV_025 = 'ERR-INV-025',
  ERR_INV_026 = 'ERR-INV-026',
  ERR_INV_027 = 'ERR-INV-027',
  ERR_INV_028 = 'ERR-INV-028',
  ERR_INV_029 = 'ERR-INV-029',
  ERR_INV_030 = 'ERR-INV-030',
  ERR_INV_031 = 'ERR-INV-031',
  ERR_INV_032 = 'ERR-INV-032',
  ERR_INV_033 = 'ERR-INV-033',
  ERR_INV_034 = 'ERR-INV-034',
  ERR_INV_035 = 'ERR-INV-035',
  ERR_INV_036 = 'ERR-INV-036',
  ERR_PRO_003 = 'ERR-PRO-003',
  ERR_PRO_004 = 'ERR-PRO-004',
  ERR_PRO_005 = 'ERR-PRO-005',
  ERR_PRO_006 = 'ERR-PRO-006',
  ERR_PRO_007 = 'ERR-PRO-007',
  ERR_PRO_008 = 'ERR-PRO-008',
  ERR_PRO_009 = 'ERR-PRO-009',
  ERR_PRO_010 = 'ERR-PRO-010',
  ERR_PRO_011 = 'ERR-PRO-011',
  ERR_PRO_012 = 'ERR-PRO-012',
  ERR_PRO_013 = 'ERR-PRO-013',
  ERR_PRO_014 = 'ERR-PRO-014',
  ERR_PRO_015 = 'ERR-PRO-015',
  ERR_PRO_016 = 'ERR-PRO-016',
  ERR_INV_071 = 'ERR-INV-071',
  ERR_INV_089 = 'ERR-INV-089',
  ERR_INV_31 = 'ERR-INV-31',

  //MASTERS ERRORS
  ERR_MAS_001 = 'ERR-MAS-001',

  //PRO ERRORS
  ERR_PRO_002 = 'ERR-PRO-002',
  ERR_PRO_001 = 'ERR-PRO-001',

  // UAM ERRORS
  ERR_UAM_001 = 'ERR-UAM-001',
  ERR_UAM_002 = 'ERR-UAM-002',
  ERR_UAM_004 = 'ERR-UAM-004',
  ERR_UAM_006 = 'ERR-UAM-006',
  ERR_UAM_007 = 'ERR-UAM-007',
  ERR_UAM_011 = 'ERR-UAM-011',
  ERR_UAM_012 = 'ERR-UAM-012',
  ERR_UAM_013 = 'ERR-UAM-013',
  ERR_UAM_014 = 'ERR-UAM-014',
  ERR_UAM_015 = 'ERR-UAM-015',
  ERR_UAM_016 = 'ERR-UAM-016',
  ERR_UAM_017 = 'ERR-UAM-017',
  ERR_UAM_018 = 'ERR-UAM-018',
  ERR_UAM_019 = 'ERR-UAM-019',
  ERR_UAM_021 = 'ERR-UAM-021',
  ERR_UAM_023 = 'ERR-UAM-023',
  ERR_UAM_024 = 'ERR-UAM-024',
  ERR_UAM_025 = 'ERR-UAM-025',
  ERR_UAM_026 = 'ERR-UAM-026',
  ERR_UAM_028 = 'ERR-UAM-028',
  ERR_UAM_029 = 'ERR-UAM-029',
  ERR_UAM_030 = 'ERR-UAM-030',
  ERR_UAM_031 = 'ERR-UAM-031',
  ERR_UAM_032 = 'ERR-UAM-032',
  ERR_UAM_035 = 'ERR-UAM-035',
  ERR_UAM_037 = 'ERR-UAM-037',
  ERR_UAM_038 = 'ERR-UAM-038',
  ERR_UAM_039 = 'ERR-UAM-039',
  ERR_UAM_040 = 'ERR-UAM-040',
  ERR_UAM_041 = 'ERR-UAM-041',
  ERR_UAM_043 = 'ERR-UAM-043',
  ERR_UAM_044 = 'ERR-UAM-044',
  ERR_UAM_045 = 'ERR-UAM-045',
  ERR_UAM_046 = 'ERR-UAM-046',
  ERR_UAM_047 = 'ERR-UAM-047',
  ERR_UAM_048 = 'ERR-UAM-048',
  ERR_UAM_049 = 'ERR-UAM-049',
  ERR_UAM_050 = 'ERR-UAM-050',
  ERR_UAM_051 = 'ERR-UAM-051',
  ERR_UAM_052 = 'ERR-UAM-052',
  ERR_UAM_053 = 'ERR-UAM-053',
  ERR_UAM_054 = 'ERR-UAM-054',
  ERR_UAM_055 = 'ERR-UAM-055',
  ERR_UAM_056 = 'ERR-UAM-056',
  ERR_UAM_057 = 'ERR-UAM-057',
  ERR_UAM_058 = 'ERR-UAM-058',
  ERR_UAM_059 = 'ERR-UAM-059',
  ERR_UAM_060 = 'ERR-UAM-060',
  ERR_UAM_061 = 'ERR-UAM-061',
  ERR_UAM_062 = 'ERR-UAM-062',
  ERR_UAM_064 = 'ERR-UAM-064',
  ERR_UAM_065 = 'ERR-UAM-065',
  ERR_UAM_066 = 'ERR-UAM-066',
  ERR_UAM_067 = 'ERR-UAM-067',
  ERR_UAM_068 = 'ERR-UAM-068',
  ERR_UAM_069 = 'ERR-UAM-069',
  ERR_UAM_070 = 'ERR-UAM-070',

  ERR_LOC_001 = 'ERR-LOC-001',
  ERR_LOC_002 = 'ERR-LOC-002',
  ERR_LOC_003 = 'ERR-LOC-003',
  ERR_LOC_004 = 'ERR-LOC-004',
  ERR_LOC_005 = 'ERR-LOC-005',
  ERR_LOC_006 = 'ERR-LOC-006',
  ERR_LOC_007 = 'ERR_LOC_007',
  ERR_LOC_008 = 'ERR-LOC-008',
  ERR_LOC_009 = 'ERR-LOC-009',
  ERR_LOC_010 = 'ERR-LOC-010',
  ERR_LOC_011 = 'ERR-LOC-011',
  ERR_LOC_012 = 'ERR-LOC-012',
  ERR_LOC_013 = 'ERR-LOC-013',
  ERR_LOC_014 = 'ERR-LOC-014',
  ERR_LOC_015 = 'ERR-LOC-015',
  ERR_LOC_016 = 'ERR-LOC-016',

  ERR_INV_SCH_001 = 'ERR-INV-SCH-001 ',
  ERR_INV_SCH_002 = 'ERR-INV-SCH-002 '
}

export const ErrorTranslateKeyMap = new Map();
// General Errors
ErrorTranslateKeyMap.set(ErrorEnums.ERR_401, 'pw.errorMessages.ERR-401');
ErrorTranslateKeyMap.set(ErrorEnums.ERR_403, 'pw.errorMessages.ERR-403');

ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_CORE_001,
  'pw.errorMessages.ERR-CORE-001'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_CORE_002,
  'pw.errorMessages.ERR-CORE-002'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_CORE_003,
  'pw.errorMessages.ERR-CORE-003'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_CORE_004,
  'pw.errorMessages.ERR-CORE-004'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_CORE_005,
  'pw.errorMessages.ERR-CORE-005'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_CORE_006,
  'pw.errorMessages.ERR-CORE-006'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_CORE_007,
  'pw.errorMessages.ERR-CORE-007'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_CORE_008,
  'pw.errorMessages.ERR-CORE-008'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_CORE_009,
  'pw.errorMessages.ERR-CORE-009'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_CORE_010,
  'pw.errorMessages.ERR-CORE-010'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_CORE_011,
  'pw.errorMessages.ERR-CORE-011'
);

ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_CORE_012,
  'pw.errorMessages.ERR-CORE-012'
);

ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_AUTH_001,
  'pw.errorMessages.ERR-AUTH-001'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_AUTH_002,
  'pw.errorMessages.ERR-AUTH-002'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_AUTH_003,
  'pw.errorMessages.ERR-AUTH-003'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_AUTH_004,
  'pw.errorMessages.ERR-AUTH-004'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_AUTH_005,
  'pw.errorMessages.ERR-AUTH-005'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_AUTH_006,
  'pw.errorMessages.ERR-AUTH-006'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_AUTH_007,
  'pw.errorMessages.ERR-AUTH-007'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_AUTH_008,
  'pw.errorMessages.ERR-AUTH-008'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_AUTH_009,
  'pw.errorMessages.ERR-AUTH-009'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_AUTH_010,
  'pw.errorMessages.ERR-AUTH-010'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_AUTH_011,
  'pw.errorMessages.ERR-AUTH-011'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_AUTH_012,
  'pw.errorMessages.ERR-AUTH-012'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_AUTH_013,
  'pw.errorMessages.ERR-AUTH-013'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_AUTH_014,
  'pw.errorMessages.ERR-AUTH-014'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_AUTH_015,
  'pw.errorMessages.ERR-AUTH-015'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_001,
  'pw.errorMessages.ERR-INV-001'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_002,
  'pw.errorMessages.ERR-INV-002'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_003,
  'pw.errorMessages.ERR-INV-003'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_004,
  'pw.errorMessages.ERR-INV-004'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_005,
  'pw.errorMessages.ERR-INV-005'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_006,
  'pw.errorMessages.ERR-INV-006'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_007,
  'pw.errorMessages.ERR-INV-007'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_008,
  'pw.errorMessages.ERR-INV-008'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_009,
  'pw.errorMessages.ERR-INV-009'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_010,
  'pw.errorMessages.ERR-INV-010'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_011,
  'pw.errorMessages.ERR-INV-011'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_012,
  'pw.errorMessages.ERR-INV-012'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_013,
  'pw.errorMessages.ERR-INV-013'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_014,
  'pw.errorMessages.ERR-INV-014'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_015,
  'pw.errorMessages.ERR-INV-015'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_016,
  'pw.errorMessages.ERR-INV-016'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_017,
  'pw.errorMessages.ERR-INV-017'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_018,
  'pw.errorMessages.ERR-INV-018'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_019,
  'pw.errorMessages.ERR-INV-019'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_020,
  'pw.errorMessages.ERR-INV-020'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_021,
  'pw.errorMessages.ERR-INV-021'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_022,
  'pw.errorMessages.ERR-INV-022'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_023,
  'pw.errorMessages.ERR-INV-023'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_024,
  'pw.errorMessages.ERR-INV-024'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_025,
  'pw.errorMessages.ERR-INV-025'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_026,
  'pw.errorMessages.ERR-INV-026'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_027,
  'pw.errorMessages.ERR-INV-027'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_028,
  'pw.errorMessages.ERR-INV-028'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_029,
  'pw.errorMessages.ERR-INV-029'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_030,
  'pw.errorMessages.ERR-INV-030'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_031,
  'pw.errorMessages.ERR-INV-031'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_032,
  'pw.errorMessages.ERR-INV-032'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_033,
  'pw.errorMessages.ERR-INV-033'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_034,
  'pw.errorMessages.ERR-INV-034'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_035,
  'pw.errorMessages.ERR-INV-035'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_036,
  'pw.errorMessages.ERR-INV-036'
);

ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_PRO_001,
  'pw.errorMessages.ERR-PRO-001'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_PRO_002,
  'pw.errorMessages.ERR-PRO-002'
);

ErrorTranslateKeyMap.set(ErrorEnums.ERR_INV_31, 'pw.errorMessages.ERR-INV-31');

// UAM ERRORS
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_PRO_003,
  'pw.errorMessages.ERR-PRO-003'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_PRO_004,
  'pw.errorMessages.ERR-PRO-004'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_PRO_005,
  'pw.errorMessages.ERR-PRO-005'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_PRO_006,
  'pw.errorMessages.ERR-PRO-006'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_PRO_007,
  'pw.errorMessages.ERR-PRO-007'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_PRO_008,
  'pw.errorMessages.ERR-PRO-008'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_PRO_009,
  'pw.errorMessages.ERR-PRO-009'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_PRO_010,
  'pw.errorMessages.ERR-PRO-010'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_PRO_011,
  'pw.errorMessages.ERR-PRO-011'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_PRO_012,
  'pw.errorMessages.ERR-PRO-012'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_PRO_013,
  'pw.errorMessages.ERR-PRO-013'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_PRO_014,
  'pw.errorMessages.ERR-PRO-014'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_PRO_015,
  'pw.errorMessages.ERR-PRO-015'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_PRO_016,
  'pw.errorMessages.ERR-PRO-016'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_001,
  'pw.errorMessages.ERR-UAM-001'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_002,
  'pw.errorMessages.ERR-UAM-002'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_004,
  'pw.errorMessages.ERR-UAM-004'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_006,
  'pw.errorMessages.ERR-UAM-006'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_007,
  'pw.errorMessages.ERR-UAM-007'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_011,
  'pw.errorMessages.ERR-UAM-011'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_012,
  'pw.errorMessages.ERR-UAM-012'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_013,
  'pw.errorMessages.ERR-UAM-013'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_014,
  'pw.errorMessages.ERR-UAM-014'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_015,
  'pw.errorMessages.ERR-UAM-015'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_016,
  'pw.errorMessages.ERR-UAM-016'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_017,
  'pw.errorMessages.ERR-UAM-017'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_018,
  'pw.errorMessages.ERR-UAM-018'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_019,
  'pw.errorMessages.ERR-UAM-019'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_021,
  'pw.errorMessages.ERR-UAM-021'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_023,
  'pw.errorMessages.ERR-UAM-023'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_024,
  'pw.errorMessages.ERR-UAM-024'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_025,
  'pw.errorMessages.ERR-UAM-025'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_026,
  'pw.errorMessages.ERR-UAM-026'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_028,
  'pw.errorMessages.ERR-UAM-028'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_029,
  'pw.errorMessages.ERR-UAM-029'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_030,
  'pw.errorMessages.ERR-UAM-030'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_031,
  'pw.errorMessages.ERR-UAM-031'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_032,
  'pw.errorMessages.ERR-UAM-032'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_035,
  'pw.errorMessages.ERR-UAM-035'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_037,
  'pw.errorMessages.ERR-UAM-037'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_038,
  'pw.errorMessages.ERR-UAM-038'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_039,
  'pw.errorMessages.ERR-UAM-039'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_040,
  'pw.errorMessages.ERR-UAM-040'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_041,
  'pw.errorMessages.ERR-UAM-041'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_043,
  'pw.errorMessages.ERR-UAM-043'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_044,
  'pw.errorMessages.ERR-UAM-044'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_045,
  'pw.errorMessages.ERR-UAM-045'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_046,
  'pw.errorMessages.ERR-UAM-046'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_047,
  'pw.errorMessages.ERR-UAM-047'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_048,
  'pw.errorMessages.ERR-UAM-048'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_049,
  'pw.errorMessages.ERR-UAM-049'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_050,
  'pw.errorMessages.ERR-UAM-050'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_051,
  'pw.errorMessages.ERR-UAM-051'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_052,
  'pw.errorMessages.ERR-UAM-052'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_053,
  'pw.errorMessages.ERR-UAM-053'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_054,
  'pw.errorMessages.ERR-UAM-054'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_055,
  'pw.errorMessages.ERR-UAM-055'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_056,
  'pw.errorMessages.ERR-UAM-056'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_057,
  'pw.errorMessages.ERR-UAM-057'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_058,
  'pw.errorMessages.ERR-UAM-058'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_059,
  'pw.errorMessages.ERR-UAM-059'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_060,
  'pw.errorMessages.ERR-UAM-060'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_061,
  'pw.errorMessages.ERR-UAM-061'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_062,
  'pw.errorMessages.ERR-UAM-062'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_064,
  'pw.errorMessages.ERR-UAM-064'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_065,
  'pw.errorMessages.ERR-UAM-065'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_066,
  'pw.errorMessages.ERR-UAM-066'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_067,
  'pw.errorMessages.ERR-UAM-067'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_068,
  'pw.errorMessages.ERR-UAM-068'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_069,
  'pw.errorMessages.ERR-UAM-069'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_UAM_070,
  'pw.errorMessages.ERR-UAM-070'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_LOC_001,
  'pw.errorMessages.ERR-LOC-001'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_LOC_002,
  'pw.errorMessages.ERR-LOC-002'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_LOC_003,
  'pw.errorMessages.ERR-LOC-003'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_LOC_004,
  'pw.errorMessages.ERR-LOC-004'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_LOC_005,
  'pw.errorMessages.ERR-LOC-005'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_LOC_006,
  'pw.errorMessages.ERR-LOC-006'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_LOC_007,
  'pw.errorMessages.ERR_LOC_007'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_LOC_008,
  'pw.errorMessages.ERR-LOC-008'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_LOC_009,
  'pw.errorMessages.ERR-LOC-009'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_LOC_010,
  'pw.errorMessages.ERR-LOC-010'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_LOC_011,
  'pw.errorMessages.ERR-LOC-011'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_LOC_012,
  'pw.errorMessages.ERR-LOC-012'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_LOC_013,
  'pw.errorMessages.ERR-LOC-013'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_LOC_014,
  'pw.errorMessages.ERR-LOC-014'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_LOC_015,
  'pw.errorMessages.ERR-LOC-015'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_LOC_016,
  'pw.errorMessages.ERR-LOC-016'
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_SCH_001,
  'pw.errorMessages.ERR-INV-SCH-001 '
);
ErrorTranslateKeyMap.set(
  ErrorEnums.ERR_INV_SCH_002,
  'pw.errorMessages.ERR-INV-SCH-002 '
);
