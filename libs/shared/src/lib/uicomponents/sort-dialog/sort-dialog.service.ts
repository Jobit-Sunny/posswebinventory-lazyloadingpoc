import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable, throwError } from 'rxjs';
import { SortDialogComponent } from './sort-dialog.component';
import { Column } from './sort-dialog.model';
import { map } from 'rxjs/operators';

@Injectable()
export class SortDialogService {
  private dialogObj = {
    data: [],
    selected: [],
    limit: 1,
    description: ['Low to High', 'High to Low']
  };

  set DataSource(columnList: Column[]) {
    this.dialogObj.data = columnList;
  }

  constructor(private dialog: MatDialog) {}

  openDialog(
    limit: number,
    selected: Column[]
  ): Observable<{ data: Column[], actionfrom: string }> {
    if (this.dialogObj.data && this.dialogObj.data.length > 0) {
      if (limit > 0) {
        this.dialogObj = { ...this.dialogObj, limit };
      }
      this.dialogObj.selected = [...selected];

      const dialogref = this.dialog.open(SortDialogComponent, {
        width: '715px',
        height: '500px',
        data: this.dialogObj
      });
      this.dialogObj.selected = [];

      return dialogref.afterClosed().pipe(
        map(sortList => {
          if (sortList === 'close') {
            return { data: selected, actionfrom: 'close' };
          } else {
            return { data: sortList, actionfrom: 'apply' };
          }
        })
      );
    } else {
      return throwError('DataSource Not available');
    }
  }
}
