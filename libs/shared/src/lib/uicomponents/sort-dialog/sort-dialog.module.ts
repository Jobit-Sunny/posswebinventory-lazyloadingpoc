import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatDialogModule,
  MatCheckboxModule,
  MatButtonModule,
  MatRadioModule,
  MatDividerModule,
  MatIconModule
} from '@angular/material';
import { SortDialogComponent } from './sort-dialog.component';
import { SortDialogService } from './sort-dialog.service';

@NgModule({
  declarations: [SortDialogComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    MatCheckboxModule,
    MatButtonModule,
    MatRadioModule,
    MatDividerModule,
    MatIconModule
  ],
  providers: [SortDialogService],
  entryComponents: [SortDialogComponent]
})
export class SortDialogModule {}
