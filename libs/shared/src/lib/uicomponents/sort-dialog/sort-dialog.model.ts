export interface Column {
  id: number;
  sortByColumnName: string;
  sortAscOrder: boolean;
}
