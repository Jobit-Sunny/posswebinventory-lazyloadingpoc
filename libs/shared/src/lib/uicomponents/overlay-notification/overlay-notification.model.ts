import { OverlayNotificationEventType } from './overlay-notification.enum';

export interface OverlayNotificationEventRef {
  eventType: OverlayNotificationEventType;
  data: any;
}

export interface OverlayNotificationError {
  code: string;
  message: string;
  traceId: string;
  timeStamp: string;
  error: Error;
}
