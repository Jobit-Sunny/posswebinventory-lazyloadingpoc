import { OverlayNotificationEventRef } from './overlay-notification.model';
import { OVERLAY_NOTIFICATION_DATA } from './overlay-notification.token';
import { ComponentPortal, PortalInjector } from '@angular/cdk/portal';
import { Injectable, EventEmitter, Injector } from '@angular/core';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { OverlayNotificationComponent } from './overlay-notification.component';
import { OverLayNotificationConfig } from './overlay-notification.config';

export class OverlayNotificationRef {
  events = new EventEmitter<OverlayNotificationEventRef>();
}

@Injectable({
  providedIn: 'root'
})
export class OverlayNotificationService {
  overlayRef: OverlayRef;
  overlayPortal: ComponentPortal<any>;
  config: OverlayConfig;
  defaultTime = 2000;

  constructor(private overlay: Overlay, private injector: Injector) {
    this.config = {
      positionStrategy: this.overlay
        .position()
        .global()
        .centerHorizontally()
        .bottom(),
      scrollStrategy: this.overlay.scrollStrategies.noop()
      // scrollStrategy: this.overlay.scrollStrategies.block()
      // backdropClass: 'overlay-backdrop',
      // hasBackdrop: true
    };
    this.overlayRef = this.overlay.create(this.config);
  }

  show(config: OverLayNotificationConfig): OverlayNotificationRef {
    //  * Detach the previous component/request
    if (this.overlayRef.hasAttached()) {
      this.overlayRef.detach();
    }

    if (config.hasBackdrop && config.hasBackdrop === true) {
      this.overlayRef.getConfig().hasBackdrop = true;
    } else {
      this.overlayRef.getConfig().hasBackdrop = false;
    }

    config.time = config.time == null ? this.defaultTime : config.time;

    const componentPortal = new ComponentPortal(
      OverlayNotificationComponent,
      null,
      this.createInjector(config)
    );
    const componentRef = this.overlayRef.attach(componentPortal);

    const componentInstance = componentRef.instance;

    const overlayNotificationRef = new OverlayNotificationRef();

    componentInstance.events.subscribe((event: OverlayNotificationEventRef) => {
      this.overlayRef.detach();
      overlayNotificationRef.events.emit(event);
    });

    return overlayNotificationRef;
  }

  // This function create a injector to pass the data to the portal
  createInjector(data): PortalInjector {
    const injectorToken = new WeakMap();
    injectorToken.set(OVERLAY_NOTIFICATION_DATA, data);
    return new PortalInjector(this.injector, injectorToken);
  }

  close() {
    if (this.overlayRef.hasAttached()) {
      this.overlayRef.detach();
    }
  }
}
