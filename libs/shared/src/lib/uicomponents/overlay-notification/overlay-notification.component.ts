import { ErrorTranslateKeyMap } from '../../error-translate-key.map';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OverlayNotificationEventRef } from './overlay-notification.model';
import { OVERLAY_NOTIFICATION_DATA } from './overlay-notification.token';
import {
  OverlayNotificationEventType,
  OverlayNotificationType
} from './overlay-notification.enum';
import {
  Component,
  OnInit,
  EventEmitter,
  Inject,
  OnDestroy
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { OverLayNotificationConfig } from './overlay-notification.config';

@Component({
  selector: 'poss-web-overlay-notification',
  templateUrl: './overlay-notification.component.html',
  styleUrls: ['./overlay-notification.component.scss']
})
export class OverlayNotificationComponent implements OnInit, OnDestroy {
  events = new EventEmitter<OverlayNotificationEventRef>();
  eventTypes = OverlayNotificationEventType;
  overlayNotificationTypes = OverlayNotificationType;
  progressValue = 0;
  notificationIntervalRef: Observable<any>;
  notificationForm: FormGroup;
  remarksMaxCharLimit = 250;
  translatedErrorMsgKey: string;
  private destroy$ = new Subject<void>();

  constructor(
    @Inject(OVERLAY_NOTIFICATION_DATA) public data: OverLayNotificationConfig,
    private formBuilder: FormBuilder,
    public translate: TranslateService
  ) {
    if (
      this.data.type !== OverlayNotificationType.CUSTOM &&
      this.data.type !== OverlayNotificationType.ERROR
    ) {
      if (this.data.type === OverlayNotificationType.TIMER) {
        setTimeout(() => {
          this.emitEvent(OverlayNotificationEventType.CLOSE);
        }, this.data.time);
      }
      this.notificationForm = this.formBuilder.group({
        remarks: [null, [Validators.maxLength(this.remarksMaxCharLimit)]]
      });

      if (this.data.hasRemarks && this.data.isRemarksMandatory) {
        this.notificationForm
          .get('remarks')
          .setValidators([
            Validators.required,
            Validators.maxLength(this.remarksMaxCharLimit)
          ]);
      }
    } else if (this.data.type === OverlayNotificationType.ERROR) {
      this.errorHandler(data.error.code);
    }
  }

  ngOnInit() {}

  emitEvent(eventType: OverlayNotificationEventType) {
    if (
      this.data.hasRemarks === true &&
      eventType !== OverlayNotificationEventType.CLOSE
    ) {
      if (this.notificationForm.get('remarks').valid) {
        this.events.emit({
          eventType,
          data: this.notificationForm.get('remarks').value
        });
      } else {
        this.notificationForm.get('remarks').markAsTouched();
      }
    } else {
      this.events.emit({ eventType, data: null });
    }
  }

  errorHandler(errorCode: string) {
    //check if the error is the custom error
    if (ErrorTranslateKeyMap.has(errorCode)) {
      //Obtain the transation key which will be use to obtain the translated error message
      //based on the language selected. Default is the english language(refer en.json from asset folder).
      this.translatedErrorMsgKey = ErrorTranslateKeyMap.get(errorCode);
    } else {
      this.translatedErrorMsgKey = 'pw.errorMessages.ERR-GERNERIC';
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
