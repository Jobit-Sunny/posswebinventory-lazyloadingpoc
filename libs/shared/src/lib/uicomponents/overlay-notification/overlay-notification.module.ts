import { MatInputModule } from '@angular/material/input';
import { OverlayNotificationComponent } from './overlay-notification.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayNotificationService } from './overlay-notification.service';

import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [OverlayNotificationComponent],
  imports: [
    CommonModule,
    MatProgressBarModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  entryComponents: [OverlayNotificationComponent]
})
export class OverlayNotificationModule {}
