import { OverlayNotificationError } from './overlay-notification.model';
import { TemplateRef } from '@angular/core';
import { OverlayNotificationType } from './overlay-notification.enum';

export interface OverLayNotificationConfig {
  type: OverlayNotificationType;
  message?: string;
  template?: TemplateRef<any>;
  time?: number;
  hasBackdrop?: boolean;
  buttonText?: string;
  hasRemarks?: boolean;
  isRemarksMandatory?: boolean;
  hasClose?: boolean;
  error?: OverlayNotificationError;
}
