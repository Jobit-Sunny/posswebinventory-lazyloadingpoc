export enum OverlayNotificationEventType {
  CLOSE,
  TRUE,
  FALSE
}

export enum OverlayNotificationType {
  SIMPLE,
  SUCCESS,
  ERROR,
  TIMER,
  PROGRESS,
  ACTION,
  CUSTOM,

  // TODO : Generalize multi button action type
  REQUEST_ACTION,
  CONFIRM_TRANSFER
}
