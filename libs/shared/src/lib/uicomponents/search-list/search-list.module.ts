import { MatDividerModule } from '@angular/material/divider';
import { SearchModule } from './../search/search.module';
import { MatListModule } from '@angular/material/list';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material';
import { SearchListComponent } from './search-list.component';

@NgModule({
  declarations: [SearchListComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatCardModule,
    MatCheckboxModule,
    MatButtonModule,
    SearchModule,
    MatDividerModule,
    FormsModule
  ],
  exports: [SearchListComponent]
})
export class SearchListModule {}
