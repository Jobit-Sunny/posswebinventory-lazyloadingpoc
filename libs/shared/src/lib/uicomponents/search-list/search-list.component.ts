import { SelectableItem } from './search-list.model';
import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  ContentChild,
  TemplateRef,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef
} from '@angular/core';
import { Subject } from 'rxjs';
import { SearchResponse } from '../search/search.model';
import { SearchComponent } from '../search/search.component';

@Component({
  selector: 'poss-web-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.scss']
})
export class SearchListComponent implements OnInit, OnDestroy, OnChanges {
  @Input() placeholder = 'Search';
  @Input() hasResults = true;
  @Input() itemList: any[];
  @Input() autocomplete = 'off';
  @Input() multipleSelection = false;
  @Output() search = new EventEmitter<SearchResponse>();
  @Output() clear = new EventEmitter<null>();
  /**
   * Single item if multipleSelection is false
   * Array of Selected Items if multipleSelection is true
   */
  @Output() selected = new EventEmitter<any>();
  @ContentChild(TemplateRef, { static: true }) templateRef: TemplateRef<any>;
  @ViewChild(SearchComponent, { static: true })
  searchRef: SearchComponent;
  itemCheckList: SelectableItem[] = [];
  selectedLength = 0;
  destroy$: Subject<null> = new Subject<null>();

  constructor() {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && changes['itemList']) {
      this.itemCheckList = changes['itemList'].currentValue.map(
        (item: any): SelectableItem => {
          return { item: item, isSelected: false };
        }
      );
      this.selectedLength = 0;
    }
  }

  onSearch(searchResponse: SearchResponse) {
    this.search.emit(searchResponse);
  }

  clearResults() {
    this.reset();
    this.clear.emit();
  }

  reset() {
    this.searchRef.reset();
    this.itemList = [];
    this.itemCheckList = [];
    this.selectedLength = 0;
    this.hasResults = true;
  }

  add(item: any) {
    this.selected.emit(item);
    this.clearResults();
  }

  onSelectionChange(isChecked) {
    if (isChecked) {
      this.selectedLength++;
    } else {
      this.selectedLength--;
    }
  }

  multipleAdd() {
    this.selected.emit(
      this.itemCheckList
        .filter((selectableItem: SelectableItem) => selectableItem.isSelected)
        .map((selectableItem: SelectableItem) => selectableItem.item)
    );
    this.clearResults();
  }

  focus() {
    if (this.searchRef) {
      this.searchRef.focus();
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
