export interface SelectableItem {
  item: any;
  isSelected: boolean;
}
