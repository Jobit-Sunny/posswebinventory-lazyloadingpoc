import {
  Component,
  OnInit,
  TemplateRef,
  Input,
  ViewChild
} from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'poss-web-thumbnail',
  templateUrl: './thumbnail.component.html',
  styleUrls: ['./thumbnail.component.scss']
})
export class ThumbnailComponent implements OnInit {
  @Input() thumbnailSrc: string;
  @Input() imageSrc: string;
  @Input() alt: string;

  @ViewChild('popUpTemplate', { static: true }) popUpTemplateRef: any;

  defaultImageUrl = 'assets/img/product-default-image.svg';

  constructor(public dialog: MatDialog) {}

  ngOnInit() {}

  showPopup(): void {
    this.dialog.open(this.popUpTemplateRef, {
      autoFocus: false,
      data: {
        defaultImageUrl: this.defaultImageUrl
      }
    });
  }

  defaultThumbnailImage() {
    this.thumbnailSrc = this.defaultImageUrl;
  }

  defaultImage() {
    this.imageSrc = this.defaultImageUrl;
  }
}
