import { Option } from './selection-dialog.model';
export interface SelectionDialogConfig {
  title: string;
  placeholder: string;
  options: Option[];
}
