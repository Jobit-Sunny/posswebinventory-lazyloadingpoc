export interface Option {
  id: string;
  description: string;
}
