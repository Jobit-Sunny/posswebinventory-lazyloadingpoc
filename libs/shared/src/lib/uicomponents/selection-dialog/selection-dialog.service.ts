import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { SelectionDialogComponent } from './selection-dialog.component';
import { SelectionDialogConfig } from './selection-dialog.config';

@Injectable()
export class SelectionDialogService {
  constructor(private dialog: MatDialog) {}

  open(config: SelectionDialogConfig): Observable<any> {
    const dialogRef = this.dialog.open(SelectionDialogComponent, {
      autoFocus: false,
      width: '300px',
      data: config
    });

    return dialogRef.afterClosed();
  }
}
