import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectionDialogComponent } from './selection-dialog.component';
import { SelectionDialogService } from './selection-dialog.service';
import { SearchModule } from '../search/search.module';
import {
  MatIconModule,
  MatButtonModule,
  MatFormFieldModule
} from '@angular/material';
import { SelectionDialogOptionDirective } from './selection-dialog-option.directive';

@NgModule({
  declarations: [SelectionDialogComponent, SelectionDialogOptionDirective],
  imports: [
    CommonModule,
    SearchModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  providers: [SelectionDialogService],
  entryComponents: [SelectionDialogComponent]
})
export class SelectionDialogModule {}
