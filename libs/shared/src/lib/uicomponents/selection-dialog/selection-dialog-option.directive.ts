import {
  ElementRef,
  HostListener,
  HostBinding,
  Directive
} from '@angular/core';
import { FocusableOption } from '@angular/cdk/a11y';

/**
 * poss-web-selection-dialog-option
 */
@Directive({
  selector: '[possWebSelectionDialogOption]'
})
export class SelectionDialogOptionDirective implements FocusableOption {
  @HostBinding() tabindex = -1;

  constructor(private elementRef: ElementRef) {}

  focus(): void {
    this.elementRef.nativeElement.focus();
  }

  /**
   *  Listener for Enter key event to call onSelected() function
   */
  @HostListener('keydown', ['$event'])
  onKeydown(event: KeyboardEvent): void {
    if (event.key === 'Enter') {
      this.elementRef.nativeElement.click();
    }
  }
}
