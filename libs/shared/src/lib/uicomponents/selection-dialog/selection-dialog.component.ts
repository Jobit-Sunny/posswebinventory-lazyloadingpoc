import { SelectionDialogConfig } from './selection-dialog.config';
import {
  Component,
  Inject,
  ViewChildren,
  QueryList,
  AfterViewInit,
  HostListener,
  HostBinding,
  OnDestroy,
  ViewChild,
  ElementRef
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Option } from './selection-dialog.model';
import { FocusKeyManager } from '@angular/cdk/a11y';
import { takeUntil, debounceTime } from 'rxjs/operators';
import { Subject, fromEvent } from 'rxjs';
import { SelectionDialogOptionDirective } from './selection-dialog-option.directive';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'poss-web-selection-dialog',
  templateUrl: './selection-dialog.component.html',
  styleUrls: ['./selection-dialog.component.scss']
})
export class SelectionDialogComponent implements AfterViewInit, OnDestroy {
  filteredOptions: Option[] = [];
  destroy$ = new Subject();
  searchForm: FormGroup;

  @HostBinding() tabindex = 0;

  @ViewChildren(SelectionDialogOptionDirective)
  private selectionDialogOptions: QueryList<SelectionDialogOptionDirective>;
  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;

  private keyManager: FocusKeyManager<SelectionDialogOptionDirective>;

  constructor(
    public dialogRef: MatDialogRef<SelectionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SelectionDialogConfig,
    private formBuilder: FormBuilder
  ) {
    this.filteredOptions = data.options;
    this.searchForm = this.formBuilder.group({
      searchValue: []
    });
  }

  ngAfterViewInit(): void {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe((event: any) => {
        const searchValue = this.searchForm.get('searchValue').value;
        if (searchValue !== '') {
          this.filteredOptions = this.data.options.filter((option: Option) =>
            option.id.toLowerCase().includes(searchValue.toLowerCase())
          );
        } else {
          this.clearSearch();
        }
      });

    if (
      this.searchBox &&
      this.searchBox.nativeElement &&
      this.searchBox.nativeElement.focus
    ) {
      setTimeout(() => {
        this.searchBox.nativeElement.focus();
      });
    }

    /**
     * FocusKeyManager configuration
     */
    this.keyManager = new FocusKeyManager(this.selectionDialogOptions)
      .withHorizontalOrientation('ltr')
      .withWrap();
  }

  clearSearch() {
    this.searchForm.patchValue({
      searchValue: ''
    });
    this.filteredOptions = this.data.options;
  }

  onSelect(option: any) {
    this.dialogRef.close(option);
  }

  close() {
    this.dialogRef.close(null);
  }

  /**
   * Listen to the keyDown event for FocusKeyManager
   * Used to send focus event for the option
   */
  @HostListener('keydown', ['$event'])
  keyEvent(event: KeyboardEvent): void {
    this.keyManager.onKeydown(event);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
