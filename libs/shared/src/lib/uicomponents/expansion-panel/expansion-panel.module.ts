import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ExpansionPanelComponent,
  ExpansionPanelSummaryComponent,
  ExpansionPanelContentComponent
} from './expansion-panel.component';

@NgModule({
  declarations: [
    ExpansionPanelComponent,
    ExpansionPanelSummaryComponent,
    ExpansionPanelContentComponent
  ],
  imports: [CommonModule],
  exports: [
    ExpansionPanelComponent,
    ExpansionPanelSummaryComponent,
    ExpansionPanelContentComponent
  ]
})
export class ExpansionPanelModule { }
