import { Component, NgZone, OnDestroy } from '@angular/core';
import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/overlay';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
@Component({
  selector: 'poss-web-expansion-panel-summary',
  template: '<ng-content></ng-content>'
})
export class ExpansionPanelSummaryComponent { }
@Component({
  selector: 'poss-web-expansion-panel-content',
  template: '<ng-content></ng-content>'
})
export class ExpansionPanelContentComponent { }
@Component({
  selector: 'poss-web-expansion-panel',
  templateUrl: './expansion-panel.component.html',
  styleUrls: ['./expansion-panel.component.scss']
})
export class ExpansionPanelComponent implements OnDestroy {
  isExpanded = true;
  destroy$ = new Subject();

  constructor(private scrollDispatcher: ScrollDispatcher, private zone: NgZone) {

    this.scrollDispatcher.scrolled().pipe(takeUntil(this.destroy$)).subscribe((cdk: CdkScrollable) => {

      const offset = cdk.getElementRef().nativeElement.scrollTop || 0;

      this.zone.run(() => {
        // TODO : causes fliker if items are 3
        if (offset === 0) {
          // this.isExpanded = true;
        } else {
          this.isExpanded = false;
        }
      });
    })
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
