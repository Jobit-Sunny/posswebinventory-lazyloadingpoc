import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import { FilterDialogComponent } from './filter-dialog.component';
import { Filter } from './models/filter-dialog.model';
import { FilterActions } from './models/filter-actions.enum';

@Injectable()
export class FilterService {
  private _DataSource: {
    [key: string]: Filter[];
  };
  private dialogObj;

  constructor(
    private dialog: MatDialog,
    public translateService: TranslateService
  ) {}

  set DataSource(data: { [key: string]: Filter[] }) {
    this._DataSource = data;

    this.dialogObj = {
      selected: {},
      filterlimit: 5,
      listlimit: 200,
      showSearch: false,
      filterdata: { ...data }
    };
  }

  openDialog(
    filterlimit: number,
    selected: { [key: string]: Filter[] },
    listlimit?: number,
    showSearch?: boolean
  ): Observable<{ data: { [key: string]: Filter[] }; actionfrom: string }> {
    if (!(this._DataSource && Object.keys(this._DataSource).length > 0)) {
      return throwError(
        this.translateService.instant(
          'pw.filterPopup.filterdatasourceunavailableErrorMessage'
        )
      );
    }

    if (!this.checkDataSize(listlimit)) {
      return throwError(
        this.translateService.instant(
          'pw.filterPopup.filterdatasourceoutoflimitErrorMessage',
          { limit: this.dialogObj.listlimit }
        )
      );
    }

    if (filterlimit > 0) {
      this.dialogObj = { ...this.dialogObj, filterlimit };
    }

    if (showSearch) {
      this.dialogObj = { ...this.dialogObj, showSearch };
    }

    this.convertArrayToMap(selected);

    const dialogref = this.dialog.open(FilterDialogComponent, {
      width: '715px',
      height: showSearch ? '515px' : '485px',
      data: this.dialogObj
    });

    return dialogref.afterClosed().pipe(
      map(filterList => {
        if (filterList === FilterActions.CLOSE) {
          return { data: selected, actionfrom: FilterActions.CLOSE };
        } else {
          return { data: filterList, actionfrom: FilterActions.APPLY };
        }
      })
    );
  }

  private convertArrayToMap(selected: { [key: string]: Filter[] }) {
    if (selected && Object.keys(selected).length > 0) {
      Object.keys(selected).forEach(
        filter =>
          (this.dialogObj.selected[filter] = new Map(
            selected[filter].map(item => [item.id, item])
          ))
      );
    } else {
      Object.keys(this._DataSource).forEach(
        title =>
          (this.dialogObj.selected[title] = new Map<string | number, Filter>())
      );
    }
  }

  private checkDataSize(listlimit?: number): boolean {
    let dataCheck = true;
    if (listlimit) {
      this.dialogObj = { ...this.dialogObj, listlimit };
    }

    Object.keys(this._DataSource).forEach(type => {
      if (this._DataSource[type].length > this.dialogObj.listlimit) {
        dataCheck = false;
      }
    });
    return dataCheck;
  }
}
