export interface Filter {
  id: string | number;
  description: string;
  selected: boolean;
}
