import { FilterDialogComponent } from './filter-dialog.component';
import { FilterService } from './filter.service';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatDialogModule,
  MatTabsModule,
  MatBadgeModule,
  MatChipsModule,
  MatIconModule,
  MatButtonModule,
  MatListModule,
  MatFormFieldModule,
  MatInputModule
} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [FilterDialogComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    MatTabsModule,
    MatBadgeModule,
    MatChipsModule,
    MatIconModule,
    MatButtonModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  providers: [FilterService],
  entryComponents: [FilterDialogComponent]
})
export class FilterDialogModule {}
