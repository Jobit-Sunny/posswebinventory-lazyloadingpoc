import { Subject } from 'rxjs';
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
  Inject,
  OnDestroy
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  ValidatorFn,
  AbstractControl
} from '@angular/forms';
import {
  LocationMappingConfig,
  LocationMappingSearchResponse,
  SelectableLocation,
  LocationMappingOption
} from './location-mapping.model';
import { MatTabGroup, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'poss-web-location-mapping',
  templateUrl: './location-mapping.component.html',
  styleUrls: ['./location-mapping.component.scss']
})
export class LocationMappingComponent implements OnInit, OnDestroy {
  @Output() search = new EventEmitter<LocationMappingSearchResponse>();
  @Output() countryChange = new EventEmitter<string>();
  @Output() stateChange = new EventEmitter<string>();
  @ViewChild(MatTabGroup, { static: true }) tabGroup: MatTabGroup;

  locationFilterForm: FormGroup;
  prevSelectedLocations: LocationMappingOption[] = [];
  locationResults: SelectableLocation[] = [];
  selectedLocations: SelectableLocation[] = [];
  minFilterSelection = 1;
  // Only multi select form controll name
  formControlNames = ['brands', 'regions', 'levels', 'towns'];
  selectAllOfSearchTab = false;
  selectAllOfSelectedTab = false;
  destroy$ = new Subject();
  showSelectFilterMessage = true;
  isInitalDataSet = true;

  constructor(
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: LocationMappingConfig,
    private dialogRef: MatDialogRef<LocationMappingComponent>,
    public translateService: TranslateService
  ) {
    this.locationFilterForm = this.formBuilder.group({
      brands: [],
      regions: [],
      levels: [],
      countries: [],
      states: [],
      towns: []
    });
    this.locationFilterForm.setValidators(this.minFilterSelectionValidator());
    this.locationFilterForm.get('states').disable();
    this.locationFilterForm.get('towns').disable();
    this.locationFilterForm
      .get('countries')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(country => {
        if (country !== null && country !== undefined) {
          this.countryChange.emit(country);
          this.locationFilterForm.get('states').enable();
          this.locationFilterForm.get('towns').enable();
        } else {
          this.locationFilterForm.get('states').disable();
          this.locationFilterForm.get('towns').disable();
        }
        this.locationFilterForm.get('states').reset();
        this.locationFilterForm.get('towns').reset();
      });
    this.locationFilterForm
      .get('states')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(state => {
        if (state !== null && state !== undefined) {
          this.stateChange.emit(state);
          this.locationFilterForm.get('towns').enable();
        } else {
          this.locationFilterForm.get('towns').disable();
        }
        this.locationFilterForm.get('towns').reset();
      });
  }

  ngOnInit() {
    this.prevSelectedLocations = this.data.selectedLocations;
    this.selectedLocations = this.data.selectedLocations.map(location => ({
      id: location.id,
      description: location.description,
      isSelected: false
    }));
  }
  setLocations(data: LocationMappingOption[]) {
    this.locationResults = data.map(location => ({
      id: location.id,
      description: location.description,
      isSelected: false
    }));
    this.selectAllOfSearchTab = false;
    if (!this.isInitalDataSet) {
      this.showSelectFilterMessage = false;
    }
    this.isInitalDataSet = false;
  }

  minFilterSelectionValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      let count = 0;
      if (
        control.get('countries').value !== null &&
        control.get('countries').value !== undefined
      ) {
        count = count + 1;
      }
      if (
        control.get('states').value !== null &&
        control.get('states').value !== undefined
      ) {
        count = count + 1;
      }
      for (let i = 0; i < this.formControlNames.length; i++) {
        if (
          control.get(this.formControlNames[i]).value &&
          control.get(this.formControlNames[i]).value.length > 0
        ) {
          count = count + 1;
        }
      }
      return count < this.minFilterSelection
        ? {
            minSelection:
              'Select minimum ' + this.minFilterSelection + ' filter'
          }
        : null;
    };
  }

  getLableWithSelectedOptionsCount(
    formControlName: string,
    placeholder: string
  ): string {
    if (
      this.locationFilterForm &&
      this.locationFilterForm.get(formControlName) &&
      this.locationFilterForm.get(formControlName).value &&
      this.locationFilterForm.get(formControlName).value.length
    ) {
      return (
        placeholder +
        ' (' +
        this.locationFilterForm.get(formControlName).value.length +
        ')'
      );
    } else {
      return placeholder;
    }
  }

  clearFilter() {
    this.locationFilterForm.reset();
    this.locationResults = [];
    this.showSelectFilterMessage = true;
  }

  searchLocations() {
    this.locationFilterForm.markAsDirty();
    if (this.locationFilterForm.valid) {
      const locationFilterFormValue = this.locationFilterForm.value;
      this.search.emit({
        brands: locationFilterFormValue['brands']
          ? locationFilterFormValue['brands']
          : [],
        regions: locationFilterFormValue['regions']
          ? locationFilterFormValue['regions']
          : [],
        levels: locationFilterFormValue['levels']
          ? locationFilterFormValue['levels']
          : [],
        countries: locationFilterFormValue['countries']
          ? [locationFilterFormValue['countries']]
          : [],
        states: locationFilterFormValue['states']
          ? [locationFilterFormValue['states']]
          : [],
        towns: locationFilterFormValue['towns']
          ? locationFilterFormValue['towns']
          : []
      });
    }
  }

  selectAll(tabIndex: number, isSelected) {
    if (tabIndex === 1) {
      this.locationResults = this.locationResults.map(location => ({
        ...location,
        isSelected: isSelected
      }));
    } else if (tabIndex === 0) {
      this.selectedLocations = this.selectedLocations.map(location => ({
        ...location,
        isSelected: isSelected
      }));
    }
  }

  selectionChange(
    location: SelectableLocation,
    isSelelected: boolean,
    tabIndex: number
  ) {
    if (location.isSelected !== isSelelected) {
      location.isSelected = isSelelected;
    }
    if (tabIndex === 1) {
      this.selectAllOfSearchTab =
        this.locationResults.length ===
        this.locationResults.filter(locationData => locationData.isSelected)
          .length;
    } else if (tabIndex === 0) {
      this.selectAllOfSelectedTab =
        this.selectedLocations.length ===
        this.selectedLocations.filter(locationData => locationData.isSelected)
          .length;
    }
  }

  updateSelectedLocations(tabIndex: number) {
    if (tabIndex === 1) {
      if (this.locationResults.filter(data => data.isSelected).length) {
        this.selectedLocations = this.selectedLocations
          .concat(
            this.locationResults.filter(
              data =>
                data.isSelected &&
                !this.selectedLocations
                  .map(location => location.id)
                  .includes(data.id)
            )
          )
          .map(location => ({
            ...location,
            isSelected: false
          }));
        this.locationResults = this.locationResults.filter(
          data => !data.isSelected
        );

        this.tabGroup.selectedIndex = 0;
        this.selectAllOfSelectedTab = false;
        if (this.locationResults.length === 0) {
          this.clearFilter();
        }
      }
    } else if (tabIndex === 0) {
      this.selectedLocations = this.selectedLocations.filter(
        data => !data.isSelected
      );
    }
  }

  applyLocations() {
    const updatedSelectedLocations = this.mapToLocationOptions(
      this.selectedLocations
    );

    this.dialogRef.close({
      type: 'apply',
      data: {
        selectedLocations: updatedSelectedLocations,
        addedLocations: this.getArrayDifference(
          updatedSelectedLocations,
          this.prevSelectedLocations
        ),
        removedLocations: this.getArrayDifference(
          this.prevSelectedLocations,
          updatedSelectedLocations
        )
      }
    });
  }

  mapToLocationOptions(array: SelectableLocation[]): LocationMappingOption[] {
    return array.map(ele => ({
      id: ele.id,
      description: ele.description
    }));
  }

  getArrayDifference(
    array1: LocationMappingOption[],
    array2: LocationMappingOption[]
  ): LocationMappingOption[] {
    const array2Ids: string[] = array2.map(data => data.id);
    return array1.filter(ele => !array2Ids.includes(ele.id));
  }

  close() {
    this.dialogRef.close({ type: 'close' });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
