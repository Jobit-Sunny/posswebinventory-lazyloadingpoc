import { TemplateRef } from '@angular/core';

export interface LocationMappingOption {
  id: string;
  description: string;
}

export interface SelectableLocation {
  id: string;
  description: string;
  isSelected: boolean;
}

export interface LocationMappingFilterOptions {
  brands: LocationMappingOption[];
  regions: LocationMappingOption[];
  levels: LocationMappingOption[];
  countries: LocationMappingOption[];
  states: LocationMappingOption[];
  towns: LocationMappingOption[];
}

export interface LocationMappingConfig {
  filterOptions: LocationMappingFilterOptions;
  selectedLocations: LocationMappingOption[];
  template?: TemplateRef<any>;
  minFilterSelection: number;
  maxFilterOptionsSelection: number;
}

export interface LocationMappingSearchResponse {
  brands: string[];
  regions: string[];
  levels: string[];
  countries: string[];
  states: string[];
  towns: string[];
}

export interface LocationMappingApplyResponse {
  selectedLocations: LocationMappingOption[];
  addedLocations: LocationMappingOption[];
  removedLocations: LocationMappingOption[];
}
