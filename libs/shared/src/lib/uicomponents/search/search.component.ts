import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Input,
  HostListener,
  Output,
  EventEmitter
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { fromEvent, Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { SearchResponse } from './search.model';
@Component({
  selector: 'poss-web-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Input() placeholder = 'Search';
  @Input() autocomplete = 'off';

  @Output() search = new EventEmitter<SearchResponse>();
  @Output() clear = new EventEmitter<null>();

  @ViewChild('searchBox', { static: true })
  searchBox: ElementRef;
  searchForm: FormGroup;
  destroy$: Subject<null> = new Subject<null>();

  // Barcode inputs
  private barCodeInput = '';
  private barCodeItemCode: string = null;
  private barCodeLotNumber: string = null;

  constructor(private formBuilder: FormBuilder) {
    this.searchForm = this.formBuilder.group({
      searchValue: []
    });
  }

  ngOnInit() {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(takeUntil(this.destroy$))
      .pipe(debounceTime(1000))
      .subscribe((event: any) => {
        const searchValue = this.searchForm.get('searchValue').value;
        if (searchValue !== '') {
          // To check if it is user input or bracode input
          if (searchValue !== this.barCodeItemCode) {
            this.clearBarcodeData();
          }

          this.search.emit({
            searchValue,
            lotNumber: this.barCodeLotNumber
          });
          this.clearBarcodeData();
        } else {
          this.clearSearch(null);
        }
      });
  }

  focus() {
    if (
      this.searchBox &&
      this.searchBox.nativeElement &&
      this.searchBox.nativeElement.focus
    )
      this.searchBox.nativeElement.focus();
  }

  /**
   * Clear Search Event to the user of the control
   * @param event : click event to stop propagation
   */
  clearSearch(event): void {
    if (event) {
      event.stopPropagation();
    }
    this.reset();
    this.clear.next();
  }

  /**
   * Reset fucntion to reset the search box value
   */
  reset(): void {
    this.searchForm.reset();
    this.clearBarcodeData();
  }

  /**
   * Barcode input read function
   * Listens for TAB and ENTER Key event
   */
  @HostListener('keydown', ['$event'])
  keyEvent(event: KeyboardEvent): void {
    const regexpNumber: RegExp = new RegExp('^[A-Z a-z 0-9]$');
    if (regexpNumber.test(event.key)) {
      this.barCodeInput += event.key;
    }
    if (event.key === 'Tab') {
      if (this.barCodeInput !== '') {
        event.preventDefault();
        // Assign the input to barCodeItemCode and barCodeLotNumber after TAB key event
        if (this.barCodeItemCode) {
          this.barCodeLotNumber = this.barCodeInput;
        } else {
          this.barCodeItemCode = this.barCodeInput;
        }
        this.barCodeInput = '';
      }
    } else if (event.key === 'Enter') {
      // Inset barcode lotnumber in search box if lotnuber is inputed by barcode
      if (this.barCodeItemCode && this.barCodeItemCode !== '') {
        this.searchForm.patchValue({
          searchValue: this.barCodeItemCode
        });
      }
    }
  }

  /**
   * Clear Barcode Data
   */
  clearBarcodeData(): void {
    this.barCodeInput = '';
    this.barCodeItemCode = null;
    this.barCodeLotNumber = null;
  }
}
