export interface SearchResponse {
  searchValue: string;
  lotNumber: string;
}
