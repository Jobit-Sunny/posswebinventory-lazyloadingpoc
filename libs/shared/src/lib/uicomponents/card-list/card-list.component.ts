import {
  Component,
  Input,
  OnInit,
  AfterViewInit,
  Output,
  EventEmitter,
  ViewChildren,
  HostListener,
  QueryList,
  TemplateRef,
  ContentChild,
  HostBinding,
  OnDestroy,
  ViewChild,
  ElementRef
} from '@angular/core';
import { FocusKeyManager } from '@angular/cdk/a11y';
import { CardComponent } from './card/card.component';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

/**
 * Poss-Web-Card-List
 * Uses the Poss-Web-Card to display card the list
 * @param count [number] : Total count of the dataList, Used for pagination calculation
 * @param pageSize [number] (Input): PageSize shown. Used for pagination calculation
 * @param dataList  [any[]]: Array of data list to display
 * @param selected  [EventEmitter<any>]: Event emitter for selected data from poss-web-card
 * @param loadMore  [EventEmitter<number>]: Event emitter for loading data with next pageIndex
 */
@Component({
  selector: 'poss-web-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss']
})
export class CardListComponent implements OnInit, AfterViewInit, OnDestroy {
  @HostBinding() tabindex = 0;
  @Input() count = 0;
  @Input() pageSize = 4;
  @Input() dataList: any[];

  @Output() private loadMore: EventEmitter<number> = new EventEmitter<number>();
  @Output() private selected: EventEmitter<any> = new EventEmitter<any>();

  /**
   * Template passed by the user of this controll
   * Will be used to render the cards based on the template passed
   */
  @ContentChild(TemplateRef, { static: true }) templateRef: any;
  @ViewChildren(CardComponent) private cardComponents: QueryList<CardComponent>;
  @ViewChild('cardList', { static: true })
  private cardListRef: ElementRef;

  dataListCount = 0;
  selectedIndex: number;
  isFocusSet = false;
  destroy$ = new Subject();

  private keyManager: FocusKeyManager<CardComponent>;

  constructor() {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    /**
     * FocusKeyManager configuration
     */
    this.keyManager = new FocusKeyManager(this.cardComponents)
      .withHorizontalOrientation('ltr')
      .withWrap();

    this.keyManager.setFirstItemActive();

    this.cardComponents.changes
      .pipe(takeUntil(this.destroy$))
      .subscribe((change: any) => {
        if (change.length > 0) {
          /**
           * Set the focus to the last element if the no more data to be shown
           * Show more button will not be rendered in this case
           */
          if (
            this.count !== 0 &&
            this.dataList.length !== 0 &&
            this.count > this.pageSize &&
            this.count - this.dataList.length === 0
          ) {
            this.keyManager.setActiveItem(this.dataList.length - 1);
          }

          /**
           * Set the focus to the first element,
           */
          if (!this.isFocusSet) {
            this.keyManager.setFirstItemActive();
            this.isFocusSet = true;
          } else {
            this.scrollDown();
          }
        }
      });
  }

  /**
   *  Emits the load event with pageIndex of next page to load
   */
  onClickLoadMore(): void {
    const pageIndex = this.dataList.length / this.pageSize;
    this.loadMore.emit(pageIndex);
  }

  /**
   *  Pass Emits with data and index of the card which is selected to the user of the control.
   */
  onSelected({ data, index }: { data: any; index: number }): void {
    this.selected.emit(data);
    this.selectedIndex = index;
  }

  /**
   * Reset focus key to set the focus to the first element
   */
  resetFocus() {
    this.isFocusSet = false;
  }

  /**
   * Scroll down fucntion to scoll to the down whenever the data overflow
   */
  scrollDown() {
    try {
      this.cardListRef.nativeElement.scrollTo({
        top: this.cardListRef.nativeElement.scrollHeight,
        behavior: 'smooth'
      });
    } catch (err) {}
  }

  /**
   * Listen to the keyDown event for FocusKeyManager
   * Used to send focus event for the card
   */
  @HostListener('keydown', ['$event'])
  onKeydown(event: KeyboardEvent): void {
    this.keyManager.onKeydown(event);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
