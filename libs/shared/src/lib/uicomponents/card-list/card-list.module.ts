import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  CardComponent,
  CardSubtitleComponent,
  CardContentComponent,
  CardTitleComponent
} from './card/card.component';
import { CardListComponent } from './card-list.component';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [
    CardListComponent,
    CardComponent,
    CardTitleComponent,
    CardSubtitleComponent,
    CardContentComponent
  ],
  imports: [CommonModule, MatCardModule],
  exports: [
    CardListComponent,
    CardComponent,
    CardTitleComponent,
    CardSubtitleComponent,
    CardContentComponent
  ]
})
export class CardListModule {}
