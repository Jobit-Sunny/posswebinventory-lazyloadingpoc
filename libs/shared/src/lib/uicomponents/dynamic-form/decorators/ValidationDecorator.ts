import { FormMetadataKeys } from './FormMetadataKeys';
import { ValidationOptions } from './ValidationOptions';
// import { Reflect } from 'core-js/es7/Reflect';
import 'reflect-metadata';

export function Validation(
  options: ValidationOptions
): (target: any, propertyKey: string) => void {
  return (target: any, propertyKey: string) => {
    Reflect.defineMetadata(
      FormMetadataKeys.VALIDATION,
      options,
      target,
      propertyKey
    );
  };
}
