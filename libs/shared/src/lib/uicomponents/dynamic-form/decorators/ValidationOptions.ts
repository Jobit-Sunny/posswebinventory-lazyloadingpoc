import { ValidatorFn } from '@angular/forms';


export interface ValidationOptions {
    validators: ValidatorFn[];
    dateTimeValidators?: { minDate?: string; maxDate?: string };
}
