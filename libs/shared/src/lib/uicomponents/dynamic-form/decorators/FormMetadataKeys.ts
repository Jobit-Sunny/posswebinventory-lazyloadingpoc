export const FormMetadataKeys = {
    FORM_FIELD: 'FORM_FIELD',
    VALIDATION: 'VALIDATION',
    CLASS_NAME: 'CLASS'
};
