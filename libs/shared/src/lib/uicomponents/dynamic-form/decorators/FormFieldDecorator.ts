import { FormFieldType } from '../FormFieldType';
import { FormMetadataKeys } from './FormMetadataKeys';
import { LabelValueKeysNotDefinedException } from '../exceptions/LabelValueKeysNotDefinedException';
import { FormFieldOptions } from './FormFieldOptions';
// import { Reflect } from 'core-js/es7/Reflect';
import 'reflect-metadata';

export function FormField(
  options: FormFieldOptions
): (target: any, propertyKey: string) => void {
  if (hasNoRequiredFields(options)) {
    throw new LabelValueKeysNotDefinedException();
  }

  return (target: any, propertyKey: string) => {
    Reflect.defineMetadata(
      FormMetadataKeys.FORM_FIELD,
      options,
      target,
      propertyKey
    );
  };
}

function hasNoRequiredFields(options: FormFieldOptions) {
  return (
    (options.fieldType === FormFieldType.SELECT && !options.selectOptionKeys) ||
    (options.fieldType === FormFieldType.RADIO && !options.selectOptionKeys)
  );
}
