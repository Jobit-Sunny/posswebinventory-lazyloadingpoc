export enum FormFieldType {
    TEXT = 'TextInput',
    DATE = 'DatePicker',
    DATE_TIME = 'DateTimePicker',
    TEXT_AREA = 'TextAreaInput',
    SELECT = 'SelectInput',
    CHECKBOX = 'CheckboxGroup',
    RADIO = 'RadioButtonGroup',
    SUB_FORM = 'SUB_FORM',
    OUTLINE = 'OutlineInput',
    TEXT_LABEL = 'TextLabel',
    TOGGLE = 'toggleInput'
}
