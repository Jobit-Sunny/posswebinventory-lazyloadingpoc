import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ChangeDetectorRef,
  AfterViewInit,
  OnChanges
} from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';
import { AbstractControl, FormControl } from '@angular/forms';
//import * as cloneDeep from 'lodash.clonedeep';
// import { TranslatorService } from '../../services/translator.service';
import * as template from '../../template.constant';

@Component({
  selector: 'poss-web-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})
export class DynamicFormComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() formFields: any;
  @Input() style: string[];
  @Input() buttonNames: string[];
  @Input() disabled: boolean;
  @Input() disableSubmit: boolean;
  @Input() enableSubmitOnInvalid: boolean;

  formTemplate: string;

  @Output() customValidation: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  @Output() formGroupCreated: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  @Output() addForm: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  @Output() deleteForm: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

  fields: any[] = [];

  formId: number;
  form: FormGroup;

  addFormButton: boolean;
  deleteFormButton: boolean;

  oldFields: any[];

  templateConst: any;

  formName: string;
  formDesc: string;

  constructor(
    private cdr: ChangeDetectorRef,
    // private translatorService: TranslatorService
  ) { }

  ngOnInit() {

    // this.ngOnInit();
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
    if (this.disabled) {
      this.form.disable();
    }
  }

  ngOnChanges() {
    // this.disabled
    this.fields = this.formFields.formFields;
    this.formName = this.formFields.formConfig.formName;
    this.formDesc = this.formFields.formConfig.formDesc;
    this.formTemplate = this.formFields.formConfig.formTemplate;

    this.templateConst = template;
    /*
           <!-- <div *ngIf="field.controlType === 'subForm';else noSubform" class="{{field.name}}">
                     <dynamic-input [style]="style" [field]="field" [form]="form" (selectChange)="selectChange($event)"
                         (radioChange)="radioChange($event)"></dynamic-input>

                 </div>
                 <ng-template #noSubform>

                 </ng-template> -->
        */

    if (this.fields[0].formId !== undefined) {
      this.formId = this.fields[0].formId;
    } else {
      if (this.fields[0].parentFormId !== undefined) {
        this.formId = this.fields[0].parentFormId;
      }
    }

    // this.translatorService.requestErrorMessages(this.formId ? this.formId : 0);
    this.oldFields = JSON.parse(JSON.stringify(this.fields));
    this.removeDependants(this.fields);
    this.buildFormGroup();
    // this.addFormButton = false;
    // this.deleteFormButton = false;

    // if (this.disabled) {
    //   this.form.disable();
    //   alert('here');
    // } else {
    //   this.addFormButton = true;
    //   this.deleteFormButton = true;

    //   // if (this.addForm.observers.length === 1) {
    //   //   this.addFormButton = true;
    //   // }

    //   // if (this.deleteForm.observers.length === 1) {
    //   //   this.deleteFormButton = true;
    //   // }
    // }

    this.initSelectDepedents(this.fields);

  }

  removeDependants(rdFields: any[]) {
    rdFields.forEach(field => {
      if (field.controlType === 'subForm') {
        this.removeDependants(field.fields);
      } else {
        if (field.dependsOn && field.dependsOn !== '') {
          field.options = [];
        }
      }
    });
  }

  dependantsInSubForm(
    sbFields: any[],
    name: string,
    selectedVal: any,
    dependants: any
  ) {
    sbFields.forEach(field => {
      if (field.controlType === 'subForm') {
        this.dependantsInSubForm(field.fields, name, selectedVal, dependants);
      } else {
        if (field.dependsOn === name) {
          dependants[field.name] = field.options.filter(pro => {
            if (pro.foreignKey === selectedVal) {
              return true;
            } else {
              return false;
            }
          });
          // if (init) {
          //   const selected = field.options.filter(pros => pros.selectedKey)[0];
          //   this.selectChange({ selectedVal: selected.value, name: field.name });
          //   if (subFormName) {
          //     const tmp = this.form.get(subFormName).get(field.name)
          //     if (tmp) {
          //       this.form.get(subFormName).get(field.name).patchValue(13);
          //     }
          //   } else {
          //     this.form.get(field.name).patchValue(selected.value);
          //   }
          // } else {
          //   this.selectChange({ selectedVal: '@', name: field.name });
          // }
          this.selectChange({ selectedVal: '@', name: field.name });

        }
      }
      // this.form.get('1-personal').get('1-cityTown').patchValue(13);
    });
  }

  subFormSelectOptsChange(sbFields: any[], dependants: any) {
    sbFields.forEach(field => {
      this.fieldChanges(field, dependants);
    });
  }

  selectChange(obj: { selectedVal: any; name: string }) {
    const dependants = {};
    // finding dependants

    this.oldFields.forEach(field => {
      if (field.controlType === 'subForm') {
        this.dependantsInSubForm(
          field.fields,
          obj.name,
          obj.selectedVal,
          dependants
          // field.name,
          // true
        );
      } else {
        if (field.dependsOn === obj.name) {
          dependants[field.name] = field.options.filter(pro => {
            if (pro.foreignKey === obj.selectedVal) {
              return true;
            } else {
              return false;
            }
          });

          this.selectChange({ selectedVal: '@', name: field.name });
        }
      }
    });

    // changing dependant drop downs options

    if (Object.keys(dependants).length !== 0) {
      this.fields.forEach(field => {
        this.fieldChanges(field, dependants);
      });
    }
  }

  fieldChanges(field: any, dependants: any) {
    if (field.controlType === 'subForm') {
      this.subFormSelectOptsChange(field.fields, dependants);
    } else {
      if (dependants[field.name] !== undefined) {
        // field.formControl.setValue(undefined);
        field.setValue(undefined);
        field.options = dependants[field.name];
      }
    }
  }
  radioValuesInSubForm(name: string, sbFields: any[]) {
    let res: any = {};
    sbFields.forEach(field => {
      if (field.controlType === 'subForm') {
        res = this.radioValuesInSubForm(name, field.fields);
        // res = this.radioValuesInSubForm(name, field.fields);
        // if (res.subForm !== undefined && res.show !== undefined) {
        //   return true;
        // }
      } else {
        if (field.name === name) {
          res.subForm = field.subForm;
          res.show = field.show;
          // return true;
        }
      }
    });
    return res;
  }

  hideOrShowSubForm(value: boolean, name: string, sbFields: any[]) {
    // let res: any;

    sbFields.forEach(field => {
      if (field.name === name) {
        field.hide = value;
        // return true;
      } else {
        if (field.controlType === 'subForm') {
          this.hideOrShowSubForm(true, name, field.fields);
          // res = this.hideOrShowSubForm(true, name, field.fields);
          // if (res !== undefined) {
          //   return res;
          // }
        }
      }
    });
  }

  radioChange(obj: { name: string; value: string }) {
    let subForm = '';
    let show = '';

    let res: any;

    // get show and subForm values in radio

    this.fields.forEach(field => {
      if (field.controlType === 'subForm') {
        res = this.radioValuesInSubForm(obj.name, field.fields);
        if (res.subForm !== undefined && res.show !== undefined) {
          subForm = res.subForm;
          show = res.show;
          // return true;
        }
      } else {
        if (field.name === obj.name) {
          subForm = field.subForm;
          show = field.show;
          // return true;
        }
      }
    });

    // Here is where you need to catch...
    // hide or show subForm based on show value

    if (subForm.length && show.length) {
      const subFormIndex = show.indexOf(obj.value);
      if (subFormIndex === -1) {
        this.fields.forEach(field => {
          if (field.name === subForm) {
            field.hide = true;
            // return true;
          } else {
            if (field.controlType === 'subForm') {
              this.hideOrShowSubForm(true, subForm, field.fields);
              // res = this.hideOrShowSubForm(true, subForm, field.fields);
              // if (res !== undefined) {
              //   return res;
              // }
            }
          }
        });
      } else {
        this.fields.forEach(field => {
          field.hide = true;
        });

        this.fields.forEach(field => {
          field.hide = true;
          if (field.name === subForm[subFormIndex]) {
            field.hide = false;
            // return true;
          } else {
            if (field.controlType === 'subForm') {
              this.hideOrShowSubForm(false, subForm, field.fields);
              // res = this.hideOrShowSubForm(false, subForm, field.fields);
              // if (res !== undefined) {
              //   return res;
              // }
            }
          }
        });
      }

      // this.buildFormGroup(); Fix, checkbox 
    }
  }

  private checkBoxCheckedFn(field: any) {
    let checked = false;

    return field.options.map((opt: { selectedKey: any; }) => {
      checked = false;
      if (opt.selectedKey) {
        checked = true;
      }
      return new FormControl(checked);
    });
  }

  private buildFormGroup() {
    const formGroup: { [key: string]: AbstractControl } = {};
    this.fields.forEach(field => {
      if (field.controlType === 'subForm') {
        if (!field.hide) {
          formGroup[field.name] = this.buildSubFormGroup(field.fields);
        }
      } else {
        if (field.controlType === 'checkbox' || field.controlType === 'toggle') {
          const controls = this.checkBoxCheckedFn(field);
          formGroup[field.name] = new FormArray(controls);
        } else {
          // formGroup[field.name] = field.formControl;
          formGroup[field.name] = field;
        }
      }
    });

    this.form = new FormGroup(formGroup);
    this.formGroupCreated.emit(this.form);
    this.customValidation.emit(this.form);
    this.cdr.detectChanges();
  }

  buildSubFormGroup(fields: any[]) {
    const subFormGroup: { [key: string]: AbstractControl } = {};
    fields.forEach(field => {
      if (field.controlType === 'subForm') {
        if (!field.hide) {
          subFormGroup[field.name] = this.buildSubFormGroup(field.fields);
        }
      } else {
        if (field.controlType === 'checkbox') {
          const controls = this.checkBoxCheckedFn(field);
          subFormGroup[field.name] = new FormArray(controls);
        } else {
          subFormGroup[field.name] = field;
        }
      }
    });

    return new FormGroup(subFormGroup);
  }

  public onAddForm() {
    if (!this.form.invalid && this.form.dirty) {
      this.addForm.emit(this.form);
    } else {
      this.form.markAllAsTouched();
    }
  }

  public onDeleteForm() {
    this.deleteForm.emit(this.form);
  }


  checkBoxChange(obj: { name: string; value: string, option: any[], index: number, subformName: string }) {
    let val = [];
    if (obj.subformName) {
      val = this.form.get(obj.subformName).get(obj.name).value;
    } else {
      val = this.form.get(obj.name).value;
    }

    const checkVal = val[obj.index];

    for (let i = 0; i < obj.option.length; i++) {
      if (obj.option[i].foreignKey === obj.value) {
        val[i] = !checkVal;
      }
    }

    if (obj.subformName) {
      this.form.get(obj.subformName).get(obj.name).patchValue(val);
    } else {
      this.form.get(obj.name).patchValue(val);
    }
  }

  initSelectDepedents(sbFields: any[], subFormName?: string) {

    const dependantControls: any[] = [];
    sbFields.forEach(field => {
      if (field.controlType === 'subForm') {
        this.initSelectDepedents(field.fields, field.name);
      } else {
        if (field.controlType === 'select') {
          const selected = field.options.filter(pros => pros.selectedKey)[0];
          if (selected) {
            this.selectChange({ selectedVal: selected.value, name: field.name });
            if (selected.foreignKey) {
              // const selectedChild = field.options.filter(pros => pros.selectedKey)[0];
              dependantControls.push({ subFormName, name: field.name, value: selected.value });
            }
          }
        }
      }
    });

    if (dependantControls.length) {
      dependantControls.forEach(control => {
        if (control.subFormName) {
          this.form.get(control.subFormName).get(control.name).patchValue(control.value);
        } else {
          this.form.get(control.name).patchValue(control.value);
        }
      })
    }
  }

}
