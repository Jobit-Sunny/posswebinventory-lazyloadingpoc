import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ElementRef,
  AfterViewInit,
  ViewChild
} from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import * as _moment from 'moment';
import {
  DateAdapter,
  MAT_DATE_LOCALE,
  MAT_DATE_FORMATS
} from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD-MM-YYYY'
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'MMMM - YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  }
};

@Component({
  selector: 'poss-web-dynamic-input',
  templateUrl: './dynamic-input.component.html',
  styleUrls: ['./dynamic-input.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    // ! Required
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },

    // * can provide only one date format. Make separate component if you need to define multiple formats
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
    // * Default will be taken. Override if you want to set it.
    // { provide: MAT_DATE_LOCALE, useValue: 'en-US' },
    // { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } }
  ]
})
export class DynamicInputComponent implements OnInit, AfterViewInit {
  @Input() field: any;

  @Input() form: FormGroup;
  @Input() style: string[];

  @Output() selectChange: EventEmitter<any> = new EventEmitter();
  @Output() radioChange: EventEmitter<any> = new EventEmitter();
  @Output() checkboxChange: EventEmitter<any> = new EventEmitter();


  checkBoxArray: any[][] = [];

  dateTimePicker = '';
  datePicker = '';

  constructor(private elRef: ElementRef, private translate: TranslateService) { }
  @ViewChild('dateTimeInput', { static: false }) dateTimeInput?: ElementRef;
  @ViewChild('dateInput', { static: false }) dateInput?: ElementRef;

  ngOnInit(): void {
    // this.field
    if (this.field.controlType === 'checkbox') {
      this.checkBoxArray[this.field.name] = this.field.options;
    }
  }

  ngAfterViewInit() {
    if (typeof this.style !== 'undefined') {
      this.createStyle(this.style.join(' '));
    }
  }

  createStyle(style: string): void {
    const styleElement = document.createElement('style');
    styleElement.appendChild(document.createTextNode(style));
    this.elRef.nativeElement.appendChild(styleElement);
  }

  public isValid(): boolean {
    // this.form.controls[this.field.name].patchValue;
    return this.form.controls[this.field.name].valid;
  }

  onChange(selectedVal: any, name: string) {

    this.selectChange.emit({ selectedVal: selectedVal.value, name });
  }

  radioClick(name: string, value: string) {
    this.radioChange.emit({ name, value });
  }


  innerSelectChange(obj: { selectedVal: string; name: string }) {
    this.selectChange.emit(obj);
  }


  innerRadioChange(obj: { name: string; value: string }) {
    this.radioChange.emit(obj);
  }

  onDateTimeSelectChange(dateTime: string) {
    // this.dateTimePicker = dateTime;
    // this.dateTimeInput.nativeElement.value = dateTime;
    // this.cdr.detectChanges();

    const input = this.dateTimeInput.nativeElement;
    input.value = dateTime;
    input.dispatchEvent(new Event('input'));
  }

  onDateSelectChange(date: string) {
    //  this.datePicker = date;
    //  this.dateInput.nativeElement.value = date;
    //  this.cdr.detectChanges();

    const input = this.dateInput.nativeElement;
    input.value = date;
    input.dispatchEvent(new Event('input'));
    // this.form.controls['1-checkBoxes'].valid;
  }

  // checkClicked(name: string | number) {
  //   const innerSelect = this.checkBoxArray[name];
  //   this.checkBoxArray[name] = [];

  //   innerSelect.forEach((element: { selectedKey: boolean; }) => {
  //     element.selectedKey = true;
  //     this.checkBoxArray[name].push(element);
  //   });
  //   // this.cdr.detectChanges();
  // }


  checkboxClick(name: string, value: string, option: any[], index: number) {
    this.checkboxChange.emit({ name, value, option, index });
  }

  innerCheckboxChanged(obj: { selectedVal: string, name: string, subformName: string }, subformName: string) {
    obj.subformName = subformName;
    this.checkboxChange.emit(obj);
  }

  isRequired(field: any) {
    let req = false;
    try {
      req = !!this.form.controls[field.name].validator(field.name).hasOwnProperty('required');
    }
    catch (e) { }
    return req;
  }

  /*
  isRequired2(field: any) {

    let req = false;
    if (field.validators) {
      field.validators.forEach((val: any) => {
        if (val === Validators.required) {
          req = true;
        }
      });
    }
    // if (field.name === '1-registrationNo') {

    // }
    return req;
  } */
}
