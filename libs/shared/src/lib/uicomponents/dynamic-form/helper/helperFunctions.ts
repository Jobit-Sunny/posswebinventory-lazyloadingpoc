import { Injectable } from '@angular/core';
import { FormGroup, Validators, ValidatorFn } from '@angular/forms';
@Injectable()
export class HelperFunctions {

    patchValue(obj: any, key: string, selector: string, idVal: string, otherFalse?: boolean) {
        if (!idVal) {
            return obj;
        }
        const objClone = obj.map((a: { [x: string]: { toString: () => string; }; }) => {
            const returnValue = { ...a };
            if (otherFalse) {
                returnValue[selector] = false;
            }
            if (a[key].toString() === idVal.toString()) {
                returnValue[selector] = true;
            }
            return returnValue
        });
        return objClone;
    }

    setValidators(formGroup: FormGroup, subForm: string, formField: string, validators: ValidatorFn[]) {
        if (formGroup === null || formGroup === undefined || formField === null || formField === undefined) {
            throw new Error('Mandatory fields missing in setValidators');
        }
        if (subForm) {
            formGroup.get(subForm).get(formField).setValidators(validators);
        } else {
            formGroup.get(formField).setValidators(validators);
        }
    }
}