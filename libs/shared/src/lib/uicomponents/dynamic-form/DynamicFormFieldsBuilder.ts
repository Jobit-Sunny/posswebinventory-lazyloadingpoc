import { forOwn, map } from 'lodash';
import { FormMetadataKeys } from './decorators/FormMetadataKeys';
import { BaseElementFactory } from './inputs/base/BaseElementFactory';
import { FormFieldType } from './FormFieldType';
import { SelectOption } from './inputs/SelectOption';
import { FormFieldOptions } from './decorators/FormFieldOptions';
import { BaseElementParams } from './inputs/base/BaseElementParams';
import { ValidationOptions } from './decorators/ValidationOptions';
import { SubFormInput } from './inputs/SubFormInput';
import { ClassOptions } from './decorators/ClassOptions';
// import { Reflect } from 'core-js/es7/Reflect';
import 'reflect-metadata';
import { Validators } from '@angular/forms';

export abstract class DynamicFormFieldsBuilder {
  public buildFormFields(): any[] {
    const inputs: any[] = [];
    let subInputs: any[] = [];
    let formId: number;
    // let isRequired: boolean;

    forOwn(this, (value: any, propertyKey: string) => {
      // isRequired = false;
      const formFieldOptions: FormFieldOptions = this.getFormFieldMetadata(
        propertyKey
      );
      let validationOptions: ValidationOptions = this.getValidationMetadata(
        propertyKey
      );

      // if (validationOptions) {
      //   validationOptions.validators.forEach(val => {
      //     if (val === Validators.required) {
      //       isRequired = true;
      //     }
      //   });
      // }

      let classOptions: ClassOptions = this.getClassMetadata(propertyKey); // Avi
      if (!classOptions) {
        classOptions = { className: [] }; // Avi
      }
      if (!formFieldOptions && !validationOptions) {
        formId = value;
      } else {
        if (formFieldOptions.fieldType === FormFieldType.SUB_FORM) {
          subInputs = value.buildFormFields();
          inputs.push(
            new SubFormInput(
              formId,
              subInputs,
              propertyKey,
              formFieldOptions,
              classOptions
            )
          );
        } else {
          if (!validationOptions) {
            validationOptions = { validators: [] };
          }

          if (formFieldOptions) {
            const selectOptions: { value: string, selectOptions: SelectOption[] } = this.buildSelectOptions(
              formFieldOptions,
              value
            );

            inputs.push(
              this.buildBaseElement(
                formFieldOptions,
                validationOptions,
                classOptions,
                value.toString(),
                propertyKey,
                selectOptions,
                formId
              )
            );
          }
        }
      }
    });

    return inputs;
  }

  private buildBaseElement(
    formFieldOptions: FormFieldOptions,
    validationOptions: ValidationOptions,
    classOptions: ClassOptions,
    value: string,
    propertyKey: string,
    selectOptions: { value: string, selectOptions: SelectOption[] },
    formId: number
  ) {
    const params: BaseElementParams = this.buildBaseInputParams(
      formFieldOptions,
      validationOptions,
      selectOptions,
      classOptions,
      value,
      formId
    );

    return BaseElementFactory.build(
      formFieldOptions.fieldType,
      propertyKey,
      params
    );
  }

  private hasSelectOptions(formFieldOptions: FormFieldOptions) {
    return (
      formFieldOptions.fieldType === FormFieldType.SELECT ||
      formFieldOptions.fieldType === FormFieldType.RADIO ||
      formFieldOptions.fieldType === FormFieldType.CHECKBOX ||
      formFieldOptions.fieldType === FormFieldType.TOGGLE
    );
  }

  private buildSelectOptions(formFieldOptions: FormFieldOptions, value: any) {
    let selectOptions: SelectOption[];
    let selvalue: string;

    if (this.hasSelectOptions(formFieldOptions)) {
      // if (formFieldOptions.selectOptionKeys.foreignKey === undefined) {
      //   selectOptions = map(value, element => {
      //     return new SelectOption(
      //       element[formFieldOptions.selectOptionKeys.labelKey],
      //       element[formFieldOptions.selectOptionKeys.valueKey],
      //       undefined,
      //       element[formFieldOptions.selectOptionKeys.selected]
      //     );
      //   });
      // } else {
      selectOptions = map(value, element => {
        return new SelectOption(
          element[formFieldOptions.selectOptionKeys.labelKey],
          element[formFieldOptions.selectOptionKeys.valueKey],
          element[formFieldOptions.selectOptionKeys.foreignKey],
          element[formFieldOptions.selectOptionKeys.selectedKey]
        );
      });
      // }
    }

    if (selectOptions) {
      selectOptions.forEach((val) => {
        if (val.selectedKey) {
          selvalue = val.value;
        }
      });
    }


    const so = { value: selvalue, selectOptions };
    return so;
  }

  private buildBaseInputParams(
    formFieldOptions: FormFieldOptions,
    validationOptions: ValidationOptions,
    selectOptions: { value: string, selectOptions: SelectOption[] },
    classOptions: ClassOptions,
    value: string,
    formId: number
  ): BaseElementParams {
    const params: BaseElementParams = this.hasSelectOptions(formFieldOptions)
      ? { selectOptions }
      : { value };

    params.formId = formId;
    params.classNames = classOptions.className; // Avi
    params.validators = validationOptions.validators;
    params.label = formFieldOptions.label;
    params.dependsOn = formFieldOptions.dependsOn
      ? formFieldOptions.dependsOn
      : '';
    params.subForm = formFieldOptions.subForm ? formFieldOptions.subForm : [''];
    params.show = formFieldOptions.show ? formFieldOptions.show : [''];
    params.validationErrorMessages = formFieldOptions.validationErrorMessages ? formFieldOptions.validationErrorMessages : [];
    return params;
  }

  private getFormFieldMetadata(propertyKey: string): FormFieldOptions {
    return Reflect.getMetadata(FormMetadataKeys.FORM_FIELD, this, propertyKey);
  }

  private getValidationMetadata(propertyKey: string): ValidationOptions {
    return Reflect.getMetadata(FormMetadataKeys.VALIDATION, this, propertyKey);
  }

  private getClassMetadata(propertyKey: string): ClassOptions {
    return Reflect.getMetadata(FormMetadataKeys.CLASS_NAME, this, propertyKey);
  }

}
