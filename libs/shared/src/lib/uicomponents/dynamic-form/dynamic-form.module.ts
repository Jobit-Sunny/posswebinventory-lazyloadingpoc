import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDividerModule,
  MatFormFieldModule,
  MatInputModule,
  MatRadioModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatDialogModule,
  MatIconModule
} from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';

import { DynamicFormFieldsBuilder } from './DynamicFormFieldsBuilder';
import { DynamicFormComponent } from './components/dynamic-form/dynamic-form.component';
import { DynamicInputComponent } from './components/dynamic-input/dynamic-input.component';
import { ErrorMessageComponent } from './components/error-message/error-message.component';
import { ValidationErrorsComponent } from './components/validation-errors/validation-errors.component';
import { ObjIteratorPipe } from './pipes/obj-iterator.pipe';
import {
  DatetimePickerComponent,
  DialogDateTimePicker
} from './components/datetime-picker/datetime-picker.component';
import {
  DatePickerComponent,
  DialogDatePicker
} from './components/date-picker/date-picker.component';
import { NonsubformfilterPipe } from './pipes/nonsubformfilter.pipe';
import { SubformfilterPipe } from './pipes/subformfilter.pipe';

import { TranslateModule } from '@ngx-translate/core';
import { HelperFunctions } from './helper/helperFunctions';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatMomentDateModule
  ],
  declarations: [
    DynamicFormComponent,
    DynamicInputComponent,
    DialogDateTimePicker,
    ErrorMessageComponent,
    ValidationErrorsComponent,
    ObjIteratorPipe,
    DatetimePickerComponent,
    NonsubformfilterPipe,
    SubformfilterPipe,
    DialogDatePicker,
    DatePickerComponent
  ],
  exports: [
    DynamicFormComponent,
    DynamicInputComponent,
    DialogDateTimePicker,
    DialogDatePicker,
    ErrorMessageComponent,
    ValidationErrorsComponent,
    ObjIteratorPipe,
    DatetimePickerComponent,
    DatePickerComponent,
    NonsubformfilterPipe,
    SubformfilterPipe
  ],
  entryComponents: [DialogDateTimePicker, DialogDatePicker],
  providers: [HelperFunctions]
})
export class DynamicFormModule {
  constructor() {}
}
