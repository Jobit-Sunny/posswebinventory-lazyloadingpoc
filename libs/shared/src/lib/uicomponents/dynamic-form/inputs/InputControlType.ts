export declare type InputControlType =
    | 'text'
    | 'textarea'
    | 'select'
    | 'checkbox'
    | 'radio'
    | 'datepicker'
    | 'datetimepicker'
    | 'outlineinput'
    | 'textlabel'
    | 'toggle';
