import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UicomponentsModule } from './uicomponents/uicomponents.module';
import { TranslateService, TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [CommonModule, UicomponentsModule, TranslateModule.forChild()],
  exports: [TranslateModule, UicomponentsModule]
})
export class SharedModule {
  constructor() {}
}
