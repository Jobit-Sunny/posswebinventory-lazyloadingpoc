## Features Added

- NAP-2352 Box Numbers and Weights(In issue to factory, ibt mer)

## Bug Fixes

- Stock-Receive : Sonar bug fix

* Sprint - 12
  Defect Nos:
  - 100 - Stock-Recieve : Spelling mistake Product Category and Group
  -104 -weight tolerance: Fixed confirmation box message

* Sprint - 13
  Defect Nos:
  - 19 Stock-Receive : Value shown in drop-down was not matching the loaded stn.

## Enhancements

Stock-Receive : Integration of the weight tolerance check
Stock-Receive : Receive-from-other-botiques added selection-box to choose type (IBT or Merchandise)
Stock-Receive : Verified tab. Auto-save will be on focus-out event. (prev 1 sec after edit)
Location-Master : Mapped new API JSON structure with location master.
IBT : added boutique address and contact number
Stock-Issue TEP/GEP: added courier details for L3 User
For courier details, auto populate employee details from database
File Upload: STN and Invoice upload integrated with api

- Stock Issue-
  1. Request No: (Heading is required before the number)
  2. Request Date is required
  3. Total weight column is required.
  4. To Factory needs to remove"
- Stock Issue Details-
  1. Product Images are not displaying.
  2. Date is not required.
  3. Item Weight should be in 3 decimals
     Stock-Receive :
  - 1. In case of 1 product then should display as " 1 Product(s)".
  - 2. 55 STN's are pending for inwarding.
  - 3. Instead of From: Factrory required "STN Date".
  - 4. Rename Received Date to "Courier Received Date"

## [IR-0.0.46](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.45%0DIR-0.0.45): (Thu 12-dec-19 2.10 PM)

## Bug Fixes

- Stock-receive and Bin-to-Bin : Filter lable mismatch for product category and product group
- Weight-tolerance :Location Mapping remove location bug fixed
- Overlay-Notifcation : Notifcation message alignment fixed
- NAP-140 Brand Duplicate value insersion message, changed checkbbox mismatch checked, after save/update loading screen was not shown- fixed, now showing toggle notification. (But still Brand/Sub brand filter is not available in API)
- NAP-140 Sub brand Duplicate value insersion message, after save/update loading screen was not shown- fixed, now showing toggle notification. (But still Brand/Sub brand filter is not available in API)
- Weight-Tolerance : Now Confirmation dialog box will appear before saving or cancelling, showing loading screen with notification on save of toggle in listing.

* Sprint - 12
  Defect Nos:

  - 82 - User update functionality is not working as expected. - On editing the user and updating, success message is displayed but the changed fields are not updated.
  - 42 - Even after removing the secondary role, the from date for secondary role is not editable. Please refer the attached screenshot.
  - 34 - Validation error messages for email are displayed at UI level are not displayed.
    'Should not allow the user to enter email ID with boutique code, mgr and location code @ titan.com' - These validation error messages are not displayed at field level.
    -73 - No Products found message is displayed here also even though no filter has been applied.
    -17 - removed product button displayed event after unchecking the radio button.

* Sprint - 11
  Defect Nos:

  - 75 - NAP-150 Details are not fetching properly from DB for below fields. Fixed.
  - 67 - NAP-141 Location Master - Location - LOYALTY-GEP-GC-TEP - Label Issue. Fixed.
  - 133 - Validate Field currency
  - 128 - Fields are accepting wrong data
  - 227 - filter not working properly PSV
  - 228 - filter not working properly Exhibtion
  - 177 - Currently the logged in users name is not displayed, hence it is difficult to identify who the logged in user is.
  - 258 - Fixed, Now special charcaters are not allowed, except "\_" and "-"

# Enhancements

- NAP-141, NAP-150, NAP-145, NAP-149 Added, common error handler with traceID
- Stock-Issue TEP/GEP : edited factory details. changed from array of objects to single object
- Return CFA : edited factory details. changed from array of objects to single object
- corporate town, bin group, bin code - error approach changed in dynamic forms
- Brand master, Location advance/custom order - error appoarch changed.
- Restructured complete flow of passing validation message to fields of dynamic forms.
- DynamicForm error message linting, now takes error message from module directly instead of dynamic-form-field-translation-master.map
- updated error translation key in models of dynamic form.
- Location ghs,grn,print - error appoarch changed
- Corrected Ngx-Translate Loader issue when website is deployed as subsite i.e. /eposs/ or /poss/
- Adjusted the Viewport to adjust the layout of the website to 16:10 aspect ratio
- Setting up of karma test runner
- Fixed Lint error in ACL Encoder related files
- Added setting for SonarQube Integration.
- UAM : Added location-selection popup for location filter
- UAM : Added Search in the role list. search by role code.
- NAP-141, Restricting user by entering the specail character in copy pop up of Location.
- NAP-141, Reloading the Location listing after copy success.
- NAP-149, Reloading the listing LOV after creating new LOV details.
- Stock-receive : Removed reason for delay text-box removed from the details page. It will be shown as pop up once the user click on confirm. Only if there a delay popup will be shown.
- Stock-receive : Replaced mat-select for bincode with selection bincode popup
- Stock-receive : not showing confirm or verify button if items failed to load
- Stock-Receive : Listing page search moved inside the tabs
- Stock-Receive : If the non-verified items count is 0. it will switch to verified tab. Added translation for the lables of sort and filter
- Weight-Tolerance : Now Confirmation dialog box will appear before saving or cancelling.
- Courier-Details : Now Confirmation dialog box will appear before saving.

## [IR-0.0.45](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.44%0DIR-0.0.43): (Mon 03-dec-19 11.15 PM)

# Bug Fixes

- Disabling of retry strategy.
- HttpStatus Code 415 -Unsupport Media Error Fix.
- Stock-Receive - Resolved lint errors.
- Stock-Receive - Bug fix : clear search of STN listing by clicking clear button was not working.
- Bin-To-Bin-Transfer - Resolved lint errors.
- Bin Group - Resolved lint errors.
- Stock-Issue TEP/GEP - Resolved lint errors.
- IBT - added bin code and out of stock in requests sent screen.

- Sprint 12
  Defect Nos:
  -62 - same sorting values exists for exhibition, loan, loss.
  - 69 - Bin group confirmation pop up was blank. fixed.
- Sprint-11
  Defect Nos:
  - 248 - NAP-149 System is allowing to save duplicate LOVs, now showing error notification if duplicate.
  - 260 - NAP-141 System is giving error instead proper message if location code and factory codes are not same, now showing error notification for this.

## [IR-0.0.44](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.44%0DIR-0.0.43): (Mon 03-dec-19 11.15 PM)

# Bug Fixes

- NAP-7 UAM : Bug-fix. Calling load country api multiple times
- Sprint 12
  Defect Nos:

  - 93- Added isActive field in courierDetails form
  - 32 - NAP-150 Product Category, Reset search textbox after editing/Adding new product Category details.
  - 31 - NAP-150 Product Category, Correction of Edits heading, Updating the label with (ASSM).
  - 30 - NAP-150 Product Category, Fixed showing proper duplicate error message, previously error code was missing.
  - 29 - NAP-150 Product Category, For ADD NEW, both is active / ASSM checkbox are true by default.
  - 40 -Other Receipts & Issues - When we search for a variant then the pagination should not be displayed.

- Sprint 11
  Defect Nos:

  - 93- Added isActive field in courierDetails form
  - 216- Brandcode and regionCode dropdowns are populated
  - 186- region and state not updated
  - 259 - NAP-141 Location Master- Disabled location code textbox for edit.
  - 180 - Barcode browser issue fixed.
  - 27 - Fixed, Now fields won't accept negative values
  - 258 - Fixed, Now special charcaters are not allowed, except "\_" and "-"

  -Sprint 10
  Defect Nos:

  - 80 - Data mismatch in number of products at card level and detail page.Please refer the attached document.
  - 92 - When user clicks on back button of the browser, while creating a new exhibition issue, Confirm Issue button disappears.Please refer the document attached.
  - 60 - 'Verify All' button is not disappearing on changing a product data. Please refer the screenshot attached.

## Enhancements

- Refactored, dynamic form models of location, lov, item master, product category by moving models files from common to respective modules model folder.
- Item Master - Removal of coommented code, translation.
- Location-Mapping Service - Integrated Core services to load brands, regions, levels, countries, states, towns and location codes.
- Location-Mapping Service - Updated Error Handling
- Stock-Receive - Integrated Core-Data-Service for product-group, product-category, bin-codes and defect-tyes
- traceId addition for error handling in other issues and other receipt.
- Bin-To-Bin transfer - Integrated Core-Data-Service for product-group, product-category, bin-codes
- Bin-To-Bin transfer - Updated error handling and manufacture date will be not shown incase of null
- Provided confirm while saving in NAP-141 location, NAP-149 LOV, NAP-145 product category.
- Changed confirm popup to pass translation text from component where its called
- Validation updated for amount fileds in Brand Master

## [IR-0.0.43](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.43%0DIR-0.0.42): (Mon 02-dec-19 1.45 PM)

## Bug FIxes

- Sprint 12
  Defect Nos:
  - 23- User form was not getting loaded.

-Sprint 11
Defect Nos:
-133- Currency textbox changed to drop down
-254- Not All Brands Are displayed In parent Drop down while creating sub brand

## Enhancements

- Location Master- Disabled location code for edit.
- Location Master - Location form brand and Sub brand field was not reseting

## [IR-0.0.42](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.42%0DIR-0.0.41): (Tue 29-nov-19 8.30 PM)

## bugs fixes

Defect nos:

- 66 - not able to add plain products in other issues adjustment
- 227 - filter for foc, adjustment, psv other issues and receipt.

## Features Added

- NAP-84 Toggel confirmation dialog box in BinCode
- NAP-53 Issue TEP-GEP courier details fetch from api
- Master Dashboard labels are now taken from translation.
- Product category search functionality fix.
- Courier master search and error notification fix
- NAP-150 Item Master error handler and show notifcation.No search result display on invalid search
- NAP-103 Merchandising Initiated Stock transfer:Upload file
- Dynamic form radio button was clearing checkbox bug fixed

## Bug FIxes

- Sprint 12
  Defect Nos:

  - 1-The error message is blocking the screen: Fixed - Now we can close the notification. NAP-141
  - 2 - Stock-Receive : Total Count of verified was not getting updated. Fixed.
  - 4 - Stock-Receive : Duplicate items in two different page. No changes done in UI, But it as wroking as expected.
  - 12 - Stock-Receive : Search bug fix
  - 8 - Even though the user role is store manager, when admin tries edit the user, ‘Admin can't update or assign roles to employee which are not accessible by corporate’ error message is displayed
  - 7 - “Provided password is same as old password” message is displayed even after giving valid password and successfully changing the password
  - 10-Search bug fix in sort and filter
  - 11-Sort filter functionality bug fix in issue
  - 13-Sort filter functionality bug fix in issue

- Sprint 11
  Defect Nos:
  - 250 - Search 'No Search Result' translation was missing
  - 247 - Now provided conform for cancel in LOV.
  - 207 - Spelling mistakes resolved in CFA-product-code
  - 215 - functionality not mentioned in user-story
  - 252 - is Mia checkbox is updated in CFA-product-code
  - 91 - Invalid courier details deleted from courier details listing page
  - 212 - Added validation for mandatory fields in CFA-product-code
  - 251 - Radio button issue was fixed
  - 145 - Amount Validation done
  - 8 - search bug fixed
  - 130- after editing deleting preiouvsly selected values getting deleted
  - 234 - "Confirm Issue" button should be displayed after clicking clear All
  - 231 - other issues "no data found" should display while filtering.
  - 228 - filter is not there for other issues.
  - 160 - other issues - unable to add studded products in PSV
  - 16 - other receipt- itemWeight is taking value of item Quantity.
  - 207 - Spelling mistakes resolved in CFA-product-code
  - 215 - functionality not mentioned in user-story
  - 91 - Invalid courier details deleted from courier details listing page
  - 246- bin code search
  - 185- special characters in search issue
  - 245- Request Approvals - Inter-Boutique Transfer - In the details page, in the card, the is a vertical space between the issuing and requesting boutique codes
  - 244- REQUEST APPROVALS > INTER-BOUTIQUE TRANSFER - Number of products and value of the products is not displayed on card
  - 242- Request Approvals - Other Issues - The cards are showing the dollar symbol before the amount instead of the rupee symbol
  - 75- Regression Issue: New Bin request is not sent to EPOSS for approval.
  - 87- Regression Issue: In request approvals screen for EPOSS, the count is displayed as 0 for Other Issue
  - 157- When REQUEST APPROVALS in clicked, both INVENTORY and REQUEST APPROVALS tabs are highlighted.
  - 140- New user login screen not implemented
  - 66 - not able to add plain products in other issues adjustment
  - 227 - filter for foc, adjustment, psv other issues and receipt.
  - 119- corporate town search
  - 120- duplicate town name

## Enhancements

- filtering and sorting added for other issues PSV, FOC, ADjustment while creating request.
- addition of master Data services.
- Updated showing errors on overlay-notification in stock-receive
- Changes for not calling Logout API on receiving 401 status code in API call
- location-mapping popup - loading all the locations (prev it was loading max 20)

## [IR-0.0.41](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.41%0DIR-0.0.40): (Tue 27-nov-19 11.00 PM)

## Features Added

- NAP-150 ItemMaster (config detail, item detail mapping pending)
- popup is added in courier details when user clicks on cancel button.
- NAP-145 Product category search functionality done, fixed edit header translation.
- Reworked on CSS for dynamic form, popups where dynamic forms are used.
- NAP-102 Factory Initiated Stock transfer:Upload file

### Bug Fixes

- Sprint 11
  Defect Nos:
  - 119-Corporate Town search fixed
  - 121-edit dialog box issue
  - 122-town name accepting only valid details.
  - 188-edit modal opening once now.
  - 193-cancel button working.
  - 194-corporate town search issue fixed
  - 195- bin group dropdown by bingroup code instead of description
  - 199-bin code search correction.
  - 197-Field validation implemented
  - 141-OTP functionality not implemented
  - 209-Heading “Enter 4 digit verification code” should be changed to “Enter 6 digit verification"
  - 220-When an existing user is edited, “User has been added successfully and a confirmation has been sent to the user mobile with OTP” message is displayed
  - 154-User is not able to create role at boutique level
  - 62-State and Country fields should be Mandatory
  - 76-message changed after status change
  - 163-status displyed at line item level
  - 219- Mobile number and Contact person fields are changed to not mandatory.
  - 213- Defect rejected because there is no validation for CFA-Product code in legacy system
  - 210- Added cancel button functionality in CFA-product-code
  - 207- Spelling mistakes resolved
  - 211- Added radio button instead of checkbox for Dynamic F1 Calculation Yes or No field
  - 212- Rejected because for some users only save and edit will happpen
  - 196- Not able to re-produce the error,
  - 96- Courier Details integration in dropdown
  - 134- Child code weight editing. Not a bug as per current flow.
  - 170- Item weight change. Not reproducible
  - 183- Rso name field validations and auto hiding
  - 200, 201, 202, 203,205, 208, 214,218- Grid functionality fixed
  - 240- Document number after conversion is added to notification.
  - 133- Validate Feilds under Currency
  - 132- On editing the CM SMS Configuration and saving feilds are becoming empty
  - 131 - User is not able to add the new weight tolerance under new config id
  - 130 On click of edit button previously selected values are getting deleted
  - 128- Feilds are accepting invalid data
  - 126 - Useris not able to add the new Weight Tolerance
  - 125 - No data under Product category code
  - 110 Brand short Name feild should be limited to 3
  - 108 Veiw edit page is not workin
  - 103 product master issues
  - 33 Edit is not workig
  - 5 Mandatory feilds are not present

## [IR-0.0.40](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.40%0DIR-0.0.39): (Tue 26-nov-19 3:30 PM)

### Enhancements

- Filter and sorting functionality added in Other Issues request creation for Exhibition, Loan and Loss.

* NAP-84 BinCode: Location Mapping
* Filter functionality added in return-invoice-to-cfa

### Bug Fixed

- Sprint 11
  Defect Nos:

  - 98-Bin code search working
  - 100-error while updating bin code toggle issue fixed
  - 102-search box not emptying on back button issue fixed
  - 107-Location mapping added to bin code
  - 190-'Add new' changed to 'Add new bin group'
  - 111-clear button issue fixed

## [IR-0.0.39](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.39%0DIR-0.0.38): (Mon 25-nov-19 11:00 PM)

### Bug Fixed

- Sprint 7,8,9
  Defect Nos:
  - 179-Added CFA Address in return-invoice-to-cfa
  - 102-Search functionality is not there in Select CFA drop down.because we removed select CFA drop down it will pick CFA Address based on logged in user.
  - 88- On clicking browser's back button, the confirm receive button was getting disappears
- Sprint 10 Defect Nos:
  - 62 - Date-Picker Problem
- Sprint 11
  Defect Nos:
  - 94-saved successfully notification issue was fixed
  - 90-CheckBox is removed in courier details listing page
  - 95-isActive functionality implemented in CourierDetails listing page
  - 106-Location Mapping implemented
  - 105-mobileNumber field added
  - 75 -Regression Issue: New Bin request is not sent to EPOSS for approval.
  - 155 -REQUEST APPROVALS OTHER ISSUES - Number of products and value of the products is not displayed on
  - 158 -REQUEST APPROVALS OTHER ISSUES - Exhibition tab text should be in CAPS.
  - 157-When REQUEST APPROVALS in clicked, both INVENTORY and REQUEST APPROVALS tabs are highlighted
  - 169-When user accepts the IBT request partially, only the accepted Variant codes should flow to
  - 62 - State and Country fields should be Mandatory. Fixed
  - 136 - Overlapping text when user enters invalid data in Employee ID field and clicks on Assigned Boutique field. Fixed
  - 142 - User is not able to select future dates as date of resignation
  - 143 - User is able to select Date of joining prior to Date of birth
  - 151 - User is able to select resignation date prior to joining date
  - 146 - User is not able to select future date as joining date
  - 159 - ‘Create New Role’ title is displayed for Edit Role pop up.
  - 176 - Navigation issue. Fixed
  - 138 - Spelling Mistake. Fixed
  - 135 - Spelling Mistake. Fixed
  - 161 - User was not able to increase the weight of the product by 0.03.
  - 132 - On editing the 'CM SMS Configuration' and saving, all the fields are becoming empty
  - 152 unable to save and continue in brand
  - 104 Product Masters - SUB-BRAND master is not displayed
  - 81 -Brand Code Master - Invalid validation error message for "Brand Code" field
  - 130 On click of edit previously selceted values are getting deleted
  - 127 location mapping is not implemented
  - 113 placeholder corrected
  - 114 No location mapping button in add new screen
  - 109 No data under product category code

## Features Added

- NAP-84 BinCode: Search
- NAP-84 Converted popup to dynamic form.
- NAP-152 CFA Product code:Add/Edit/View.
- sort is implemented in return-invoice-to-CFA
- NAP-149 List of Values : Add /Edit /View. (Given to products, location, inventory lov types)
- NAP-2035 Converted popup to dynamic form.
- NAP-1067 (UAM) - Users Reset Password with OTP Validation (NEW)
- NAP-1065 - Store manager Edit/Unlock/Deactivate users for specific boutique POSS (NEW)
- NAP-1062 - System Admin Edit/Unlock/Deactivate users for ePOSS and Boutique POSS Users

### Enhancements

- NAP-81 CourierDetails form has changed to dynamic forms
- NAP-141 Refactored entire notification handling in location master, included standard global notification if network/API error while fetching from server.
- NAP 141 - Created a new popup confirm dialog box which can be used across. (Currently used during cancel of dynamic form).
- NAP-649, 671 - factory address details added in Stock Issue TEP/GEP.
- NAP-7 - Changes for StoreType enum and RoleType enum
- NAP-87 - Stock-Receive : Reason for delay lofic change . STN to Current date.
- NAP-33 - Stock-Receive : Confirm Success Notifcation Changed
- NAP-31 - Stock-Receive : Filter, Sort integration

## [IR-0.0.38](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.38%0DIR-0.0.37): (Thu 22-nov-19 11:30 AM)

## Features Added

-NAP-6 (UAM) - System Admin Adds/Removes Roles for UAM
-NAP-7 (UAM) - System Admin Creates users at ePOSS and User at Boutique POSS - Only Store Manager

### Enhancements

- NAP-51, NAP-53 - getting Product categories and Product Groups from Api service. - implementaion of bar Code service and Shortcut Service while Searching. - implementaion of sort and filter functionality.
- NAP-75, NAP-42 - implemented bar Code Service and shortcut keys

### Bug Fixed

- Sprint 11
  Defect No:
  - 76 - Issue in enabling checkbox and status message. Fixed

## [IR-0.0.37](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.37%0DIR-0.0.36): (Thu 20-nov-19 10:00 PM)

## Features Added

- NAP-41 Stock Issue- Request Corporate Approval: Search & Confirm.
- NAP-2039 Corporate Approval for Other Issues.
- NAP-81 Courier Master:Add & Edit,search,
- NAP-140 Brand Master: Add & Edit,search,
- NAP-84 BinCode:Add & Edit, BinGroup: Add & Edit,search,
- NAP-2035 Corporate Town: Add & Edit,search,
- NAP-79 Weight-tolerance: Add & Edit, search

### Enhancements

- Changed Filter PopUp view for filter items to list view from grid view and introduced enum for actions responded from Filter PopUp
- NAP-47, NAP-48 - getting Bincodes and Remarks from Backend. - implementaion of bar Code service and Shortcut Service in while Searching.

### Bug Fixed

- Sprint 11
  Defect No:
  - 63 - Due to ACL implemetation, there are many url changes. Fixed
  - 59 - Due to error in api, count is not displayed properly. Fixed
  - 56 - Back arrow navigation was taking to master main dashboard directly instead of listing page. Fixed
  - 68-UI changes
  - 69-UI Changes
  - 70-UI changes
  - 71-Proper error msg given

## [IR-0.0.36](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.36%0DIR-0.0.35): (Thu 19-nov-19 11:45 PM)

## Features Added

- NAP 66 - Conversion Selection
- NAP-101 - Sending Conversion Requests
- NAP-1195- Confirm the transaction
- NAP-7 System Admin Creates users at ePOSS and Boutique POSS level (only SM)
- NAP-1063 User Access Management Landing Page
- NAP-11 Users Log In/Log Out

### Enhancements

- Removed product list and selected products tabs in Issue-to-CFA. because there is no relevancy of tabs in issue-to-CFA,when user search product/variant then product/variant will be addded to the screen directly.

### Bug Fixed

- Sprint 7_8_9
  Defect No:
  - 96-Defect is rejected because now there is no listing page in Issue-To-CFA
  - 227- Reg_Issue\* Getting something went wrong error message while transferring products from Bangle bin to Loan Bin.
  - 215- Search by invalid inputs is handled in stock issue details page
  - 220- Error msgs are made uniform in stock issue
  - 196- when the quantity is changed, the user have to enter the quantity. flow is changed.
  - 164 - List of Products and Selected Products tab - Items per page state is not retained when we move between tabs.
  - 67 - Stock-Receive Details page verify all button was not shown when search and clear using back-space. Fixed

* Sprint 10
  Defect No:

  - 96-Defect is rejected because now there is no listing page in Issue-To-CFA
  - 227- Reg_Issue\* Getting something went wrong error message while transferring products from Bangle bin to Loan Bin.

  - 21-message inconsistency bug fixed
  - 41-the confirmation message is not uniform throughout the application
  - 85 - Reg_Issue_Please change the warning message.
  - 97 - Search is not working as expected when invalid data is searched in Other Receipts.
  - 95 - 'gms' is not displayed for Item Weight in other receipts.
  - 79 - Card is still present in listing page even after confirming the issue. Please refer the document attached
  - 89 - Bin to Bin searching was not taking special chars . Fixed
  - 91 - Distortion of screen when user searches for a variant in all screens in Edge browser. Rejected out of scope as it is not working in 42.XX and below version
  - 100 - Stock - receive once loaded stn's were not getting reset

- Sprint 11
  Defect Nos:
  - 11-wrong placeholder bug fixed
  - 27-validation bug fixed
  - 25 - No proper warning message if user enters invalid variant codes
  - 40- Now, Restore the values to old state if cancel is clicked
  - 21- Now, Restore the values to old state if cancel is clicked
  - 38- Home page Brand code is missing even after updating brand code
  - 4-City/Town mandatory field dropdown does not have any options
  - 6-something went wrong message is displayed
  - 37- CIN Number field accepting alpha numeric and special characters
  - 36- GST Registration Number field error msg is not matching with the scenario
  - 9- Configure payment modes,Location Price group mapping modules are not implemented in Location
  - 23-Add unit as 'gms' in all the weight related fields in Location
  - 2-Labels should be in capital letters in ADD NEW scree. Kindly refer screen shot.
    Labels are proper in view and edit screen.
  - 45- Corp credentials are shared to QA and issue is resolved
  - 51- STN search functionality is working in stock issue page
  - 52- Line item update is working in stock issue
  - 53- Line item update is working in stock issue
  - 16- The Item weight is taking the value of Item Quantity
  - 15- Email Id field is not accepting capital letters while creating New Loan Issue
  - 32-when new location created application does not display successfull confirmation

## [IR-0.0.35](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.35%0DIR-0.0.34): (Wed 6-Oct-19 6:35 PM)

## Features Added

- NAP-1188: Stock request Cancellation by requesting boutique
- NAP-1224: IBT lot number Selection

### Bug Fixed

- Sprint 11
  o Defect Nos:
  34 -
  26 -
  24 -
  18 -
  14 -
  16 -

- Sprint 10
  o Defect Nos:
  81 -
  82 -
  80 -
  83 -

- Sprint 7_8_9
  o Defect Nos:
  164 -
  226 -
  172 -
  171 -
  155 -
  153 -
  124 -
  101 -
  60 -
  126 -
  57 -

## [IR-0.0.34](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.34%0DIR-0.0.33): (Wed 6-Oct-19 6:35 PM)

### Bug Fixed

- Sprint 10
  o Defect Nos:
  40 -
  18 -
  4 -
  84 -
  75 -
  72 -
  49 -
  54 -
  59 -

- Sprint 7_8_9
  o Defect Nos:
  119 -
  151 -
  219 -
  220 -
  196 -

## [IR-0.0.33](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.33%0DIR-0.0.32): (Wed 6-Oct-19 6:35 PM)

## Features Added

NAP-141 Location Master Management

## [IR-0.0.32](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.32%0DIR-0.0.31): (Thu 17-Oct-19 7:03 PM)

### Bug Fixed

- Rounding off to 3 decimal digits in weight field in TEP/FEP
- Radar issues- 72 (Sprint 10)
- Correction of some notification messages
- Issue with count when an item is verified after searching with barcode scanner
- Bin-to Bin: Added shortcut keys for focus, as follows
  o CardList (F4)
  o SearchList(F2)
  o Dropdown(F7),
  o Detail page search(F2)

## [IR-0.0.31](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.31%0DIR-0.0.30): (Wed 16-Oct-19 1:53 PM)

### Bug Fixed

- Rounding off to 3 decimal digits in weight field in issues
- Radar issues- 79, 68,84, 55 (Sprint 10)
- Correction of error message in Other issues

## [IR-0.0.30](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.30%0DIR-0.0.29): (Wed 16-Oct-19 1:03 AM)

### Bug Fixed

- Duplicate Bins were getting added in the BINS listing In-Stock Management landing page
- Request Creation was not working in IBT
- Correction of Error Messages
- Unwanted fonts removed
- Radar Defect No: 88(in sprint 10)

## [IR-0.0.29](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.29%0DIR-0.0.27) : (Mon 14-Oct-19 11:19 PM)

## Features Added

NAP-1225 – Adding Exhibition Bin, LOSS Bin and LOAN Bin

### Bug Fixed

- 220, 216, 215, 214, 211, 196, 195, 167, 178, 164 From (Sprint 7_8_9)
- 23,47,46,40,39,25, 45,26,27,30,32,50,35,53,58,63,64,65,67,75,77 from (Sprint 10)

## IR-0.0.28 : (Mon 14-Oct-19 1:24 AM)

## Features Added

NAP-49,NAP-50.NAP-56,NAP-1194,NAP-1193

## [IR-0.0.27](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.27%0DIR-0.0.26) : (Sat 12-Oct-19 12:12 AM)

### Bug Fixed (Radar issues)

- 19,15 fixed from sprint 10
- 7,6 Deferred from sprint 10
- 69 fixed from sprint 7,8,9
- 57,51,45,42,32 from sprint 10

## [IR-0.0.26](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.26%0DIR-0.0.23) : (Fri 11-Oct-19 1:16 AM)

## Features Added

- NAP-82-,NAP-83,NAP-46,NAP-54,NAP-59,NAP-58,NAP-1191,NAP-1198,NAP-1197,NAP-1196,NAP-1250,NAP-1249

## IR-0.0.24 (Fri 04-Oct-19 10:58 PM):

## Features Added

- NAP-46.A,NAP-44.A,NAP-45.A
  Known Issues:
- Confirm transfer functionality is not working for L3User due to API issues.

## [IR-0.0.23](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.23%0DIR-0.0.22) : (Tue 01-Oct-19 9:48 PM)

### Bugs Fixed

Radar Issue #:
196, 195, 185, 170, 169, 162, 157, 152, 136, 123, 165,164,161,155,154,150,149,148,147, 187,186, 125,183,189,192,193,194 203,204,205,207,119,144
Radar Issue Deferred
51, 17, 102,130, 115,138,182,188,208
Radar Issue Rejected
120,191

### Pending Items

- Sorting & Filtering
- Confirmation Functionality in Stock Receipt is not functioning due to latest backend changes.
- This will be fixed in the next release.
- List of address for selecting address popup in TEP /GEP
- Courier details form for employee information in TEP/GEP
- Updating of inventory from backend is not happening in TEP/GEP
- TEP/GEP flow for L3USER (API pending)
- Adding Pre-tax vale, tax value ,price-Per Unit, Gross Weight column at Item level in product listing grid in all pages.
- Display of Total Value for Exhibition and Loan
- Auto calculation of total value if we change any item level value.
- Total Measured weight calculation in the Header Level.
- Showing Solitaire Item and its UIN verification.
- Filters with multiple values ( ex : productCategory : category1 or category2).
- Reason for delay : STN date and Document Date ( as confirmed by the BA with clients) only L1
- Handling of scenario wherein adding of variant which is already selected in Issue to CFA is not done. Same is applicable for all those page were we have selected product tab.

## [IR-0.0.22](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.22%0DIR-0.0.19) : (Mon 23-Sep-19 10:09 PM)

### Bugs Fixed

Test Defect # 142,122,121,114,113,112,111,110,109,108,101,100,95,94,93,92,82,81,76,75,64,63

## Features Added

NAP-42,NAP-64,NAP-70,NAP-71,NAP-72,NAP-73,NAP-74,NAP-75,NAP-51,NAP-53,NAP-47,NAP-48 & NAP-55, NAP-57
Pending items:

- Logout functionality not working
- L3 related Other Issue, TEP, GEP
- Implementation of Filter and Sort functionality in product/item listing grid.

## [IR-0.0.19](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.19%0DIR-0.0.18)(Tue 17-Sep-19 1:50 PM) – Dev Build

## [IR 0.0.18](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.18%0DIR-0.0.17) (Wed 11-Sep-19 11:29 PM)

## Features Added

- Listing of Issue to factory STN.
  Know Issues:
- User can search Issues STN from the listing page
- Listing of Issue to Boutique STN
- NAP-38 Issue Invoice Return: Destination and product selection
- User can search Issues STN from the listing page

Pending item:

- Bar code integration is still pending. Will be deployed with next build.
- Upload of .csv file is still pending.
- NAP-39 Issue Invoice Return: Confirm and create Invoice no
- Update of Quantity in the selected item tab is not implemented.
  Bug Fixed
- IBT – Request Received and Request Sent –Bug Fixing (i.e. Test Defect No.#:30)

## [IR 0.0.17](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.17%0DIR-0.0.16) : New Build Deployed(Thu 29-Aug-19 8:52 PM)

## Features Added

- Bin-Bin Transfer functionality

### Bug Fixed

- IBT – Request Received and Request Sent –Bug Fixing (i.e. Test Defect No.#:61,62)

## [IR 0.0.16](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.16%0DIR-0.0.15) : New Build Deployed(Wed 28-Aug-19 12:20 AM)

## Features Added

- Stock Receipt
- IBT – Request Received and Request Sent

## [IR 0.0.15](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.15%0DIR-0.0.14) : New Build Deployed(Mon 26-Aug-19 11:16 PM)

### Bug Fixed

- Issue ID’s from Test Defect List : 11, 13, 21, 27, 34, 37, 38, 42,47, 48, 49, 50, 52
- Issue ID’s from Issue List : 38,36
  Feature Added
- Bar code reader integration
- Hot key feature to focus the search box on F2 press.

## [IR 0.0.14](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.14%0DIR-0.0.13) : New Build Deployed(Tue 20-Aug-19 11:33 PM)

### Bug Fixed

- copy text issue in notification messages and bin dropdown fixed.

## [IR 0.0.13](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.13%0DIR-0.0.12) : New Build Deployed(Tue 20-Aug-19 8:03 PM

### Bug Fixed

- All issues reported in radar relating to Stock Receipt and IBT ( Request listing and Sending Request) fixed.
- Stock Issue Listing for L1& L3 is working as expected. These module can be tested.

## [IR 0.0.12](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.12%0DIR-0.0.11) : New Build Deployed(Wed 14-Aug-19 10:57 PM)

### Bug Fixed

- Radar Issue # :46 Last item row is not visible because the "SAVE" button menu covers the last row.
- Radar Issue # :43 Correct Tab is not selected after navigating back from a tile
- Radar Issue # :42 The default value of Assigned Bin is not PURCFA for L3USER (NAP-35)
- Radar Issue # :39 System is allowing the user to assign actual date of receipt less than the STN date (NAP-99)
- Radar Issue # :37 Reason for delay field is not displayed even though the STN date and the Inwarding (NAP-99)
- Radar Issue # :31 L1USER\*Receive from factory_Non Verified products
- Radar Issue # :28 L1User_Recv frm other Botqs_DB and UI data not matching
- Radar Issue # :25 L1 USer login\* before verifiying all the item lines
- Radar Issue # :22 L1 user logins- Inventory module-In-Stock management page and Receive Stock from Factory
- Radar Test Issue #: 12 For L3 User - System is allowing user to assign received invoice date less than the invoice date (N
- Radar Test Issue #: 10 Show More button is displayed when no products are present or when the search returns no results
- Radar Test Issue #: 9 For L3 User the STNs are not arranged in oldest to newest to format (NAP-34)
- Radar Test Issue #: 8 Non-Verified Products tab and Verified Products tab show count of only displayed products

## [IR 0.0.11](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.11%0DIR-0.0.10) : New Build Deployed(Tue 06-Aug-19 12:35 PM)

### Bug Fixed

- Fixed the issue wherein Bin Group was not showing up in the Verified Product list tab.

## [IR 0.0.10](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.10%0DIR-0.0.8) : New Build Deployed(Mon 05-Aug-19 11:36 PM)

### Bug Fixed

- Fixed the issue with shortkey when input control is in focus.
- Fixed the typo errors in stock vertify component
- Fixed the bug where total value was shown against item in stock verify component.
- Fixed the currency undefined issue.
- Redirecting to stock receive page after closing the notification message displaying doc number.
- Added the Received Date Selection functionality in the Stock Receive details page.

## [IR 0.0.8](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.8%0DIR-0.0.7) : New Build Deployed(Mon 05-Aug-19 7:19 PM)

### Bug Fixed

- L3 API were referred for L1 Store. Now this is fixed.
- We have made the default measureWeight of the product in stock receive grid equal to STN Weight of the product. Previously it was zero because of which weight mismatch error was shown always. Now we made a positive case as default.

## [IR 0.0.7](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.7%0DIR-0.0.6): New Build Deployed(Sat 03-Aug-19 9:36 PM)

## Features Added

- NAP-98 Receive Invoice from Factory: View details of Invoice
- NAP-37 Receive Invoice from Factory: Save stock received and generate the Receipt Invoice Doc no

## [IR 0.0.6](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.6%0DIR-0.0.4) : New Build Deployed(Sat 03-Aug-19 12:53 AM)

## Features Added

- NAP-16 Stock Receipt from Factory: Search and view STN
- NAP-16 Stock Receipt from Factory: Search and view STN
- NAP-30 Landing Page of Inventory Module
- NAP-31 Stock Receipt from Factory: View & verify details of stock received from factory
- NAP-33 Stock Receipt from Factory: Save the stock received and generate the STN Receipt Doc no
- NAP-34 Receive Invoice from Factory: Search and view Invoice Number details
- NAP-38 In Stock- Request from Another Boutique: Create request for stock transfer
- NAP-43 Stock Receipt from another boutique: Search and view STN
- NAP-44 Stock Receipt from another boutique: View & verify details of stock received
- NAP-45 Stock Receipt from another boutique: Save the stock received and generate the Doc no
- NAP-87 Stock Receipt from Factory: View and update details of STN
- NAP-99 Stock Receipt from another boutique: View and Update details of STN

## [IR 0.0.4](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.4%0DIR-0.0.2): New Build Deployed (01-Aug-19 9:16 PM)

## [IR 0.0.2](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.2%0DIR-0.0.2) (01-Aug-19)

- Initial Release

## [IR-0.0.5](https://bitbucket.org/titan-poss/poss-web/branches/compare/IR-0.0.5%0DIR-0.0.4) (2019-08-01)

### Features

### Bug Fixes

### Performance Improvements

### BREAKING CHANGES

### Code Refactoring

### DEPRECATION
